
function autofill() {
	var nomr = $('#nomr').val();
	$.ajax({
		type 	: 'POST',
		url 	: 'proses.php',
		data 	: {nomr:nomr},
		success : function(data){
			var json = data,
			obj  = JSON.parse(json);

			$('#nama').val(obj.nama);
			$('#jenis_kelamin').val(obj.jk);
			$('#kode_jenis_kelamin').val(obj.kode_jenis_kelamin);
			$('#id_jenis_kelamin').val(obj.id_jenis_kelamin);
			$('#ruangan').val(obj.ruangan);
			$('#id_ruangan').val(obj.id_ruangan);
			$('#penjamin').val(obj.penjamin);
			$('#id_penjamin').val(obj.id_penjamin);
			$('#penanggung').val(obj.penanggung);
			$('#kel_usia').val(obj.kel_usia);	
			$('#nama_penanggung').val(obj.nama_penanggung);	
			$('#umur').val(obj.umur);
			$('#umur_t').val(obj.umur_t);			
			$('#tgl_masuk').val(obj.tgl_masuk);	
			$('#tgl_lahir').val(obj.tgl_lahir);		
			$('#ktp').val(obj.ktp);
			$('#warganegara').val(obj.warganegara);
			$('#pekerjaan').val(obj.pekerjaan);
			$('#telpon').val(obj.telpon);
			$('#alamat').val(obj.alamat);
			$('#jk').val(obj.jk);	
			$('#nopen').val(obj.nopen);	
			$('#icd').val(obj.icd);	
			$('#diagnosa').val(obj.diagnosa);
			$('#diagmasuk').val(obj.diagmasuk);
			$('#tglusia').val(obj.tglusia);
			$('#telpkel').val(obj.telpkel);	
			$('#namakel').val(obj.namakel);	
			$('#norm').val(obj.norm);
			$('#postid').val(obj.postid);		
		}
	});
}

$(document).ready(function() {
	$('#datatabelss').dataTable( {
		"bProcessing": false,
		"bServerSide": true,
		"sAjaxSource": "data.php",
		"order": [[ 0, "desc" ]],
		"scrollCollapse": true,
		"aoColumns": [
		null,
		null,
		null,
		null,
		null,
		{
			"mData": "5", <!-- Ini adalah untuk Link ID urutan kolom seperti table mulai dari 0 untuk data pertama -->
			"mRender": function ( data, type, full ) {
				return '<a class="green" href=index.php?page=edit_h&id='+data+'><i class="glyphicon glyphicon-pencil btn btn-success btn-sm"></i></a><a class="red" href=index.php?page=delet_h&id='+data+'> <i class="glyphicon glyphicon-trash btn btn-danger btn-sm"></i></a>';			
			}
		}
		]
	} );
} );

$(document).ready(function() {
	$('#datacovid').dataTable( {
		"bProcessing": false,
		"bServerSide": true,
		"sAjaxSource": "datacovid.php",
		"order": [[ 0, "desc" ]],
		"scrollCollapse": true,
		"aoColumns": [
		null,
		null,
		null,
		null,
		null,
		null,
		{
			"mData": "6", <!-- Ini adalah untuk Link ID urutan kolom seperti table mulai dari 0 untuk data pertama -->
			"mRender": function ( data, type, full ) {
				return '<a class="green" href=index.php?page=covidedit&id='+data+'><i class="glyphicon glyphicon-pencil btn btn-success btn-sm"></i></a><a class="red" href=index.php?page=deletcovid&id='+data+'> <i class="glyphicon glyphicon-trash btn btn-danger btn-sm"></i></a><a class="green" href=http://simrspknrskd.com/cetakan_hanif/cetakanppi/cetakcovid.php?format=pdf&id='+data+' target="_blank"> <i class="glyphicon glyphicon-print btn btn-warning btn-sm"></i></a>';	

			}
		}
		]
	} );
} );	

$(document).ready(function() {
	$('#tbisopo').dataTable( {
		"bProcessing": false,
		"bServerSide": true,
		"sAjaxSource": "modul/isopo/dataisopo.php",
		"order": [[ 0, "desc" ]],
		"scrollCollapse": true,
		"aoColumns": [
		null,
		null,
		null,
		null,
		null,
		{
			"mData": "5", <!-- Ini adalah untuk Link ID urutan kolom seperti table mulai dari 0 untuk data pertama -->
			"mRender": function ( data, type, full ) {
				return '<a class="green" href=index.php?page=edit_isopo&id='+data+'><i class="glyphicon glyphicon-pencil btn btn-success btn-sm"></i></a><a class="red" href=index.php?page=delet_isopo&id='+data+'> <i class="glyphicon glyphicon-trash btn btn-danger btn-sm"></i></a>';			
			}
		}
		]
	} );
} );

$(document).ready(function() {
	$('#tbisone').dataTable( {
		"bProcessing": false,
		"bServerSide": true,
		"sAjaxSource": "modul/isone/dataisone.php",
		"order": [[ 0, "desc" ]],
		"scrollCollapse": true,
		"aoColumns": [
		null,
		null,
		null,
		null,
		null,
		{
			"mData": "5", <!-- Ini adalah untuk Link ID urutan kolom seperti table mulai dari 0 untuk data pertama -->
			"mRender": function ( data, type, full ) {
				return '<a class="green" href=index.php?page=edit_isone&id='+data+'><i class="glyphicon glyphicon-pencil btn btn-success btn-sm"></i></a><a class="red" href=index.php?page=delet_isone&id='+data+'> <i class="glyphicon glyphicon-trash btn btn-danger btn-sm"></i></a>';			
			}
		}
		]
	} );
} );

$(document).ready(function() {
	$('#tbsitostika').dataTable( {
		"bProcessing": false,
		"bServerSide": true,
		"sAjaxSource": "modul/sitostatika/datasitostatika.php",
		"order": [[ 0, "desc" ]],
		"scrollCollapse": true,
		"aoColumns": [
		null,
		null,
		null,
		null,
		null,
		null,
		{
			"mData": "6", <!-- Ini adalah untuk Link ID urutan kolom seperti table mulai dari 0 untuk data pertama -->
			"mRender": function ( data, type, full ) {
				return '<a class="green" href=index.php?page=edit_sitostatika&id='+data+'><i class="glyphicon glyphicon-pencil btn btn-success btn-sm"></i></a><a class="red" href=index.php?page=delet_sitostatika&id='+data+'> <i class="glyphicon glyphicon-trash btn btn-danger btn-sm"></i></a>';			
			}
		}
		]
	} );
} );

$(document).ready (function () {
	$('#datetimepicker').datetimepicker({
		format: 'YYYY-MM-DD',
	});
})

$(document).ready (function () {
	$('#datetimepicker1').datetimepicker({
		format: 'YYYY-MM-DD',
	});
})

$(document).ready (function () {
	$('#datetimepicker11').datetimepicker({
		format: 'YYYY-MM-DD',
	});
})

$(document).ready (function () {
	$('#datetimepicker12').datetimepicker({
		format: 'YYYY-MM-DD',
	});
})

$(document).ready (function () {
	$('#datetimepicker13').datetimepicker({
		format: 'YYYY-MM-DD',
	});
})

$(document).ready (function () {
	$('#datetimepicker14').datetimepicker({
		format: 'YYYY-MM-DD',
	});
})

$(document).ready (function () {
	$('#datet1').datetimepicker({
		format: 'YYYY-MM-DD',
	});
})

$(document).ready (function () {
	$('#datet3').datetimepicker({
		format: 'YYYY-MM-DD',
	});
})

$(document).ready (function () {
	$('#datet2').datetimepicker({
		format: 'YYYY-MM-DD',
	});
})

$(document).ready (function () {
	$('#datetimepicker2').datetimepicker({
		format: 'YYYY-MM-DD h:m:s A',
	});
})

$(document).ready (function () {
	$('#datetimepicker3').datetimepicker({
		format: 'YYYY-MM-DD h:m:s A',
	});
})

$(document).ready (function () {
	$('#datetimepicker4').datetimepicker({
		format: 'YYYY-MM-DD h:m:s A',
	});
})

$(document).ready (function () {
	$('#datetimepicker5').datetimepicker({
		format: 'YYYY-MM-DD h:m:s A',
	});
})

$(document).ready (function () {
	$('#datetimepicker6').datetimepicker({
		format: 'YYYY-MM-DD h:m:s A',
	});
})



$(document).ready(function() {
	$('#dataTable1').DataTable( {
		dom: 'Bfrtip',
		buttons: [
		{
			extend: 'excel',
			orientation: 'potrait',
			pageSize: 'A4'
		},
		{
			extend: 'pdf',
			orientation: 'potrait',
			pageSize: 'A4'
		},
		{
			extend: 'print',
			orientation: 'potrait',
			pageSize: 'A4'
		}
		]
	} );
} );

$(document).ready(function() {
	$('#dataTable2').DataTable( {
		dom: 'Bfrtip',
		buttons: [
		{
			extend: 'excel',
			orientation: 'potrait',
			pageSize: 'A4'
		},
		{
			extend: 'pdf',
			orientation: 'potrait',
			pageSize: 'A4'
		},
		{
			extend: 'print',
			orientation: 'potrait',
			pageSize: 'A4'
		}
		]
	} );
} );

$(document).ready(function() {
	$('#dataTable3').DataTable();
} );

$(document).ready(function() {
	$('#tbratabundle').DataTable();
} );

$(document).ready(function() {
	$('#tabeldata').DataTable();
} );

$(document).ready(function() {
	$('#example').DataTable( {
        //"scrollY": 200,
        "scrollX": true
    } );
} );

$(document).ready(function() {
	$('#example1').DataTable( {
		"scrollX": true,
		"order": [[ 0, "desc" ]]
	} );
} );

$(document).ready(function() {
	$('#example2').DataTable( {
		dom: 'Bfrtip',
		buttons: [
		'excel', 'pdf', 'print'
		]
	} );
} );

$(document).ready(function() {
	$('#tbhand').DataTable( {
		"order": [[ 3, "desc" ]]
	} );
} );

$(document).ready(function() {
	$('#tbhandd').dataTable( {
		"bProcessing": false,
		"bServerSide": true,
		"sAjaxSource": "datahand.php",
		"order": [[ 0, "desc" ]],
		"scrollCollapse": true,
		"aoColumns": [
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		{
			"mData": "9", <!-- Ini adalah untuk Link ID urutan kolom seperti table mulai dari 0 untuk data pertama -->
			"mRender": function ( data, type, full ) {
				return '<a class="green" href=index.php?page=edit_hd&id='+data+'><i class="ace-icon fa fa-pencil bigger-130"></i></a> &nbsp; | &nbsp;<a class="red" href=index.php?page=delet_hd&id='+data+'> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>';			
			}
		}
		]
	} );
} );	

$(document).ready(function() {
	$('#tblhand').dataTable( {
		"bProcessing": false,
		"bServerSide": true,
		"sAjaxSource": "datalhand.php",
		"order": [[ 0, "desc" ]],
		"scrollCollapse": true,
		"aoColumns": [
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,

		{
			"mData": "9", <!-- Ini adalah untuk Link ID urutan kolom seperti table mulai dari 0 untuk data pertama -->
			"mRender": function ( data, type, full ) {
				return '<a class="green" href=index.php?page=edit_lhand&id='+data+'><i class="ace-icon fa fa-pencil bigger-130"></i></a> &nbsp; | &nbsp;<a class="red" href=index.php?page=delet_lhand&id='+data+'> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>';			
			}
		}
		]
	} );
} );

$(document).ready(function(){
	$('#obat').click(function(){
		var tanggal1  = $('#tanggal1').val();
		var tanggal2  = $('#tanggal2').val();
		var antibiotik  = $('#antibiotik').val();
		// console.log(ruangan);
		$.ajax({
			type 	: 'POST',
			url 	: 'modul/laporan/table_obat.php',
			data 	: { tanggal1:tanggal1, tanggal2:tanggal2, antibiotik:antibiotik},
			success : function(data){
				$('#table_obat').html(data);	
				$.ajax({
					type 	: 'POST',
					url 	: 'modul/laporan/chart_obat.php',
					data 	: { tanggal1:tanggal1, tanggal2:tanggal2, antibiotik:antibiotik},
					success : function(data){
						$('#chart_obat').html(data);						
					}
				});
			}
		});
	});
});

$(document).ready(function(){
	$('#handy').click(function(){
		var tanggal11  = $('#tanggal11').val();
		var tanggal12  = $('#tanggal12').val();
		var ruangan    = $('#ruangan').val();
		var petugas    = $('#petugas').val();
		// console.log(ruangan);
		$.ajax({
			type 	: 'POST',
			url 	: 'modul/laporan/table_handy.php',
			data 	: { tanggal11:tanggal11, tanggal12:tanggal12, ruangan:ruangan,petugas:petugas},
			success : function(data){
				$('#table_handy').html(data);	
				$.ajax({
					type 	: 'POST',
					url 	: 'modul/laporan/chart_handy.php',
					data 	: { tanggal11:tanggal11, tanggal12:tanggal12, ruangan:ruangan,petugas:petugas},
					success : function(data){
						$('#chart_handy').html(data);						
					}
				});
			}
		});
	});
});

$(document).ready(function(){
	$('#bundles').click(function(){
		var tanggal13  = $('#tanggal13').val();
		var tanggal14  = $('#tanggal14').val();
		var bundle     = $('#bundle').val();
		var nomr       = $('#nomr').val();
		// console.log(ruangan);
		$.ajax({
			type 	: 'POST',
			url 	: 'modul/laporan/table_bundle.php',
			data 	: { tanggal13:tanggal13, tanggal14:tanggal14, bundle:bundle,nomr:nomr},
			success : function(data){
				$('#table_bundle').html(data);	
				$.ajax({
					type 	: 'POST',
					url 	: 'modul/laporan/chart_bundle.php',
					data 	: { tanggal13:tanggal13, tanggal14:tanggal14, bundle:bundle,nomr:nomr},
					success : function(data){
						$('#chart_bundle').html(data);						
					}
				});
			}
		});
	});
});

$(document).ready(function(){
	$('#ctk_bundl').click(function(){
		var tanggal13  = $('#tanggal13').val();
		var tanggal14  = $('#tanggal14').val();
		var bundle     = $('#bundle').val();
		var nomr       = $('#nomr').val();
		// console.log(ruangan);
		$.ajax({
			type 	: 'POST',
			url 	: 'modul/cetakan/cetakbundle.php',
			data 	: { tanggal13:tanggal13, tanggal14:tanggal14, bundle:bundle,nomr:nomr},
			success : function(data){
					//$('#table_bundle').html(data);					
				}
			});
	});
});

jQuery(function($) {			
	var myTable = 
	$('#dynamic-table')
	//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
	.DataTable( {
		bAutoWidth: false,
		"aoColumns": [
		{ "bSortable": false },
		null, null,null, null, null, null,
		{ "bSortable": false }
		],
		"aaSorting": [],
		
		
		//"bProcessing": true,
		//"bServerSide": true,
		//"sAjaxSource": "http://127.0.0.1/table.php"	,

		//,
		"sScrollY": "200px",
		//"bPaginate": false,

		//"sScrollX": "100%",
		//"sScrollXInner": "120%",
		//"bScrollCollapse": true,
		//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
		//you may want to wrap the table inside a "div.dataTables_borderWrap" element

		//"iDisplayLength": 50
		select: {
			style: 'multi'
		}
	} );		
});

$(document).ready(function() {
	$('#dataTable').DataTable(
		{"scrollX": true,
	}
	);
} );

jQuery(function($) {			
	if(!ace.vars['touch']) {
		$('.chosen-select').chosen({allow_single_deselect:true}); 
	//resize the chosen on window resize

	$(window)
	.off('resize.chosen')
	.on('resize.chosen', function() {
		$('.chosen-select').each(function() {
			var $this = $(this);
			$this.next().css({'width': $this.parent().width()});
		})
	}).trigger('resize.chosen');
	//resize chosen on sidebar collapse/expand
	$(document).on('settings.ace.chosen', function(e, event_name, event_val) {
		if(event_name != 'sidebar_collapsed') return;
		$('.chosen-select').each(function() {
			var $this = $(this);
			$this.next().css({'width': $this.parent().width()});
		})
	});


	$('#chosen-multiple-style .btn').on('click', function(e){
		var target = $(this).find('input[type=radio]');
		var which = parseInt(target.val());
		if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
		else $('#form-field-select-4').removeClass('tag-input-style');
	});
}				
});
