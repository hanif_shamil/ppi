<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>PPI</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
   <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- Theme style -->
    
    <!-- iCheck -->
    <!--<link href="aset/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />-->
	
	<link rel="shortcut icon" href="assets/images/ico.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
/*	@import url(https://fonts.googleapis.com/css?family=Exo:400,500,500italic,400italic,600,600italic,700,700italic,800,800italic,300,300italic);*/

	body {
	padding-top: 200px;
	/*The below background is just to add some color, you can set this to anything*/
	background: url(assets/images/desktop-3246124_1920.jpg) no-repeat;
	}

	.login-form{width:400px;}
	.login-title{font-family: 'Exo', sans-serif;text-align:center;color: #ffffff;}
	.login-userinput{margin-bottom: 10px;}
	.login-button{margin-top:20px;}
	.login-options{margin-bottom:0px;}
	.login-forgot{float: right;}
	</style>
	</head>
<body>
<div class="container login-form" style="margin-top:1px; border:1px solid #cecece; border-radius:10px">
<h3 class="login-title"><img src="assets/images/13747.png" class="left" style="width:60px"> R.S Kanker "Dharmais"</h3>
	<h4 class="login-title">Aplikasi Pengendalian & Pencegahan Infeksi</h4>	
	<div class="panel panel-default" ><span class="border border-primary"></span>
		<div class="panel-body">
			 <form name="login-form" action="login_act.php"  method="post">
				<div class="input-group login-userinput">
					<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
					<input id="txtUser" type="text" class="form-control" name="username" placeholder="Username">
				</div>
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
					<input  id="txtPassword" type="password" class="form-control" name="password" placeholder="Password">
					<span id="showPassword" class="input-group-btn">
						<button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
					</span>  
				</div>
				<button class="btn btn-primary btn-block login-button" type="submit"><i class="fa fa-sign-in"></i> Login</button>
				
			</form>			
		</div>
	</div>
	<!--<h4 class="login-title">Silahkan Login dengan Username & Password Indikator Mutu</h4>-->
</div>
  <script src="assets/js/jquery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <!--<script src="aset/plugins/iCheck/icheck.min.js" type="text/javascript"></script>-->
		<script>
		window.onload = function(){$("#showPassword").hide();}

		$("#txtPassword").on('change',function() {  
		if($("#txtPassword").val())
		{
		$("#showPassword").show();
		}
		else
		{
		$("#showPassword").hide();
		}
		});

		$(".reveal").on('click',function() {
		var $pwd = $("#txtPassword");
		if ($pwd.attr('type') === 'password') 
		{
		$pwd.attr('type', 'text');
		} 
		else 
		{
		$pwd.attr('type', 'password');
		}
		});
		</script>
  </body>
</html>