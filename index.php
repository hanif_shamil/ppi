<?php
session_start(); 
if(empty($_SESSION['username']))  {
	header("location:login.php");
}
include 'config.php';
//include_once('plugins/aset/class/tcpdf/tcpdf.php');
//include_once("plugins/aset/class/PHPJasperXML.inc.php");
//include 'inc/inc.library.php';
//include "setting.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>PPI</title>

	<meta name="description" content="overview &amp; stats" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<link rel="stylesheet" href="assets/css/bootstrap-datepicker3.min.css" />		
	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />
	<!-- page specific plugin styles -->
	<link rel="stylesheet" href="assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" href="assets/css/chosen.min.css" />

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css" />
	<link rel="stylesheet" href="assets/css/bootstrap-colorpicker.min.css" />
	<!-- text fonts -->
	<!-- <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" /> -->
	<!-- ace styles -->
	<link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
			<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
			<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

			<!-- inline styles related to this page -->
			<link rel="stylesheet" href="assets/css/bootstrap-duallistbox.min.css" />
			<link rel="stylesheet" href="assets/css/bootstrap-multiselect.min.css" />
			<link rel="stylesheet" href="assets/css/select2.min.css" />
			<!-- ace settings handler -->
			<script src="assets/js/ace-extra.min.js"></script>
			<link rel="shortcut icon" href="assets/images/13747.png">
			<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-header pull-left">
					<a href="index.html" class="navbar-brand">
						<small>
							<i class="fa fa-empire"></i>
							Aplikasi Pencegahan & Pengendalian Infeksi
						</small>
					</a>
				</div>
				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">												
						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo" />-->
								<span class="user-info">
									<small>Welcome,</small>
									<?php echo $_SESSION['nama'] ?>
								</span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">								
								<li>
									<a href="">
										<i class="ace-icon fa fa-user"></i>
										Profile
									</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="logout.php">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			<?php
			include "menu.php";			
			?>
			<div class="main-content">
				<div class="main-content-inner">
					<?php 
					include "content.php";
					?>					
				</div>
			</div><!-- /.main-content -->

			

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="blue bolder">Copyright &copy; 2018 | PPI, Developed by AbuShamil (SIMRS)</span>
					</div>
				</div>
			</div>

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
		<script src="assets/js/jquery-1.11.3.min.js"></script>
	<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->
			<script src="assets/js/main.js"></script>
			<script src="assets/js/jquery-ui.custom.min.js"></script>
			<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
			<script src="assets/js/chosen.jquery.min.js"></script>
			<script src="assets/js/spinbox.min.js"></script>
			<script src="assets/js/bootstrap-datepicker.min.js"></script>
			<script src="assets/js/moment.min.js"></script>
			<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
			<script src="assets/js/bootstrap-colorpicker.min.js"></script>
			<script src="assets/js/jquery.knob.min.js"></script>
			<script src="assets/js/autosize.min.js"></script>
			<script src="assets/js/jquery.inputlimiter.min.js"></script>
			<script src="assets/js/jquery.maskedinput.min.js"></script>
			<script src="assets/js/bootstrap-tag.min.js"></script>
			<script src="assets/js/jquery.dataTables.min.js"></script>
			<script src="assets/js/jquery.dataTables.bootstrap.min.js"></script>
			<script src="assets/js/dataTables.buttons.min.js"></script>
			<script src="assets/js/buttons.flash.min.js"></script>
			<script src="assets/js/buttons.html5.min.js"></script>
			<script src="assets/js/buttons.print.min.js"></script>
			<script src="assets/js/buttons.colVis.min.js"></script>
			<script src="assets/js/dataTables.select.min.js"></script>
			<!-- ace scripts -->
			<script src="assets/js/ace-elements.min.js"></script>
			<script src="assets/js/ace.min.js"></script>
			<script src="assets/js/highcharts.js"></script>
			<script src="assets/js/exporting.js"></script>
			<script src="assets/js/export-data.js"></script>
			<!-- page specific plugin scripts -->
			<script src="assets/js/jquery.bootstrap-duallistbox.min.js"></script>
			<script src="assets/js/jquery.raty.min.js"></script>
			<script src="assets/js/bootstrap-multiselect.min.js"></script>
			<script src="assets/js/select2.min.js"></script>
			<script src="assets/js/jquery-typeahead.js"></script>
			<script type="text/javascript">
				$(function () {
					var chart;
					$(document).ready(function() {
						$.getJSON("modul/laporan/modul.php", function(json) {

							chart = new Highcharts.Chart({
								chart: {
									renderTo: 'mygraph',
									type: 'column'

								},
								title: {
									text: 'Rata-rata nilai bundles PPI'

								},
								subtitle: {
									text: '(Total)'

								},
								xAxis: {
									categories: [
									"BUNDLES"]
								},
								yAxis: {
									title: {
										text: 'Nilai Rata-rata'
									},
									plotLines: [{
										value: 0,
										width: 1,
										color: '#808080'
									}]
								},

								tooltip: {
									formatter: function() {
										return '<b>'+ this.series.name +'</b><br/>'+
										this.x +': '+ this.y;
									}
								},
								legend: {
									align: 'right',
									verticalAlign: 'middle',
									layout: 'verticalAlign'
								},
								plotOptions: {
									column: {
										dataLabels: {
											enabled: true
										}
									}
								},
								series: json
							});
						});

					});

				});

				$(function () {
					var chart;
					$(document).ready(function() {
						$.getJSON("modul/laporan/modul.php", function(json) {

							chart = new Highcharts.Chart({
								chart: {
									renderTo: 'mygraph2',
									type: 'column'

								},
								title: {
									text: 'Rata-rata nilai bundles PPI'

								},
								subtitle: {
									text: '(Total)'

								},
								xAxis: {
									categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
									"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
								},
								yAxis: {
									title: {
										text: 'Nilai Rata-rata'
									},
									plotLines: [{
										value: 0,
										width: 1,
										color: '#808080'
									}]
								},

								tooltip: {
									formatter: function() {
										return '<b>'+ this.series.name +'</b><br/>'+
										this.x +': '+ this.y;
									}
								},
								legend: {
									align: 'right',
									verticalAlign: 'middle',
									layout: 'verticalAlign'
								},
								plotOptions: {
									line: {
										dataLabels: {
											enabled: true
										},
										enableMouseTracking: true
									}
								},
								series: json
							});
						});

					});

				});

				Highcharts.chart('handy', {
					data: {
						table: 'dataTable3'
					},
					chart: {
						type: 'column'
					},
					title: {
						text: 'Grafik Rata-rata nilai Hand Hygine Tahun (<?= date("Y"); ?>)'
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Nilai',
							align: 'high'
						},
						labels: { overflow: 'justify' }
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.series.name +'</b><br/>'+
							this.y;
						}
					},
					legend: {
						align: 'right',
						verticalAlign: 'middle',
						layout: 'verticalAlign'
					},
					plotOptions: {
						column: {
							dataLabels: {
								enabled: true,
								inside: false,
								crop: false,
								overflow: 'none'
							}
						}
					}
				});
			</script>

			<script type="text/javascript">
				Highcharts.chart('chartcssd', {
					data: {
						table: 'tabelcssd'
					},
					chart: {
						type: 'column'
					},
					title: {
						text: 'Grafik Audit CSSD Tahun (<?= date("Y"); ?>)'
					},
					yAxis: {
						allowDecimals: false,
						title: {
							text: 'Nilai Rata-rata'
						}
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.series.name +'</b><br/>'+
							this.y;
						}
					},
					legend: {
						align: 'right',
						verticalAlign: 'middle',
						layout: 'verticalAlign'
					},
					plotOptions: {
						column: {
							dataLabels: {
								enabled: true,
								inside: false,
								crop: false,
								overflow: 'none'
							}
						}
					}
				});	
			</script>
			<script type="text/javascript">
				Highcharts.chart('chartkamarjnz', {
					data: {
						table: 'tabelkamarjnz'
					},
					chart: {
						type: 'column'
					},
					title: {
						text: 'Grafik Audit Kamar Jenazah Tahun (<?= date("Y"); ?>)'
					},
					yAxis: {
						allowDecimals: false,
						title: {
							text: 'Nilai Rata-rata'
						}
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.series.name +'</b><br/>'+
							this.y;
						}
					},
					legend: {
						align: 'right',
						verticalAlign: 'middle',
						layout: 'verticalAlign'
					},
					plotOptions: {
						column: {
							dataLabels: {
								enabled: true,
								inside: false,
								crop: false,
								overflow: 'none'
							}
						}
					}
				});	
			</script>
			<script type="text/javascript">
				Highcharts.chart('chartlaundry', {
					data: {
						table: 'tabellaundry'
					},
					chart: {
						type: 'column'
					},
					title: {
						text: 'Grafik Audit Laundry Tahun (<?= date("Y"); ?>)'
					},
					yAxis: {
						allowDecimals: false,
						title: {
							text: 'Nilai Rata-rata'
						}
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.series.name +'</b><br/>'+
							this.y;
						}
					},
					legend: {
						align: 'right',
						verticalAlign: 'middle',
						layout: 'verticalAlign'
					},
					plotOptions: {
						column: {
							dataLabels: {
								enabled: true,
								inside: false,
								crop: false,
								overflow: 'none'
							}
						}
					}
				});	
			</script>
			<script type="text/javascript">
				Highcharts.chart('chartgizi', {
					data: {
						table: 'tabelgizi'
					},
					chart: {
						type: 'column'
					},
					title: {
						text: 'Grafik Audit Gizi Tahun (<?= date("Y"); ?>)'
					},
					yAxis: {
						allowDecimals: false,
						title: {
							text: 'Nilai Rata-rata'
						}
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.series.name +'</b><br/>'+
							this.y;
						}
					},
					legend: {
						align: 'right',
						verticalAlign: 'middle',
						layout: 'verticalAlign'
					},
					plotOptions: {
						column: {
							dataLabels: {
								enabled: true,
								inside: false,
								crop: false,
								overflow: 'none'
							}
						}
					}
				});	
			</script>
			<script type="text/javascript">
				$(function(){
					$('#diagnosa').select2({
					//minimumInputLength: 3,
					allowClear: true,
					placeholder: 'Masukkan nama diagnosa',
					ajax: {
						dataType: 'json',
						url: 'datadiagnosa.php',
						delay: 250,
						data: function(params) {
							return {
								search: params.term
							}
						},
						processResults: function (data, page) {
							return {
								results: data
							};
						},
					}
				})
				});

				Highcharts.chart('ratabundle', {
					data: {
						table: 'tbratabundle'
					},
					chart: {
						type: 'column'
					},
					title: {
						text: 'Grafik Rata-rata nilai Bundles Tahun (<?= date("Y"); ?>)'
					},
					yAxis: {
						allowDecimals: false,
						title: {
							text: 'Nilai Rata-rata'
						}
					},
					tooltip: {
						formatter: function() {
							return '<b>'+ this.series.name +'</b><br/>'+
							this.y;
						}
					},
					legend: {
						align: 'right',
						verticalAlign: 'middle',
						layout: 'verticalAlign'
					},
					plotOptions: {
						column: {
							dataLabels: {
								enabled: true,
								inside: false,
								crop: false,
								overflow: 'none'
							}
						}
					}
				});
			</script>
			<script type="text/javascript">
				$(document).ready(function() {
					$('#tbapdnew').dataTable( {
						"bProcessing": false,
						"bServerSide": true,
						"sAjaxSource": "dataapdnew.php",
						"order": [[ 0, "desc" ]],
						"scrollCollapse": true,
						"aoColumns": [
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						{
							"mData": "11", <!-- Ini adalah untuk Link ID urutan kolom seperti table mulai dari 0 untuk data pertama -->
							"mRender": function ( data, type, full ) {
								return '<a class="green" href=index.php?page=apdnew_edit&id='+data+'><i class="ace-icon fa fa-pencil bigger-130"></i></a> &nbsp; | &nbsp;<a class="red" href=index.php?page=delet_apdnew&id='+data+'> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>';			
							}
						}
						]
					} );
				} );

				$(document).ready(function() {

					$('#tblkinerja').dataTable( {
						"bProcessing": false,
						"bServerSide": true,
						"sAjaxSource": "datakinerja.php",
						"order": [[ 0, "desc" ]],
						"scrollCollapse": true,
		
						"columnDefs": [{
							"targets": [14], 
							"className": "text-center"},
							{"width": "10%", "targets": 14}],
							"aoColumns": [
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							null,
							{
								"mData": "14", <!-- Ini adalah untuk Link ID urutan kolom seperti table mulai dari 0 untuk data pertama -->
								"mRender": function ( data, type, full ) {
									var user = "<?php echo $_SESSION['userid'] ?>";
									if(user==135 || user==143)
										{ return '<a class="btn btn-sm btn-success" href=index.php?page=edit_k&id='+data+'><i class="ace-icon fa fa-pencil bigger-130"></i></a> <a class="btn btn-sm btn-danger" href=index.php?page=delet_k&id='+data+'><i class="ace-icon fa fa-trash-o bigger-130"></i></a>';
								} else {
									return '<a class="btn btn-sm btn-success" index.php?page=edit_k&id='+data+'><i class="ace-icon fa fa-pencil bigger-130"></i></a>';
								}

							}
						}
						]
					} );
				} );

				$(document).ready (function () {
					$('.datetime').datetimepicker({
						format: 'YYYY-MM-DD',
					});
				})	

			</script>


			<script type="text/javascript">
				jQuery(function($) {
					$('#chosen-multiple-style .btn').on('click', function(e){
						var target = $(this).find('input[type=radio]');
						var which = parseInt(target.val());
						if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
						else $('#form-field-select-4').removeClass('tag-input-style');
					});
				});
			</script>

		</body>
		</html>
