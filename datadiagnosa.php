<?php
if($_SERVER['REQUEST_METHOD']=="GET"){
   require 'config.php';
   datadiagnosa($_GET['search']);
}

function datadiagnosa($search){
   global $conn1;

   if ($conn1->connect_error) {
       die("Koneksi Gagal: " . $conn->connect_error);
   }

   $sql = "SELECT diag.CODE, CONCAT(diag.CODE,' - ',diag.STR) NAMA
   FROM master.mrconso diag
   WHERE diag.CODE like '%$search%' or diag.STR like '%$search%' 
   GROUP BY diag.CODE
   ORDER BY STR ASC
   LIMIT 20";

   $result = $conn1->query($sql);

   if ($result->num_rows > 0) {
       $list = array();
       $key=0;
       while($row = $result->fetch_assoc()) {
           $list[$key]['id'] = $row['CODE'];
           $list[$key]['text'] = $row['NAMA']; 
           $key++;
       }
       echo json_encode($list);
   } else {
       echo "hasil kosong";
   }
   $conn1->close();
}

?>