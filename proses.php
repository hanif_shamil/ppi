<?php
$con=mysqli_connect('192.168.7.3','simrsdev','G0l0ks4kt1');
//$con=mysqli_connect('localhost','root',''); 

if(isset($_POST)){
	$nomr = $_POST['nomr'];
	$query = "SELECT ruan.DESKRIPSI RUANGAN ,ruan.ID ID_RUANGAN, ps.NORM,ps.NAMA, IF(ps.JENIS_KELAMIN=1,'52', '53') KODE_JENIS_KELAMIN, IF(ps.JENIS_KELAMIN=1,'Laki-Laki', 'Perempuan') JENIS_KELAMIN
	, ps.JENIS_KELAMIN ID_JK,DATE_FORMAT(ps.TANGGAL_LAHIR, '%d-%m-%Y') TANGGAL_LAHIR, p.TANGGAL TGL_MASUK
	, (YEAR(CURDATE())-YEAR(ps.TANGGAL_LAHIR)) UMUR, concat((YEAR(CURDATE())-YEAR(ps.TANGGAL_LAHIR)),' Th') UMUR_T
	, ref.DESKRIPSI PENJAMIN, ref.ID ID_PENJAMIN, IF(ref.ID=2,'56',IF(ref.ID=62,'57',IF(1,'54','55'))) PENANGGUNG
	, IF(ref.ID=2,'BPJS',IF(ref.ID=62,'Karyawan RSKD',IF(1,'Umum/Tanpa Asuransi','Jaminan Perusahaan'))) NAMA_PENANGGUNG
	, neg.DESKRIPSI KEWARGANEGARAAN, pek.DESKRIPSI PEKERJAAN, kp.NOMOR TELPON, ps.ALAMAT, kip.NOMOR KTP, p.NOMOR NOPEN
	, dia.ICD, dia.DIAGNOSA, CONCAT(dia.ICD,'-',dia.DIAGNOSA) DIAGMASUK, kk.NOMOR TELPKEL, kel.NAMA NAMAKEL
	, (SELECT COUNT(id)+1 FROM db_ppi.tb_postido WHERE nomr='$nomr') POSTID
	FROM pendaftaran.pendaftaran p
	LEFT JOIN pendaftaran.kunjungan pk ON p.NOMOR = pk.NOPEN
	LEFT JOIN master.ruangan ruan ON ruan.ID = pk.RUANGAN
	LEFT JOIN master.pasien ps ON ps.NORM = p.NORM
	LEFT JOIN pendaftaran.tujuan_pasien tp ON tp.NOPEN = p.NOMOR
	LEFT JOIN master.ruangan r ON r.ID = tp.RUANGAN	
	LEFT JOIN pendaftaran.penjamin pj ON pj.NOPEN = p.NOMOR
	LEFT JOIN master.referensi ref ON ref.ID = pj.JENIS AND ref.JENIS=10
	left JOIN master.negara neg ON neg.ID=ps.KEWARGANEGARAAN
	LEFT JOIN master.referensi pek ON pek.ID=ps.PEKERJAAN and pek.JENIS=4
	LEFT JOIN master.kontak_pasien kp ON kp.NORM=ps.NORM
	LEFT JOIN master.kartu_identitas_pasien kip ON kip.NORM=ps.NORM
	LEFT JOIN master.diagnosa_masuk dia ON dia.ID=p.DIAGNOSA_MASUK
	LEFT JOIN master.kontak_keluarga_pasien kk ON kk.NORM=p.NORM
	LEFT JOIN master.keluarga_pasien kel ON kel.NORM=p.NORM 
		WHERE p.NORM = '$nomr' AND p.`STATUS`!=0 #AND pk.`STATUS`=1 
		AND ruan.JENIS_KUNJUNGAN in (3)
		ORDER BY pk.MASUK desc limit 1";
		$sql   = mysqli_query ($con,$query);
		$data  = mysqli_fetch_assoc($sql);
		//echo $query; die();
		$row = array(
			'norm' 				=> $data['NORM'],
			'nama' 				=> $data['NAMA'],			
			'jk'   				=> $data['JENIS_KELAMIN'],
			'kode_jenis_kelamin'=> $data['KODE_JENIS_KELAMIN'],
			'id_jenis_kelamin' 	=> $data['ID_JK'],
			'ruangan'			=> $data['RUANGAN'],
			'id_ruangan'		=> $data['ID_RUANGAN'],
			'penjamin'			=> $data['PENJAMIN'],
			'id_penjamin'       => $data['ID_PENJAMIN'],
			'penanggung'        => $data['PENANGGUNG'],
			'nama_penanggung'   => $data['NAMA_PENANGGUNG'],
			'umur'          	=> $data['UMUR'],
			'umur_t'          	=> $data['UMUR_T'],
			'tgl_masuk'         => $data['TGL_MASUK'],
			'tgl_lahir'         => $data['TANGGAL_LAHIR'],
			'ktp'         		=> $data['KTP'],
			'warganegara'       => $data['KEWARGANEGARAAN'],
			'pekerjaan'         => $data['PEKERJAAN'],
			'telpon'         	=> $data['TELPON'],
			'alamat'         	=> $data['ALAMAT'],
			'nopen'         	=> $data['NOPEN'],
			'icd'         		=> $data['ICD'],
			'diagnosa'         	=> $data['DIAGNOSA'],
			'diagmasuk'         => $data['DIAGMASUK'],
			'tglusia'         	=> $data['TANGGAL_LAHIR'].' / '.$data['UMUR'].'Thn',
			'telpkel'         	=> $data['TELPKEL'],
			'namakel'         	=> $data['NAMAKEL'],
			'postid'         	=> $data['POSTID'],
		);
		echo json_encode($row); 
		//echo "<pre>";print_r($nomr);exit();
	}
?>