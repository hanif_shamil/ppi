<?php

// session_start();
// echo $_SESSION['username'];
//exit();
// if(!isset($_SESSION['username'])){ // Jika tidak ada session username berarti dia belum login
  // header("location:login.php"); // Kita Redirect ke halaman index.php karena belum login
// }
//data kolom yang akan di tampilkan
$aColumns = array('TANGGAL','NM_RUANGAN','TOPI','APRON','GAUN','SARUNGTANGAN','MASKER','SEPATU','KACAMATA','HAZMART','PERSEN','ID');

//primary key
$sIndexColumn = "ID";

//nama table database
$sTable = "(SELECT th.ID, th.TANGGAL
, if(th.TOPI=1,'Ya',if(th.TOPI=2,'Tidak','NA')) TOPI
, if(th.APRON=1,'Ya',if(th.APRON=2,'Tidak','NA')) APRON 
, if(th.GAUN=1,'Ya',if(th.GAUN=2,'Tidak','NA')) GAUN 
, if(th.SARUNGTANGAN=1,'Ya',if(th.SARUNGTANGAN=2,'Tidak','NA')) SARUNGTANGAN
, if(th.MASKER=1,'Ya',if(th.MASKER=2,'Tidak','NA')) MASKER
, if(th.SEPATU=1,'Ya',if(th.SEPATU=2,'Tidak','NA')) SEPATU
, if(th.KACAMATA=1,'Ya',if(th.KACAMATA=2,'Tidak','NA')) KACAMATA
, if(th.HAZMART=1,'Ya',if(th.HAZMART=2,'Tidak','NA')) HAZMART
, concat(th.NILAI,' ','%') PERSEN, ru.DESKRIPSI NM_RUANGAN
, r.DESKRIPSI NM_PETUGAS
FROM db_ppi.tb_apdnew th
left join referensi r ON r.ID=th.PETUGAS and r.JENIS=4
left join ruangan ru ON ru.ID=th.RUANGAN and ru.JENIS=5 and ru.JENIS_KUNJUNGAN !=0
where th.STATUS=1 ORDER BY th.TANGGAL DESC)ma";

//informasi koneksi ke database
include 'config.php';


$sLimit = "";
if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
{
$sLimit = "LIMIT ".mysqli_real_escape_string( $conn1,$_GET['iDisplayStart'] ).", ".
mysqli_real_escape_string( $conn1,$_GET['iDisplayLength'] );
}

if ( isset( $_GET['iSortCol_0'] ) )
{
$sOrder = "ORDER BY  ";
for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
{
if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
{
$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
".mysqli_real_escape_string( $conn1,$_GET['sSortDir_'.$i] ) .", ";
}
}

$sOrder = substr_replace( $sOrder, "", -2 );
if ( $sOrder == "ORDER BY" )
{
$sOrder = "";
}
}

$sWhere = "";
if ( $_GET['sSearch'] != "" )
{
$sWhere = "WHERE (";
for ( $i=0 ; $i<count($aColumns) ; $i++ )
{
$sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($conn1,$_GET['sSearch'] )."%' OR ";
}
$sWhere = substr_replace( $sWhere, "", -3 );
$sWhere .= ')';
}

for ( $i=0 ; $i<count($aColumns) ; $i++ )
{
if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
{
if ( $sWhere == "" )
{
$sWhere = "WHERE ";
}
else
{
$sWhere .= " AND ";
}
$sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($_GET['sSearch_'.$i])."%' ";
}
}

$sQuery = "
SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
FROM   $sTable
$sWhere
$sOrder
$sLimit
";
$rResult = mysqli_query( $conn1,$sQuery ) or die(mysqli_error());

$sQuery = "
SELECT FOUND_ROWS()
";
$rResultFilterTotal = mysqli_query( $conn1,$sQuery) or die(mysql_error());
$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];

$sQuery = "
SELECT COUNT(".$sIndexColumn.")
FROM   $sTable
";
$rResultTotal = mysqli_query( $conn1,$sQuery) or die(mysql_error());
$aResultTotal = mysqli_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];

$output = array(
"sEcho" => intval($_GET['sEcho']),
"iTotalRecords" => $iTotal,
"iTotalDisplayRecords" => $iFilteredTotal,
"aaData" => array()
);

while ( $aRow = mysqli_fetch_array( $rResult ) )
{
$row = array();
for ( $i=0 ; $i<count($aColumns) ; $i++ )
{
if ( $aColumns[$i] == "version" )
{
$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
}
else if ( $aColumns[$i] != ' ' )
{
$row[] = $aRow[ $aColumns[$i] ];
}
}
$output['aaData'][] = $row;
}

echo json_encode( $output );
?>