<div id="sidebar" class="sidebar responsive ace-save-state">
	<script type="text/javascript">
		try{ace.settings.loadState('sidebar')}catch(e){}
	</script>
	<ul class="nav nav-list">
		<li class="active">
			<a href="index.php?page=dashboard">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text"> Dashboard </span>
			</a>
			<b class="arrow"></b>
		</li>

		<?php if($_SESSION['userid']!=133){ ?>

			<li class="">
				<a href="#" class="dropdown-toggle">
					<i class="menu-icon fa fa-pencil-square-o"></i>
					<span class="menu-text"> Formulir </span>

					<b class="arrow fa fa-angle-down"></b>
				</a>
				<b class="arrow"></b>
				<ul class="submenu">
					<li class="">
						<a href="index.php?page=transcovid">
							<i class="menu-icon fa fa-caret-right"></i>
							Transfer Pasien COVID
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=harian">
							<i class="menu-icon fa fa-caret-right"></i>
							Surveilans Harian
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=hand">
							<i class="menu-icon fa fa-caret-right"></i>
							Form monitoring Hand Hygiene
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=kinerja">
							<i class="menu-icon fa fa-caret-right"></i>
							Indikator kinerja
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=iadp_insersi">
							<i class="menu-icon fa fa-caret-right"></i>
							Form bundle IADP : INSERSI
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=iadp_mtnc">
							<i class="menu-icon fa fa-caret-right"></i>
							Form bundle IADP : Maintenance
						</a>
						<b class="arrow"></b>
					</li>
			<!-- 		<li class="">
						<a href="index.php?page=iadp">
							<i class="menu-icon fa fa-caret-right"></i>
							Form bundle IADP
						</a>
						<b class="arrow"></b>
					</li> -->
					<li class="">
						<a href="index.php?page=isk_new">
							<i class="menu-icon fa fa-caret-right"></i>
							Form bundle ISK
						</a>
						<b class="arrow"></b>
					</li>
					<!-- <li class="">
						<a href="index.php?page=isk">
							<i class="menu-icon fa fa-caret-right"></i>
							Form bundle ISK
						</a>
						<b class="arrow"></b>
					</li> -->
					<li class="">
						<a href="index.php?page=vap">
							<i class="menu-icon fa fa-caret-right"></i>
							Form bundle VAP
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=ido_new">
							<i class="menu-icon fa fa-caret-right"></i>
							Form bundle IDO
						</a>
						<b class="arrow"></b>
					</li>
					<!-- <li class="">
						<a href="index.php?page=ido">
							<i class="menu-icon fa fa-caret-right"></i>
							Form bundle IDO
						</a>
						<b class="arrow"></b>
					</li> -->
					<li class="">
						<a href="index.php?page=tpia">
							<i class="menu-icon fa fa-caret-right"></i>
							Monitoring Transfer Pasien Infeksi Airborne
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=renov">
							<i class="menu-icon fa fa-caret-right"></i>
							Pemantauan Selama Renov/Konstruksi Bangunan Kelas III
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=renoviv">
							<i class="menu-icon fa fa-caret-right"></i>
							Pemantauan Selama Renov/Konstruksi Bangunan Kelas IV
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=bmp">
							<i class="menu-icon fa fa-caret-right"></i>
							Kepatuhan Bundles BMP
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=afkt">
							<i class="menu-icon fa fa-caret-right"></i>
							Fasilitas Kebersihan Tangan
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=limbah_new">
							<i class="menu-icon fa fa-caret-right"></i>
							Kepatuhan Membuang Limbah
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=apdnew">
							<i class="menu-icon fa fa-caret-right"></i>
							Kepatuhan Penggunaan APD
						</a>
						<b class="arrow"></b>
					</li>
					<!-- <li class="">
						<a href="index.php?page=aapd">
							<i class="menu-icon fa fa-caret-right"></i>
							Kepatuhan Penggunaan APD
						</a>
						<b class="arrow"></b>
					</li> -->
					<li class="">
						<a href="index.php?page=akj_new">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit Kamar Jenazah
						</a>
						<b class="arrow"></b>
					</li>
					<!-- <li class="">
						<a href="index.php?page=akj">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit Kamar Jenazah
						</a>
						<b class="arrow"></b>
					</li> -->
					<li class="">
						<a href="index.php?page=cssd_new">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit CSSD
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=londri_new">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit Laundry
						</a>
						<b class="arrow"></b>
					</li>
					<!-- <li class="">
						<a href="index.php?page=cssd">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit CSSD
						</a>
						<b class="arrow"></b>
					</li> -->
					<li class="">
						<a href="index.php?page=gizi_new">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit Gizi
						</a>
						<b class="arrow"></b>
					</li>
					<!-- <li class="">
						<a href="index.php?page=gizi">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit Gizi
						</a>
						<b class="arrow"></b>
					</li> -->
	<!-- 				<li class="">
						<a href="index.php?page=appi">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit PPI
						</a>
						<b class="arrow"></b>
					</li> -->
					<li class="">
						<a href="index.php?page=prako">
							<i class="menu-icon fa fa-caret-right"></i>
							Cheklist Pra konstruksi
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=posko">
							<i class="menu-icon fa fa-caret-right"></i>
							Cheklist Post konstruksi
						</a>
						<b class="arrow"></b>
					</li>
					<!-- <li class="">
						<a href="index.php?page=apkb">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit PPI di Kamar Bedah
						</a>
						<b class="arrow"></b>
					</li> -->
					<li class="">
						<a href="index.php?page=aprp">
							<i class="menu-icon fa fa-caret-right"></i>
							Audit PPI di Ruang Perawatan
						</a>
						<b class="arrow"></b>
					</li> 
					<li class="">
						<a href="index.php?page=ikpi">
							<i class="menu-icon fa fa-caret-right"></i>
							Izin Konstruksi Pengendalian Infeksi
						</a>
						<b class="arrow"></b>
					</li>
					<!-- <li class="">
						<a href="index.php?page=londri">
							<i class="menu-icon fa fa-caret-right"></i>
							Formulir Audit Laundry
						</a>
						<b class="arrow"></b>
					</li> -->
					<li class="">
						<a href="index.php?page=form_lhand">
							<i class="menu-icon fa fa-caret-right"></i>
							Formulir 6 Langkah Kebersihan Tangan
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=form_isopo">
							<i class="menu-icon fa fa-caret-right"></i>
							Monitoring RIIM (Isolasi Tekanan Positif)
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=form_isone">
							<i class="menu-icon fa fa-caret-right"></i>
							Monitoring Ruang Isolasi Tekanan Negatif
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=form_sitostatika">
							<i class="menu-icon fa fa-caret-right"></i>
							Monitoring Tindakan Intra Tekal Pemberian Obat Sitostika
						</a>
						<b class="arrow"></b>
					</li>
				<!-- sini -->
					<li class="">
						<a href="index.php?page=form_hap">
							<i class="menu-icon fa fa-caret-right"></i>
							Formulir Audit Kepatuhan Bundles HAP
						</a>
						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="index.php?page=form_plebitis">
							<i class="menu-icon fa fa-caret-right"></i>
							Formulir Audit Kepatuhan Bundles PHLEBITIS
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=lumbal">
							<i class="menu-icon fa fa-caret-right"></i>
							Formulir Audit Bundle BMP (Tindakan Lumbal Fungsi)
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=ambulan">
							<i class="menu-icon fa fa-caret-right"></i>
							Formulir Audit Pembersihan Ambulance
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=form_survido">
							<i class="menu-icon fa fa-caret-right"></i>
							Formulir Survailans Pre-Operatif IDO
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=form_postido">
							<i class="menu-icon fa fa-caret-right"></i>
							Formulir Survailans Post-Operatif IDO
						</a>
						<b class="arrow"></b>
					</li>
				</ul>
			</li>
			<li class="">
				<a href="#" class="dropdown-toggle">
					<i class="menu-icon fa fa-table"></i>
					<span class="menu-text"> Tabel </span>
					<b class="arrow fa fa-angle-down"></b>
				</a>
				<b class="arrow"></b>
				<ul class="submenu">
					<li class="">
						<a href="index.php?page=tabel_transcovid">
							<i class="menu-icon fa fa-table"></i>				
							Transfer Pasien Covid					
						</a>
						<b class="arrow"></b>
					</li>
					<li class="">
						<a href="index.php?page=tabell">
							<i class="menu-icon fa fa-table"></i>				
							Tabel Harian					
						</a>
						<b class="arrow"></b>
					</li>
				<!--<li class="">
					<a href="index.php?page=tabel_b">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Bulanan					
					</a>
					<b class="arrow"></b>
				</li>-->
				<li class="">
					<a href="index.php?page=tabel_hd">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Handy Hygiene					
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_k">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Indikiator kinerja					
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_iadp_insersi">
						<i class="menu-icon fa fa-table"></i>				
						Tabel IADP : INSERSI					
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_iadp_mtnc">
						<i class="menu-icon fa fa-table"></i>				
						Tabel IADP : Maintenance					
					</a>
					<b class="arrow"></b>
				</li>
				<!-- <li class="">
					<a href="index.php?page=tabel_iadp">
						<i class="menu-icon fa fa-table"></i>				
						Tabel IADP					
					</a>
					<b class="arrow"></b>
				</li> -->
				<li class="">
					<a href="index.php?page=tabel_isk_new">
						<i class="menu-icon fa fa-table"></i>				
						Tabel ISK					
					</a>
					<b class="arrow"></b>
				</li>
				<!-- <li class="">
					<a href="index.php?page=tabel_isk">
						<i class="menu-icon fa fa-table"></i>				
						Tabel ISK					
					</a>
					<b class="arrow"></b>
				</li> -->
				<li class="">
					<a href="index.php?page=tabel_vap">
						<i class="menu-icon fa fa-table"></i>				
						Tabel VAP					
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_ido_new">
						<i class="menu-icon fa fa-table"></i>				
						Tabel IDO					
					</a>
					<b class="arrow"></b>
				</li>
				<!-- <li class="">
					<a href="index.php?page=tabel_ido">
						<i class="menu-icon fa fa-table"></i>				
						Tabel IDO					
					</a>
					<b class="arrow"></b>
				</li> -->
				<li class="">
					<a href="index.php?page=tabel_bmp">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Bundles BMP 						
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_afkt">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Fasilitas Kebersihan Tangan						
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_renov">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Pemantauan Selama Renov KELAS III				
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_renoviv">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Pemantauan Selama Renov KELAS IV				
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_limbah_new">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Kepatuhan Membuang Limbah				
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_apdnew">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Kepatuhan Penggunaan APD				
					</a>
					<b class="arrow"></b>
				</li>
				<!-- <li class="">
					<a href="index.php?page=tabel_aapd">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Kepatuhan Penggunaan APD				
					</a>
					<b class="arrow"></b>
				</li> -->
				<li class="">
					<a href="index.php?page=tabel_tpia">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Monitoring Transfer Pasien Infeksi Airborne				
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_appi">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Pencegahan & Pengendalian Infeksi 			
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_gizi_new">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit GIZI			
					</a>
					<b class="arrow"></b>
				</li>				
				<!-- <li class="">
					<a href="index.php?page=tabel_gizi">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit GIZI			
					</a>
					<b class="arrow"></b>
				</li> -->
				<li class="">
					<a href="index.php?page=tabel_prako">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Cheklist Pra-Konstruksi		
					</a>
					<b class="arrow"></b>
				</li>	
				<li class="">
					<a href="index.php?page=tabel_posko">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Cheklist Post-Konstruksi		
					</a>
					<b class="arrow"></b>
				</li>	
				<li class="">
					<a href="index.php?page=tabel_appi">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit PPI		
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_cssd_new">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit CSSD	
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_londri_new">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Laundry
					</a>
					<b class="arrow"></b>
				</li>
				<!-- <li class="">
					<a href="index.php?page=tabel_cssd">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit CSSD		
					</a>
					<b class="arrow"></b>
				</li> -->
				<li class="">
					<a href="index.php?page=tabel_akj_new">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Kamar Jenazah	
					</a>
					<b class="arrow"></b>
				</li>
				<!-- <li class="">
					<a href="index.php?page=tabel_akj">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Kamar Jenazah		
					</a>
					<b class="arrow"></b>
				</li> -->	
				<li class="">
					<a href="index.php?page=tabel_apkb">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Monitoring/Audit PPI di Kamar Bedah		
					</a>
					<b class="arrow"></b>
				</li>
				<!-- <li class="">
					<a href="index.php?page=tabel_londri">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Laundry
					</a>
					<b class="arrow"></b>
				</li> -->
				<li class="">
					<a href="index.php?page=tabel_ikpi">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Izin Konstruksi PI
					</a>
					<b class="arrow"></b>
				</li>	
				<li class="">
					<a href="index.php?page=tabel_aprp">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit PPI di Ruang Perawatan
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_lhand">
						<i class="menu-icon fa fa-table"></i>				
						Tabel 6 Langkah Kebersihan Tangan
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_isopo">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Monitoring RIIM (Isolasi Positif)
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_isone">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Monitoring Ruang Isolasi Negatif
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_sitostatika">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Monitoring Tindakan Intra Tekal Pemberian Obat Sitostika
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_hap">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Formulir Audit Kepatuhan Bundles HAP
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_plebitis">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Formulir Audit Kepatuhan Bundles PHLEBITIS
					</a>
					<b class="arrow"></b>
				</li>	
				<li class="">
					<a href="index.php?page=tabel_lumbal">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Bundle BMP (Tindakan Lumbal Fungsi)			
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabel_ambulan">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Audit Pembersihan Ambulance
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=tabelsurvido">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Survielans Pre-Operatif IDO
					</a>
					<b class="arrow"></b>
				</li>	
				<li class="">
					<a href="index.php?page=tabelpostido">
						<i class="menu-icon fa fa-table"></i>				
						Tabel Survielans Post-Operatif IDO
					</a>
					<b class="arrow"></b>
				</li>				
			</ul>						
		</li>
		<!--<li class="">
			<a href="index.php?page=laporan">
				<i class="menu-icon fa fa-bar-chart-o"></i>				
				Pemantauan Selama Renov/Konstruksi Bangunan				
			</a>
			<b class="arrow"></b>
		</li>-->
		<li class="">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-bar-chart-o"></i>				
				Laporan
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="">
					<a href="index.php?page=grafik_bundle">
						<i class="menu-icon fa fa-caret-right"></i>
						Grafik Rata-rata Bundle
					</a>
					<b class="arrow"></b>
				</li>
				<!-- <li class="">
					<a href="index.php?page=lap_bundle">
						<i class="menu-icon fa fa-caret-right"></i>
						Per Bundles
					</a>
					<b class="arrow"></b>
				</li> -->
				<li class="">
					<a href="index.php?page=grafik_handy">
						<i class="menu-icon fa fa-caret-right"></i>
						Grafik Hand Hygine
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=grafik_cssd">
						<i class="menu-icon fa fa-caret-right"></i>
						Grafik Audit CSSD
					</a>
					<b class="arrow"></b>
				</li>	
				<li class="">
					<a href="index.php?page=grafik_kamarjnz">
						<i class="menu-icon fa fa-caret-right"></i>
						Grafik Audit Kamar Jenazah
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=grafik_laundry">
						<i class="menu-icon fa fa-caret-right"></i>
						Grafik Audit Laundry
					</a>
					<b class="arrow"></b>
				</li>
				<li class="">
					<a href="index.php?page=grafik_gizi">
						<i class="menu-icon fa fa-caret-right"></i>
						Grafik Audit Gizi
					</a>
					<b class="arrow"></b>
				</li>					
				<!--<li class="">
					<a href="index.php?page=lap_hand">
						<i class="menu-icon fa fa-caret-right"></i>
						Hand Hygiene
					</a>
					<b class="arrow"></b>
				</li>-->
				
				<!--<li class="">
					<a href="index.php?page=lap_antibiotik">
						<i class="menu-icon fa fa-caret-right"></i>
						Antibiotik
					</a>
					<b class="arrow"></b>
				</li>-->			
			</ul>
		</li>
		<li class="">
			<a href="index.php?page=form_cetak">
				<i class="menu-icon fa fa-folder-open"></i>				
				Cetakan				
			</a>
			<b class="arrow"></b>
		</li>
		<li class="">
			<a href="index.php?page=master">
				<i class="menu-icon fa fa-list"></i>
				<span class="menu-text"> Master </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li class="">
			<a href="index.php?page=listcovid">
				<i class="menu-icon fa fa-cogs" style="color:red"></i>
				<span class="menu-text" style="color:red"> Covid-19 </span>
			</a>
			<b class="arrow"></b>
		</li>

	<?php } else { ?>
		<li class="">
			<a href="index.php?page=form_cetak">
				<i class="menu-icon fa fa-folder-open"></i>				
				Cetakan				
			</a>
			<b class="arrow"></b>
		</li>
		<li class="">
			<a href="index.php?page=listcovid">
				<i class="menu-icon fa fa-cogs" style="color:red"></i>
				<span class="menu-text" style="color:red"> Covid-19 </span>
			</a>
			<b class="arrow"></b>
		</li>
	<?php } ?>
	</ul><!-- /.nav-list -->

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-save-state ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>