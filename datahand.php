<?php

// session_start();
// echo $_SESSION['username'];
//exit();
// if(!isset($_SESSION['username'])){ // Jika tidak ada session username berarti dia belum login
  // header("location:login.php"); // Kita Redirect ke halaman index.php karena belum login
// }
//data kolom yang akan di tampilkan
$aColumns = array('TANGGAL','NM_RUANGAN','NM_PETUGAS','SBLM_KNTKPS','STLH_KNTKPS','SBLM_TNDK','STLH_LNGK','STLH_KNTKTBH','PERSEN','ID_HAND');

//primary key
$sIndexColumn = "ID_HAND";

//nama table database
$sTable = "(SELECT th.ID_HAND, th.TANGGAL,if(th.SBLM_KNTKPS=1,'Ya',if(th.SBLM_KNTKPS=2,'Tidak','NA')) SBLM_KNTKPS, if(th.STLH_KNTKPS=1,'Ya',if(th.STLH_KNTKPS=2,'Tidak','NA')) STLH_KNTKPS 
,if(th.SBLM_TNDK=1,'Ya',if(th.SBLM_TNDK=2,'Tidak','NA')) SBLM_TNDK, if(th.STLH_LNGK=1,'Ya',if(th.STLH_LNGK=2,'Tidak','NA')) STLH_LNGK
, if(th.STLH_KNTKTBH=1,'Ya',if(th.STLH_KNTKTBH=2,'Tidak','NA')) STLH_KNTKTBH,concat(th.NILAI,' ','%') PERSEN, ru.DESKRIPSI NM_RUANGAN
, r.DESKRIPSI NM_PETUGAS, rh.DESKRIPSI NM_HAND_HYGINE, rs.DESKRIPSI NM_SARUNG_TNGN
FROM tb_hand th
left join referensi r ON r.ID=th.PETUGAS and r.JENIS=4
left join ruangan ru ON ru.ID=th.RUANGAN and ru.JENIS=5 and ru.JENIS_KUNJUNGAN !=0
left join referensi rh ON rh.ID=th.HAND_HYGIENE and rh.JENIS=5
left join referensi rs ON rs.ID=th.SARUNG_TANGAN and rs.JENIS=6
where th.STATUS=1 ORDER BY th.TANGGAL DESC)ma";

//informasi koneksi ke database
include 'config.php';


$sLimit = "";
if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
{
$sLimit = "LIMIT ".mysqli_real_escape_string( $conn1,$_GET['iDisplayStart'] ).", ".
mysqli_real_escape_string( $conn1,$_GET['iDisplayLength'] );
}

if ( isset( $_GET['iSortCol_0'] ) )
{
$sOrder = "ORDER BY  ";
for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
{
if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
{
$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
".mysqli_real_escape_string( $conn1,$_GET['sSortDir_'.$i] ) .", ";
}
}

$sOrder = substr_replace( $sOrder, "", -2 );
if ( $sOrder == "ORDER BY" )
{
$sOrder = "";
}
}

$sWhere = "";
if ( $_GET['sSearch'] != "" )
{
$sWhere = "WHERE (";
for ( $i=0 ; $i<count($aColumns) ; $i++ )
{
$sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($conn1,$_GET['sSearch'] )."%' OR ";
}
$sWhere = substr_replace( $sWhere, "", -3 );
$sWhere .= ')';
}

for ( $i=0 ; $i<count($aColumns) ; $i++ )
{
if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
{
if ( $sWhere == "" )
{
$sWhere = "WHERE ";
}
else
{
$sWhere .= " AND ";
}
$sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($_GET['sSearch_'.$i])."%' ";
}
}

$sQuery = "
SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
FROM   $sTable
$sWhere
$sOrder
$sLimit
";
$rResult = mysqli_query( $conn1,$sQuery ) or die(mysqli_error());

$sQuery = "
SELECT FOUND_ROWS()
";
$rResultFilterTotal = mysqli_query( $conn1,$sQuery) or die(mysql_error());
$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];

$sQuery = "
SELECT COUNT(".$sIndexColumn.")
FROM   $sTable
";
$rResultTotal = mysqli_query( $conn1,$sQuery) or die(mysql_error());
$aResultTotal = mysqli_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];

$output = array(
"sEcho" => intval($_GET['sEcho']),
"iTotalRecords" => $iTotal,
"iTotalDisplayRecords" => $iFilteredTotal,
"aaData" => array()
);

while ( $aRow = mysqli_fetch_array( $rResult ) )
{
$row = array();
for ( $i=0 ; $i<count($aColumns) ; $i++ )
{
if ( $aColumns[$i] == "version" )
{
$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
}
else if ( $aColumns[$i] != ' ' )
{
$row[] = $aRow[ $aColumns[$i] ];
}
}
$output['aaData'][] = $row;
}

echo json_encode( $output );
?>