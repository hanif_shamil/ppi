<script type="text/javascript">
function tugas1()
{
var jumlah=0;
var nilai;

if(document.getElementById("SBLM_KNTKPS").checked)
{
nilai=document.getElementById("SBLM_KNTKPS").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("SBLM_KNTKPSNN").checked)
{
nilai=document.getElementById("SBLM_KNTKPSNN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKPS").checked)
{
nilai=document.getElementById("STLH_KNTKPS").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKPSNN").checked)
{
nilai=document.getElementById("STLH_KNTKPSNN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("SBLM_TNDK").checked)
{
nilai=document.getElementById("SBLM_TNDK").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("SBLM_TNDKNN").checked)
{
nilai=document.getElementById("SBLM_TNDKNN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_LNGK").checked)
{
nilai=document.getElementById("STLH_LNGK").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_LNGKNN").checked)
{
nilai=document.getElementById("STLH_LNGKNN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKTBH").checked)
{
nilai=document.getElementById("STLH_KNTKTBH").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKTBHNN").checked)
{
nilai=document.getElementById("STLH_KNTKTBHNN").value;
jumlah=jumlah+parseInt(nilai);
}
document.getElementById("total").value=jumlah/5*100;
}
</script>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Monitoring Hand Hygiene</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Monitoring Hand Hygiene</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_hd" method="post">								
									<div class="form-group">										
										<div class="col-sm-3">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tanggal survey :</label>
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>	
										</div>
										<div class="col-sm-3">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Ruangan :</label>
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ Ruangan ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data[0]?>"><?=$data[3]?></option>
												<?php
												}
												?>	
											</select>
										</div>
										</div>
										<div class="col-sm-6">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Jenis petugas kesehatan :</label>
												<div class="checkbox">
													<label>
														<input name="PETUGAS[]" type="checkbox" value="11" class="ace" />
														<span class="lbl"> LAB</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="12" type="checkbox" class="ace" />
														<span class="lbl"> DR</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="13" class="ace" type="checkbox" />
														<span class="lbl"> PWT</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="14" class="ace" type="checkbox" />
														<span class="lbl"> POS</span>
													</label>
													<label>
														<input name="PETUGAS[]" type="checkbox" value="15" class="ace" />
														<span class="lbl"> TH</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="16" type="checkbox" class="ace" />
														<span class="lbl"> M. KEP</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="17" class="ace" type="checkbox" />
														<span class="lbl"> M. KED</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="18" class="ace" type="checkbox" />
														<span class="lbl"> DLL</span>
													</label>
												</div>									
											</div>
										</div>
									</div>
									<hr />									
									<div class="form-group">																																																			
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">5 Indikasi kebersihan tangan :</label>
												<table class="table table-bordered">
												<thead>
													<tr>
														<th width="5%" style="text-align:center">NO</th>
														<th style="text-align:center"> INDIKATOR</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">NA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Sebelum kontak ke pasien</td>
														<td align="center"><label>
															<input type="checkbox" name="SBLM_KNTKPS[]" id="SBLM_KNTKPS" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
															</label></td>
															<td align="center"><label>
															<input type="checkbox" name="SBLM_KNTKPSNN[]" id="SBLM_KNTKPSNN" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
															</label></td>
														<td align="center">
															<label>
															<input name="SBLM_KNTKPS[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Setelah kontak ke pasien</td>
														<td align="center"><label>
														<input type="checkbox" name="STLH_KNTKPS[]" id="STLH_KNTKPS" value="1" class="ace input-lg" onClick="tugas1()">													
													<span class="lbl"></span>
												</label></td>
												<td align="center"><label>
														<input type="checkbox" name="STLH_KNTKPSNN[]" id="STLH_KNTKPSNN" value="1" class="ace input-lg" onClick="tugas1()">													
													<span class="lbl"></span>
												</label></td>
														<td align="center"><label>
													<input name="STLH_KNTKPS[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Sebelum tindakan ASP & INV</td>
														<td align="center">
															<label>
															<input type="checkbox" name="SBLM_TNDK[]" id="SBLM_TNDK" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input type="checkbox" name="SBLM_TNDKNN[]" id="SBLM_TNDKNN" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="SBLM_TNDK[]" type="checkbox" value="2" id="1" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Setelah lingkungan</td>
														<td align="center">
															<label>
															<input name="STLH_LNGK[]" type="checkbox" value="1" id="STLH_LNGK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="STLH_LNGKNN[]" type="checkbox" value="1" id="STLH_LNGKNN" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="STLH_LNGK[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Setelah kontak Cairan Tubuh</td>
														<td align="center">
															<label>
															<input name="STLH_KNTKTBH[]" type="checkbox" value="1" id="STLH_KNTKTBH" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="STLH_KNTKTBHNN[]" type="checkbox" value="1" id="STLH_KNTKTBHNN" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="STLH_KNTKTBH[]" type="checkbox" value="2" id="1" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>									
													<tr>
														<td style="text-align:center" colspan="2">NILAI KEPATUHAN</td>
														<td colspan="3" style="text-align:center">
															<div class="input-group">
															<input type="text" id="total" name="NILAI" placeholder="[ NILAI ]" class="form-control" />
															<span class="input-group-addon">
																%
															</span>
															</div>													
														</td>														
													</tr>													
												</tbody>
											</table>																									
											</div>
										</div>
										
									</div>																										
									<hr />
									<div class="form-group">										
										<div class="col-xs-12 col-sm-3">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Hand Hygiene :</label>
												<div class="checkbox">
													<label>
														<input name="HAND_HYGIENE[]" value="19" type="checkbox" class="ace" />
														<span class="lbl"> ALK</span>
													</label>												
													<label>
														<input name="HAND_HYGIENE[]" value="20" type="checkbox" class="ace" />
														<span class="lbl"> CTDA</span>
													</label>
													<label>
														<input name="HAND_HYGIENE[]" value="21" type="checkbox" class="ace" />
														<span class="lbl"> TIDAK</span>
													</label>																									
												</div>															
											</div>										
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Sarung tangan :</label>
												<div class="checkbox">
													<label>
														<input name="SARUNG_TNGN[]" type="checkbox" value="22" class="ace" />
														<span class="lbl"> SEBELUM </span>
													</label>											
													<label>
														<input name="SARUNG_TNGN[]" value="23" type="checkbox" class="ace" />
														<span class="lbl"> SETELAH</span>
													</label>											
													<label>
														<input name="SARUNG_TNGN[]" value="24" class="ace" type="checkbox" />
														<span class="lbl"> TERUS MENERUS</span>
													</label>											
												</div>									
											</div>
										</div>
										<div class="col-sm-5">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
													<label class="control-label bolder blue" style="text-decoration: underline">Catatan :</label>
												<input type="text" id="form-field-1-1" name="CATATAN" placeholder="[ CATATAN ]" class="form-control" />
											</div>	
										</div>											
									</div>																																																							
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->