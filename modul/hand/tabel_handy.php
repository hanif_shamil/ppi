<?php
function serialize_ke_string($serial)
{
	$hasil = unserialize($serial);
	return implode(', ', $hasil);
}
?>
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>
		<li>
			<a href="#">Tables</a>
		</li>
		<li class="active">Laporan Handy Hygiene</li>
	</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Data Laporan Handy Hygiene</h4>
							<div class="widget-toolbar">
								<a href="index.php?page=hand">
									<i class="ace-icon fa fa-plus" style="color:blue"> Tambah data</i>
								</a>
							</div>
						</div>	
						<!-- div.table-responsive -->
						<!-- div.dataTables_borderWrap -->
						<div>
							<table id="tbhandd" class="table table-striped table-bordered table-hover" width="100%">
								<thead>
									<tr align="center">
										<th rowspan="2" width="10%"><div align="center">Tanggal</div></th>
										<th rowspan="2"><div align="center">Ruangan</div></th>
										<th rowspan="2" width="9%"><div align="center">Petugas</div></th>
										<th colspan="5"><div align="center">Indikasi kebersihan tangan</div></th>
										<th rowspan="2" width="7%"><div align="center">Nilai</div></th>				
										<th rowspan="2" width="8%"><div align="center">Opsi</div></th>
									</tr>
									<tr>
										<td align="center" width="9%">Sblm kntk psn</td>
										<td align="center" width="8%">Stlh kntk psn</td>
										<td align="center" width="13%">Sblm tndkan ASP & INV</td>
										<td align="center" width="8%">Stlh lingkngn</td>
										<td align="center" width="10%">Stlh kntk ca. tbh</td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->

