<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					if(isset($_POST['btnsimpan'])){	


						$TANGGAL 			= $_POST['TANGGAL'];		
						$RUANGAN 			= $_POST['RUANGAN'];								
						$AUDITEE 			= $_POST['AUDITEE'];
						$NILAI 				= $_POST['NILAI'];
						if(isset($_POST['PETUGAS'])){
							$PETUGAS = $_POST['PETUGAS'];
						}else{
							$PETUGAS = "";
						}						
						if(isset($_POST['SBLM_KNTKPS'])){
							$SBLM_KNTKPS = implode(',',$_POST['SBLM_KNTKPS']);
						}else{
							$SBLM_KNTKPS = "";
						}
						
						if(isset($_POST['STLH_KNTKPS'])){
							$STLH_KNTKPS = implode(',',$_POST['STLH_KNTKPS']);
						}else{
							$STLH_KNTKPS = "";
						}
						
						if(isset($_POST['SBLM_TNDK'])){
							$SBLM_TNDK = implode(',',$_POST['SBLM_TNDK']);
						}else{
							$SBLM_TNDK = "";
						}		

						if(isset($_POST['STLH_LNGK'])){
							$STLH_LNGK = implode(',',$_POST['STLH_LNGK']);
						}else{
							$STLH_LNGK = "";
						}
						
						if(isset($_POST['STLH_KNTKTBH'])){
							$STLH_KNTKTBH = implode(',',$_POST['STLH_KNTKTBH']);
						}else{
							$STLH_KNTKTBH = "";
						}
						
						if(isset($_POST['HAND_HYGIENE'])){
							$HAND_HYGIENE = implode(',',$_POST['HAND_HYGIENE']);
						}else{
							$HAND_HYGIENE = "";
						}	
						if(isset($_POST['SARUNG_TNGN'])){
							$SARUNG_TNGN = implode(',',$_POST['SARUNG_TNGN']);
						}else{
							$SARUNG_TNGN = "";
						}			
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 			=	addslashes($_POST['AUDITOR']); 
						$pengisi 			= $_SESSION['userid'];	

						// echo "<pre>";print_r($_POST);exit();  

						if((empty($_POST['RUANGAN']))){
							echo '<script>alert("NAMA RUANGAN BELUM DIPILIH!"); window.location = "index.php?page=hand"</script>';
						}
						else {

							$query2 = "INSERT INTO db_ppi.tb_hand (
								RUANGAN,
								TANGGAL,
								PETUGAS,
								SBLM_KNTKPS,
								STLH_KNTKPS,
								SBLM_TNDK,
								STLH_LNGK,
								STLH_KNTKTBH,
								HAND_HYGIENE,
								SARUNG_TANGAN,
								NILAI,
								AUDITEE,
								ANALISA,
								TINDAKLANJUT,
								KEPALA,
								AUDITOR,	
								PENGISI) 
							VALUES 
							('$RUANGAN',
								'$TANGGAL',
								'$PETUGAS',
								'$SBLM_KNTKPS',
								'$STLH_KNTKPS',
								'$SBLM_TNDK',
								'$STLH_LNGK',
								'$STLH_KNTKTBH',
								'$HAND_HYGIENE',
								'$SARUNG_TNGN',
								'$NILAI',
								'$AUDITEE',	
								'$ANALISA',  
								'$TINDAKLANJUT', 
								'$KEPALA', 
								'$AUDITOR',
								'$pengisi')";
						// echo $query2; die();	
							$insert=mysqli_query($conn1,$query2);									
							if($insert){
								echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_hd'</script>"; 
							}
						}						
					}                       
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			