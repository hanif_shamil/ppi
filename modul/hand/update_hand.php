<html>
<head>
	<script src="assets/sweetalert/dist/sweetalert-dev.js"></script>
	<link rel="stylesheet" href="assets/sweetalert/dist/sweetalert.css">

</head>
<body>

	<?php
	date_default_timezone_set('Asia/Jakarta');
	if(isset($_POST['btnEdit'])){
		$ID_HAND 			= $_POST['ID_HAND'];	
		$TANGGAL 			= $_POST['TANGGAL'];	
		$TANGGAL 			= $_POST['TANGGAL'];		
		$RUANGAN 			= $_POST['RUANGAN'];								
		$AUDITEE 			= $_POST['AUDITEE'];
		$NILAI 				= $_POST['NILAI'];
		if(isset($_POST['PETUGAS'])){
			$PETUGAS = $_POST['PETUGAS'];
		}else{
			$PETUGAS = "";
		}						
		if(isset($_POST['SBLM_KNTKPS'])){
			$SBLM_KNTKPS = implode(',',$_POST['SBLM_KNTKPS']);
		}else{
			$SBLM_KNTKPS = "";
		}					
		if(isset($_POST['STLH_KNTKPS'])){
			$STLH_KNTKPS = implode(',',$_POST['STLH_KNTKPS']);
		}else{
			$STLH_KNTKPS = "";
		}						
		if(isset($_POST['SBLM_TNDK'])){
			$SBLM_TNDK = implode(',',$_POST['SBLM_TNDK']);
		}else{
			$SBLM_TNDK = "";
		}														
		if(isset($_POST['STLH_LNGK'])){
			$STLH_LNGK = implode(',',$_POST['STLH_LNGK']);
		}else{
			$STLH_LNGK = "";
		}						
		if(isset($_POST['STLH_KNTKTBH'])){
			$STLH_KNTKTBH = implode(',',$_POST['STLH_KNTKTBH']);
		}else{
			$STLH_KNTKTBH = "";
		}					
		if(isset($_POST['HAND_HYGIENE'])){
			$HAND_HYGIENE = implode(',',$_POST['HAND_HYGIENE']);
		}else{
			$HAND_HYGIENE = "";
		}	
		if(isset($_POST['SARUNG_TNGN'])){
			$SARUNG_TNGN = implode(',',$_POST['SARUNG_TNGN']);
		}else{
			$SARUNG_TNGN = "";
		}
		$ANALISA 			=	$_POST['ANALISA'];  
		$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
		$KEPALA 			=	$_POST['KEPALA'];  
		$AUDITOR 			=	addslashes($_POST['AUDITOR']); 							
		$pengisi 			= $_SESSION['userid'];					
		$waktu = date("Y-m-d H:i:s");							
//echo "<pre>";print_r($_POST);exit();                											
		$query2 = "UPDATE db_ppi.tb_hand SET						
		RUANGAN 			='$RUANGAN',
		TANGGAL 			='$TANGGAL',
		PETUGAS 			='$PETUGAS',
		SBLM_KNTKPS 		='$SBLM_KNTKPS',						
		STLH_KNTKPS 		='$STLH_KNTKPS',						
		SBLM_TNDK 			='$SBLM_TNDK',						
		STLH_LNGK 			='$STLH_LNGK',						
		STLH_KNTKTBH 		='$STLH_KNTKTBH',						
		HAND_HYGIENE 		='$HAND_HYGIENE',
		SARUNG_TANGAN 		='$SARUNG_TNGN',
		NILAI 				='$NILAI',
		AUDITEE 			='$AUDITEE',
		ANALISA 			='$ANALISA', 
		TINDAKLANJUT 		='$TINDAKLANJUT', 
		KEPALA 				='$KEPALA', 
		AUDITOR 			='$AUDITOR',	
		TGL_UBAH 			='$waktu', 
		PENGUBAH 			='$pengisi'
		where ID_HAND='$ID_HAND'";
//echo $query2; die();	
		$insert=mysqli_query($conn1,$query2);									
		if($insert){
			echo '<script>
			swal({
				html: true,
				title: "Sukses",
				text: "Sukses Update Hand Hygine",
				type: "success",
				confirmButtonText: "Lihat Inputan",
				closeOnConfirm: false,
				closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href="index.php?page=tabel_hd";
					} 
					});
					</script>'; 
				}
			}
			if(isset($_POST['btnVerif'])){
				$id_isian 			= $_POST['ID_HAND'];									
//echo "<pre>";print_r($_POST);exit();                
				$query3 ="UPDATE db_ppi.tb_hand SET VERIF='2' where ID_HAND='$id_isian'";
				$verf=mysqli_query($conn1,$query3);
//echo $query3; die();									
				if($verf){
					echo '<script>
					swal({
						html: true,
						title: "Sukses",
						text: "Data sudah diverifikasi",
						type: "success",
						confirmButtonText: "Lihat Inputan",
						closeOnConfirm: false,
						closeOnCancel: false
						},
						function(isConfirm) {
							if (isConfirm) {
								window.location.href="index.php?page=tabel_hd";
							} });</script>'; 	
						}
					} 						
					
					?>
				</body>
				</html>	