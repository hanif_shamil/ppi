<?php
function serialize_ke_string($serial)
{
    $hasil = unserialize($serial);
    return implode(', ', $hasil);
}
?>

<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Tables</a>
		</li>
		<li class="active">Laporan Handy Hygiene</li>
	</ul><!-- /.breadcrumb -->

	<!--<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="ace-icon fa fa-search nav-search-icon"></i>
			</span>
		</form>
	</div><!-- /.nav-search -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="table-header">
						Laporan Handy Hygiene
					</div>

					<!-- div.table-responsive -->

					<!-- div.dataTables_borderWrap -->
					<div>
						<table id="tbhand" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr align="center">
									<th rowspan="2"><div align="center">Tanggal</div></th>
									<th rowspan="2"><div align="center">Ruangan</div></th>
									<th rowspan="2"><div align="center">Petugas</div></th>
									<th colspan="5"><div align="center">Indikasi kebersihan tangan</div></th>
									<th rowspan="2"><div align="center">Nilai</div></th>
									<th rowspan="2"><div align="center">Opsi</div></th>
								</tr>
								<tr>
								<td align="center" width="6%">Sblm kntk psn</td>
								<td align="center" width="6%">Stlh kntk psn</td>
								<td align="center" width="10%">Sblm tndkan ASP & INV</td>
								<td align="center" width="6%">Stlh lingkngn</td>
								<td align="center" width="6%">Stlh kntk ca. tbh</td>
								
								</tr>
							</thead>

							<tbody>
							<?php					
							$query="SELECT th.ID_HAND, th.TANGGAL,th.SBLM_KNTKPS, th.STLH_KNTKPS,th.SBLM_TNDK,th.STLH_LNGK,th.STLH_KNTKTBH, concat(th.NILAI,' ','%') PERSEN, ru.DESKRIPSI NM_RUANGAN, r.DESKRIPSI NM_PETUGAS, rh.DESKRIPSI NM_HAND_HYGINE, rs.DESKRIPSI NM_SARUNG_TNGN
							FROM tb_hand th
							left join referensi r ON r.ID=th.PETUGAS and r.JENIS=4
							left join ruangan ru ON ru.ID=th.RUANGAN and ru.JENIS=5 and ru.JENIS_KUNJUNGAN !=0
							left join referensi rh ON rh.ID=th.HAND_HYGIENE and rh.JENIS=5
							left join referensi rs ON rs.ID=th.SARUNG_TNGN and rs.JENIS=6
							where th.STATUS=1 ORDER BY th.TANGGAL DESC";								
							$info=mysqli_query($conn1,$query); 
							//untuk penomoran data
							//$no=1;						
							//menampilkan data
							while($row=mysqli_fetch_array($info)){
							?>
								<tr>
									<td><?php echo $row['TANGGAL'] ?></td>
									<td><?php echo $row['NM_RUANGAN'] ?></td>
									<td><?php echo $row['NM_PETUGAS'] ?></td>
									<td align="center">
										<?php if ($row['SBLM_KNTKPS']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>						
									</td>
									<td align="center">
										<?php if ($row['STLH_KNTKPS']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>						
									</td>
									<td align="center">
										<?php if ($row['SBLM_TNDK']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>						
									</td>
									<td align="center">
										<?php if ($row['STLH_LNGK']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>						
									</td>
									<td align="center">
										<?php if ($row['STLH_KNTKTBH']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>						
									</td>
									<td><?php echo $row['PERSEN'] ?></td>
																	
									<td align="center">
										<div class="hidden-sm hidden-xs action-buttons">
											<!--<a class="blue" href="?page=detail_hd&id=<?php echo $row['ID_HAND']?>">
												<i class="ace-icon fa fa-search-plus bigger-130"></i>												
											</a>-->
											<a class="green" href="?page=edit_hd&id=<?php echo $row['ID_HAND']?>">
												<i class="ace-icon fa fa-pencil bigger-130"></i>
											</a>
											<a class="red" href="?page=delet_hd&id=<?php echo $row['ID_HAND']?>">
												<i class="ace-icon fa fa-trash-o bigger-130"></i>
											</a>
										</div>
									</td>
								</tr>							
								<?php							
								}
								?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

