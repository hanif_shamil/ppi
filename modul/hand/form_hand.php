<script type="text/javascript">
	function tugas1()
	{
		var jumlah=0;
		var jumlahx=0;
//var nilaix;
var nilai;

if(document.getElementById("SBLM_KNTKPS").checked)
{
	nilai=document.getElementById("SBLM_KNTKPS").value;
	jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("SBLM_KNTKPSTDK").checked)
{
	nilai=document.getElementById("SBLM_KNTKPSTDK").value;
	jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("STLH_KNTKPS").checked)
{
	nilai=document.getElementById("STLH_KNTKPS").value;
	jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKPSTDK").checked)
{
	nilai=document.getElementById("STLH_KNTKPSTDK").value;
	jumlahx=jumlahx+parseInt(nilai)/2;
}

if(document.getElementById("SBLM_TNDK").checked)
{
	nilai=document.getElementById("SBLM_TNDK").value;
	jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("SBLM_TNDKTDK").checked)
{
	nilai=document.getElementById("SBLM_TNDKTDK").value;
	jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("STLH_LNGK").checked)
{
	nilai=document.getElementById("STLH_LNGK").value;
	jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_LNGKTDK").checked)
{
	nilai=document.getElementById("STLH_LNGKTDK").value;
	jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("STLH_KNTKTBH").checked)
{
	nilai=document.getElementById("STLH_KNTKTBH").value;
	jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKTBHTDK").checked)
{
	nilai=document.getElementById("STLH_KNTKTBHTDK").value;
	jumlahx=jumlahx+parseInt(nilai)/2;
}

document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
}
</script>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Monitoring Hand Hygiene</li>
			</ul><!-- /.breadcrumb -->


		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Monitoring Hand Hygiene</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_hd" method="post">								
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tanggal survey :</label>
												<div class="input-group">
													<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>	
										</div>
										<div class="col-sm-3">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Ruangan :</label>
												<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ Ruangan ]">
													<option value=""></option>
													<?php
													$sql = "select * from db_ppi.ruangan r
													where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0 and r.STATUS=1";
													$rs = mysqli_query($conn1,$sql);
													while ($data = mysqli_fetch_array($rs)) {
														?>
														<option value="<?=$data[0]?>"><?=$data[3]?></option>
														<?php
													}
													?>	
												</select>
											</div>
										</div>
										<div class="col-sm-7">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Jenis petugas kesehatan :</label>
												<div class="radio">
													<label>
														<input name="PETUGAS" type="radio" value="11" class="ace" />
														<span class="lbl"> LAB</span>
													</label>											
													<label>
														<input name="PETUGAS" value="12" type="radio" class="ace" />
														<span class="lbl"> DR</span>
													</label>											
													<label>
														<input name="PETUGAS" value="13" class="ace" type="radio" />
														<span class="lbl"> PWT</span>
													</label>											
													<label>
														<input name="PETUGAS" value="14" class="ace" type="radio" />
														<span class="lbl"> POS</span>
													</label>
													<label>
														<input name="PETUGAS" type="radio" value="15" class="ace" />
														<span class="lbl"> TH</span>
													</label>											
													<label>
														<input name="PETUGAS" value="16" type="radio" class="ace" />
														<span class="lbl"> M. KEP</span>
													</label>											
													<label>
														<input name="PETUGAS" value="17" class="ace" type="radio" />
														<span class="lbl"> M. KED</span>
													</label>											
													<label>
														<input name="PETUGAS" value="18" class="ace" type="radio" />
														<span class="lbl"> DLL</span>
													</label>
												</div>									
											</div>
										</div>
									</div>
									<hr />									
									<div class="form-group">																																																			
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">5 Indikasi kebersihan tangan :</label>
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> INDIKATOR</th>
															<th width="5%" style="text-align:center">YA</th>

															<th width="5%" style="text-align:center">TIDAK</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>Sebelum kontak ke pasien</td>
															<td align="center"><label>
																<input type="radio" name="SBLM_KNTKPS[]" id="SBLM_KNTKPS" value="1" class="ace input-lg" onClick="tugas1()">																																													
																<span class="lbl"></span>
															</label></td>

															<td align="center">
																<label>
																	<input name="SBLM_KNTKPS[]" type="radio" value="2" id="SBLM_KNTKPSTDK" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Setelah kontak ke pasien</td>
															<td align="center"><label>
																<input type="radio" name="STLH_KNTKPS[]" id="STLH_KNTKPS" value="1" class="ace input-lg" onClick="tugas1()">													
																<span class="lbl"></span>
															</label>
														</td>

														<td align="center"><label>
															<input type="radio" name="STLH_KNTKPS[]" id="STLH_KNTKPSTDK" value="2" class="ace input-lg" onClick="tugas1()">													
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Sebelum tindakan ASP & INV</td>
													<td align="center">
														<label>
															<input type="radio" name="SBLM_TNDK[]" id="SBLM_TNDK" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center">
														<label>
															<input type="radio" name="SBLM_TNDK[]" id="SBLM_TNDKTDK" value="2" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>4</td>
													<td>Setelah lingkungan</td>
													<td align="center">
														<label>
															<input name="STLH_LNGK[]" type="radio" value="1" id="STLH_LNGK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center">
														<label>
															<input name="STLH_LNGK[]" type="radio" value="2" id="STLH_LNGKTDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>5</td>
													<td>Setelah kontak Cairan Tubuh</td>
													<td align="center">
														<label>
															<input name="STLH_KNTKTBH[]" type="radio" value="1" id="STLH_KNTKTBH" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center">
														<label>
															<input name="STLH_KNTKTBH[]" type="radio" value="2" id="STLH_KNTKTBHTDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>									
												<tr>
													<td style="text-align:center" colspan="2">NILAI KEPATUHAN</td>
													<td colspan="3" style="text-align:center">
														<div class="input-group">
															<input type="text" id="total" name="NILAI" placeholder="[ NILAI ]" class="form-control" />
															<span class="input-group-addon">
																%
															</span>
														</div>													
													</td>														
												</tr>													
											</tbody>
										</table>																									
									</div>
								</div>

							</div>																										
							<hr />
							<div class="form-group">										
								<div class="col-xs-12 col-sm-3">							
									<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
										<label class="control-label bolder blue" style="text-decoration: underline">Hand Hygiene :</label>
										<div class="radio">
											<label>
												<input name="HAND_HYGIENE[]" value="19" type="radio" class="ace" />
												<span class="lbl"> ALK</span>
											</label>												
											<label>
												<input name="HAND_HYGIENE[]" value="20" type="radio" class="ace" />
												<span class="lbl"> CTDA</span>
											</label>
											<label>
												<input name="HAND_HYGIENE[]" value="21" type="radio" class="ace" />
												<span class="lbl"> TIDAK</span>
											</label>																									
										</div>															
									</div>										
								</div>
								<div class="col-xs-12 col-sm-4">
									<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
										<label class="control-label bolder blue" style="text-decoration: underline">Sarung tangan :</label>
										<div class="radio">
											<label>
												<input name="SARUNG_TNGN[]" type="radio" value="22" class="ace" />
												<span class="lbl"> SEBELUM </span>
											</label>											
											<label>
												<input name="SARUNG_TNGN[]" value="23" type="radio" class="ace" />
												<span class="lbl"> SETELAH</span>
											</label>											
											<label>
												<input name="SARUNG_TNGN[]" value="24" class="ace" type="radio" />
												<span class="lbl"> TERUS MENERUS</span>
											</label>											
										</div>									
									</div>
								</div>
								<div class="col-sm-5">
									<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
										<label class="control-label bolder blue" style="text-decoration: underline">Auditee :</label>
										<input type="text" id="form-field-1-1" name="AUDITEE" placeholder="[ AUDITEE ]" class="form-control" />
									</div>	
								</div>											
							</div>																			
							<hr />
							<div class="form-group">										
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
									<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
								</div>				
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
									<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
								</div>
							</div>
							<div class="form-group">			
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
									<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
								</div>	
								<div class="col-md-6 col-sm-12">	
									<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
									<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
								</div>								
							</div>																											
							<hr />
							<div class="form-group">							
								<div class="col-md-offset-3 col-md-9">
									<button class="btn btn-info" name="btnsimpan" type="submit">
										<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
										<i class="ace-icon fa fa-undo bigger-110"></i>
										Reset
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><!-- /.span -->
	</div>
</div><!-- /.page-content -->	
</div> <!-- container -->
		</div><!-- /.main-content -->