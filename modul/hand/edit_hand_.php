<script type="text/javascript">
function tugas1()
{
var jumlah=0;
var nilai;

if(document.getElementById("SBLM_KNTKPS").checked)
{
nilai=document.getElementById("SBLM_KNTKPS").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("SBLM_KNTKPSNN").checked)
{
nilai=document.getElementById("SBLM_KNTKPSNN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKPS").checked)
{
nilai=document.getElementById("STLH_KNTKPS").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKPSNN").checked)
{
nilai=document.getElementById("STLH_KNTKPSNN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("SBLM_TNDK").checked)
{
nilai=document.getElementById("SBLM_TNDK").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("SBLM_TNDKNN").checked)
{
nilai=document.getElementById("SBLM_TNDKNN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_LNGK").checked)
{
nilai=document.getElementById("STLH_LNGK").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_LNGKNN").checked)
{
nilai=document.getElementById("STLH_LNGKNN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKTBH").checked)
{
nilai=document.getElementById("STLH_KNTKTBH").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("STLH_KNTKTBHNN").checked)
{
nilai=document.getElementById("STLH_KNTKTBHNN").value;
jumlah=jumlah+parseInt(nilai);
}
document.getElementById("total").value=jumlah/5*100;
}
</script>
<?php
$id=($_GET['id']);
$query="SELECT *, r.DESKRIPSI NM_PETUGAS, rh.DESKRIPSI NM_HAND_HYGINE, rs.DESKRIPSI NM_SARUNG_TNGN
FROM tb_hand th
left join referensi r ON r.ID=th.PETUGAS and r.JENIS=4
left join ruangan ru ON ru.ID=th.RUANGAN and ru.JENIS=5 and ru.JENIS_KUNJUNGAN !=0
left join referensi rh ON rh.ID=th.HAND_HYGIENE and rh.JENIS=5
left join referensi rs ON rs.ID=th.SARUNG_TNGN and rs.JENIS=6
where ID_HAND='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$PETUGAS = explode(",", $dt['PETUGAS']);
$SBLM_KNTKPS = explode(",", $dt['SBLM_KNTKPS']);
$STLH_KNTKPS = explode(",", $dt['STLH_KNTKPS']);
$SBLM_TNDK = explode(",", $dt['SBLM_TNDK']);
$STLH_LNGK = explode(",", $dt['STLH_LNGK']);
$STLH_KNTKTBH = explode(",", $dt['STLH_KNTKTBH']);
$SBLM_KNTKPSNN = explode(",", $dt['SBLM_KNTKPSNN']);
$STLH_KNTKPSNN = explode(",", $dt['STLH_KNTKPSNN']);
$SBLM_TNDKNN = explode(",", $dt['SBLM_TNDKNN']);
$STLH_LNGKNN = explode(",", $dt['STLH_LNGKNN']);
$STLH_KNTKTBHNN = explode(",", $dt['STLH_KNTKTBHNN']);
$HAND_HYGIENE = explode(",", $dt['HAND_HYGIENE']);
$SARUNG_TNGN = explode(",", $dt['SARUNG_TNGN']);
{
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Monitoring Hand Hygiene</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Monitoring Hand Hygiene</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=update_hd" method="post">
								<input type="hidden" name="ID_HAND" class="form-control" value="<?php echo $id; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-3">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tanggal survey :</label>
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" placeholder="[ Tanggal survey ]" value="<?php echo $dt['TANGGAL']; ?>" name="TANGGAL" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>	
										</div>
										<div class="col-sm-3">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Ruangan :</label>
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ Ruangan ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
											
												<option <?php if($dt['RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
												<?php
												}
												?>	
											</select>
										</div>
										</div>
										<div class="col-sm-6">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Jenis petugas kesehatan :</label>
												<div class="checkbox">
													<label>
														<input name="PETUGAS[]" type="checkbox" value="11" <?php if(in_array("11", $PETUGAS)){ echo " checked=\"checked\""; } ?> class="ace" />
														<span class="lbl"> LAB</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="12" type="checkbox" <?php if(in_array("12", $PETUGAS)){ echo " checked=\"checked\""; } ?> class="ace" />
														<span class="lbl"> DR</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="13" class="ace" <?php if(in_array("13", $PETUGAS)){ echo " checked=\"checked\""; } ?> type="checkbox" />
														<span class="lbl"> PWT</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="14" class="ace" <?php if(in_array("14", $PETUGAS)){ echo " checked=\"checked\""; } ?> type="checkbox" />
														<span class="lbl"> POS</span>
													</label>
													<label>
														<input name="PETUGAS[]" type="checkbox" value="15" <?php if(in_array("15", $PETUGAS)){ echo " checked=\"checked\""; } ?> class="ace" />
														<span class="lbl"> TH</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="16" type="checkbox" <?php if(in_array("16", $PETUGAS)){ echo " checked=\"checked\""; } ?> class="ace" />
														<span class="lbl"> M.KEP</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="17" class="ace" <?php if(in_array("17", $PETUGAS)){ echo " checked=\"checked\""; } ?> type="checkbox" />
														<span class="lbl"> MKED</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="18" class="ace" <?php if(in_array("18", $PETUGAS)){ echo " checked=\"checked\""; } ?> type="checkbox" />
														<span class="lbl"> DLL</span>
													</label>
												</div>									
											</div>
										</div>
									</div>
									<hr />									
									<div class="form-group">																																																			
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">5 Indikasi kebersihan tangan :</label>
												<table class="table table-bordered">
												<thead>
													<tr>
														<th width="5%" style="text-align:center">NO</th>
														<th style="text-align:center"> INDIKATOR</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">NA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Sebelum kontak ke pasien</td>
														<td align="center"><label>
															<input type="checkbox" name="SBLM_KNTKPS[]" <?php if(in_array("1", $SBLM_KNTKPS)){ echo " checked=\"checked\""; } ?> id="SBLM_KNTKPS" value="1" class="ace input-lg" onClick="tugas1()">	
																									
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center"><label>
															<input type="checkbox" name="SBLM_KNTKPSNN[]" <?php if(in_array("1", $SBLM_KNTKPSNN)){ echo " checked=\"checked\""; } ?> id="SBLM_KNTKPSNN" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="SBLM_KNTKPS[]" type="checkbox" <?php if(in_array("2", $SBLM_KNTKPS)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Setelah kontak ke pasien</td>
														<td align="center"><label>
														<input type="checkbox" name="STLH_KNTKPS[]" <?php if(in_array("1", $STLH_KNTKPS)){ echo " checked=\"checked\""; } ?>  id="STLH_KNTKPS" value="1" class="ace input-lg" onClick="tugas1()">													
													<span class="lbl"></span>
												</label></td>
												<td align="center"><label>
														<input type="checkbox" name="STLH_KNTKPSNN[]" <?php if(in_array("1", $STLH_KNTKPSNN)){ echo " checked=\"checked\""; } ?> id="STLH_KNTKPSNN" value="1" class="ace input-lg" onClick="tugas1()">													
													<span class="lbl"></span>
												</label></td>
														<td align="center"><label>
													<input name="STLH_KNTKPS[]" type="checkbox" value="2" <?php if(in_array("2", $STLH_KNTKPS)){ echo " checked=\"checked\""; } ?> id="1" class="ace input-lg" onClick="hitung( this )" />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Sebelum tindakan ASP & INV</td>
														<td align="center">
															<label>
															<input type="checkbox" name="SBLM_TNDK[]" <?php if(in_array("1", $SBLM_TNDK)){ echo " checked=\"checked\""; } ?> id="SBLM_TNDK" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input type="checkbox" name="SBLM_TNDKNN[]" id="SBLM_TNDKNN" <?php if(in_array("1", $SBLM_TNDKNN)){ echo " checked=\"checked\""; } ?> value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="SBLM_TNDK[]" type="checkbox" value="2" <?php if(in_array("2", $SBLM_TNDK)){ echo " checked=\"checked\""; } ?> id="1" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Setelah lingkungan</td>
														<td align="center">
															<label>
															<input name="STLH_LNGK[]" <?php if(in_array("1", $STLH_LNGK)){ echo " checked=\"checked\""; } ?> type="checkbox" value="1" id="STLH_LNGK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="STLH_LNGKNN[]" type="checkbox" value="1" <?php if(in_array("1", $STLH_LNGKNN)){ echo " checked=\"checked\""; } ?> id="STLH_LNGKNN" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="STLH_LNGK[]" type="checkbox" value="2" id="1" <?php if(in_array("2", $STLH_LNGK)){ echo " checked=\"checked\""; } ?> class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Setelah kontak Cairan Tubuh</td>
														<td align="center">
															<label>
															<input name="STLH_KNTKTBH[]" <?php if(in_array("1", $STLH_KNTKTBH)){ echo " checked=\"checked\""; } ?> type="checkbox" value="1" id="STLH_KNTKTBH" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="STLH_KNTKTBHNN[]" <?php if(in_array("1", $STLH_KNTKTBHNN)){ echo " checked=\"checked\""; } ?> type="checkbox" value="1" id="STLH_KNTKTBHNN" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="STLH_KNTKTBH[]" type="checkbox" value="2" <?php if(in_array("2", $STLH_KNTKTBH)){ echo " checked=\"checked\""; } ?> id="1" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>									
													<tr>
														<td style="text-align:center" colspan="2">NILAI KEPATUHAN</td>
														<td colspan="3" style="text-align:center">
															<div class="input-group">
															<input type="text" id="total" name="NILAI" placeholder="[ NILAI ]" value="<?php echo $dt['NILAI']; ?>"class="form-control" />
															<span class="input-group-addon">
																%
															</span>
															</div>													
														</td>														
													</tr>													
												</tbody>
											</table>																									
											</div>
										</div>
						
									</div>																										
									<hr />
									<div class="form-group">										
										<div class="col-xs-12 col-sm-3">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Hand Hygiene :</label>
												<div class="checkbox">
													<label>
														<input name="HAND_HYGIENE[]" value="19" <?php if(in_array("19", $HAND_HYGIENE)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> ALK</span>
													</label>												
													<label>
														<input name="HAND_HYGIENE[]" value="20" <?php if(in_array("20", $HAND_HYGIENE)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> CTDA</span>
													</label>
													<label>
														<input name="HAND_HYGIENE[]" value="21" <?php if(in_array("21", $HAND_HYGIENE)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> TIDAK</span>
													</label>																									
												</div>															
											</div>										
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Sarung tangan :</label>
												<div class="checkbox">
													<label>
														<input name="SARUNG_TNGN[]" type="checkbox" <?php if(in_array("22", $SARUNG_TNGN)){ echo " checked=\"checked\""; } ?> value="22" class="ace" />
														<span class="lbl"> SEBELUM </span>
													</label>											
													<label>
														<input name="SARUNG_TNGN[]" value="23" type="checkbox" <?php if(in_array("23", $SARUNG_TNGN)){ echo " checked=\"checked\""; } ?> class="ace" />
														<span class="lbl"> SETEAH</span>
													</label>											
													<label>
														<input name="SARUNG_TNGN[]" value="24" class="ace" <?php if(in_array("24", $SARUNG_TNGN)){ echo " checked=\"checked\""; } ?> type="checkbox" />
														<span class="lbl"> TERUS MENERUS</span>
													</label>											
												</div>									
											</div>
										</div>
										<div class="col-sm-5">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
													<label class="control-label bolder blue" style="text-decoration: underline">Catatan :</label>
												<input type="text" id="form-field-1-1" name="CATATAN" value="<?php echo $dt['CATATAN']; ?>" placeholder="[ CATATAN ]" class="form-control" />
											</div>	
										</div>											
									</div>																																																							
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnEdit" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->
<?php }
?>