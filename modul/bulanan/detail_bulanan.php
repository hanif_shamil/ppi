<?php					
$id=($_GET['id']);
$query="select tdi.ID_DETAIL, tdi.ID_ISIAN, tdi.TGL_DETAIL, tdi.SURVEYOR, tdi.SUHU, tdi.`STATUS`,rk.DESKRIPSI KELAMIN, tdi.JENIS_KELAMIN
,ru.DESKRIPSI RUANGAN, tdi.RUANGAN ID_RUANGAN, tdi.DIVISI, tdi.NOMR,tdi.NAMA
,concat ((tdi.UMUR),' Tahun') UMUR_T, tdi.UMUR, tdi.DX_MEDIS, tdi.TINDAKAN, tdi.INFEKSI, tdi.TIRAH, tdi.KULTUR, tdi.ANTIBIOTIK
from tb_detail_isian tdi
left join referensi rk ON rk.ID=tdi.JENIS_KELAMIN and rk.JENIS=1
left join ruangan ru ON ru.ID=tdi.RUANGAN and ru.JENIS=5
left join ruangan dep ON dep.ID=tdi.DIVISI and dep.JENIS=5
where tdi.ID_ISIAN='$id'";
//die($query);
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$tindakan = explode(",", $dt['TINDAKAN']);
$infeksi = explode(",", $dt['INFEKSI']);
  {
	  ?>
 
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Harian</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Text Area</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="?page=update" method="post">
								<input type="hidden" name="id_isian" class="form-control" value="<?php echo $id; ?>" disabled>																		
									<div class="form-group">										
										<div class="col-sm-3">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TGL_DETAIL']; ?>"  name="tanggal" type="text" / disabled>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">
											<input class="form-control" id="datetimepicker1" value="<?php echo $dt['RUANGAN']; ?>"  name="tanggal" type="text" / disabled>
										</div>
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" name="surveyor" value="<?php echo $dt['SURVEYOR']; ?>" placeholder="[ Surveyor ]" class="form-control" / disabled>
										</div>
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" name="divisi" value="<?php echo $dt['DIVISI']; ?>" placeholder="[ Divisi ]" class="form-control" / disabled>
										</div>
									</div>									
									<hr />									
									<div class="form-group">																																		
										<div class="col-sm-2">
											<div class="input-group">
												<input type="text" id="form-field-1-1" value="<?php echo $dt['SUHU']; ?>" name="suhu" placeholder="[ Suhu ]" class="form-control" / disabled>
												<span class="input-group-addon">
												&deg;C
												</span>
											</div>
										</div>								
										<div class="col-sm-2">
											<div class="input-group">
												<input type="text" id="nomr" name="nomr" value="<?php echo $dt['NOMR']; ?>" placeholder="[ No.RM ]" class="form-control" onkeyup="autofill()" / disabled>
												<span class="input-group-addon">
													NOMR
												</span>
											</div>
										</div>							
										<div class="col-sm-3">
											<div class="input-group">
											<input type="text" id="nama" name="nama" value="<?php echo $dt['NAMA']; ?>" placeholder="[ Nama ]" class="form-control" disabled />
											<span class="input-group-addon">
												<i class="fa fa-user"></i>
												</span>
											</div>
										</div>														
										<div class="col-sm-2">
											<div class="input-group">
												<input type="" id="umur_t" value="<?php echo $dt['UMUR_T']; ?>" name="" placeholder="[ Usia ]" class="form-control" disabled>
												<input type="hidden" id="umur" name="umur" value="<?php echo $dt['UMUR']; ?>" class="form-control" readonly>
												<span class="input-group-addon">
													<i class="fa fa-users"></i>
												</span>
											</div>
										</div>															
										<div class="col-sm-3">
											<div class="input-group">
												<input name="" id="jenis_kelamin" value="<?php echo $dt['KELAMIN']; ?>" class="form-control" disabled> 
												<input type="hidden" name="id_jenis_kelamin" id="id_jenis_kelamin" value="<?php echo $dt['JENIS_KELAMIN'];?>" class="form-control">
												<span class="input-group-addon">
													Jenis Kelamin
												</span>
											</div>
										</div>
									</div>							
									<div class="form-group">									
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['DX_MEDIS']; ?>" name="dxmedis" placeholder=" [DX. MEDIS ]" class="form-control" /disabled>
										</div>																					
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['TIRAH']; ?>" name="tirah" placeholder=" [ Tirah Baring ]" class="form-control" / disabled>
										</div>																							
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['KULTUR']; ?>" name="kultur" placeholder="[ Hasil Kultur ]" class="form-control" / disabled>
										</div>																							
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['ANTIBIOTIK']; ?>" name="antibiotik" placeholder="[ Antibiotik ]" class="form-control" / disabled>
										</div>
									</div>															
									<div class="form-group">
										<div class="col-xs-12 col-sm-5">
											<div class="control-group">
												<label class="control-label bolder blue">Tindakan</label>
												<div class="checkbox">
													<label>
														<input name="tindakan[]" type="checkbox" value="UC" <?php if(in_array("UC", $tindakan)){ echo " checked=\"checked\""; } ?> class="ace" / disabled>
														
														<span class="lbl"> UC</span>
													</label>											
													<label>
														<input name="tindakan[]" value="IVL" <?php if(in_array("IVL", $tindakan)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" / disabled>
														<span class="lbl"> IVL</span>
													</label>											
													<label>
														<input name="tindakan[]" value="CVL" <?php if(in_array("CVL", $tindakan)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" / disabled>
														<span class="lbl"> CVL</span>
													</label>											
													<label>
														<input name="tindakan[]" value="ETT/VENT" <?php if(in_array("ETT/VENT", $tindakan)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" / disabled>
														<span class="lbl"> ETT/VENT</span>
													</label>
												</div>									
											</div>
										</div>							
										<div class="col-xs-12 col-sm-5">							
											<div class="control-group">
												<label class="control-label bolder blue">Infeksi Rumah Sakit</label>
												<div class="checkbox">
													<label>
														<input name="infeksi[]" value="VAP" <?php if(in_array("VAP", $infeksi)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" / disabled>
														<span class="lbl"> VAP</span>
													</label>
												
													<label>
														<input name="infeksi[]" value="HAP" <?php if(in_array("HAP", $infeksi)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" / disabled>
														<span class="lbl"> HAP</span>
													</label>
												
													<label>
														<input name="infeksi[]" value="ISK" <?php if(in_array("ISK", $infeksi)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" / disabled>
														<span class="lbl"> ISK</span>
													</label>
												
													<label>
														<input name="infeksi[]" value="IAD" <?php if(in_array("IAD", $infeksi)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" / disabled>
														<span class="lbl"> IAD</span>
													</label>
												</div>										
											</div>
										</div>
									</div>							
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->
<?php
	}
	?>
