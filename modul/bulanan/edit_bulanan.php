<?php					
$id=($_GET['id']);
$query="select tdb.ID_DETAIL_B, tdb.ID_ISIAN_B, tdb.TGL_DETAIL_B, tdb.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, tdb.SURVEYOR, tdb.SUHU, tdb.UMUR, if(tdb.JENIS_KELAMIN=1,'L','P') JENIS_KELAMIN
, tdb.JENIS_KELAMIN ID_JENIS_KELAMIN, tdb.DX_MEDIS, tdb.UC, tdb.IVL, tdb.CVL, tdb.OB, tdb.OBT, tdb.OK, tdb.VAP, tdb.HAP, tdb.ISK, tdb.PLEBITIS, tdb.IAD, tdb.ILS, tdb.ILD, tdb.ILO, tdb.TIRAH
, tdb.DEKU, tdb.KULTUR, tdb.ANTIBIOTIK, tdb.TAB, tdb.SATU, tdb.DUA, tdb.TIGA, tdb.LEBIH_TIGA, tdb.DARAH, tdb.SWAB, tdb.SPUTUM, tdb.URINE, tdb.HASIL, tdb.`STATUS`
from tb_detail_bulanan tdb
LEFT JOIN referensi rjk ON rjk.ID=tdb.JENIS_KELAMIN and rjk.JENIS=1
LEFT JOIN ruangan ru ON ru.ID=tdb.RUANGAN and ru.JENIS=5							
where tdb.ID_ISIAN_B='$id'";
//die($query);
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$UC = explode(",", $dt['UC']);$IVL = explode(",", $dt['IVL']);$CVL = explode(",", $dt['CVL']);$OB = explode(",", $dt['OB']);$OBT = explode(",", $dt['OBT']);$OK = explode(",", $dt['OK']);
;$VAP = explode(",", $dt['VAP']);$HAP = explode(",", $dt['HAP']);$ISK = explode(",", $dt['ISK']);$PLEBITIS = explode(",", $dt['PLEBITIS']);$IAD = explode(",", $dt['IAD']);$ILS = explode(",", $dt['ILS']);$ILD = explode(",", $dt['ILD']);
$ILO = explode(",", $dt['ILO']);$TAB = explode(",", $dt['TAB']);$SATU = explode(",", $dt['SATU']);$DUA = explode(",", $dt['DUA']);$TIGA = explode(",", $dt['TIGA']);$LEBIH_TIGA = explode(",", $dt['LEBIH_TIGA']);
$DARAH = explode(",", $dt['DARAH']);$SWAB = explode(",", $dt['SWAB']);$SPUTUM = explode(",", $dt['SPUTUM']);$URINE = explode(",", $dt['URINE']);

  {
	  ?>
 
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Bulanan</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Surveilans Bulanan Infeksi Rumah Sakit</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=update_b" method="post">
								<input type="hidden" name="ID_ISIAN_B" class="form-control" value="<?php echo $dt['ID_ISIAN_B']; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TGL_DETAIL_B']; ?>" name="TGL_DETAIL_B" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ Ruangan ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ID_RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
												<?php
												}
												?>	
											</select>
										</div>
										<div class="col-sm-2">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['SURVEYOR']; ?>" name="SURVEYOR" placeholder="[ Surveyor ]" class="form-control" />
										</div>
										<div class="col-sm-1">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['SUHU']; ?>" name="SUHU" placeholder="[ SUHU ]" class="form-control" />
										</div>
										<div class="col-sm-1">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['UMUR']; ?>" name="UMUR" placeholder="[ UMUR ]" class="form-control" />
										</div>
										<div class="col-sm-1">
											<select class="chosen-select form-control" name="JENIS_KELAMIN" id="form-field-select-3">
												<option value=""></option>
												<?php
												$sql = "select r.ID, r.DESKRIPSI, if(r.ID=1,'L','P') JK 
												from db_ppi.referensi r
												where r.JENIS=1";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ID_JENIS_KELAMIN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['JK'] ?></option>
												<?php
												}
												?>	
											</select>
										</div>
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['DX_MEDIS']; ?>" name="DX_MEDIS" placeholder="[ DX MEDIS ]" class="form-control" />
										</div>
									</div>									
									<hr />
									<div class="form-group">
									
										<div class="col-sm-6">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tindakan :</label>
												<div class="checkbox">
													<label>
														<input name="UC" <?php if(in_array("1", $UC)){ echo " checked=\"checked\""; } ?> type="checkbox" value="1" class="ace" />
														<span class="lbl"> UC</span>
													</label>											
													<label>
														<input name="IVL" value="1" <?php if(in_array("1", $IVL)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> IVL</span>
													</label>											
													<label>
														<input name="CVL" value="1" <?php if(in_array("1", $CVL)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> CVL</span>
													</label>											
													<label>
														<input name="OB" value="1" <?php if(in_array("1", $OB)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> OB</span>
													</label>
													<label>
														<input name="OBT" value="1" <?php if(in_array("1", $OBT)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> OBT</span>
													</label>
													<label>
														<input name="OK" value="1" <?php if(in_array("1", $OK)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> OK</span>
													</label>
												</div>									
											</div>
										</div>	
										
										<div class="col-sm-6">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Infeksi Rumah Sakit :</label>
												<div class="checkbox">
													<label>
														<input name="VAP" value="1" <?php if(in_array("1", $VAP)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> VAP</span>
													</label>												
													<label>
														<input name="HAP" value="1" <?php if(in_array("1", $HAP)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> HAP</span>
													</label>												
													<label>
														<input name="ISK" value="1" <?php if(in_array("1", $ISK)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> ISK</span>
													</label>												
													<label>
														<input name="PLEBITIS" value="1" <?php if(in_array("1", $PLEBITIS)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> PLEBITIS</span>
													</label>
													<label>
														<input name="IAD" value="1" <?php if(in_array("1", $IAD)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> IAD</span>
													</label>
													<label>
														<input name="ILS" value="1" <?php if(in_array("1", $ILS)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> ILS</span>
													</label>
													<label>
														<input name="ILD" value="1" <?php if(in_array("1", $ILD)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> ILD</span>
													</label>
													<label>
														<input name="ILO" value="1" <?php if(in_array("1", $ILO)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> ILO</span>
													</label>
												</div>										
											</div>
										</div>
									</div>
									<hr />
									<div class="form-group">									
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['TIRAH']; ?>" name="TIRAH" placeholder=" [ TIRAH BARING ]" class="form-control" />
										</div>
																					
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['DEKU']; ?>" name="DEKU" placeholder=" [ DEKU ]" class="form-control" />
										</div>																							
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['KULTUR']; ?>" name="KULTUR" placeholder="[ HASIL KULTUR ]" class="form-control" />
										</div>																							
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['ANTIBIOTIK']; ?>" name="ANTIBIOTIK" placeholder="[ ANTIBIOTIK ]" class="form-control" />
										</div>
									</div>	
									<hr />
									<div class="form-group">
										<div class="col-xs-12 col-sm-4">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Macam antibiotik :</label>
												<div class="checkbox">
													<label>
														<input name="TAB" type="checkbox" value="1" <?php if(in_array("1", $TAB)){ echo " checked=\"checked\""; } ?> class="ace" />
														<span class="lbl"> TAB</span>
													</label>											
													<label>
														<input name="SATU" value="1" <?php if(in_array("1", $SATU)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> 1</span>
													</label>											
													<label>
														<input name="DUA" value="1" <?php if(in_array("1", $DUA)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> 2</span>
													</label>											
													<label>
														<input name="TIGA" value="1" <?php if(in_array("1", $TIGA)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> 3</span>
													</label>
													<label>
														<input name="LEBIH_TIGA" value="1" <?php if(in_array("1", $LEBIH_TIGA)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> >3</span>
													</label>
												</div>									
											</div>
										</div>							
										<div class="col-xs-12 col-sm-4">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Jenis Kuman :</label>
												<div class="checkbox">
													<label>
														<input name="DARAH" value="1" <?php if(in_array("1", $DARAH)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> DARAH</span>
													</label>
												
													<label>
														<input name="SWAB" value="1" <?php if(in_array("1", $SWAB)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> SWAB</span>
													</label>
												
													<label>
														<input name="SPUTUM" value="1" <?php if(in_array("1", $SPUTUM)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> SPUTUM</span>
													</label>
												
													<label>
														<input name="URINE" value="1" <?php if(in_array("1", $URINE)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> URINE</span>
													</label>
												</div>										
											</div>
										</div>
										<div class="col-sm-4">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['HASIL']; ?>" name="HASIL" placeholder="[ HASIL ]" class="form-control" />
										</div>								
									</div>																																												
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnEdit" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->
<?php
	}
	?>
