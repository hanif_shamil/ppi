<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>
		<li>
			<a href="#">Data</a>
		</li>
		<li class="active">Laporan Bulanan</li>
	</ul><!-- /.breadcrumb -->
	<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="ace-icon fa fa-search nav-search-icon"></i>
			</span>
		</form>
	</div><!-- /.nav-search -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="table-header">
						Laporan Bulanan
					</div>
					<!-- div.table-responsive -->
					<!-- div.dataTables_borderWrap -->
					<div>
						<table id="example" class="table table-striped table-bordered table-hover">
							<thead>
								<tr align="center">
									<th rowspan="2"><div align="center">Tanggal</div></th>
									<th rowspan="2"><div align="center">Ruangan</div></th>
									<th rowspan="2"><div align="center">Suhu</div></th>
									<th rowspan="2"><div align="center">Surveyor</div></th>
									<th rowspan="2"><div align="center">Umur</div></th>
									<th rowspan="2"><div align="center">JK</div></th>
									<th rowspan="2"><div align="center">DX Medis</div></th>
									<th colspan="6" ><div align="center">Tindakan</div></th>
									<th colspan="5"><div align="center">Infeksi RS</div></th>
									<th rowspan="2"><div align="center">Tirah</div></th>
									<th rowspan="2"><div align="center">Antibiotik</div></th>									
									<th colspan="4"><div align="center">Hasil kultur</div></th>
									<th rowspan="2"><div align="center">Pilihan</div></th>
								</tr>
								<tr>
								<td>UC</td>
								<td>IVL</td>
								<td>CVL</td>
								<td>OB</td>
								<td>OBT</td>
								<td>OK</td>
								<td>VAP</td>
								<td>HAP</td>
								<td>ISK</td>
								<td>PLEB</td>
								<td>IAD</td>
								<td>Darah</td>
								<td>Swab</td>
								<td>Sputum</td>
								<td>Urine</td>
								</tr>																
							</thead>
							<tbody>
							<?php					
							$query="select tdb.ID_DETAIL_B, tdb.ID_ISIAN_B, tdb.TGL_DETAIL_B, tdb.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN
							, tdb.SURVEYOR, tdb.SUHU, tdb.UMUR, if(tdb.JENIS_KELAMIN=1,'L','P') JENIS_KELAMIN, tdb.JENIS_KELAMIN ID_JENIS_KELAMIN
							, tdb.DX_MEDIS, tdb.UC, tdb.IVL, tdb.CVL, tdb.OB, tdb.OBT, tdb.OK, tdb.VAP, tdb.HAP, tdb.ISK, tdb.PLEBITIS
							, tdb.IAD, tdb.ILS, tdb.ILD, tdb.ILO, tdb.TIRAH, tdb.DEKU, tdb.KULTUR, tdb.ANTIBIOTIK, tdb.TAB, tdb.SATU
							, tdb.DUA, tdb.TIGA, tdb.LEBIH_TIGA, tdb.DARAH, tdb.SWAB, tdb.SPUTUM, tdb.URINE, tdb.HASIL, tdb.`STATUS`
							from tb_detail_bulanan tdb
							LEFT JOIN referensi rjk ON rjk.ID=tdb.JENIS_KELAMIN and rjk.JENIS=1
							LEFT JOIN ruangan ru ON ru.ID=tdb.RUANGAN and ru.JENIS=5
							where tdb.`STATUS`=1";							
							$info=mysqli_query($conn1,$query); 
							//untuk penomoran data
							//$no=1;						
							//menampilkan data
							while($row=mysqli_fetch_array($info)){
							?>
								<tr>
									<td><?php echo $row['TGL_DETAIL_B'] ?></td>
									<td><?php echo $row['RUANGAN'] ?></td>
									<td><?php echo $row['SUHU'] ?></td>
									<td><?php echo $row['SURVEYOR'] ?></td>
									<td><?php echo $row['UMUR'] ?></td>
									<td><?php echo $row['JENIS_KELAMIN'] ?></td>
									<td><?php echo $row['DX_MEDIS'] ?></td>									
									<td>
										<?php if ($row['UC']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['IVL']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['CVL']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['OB']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['OBT']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['OK']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['VAP']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['HAP']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['ISK']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['PLEBITIS']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
									<td>
										<?php if ($row['IAD']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>										
									<td><?php echo $row['TIRAH'] ?></td>								
									<td><?php echo $row['ANTIBIOTIK'] ?></td>											
										<td>
										<?php if ($row['DARAH']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
										<td>
										<?php if ($row['SWAB']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
										<td>
										<?php if ($row['SPUTUM']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>
										<td>
										<?php if ($row['URINE']=='1'){?>
										<?php echo '<i class="ace-icon glyphicon glyphicon-ok" style="color: blue"></i>' ?>
										<?php }
										else {?>
										<?php echo '<i class="ace-icon glyphicon  glyphicon-remove" style="color: red" ></i>' ?>
										<?php }
										?>					
									</td>									
									<td>
										<div class="hidden-sm hidden-xs action-buttons">
											<a class="blue" href="?page=detail_b&id=<?php echo $row['ID_ISIAN_B']?>">
												<i class="ace-icon fa fa-search-plus bigger-130"></i>												
											</a>
											<a class="green" href="?page=edit_b&id=<?php echo $row['ID_ISIAN_B']?>">
												<i class="ace-icon fa fa-pencil bigger-130"></i>
											</a>
											<a class="red" href="?page=delet_b&id=<?php echo $row['ID_ISIAN_B']?>">
												<i class="ace-icon fa fa-trash-o bigger-130"></i>
											</a>
										</div>
									</td>
								</tr>							
								<?php							
								}
								?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

