<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISIAN_B) AS last FROM tb_isian_bulanan WHERE ID_ISIAN_B LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Bulanan</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Surveilans Bulanan Infeksi Rumah Sakit</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_b" method="post">
								<input type="hidden" name="ID_ISIAN_B" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" name="TGL_DETAIL_B" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ Ruangan ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data[0]?>"><?=$data[3]?></option>
												<?php
												}
												?>	
											</select>
										</div>
										<div class="col-sm-2">
											<input type="text" id="form-field-1-1" name="SURVEYOR" placeholder="[ Surveyor ]" class="form-control" />
										</div>
										<div class="col-sm-1">
											<input type="text" id="form-field-1-1" name="SUHU" placeholder="[ SUHU ]" class="form-control" />
										</div>
										<div class="col-sm-1">
											<input type="text" id="form-field-1-1" name="UMUR" placeholder="[ UMUR ]" class="form-control" />
										</div>
										<div class="col-sm-1">
											<select class="chosen-select form-control" name="JENIS_KELAMIN" id="form-field-select-3" data-placeholder="[ JK ]">
												<option value=""></option>
												<?php
												$sql = "select r.ID, r.DESKRIPSI, if(r.ID=1,'L','P') Jk 
												from db_ppi.referensi r
												where r.JENIS=1";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data['ID']?>"><?=$data['Jk']?></option>
												<?php
												}
												?>	
											</select>
										</div>
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" name="DX_MEDIS" placeholder="[ DX MEDIS ]" class="form-control" />
										</div>
									</div>									
									<hr />
									<div class="form-group">
									
										<div class="col-sm-6">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tindakan :</label>
												<div class="checkbox">
													<label>
														<input name="UC[]" type="checkbox" value="1" class="ace" />
														<span class="lbl"> UC</span>
													</label>											
													<label>
														<input name="IVL[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> IVL</span>
													</label>											
													<label>
														<input name="CVL[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> CVL</span>
													</label>											
													<label>
														<input name="OB[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> OB</span>
													</label>
													<label>
														<input name="OBT[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> OBT</span>
													</label>
													<label>
														<input name="OK[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> OK</span>
													</label>
												</div>									
											</div>
										</div>	
										
										<div class="col-sm-6">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Infeksi Rumah Sakit :</label>
												<div class="checkbox">
													<label>
														<input name="VAP[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> VAP</span>
													</label>												
													<label>
														<input name="HAP[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> HAP</span>
													</label>												
													<label>
														<input name="ISK[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> ISK</span>
													</label>												
													<label>
														<input name="PLEBITIS[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> PLEBITIS</span>
													</label>
													<label>
														<input name="IAD[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> IAD</span>
													</label>
													<label>
														<input name="ILS[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> ILS</span>
													</label>
													<label>
														<input name="ILD[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> ILD</span>
													</label>
													<label>
														<input name="ILO[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> ILO</span>
													</label>
												</div>										
											</div>
										</div>
									</div>
									<hr />
									<div class="form-group">									
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" name="TIRAH" placeholder=" [ TIRAH BARING ]" class="form-control" />
										</div>
																					
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" name="DEKU" placeholder=" [ DEKU ]" class="form-control" />
										</div>																							
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" name="KULTUR" placeholder="[ HASIL KULTUR ]" class="form-control" />
										</div>																							
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" name="ANTIBIOTIK" placeholder="[ ANTIBIOTIK ]" class="form-control" />
										</div>
									</div>	
									<hr />
									<div class="form-group">
										<div class="col-xs-12 col-sm-4">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Macam antibiotik :</label>
												<div class="checkbox">
													<label>
														<input name="TAB[]" type="checkbox" value="1" class="ace" />
														<span class="lbl"> TAB</span>
													</label>											
													<label>
														<input name="SATU[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> 1</span>
													</label>											
													<label>
														<input name="DUA[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> 2</span>
													</label>											
													<label>
														<input name="TIGA[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> 3</span>
													</label>
													<label>
														<input name="LEBIH_TIGA[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> >3</span>
													</label>
												</div>									
											</div>
										</div>							
										<div class="col-xs-12 col-sm-4">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Jenis Kuman :</label>
												<div class="checkbox">
													<label>
														<input name="DARAH[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> DARAH</span>
													</label>
												
													<label>
														<input name="SWAB[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> SWAB</span>
													</label>
												
													<label>
														<input name="SPUTUM[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> SPUTUM</span>
													</label>
												
													<label>
														<input name="URINE[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> URINE</span>
													</label>
												</div>										
											</div>
										</div>
										<div class="col-sm-4">
											<input type="text" id="form-field-1-1" name="HASIL" placeholder="[ HASIL ]" class="form-control" />
										</div>								
									</div>																																												
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->