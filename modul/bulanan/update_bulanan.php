<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnEdit'])){
						$ID_ISIAN_B = $_POST['ID_ISIAN_B'];
						$TGL_DETAIL_B = $_POST['TGL_DETAIL_B'];
						$RUANGAN = $_POST['RUANGAN'];
						$SURVEYOR = $_POST['SURVEYOR'];
						$SUHU = $_POST['SUHU'];
						$UMUR = $_POST['UMUR'];
						$JENIS_KELAMIN = $_POST['JENIS_KELAMIN'];
						$DX_MEDIS = $_POST['DX_MEDIS'];
						if(isset($_POST['UC'])){
							$UC = $_POST['UC'];
						}else{
						$UC = "";}
						if(isset($_POST['IVL'])){
							$IVL = $_POST['IVL'];
						}else{
						$IVL = "";}
						if(isset($_POST['CVL'])){
							$CVL = $_POST['CVL'];
						}else{
						$CVL = "";}						
						if(isset($_POST['OB'])){
							$OB = $_POST['OB'];
						}else{
						$OB = "";}
						if(isset($_POST['OBT'])){
							$OBT = $_POST['OBT'];
						}else{
						$OBT = "";}
						if(isset($_POST['OK'])){
							$OK = $_POST['OK'];
						}else{
						$OK = "";}
						if(isset($_POST['VAP'])){
							$VAP = $_POST['VAP'];
						}else{
						$VAP = "";}								
						if(isset($_POST['HAP'])){
							$HAP = $_POST['HAP'];
						}else{
						$HAP = "";	}					
						if(isset($_POST['ISK'])){
							$ISK = $_POST['ISK'];
						}else{
						$ISK = "";	}
						if(isset($_POST['PLEBITIS'])){
							$PLEBITIS = $_POST['PLEBITIS'];
						}else{
						$PLEBITIS = "";}	
						if(isset($_POST['IAD'])){
							$IAD = $_POST['IAD'];
						}else{
						$IAD = "";}	
						if(isset($_POST['ILS'])){
							$ILS = $_POST['ILS'];
						}else{
						$ILS = "";}
						if(isset($_POST['ILD'])){
							$ILD = $_POST['ILD'];
						}else{
						$ILD = "";}	
						if(isset($_POST['ILO'])){
							$ILO = $_POST['ILO'];
						}else{
						$ILO = "";}	
						$TIRAH = $_POST['TIRAH'];
						$DEKU = $_POST['DEKU'];
						$KULTUR = $_POST['KULTUR'];
						$ANTIBIOTIK = $_POST['ANTIBIOTIK'];
						if(isset($_POST['TAB'])){
							$TAB = $_POST['TAB'];
						}else{
						$TAB = "";	}
						if(isset($_POST['SATU'])){
							$SATU = $_POST['SATU'];
						}else{
						$SATU = "";	}
						if(isset($_POST['DUA'])){
							$DUA = $_POST['DUA'];
						}else{
						$DUA = "";}	
						if(isset($_POST['TIGA'])){
							$TIGA = $_POST['TIGA'];
						}else{
						$TIGA = "";}	
						if(isset($_POST['LEBIH_TIGA'])){
							$LEBIH_TIGA = $_POST['LEBIH_TIGA'];
						}else{
						$LEBIH_TIGA = "";}	
						if(isset($_POST['DARAH'])){
							$DARAH = $_POST['DARAH'];
						}else{
						$DARAH = "";}	
						if(isset($_POST['SWAB'])){
							$SWAB = $_POST['SWAB'];
						}else{
						$SWAB = "";	}
						if(isset($_POST['SPUTUM'])){
							$SPUTUM = $_POST['SPUTUM'];
						}else{
						$SPUTUM = "";}	
						if(isset($_POST['URINE'])){
							$URINE = $_POST['URINE'];
						}else{
							$URINE = "";}
						$HASIL = $_POST['HASIL'];	
						$pengisi 			= $_SESSION['userid'];		
						$waktu = date("Y-m-d H:i:s");	
						//echo "<pre>";print_r($_POST);exit();                
                        $query ="UPDATE db_ppi.tb_isian_bulanan SET TGL_UBAH='$waktu', UBAH_OLEH='$pengisi' where  ID_ISIAN_B='$ID_ISIAN_B'";
						//echo $query; die();	
						$rs = mysqli_query($conn1,$query);
						if($rs)	{						
                        $query2 = "UPDATE db_ppi.tb_detail_bulanan SET						
						ID_ISIAN_B = '$ID_ISIAN_B',
						TGL_DETAIL_B = '$TGL_DETAIL_B',
						RUANGAN = '$RUANGAN',
						SURVEYOR = '$SURVEYOR',
						SUHU = '$SUHU',
						UMUR = '$UMUR',
						JENIS_KELAMIN = '$JENIS_KELAMIN',
						DX_MEDIS = '$DX_MEDIS',
						UC = '$UC',
						IVL = '$IVL',
						CVL = '$CVL',
						OB = '$OB',
						OBT = '$OBT',
						OK = '$OK',
						VAP = '$VAP',
						HAP = '$HAP',
						ISK = '$ISK',
						PLEBITIS = '$PLEBITIS',
						IAD = '$IAD',
						ILS = '$ILS',
						ILD = '$ILD',
						ILO = '$ILO',
						TIRAH = '$TIRAH',
						DEKU = '$DEKU',
						KULTUR = '$KULTUR',
						ANTIBIOTIK = '$ANTIBIOTIK',
						TAB = '$TAB',
						SATU = '$SATU',
						DUA = '$DUA',
						TIGA = '$TIGA',
						LEBIH_TIGA = '$LEBIH_TIGA',
						DARAH = '$DARAH',
						SWAB = '$SWAB',
						SPUTUM = '$SPUTUM',
						URINE = '$URINE',
						HASIL = '$HASIL'	
						where ID_ISIAN_B='$ID_ISIAN_B'";
						//echo $query2; die();	
						$insert=mysqli_query($conn1,$query2);									
						if($insert){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_b'</script>"; 
						}
						}
						}                       
						else{
                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
                        }                         
?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			