<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						if(isset($_POST['btnsimpan'])){
						$ID_ISIAN_B 		= $_POST['ID_ISIAN_B'];							
						$TGL_DETAIL_B 		= $_POST['TGL_DETAIL_B'];		
						$RUANGAN 			= $_POST['RUANGAN'];								
						$SURVEYOR 			= $_POST['SURVEYOR'];			
						$SUHU 				= $_POST['SUHU'];					
						$UMUR 				= $_POST['UMUR'];			
						$JENIS_KELAMIN 		= $_POST['JENIS_KELAMIN'];			
						$DX_MEDIS 			= $_POST['DX_MEDIS'];														
						if(isset($_POST['UC'])){
							$UC = implode(',',$_POST['UC']);
						}else{
							$UC = "";
						}							
						if(isset($_POST['IVL'])){
							$IVL = implode(',',$_POST['IVL']);
						}else{
							$IVL = "";
						}
						if(isset($_POST['CVL'])){
							$CVL = implode(',',$_POST['CVL']);
						}else{
							$CVL = "";
						}
						if(isset($_POST['OB'])){
							$OB = implode(',',$_POST['OB']);
						}else{
							$OB = "";
						}
						if(isset($_POST['OBT'])){
							$OBT = implode(',',$_POST['OBT']);
						}else{
							$OBT = "";
						}
						if(isset($_POST['OK'])){
							$OK = implode(',',$_POST['OK']);
						}else{
							$OK = "";
						}
						if(isset($_POST['VAP'])){
							$VAP = implode(',',$_POST['VAP']);
						}else{
							$VAP = "";
						}
						if(isset($_POST['HAP'])){
							$HAP = implode(',',$_POST['HAP']);
						}else{
							$HAP = "";
						}
						if(isset($_POST['ISK'])){
							$ISK = implode(',',$_POST['ISK']);
						}else{
							$ISK = "";
						}
						if(isset($_POST['PLEBITIS'])){
							$PLEBITIS = implode(',',$_POST['PLEBITIS']);
						}else{
							$PLEBITIS = "";
						}
						if(isset($_POST['IAD'])){
							$IAD = implode(',',$_POST['IAD']);
						}else{
							$IAD = "";
						}
						if(isset($_POST['ILS'])){
							$ILS = implode(',',$_POST['ILS']);
						}else{
							$ILS = "";
						}
						if(isset($_POST['ILD'])){
							$ILD = implode(',',$_POST['ILD']);
						}else{
							$ILD = "";
						}
						if(isset($_POST['ILO'])){
							$ILO = implode(',',$_POST['ILO']);
						}else{
							$ILO = "";
						}
						
						$TIRAH	 	= $_POST['TIRAH'];
						$DEKU 		= $_POST['DEKU'];
						$KULTUR 	= $_POST['KULTUR'];
						$ANTIBIOTIK = $_POST['ANTIBIOTIK'];
						if(isset($_POST['TAB'])){
							$TAB = implode(',',$_POST['TAB']);
						}else{
							$TAB = "";
						}
						if(isset($_POST['SATU'])){
							$SATU = implode(',',$_POST['SATU']);
						}else{
							$SATU = "";
						}
						if(isset($_POST['DUA'])){
							$DUA = implode(',',$_POST['DUA']);
						}else{
							$DUA = "";
						}
						if(isset($_POST['TIGA'])){
							$TIGA = implode(',',$_POST['TIGA']);
						}else{
							$TIGA = "";
						}	
						if(isset($_POST['LEBIH_TIGA'])){
							$LEBIH_TIGA = implode(',',$_POST['LEBIH_TIGA']);
						}else{
							$LEBIH_TIGA = "";
						}	
						if(isset($_POST['DARAH'])){
							$DARAH = implode(',',$_POST['DARAH']);
						}else{
							$DARAH = "";
						}
						if(isset($_POST['SWAB'])){
							$SWAB = implode(',',$_POST['SWAB']);
						}else{
							$SWAB = "";
						}	
						if(isset($_POST['SPUTUM'])){
							$SPUTUM = implode(',',$_POST['SPUTUM']);
						}else{
							$SPUTUM = "";
						}	
						if(isset($_POST['URINE'])){
							$URINE = implode(',',$_POST['URINE']);
						}else{
							$URINE = "";
						}	
						$HASIL 		= $_POST['HASIL'];
						$pengisi 	= $_SESSION['userid'];							
						//echo "<pre>";print_r($_POST);exit();                                      
						$query ="INSERT INTO db_ppi.tb_isian_bulanan (ID_ISIAN_B,USER,STATUS) VALUES ('$ID_ISIAN_B','$pengisi','1')"; //echo $query; die();	
						$rs = mysqli_query($conn1,$query);
						if($rs)	{												
                        $query2 = "INSERT INTO db_ppi.tb_detail_bulanan (
						ID_ISIAN_B,
						TGL_DETAIL_B,
						RUANGAN,
						SURVEYOR,
						SUHU,						
						UMUR,
						JENIS_KELAMIN,
						DX_MEDIS,
						UC,
						IVL,
						CVL,
						OB,
						OBT,
						OK,
						VAP,
						HAP,
						ISK,
						PLEBITIS,
						IAD,
						ILS,
						ILD,
						ILO,						
						TIRAH,
						DEKU,
						KULTUR,
						ANTIBIOTIK,
						TAB,
						SATU,
						DUA,
						TIGA,
						LEBIH_TIGA,
						DARAH,
						SWAB,
						SPUTUM,
						URINE,
						HASIL,
						STATUS) 
						VALUES 
						('$ID_ISIAN_B',
						'$TGL_DETAIL_B',
						'$RUANGAN',
						'$SURVEYOR',
						'$SUHU',						
						'$UMUR',
						'$JENIS_KELAMIN',
						'$DX_MEDIS',
						'$UC',
						'$IVL',
						'$CVL',
						'$OB',
						'$OBT',
						'$OK',
						'$VAP',
						'$HAP',
						'$ISK',
						'$PLEBITIS',
						'$IAD',
						'$ILS',
						'$ILD',
						'$ILO',						
						'$TIRAH',
						'$DEKU',
						'$KULTUR',
						'$ANTIBIOTIK',
						'$TAB',
						'$SATU',
						'$DUA',
						'$TIGA',
						'$LEBIH_TIGA',
						'$DARAH',
						'$SWAB',
						'$SPUTUM',
						'$URINE',
						'$HASIL',					
						'1')";
						//echo $query2; die();	
						$insert=mysqli_query($conn1,$query2);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_b'</script>"; 
						}
						}
						}                       
						else{
                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
                        }                         
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			