<?php
$id=($_GET['id']);
$query="SELECT ti.*, ru.ID ID_RUANG, ru.DESKRIPSI RUANGAN FROM db_ppi.tb_transfercovid ti
left join ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5							
where ti.ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$SOAL1 = explode(",", $dt['SOAL1']);$SOAL2 = explode(",", $dt['SOAL2']);$SOAL3 = explode(",", $dt['SOAL3']);$SOAL4 = explode(",", $dt['SOAL4']);
$SOAL5 = explode(",", $dt['SOAL5']);$SOAL6 = explode(",", $dt['SOAL6']);$SOAL7 = explode(",", $dt['SOAL7']);$SOAL8 = explode(",", $dt['SOAL8']);
$SOAL9 = explode(",", $dt['SOAL9']);$SOAL10 = explode(",", $dt['SOAL10']);$SOAL11 = explode(",", $dt['SOAL11']);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>
					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">Formulir Edit Monitoring Transfer Pasien Infeksi Airborne</li>
				</ul><!-- /.breadcrumb -->
			</div>

			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Formulir Edit Monitoring Transfer Pasien Infeksi Airborne</h4>

								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>
									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_transcovid" method="post">
										<input type="hidden" name="ID" class="form-control" value="<?php echo $dt['ID']; ?>" readonly>										
										<div class="form-group">										
											<div class="col-sm-3">
												<div class="input-group">
													<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TANGGAL']; ?>" name="TANGGAL" type="text" />
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-3">										
												<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" value="<?php echo $dt['RUANGAN']; ?>" data-placeholder="[ RUANGAN ]">
													<option value=""></option>
													<?php
													$sql = "select * from db_ppi.ruangan r
													where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
													$rs = mysqli_query($conn1,$sql);
													while ($data = mysqli_fetch_array($rs)) {
														?>
														<option <?php if($dt['ID_RUANG']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
														<?php
													}
													?>	
												</select>
											</div>	

											<div class="col-sm-3">
												<input type="text" id="nomr" name="PENGIRIM" value="<?php echo $dt['PENGIRIM']; ?>" placeholder="[ PETUGAS YG MEMINDAHKAN ]" class="form-control" onkeyup="autofill()" />												
											</div>	
											<div class="col-sm-3">									
												<input type="text" id="nama" name="PENERIMA" value="<?php echo $dt['PENERIMA']; ?>" placeholder="[ PETUGAS YG MENRIMA ]" class="form-control" />												
											</div>																			
										</div>									
										<hr />

										<div class="form-group">
											<div class="col-sm-12">											
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="text-align:center">NO</th>
															<th style="text-align:center"> KEGIATAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
														</tr>
													</thead>
													<tbody>													
														<tr>
															<td>1</td>
															<td>Menginformasikan ke ruang perawatan isolasi covid 19  bahwa akan ada pasien   yang akan di transfer ke ruang tersebut</td>
															<td align="center"><label>
																<input type="radio" name="SOAL1[]" <?php if(in_array("1", $SOAL1)){ echo " checked=\"checked\""; } ?> id="" value="1" class="ace input-lg" >																																													
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="SOAL1[]" type="radio" <?php if(in_array("2", $SOAL1)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Melakukan identifikasi pasien yang akan di transfer, meliputi: nama, tanggal lahir, nomor MR</td>
															<td align="center"><label>
																<input type="radio" name="SOAL2[]" <?php if(in_array("1", $SOAL2)){ echo " checked=\"checked\""; } ?> id="" value="1" class="ace input-lg" >													
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center"><label>
															<input name="SOAL2[]" type="radio" <?php if(in_array("2", $SOAL2)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Melengkapi formulir pemindahan/ transfer pasien rawat</td>
													<td align="center">
														<label>
															<input type="radio" name="SOAL3[]" <?php if(in_array("1", $SOAL3)){ echo " checked=\"checked\""; } ?> id="" value="1" class="ace input-lg" >
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SOAL3[]" type="radio" <?php if(in_array("2", $SOAL3)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>4</td>
													<td>Petugas menggunakan APD level 3 saat memindahkan pasien dari tempat tidur ke brankart khusus</td>
													<td align="center">
														<label>
															<input name="SOAL4[]" type="radio" <?php if(in_array("1", $SOAL4)){ echo " checked=\"checked\""; } ?> value="1" id="" class="ace input-lg"  />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SOAL4[]" type="radio" <?php if(in_array("2", $SOAL4)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>5</td>
													<td>Petugas yang menerima pasien di ruang isolasi covid 19 menggunakan APD level 3</td>
													<td align="center">
														<label>
															<input name="SOAL5[]" type="radio" <?php if(in_array("1", $SOAL5)){ echo " checked=\"checked\""; } ?> value="1" id="" class="ace input-lg"  />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SOAL5[]" type="radio" <?php if(in_array("2", $SOAL5)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>6</td>
													<td>Petugas melakukan kebersihan tangan sebelum masuk ke ruang isolasi covid 19</td>
													<td align="center">
														<label>
															<input name="SOAL6[]" type="radio" <?php if(in_array("1", $SOAL6)){ echo " checked=\"checked\""; } ?> value="1" id="" class="ace input-lg"  />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SOAL6[]" type="radio" <?php if(in_array("2", $SOAL6)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>7</td>
													<td>Petugas melakukan kebersihan tangan sesudah mentransfer pasien konfirmasi covid 19</td>
													<td align="center">
														<label>
															<input name="SOAL7[]" type="radio" <?php if(in_array("1", $SOAL7)){ echo " checked=\"checked\""; } ?> value="1" id="" class="ace input-lg"  />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SOAL7[]" type="radio" <?php if(in_array("2", $SOAL7)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>8</td>
													<td>Edukasi pasien tentang etika batuk, tidak membuang dahak di sembarang tempat dan kebersihan tangan oleh petugas ruang isolasi covid 19, sosialisasi ruangan dan fasilitas yang tersedia</td>
													<td align="center">
														<label>
															<input name="SOAL8[]" type="radio" <?php if(in_array("1", $SOAL8)){ echo " checked=\"checked\""; } ?> value="1" id="" class="ace input-lg"  />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SOAL8[]" type="radio" <?php if(in_array("2", $SOAL8)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>9</td>
													<td>Petugas ruang isolasi melepas linen dari brancard, masukkan ke dalam plastik kuning /infeksius</td>
													<td align="center">
														<label>
															<input name="SOAL9[]" type="radio" <?php if(in_array("1", $SOAL9)){ echo " checked=\"checked\""; } ?> value="1" id="" class="ace input-lg"  />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SOAL9[]" type="radio" <?php if(in_array("2", $SOAL9)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr><tr>
													<td>10</td>
													<td>Brancard khusus setelah digunakan untuk transfer dibersihkan menggunakan cairan desinfektan sterisid atau chlorin</td>
													<td align="center">
														<label>
															<input name="SOAL10[]" type="radio" <?php if(in_array("1", $SOAL10)){ echo " checked=\"checked\""; } ?> value="1" id="" class="ace input-lg"  />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SOAL10[]" type="radio" <?php if(in_array("2", $SOAL10)){ echo " checked=\"checked\""; } ?> value="2" id="" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr><tr>
													<td>11</td>
													<td>Petugas yang mengantar melepaskan APD sesuai prosedur melepaskan APD</td>
													<td align="center">
														<label>
															<input name="SOAL11[]" type="radio" <?php if(in_array("1", $SOAL11)){ echo " checked=\"checked\""; } ?> value="1"  class="ace input-lg"  />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SOAL11[]" type="radio" <?php if(in_array("2", $SOAL11)){ echo " checked=\"checked\""; } ?> value="2" class="ace input-lg" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>												
											</tbody>
										</table>											
									</div>
								</div>													


								<hr />
								<div class="form-group">							
									<div class="col-md-offset-3 col-md-9">
										<button class="btn btn-info" name="btnEdit" type="submit">
											<i class="ace-icon fa fa-check bigger-110"></i>
											Submit
										</button>
										&nbsp; &nbsp; &nbsp;
										<button class="btn" type="reset">
											%
											Reset
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div><!-- /.span -->
		</div>
	</div><!-- /.page-content -->	
</div> <!-- container -->
</div><!-- /.main-content -->
<?php
}?>