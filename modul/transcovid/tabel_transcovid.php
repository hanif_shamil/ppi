<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Data</a>
		</li>
		<li class="active">Data Monitoring Transfer Pasien Covid Ke Ruang Isolasi</li>
	</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Data Monitoring Transfer Pasien Covid Ke Ruang Isolasi</h4>
							<div class="widget-toolbar">
								<a href="index.php?page=transcovid">
									<i class="ace-icon fa fa-plus" style="color:blue"> Tambah data</i>
								</a>
							</div>
						</div>
						<!-- div.table-responsive -->
						<!-- div.dataTables_borderWrap -->
						<div>
							<table id="tabeldata" class="table table-striped table-bordered table-hover">
								<thead>
									<tr align="center">
										<th><div align="center">Tanggal</div></th>
										<th><div align="center">Ruang Asal</div></th>
										<th><div align="center">Pengirim</div></th>
										<th><div align="center">Penerima</div></th>
										<th><div align="center">Opsi</div></th>
									</tr>								

								</thead>

								<tbody>
									<?php					
									$query="SELECT ti.*, ru.ID ID_RUANG, ru.DESKRIPSI RUANGAN FROM db_ppi.tb_transfercovid ti
									left join ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5							
									where ti.`STATUS`=1";							
									$info=mysqli_query($conn1,$query); 
									while($row=mysqli_fetch_array($info)){
										?>
										<tr>
											<td><?php echo $row['TANGGAL'] ?></td>
											<td><?php echo $row['RUANGAN'] ?></td>
											<td><?php echo $row['PENGIRIM'] ?></td>
											<td><?php echo $row['PENERIMA'] ?></td>														
											<td width="10%" align="center" >
												<div class="hidden-sm hidden-xs action-buttons">
													<a class="green" href="?page=transcovid_edit&id=<?php echo $row['ID']?>">
														<i class="glyphicon glyphicon-pencil btn btn-success btn-sm"></i>
													</a>
													<a class="red" href="?page=simpan_transcovid&id=<?php echo $row['ID']?>">
														<i class="glyphicon glyphicon-trash btn btn-danger btn-sm"></i>	
													</a>
												</div>
											</td>
										</tr>							
										<?php							
									}
									?>	
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.page-content -->

