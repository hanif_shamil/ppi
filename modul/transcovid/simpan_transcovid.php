<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];	
						$TUJUAN 		= $_POST['TUJUAN'];							
						$PENGIRIM 		= $_POST['PENGIRIM'];
						$PENERIMA 		= $_POST['PENERIMA'];
						if(isset($_POST['SOAL1'])){
							$SOAL1 = implode(',',$_POST['SOAL1']);
						}else{
							$SOAL1 = "";
						}		
						if(isset($_POST['SOAL2'])){
							$SOAL2 = implode(',',$_POST['SOAL2']);
						}else{
							$SOAL2 = "";
						}	
						if(isset($_POST['SOAL3'])){
							$SOAL3 = implode(',',$_POST['SOAL3']);
						}else{
							$SOAL3 = "";
						}	
						if(isset($_POST['SOAL4'])){
							$SOAL4 = implode(',',$_POST['SOAL4']);
						}else{
							$SOAL4 = "";
						}	
						if(isset($_POST['SOAL5'])){
							$SOAL5 = implode(',',$_POST['SOAL5']);
						}else{
							$SOAL5 = "";
						}
						if(isset($_POST['SOAL6'])){
							$SOAL6 = implode(',',$_POST['SOAL6']);
						}else{
							$SOAL6 = "";
						}
						if(isset($_POST['SOAL7'])){
							$SOAL7 = implode(',',$_POST['SOAL7']);
						}else{
							$SOAL7 = "";
						}
						if(isset($_POST['SOAL8'])){
							$SOAL8 = implode(',',$_POST['SOAL8']);
						}else{
							$SOAL8 = "";
						}
						if(isset($_POST['SOAL9'])){
							$SOAL9 = implode(',',$_POST['SOAL9']);
						}else{
							$SOAL9 = "";
						}
						if(isset($_POST['SOAL10'])){
							$SOAL10 = implode(',',$_POST['SOAL10']);
						}else{
							$SOAL10 = "";
						}
						if(isset($_POST['SOAL11'])){
							$SOAL11 = implode(',',$_POST['SOAL11']);
						}else{
							$SOAL11 = "";
						}
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						// echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_transfercovid (
						TANGGAL,
						RUANGAN,
						PENGIRIM,
						PENERIMA,
						SOAL1,
						SOAL2,
						SOAL3,
						SOAL4,
						SOAL5,
						SOAL6,
						SOAL7,
						SOAL8,
						SOAL9,
						SOAL10,
						SOAL11,
						USER) 
						VALUES 
						('$TANGGAL',
						'$RUANGAN',
						'$PENGIRIM',
						'$PENERIMA',
						'$SOAL1',
						'$SOAL2',	
						'$SOAL3',		
						'$SOAL4',				
						'$SOAL5',	
						'$SOAL6',	
						'$SOAL7',	
						'$SOAL8',	
						'$SOAL9',	
						'$SOAL10',	
						'$SOAL11',	
						'$USER')";
						// echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_transcovid'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID				= $_POST['ID'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];							
						$PENGIRIM 		= $_POST['PENGIRIM'];
						$PENERIMA 		= $_POST['PENERIMA'];
						if(isset($_POST['SOAL1'])){
							$SOAL1 = implode(',',$_POST['SOAL1']);
						}else{
							$SOAL1 = "";
						}		
						if(isset($_POST['SOAL2'])){
							$SOAL2 = implode(',',$_POST['SOAL2']);
						}else{
							$SOAL2 = "";
						}	
						if(isset($_POST['SOAL3'])){
							$SOAL3 = implode(',',$_POST['SOAL3']);
						}else{
							$SOAL3 = "";
						}	
						if(isset($_POST['SOAL4'])){
							$SOAL4 = implode(',',$_POST['SOAL4']);
						}else{
							$SOAL4 = "";
						}	
						if(isset($_POST['SOAL5'])){
							$SOAL5 = implode(',',$_POST['SOAL5']);
						}else{
							$SOAL5 = "";
						}
						if(isset($_POST['SOAL6'])){
							$SOAL6 = implode(',',$_POST['SOAL6']);
						}else{
							$SOAL6 = "";
						}
						if(isset($_POST['SOAL7'])){
							$SOAL7 = implode(',',$_POST['SOAL7']);
						}else{
							$SOAL7 = "";
						}
						if(isset($_POST['SOAL8'])){
							$SOAL8 = implode(',',$_POST['SOAL8']);
						}else{
							$SOAL8 = "";
						}
						if(isset($_POST['SOAL9'])){
							$SOAL9 = implode(',',$_POST['SOAL9']);
						}else{
							$SOAL9 = "";
						}
						if(isset($_POST['SOAL10'])){
							$SOAL10 = implode(',',$_POST['SOAL10']);
						}else{
							$SOAL10 = "";
						}	
						if(isset($_POST['SOAL11'])){
							$SOAL11 = implode(',',$_POST['SOAL11']);
						}else{
							$SOAL11 = "";
						}						
						$USER 			= $_SESSION['userid'];			
						// echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_transfercovid SET												
						TANGGAL='$TANGGAL',
						RUANGAN='$RUANGAN',
						PENGIRIM='$PENGIRIM',
						PENERIMA='$PENERIMA',
						SOAL1='$SOAL1',
						SOAL2='$SOAL2',
						SOAL3='$SOAL3',
						SOAL4='$SOAL4',
						SOAL5='$SOAL5',
						SOAL6='$SOAL6',
						SOAL7='$SOAL7',
						SOAL8='$SOAL8',
						SOAL9='$SOAL9',
						SOAL10='$SOAL10',
						SOAL11='$SOAL11',
						DIUBAH_OLEH='$USER'
						where ID='$ID'";
						// echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_transcovid'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_transfercovid ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_transcovid'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_transcovid'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			