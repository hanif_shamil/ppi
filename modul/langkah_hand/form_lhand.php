<script type="text/javascript">
	function tugas1()
	{
		var jumlah=0;
		var jumlahx=0;
//var nilaix;
		var nilai;

		if(document.getElementById("RATAHANDRUB").checked)
		{
			nilai=document.getElementById("RATAHANDRUB").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("RATAHANDRUBTDK").checked)
		{
			nilai=document.getElementById("RATAHANDRUBTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}

		if(document.getElementById("RATAHANDRUB2").checked)
		{
			nilai=document.getElementById("RATAHANDRUB2").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("RATAHANDRUBTDK2").checked)
		{
			nilai=document.getElementById("RATAHANDRUBTDK2").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}

		if(document.getElementById("GOSOKPUNGGUNG").checked)
		{
			nilai=document.getElementById("GOSOKPUNGGUNG").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("GOSOKPUNGGUNGTDK").checked)
		{
			nilai=document.getElementById("GOSOKPUNGGUNGTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}

		if(document.getElementById("GOSOKTAPAK").checked)
		{
			nilai=document.getElementById("GOSOKTAPAK").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("GOSOKTAPAKTDK").checked)
		{
			nilai=document.getElementById("GOSOKTAPAKTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("GOSOKJARI").checked)
		{
			nilai=document.getElementById("GOSOKJARI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("GOSOKJARITDK").checked)
		{
			nilai=document.getElementById("GOSOKJARITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("GOSOKJARIKIRI").checked)
		{
			nilai=document.getElementById("GOSOKJARIKIRI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("GOSOKJARIKIRITDK").checked)
		{
			nilai=document.getElementById("GOSOKJARIKIRITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("USAPKUKU").checked)
		{
			nilai=document.getElementById("USAPKUKU").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("USAPKUKUTDK").checked)
		{
			nilai=document.getElementById("USAPKUKUTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}

		document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
	}
</script>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Audit 6 langkah kebersihan tangan</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Form Audit 6 langkah kebersihan tangan</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_lhand" method="post">		
									<div class="form-group">										
										<div class="col-sm-3">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tanggal survey :</label>
												<div class="input-group">
													<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>	
										</div>
										
										<div class="col-sm-9">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Jenis petugas kesehatan :</label>
												<div class="radio">
													<label>
														<input name="PETUGAS[]" type="radio" value="11" class="ace" />
														<span class="lbl"> LAB</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="12" type="radio" class="ace" />
														<span class="lbl"> DR</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="13" class="ace" type="radio" />
														<span class="lbl"> PWT</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="14" class="ace" type="radio" />
														<span class="lbl"> POS</span>
													</label>
													<label>
														<input name="PETUGAS[]" type="radio" value="15" class="ace" />
														<span class="lbl"> TH</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="16" type="radio" class="ace" />
														<span class="lbl"> M. KEP</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="17" class="ace" type="radio" />
														<span class="lbl"> M. KED</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="18" class="ace" type="radio" />
														<span class="lbl"> DLL</span>
													</label>
												</div>									
											</div>
										</div>
									</div>
									<hr />									
									<div class="form-group">																				
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">6 Langkah kebersihan tangan :</label>
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> 6 LANGKAH CUCI TANGAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>Mengambil handrub sebanyak 3-5 cc dengan punggung tangan</td>
															<td align="center"><label>
																<input type="radio" name="RATAHANDRUB[]" id="RATAHANDRUB" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>

															<td align="center">
																<label>
																	<input name="RATAHANDRUB[]" type="radio" value="2" id="RATAHANDRUBTDK" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>	
														<tr>
															<td>2</td>
															<td>Ratakan handrub dengan menggosokkan pada dua tangan 4 hitungan</td>
															<td align="center"><label>
																<input type="radio" name="RATAHANDRUB2[]" id="RATAHANDRUB2" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>

															<td align="center">
																<label>
																	<input name="RATAHANDRUB2[]" type="radio" value="2" id="RATAHANDRUBTDK2" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>														
														<tr>
															<td>3</td>
															<td>Gosok punggung tangan dan sela-sela jari 4 hitungan</td>
															<td align="center">
																<label>
																	<input type="radio" name="GOSOKPUNGGUNG[]" id="GOSOKPUNGGUNG" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="GOSOKPUNGGUNG[]" id="GOSOKPUNGGUNGTDK" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Gosokkan kedua telapak dan sela-sela jari kedua tangan 4 hitungan</td>
															<td align="center">
																<label>
																	<input name="GOSOKTAPAK[]" type="radio" value="1" id="GOSOKTAPAK" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="GOSOKTAPAK[]" type="radio" value="2" id="GOSOKTAPAKTDK" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>5</td>
															<td>Gosok punggung jari tangan dengan posisi kedua tangan saling mengunci 4 hitungan</td>
															<td align="center">
																<label>
																	<input name="GOSOKJARI[]" type="radio" value="1" id="GOSOKJARI" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="GOSOKJARI[]" type="radio" value="2" id="GOSOKJARITDK" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Gosok ibu jari tangan kiri dengan diputar dalam genggaman tangan kanan, laukan pula sebaliknya, 4 hitungan</td>
															<td align="center">
																<label>
																	<input name="GOSOKJARIKIRI[]" type="radio" value="1" id="GOSOKJARIKIRI" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="GOSOKJARIKIRI[]" type="radio" value="2" id="GOSOKJARIKIRITDK" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>7</td>
															<td>Usapkan ujung kuku tangan kanan dengan diputar di telapak tangan kiri, lakukan juga pada tangan satunya, 4 hitungan</td>
															<td align="center">
																<label>
																	<input name="USAPKUKU[]" type="radio" value="1" id="USAPKUKU" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="USAPKUKU[]" type="radio" value="2" id="USAPKUKUTDK" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>												
														<tr>
															<td style="text-align:center" colspan="2">NILAI KEPATUHAN</td>
															<td colspan="3" style="text-align:center">
																<div class="input-group">
																	<input type="text" id="total" name="NILAI" placeholder="[ NILAI ]" class="form-control" />
																	<span class="input-group-addon">
																		%
																	</span>
																</div>													
															</td>														
														</tr>													
													</tbody>
												</table>																									
											</div>
										</div>
									</div>																										

									<div class="form-group">										
										<div class="col-xs-12 col-sm-6">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Auditee :</label>
												<input type="text" id="form-field-1-1" name="AUDITEE" placeholder="[ AUDITEE ]" class="form-control" />
											</div>	

										</div>

										<div class="col-xs-12 col-sm-6">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Auditor :</label>
												<input type="text" id="form-field-1-1" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
											</div>	
										</div>											
									</div>																		<div class="form-group">										
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
											<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
										</div>				
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
											<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
										</div>
									</div>
									<div class="form-group">			
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
											<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />					
										</div>							
									</div>											

									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div>