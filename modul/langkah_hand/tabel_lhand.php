<?php
function serialize_ke_string($serial)
{
    $hasil = unserialize($serial);
    return implode(', ', $hasil);
}
?>

<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Tables</a>
		</li>
		<li class="active">Laporan 6 Langkah kebersihan tangan</li>
	</ul><!-- /.breadcrumb -->

	<!--<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="ace-icon fa fa-search nav-search-icon"></i>
			</span>
		</form>
	</div><!-- /.nav-search -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="table-header">
						Laporan 6 Langkah kebersihan tangan
					</div>

					<!-- div.table-responsive -->

					<!-- div.dataTables_borderWrap -->
					<div>
						<table id="tblhand" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr align="center">
									<th rowspan="2" width="10%"><div align="center">Tanggal</div></th>
									
									<th rowspan="2" width="9%"><div align="center">Profesi</div></th>
									<th colspan="6"><div align="center">6 Langkah kebersihan tangan</div></th>
									<th rowspan="2" width="7%"><div align="center">Nilai</div></th>									
									<th rowspan="2" width="7%"><div align="center">Opsi</div></th>
								</tr>
								<tr>
								<td align="center" width="9%">Langkah 1</td>
								<td align="center" width="8%">Langkah 2</td>
								<td align="center" width="13%">Langkah 3</td>
								<td align="center" width="8%">Langkah 4</td>
								<td align="center" width="10%">Langkah 5</td>
								<td align="center" width="10%">Langkah 6</td>
								
								</tr>
							</thead>

							
						</table>
					</div>
				</div>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

