<html>
<head>
	<script src="assets/sweetalert/dist/sweetalert-dev.js"></script>
	<link rel="stylesheet" href="assets/sweetalert/dist/sweetalert.css">

</head>
<body>

	<?php
	date_default_timezone_set('Asia/Jakarta');
	if(isset($_POST['btnEdit'])){
		$ID_LHAND 			= $_POST['ID_LHAND'];	
		$TANGGAL 			= $_POST['TANGGAL'];	
		$TANGGAL 			= $_POST['TANGGAL'];									
		$AUDITEE 			= $_POST['AUDITEE'];
		$ANALISA 			=	$_POST['ANALISA'];  
		$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
		$KEPALA 			=	$_POST['KEPALA'];
		$AUDITOR 			= addslashes($_POST['AUDITOR']);
		$NILAI 				= $_POST['NILAI'];
		if(isset($_POST['PETUGAS'])){
			$PETUGAS = implode(',',$_POST['PETUGAS']);
		}else{
			$PETUGAS = "";
		}						
		if(isset($_POST['RATAHANDRUB'])){
			$RATAHANDRUB = implode(',',$_POST['RATAHANDRUB']);
		}else{
			$RATAHANDRUB = "";
		}
		if(isset($_POST['RATAHANDRUB2'])){
			$RATAHANDRUB2 = implode(',',$_POST['RATAHANDRUB2']);
		}else{
			$RATAHANDRUB2 = "";
		}
		if(isset($_POST['GOSOKPUNGGUNG'])){
			$GOSOKPUNGGUNG = implode(',',$_POST['GOSOKPUNGGUNG']);
		}else{
			$GOSOKPUNGGUNG = "";
		}
		if(isset($_POST['GOSOKTAPAK'])){
			$GOSOKTAPAK = implode(',',$_POST['GOSOKTAPAK']);
		}else{
			$GOSOKTAPAK = "";
		}
		if(isset($_POST['GOSOKJARI'])){
			$GOSOKJARI = implode(',',$_POST['GOSOKJARI']);
		}else{
			$GOSOKJARI = "";
		}
		if(isset($_POST['GOSOKJARIKIRI'])){
			$GOSOKJARIKIRI = implode(',',$_POST['GOSOKJARIKIRI']);
		}else{
			$GOSOKJARIKIRI = "";
		}		
		if(isset($_POST['USAPKUKU'])){
			$USAPKUKU = implode(',',$_POST['USAPKUKU']);
		}else{
			$USAPKUKU = "";
		}					
		$pengisi 			= $_SESSION['userid'];					
		$waktu = date("Y-m-d H:i:s");							
//echo "<pre>";print_r($_POST);exit();                											
		$query2 = "UPDATE db_ppi.tb_lang_hand SET						
		TANGGAL='$TANGGAL',
		PETUGAS='$PETUGAS',
		RATAHANDRUB='$RATAHANDRUB',		
		RATAHANDRUB2='$RATAHANDRUB2',			
		GOSOKPUNGGUNG='$GOSOKPUNGGUNG',				
		GOSOKTAPAK='$GOSOKTAPAK',					
		GOSOKJARI='$GOSOKJARI',					
		GOSOKJARIKIRI='$GOSOKJARIKIRI',					
		USAPKUKU='$USAPKUKU',
		NILAI='$NILAI',
		AUDITEE='$AUDITEE',
		ANALISA 		= '$ANALISA', 
		TINDAKLANJUT 	= '$TINDAKLANJUT', 
		KEPALA 			= '$KEPALA',
		AUDITOR='$AUDITOR',
		TGL_UBAH='$waktu', 
		PENGUBAH='$pengisi'
		where ID_LHAND='$ID_LHAND'";
//echo $query2; die();	
		$insert=mysqli_query($conn1,$query2);									
		if($insert){
			echo '<script>
			swal({
				html: true,
				title: "Sukses",
				text: "Laporan Berhasil diubah!",
				type: "success",
				confirmButtonText: "OK",
				closeOnConfirm: false,
				closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href="index.php?page=tabel_lhand";
					} });</script>'; 
				}
			}
			if(isset($_POST['btnVerif'])){
				$id_isian = $_POST['ID_LHAND'];									
//echo "<pre>";print_r($_POST);exit();                
				$query3 ="UPDATE db_ppi.tb_lang_hand SET VERIF='2' where ID_LHAND='$id_isian'";
				$verf=mysqli_query($conn1,$query3);
//echo $query3; die();									
				if($verf){
					echo '<script>
					swal({
						html: true,
						title: "Sukses",
						text: "Data sudah diverifikasi",
						type: "success",
						confirmButtonText: "Lihat Inputan",
						closeOnConfirm: false,
						closeOnCancel: false
						},
						function(isConfirm) {
							if (isConfirm) {
								window.location.href="index.php?page=tabel_lhand";
							} });</script>'; 	
						}
					} 						

					?>
				</body>
				</html>	