	<script type="text/javascript">
		function tugas1()
		{
			var jumlah=0;
			var nilai;


			if(document.getElementById("BERSIH_TNGN").checked)
			{
				nilai=document.getElementById("BERSIH_TNGN").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("POSISI_KPLA").checked)
			{
				nilai=document.getElementById("POSISI_KPLA").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("MIRING").checked)
			{
				nilai=document.getElementById("MIRING").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("ANTISEPTIC").checked)
			{
				nilai=document.getElementById("ANTISEPTIC").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("NGT").checked)
			{
				nilai=document.getElementById("NGT").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("SUCTION").checked)
			{
				nilai=document.getElementById("SUCTION").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("SIKAT").checked)
			{
				nilai=document.getElementById("SIKAT").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("UCLER").checked)
			{
				nilai=document.getElementById("UCLER").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("DVT").checked)
			{
				nilai=document.getElementById("DVT").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("SEDASI").checked)
			{
				nilai=document.getElementById("SEDASI").value;
				jumlah=jumlah+parseInt(nilai);
			}
			if(document.getElementById("CONTROL").checked)
			{
				nilai=document.getElementById("CONTROL").value;
				jumlah=jumlah+parseInt(nilai);
			}
			document.getElementById("total").value=jumlah/11*100;
		}
	</script>
	<?php
	$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
	$query = "SELECT max(ID_ISI) AS last FROM tb_vap WHERE ID_ISI LIKE '$today%'";
	$hasil = mysqli_query($conn1,$query);
	$data  = mysqli_fetch_assoc($hasil);
	$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
	$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
	$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
	$nextID = $today.sprintf('%04s', $nextNoUrut);
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">Formulir Audit Bundles VAP</li>
				</ul><!-- /.breadcrumb -->


			</div>

			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Formulir Audit Bundles VAP</h4>

								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>

									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_vap" method="post">
										<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
										<div class="form-group">										
											<div class="col-sm-2">
												<div class="input-group">
													<input class="form-control" id="datetimepicker1" value="<?php echo date('Y/m/d H:i:s') ?>" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-3">										
												<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ]">
													<option value=""></option>
													<?php
													$sql = "select * from db_ppi.ruangan r
													where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
													$rs = mysqli_query($conn1,$sql);
													while ($data = mysqli_fetch_array($rs)) {
														?>
														<option value="<?=$data[0]?>"><?=$data[3]?></option>
														<?php
													}
													?>	
												</select>
											</div>	
											<div class="col-sm-2">
												<input type="text" id="nomr" name="NOMR" placeholder="[ MR ]" autocomplete="off" class="form-control" onkeyup="autofill()" />												
											</div>	
											<div class="col-sm-5">									
												<input type="text" id="nama" name="NAMA" placeholder="[ NAMA ]" autocomplete="off" class="form-control" />
											</div>																			

										</div>															
										<hr />

										<div class="form-group">
											<div class="col-sm-12">											
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="text-align:center">NO</th>
															<th style="text-align:center"> BUNDLES VAP</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>Melakukan kebersihan tangan</td>
															<td align="center"><label>
																<input name="BERSIH_TNGN[]" type="checkbox" id="BERSIH_TNGN" value="1" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="BERSIH_TNGN[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Memberikan posisi kepala ditinggikan 30-45° (bila tidak ada kontra indikasi, seperti fr.servikal)</td>
															<td align="center"><label>
																<input name="POSISI_KPLA[]" type="checkbox" id="POSISI_KPLA" value="1" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label></td>
															<td align="center"><label>
																<input name="POSISI_KPLA[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																<span class="lbl"></span>
															</label></td>
														</tr>
														<tr>
															<td>3</td>
															<td>Melakukan mobilisasi miring kiri dan kanan setiap 2-4 jam dan lakukan chest terapi (bila tidak ada kontra indikasi)</td>
															<td align="center">
																<label>
																	<input name="MIRING[]" type="checkbox" id="MIRING" value="1" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label></td>
																<td align="center">
																	<label>
																		<input name="MIRING[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																		<span class="lbl"></span>
																	</label></td>
																</tr>
																<tr>
																	<td>4</td>
																	<td>Melakukan oral hygiene dengan antiseptic chlorhexidin 0,2% tiap 4– 6 jam sekali</td>
																	<td align="center">
																		<label>
																			<input name="ANTISEPTIC[]" type="checkbox" id="ANTISEPTIC" value="1" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td align="center">
																		<label>
																			<input name="ANTISEPTIC[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																</tr>
																<tr>
																	<td>5</td>
																	<td>Sikat gigi tiap 12 jam</td>
																	<td align="center">
																		<label>
																			<input name="SIKAT[]" type="checkbox" id="SIKAT" value="1" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td align="center">
																		<label>
																			<input name="SIKAT[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																</tr>
																<tr>
																	<td>6</td>
																	<td>Memberi makan per NGT per drip</td>
																	<td align="center">
																		<label>
																			<input name="NGT[]" type="checkbox" id="NGT" value="1" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td align="center">
																		<label>
																			<input name="NGT[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																</tr>
																<tr>
																	<td>7</td>
																	<td>Melakukan suction dengan close suction system</td>
																	<td align="center">
																		<label>
																			<input name="SUCTION[]" type="checkbox" id="SUCTION" value="1" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td align="center">
																		<label>
																			<input name="SUCTION[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																</tr>
																<tr>
																	<td>8</td>
																	<td>Mereview sedasi untuk wining</td>
																	<td align="center">
																		<label>
																			<input name="SEDASI[]" type="checkbox" id="SEDASI" value="1" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td align="center">
																		<label>
																			<input name="SEDASI[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																</tr>
																<tr>
																	<td>9</td>
																	<td>Pemberian Peptic ucler profilaksis</td>
																	<td align="center">
																		<label>
																			<input name="UCLER[]" type="checkbox" id="UCLER" value="1" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td align="center">
																		<label>
																			<input name="UCLER[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																</tr>
																<tr>
																	<td>10</td>
																	<td>Pemberian DVT profilaksis</td>
																	<td align="center">
																		<label>
																			<input name="DVT[]" type="checkbox" id="DVT" value="1" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td align="center">
																		<label>
																			<input name="DVT[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																</tr>

																<tr>
																	<td>11</td>
																	<td>Control cuff / 24 jam</td>
																	<td align="center">
																		<label>
																			<input name="CONTROL[]" type="checkbox" id="CONTROL" value="1" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td align="center">
																		<label>
																			<input name="CONTROL[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																</tr>
																<tr>
																	<td style="text-align:center" colspan="2">Total</td>
																	<td style="text-align:center"></td>
																	<td style="text-align:center"></td>
																</tr>
																<tr>
																	<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
																	<td colspan="2" style="text-align:center">
																		<div class="input-group">
																			<input name="TOTAL" type="text" id="total" style="width:60px"></br>
																			<span class="input-group-addon">
																				%
																			</span>
																		</div>														
																	</td>														
																</tr>													
															</tbody>
														</table>											
													</div>
												</div>													
												<div class="form-group">										
													<div class="col-md-6 col-sm-12">
														<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
														<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
													</div>				
													<div class="col-md-6 col-sm-12">
														<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
														<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
													</div>
												</div>
												<div class="form-group">			
													<div class="col-md-6 col-sm-12">
														<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
														<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
													</div>	
													<div class="col-md-6 col-sm-12">	
														<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
														<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
													</div>								
												</div>
												<hr>
												<div class="form-group">							
													<div class="col-md-offset-3 col-md-9">
														<button class="btn btn-info" name="btnsimpan" type="submit">
															<i class="ace-icon fa fa-check bigger-110"></i>
															Submit
														</button>
														&nbsp; &nbsp; &nbsp;
														<button class="btn" type="reset">
															%
															Reset
														</button>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div><!-- /.span -->
						</div>
					</div><!-- /.page-content -->	
				</div> <!-- container -->
</div><!-- /.main-content -->