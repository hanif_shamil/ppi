<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Data</a>
		</li>
		<li class="active">Laporan Modul VAP</li>
	</ul><!-- /.breadcrumb -->

	<!--<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="ace-icon fa fa-search nav-search-icon"></i>
			</span>
		</form>
	</div><!-- /.nav-search -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Data Laporan Modul VAP</h4>
							<div class="widget-toolbar">
								<a href="index.php?page=vap">
									<i class="ace-icon fa fa-plus" style="color:blue"> Tambah data</i>
								</a>
							</div>
						</div>
						<!-- div.table-responsive -->
						<!-- div.dataTables_borderWrap -->
						<table id="dataTable3" class="table table-striped table-bordered table-hover">
							<thead>
								<tr align="center">
									<th><div align="center">Tanggal</div></th>
									<th><div align="center">NOMR</div></th>
									<th><div align="center">Nama</div></th>
									<th><div align="center">Ruangan</div></th>
									<th><div align="center">Surveyor</div></th>
									<th><div align="center">Total (%)</div></th>
									<th><div align="center">Opsi</div></th>
								</tr>								
								
							</thead>
							<tbody>
								<?php					
								$query="select ti.ID_ISI, ti.TGL_INPUT, ti.TANGGAL, ti.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, ti.NOMR, ti.NAMA, ti.SURVEYOR,ti.BERSIH_TNGN
								, ti.POSISI_KPLA, ti.MIRING, ti.ANTISEPTIC, ti.NGT, ti.SUCTION, ti.SIKAT, ti.UCLER, ti.DVT, ti.SEDASI, ti.CONTROL, ti.USER, ti.TOTAL, ti.`STATUS`
								from tb_vap ti
								left join ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5
								where ti.`STATUS`=1";							
								$info=mysqli_query($conn1,$query); 
							//untuk penomoran data
							//$no=1;						
							//menampilkan data
								while($row=mysqli_fetch_array($info)){
									?>
									<tr>
										<td><?php echo $row['TANGGAL'] ?></td>
										<td><?php echo $row['NOMR'] ?></td>
										<td><?php echo $row['NAMA'] ?></td>
										<td><?php echo $row['RUANGAN'] ?></td>								
										<td><?php echo $row['SURVEYOR'] ?></td>
										<td><?php echo $row['TOTAL'] ?></td>						
										<td>
											<div class="hidden-sm hidden-xs action-buttons">
												<a class="btn btn-info btn-xs" href="?page=det_vap&id=<?php echo $row['ID_ISI']?>">
													<i class="ace-icon fa fa-search-plus bigger-130"></i>
												</a>
												<a class="btn btn-success btn-xs" href="?page=edit_vap&id=<?php echo $row['ID_ISI']?>">
													<i class="ace-icon fa fa-pencil bigger-130"></i>
												</a>
												<a class="btn btn-danger btn-xs" href="?page=simpan_vap&id=<?php echo $row['ID_ISI']?>">
													<i class="ace-icon fa fa-trash-o bigger-130"></i>
												</a>
											</div>
										</td>
									</tr>							
									<?php							
								}
								?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->

