<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$SURVEYOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['BERSIH_TNGN'])){
							$BERSIH_TNGN = implode(',',$_POST['BERSIH_TNGN']);
						}else{
							$BERSIH_TNGN = "";
						}	
						if(isset($_POST['POSISI_KPLA'])){
							$POSISI_KPLA = implode(',',$_POST['POSISI_KPLA']);
						}else{
							$POSISI_KPLA = "";
						}
						if(isset($_POST['MIRING'])){
							$MIRING = implode(',',$_POST['MIRING']);
						}else{
							$MIRING = "";
						}
						if(isset($_POST['ANTISEPTIC'])){
							$ANTISEPTIC = implode(',',$_POST['ANTISEPTIC']);
						}else{
							$ANTISEPTIC = "";
						}
						if(isset($_POST['NGT'])){
							$NGT = implode(',',$_POST['NGT']);
						}else{
							$NGT = "";
						}
						if(isset($_POST['SUCTION'])){
							$SUCTION = implode(',',$_POST['SUCTION']);
						}else{
							$SUCTION = "";
						}
						if(isset($_POST['SIKAT'])){
							$SIKAT = implode(',',$_POST['SIKAT']);
						}else{
							$SIKAT = "";
						}
						if(isset($_POST['UCLER'])){
							$UCLER = implode(',',$_POST['UCLER']);
						}else{
							$UCLER = "";
						}
						if(isset($_POST['DVT'])){
							$DVT = implode(',',$_POST['DVT']);
						}else{
							$DVT = "";
						}
						if(isset($_POST['SEDASI'])){
							$SEDASI = implode(',',$_POST['SEDASI']);
						}else{
							$SEDASI = "";
						}
						if(isset($_POST['CONTROL'])){
							$CONTROL = implode(',',$_POST['CONTROL']);
						}else{
							$CONTROL = "";
						}
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                        						
						$query = "INSERT INTO db_ppi.tb_vap (
							ID_ISI,
							TANGGAL,
							RUANGAN,
							NOMR,
							NAMA,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							SURVEYOR,
							BERSIH_TNGN,
							POSISI_KPLA,
							MIRING,
							ANTISEPTIC,
							NGT,
							SUCTION,
							SIKAT, 
							UCLER,
							DVT,
							SEDASI,
							CONTROL,
							TOTAL,
							USER,
							STATUS) 
						VALUES 
						('$ID_ISI',
							'$TANGGAL',
							'$RUANGAN',
							'$NOMR',
							'$NAMA',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$SURVEYOR',
							'$BERSIH_TNGN',
							'$POSISI_KPLA',
							'$MIRING',
							'$ANTISEPTIC',
							'$NGT',
							'$SUCTION',
							'$SIKAT', 
							'$UCLER',
							'$DVT',
							'$SEDASI',
							'$CONTROL',
							'$TOTAL',
							'$USER',												
							'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=vap'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA']; 
						$SURVEYOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['BERSIH_TNGN'])){
							$BERSIH_TNGN = implode(',',$_POST['BERSIH_TNGN']);
						}else{
							$BERSIH_TNGN = "";
						}	
						if(isset($_POST['POSISI_KPLA'])){
							$POSISI_KPLA = implode(',',$_POST['POSISI_KPLA']);
						}else{
							$POSISI_KPLA = "";
						}
						if(isset($_POST['MIRING'])){
							$MIRING = implode(',',$_POST['MIRING']);
						}else{
							$MIRING = "";
						}
						if(isset($_POST['ANTISEPTIC'])){
							$ANTISEPTIC = implode(',',$_POST['ANTISEPTIC']);
						}else{
							$ANTISEPTIC = "";
						}
						if(isset($_POST['NGT'])){
							$NGT = implode(',',$_POST['NGT']);
						}else{
							$NGT = "";
						}
						if(isset($_POST['SUCTION'])){
							$SUCTION = implode(',',$_POST['SUCTION']);
						}else{
							$SUCTION = "";
						}
						if(isset($_POST['SIKAT'])){
							$SIKAT = implode(',',$_POST['SIKAT']);
						}else{
							$SIKAT = "";
						}
						if(isset($_POST['UCLER'])){
							$UCLER = implode(',',$_POST['UCLER']);
						}else{
							$UCLER = "";
						}
						if(isset($_POST['DVT'])){
							$DVT = implode(',',$_POST['DVT']);
						}else{
							$DVT = "";
						}
						if(isset($_POST['SEDASI'])){
							$SEDASI = implode(',',$_POST['SEDASI']);
						}else{
							$SEDASI = "";
						}
						if(isset($_POST['CONTROL'])){
							$CONTROL = implode(',',$_POST['CONTROL']);
						}else{
							$CONTROL = "";
						}
						$USER 			= $_SESSION['userid'];	
						$TOTAL	 		= $_POST['TOTAL'];				
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   					
						$query2 = "UPDATE db_ppi.tb_vap SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						ANALISA = '$ANALISA', 
						TINDAKLANJUT = '$TINDAKLANJUT', 
						KEPALA = '$KEPALA',
						SURVEYOR='$SURVEYOR',
						NOMR='$NOMR',
						NAMA='$NAMA',
						BERSIH_TNGN='$BERSIH_TNGN',
						POSISI_KPLA='$POSISI_KPLA',
						MIRING='$MIRING',
						ANTISEPTIC='$ANTISEPTIC',
						NGT='$NGT',
						SUCTION='$SUCTION',
						SIKAT='$SIKAT', 
						UCLER='$UCLER',
						DVT='$DVT',
						SEDASI='$SEDASI',
						CONTROL='$CONTROL',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_vap'</script>"; 
						}
					}                        
					$id = $_GET['id'];
					if(isset($_GET['id'])){						
						$query1 = "UPDATE db_ppi.tb_vap ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_vap'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_isk'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			