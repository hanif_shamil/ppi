<script type="text/javascript">
function tugas1()
{
var jumlah=0;
var nilai;


if(document.getElementById("BERSIH_TNGN").checked)
{
nilai=document.getElementById("BERSIH_TNGN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("MNGNAKAN_APD").checked)
{
nilai=document.getElementById("MNGNAKAN_APD").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("PREPARASI").checked)
{
nilai=document.getElementById("PREPARASI").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("INFUS_TEPAT").checked)
{
nilai=document.getElementById("INFUS_TEPAT").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("MONITORING").checked)
{
nilai=document.getElementById("MONITORING").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("TRANSPARAN").checked)
{
nilai=document.getElementById("TRANSPARAN").value;
jumlah=jumlah+parseInt(nilai);
}
document.getElementById("total").value=jumlah/6*100;
}
</script>
<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISI) AS last FROM tb_iadp WHERE ID_ISI LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Audit Bundles IADP</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Audit Bundles IADP</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_iadp" method="post">
								<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data[0]?>"><?=$data[3]?></option>
												<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-2">
											<input type="text" id="nomr" name="NOMR" placeholder="[ MR ]" autocomplete="off" class="form-control" onkeyup="autofill()" />												
										</div>	
										<div class="col-sm-3">									
											<input type="text" id="nama" name="NAMA" placeholder="[ NAMA ]" autocomplete="off" class="form-control" />												
										</div>																			
										<div class="col-sm-2">											
											<input type="text" name="SURVEYOR" placeholder="[ SURVEYOR ]" class="form-control" />												
										</div>
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> BUNDLES IADP</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Melakukan kebersihan tangan</td>
														<td align="center"><label>
															<input type="checkbox" name="BERSIH_TNGN[]" id="BERSIH_TNGN" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
															</label></td>
														<td align="center">
															<label>
															<input name="BERSIH_TNGN[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Menggunakan APD dengan tepat dan benar</td>
														<td align="center"><label>
														<input type="checkbox" name="MNGNAKAN_APD[]" id="MNGNAKAN_APD" value="1" class="ace input-lg" onClick="tugas1()">													
													<span class="lbl"></span>
												</label></td>
														<td align="center"><label>
													<input name="MNGNAKAN_APD[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Melakukan preparasi kulit dengan chlorhexidine alkohol</td>
														<td align="center">
															<label>
															<input type="checkbox" name="PREPARASI[]" id="PREPARASI" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="PREPARASI[]" type="checkbox" value="2" id="1" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Pemilihan vena pemasangan infus yang tepat</td>
														<td align="center">
															<label>
															<input name="INFUS_TEPAT[]" type="checkbox" value="1" id="INFUS_TEPAT" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="INFUS_TEPAT[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Melakukan monitoring</td>
														<td align="center">
															<label>
															<input name="MONITORING[]" type="checkbox" value="1" id="MONITORING" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="MONITORING[]" type="checkbox" value="2" id="1" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>MENGGUNAKAN TRANSPARAN DRESSING</td>
														<td align="center">
															<label>
															<input name="TRANSPARAN[]" type="checkbox" value="1" id="TRANSPARAN" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="TRANSPARAN[]" type="checkbox" value="2" id="1" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
														<div class="input-group">
															<input name="TOTAL" type="text" id="total" style="width:60px"></br>
															<span class="input-group-addon">
																%
															</span>
														</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													

									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->