<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$SURVEYOR 		= $_POST['SURVEYOR'];
						if(isset($_POST['BERSIH_TNGN'])){
							$BERSIH_TNGN = implode(',',$_POST['BERSIH_TNGN']);
						}else{
							$BERSIH_TNGN = "";
						}		
						if(isset($_POST['MNGNAKAN_APD'])){
							$MNGNAKAN_APD = implode(',',$_POST['MNGNAKAN_APD']);
						}else{
							$MNGNAKAN_APD = "";
						}	
						if(isset($_POST['PREPARASI'])){
							$PREPARASI = implode(',',$_POST['PREPARASI']);
						}else{
							$PREPARASI = "";
						}	
						if(isset($_POST['INFUS_TEPAT'])){
							$INFUS_TEPAT = implode(',',$_POST['INFUS_TEPAT']);
						}else{
							$INFUS_TEPAT = "";
						}	
						if(isset($_POST['MONITORING'])){
							$MONITORING = implode(',',$_POST['MONITORING']);
						}else{
							$MONITORING = "";
						}	
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_iadp (
						ID_ISI,
						TANGGAL,
						RUANGAN,
						SURVEYOR,
						NOMR,
						NAMA,
						BERSIH_TNGN,
						MNGNAKAN_APD,
						PREPARASI,
						INFUS_TEPAT,
						MONITORING,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$RUANGAN',
						'$SURVEYOR',
						'$NOMR',
						'$NAMA',
						'$BERSIH_TNGN',
						'$MNGNAKAN_APD',	
						'$PREPARASI',		
						'$INFUS_TEPAT',				
						'$MONITORING',	
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=iadp'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$SURVEYOR 		= $_POST['SURVEYOR'];
						if(isset($_POST['BERSIH_TNGN'])){
							$BERSIH_TNGN = implode(',',$_POST['BERSIH_TNGN']);
						}else{
							$BERSIH_TNGN = "";
						}		
						if(isset($_POST['MNGNAKAN_APD'])){
							$MNGNAKAN_APD = implode(',',$_POST['MNGNAKAN_APD']);
						}else{
							$MNGNAKAN_APD = "";
						}	
						if(isset($_POST['PREPARASI'])){
							$PREPARASI = implode(',',$_POST['PREPARASI']);
						}else{
							$PREPARASI = "";
						}	
						if(isset($_POST['INFUS_TEPAT'])){
							$INFUS_TEPAT = implode(',',$_POST['INFUS_TEPAT']);
						}else{
							$INFUS_TEPAT = "";
						}	
						if(isset($_POST['MONITORING'])){
							$MONITORING = implode(',',$_POST['MONITORING']);
						}else{
							$MONITORING = "";
						}	
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_iadp SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						SURVEYOR='$SURVEYOR',
						NOMR='$NOMR',
						NAMA='$NAMA',
						BERSIH_TNGN='$BERSIH_TNGN',
						MNGNAKAN_APD='$MNGNAKAN_APD',
						PREPARASI='$PREPARASI',
						INFUS_TEPAT='$INFUS_TEPAT',
						MONITORING='$MONITORING',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_iadp'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_iadp ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_iadp'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			