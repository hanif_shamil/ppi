<script type="text/javascript">
function tugas1()
{
var jumlah=0;
var nilai;


if(document.getElementById("BERSIH_TNGN").checked)
{
nilai=document.getElementById("BERSIH_TNGN").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("MNGNAKAN_APD").checked)
{
nilai=document.getElementById("MNGNAKAN_APD").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("PREPARASI").checked)
{
nilai=document.getElementById("PREPARASI").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("INFUS_TEPAT").checked)
{
nilai=document.getElementById("INFUS_TEPAT").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("MONITORING").checked)
{
nilai=document.getElementById("MONITORING").value;
jumlah=jumlah+parseInt(nilai);
}
document.getElementById("total").value=jumlah/5*100;
}
</script>
<?php
$id=($_GET['id']);
$query="select ti.ID_ISI, ti.TGL_INPUT, ti.TANGGAL, ti.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, ti.NOMR, ti.NAMA
, ti.SURVEYOR, ti.BERSIH_TNGN, ti.MNGNAKAN_APD, ti.PREPARASI, ti.INFUS_TEPAT, ti.MONITORING, ti.TOTAL, ti.`STATUS`
from tb_iadp ti
left join ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5							
where ti.ID_ISI='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$BERSIH_TNGN = explode(",", $dt['BERSIH_TNGN']);$MNGNAKAN_APD = explode(",", $dt['MNGNAKAN_APD']);$PREPARASI = explode(",", $dt['PREPARASI']);
$INFUS_TEPAT = explode(",", $dt['INFUS_TEPAT']);$MONITORING = explode(",", $dt['MONITORING']);
  {
	  ?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Audit Bundles IADP</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Audit Bundles IADP</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_iadp" method="post">
								<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $dt['ID_ISI']; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TANGGAL']; ?>" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" value="<?php echo $dt['RUANGAN']; ?>" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ID_RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
												<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-1">
											<input type="text" id="nomr" name="NOMR" value="<?php echo $dt['NOMR']; ?>" autocomplete="off" placeholder="[ MR ]" class="form-control" onkeyup="autofill()" />												
										</div>	
										<div class="col-sm-3">									
											<input type="text" id="nama" name="NAMA" value="<?php echo $dt['NAMA']; ?>" autocomplete="off" placeholder="[ NAMA ]" class="form-control" />												
										</div>																			
										<div class="col-sm-3">											
											<input type="text" name="SURVEYOR" value="<?php echo $dt['SURVEYOR']; ?>" placeholder="[ SURVEYOR ]" class="form-control" />												
										</div>
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> BUNDLES IDAP</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Melakukan kebersihan tangan</td>
														<td align="center"><label>
															<input type="checkbox" name="BERSIH_TNGN[]" <?php if(in_array("1", $BERSIH_TNGN)){ echo " checked=\"checked\""; } ?> id="BERSIH_TNGN" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
															</label></td>
														<td align="center">
															<label>
															<input name="BERSIH_TNGN[]" type="checkbox" <?php if(in_array("2", $BERSIH_TNGN)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Menggunakan APD dengan tepat dan benar</td>
														<td align="center"><label>
														<input type="checkbox" name="MNGNAKAN_APD[]" <?php if(in_array("1", $MNGNAKAN_APD)){ echo " checked=\"checked\""; } ?> id="MNGNAKAN_APD" value="1" class="ace input-lg" onClick="tugas1()">													
													<span class="lbl"></span>
												</label></td>
														<td align="center"><label>
													<input name="MNGNAKAN_APD[]" type="checkbox" <?php if(in_array("2", $MNGNAKAN_APD)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Melakukan preparasi kulit dengan chlorhexidine alkohol</td>
														<td align="center">
															<label>
															<input type="checkbox" name="PREPARASI[]" <?php if(in_array("1", $PREPARASI)){ echo " checked=\"checked\""; } ?> id="PREPARASI" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="PREPARASI[]" type="checkbox" <?php if(in_array("2", $PREPARASI)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Pemilihan vena pemasangan infus yang tepat</td>
														<td align="center">
															<label>
															<input name="INFUS_TEPAT[]" type="checkbox" value="1" <?php if(in_array("1", $INFUS_TEPAT)){ echo " checked=\"checked\""; } ?> id="INFUS_TEPAT" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="INFUS_TEPAT[]" type="checkbox" value="2" <?php if(in_array("2", $INFUS_TEPAT)){ echo " checked=\"checked\""; } ?> id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Melakukan monitoring</td>
														<td align="center">
															<label>
															<input name="MONITORING[]" type="checkbox" value="1" <?php if(in_array("1", $MONITORING)){ echo " checked=\"checked\""; } ?> id="MONITORING" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="MONITORING[]" type="checkbox" value="2" <?php if(in_array("2", $MONITORING)){ echo " checked=\"checked\""; } ?> id="1" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
														<div class="input-group">
															<input name="TOTAL" value="<?php echo $dt['TOTAL']; ?>" type="text" id="total" style="width:60px"></br>
															<span class="input-group-addon">
																%
															</span>
														</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													
									
									
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnEdit" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->
<?php
  }?>