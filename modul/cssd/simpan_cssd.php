<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];						
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['LOKASI_TERPISAH'])){
							$LOKASI_TERPISAH = implode(',',$_POST['LOKASI_TERPISAH']);
						}else{
							$LOKASI_TERPISAH = "";
						}		
						if(isset($_POST['FASILITAS_TERSEDIA'])){
							$FASILITAS_TERSEDIA = implode(',',$_POST['FASILITAS_TERSEDIA']);
						}else{
							$FASILITAS_TERSEDIA = "";
						}	
						if(isset($_POST['PINTU_TERPISAH'])){
							$PINTU_TERPISAH = implode(',',$_POST['PINTU_TERPISAH']);
						}else{
							$PINTU_TERPISAH = "";
						}	
						if(isset($_POST['RUANG_GOWNING'])){
							$RUANG_GOWNING = implode(',',$_POST['RUANG_GOWNING']);
						}else{
							$RUANG_GOWNING = "";
						}	
						if(isset($_POST['DOKUMENTASI'])){
							$DOKUMENTASI = implode(',',$_POST['DOKUMENTASI']);
						}else{
							$DOKUMENTASI = "";
						}
						if(isset($_POST['SARANA_CUCI'])){
							$SARANA_CUCI = implode(',',$_POST['SARANA_CUCI']);
						}else{
							$SARANA_CUCI = "";
						}
						if(isset($_POST['STERILASI_VALID'])){
							$STERILASI_VALID = implode(',',$_POST['STERILASI_VALID']);
						}else{
							$STERILASI_VALID = "";
						}
						if(isset($_POST['SISTEM_FIFO'])){
							$SISTEM_FIFO = implode(',',$_POST['SISTEM_FIFO']);
						}else{
							$SISTEM_FIFO = "";
						}
						if(isset($_POST['LINEN_PISAH'])){
							$LINEN_PISAH = implode(',',$_POST['LINEN_PISAH']);
						}else{
							$LINEN_PISAH = "";
						}
						if(isset($_POST['SERAGAM_APD'])){
							$SERAGAM_APD = implode(',',$_POST['SERAGAM_APD']);
						}else{
							$SERAGAM_APD = "";
						}
						if(isset($_POST['PROTECTIVE'])){
							$PROTECTIVE = implode(',',$_POST['PROTECTIVE']);
						}else{
							$PROTECTIVE = "";
						}
						if(isset($_POST['MAKAN_MINUM'])){
							$MAKAN_MINUM = implode(',',$_POST['MAKAN_MINUM']);
						}else{
							$MAKAN_MINUM = "";
						}
						if(isset($_POST['TROLY_BEDA'])){
							$TROLY_BEDA = implode(',',$_POST['TROLY_BEDA']);
						}else{
							$TROLY_BEDA = "";
						}
						if(isset($_POST['LABEL_KIMIA'])){
							$LABEL_KIMIA = implode(',',$_POST['LABEL_KIMIA']);
						}else{
							$LABEL_KIMIA = "";
						}
						if(isset($_POST['PASSBOX'])){
							$PASSBOX = implode(',',$_POST['PASSBOX']);
						}else{
							$PASSBOX = "";
						}
						if(isset($_POST['INSTRUMEN_OPRASI'])){
							$INSTRUMEN_OPRASI = implode(',',$_POST['INSTRUMEN_OPRASI']);
						}else{
							$INSTRUMEN_OPRASI = "";
						}
						if(isset($_POST['INDICATOR_INSTRUMEN'])){
							$INDICATOR_INSTRUMEN = implode(',',$_POST['INDICATOR_INSTRUMEN']);
						}else{
							$INDICATOR_INSTRUMEN = "";
						}
						if(isset($_POST['KEMASAN_STERIL'])){
							$KEMASAN_STERIL = implode(',',$_POST['KEMASAN_STERIL']);
						}else{
							$KEMASAN_STERIL = "";
						}
						if(isset($_POST['RUANG_BERSIH'])){
							$RUANG_BERSIH = implode(',',$_POST['RUANG_BERSIH']);
						}else{
							$RUANG_BERSIH = "";
						}
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_cssd (
						ID_ISI,
						TANGGAL,
						RUANGAN,
						AUDITOR,
						LOKASI_TERPISAH,
						FASILITAS_TERSEDIA,
						PINTU_TERPISAH,
						RUANG_GOWNING,
						DOKUMENTASI,
						SARANA_CUCI,
						STERILASI_VALID,
						SISTEM_FIFO,
						LINEN_PISAH,
						SERAGAM_APD,
						PROTECTIVE,
						MAKAN_MINUM,
						TROLY_BEDA,
						LABEL_KIMIA,
						PASSBOX,
						INSTRUMEN_OPRASI,
						INDICATOR_INSTRUMEN,
						KEMASAN_STERIL,
						RUANG_BERSIH,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$RUANGAN',
						'$AUDITOR',
						'$LOKASI_TERPISAH',
						'$FASILITAS_TERSEDIA',	
						'$PINTU_TERPISAH',		
						'$RUANG_GOWNING',				
						'$DOKUMENTASI',	
						'$SARANA_CUCI',	
						'$STERILASI_VALID',	
						'$SISTEM_FIFO',	
						'$LINEN_PISAH',	
						'$SERAGAM_APD',	
						'$PROTECTIVE',	
						'$MAKAN_MINUM',	
						'$TROLY_BEDA',	
						'$LABEL_KIMIA',	
						'$PASSBOX',	
						'$INSTRUMEN_OPRASI',	
						'$INDICATOR_INSTRUMEN',	
						'$KEMASAN_STERIL',	
						'$RUANG_BERSIH',	
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_cssd'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['LOKASI_TERPISAH'])){
							$LOKASI_TERPISAH = implode(',',$_POST['LOKASI_TERPISAH']);
						}else{
							$LOKASI_TERPISAH = "";
						}		
						if(isset($_POST['FASILITAS_TERSEDIA'])){
							$FASILITAS_TERSEDIA = implode(',',$_POST['FASILITAS_TERSEDIA']);
						}else{
							$FASILITAS_TERSEDIA = "";
						}	
						if(isset($_POST['PINTU_TERPISAH'])){
							$PINTU_TERPISAH = implode(',',$_POST['PINTU_TERPISAH']);
						}else{
							$PINTU_TERPISAH = "";
						}	
						if(isset($_POST['RUANG_GOWNING'])){
							$RUANG_GOWNING = implode(',',$_POST['RUANG_GOWNING']);
						}else{
							$RUANG_GOWNING = "";
						}	
						if(isset($_POST['DOKUMENTASI'])){
							$DOKUMENTASI = implode(',',$_POST['DOKUMENTASI']);
						}else{
							$DOKUMENTASI = "";
						}
						if(isset($_POST['SARANA_CUCI'])){
							$SARANA_CUCI = implode(',',$_POST['SARANA_CUCI']);
						}else{
							$SARANA_CUCI = "";
						}
						if(isset($_POST['STERILASI_VALID'])){
							$STERILASI_VALID = implode(',',$_POST['STERILASI_VALID']);
						}else{
							$STERILASI_VALID = "";
						}
						if(isset($_POST['SISTEM_FIFO'])){
							$SISTEM_FIFO = implode(',',$_POST['SISTEM_FIFO']);
						}else{
							$SISTEM_FIFO = "";
						}
						if(isset($_POST['LINEN_PISAH'])){
							$LINEN_PISAH = implode(',',$_POST['LINEN_PISAH']);
						}else{
							$LINEN_PISAH = "";
						}
						if(isset($_POST['SERAGAM_APD'])){
							$SERAGAM_APD = implode(',',$_POST['SERAGAM_APD']);
						}else{
							$SERAGAM_APD = "";
						}
						if(isset($_POST['PROTECTIVE'])){
							$PROTECTIVE = implode(',',$_POST['PROTECTIVE']);
						}else{
							$PROTECTIVE = "";
						}
						if(isset($_POST['MAKAN_MINUM'])){
							$MAKAN_MINUM = implode(',',$_POST['MAKAN_MINUM']);
						}else{
							$MAKAN_MINUM = "";
						}
						if(isset($_POST['TROLY_BEDA'])){
							$TROLY_BEDA = implode(',',$_POST['TROLY_BEDA']);
						}else{
							$TROLY_BEDA = "";
						}
						if(isset($_POST['LABEL_KIMIA'])){
							$LABEL_KIMIA = implode(',',$_POST['LABEL_KIMIA']);
						}else{
							$LABEL_KIMIA = "";
						}
						if(isset($_POST['PASSBOX'])){
							$PASSBOX = implode(',',$_POST['PASSBOX']);
						}else{
							$PASSBOX = "";
						}
						if(isset($_POST['INSTRUMEN_OPRASI'])){
							$INSTRUMEN_OPRASI = implode(',',$_POST['INSTRUMEN_OPRASI']);
						}else{
							$INSTRUMEN_OPRASI = "";
						}
						if(isset($_POST['INDICATOR_INSTRUMEN'])){
							$INDICATOR_INSTRUMEN = implode(',',$_POST['INDICATOR_INSTRUMEN']);
						}else{
							$INDICATOR_INSTRUMEN = "";
						}
						if(isset($_POST['KEMASAN_STERIL'])){
							$KEMASAN_STERIL = implode(',',$_POST['KEMASAN_STERIL']);
						}else{
							$KEMASAN_STERIL = "";
						}
						if(isset($_POST['RUANG_BERSIH'])){
							$RUANG_BERSIH = implode(',',$_POST['RUANG_BERSIH']);
						}else{
							$RUANG_BERSIH = "";
						}	
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_cssd SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						AUDITOR='$AUDITOR',
						LOKASI_TERPISAH = '$LOKASI_TERPISAH',									
						FASILITAS_TERSEDIA = '$FASILITAS_TERSEDIA',													
						PINTU_TERPISAH = '$PINTU_TERPISAH',													
						RUANG_GOWNING = '$RUANG_GOWNING',																		
						DOKUMENTASI = '$DOKUMENTASI',													
						SARANA_CUCI = '$SARANA_CUCI',													
						STERILASI_VALID = '$STERILASI_VALID',						
						SISTEM_FIFO = '$SISTEM_FIFO',												
						LINEN_PISAH = '$LINEN_PISAH',												
						SERAGAM_APD = '$SERAGAM_APD',												
						PROTECTIVE = '$PROTECTIVE',												
						MAKAN_MINUM = '$MAKAN_MINUM',												
						TROLY_BEDA = '$TROLY_BEDA',						
						LABEL_KIMIA = '$LABEL_KIMIA',					
						PASSBOX = '$PASSBOX',					
						INSTRUMEN_OPRASI = '$INSTRUMEN_OPRASI',													
						INDICATOR_INSTRUMEN = '$INDICATOR_INSTRUMEN',						
						KEMASAN_STERIL = '$KEMASAN_STERIL',						
						RUANG_BERSIH = '$RUANG_BERSIH',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_cssd'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_cssd ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_cssd'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			