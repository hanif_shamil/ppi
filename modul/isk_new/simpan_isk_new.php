<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SATU'])){
							$SATU = implode(',',$_POST['SATU']);
						}else{
							$SATU = "";
						}		
						if(isset($_POST['DUA'])){
							$DUA = implode(',',$_POST['DUA']);
						}else{
							$DUA = "";
						}	
						if(isset($_POST['TIGA'])){
							$TIGA = implode(',',$_POST['TIGA']);
						}else{
							$TIGA = "";
						}	
						if(isset($_POST['EMPAT_A'])){
							$EMPAT_A = implode(',',$_POST['EMPAT_A']);
						}else{
							$EMPAT_A = "";
						}
						if(isset($_POST['EMPAT_B'])){
							$EMPAT_B = implode(',',$_POST['EMPAT_B']);
						}else{
							$EMPAT_B = "";
						}	
						if(isset($_POST['EMPAT_C'])){
							$EMPAT_C = implode(',',$_POST['EMPAT_C']);
						}else{
							$EMPAT_C = "";
						}	
						if(isset($_POST['EMPAT_D'])){
							$EMPAT_D = implode(',',$_POST['EMPAT_D']);
						}else{
							$EMPAT_D = "";
						}	
						if(isset($_POST['EMPAT_E'])){
							$EMPAT_E = implode(',',$_POST['EMPAT_E']);
						}else{
							$EMPAT_E = "";
						}							
						if(isset($_POST['LIMA_A'])){
							$LIMA_A = implode(',',$_POST['LIMA_A']);
						}else{
							$LIMA_A = "";
						}	
						if(isset($_POST['LIMA_B'])){
							$LIMA_B = implode(',',$_POST['LIMA_B']);
						}else{
							$LIMA_B = "";
						}	
						if(isset($_POST['ENAM'])){
							$ENAM = implode(',',$_POST['ENAM']);
						}else{
							$ENAM = "";
						}					
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_isk_new (
							TANGGAL,
							RUANGAN,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							AUDITOR,
							NOMR,
							NAMA,
							SATU,
							DUA,
							TIGA,
							EMPAT_A,
							EMPAT_B,
							EMPAT_C,
							EMPAT_D,
							EMPAT_E,
							LIMA_A,
							LIMA_B,
							ENAM,

							TOTAL,
							USER) 
						VALUES 
						('$TANGGAL',
							'$RUANGAN',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$AUDITOR',
							'$NOMR',
							'$NAMA',
							'$SATU',
							'$DUA',	
							'$TIGA',		
							'$EMPAT_A',
							'$EMPAT_B',
							'$EMPAT_C',
							'$EMPAT_D',
							'$EMPAT_E',						
							'$LIMA_A',
							'$LIMA_B',
							'$ENAM',

							'$TOTAL',
							'$USER')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_isk_new'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID			= $_POST['ID'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SATU'])){
							$SATU = implode(',',$_POST['SATU']);
						}else{
							$SATU = "";
						}		
						if(isset($_POST['DUA'])){
							$DUA = implode(',',$_POST['DUA']);
						}else{
							$DUA = "";
						}	
						if(isset($_POST['TIGA'])){
							$TIGA = implode(',',$_POST['TIGA']);
						}else{
							$TIGA = "";
						}	
						if(isset($_POST['EMPAT_A'])){
							$EMPAT_A = implode(',',$_POST['EMPAT_A']);
						}else{
							$EMPAT_A = "";
						}
						if(isset($_POST['EMPAT_B'])){
							$EMPAT_B = implode(',',$_POST['EMPAT_B']);
						}else{
							$EMPAT_B = "";
						}	
						if(isset($_POST['EMPAT_C'])){
							$EMPAT_C = implode(',',$_POST['EMPAT_C']);
						}else{
							$EMPAT_C = "";
						}	
						if(isset($_POST['EMPAT_D'])){
							$EMPAT_D = implode(',',$_POST['EMPAT_D']);
						}else{
							$EMPAT_D = "";
						}	
						if(isset($_POST['EMPAT_E'])){
							$EMPAT_E = implode(',',$_POST['EMPAT_E']);
						}else{
							$EMPAT_E = "";
						}							
						if(isset($_POST['LIMA_A'])){
							$LIMA_A = implode(',',$_POST['LIMA_A']);
						}else{
							$LIMA_A = "";
						}	
						if(isset($_POST['LIMA_B'])){
							$LIMA_B = implode(',',$_POST['LIMA_B']);
						}else{
							$LIMA_B = "";
						}	
						if(isset($_POST['ENAM'])){
							$ENAM = implode(',',$_POST['ENAM']);
						}else{
							$ENAM = "";
						}

						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_isk_new SET												
						TANGGAL='$TANGGAL',
						RUANGAN='$RUANGAN',
						ANALISA = '$ANALISA', 
						TINDAKLANJUT = '$TINDAKLANJUT', 
						KEPALA = '$KEPALA',
						AUDITOR='$AUDITOR',
						NOMR='$NOMR',
						NAMA='$NAMA',
						SATU='$SATU',
						DUA='$DUA',
						TIGA='$TIGA',
						EMPAT_A='$EMPAT_A',
						EMPAT_B='$EMPAT_B',
						EMPAT_C='$EMPAT_C',
						EMPAT_D='$EMPAT_D',
						EMPAT_E='$EMPAT_E',
						LIMA_A='$LIMA_A',
						LIMA_B='$LIMA_B',
						ENAM='$ENAM',
						
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID='$ID'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_isk_new'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_isk_new ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_isk_new'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_isk_new'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			