<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Data</a>
		</li>
		<li class="active">Laporan BUNDLES ISK</li>
	</ul><!-- /.breadcrumb -->

	<!--<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="ace-icon fa fa-search nav-search-icon"></i>
			</span>
		</form>
	</div><!-- /.nav-search -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Data Laporan BUNDLES ISK</h4>
							<div class="widget-toolbar">
								<a href="index.php?page=isk_new">
									<i class="ace-icon fa fa-plus" style="color:blue"> Tambah data</i>
								</a>
							</div>
						</div>
					<!-- div.table-responsive -->

					<!-- div.dataTables_borderWrap -->
					<div>
						<table id="tabeldata" class="table table-striped table-bordered table-hover">
							<thead>
								<tr align="center">
									<th width="10%"><div align="center">Tanggal</div></th>
									<th><div align="center">NOMR</div></th>
									<th><div align="center">Nama</div></th>
									<th><div align="center">Ruangan</div></th>
									<th><div align="center">Auditor</div></th>
									<th><div align="center">Total (%)</div></th>
									<th width="10%"><div align="center">Opsi</div></th>
								</tr>								
								
							</thead>

							<tbody>
							<?php					
							$query="SELECT ti.*, ru.DESKRIPSI RUANGAN 
							FROM db_ppi.tb_isk_new ti
							left join ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5					
							where ti.`STATUS`=1";							
							$info=mysqli_query($conn1,$query); 
							//untuk penomoran data
							//$no=1;						
							//menampilkan data
							while($row=mysqli_fetch_array($info)){
							?>
								<tr>
									<td><?php echo $row['TANGGAL'] ?></td>
									<td><?php echo $row['NOMR'] ?></td>
									<td><?php echo $row['NAMA'] ?></td>
									<td><?php echo $row['RUANGAN'] ?></td>								
									<td><?php echo $row['AUDITOR'] ?></td>
									<td><?php echo $row['TOTAL'] ?></td>						
									<td align="center">
										<div class="hidden-sm hidden-xs action-buttons">
											<a class="btn btn-info btn-xs" href="?page=edit_isk_new&id=<?php echo $row['ID']?>">
												<i class="ace-icon fa fa-pencil bigger-130"></i>
											</a>
											<a class="btn btn-danger btn-xs"  href="?page=simpan_isk_new&id=<?php echo $row['ID']?>">
												<i class="ace-icon fa fa-trash-o bigger-130"></i>
											</a>
										</div>
									</td>
								</tr>							
								<?php							
								}
								?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

