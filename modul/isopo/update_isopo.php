<html>
<head>
	<script src="assets/sweetalert/dist/sweetalert-dev.js"></script>
	<link rel="stylesheet" href="assets/sweetalert/dist/sweetalert.css">

</head>
<body>
	<?php
	date_default_timezone_set('Asia/Jakarta');
	if(isset($_POST['btnEdit'])){
		$ID					= $_POST['ID'];
		$PENEMPATAN			= $_POST['PENEMPATAN'];
		$TANGGAL 			= $_POST['TANGGAL'];									
		$NILAI 				= $_POST['NILAI'];
		$NOMR				= $_POST['NOMR'];
		$NAMA				= addslashes($_POST['NAMA']);
		$UMUR				= $_POST['UMUR'];
		$DPJP				= $_POST['DPJP'];
		$DIAGNOSA			= $_POST['DIAGNOSA'];	
		$A1 				= $_POST['A1'];
		$KET_A1				= $_POST['KET_A1']; 
		$A2					= $_POST['A2'];
		$KET_A2				= $_POST['KET_A2']; 
		$A3					= $_POST['A3'];
		$KET_A3 			= $_POST['KET_A3'];	
		$A4					= $_POST['A4'];
		$KET_A4				= $_POST['KET_A4'];  
		$A5 				= $_POST['A5'];
		$KET_A5 			= $_POST['KET_A5'];
		$A6					= $_POST['A6'];
		$KET_A6 			= $_POST['KET_A6'];  
		$A7 				= $_POST['A7'];
		$KET_A7				= $_POST['KET_A7'];  
		$B1 				= $_POST['B1'];
		$KET_B1 			=	$_POST['KET_B1'];  
		$B2 				=	$_POST['B2'];
		$KET_B2 			=	$_POST['KET_B2'];  
		$C1 				=	$_POST['C1'];
		$KET_C1 			=	$_POST['KET_C1'];  
		$C2 				=	$_POST['C2'];
		$KET_C2 			=	$_POST['KET_C2'];  
		$C3 				=	$_POST['C3']; 
		$KET_C3 			=	$_POST['KET_C3']; 
		$C4 				=	$_POST['C4'];
		$KET_C4 			=	$_POST['KET_C4'];  
		$C5 				=	$_POST['C5'];
		$KET_C5 			=	$_POST['KET_C5'];  
		$C6 				=	$_POST['C6'];
		$KET_C6 			=	$_POST['KET_C6'];  
		$D1 				=	$_POST['D1'];
		$KET_D1 			=	$_POST['KET_D1']; 
		$D2 				=	$_POST['D2'];
		$KET_D2 			=	$_POST['KET_D2']; 
		$D3 				=	$_POST['D3'];
		$KET_D3				= 	$_POST['KET_D3'];  
		$D4 				=	$_POST['D4'];
		$KET_D4 			=	$_POST['KET_D4'];  
		$D5 				=	$_POST['D5'];
		$KET_D5 			=	$_POST['KET_D5'];  
		$D6 				=	$_POST['D6'];
		$KET_D6				=	$_POST['KET_D6'];  
		$NILAI 				=	$_POST['NILAI'];  
		$ANALISA 			=	$_POST['ANALISA'];  
		$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
		$KEPALA 			=	$_POST['KEPALA'];  
		$AUDITOR 			=	addslashes($_POST['AUDITOR']); 
		$pengisi 			= 	$_SESSION['userid'];						
//echo "<pre>";print_r($_POST);exit();                											
		$query2 = "UPDATE db_ppi.tb_isopo SET						
		PENEMPATAN		= '$PENEMPATAN',
		TANGGAL 		= '$TANGGAL',									
		NILAI 			= '$NILAI',
		MR				= '$NOMR',
		NAMA			= '$NAMA',
		UMUR			= '$UMUR',
		DPJP			= '$DPJP',
		DIAGNOSA		= '$DIAGNOSA',	
		A1 				= '$A1',
		KET_A1			= '$KET_A1', 
		A2				= '$A2',
		KET_A2			= '$KET_A2', 
		A3				= '$A3',
		KET_A3 			= '$KET_A3',	
		A4				= '$A4',
		KET_A4			= '$KET_A4',  
		A5 				= '$A5',
		KET_A5 			= '$KET_A5',
		A6				= '$A6',
		KET_A6 			= '$KET_A6',  
		A7 				= '$A7',
		KET_A7			= '$KET_A7',  
		B1 				= '$B1',
		KET_B1 			=	'$KET_B1',  
		B2 				=	'$B2',
		KET_B2 			=	'$KET_B2',  
		C1 				=	'$C1',
		KET_C1 			=	'$KET_C1',  
		C2 				=	'$C2',
		KET_C2 			=	'$KET_C2',  
		C3 				=	'$C3', 
		KET_C3 			=	'$KET_C3', 
		C4 				=	'$C4',
		KET_C4 			=	'$KET_C4',  
		C5 				=	'$C5',
		KET_C5 			=	'$KET_C5',  
		C6 				=	'$C6',
		KET_C6 			=	'$KET_C6',  
		D1 				=	'$D1',
		KET_D1 			=	'$KET_D1', 
		D2 				=	'$D2',
		KET_D2 			=	'$KET_D2', 
		D3 				=	'$D3',
		KET_D3			= 	'$KET_D3',  
		D4 				=	'$D4',
		KET_D4 			=	'$KET_D4',  
		D5 				=	'$D5',
		KET_D5 			=	'$KET_D5',  
		D6 				=	'$D6',
		KET_D6			=	'$KET_D6',  
		NILAI 			=	'$NILAI',  
		ANALISA 		=	'$ANALISA',  
		TINDAKLANJUT 	=	'$TINDAKLANJUT',  
		KEPALA 			=	'$KEPALA',  
		AUDITOR 		=	'$AUDITOR', 
		DIUBAH_OLEH		=	'$pengisi'
		where ID='$ID'";
//echo $query2; die();	
		$insert=mysqli_query($conn1,$query2);									
		if($insert){
			echo '<script>
			swal({
				html: true,
				title: "Sukses",
				text: "Sukses Update ISOPO",
				type: "success",
				confirmButtonText: "Lihat Inputan",
				closeOnConfirm: false,
				closeOnCancel: false
				},
				function() {
				
						window.location.href="index.php?page=tabel_isopo";
					} 
					);
					</script>'; 
				}
			}
			?>
			</body>
			</html>	

