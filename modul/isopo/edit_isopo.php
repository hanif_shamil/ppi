<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>

<?php
$id=($_GET['id']);
$query="select * from db_ppi.tb_isopo where ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>
					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">FORMULIR MONITORING  RUANG ISOLASI  IMUNITAS MENURUN (ISOLASI POSITIF)</li>
				</ul><!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">FORMULIR MONITORING  RUANG ISOLASI  IMUNITAS MENURUN (ISOLASI POSITIF)</h4>
								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>
									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=update_isopo" method="post">	
										<input type="hidden" name="ID" value="<?php echo $dt['ID']; ?>">	
										<div class="form-group">
											<div class="col-sm-2">	
												<label class="control-label bolder blue">Penempatan Pasien</label>
											</div>
											<div class="col-sm-3">
												<div class="radio">
													<label>
														<input name="PENEMPATAN" <?php if($dt['PENEMPATAN']=='1') echo " checked "?> type="radio" value="1" class="ace input-lg" />
														<span class="lbl"> Sesuai</span>
													</label>
													<label>
														<input name="PENEMPATAN" <?php if($dt['PENEMPATAN']=='2') echo " checked "?> value="2" type="radio" class="ace input-lg" />
														<span class="lbl"> Tidak Sesuai</span>
													</label>											
												</div>	
											</div>
											<div class="col-sm-2">
												<input type="text" id="nomr" name="NOMR" value="<?php echo $dt['MR']; ?>"" placeholder="[ MR ]" class="form-control" onkeyup="autofill()" />
											</div>	
											<div class="col-sm-3">									
												<input type="text" id="nama" name="NAMA" value="<?php echo $dt['NAMA']; ?>" placeholder="[ NAMA ]" class="form-control" />					
											</div>
											<div class="col-sm-2">									
												<input type="text" id="umur" name="UMUR" value="<?php echo $dt['UMUR']; ?>" placeholder="[ UMUR ]" class="form-control" />					
											</div>	
										</div>	
										<div class="form-group">						
											<div class="col-sm-4">
												<input type="text" id="diagnosa" name="DIAGNOSA" value="<?php echo $dt['DIAGNOSA']; ?>" placeholder="[ DIAGNOSA ]" class="form-control">										
											</div>	
											<div class="col-sm-5">									
												<input type="text" id="dpjp" name="DPJP" value="<?php echo $dt['DPJP']; ?>" placeholder="[ DPJP ]" class="form-control" />												
											</div>
											<div class="col-sm-1">	
												<label class="col-sm-12 control-label no-padding-right bolder blue" for="form-field-1">Tanggal</label>
											</div>
											<div class="col-sm-2">									
												<div class="input-group">
													<input class="form-control" value="<?php echo $dt['TANGGAL']; ?>"" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>												
											</div>
										</div>

										<div class="form-group">																				
											<div class="col-sm-12">
												<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th width="5%" style="text-align:center">NO</th>
																<th style="text-align:center"> KEGIATAN</th>
																<th width="5%" style="text-align:center">YA</th>
																<th width="5%" style="text-align:center">TIDAK</th>
																<th width="5%" style="text-align:center">NA</th>
																<th width="30%" style="text-align:center">KETERANGAN</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>A.</td>
																<td>RUANG ISOLASI</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Tekanan Positif  ≥ 8 Pascal</td>
																<td align="center">
																	<label><input type="radio" name="A1" id="AA" <?php if($dt['A1']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">											
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A1" type="radio" value="2" <?php if($dt['A1']=='2') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A1" type="radio" value="2" <?php if($dt['A1']=='3') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_A1" value="<?php echo $dt['KET_A1']; ?>" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Ada ruang anteroom</td>
																<td align="center"><label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>

															<td align="center"><label>
																<input type="radio" name="A2" id="SS" <?php if($dt['A2']=='2') echo " checked "?> value="2" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center"><label>
																<input type="radio" name="A2" id="SS" <?php if($dt['A2']=='3') echo " checked "?> value="2" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														<td>
															<input type="text" id="nama" name="KET_A2" value="<?php echo $dt['KET_A2']; ?>" placeholder="" class="form-control" />		
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Pintu selalu tertutup</td>
														<td align="center">
															<label>
																<input type="radio" name="A3" id="AA" <?php if($dt['A3']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input type="radio" name="A3" <?php if($dt['A3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input type="radio" name="A3" <?php if($dt['A3']=='3') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														<td>
															<input type="text" id="nama" name="KET_A3" value="<?php echo $dt['KET_A3']; ?>" placeholder="" class="form-control" />		
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Tersedia tempat pembuangan limbah infeksius dan Non Infeksius & benda tajam</td>
														<td align="center">
															<label>
																<input name="A4" type="radio" <?php if($dt['A4']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="A4" type="radio" <?php if($dt['A4']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="A4" type="radio" <?php if($dt['A4']=='3') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td>
															<input type="text" id="nama" name="KET_A4" value="<?php echo $dt['KET_A4']; ?>" placeholder="" class="form-control" />		
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Lantai bersih tidak berjamur</td>
														<td align="center">
															<label>
																<input name="A5" type="radio" <?php if($dt['A5']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="A5" type="radio" <?php if($dt['A5']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="A5" type="radio" <?php if($dt['A5']=='3') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td>
															<input type="text" id="nama" name="KET_A5" value="<?php echo $dt['KET_A5']; ?>" placeholder="" class="form-control" />		
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Tempat Makanan tertutup, penyajian buah tidak di potong</td>
														<td align="center">
															<label>
																<input name="A6" type="radio" <?php if($dt['A6']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="A6" type="radio" <?php if($dt['A6']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="A6" type="radio" <?php if($dt['A6']=='3') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td>
															<input type="text" id="nama" name="KET_A6" value="<?php echo $dt['KET_A6']; ?>" placeholder="" class="form-control" />		
														</td>
													</tr>
													<tr>
														<td>7</td>
														<td>Makanan di microwave sebelum disajikan</td>
														<td align="center">
															<label>
																<input name="A7" type="radio" <?php if($dt['A7']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="A7" type="radio" <?php if($dt['A7']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="A7" type="radio" <?php if($dt['A7']=='3') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td>
															<input type="text" id="nama" name="KET_A7" value="<?php echo $dt['KET_A7']; ?>" placeholder="" class="form-control" />		
														</td>
													</tr>
													<tr>
														<td>B.</td>
														<td>RUANG ISOLASI</td>
														<td></td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td>1</td>
														<td>Tersedia sabun anti septik (hand wash)/ Hand Rub</td>
														<td align="center"><label>
															<input type="radio" name="B1" <?php if($dt['B1']=='1') echo " checked "?> id="AA"  value="1" class="ace input-lg" onClick="tugas1()">											
															<span class="lbl"></span>
														</label></td>

														<td align="center">
															<label>
																<input name="B1" type="radio" <?php if($dt['B1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="B1" type="radio" <?php if($dt['B1']=='3') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td>
															<input type="text" id="nama" name="KET_B1" value="<?php echo $dt['KET_B1']; ?>" placeholder="" class="form-control" />		
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Tersedia Paper Towel</td>
														<td align="center"><label>
															<input type="radio" name="B2" <?php if($dt['B2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center"><label>
														<input type="radio" name="B2" <?php if($dt['B2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center"><label>
														<input type="radio" name="B2" <?php if($dt['B2']=='3') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
														<span class="lbl"></span>
													</label>
												</td>
												<td>
													<input type="text" id="nama" name="KET_B2" value="<?php echo $dt['KET_B2']; ?>" placeholder="" class="form-control" />		
												</td>
											</tr>
											<tr>
												<td>C.</td>
												<td>FASILITAS ALAT PELINDUNG DIRI (APD) SESUAI KEBUTUHAN</td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td>1</td>
												<td>Tersedia Penutup Kepala</td>
												<td align="center"><label>
													<input type="radio" name="C1" <?php if($dt['C1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
													<span class="lbl"></span>
												</label></td>

												<td align="center">
													<label>
														<input name="C1" type="radio" <?php if($dt['C1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="C1" type="radio" <?php if($dt['C1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td>
													<input type="text" id="nama" name="KET_C1" value="<?php echo $dt['KET_C1']; ?>" placeholder="" class="form-control" />		
												</td>
											</tr>
											<tr>
												<td>2</td>
												<td>Tersedia kaca mata (Google) </td>
												<td align="center"><label>
													<input type="radio" name="C2" <?php if($dt['C2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
													<span class="lbl"></span>
												</label>
											</td>

											<td align="center"><label>
												<input type="radio" name="C2" id="SS" <?php if($dt['C2']=='2') echo " checked "?> value="2" class="ace input-lg" onClick="tugas1()">
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center"><label>
												<input type="radio" name="C2" id="SS" <?php if($dt['C2']=='3') echo " checked "?> value="2" class="ace input-lg" onClick="tugas1()">
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" name="KET_C2" value="<?php echo $dt['KET_C2']; ?>" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td>3</td>
										<td>Tersedia Masker Bedah</td>
										<td align="center">
											<label>
												<input type="radio" name="C3" id="AA" <?php if($dt['C3']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input type="radio" name="C3" id="SS" <?php if($dt['C3']=='2') echo " checked "?> value="2" class="ace input-lg" onClick="tugas1()">
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input type="radio" name="C3" id="SS" <?php if($dt['C3']=='3') echo " checked "?> value="2" class="ace input-lg" onClick="tugas1()">
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" name="KET_C3" value="<?php echo $dt['KET_C3']; ?>" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td>4</td>
										<td>Tersedia Sarung tangan bedah/ steril</td>
										<td align="center">
											<label>
												<input name="C4" type="radio" value="1" <?php if($dt['C4']=='1') echo " checked "?> id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="C4" type="radio" value="2" <?php if($dt['C4']=='2') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="C4" type="radio" value="2" <?php if($dt['C4']=='3') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" name="KET_C4" value="<?php echo $dt['KET_C4']; ?>" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td>5</td>
										<td>Tersedia Gaun dan Linen Steril</td>
										<td align="center">
											<label>
												<input name="C5" type="radio" value="1" <?php if($dt['C5']=='1') echo " checked "?> id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="C5" type="radio" value="2" <?php if($dt['C5']=='2') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="C5" type="radio" value="2" <?php if($dt['C5']=='3') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" name="KET_C5" value="<?php echo $dt['KET_C5']; ?>" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td>6</td>
										<td>Tersedia Sandal Khusus Isolasi</td>
										<td align="center">
											<label>
												<input name="C6" type="radio" value="1" <?php if($dt['C6']=='1') echo " checked "?> id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="C6" type="radio" value="2" <?php if($dt['C6']=='2') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="C6" type="radio" value="2" <?php if($dt['C6']=='3') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" name="KET_C6" value="<?php echo $dt['KET_C6']; ?>" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td>D.</td>
										<td>KEPATUHAN PETUGAS</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>1</td>
										<td>Melakukan kebersihan tangan (5 moment)</td>
										<td align="center"><label>
											<input type="radio" name="D1" id="AA" value="1" <?php if($dt['D1']=='1') echo " checked "?> class="ace input-lg" onClick="tugas1()">											
											<span class="lbl"></span>
										</label></td>

										<td align="center">
											<label>
												<input name="D1" type="radio" value="2" <?php if($dt['D1']=='2') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="D1" type="radio" value="2" <?php if($dt['D1']=='3') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" name="KET_D1" value="<?php echo $dt['KET_D1']; ?>" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td>2</td>
										<td>Menggunakan APD saat melakukan kegiatan di ruang isolasi</td>
										<td align="center"><label>
											<input type="radio" name="D2" id="AA" value="1" <?php if($dt['D2']=='1') echo " checked "?> class="ace input-lg" onClick="tugas1()">		
											<span class="lbl"></span>
										</label>
									</td>

									<td align="center"><label>
										<input type="radio" name="D2" id="SS" value="2" <?php if($dt['D2']=='2') echo " checked "?> class="ace input-lg" onClick="tugas1()">
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center"><label>
										<input type="radio" name="D2" id="SS" value="2" <?php if($dt['D2']=='3') echo " checked "?> class="ace input-lg" onClick="tugas1()">
										<span class="lbl"></span>
									</label>
								</td>
								<td>
									<input type="text" id="nama" name="KET_D2" value="<?php echo $dt['KET_D2']; ?>" placeholder="" class="form-control" />		
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Membuang limbah sesuai standar</td>
								<td align="center">
									<label>
										<input type="radio" name="D3" id="AA" value="1" <?php if($dt['D3']=='1') echo " checked "?> class="ace input-lg" onClick="tugas1()">
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center">
									<label>
										<input type="radio" name="D3" id="SS" value="2" <?php if($dt['D3']=='2') echo " checked "?> class="ace input-lg" onClick="tugas1()">
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center">
									<label>
										<input type="radio" name="D3" id="SS" value="2" <?php if($dt['D3']=='3') echo " checked "?> class="ace input-lg" onClick="tugas1()">
										<span class="lbl"></span>
									</label>
								</td>
								<td>
									<input type="text" id="nama" name="KET_D3" value="<?php echo $dt['KET_D3']; ?>" placeholder="" class="form-control" />		
								</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Menempatkan linen kotor pada tempatnya</td>
								<td align="center">
									<label>
										<input name="D4" type="radio" value="1" id="AA" <?php if($dt['D4']=='1') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center">
									<label>
										<input name="D4" type="radio" value="2" id="SS" <?php if($dt['D4']=='2') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center">
									<label>
										<input name="D4" type="radio" value="2" id="SS" <?php if($dt['D4']=='3') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td>
									<input type="text" id="nama" name="KET_D4" value="<?php echo $dt['KET_D4']; ?>" placeholder="" class="form-control" />		
								</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Memberikan edukasi kepada pasien dan Keluarga</td>
								<td align="center">
									<label>
										<input name="D5" type="radio" value="1" id="AA" <?php if($dt['D5']=='1') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center">
									<label>
										<input name="D5" type="radio" value="2" id="SS" <?php if($dt['D5']=='2') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center">
									<label>
										<input name="D5" type="radio" value="2" id="SS" <?php if($dt['D5']=='3') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td>
									<input type="text" id="nama" name="KET_D5" value="<?php echo $dt['KET_D5']; ?>" placeholder="" class="form-control" />		
								</td>
							</tr>
							<tr>
								<td>6</td>
								<td>Melakukan monitoring ruangan dengan cek list : tekanan udara setiap hari.</td>
								<td align="center">
									<label>
										<input name="D6" type="radio" value="1" id="AA" <?php if($dt['D6']=='1') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center">
									<label>
										<input name="D6" type="radio" value="2" id="SS" <?php if($dt['D6']=='2') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center">
									<label>
										<input name="D6" type="radio" value="2" id="SS" <?php if($dt['D6']=='3') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td>
									<input type="text" id="nama" name="KET_D6" value="<?php echo $dt['KET_D6']; ?>" placeholder="" class="form-control" />		
								</td>
							</tr>												
							<tr>
								<td style="text-align:center" colspan="4">NILAI KEPATUHAN</td>
								<td colspan="3" style="text-align:center">
									<div class="input-group">
										<input type="text" id="total" name="NILAI" value="<?php echo $dt['NILAI']; ?>" placeholder="[ NILAI ]" class="form-control" />
										<span class="input-group-addon">
											%
										</span>
									</div>													
								</td>														
							</tr>													
						</tbody>
					</table>																									
				</div>
			</div>
		</div>																										
		<hr />
		<div class="form-group">										
			<div class="col-md-6 col-sm-12">
				<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
				<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
			</div>				
			<div class="col-md-6 col-sm-12">
				<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
				<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
			</div>
		</div>
		<div class="form-group">			
			<div class="col-md-6 col-sm-12">
				<label class="control-label bolder blue" style="text-decoration: underline">Kepala Unit/Kepala Ruangan</label>	
				<input type="text" id="nama" name="KEPALA" value="<?php echo $dt['KEPALA']; ?>" placeholder="[ Kepala Unit/Kepala Ruangan ]" class="form-control" />			
			</div>	
			<div class="col-md-6 col-sm-12">	
				<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
				<input type="text" id="nama" name="AUDITOR" value="<?php echo $dt['AUDITOR']; ?>" placeholder="[ AUDITOR ]" class="form-control" />
			</div>								
		</div>																											
		<hr />
		<div class="form-group">							
			<div class="col-md-offset-3 col-md-9">
				<button class="btn btn-info" name="btnEdit" type="submit">
					<i class="ace-icon fa fa-check bigger-110"></i>
					Submit
				</button>
				&nbsp; &nbsp; &nbsp;
				<button class="btn" type="reset">
					<i class="ace-icon fa fa-undo bigger-110"></i>
					Reset
				</button>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div><!-- /.span -->
</div>
</div><!-- /.page-content -->	
<?php }
?>