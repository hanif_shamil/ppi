<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>
		<li class="active">Dashboard</li>
	</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Grafik Rata-rata nilai Hand Hygine</div>
				<div class="panel-body">
					<div id="handy"></div>
				</div>
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Rata-rata nilai Hand Hygine</div>
				<div class="panel-body">
					<table id="dataTable3" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>BULAN</th>
								<th>LAB</th>
								<th>DOKTER</th>		
								<th>PERAWAT</th>		
								<th>POS</th>		
								<!-- <th>TH</th>		 -->
							</tr>
						</thead>
						<tbody>
							<?php					
							$query="SELECT DATE_FORMAT(th.TANGGAL,'%Y-%m') PERIODE, round(lab.RATA,2) LAB, round(dr.RATA,2) DR, round(pwt.RATA,2) PWT, 
							round(pos.RATA,2) POS, round(th2.RATA,2) TH
							FROM tb_hand th
							LEFT JOIN (
								SELECT DATE_FORMAT(th1.TANGGAL,'%Y-%m') PERIODE, AVG(th1.NILAI) RATA
								FROM tb_hand th1 
								WHERE th1.PETUGAS = 11
								GROUP BY DATE_FORMAT(th1.TANGGAL,'%Y-%m')
								) lab ON lab.PERIODE = DATE_FORMAT(th.TANGGAL,'%Y-%m')
							LEFT JOIN (
								SELECT DATE_FORMAT(th1.TANGGAL,'%Y-%m') PERIODE, AVG(th1.NILAI) RATA
								FROM tb_hand th1 
								WHERE th1.PETUGAS = 12
								GROUP BY DATE_FORMAT(th1.TANGGAL,'%Y-%m')
								) dr ON dr.PERIODE = DATE_FORMAT(th.TANGGAL,'%Y-%m')
							LEFT JOIN (
								SELECT DATE_FORMAT(th1.TANGGAL,'%Y-%m') PERIODE, AVG(th1.NILAI) RATA
								FROM tb_hand th1 
								WHERE th1.PETUGAS = 13
								GROUP BY DATE_FORMAT(th1.TANGGAL,'%Y-%m')
								) pwt ON pwt.PERIODE = DATE_FORMAT(th.TANGGAL,'%Y-%m')
							LEFT JOIN (
								SELECT DATE_FORMAT(th1.TANGGAL,'%Y-%m') PERIODE, AVG(th1.NILAI) RATA
								FROM tb_hand th1 
								WHERE th1.PETUGAS = 14
								GROUP BY DATE_FORMAT(th1.TANGGAL,'%Y-%m')
								) pos ON pos.PERIODE = DATE_FORMAT(th.TANGGAL,'%Y-%m')
							LEFT JOIN (
								SELECT DATE_FORMAT(th1.TANGGAL,'%Y-%m') PERIODE, AVG(th1.NILAI) RATA
								FROM tb_hand th1 
								WHERE th1.PETUGAS = 15
								GROUP BY DATE_FORMAT(th1.TANGGAL,'%Y-%m')
								) th2 ON th2.PERIODE = DATE_FORMAT(th.TANGGAL,'%Y-%m')
							WHERE DATE_FORMAT(th.TANGGAL,'%Y') = DATE_FORMAT(NOW(),'%Y') 
							GROUP BY DATE_FORMAT(th.TANGGAL,'%Y-%m')";							
							$info=mysqli_query($conn1,$query); 
								//untuk penomoran data
							$no=1;						
								//menampilkan data
							while($row=mysqli_fetch_array($info)){
								?>
								<tr>   			
									<td><?php echo $row[0]; ?></td> 
									<td><?php echo $row[1]; ?></td> 
									<td><?php echo $row[2]; ?></td>  
									<td><?php echo $row[3]; ?></td> 
									<td><?php echo $row[4]; ?></td> 
									<!-- <td><?php echo $row[5]; ?></td>		 -->
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div><!-- /.row -->
</div>

