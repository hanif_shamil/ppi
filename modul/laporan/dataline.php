<?php
	include '../../config.php';

// Data for Sugar
$query = mysqli_query($conn1,"select AVG(TOTAL) RT_ISK FROM tb_isk");
$rows = array();
$rows['name'] = 'ISK';
while($tmp= mysqli_fetch_array($query)) {
    $rows['data'][] = $tmp['RT_ISK'];
}

// Data for Rice
$query = mysqli_query($conn1,"select AVG(TOTAL) RT_IADP FROM tb_iadp");
$rows1 = array();
$rows1['name'] = 'IADP';
while($tmp = mysqli_fetch_array($query)) {
    $rows1['data'][] = $tmp['RT_IADP'];
}

// Data for Wheat Flour
$query = mysqli_query($conn1,"select AVG(TOTAL) RT_IDO FROM tb_ido");
$rows2 = array();
$rows2['name'] = 'IDO';
while($tmp = mysqli_fetch_array($query)) {
    $rows2['data'][] = $tmp['RT_IDO'];
}

// Data for Wheat Flour
$query = mysqli_query($conn1,"select AVG(TOTAL) RT_VAP FROM tb_vap");
$rows3 = array();
$rows3['name'] = 'VAP';
while($tmp = mysqli_fetch_array($query)) {
    $rows3['data'][] = $tmp['RT_VAP'];
}

$result = array();
array_push($result,$rows);
array_push($result,$rows1);
array_push($result,$rows2);
array_push($result,$rows3);

print json_encode($result, JSON_NUMERIC_CHECK);

mysqli_close($conn1);
?> 