<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Laporan</a>
				</li>
				<li class="active">Laporan Bundles</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Laporan Bundles</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
							
								
								<div class="col-md-3">
								<form class="form-horizontal" id="sample-form" action="modul/laporan/cetakbundles.php" target="_blank" method="post">
									<div class="form-group">
										<label> Tanggal Awal:</label>
									</div>
									<div class="form-group">	
										<div class='input-group date' id='datetimepicker11'>															
											<input type='text' name="tanggal1" id="tanggal13" class="form-control" />	
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>															
										</div>										
									</div>								
									<div class="form-group">
										<label> Tanggal Akhir:</label>
									</div>
									<div class="form-group">	
										<div class='input-group date' id='datetimepicker12'>															
											<input type='text' name="tanggal2" id="tanggal14" class="form-control" />	
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-calendar"></span>
											</span>															
										</div>										
									</div>								
								
									<div class="form-group">										
										<select class="chosen-select form-control" name="bundle" id="bundle" data-placeholder="[ Jenis Bundle ]">
										<option value=""></option>
										<?php
										$sql = "select * from db_ppi.referensi rf
											where rf.JENIS=7";												
										$rs = mysqli_query($conn1,$sql);
										while ($data = mysqli_fetch_array($rs)) {
										?>
										<option value="<?=$data[0]?>"><?=$data[2]?></option>
										<?php
										}
										?>	
										</select>										
									</div>								
									<div class="form-group">										
										<select class="chosen-select form-control" name="nomr" id="nomr" data-placeholder="[ Nama Pasien ]">
										<option value=""></option>
										<?php
										$sql = "select NOMR, NAMA from db_ppi.tb_iadp GROUP BY NOMR";												
										$rs = mysqli_query($conn1,$sql);
										while ($data = mysqli_fetch_array($rs)) {
										?>
										<option value="<?=$data['NOMR']?>"><?=$data['NAMA']?></option>
										<?php
										}
										?>	
										</select>										
									</div>
									<div class="form-group">
										<div class="col-md-6">
											<button class="btn btn-success btn-xs" name="CETAK" type="submit"><i class="ace-icon fa fa-print"></i>Cetak Hasil</button>
										</div>									
										<div class="col-md-6">
											<a type="submit" class="btn btn-primary btn-xs" id="bundles">Tampilkan <i class="ace-icon glyphicon glyphicon-play-circle"></i></a>
										</div>									
									</div>	
								</form>									
								</div>								
								
								<div class="col-md-9">
									<div class="widget-body">
										<div class="widget-main">
											<div class="panel panel-default">		
												<div class="panel-heading"><i class="fa fa-table"></i></div>								
												<div class="panel-body">
													<div id="table_bundle"></div>
												</div>
											</div>
										</div>
									</div>
								</div>			
							
							</div>
						
							<div class="row">
								<div class="col-md-12">
									<div class="widget-body">
										<div class="widget-main">
											<div class="panel panel-default">
												<div class="panel-heading"><i class="fa fa-bar-chart-o"></i> Grafik</div>											
												<div class="panel-body">							
													<div id ="chart_bundle"></div>			
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
														
													
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->