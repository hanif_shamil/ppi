<?php
include '../../../config.php';
require('../../../assets/pdf/fpdf.php');

if(isset($_POST)){
//echo "<pre>";print_r($_POST);
    $tanggal1 = date("Y-m",strtotime($_POST['tanggal1']));
	$bundle  	= $_POST['bundle'];
	$bulan = date("M Y",strtotime($_POST['tanggal1']));
}

if($_POST['tanggal1'] != '' && (empty($_POST['bundle'] ='' ))){
	//if($_POST['tanggal1'] != '' && $_POST['tanggal2'] != ''){
//echo "<pre>";print_r($_POST);exit();

if($_POST){
$pdf = new FPDF("P","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',12);
$pdf->Image('../../../assets/images/13747.png',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,"Laporan Bundles",0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Instalasi Rawat Inap',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Ruang',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Rumah Sakit Kanker "Dharmais"',0,'L');
$pdf->Line(0,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(0,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Times','B',14);
$pdf->Cell(18,0.7,"Laporan Data Bundles Bulan ".$bulan,0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'RUANGAN', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'ISK', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'IADP', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'IDO', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'VAP', 1, 1, 'C');

$pdf->SetFont('Arial','',12);
$no=1;
$query=mysqli_query($conn1,"SELECT IF(date_format(ti.TANGGAL,'%m')=01,'januari',IF(date_format(ti.TANGGAL,'%m')=02,'Febr',IF(date_format(ti.TANGGAL,'%m')=03,'Mar'
		,IF(date_format(ti.TANGGAL,'%m')=04,'Apr',IF(date_format(ti.TANGGAL,'%m')=05,'Mei',IF(date_format(ti.TANGGAL,'%m')=06,'Juni'
		,IF(date_format(ti.TANGGAL,'%m')=07,'Juli',IF(date_format(ti.TANGGAL,'%m')=08,'Agustus',IF(date_format(ti.TANGGAL,'%m')=09,'September'
		,IF(date_format(ti.TANGGAL,'%m')=10,'Oktober',IF(date_format(ti.TANGGAL,'%m')=11,'November','Desember'))))))))))) BULAN,r.DESKRIPSI
		, (ROUND(AVG(ti.TOTAL),2)) ISK,A.RATA_RATA IADP,B.RATA_RATA IDO,C.RATA_RATA VAP
		from tb_isk ti 
		LEFT JOIN (SELECT date_format(ti.TANGGAL,'%m') BULAN, (ROUND(AVG(ti.TOTAL),2)) RATA_RATA
		from tb_iadp ti group by month(TANGGAL)) A ON A.BULAN = date_format(ti.TANGGAL,'%m')

		LEFT JOIN (SELECT date_format(ti.TANGGAL,'%m') BULAN, (ROUND(AVG(ti.TOTAL),2)) RATA_RATA
		from tb_ido ti 
		group by month(TANGGAL)) B ON B.BULAN = date_format(ti.TANGGAL,'%m')

		LEFT JOIN (SELECT date_format(ti.TANGGAL,'%m') BULAN, (ROUND(AVG(ti.TOTAL),2)) RATA_RATA
		from tb_vap ti 
		group by month(TANGGAL)) C ON C.BULAN = date_format(ti.TANGGAL,'%m')
		LEFT join ruangan r ON r.ID=ti.RUANGAN and r.JENIS=5
		WHERE date_format(ti.TANGGAL,'%Y-%m') = '$tanggal1'
		group by RUANGAN");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['DESKRIPSI'],1, 0, 'L');
	$pdf->Cell(2, 0.8, $lihat['ISK'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['IADP'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['IDO'],1, 0, 'C');
	$pdf->Cell(2, 0.8, $lihat['VAP'], 1, 1,'C');
	$no++;
}

$pdf->Output("laporan_buku.pdf","I");	
} 
	}
?>