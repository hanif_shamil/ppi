<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>
		<li class="active">Dashboard</li>
	</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Grafik Rata-rata nilai Bundles</div>
				<div class="panel-body">
					<div id="ratabundle"></div>
				</div>
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Grafik Rata-rata nilai Bundles</div>
				<div class="panel-body">
					<table id="tbratabundle" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th width="10%">BULAN</th>
								<th>IADP</th>
								<th>VAP</th>
								<th>IDO</th>
								<th>ISK</th>
							</tr>
						</thead>
						<tbody>
							<?php					
							$query="SELECT IF(date_format(ti.TANGGAL,'%m')=01,'januari',IF(date_format(ti.TANGGAL,'%m')=02,'Febr',IF(date_format(ti.TANGGAL,'%m')=03,'Mar'
								,IF(date_format(ti.TANGGAL,'%m')=04,'Apr',IF(date_format(ti.TANGGAL,'%m')=05,'Mei',IF(date_format(ti.TANGGAL,'%m')=06,'Juni'
									,IF(date_format(ti.TANGGAL,'%m')=07,'Juli',IF(date_format(ti.TANGGAL,'%m')=08,'Agustus',IF(date_format(ti.TANGGAL,'%m')=09,'September'
										,IF(date_format(ti.TANGGAL,'%m')=10,'Oktober',IF(date_format(ti.TANGGAL,'%m')=11,'November','Desember')))))))))))	BULAN
							, (ROUND(AVG(ti.TOTAL),2)) ISK,A.RATA_RATA IADP,B.RATA_RATA IDO,C.RATA_RATA VAP, ti.TANGGAL
							from db_ppi.tb_isk_new ti 
							LEFT JOIN (SELECT date_format(ti.TANGGAL,'%m') BULAN, (ROUND(AVG(ti.TOTAL),2)) RATA_RATA
								from tb_iadp ti group by month(TANGGAL)) A ON A.BULAN = date_format(ti.TANGGAL,'%m')

							LEFT JOIN (SELECT date_format(ti.TANGGAL,'%m') BULAN, (ROUND(AVG(ti.TOTAL),2)) RATA_RATA
								from db_ppi.tb_ido_new ti 
								group by month(TANGGAL)) B ON B.BULAN = date_format(ti.TANGGAL,'%m')

							LEFT JOIN (SELECT date_format(ti.TANGGAL,'%m') BULAN, (ROUND(AVG(ti.TOTAL),2)) RATA_RATA
								from db_ppi.tb_vap ti 
								group by month(TANGGAL)) C ON C.BULAN = date_format(ti.TANGGAL,'%m')
							WHERE DATE_FORMAT(ti.TANGGAL,'%Y') = DATE_FORMAT(NOW(),'%Y') 
							group by month(TANGGAL)";							
							$info=mysqli_query($conn1,$query); 
								//untuk penomoran data
							$no=1;						
								//menampilkan data
							while($row=mysqli_fetch_array($info)){
								?>
								<tr>   
									<td><?php echo $row[0]; ?></td> 
									<td><?php echo $row[1]; ?></td> 
									<td><?php echo $row[2]; ?></td> 
									<td><?php echo $row[3]; ?></td> 
									<td><?php echo $row[4]; ?></td>  						
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div><!-- /.row -->
</div>

