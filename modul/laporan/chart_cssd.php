<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>


<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>
		<li class="active">Dashboard</li>
	</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Grafik Audit CSSD</div>
				<div class="panel-body">
					<div id="chartcssd"></div>
				</div>
			</div>
		</div>
	</div><!-- /.row -->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Rata-rata nilai Audit CSSD</div>
				<div class="panel-body">
					<table id="tabelcssd" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>BULAN</th>
								<th>NILAI</th>	
							</tr>
						</thead>
						<tbody>
							<?php					
							$query="SELECT DATE_FORMAT(cs.TANGGAL,'%m-%Y') PERIODE, round(cs.NILAI,1) NILAI FROM db_ppi.tb_cssd_new cs
							WHERE DATE_FORMAT(cs.TANGGAL,'%Y') = DATE_FORMAT(NOW(),'%Y') 
							group by cs.TANGGAL";							
							$info=mysqli_query($conn1,$query); 
								//untuk penomoran data
							$no=1;						
								//menampilkan data
							while($row=mysqli_fetch_array($info)){
								?>
								<tr>   			
									<td><?php echo $row[0]; ?></td> 
									<td><?php echo $row[1]; ?></td> 			
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div><!-- /.row -->
</div>

