<?php
	include '../../config.php';

// Data for Sugar
$query = mysqli_query($conn1,"SELECT COUNT(date_format(ti.TANGGAL,'%Y-%m')) JUMLAH, date_format(ti.TANGGAL,'%m') BULAN, (ROUND(AVG(ti.TOTAL),2)) RATA_RATA
from tb_isk ti 
group by month(TANGGAL)");
$rows = array();
$rows['name'] = 'ISK';
while($tmp= mysqli_fetch_array($query)) {
    $rows['data'][] = $tmp['RATA_RATA'];
}

// Data for Rice
$query = mysqli_query($conn1,"SELECT COUNT(date_format(ti.TANGGAL,'%Y-%m')) JUMLAH, date_format(ti.TANGGAL,'%m') BULAN, (ROUND(AVG(ti.TOTAL),2)) RATA_RATA
from tb_iadp ti 
group by month(TANGGAL)");
$rows1 = array();
$rows1['name'] = 'IADP';
while($tmp = mysqli_fetch_array($query)) {
    $rows1['data'][] = $tmp['RATA_RATA'];
}

// Data for Wheat Flour
$query = mysqli_query($conn1,"SELECT COUNT(date_format(ti.TANGGAL,'%Y-%m')) JUMLAH, date_format(ti.TANGGAL,'%m') BULAN, (ROUND(AVG(ti.TOTAL),2)) RATA_RATA
from tb_ido ti 
group by month(TANGGAL)");
$rows2 = array();
$rows2['name'] = 'IDO';
while($tmp = mysqli_fetch_array($query)) {
    $rows2['data'][] = $tmp['RATA_RATA'];
}

// Data for Wheat Flour
$query = mysqli_query($conn1,"SELECT COUNT(date_format(ti.TANGGAL,'%Y-%m')) JUMLAH, date_format(ti.TANGGAL,'%m') BULAN,(ROUND(AVG(ti.TOTAL),2)) RATA_RATA
from tb_vap ti 
group by month(TANGGAL)");
$rows3 = array();
$rows3['name'] = 'VAP';
while($tmp = mysqli_fetch_array($query)) {
    $rows3['data'][] = $tmp['RATA_RATA'];
}

$result = array();
array_push($result,$rows);
array_push($result,$rows1);
array_push($result,$rows2);
array_push($result,$rows3);

print json_encode($result, JSON_NUMERIC_CHECK);

mysqli_close($conn1);
?> 