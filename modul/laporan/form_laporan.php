<style>
.button {
    background-color: white;
    border: 1px solid #ddd;
    color: #3e4752;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
.button {width: 100%; height:100px}
</style>
<div class="main-content">
	<div class="breadcrumbs ace-save-state" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="ace-icon fa fa-home home-icon"></i>
				<a href="#">Home</a>
			</li>
			<li>
				<a href="#">Laporan</a>
			</li>
			<li class="active">Laporan</li>
		</ul><!-- /.breadcrumb -->			
	</div>
	<div class="page-content">
		<div class="row">
			<div class="col-xs-12 col-sm-12">											
				<div class="tabbable">
					<div class="tabbable">
						<ul class="nav nav-tabs" id="myTab">
							<li class="active">
								<a data-toggle="tab" href="#antibiotik">
									<i class="green ace-icon fa fa-bullseye bigger-120"></i>
									Laporan Antibiotik
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#handy">
								<i class="green ace-icon fa fa-hand-paper-o bigger-120"></i>
									Laporan Handy higien									
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#bundles">
								<i class="green ace-icon fa fa-object-group bigger-120"></i>
									Laporan Bundles								
								</a>
							</li>							
							<li>
								<a data-toggle="tab" href="#kinerja">
								<i class="green ace-icon fa fa-gavel bigger-120"></i>
									Laporan Kinerja									
								</a>
							</li>							
						</ul>

						<div class="tab-content">
							<div id="antibiotik" class="tab-pane fade in active">
								<div class="panel-body">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label no-padding-left" for="form-field-1"> Periode Awal:</label>
											<div class="col-md-7">
												<div class='input-group date' id='datetimepicker'>															
													<input type='text' name="tanggal1" value="<?php echo date('Y/m/d H:i:s') ?>" id="tanggal1" class="form-control" />	
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>															
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label no-padding-left" for="form-field-1-1"> Periode Akhir:</label>
											<div class="col-md-7">
												<div class='input-group date' id='datetimepicker1'>															
													<input type='text' name="tanggal2" value="<?php echo date('Y/m/d H:i:s') ?>" id="tanggal2" class="form-control" />	
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>															
												</div>
											</div>
										</div>
									</div>
									<!--<div class="col-md-2">
										<div class="form-group">
											<div class="col-md-12">
												<select class="chosen-select form-control" name="antibiotik" id="antibiotik" data-placeholder="[ Antibiotik ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data['ID']?>"><?=$data['NAMA_OBAT']?></option>
												<?php
												}
												?>	
												</select>
											</div>
										</div>
									</div>-->
									<div class="col-md-1">
										<div class="form-group">
										<a type="submit" class="btn btn-primary btn-xs" id="cetak">Tampilkan <i class="ace-icon glyphicon glyphicon-play-circle"></i></a>
										</div>	
									</div>	
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading"><i class="fa fa-bar-chart-o"></i> Grafik</div>											
											<div class="panel-body">							
												<div id ="chart_obat"></div>			
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading"><i class="fa fa-table"></i> Laporan</div>
											<div class="panel-body">
												<div id="table_laporan"></div>
											</div>
										</div>
									</div>
									<!--</form>-->	
								</div>							
							</div>
							<div id="handy" class="tab-pane fade">
								<div class="panel-body">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label no-padding-left" for="form-field-1"> Periode Awal:</label>
											<div class="col-md-7">
												<div class='input-group date' id='datetimepicker11'>															
													<input type='text' name="tanggal1" id="tanggal11" class="form-control" />	
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>															
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label no-padding-left" for="form-field-1-1"> Periode Akhir:</label>
											<div class="col-md-7">
												<div class='input-group date' id='datetimepicker12'>															
													<input type='text' name="tanggal12" id="tanggal12" class="form-control" />	
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>															
												</div>
											</div>
										</div>
									</div>
									<!--<div class="col-md-2">
										<div class="form-group">
											<div class="col-md-12">
												<select class="chosen-select form-control" name="antibiotik" id="antibiotik" data-placeholder="[ Antibiotik ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data['ID']?>"><?=$data['NAMA_OBAT']?></option>
												<?php
												}
												?>	
												</select>
											</div>
										</div>
									</div>-->
									<div class="col-md-1">
										<div class="form-group">
										<a type="submit" class="btn btn-primary btn-xs" id="handy">Tampilkan <i class="ace-icon glyphicon glyphicon-play-circle"></i></a>
										</div>	
									</div>	
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading"><i class="fa fa-bar-chart-o"></i> Grafik</div>											
											<div class="panel-body">							
												<div id ="chart_handy"></div>			
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading"><i class="fa fa-table"></i> Laporan</div>
											<div class="panel-body">
												<div id="table_handy"></div>
											</div>
										</div>
									</div>
									<!--</form>-->	
								</div>
							</div>
							<div id="bundles" class="tab-pane fade">
								<div class="panel-body">
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label no-padding-left" for="form-field-1"> Periode Awal:</label>
											<div class="col-md-7">
												<div class='input-group date' id='datetimepicker13'>															
													<input type='text' name="tanggal13" id="tanggal13" class="form-control" />	
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>															
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="col-md-4 control-label no-padding-left" for="form-field-1-1"> Periode Akhir:</label>
											<div class="col-md-7">
												<div class='input-group date' id='datetimepicker14'>															
													<input type='text' name="tanggal14" id="tanggal14" class="form-control" />	
													<span class="input-group-addon">
														<span class="glyphicon glyphicon-calendar"></span>
													</span>															
												</div>
											</div>
										</div>
									</div>
									<!--<div class="col-md-2">
										<div class="form-group">
											<div class="col-md-12">
												<select class="chosen-select form-control" name="antibiotik" id="antibiotik" data-placeholder="[ Antibiotik ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data['ID']?>"><?=$data['NAMA_OBAT']?></option>
												<?php
												}
												?>	
												</select>
											</div>
										</div>
									</div>-->
									<div class="col-md-1">
										<div class="form-group">
										<a type="submit" class="btn btn-primary btn-xs" id="bundles">Tampilkan <i class="ace-icon glyphicon glyphicon-play-circle"></i></a>
										</div>	
									</div>	
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading"><i class="fa fa-bar-chart-o"></i> Grafik</div>											
											<div class="panel-body">							
												<div id ="chart_bundles"></div>			
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading"><i class="fa fa-table"></i> Laporan</div>
											<div class="panel-body">
												<div id="table_bundles"></div>
											</div>
										</div>
									</div>
									<!--</form>-->	
								</div>
							</div>
							
							<div id="kinerja" class="tab-pane fade">
								<p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan DIY, art party locavore wolf cliche high life echo park Austin.</p>
							</div>
						</div>
					</div>				
				</div>
			</div><!-- /.row -->			
		</div>
	</div><!-- /.page-content -->	
</div><!-- /.main-content -->