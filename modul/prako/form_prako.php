<script type="text/javascript">

	function tugas1()
	{	
		var ya = $('#aa:checked').length;
		var tidak = $('#bb:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISI) AS last FROM tb_prako WHERE ID_ISI LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Form</a>
				</li>
				<li class="active">Form Cheklist Pra-Konstruksi</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Form Cheklist Pra-Konstruksi</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_prako" method="post">
									<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" autocomplete="off" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-5">										
											<input type="text" name="AREA" placeholder="[ AREA ]" class="form-control" />
										</div>	

										<div class="col-sm-5">											
											<input type="text" name="PROYEK" placeholder="[ PROYEK ]" class="form-control" />												
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">										
											<input type="text" name="KAINSTALASI" placeholder="[ KA. INSTALASI ]" class="form-control" />
										</div>	

										<div class="col-sm-3">											
											<input type="text" name="PANITIA_PPI" placeholder="[ KA. PANITIA PPI ]" class="form-control" />												
										</div>
										<div class="col-sm-3">										
											<input type="text" name="KONTRAKTOR" placeholder="[ KONTRAKTOR ]" class="form-control" />
										</div>	

										<div class="col-sm-3">											
											<input type="text" name="PETUGAS_K3" placeholder="[ PETUGAS K3 ]" class="form-control" />												
										</div>
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> KRITERIA</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>A</td>
														<td>Apakah konstruksi dapat mempengaruhi akses keluar dari area perawatan yang berbatasan dengan lokasi pembangunan?</td>
														<td align="center"><label>
															<input type="checkbox" name="SATU_A" id="aa" value="1" class="ace input-lg" onClick="tugas1()">								
															<span class="lbl"></span>
														</label></td>
														<td align="center">
															<label>
																<input name="SATU_A" type="checkbox" value="2" id="bb" class="ace input-lg"  onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>B</td>
														<td>Apakah terdapat salah satu dari bahaya lingkungan di bawah ini?</td>
														<td align="center">

														</td>
														<td align="center"></td>
													</tr>
													<tr>
														<td>1</td>
														<td>Asbes</td>
														<td align="center">
															<label>
																<input type="checkbox" name="SATU_B" id="aa" value="1" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_B" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Bahan kimia berbahaya</td>
														<td align="center">
															<label>
																<input name="DUA_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_B" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg" onClick="hitung( this )" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Ruang sempit</td>
														<td align="center">
															<label>
																<input name="TIGA_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TIGA_B" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Lainnya (misalnya masalah pengendalian infeksi)</td>
														<td align="center">
															<label>
																<input name="EMPAT_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="EMPAT_B" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>C</td>
														<td>Apakah salah satu dari sistem berikut ini dapat berdampak buruk?</td>
														<td align="center">
															
														</td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td>1</td>
														<td>Alarm kebakaran</td>
														<td align="center">
															<label>
																<input name="SATU_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_C" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Sprinkle/penyemprot air</td>
														<td align="center">
															<label>
																<input name="DUA_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_C" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr><tr>
														<td>3</td>
														<td>Air Domestik</td>
														<td align="center">
															<label>
																<input name="TIGA_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TIGA_C" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr><tr>
														<td>4</td>
														<td>Oksigen</td>
														<td align="center">
															<label>
																<input name="EMPAT_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="EMPAT_C" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Limbah</td>
														<td align="center">
															<label>
																<input name="LIMA_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="LIMA_C" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Heating Ventilation AIr Conditioner</td>
														<td align="center">
															<label>
																<input name="ENAM_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="ENAM_C" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>D</td>
														<td>Pengendalian Infeksi</td>
														<td align="center">
															
														</td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td></td>
														<td>Melakukan edukasi kepada manajer, staf medis, petugas kesehatan lingkungan, dan staf lain tentang risiko pasien immunesupresi terhadap debu konstruksi</td>
														<td align="center">
															
														</td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td>1</td>
														<td>Kontraktor diberikan salinan, pengelolaan bahan berbahaya, definisi kode darurat, dan dokumentasi lainnya yang harus dikaji untuk mengurangi resiko cedera dan penyakit pada karyawan</td>
														<td align="center">
															<label>
																<input name="SATU_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Dokumen tersebut dikaji bersama kontraktor beserta pertanyaan dan jawabannya</td>
														<td align="center">
															<label>
																<input name="DUA_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Pengkajian lokasi dan metode pemasangan barrier debu sementara</td>
														<td align="center">
															<label>
																<input name="TIGA_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TIGA_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Menilai efisiensi yang berkaitan dengan kemampuan penghambat debu (dust barriers) terhadap pencegahan keluarnya partikulat udara</td>
														<td align="center">
															<label>
																<input name="EMPAT_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="EMPAT_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Menilai efektifitas aliran udara negatif dan sistem filtrasi  </td>
														<td align="center">
															<label>
																<input name="LIMA_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="LIMA_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Terdapat peralatan untuk menangkap pertikulat seperti vakum dan peralatan HEPA yang sesuai dengan urutan kerja </td>
														<td align="center">
															<label>
																<input name="ENAM_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="ENAM_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>7</td>
														<td>Evaluasi rencana pembersihan dan pengendalian </td>
														<td align="center">
															<label>
																<input name="TUJUH_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TUJUH_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>8</td>
														<td>Pengkajian dan evaluasi pola kontrol sirkulasi dan lalulintas </td>
														<td align="center">
															<label>
																<input name="DELAPAN_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DELAPAN_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>9</td>
														<td>Pengkajian pembatasan/larangan untuk kegiatan konstruksi/pembongkaran dengan kontraktor</td>
														<td align="center">
															<label>
																<input name="SEMBILAN_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SEMBILAN_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>10</td>
														<td>Terdapat exhaust fan dan berfungsi dengan baik</td>
														<td align="center">
															<label>
																<input name="SEPULUH_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SEPULUH_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>11</td>
														<td>Terdapat unit filtrasi HEPA di daerah perawatan pasien yang berdekatan dengan area konstruksi dan berfungsi dengan baik</td>
														<td align="center">
															<label>
																<input name="SEBELAS_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SEBELAS_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>12</td>
														<td>Tersedianya ruang isolasi </td>
														<td align="center">
															<label>
																<input name="DUABELAS_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUABELAS_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>13</td>
														<td>Pembahasan permasalahan rumah tangga</td>
														<td align="center">
															<label>
																<input name="TIGABELAS_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TIGABELAS_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>14</td>
														<td>Matras rekat yang tersedia di lokasi</td>
														<td align="center">
															<label>
																<input name="EMPATBELAS_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="EMPATBELAS_D" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>E</td>
														<td>Keselamatan Jiwa</td>
														<td align="center">
															
														</td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td>1</td>
														<td>Apakah ada jalan keluar yang disetujui diblokir?</td>
														<td align="center">
															<label>
																<input name="SATU_E" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_E" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Apakah lalulintas ke emergency Room diblokir?</td>
														<td align="center">
															<label>
																<input name="DUA_E" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_E" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Apakah renovasi mempengaruhi area yang digunakan?</td>
														<td align="center">
															<label>
																<input name="TIGA_E" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TIGA_E" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Apakah modifikasi signifikan terjadi untuk asap atau api dinding penghalang?</td>
														<td align="center">
															<label>
																<input name="EMPAT_E" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="EMPAT_E" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Apakah proyek menambahkan selain sruktur yang ada?</td>
														<td align="center">
															<label>
																<input name="LIMA_E" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="LIMA_E" type="checkbox" value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
															<div class="input-group">
																<input name="TOTAL" type="text" id="total" style="width:60px"></br>
																<span class="input-group-addon">
																	%
																</span>
															</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													
									<div class="form-group">										
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
											<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
										</div>				
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
											<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
										</div>
									</div>
									<div class="form-group">			
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
											<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
										</div>	
										<div class="col-md-6 col-sm-12">	
											<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
											<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
										</div>								
									</div>
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->