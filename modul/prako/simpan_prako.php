<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AREA 			= $_POST['AREA'];						
						$PROYEK 		= $_POST['PROYEK'];
						$KAINSTALASI 	= $_POST['KAINSTALASI'];
						$PANITIA_PPI 	= $_POST['PANITIA_PPI'];
						$KONTRAKTOR 	= $_POST['KONTRAKTOR'];
						$PETUGAS_K3 	= $_POST['PETUGAS_K3'];
						$SATU_A 		= $_POST['SATU_A'];
						$SATU_B 		= $_POST['SATU_B'];
						$DUA_B 			= $_POST['DUA_B'];
						$TIGA_B 		= $_POST['TIGA_B'];
						$EMPAT_B 		= $_POST['EMPAT_B'];
						$SATU_C 		= $_POST['SATU_C'];
						$DUA_C 			= $_POST['DUA_C'];
						$TIGA_C 		= $_POST['TIGA_C'];
						$EMPAT_C 		= $_POST['EMPAT_C'];
						$LIMA_C 		= $_POST['LIMA_C'];
						$ENAM_C 		= $_POST['ENAM_C'];
						$SATU_D 		= $_POST['SATU_D'];
						$DUA_D 			= $_POST['DUA_D'];
						$TIGA_D 		= $_POST['TIGA_D'];
						$EMPAT_D 		= $_POST['EMPAT_D'];
						$LIMA_D 		= $_POST['LIMA_D'];
						$ENAM_D 		= $_POST['ENAM_D'];
						$TUJUH_D 		= $_POST['TUJUH_D'];
						$DELAPAN_D 		= $_POST['DELAPAN_D'];
						$SEMBILAN_D 	= $_POST['SEMBILAN_D'];
						$SEPULUH_D 		= $_POST['SEPULUH_D'];
						$SEBELAS_D 		= $_POST['SEBELAS_D'];
						$DUABELAS_D 	= $_POST['DUABELAS_D'];
						$TIGABELAS_D 	= $_POST['TIGABELAS_D'];
						$EMPATBELAS_D 	= $_POST['EMPATBELAS_D'];
						$SATU_E 		= $_POST['SATU_E'];
						$DUA_E 			= $_POST['DUA_E'];
						$TIGA_E 		= $_POST['TIGA_E'];
						$EMPAT_E 		= $_POST['EMPAT_E'];		
						$LIMA_E 		= $_POST['LIMA_E'];								
						$TOTAL	 		= $_POST['TOTAL'];	
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$AUDITOR		= addslashes($_POST['AUDITOR']);						
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_prako (
							ID_ISI,
							TANGGAL,
							AREA,
							PROYEK,
							KAINSTALASI,
							PANITIA_PPI,
							KONTRAKTOR,
							PETUGAS_K3,
							SATU_A,
							SATU_B,
							DUA_B,
							TIGA_B,
							EMPAT_B,
							SATU_C,
							DUA_C,
							TIGA_C,
							EMPAT_C,
							LIMA_C,
							ENAM_C,
							SATU_D,
							DUA_D,
							TIGA_D,
							EMPAT_D,
							LIMA_D,
							ENAM_D,
							TUJUH_D,
							DELAPAN_D,
							SEMBILAN_D,
							SEPULUH_D,
							SEBELAS_D,
							DUABELAS_D,
							TIGABELAS_D,
							EMPATBELAS_D,
							SATU_E,
							DUA_E,
							TIGA_E,
							EMPAT_E,
							LIMA_E,
							TOTAL,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							AUDITOR,
							USER,
							STATUS) 
						VALUES 
						('$ID_ISI',
							'$TANGGAL',
							'$AREA',
							'$PROYEK',
							'$KAINSTALASI',
							'$PANITIA_PPI',
							'$KONTRAKTOR',
							'$PETUGAS_K3',
							'$SATU_A',
							'$SATU_B',
							'$DUA_B',
							'$TIGA_B',
							'$EMPAT_B',
							'$SATU_C',
							'$DUA_C',
							'$TIGA_C',
							'$EMPAT_C',
							'$LIMA_C',
							'$ENAM_C',
							'$SATU_D',
							'$DUA_D',
							'$TIGA_D',
							'$EMPAT_D',
							'$LIMA_D',
							'$ENAM_D',
							'$TUJUH_D',
							'$DELAPAN_D',
							'$SEMBILAN_D',
							'$SEPULUH_D',
							'$SEBELAS_D',
							'$DUABELAS_D',
							'$TIGABELAS_D',
							'$EMPATBELAS_D',
							'$SATU_E',
							'$DUA_E',
							'$TIGA_E',
							'$EMPAT_E',
							'$LIMA_E', 
							'$TOTAL',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$AUDITOR',	
							'$USER',												
							'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_prako'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AREA 			= $_POST['AREA'];						
						$PROYEK 		= $_POST['PROYEK'];
						$KAINSTALASI 	= $_POST['KAINSTALASI'];
						$PANITIA_PPI 	= $_POST['PANITIA_PPI'];
						$KONTRAKTOR 	= $_POST['KONTRAKTOR'];
						$PETUGAS_K3 	= $_POST['PETUGAS_K3'];
						$SATU_A 		= $_POST['SATU_A'];
						$SATU_B 		= $_POST['SATU_B'];
						$DUA_B 			= $_POST['DUA_B'];
						$TIGA_B 		= $_POST['TIGA_B'];
						$EMPAT_B 		= $_POST['EMPAT_B'];
						$SATU_C 		= $_POST['SATU_C'];
						$DUA_C 			= $_POST['DUA_C'];
						$TIGA_C 		= $_POST['TIGA_C'];
						$EMPAT_C 		= $_POST['EMPAT_C'];
						$LIMA_C 		= $_POST['LIMA_C'];
						$ENAM_C 		= $_POST['ENAM_C'];
						$SATU_D 		= $_POST['SATU_D'];
						$DUA_D 			= $_POST['DUA_D'];
						$TIGA_D 		= $_POST['TIGA_D'];
						$EMPAT_D 		= $_POST['EMPAT_D'];
						$LIMA_D 		= $_POST['LIMA_D'];
						$ENAM_D 		= $_POST['ENAM_D'];
						$TUJUH_D 		= $_POST['TUJUH_D'];
						$DELAPAN_D 		= $_POST['DELAPAN_D'];
						$SEMBILAN_D 	= $_POST['SEMBILAN_D'];
						$SEPULUH_D 		= $_POST['SEPULUH_D'];
						$SEBELAS_D 		= $_POST['SEBELAS_D'];
						$DUABELAS_D 	= $_POST['DUABELAS_D'];
						$TIGABELAS_D 	= $_POST['TIGABELAS_D'];
						$EMPATBELAS_D 	= $_POST['EMPATBELAS_D'];
						$SATU_E 		= $_POST['SATU_E'];
						$DUA_E 			= $_POST['DUA_E'];
						$TIGA_E 		= $_POST['TIGA_E'];
						$EMPAT_E 		= $_POST['EMPAT_E'];		
						$LIMA_E 		= $_POST['LIMA_E'];								
						$TOTAL	 		= $_POST['TOTAL'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$AUDITOR		= addslashes($_POST['AUDITOR']);									
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_prako SET												
						TANGGAL			= '$TANGGAL',
						TGL_UBAH		= '$waktu',
						AREA 			= '$AREA',						
						PROYEK 			= '$PROYEK',
						KAINSTALASI 	= '$KAINSTALASI',
						PANITIA_PPI 	= '$PANITIA_PPI',
						KONTRAKTOR 		= '$KONTRAKTOR',
						PETUGAS_K3 		= '$PETUGAS_K3',
						SATU_A 			= '$SATU_A',
						SATU_B 			= '$SATU_B',
						DUA_B 			= '$DUA_B',
						TIGA_B 			= '$TIGA_B',
						EMPAT_B 		= '$EMPAT_B',
						SATU_C 			= '$SATU_C',
						DUA_C 			= '$DUA_C',
						TIGA_C 			= '$TIGA_C',
						EMPAT_C 		= '$EMPAT_C',
						LIMA_C 			= '$LIMA_C',
						ENAM_C 			= '$ENAM_C',
						SATU_D 			= '$SATU_D',
						DUA_D 			= '$DUA_D',
						TIGA_D 			= '$TIGA_D',
						EMPAT_D 		= '$EMPAT_D',
						LIMA_D 			= '$LIMA_D',
						ENAM_D 			= '$ENAM_D',
						TUJUH_D 		= '$TUJUH_D',
						DELAPAN_D 		= '$DELAPAN_D',
						SEMBILAN_D 		= '$SEMBILAN_D',
						SEPULUH_D 		= '$SEPULUH_D',
						SEBELAS_D 		= '$SEBELAS_D',
						DUABELAS_D 		= '$DUABELAS_D',
						TIGABELAS_D 	= '$TIGABELAS_D',
						EMPATBELAS_D 	= '$EMPATBELAS_D',
						SATU_E 			= '$SATU_E',
						DUA_E 			= '$DUA_E',
						TIGA_E 			= '$TIGA_E',
						EMPAT_E 		= '$EMPAT_E',		
						LIMA_E 			= '$LIMA_E',	
						TOTAL			='$TOTAL',
						ANALISA 		= '$ANALISA', 
						TINDAKLANJUT 	= '$TINDAKLANJUT', 
						KEPALA 			= '$KEPALA',
						AUDITOR			='$AUDITOR',
						DIUBAH_OLEH		='$USER'
						where ID_ISI 	='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_prako'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_prako ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_prako'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			