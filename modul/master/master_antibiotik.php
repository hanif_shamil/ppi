<style>
th {text-align:center}
</style>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Antibiotik</li>
			</ul><!-- /.breadcrumb -->			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Master Antibiotik</h4>
							<div class="widget-toolbar">
								<a data-toggle="modal" data-target="#myModal">
									<i class="ace-icon fa fa-plus"></i>
								</a>
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
	   
						<!-- Modal -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Form Tambah Antibiotik</h4>
									</div>
									<form action="?page=antibiotik" method="POST" role="form">
										<div class="modal-body">								
											<div class="form-group">
											<label for="">Nama Antibiotik</label>
											<input type="text" name="NAMA" class="form-control" id="" placeholder="Nama Antibiotik">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="submit" name="btnsimpan" class="btn btn-primary">Simpan obat</button>
										
										</div>
									</form>
								</div>
							</div>
						</div>
						
						<table id="tabeldata" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th width="10%">NO</th>
									<th>NAMA OBAT</th>
									<th width="20%" class="text-center">AKSI</th>
								</tr>
							</thead>
							<tbody>
								<?php					
								$query="select * from antibiotik ab						
								where ab.`STATUS`=1";							
								$info=mysqli_query($conn1,$query); 
								//untuk penomoran data
								$no=1;						
								//menampilkan data
								while($row=mysqli_fetch_array($info)){
								?>
								<tr>   
									<td><?php echo $no++; ?></td>  
									<td><?php echo $row[1]; ?></td>  
									<td align="center">
										<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#my<?php echo $row[0]; ?>">
										<span class="glyphicon glyphicon-edit" aria-hidden="true"> EDIT</span>
										</button>
									
									<!-- Modal -->										
										<div class="modal fade" id="my<?php echo $row[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Form Update Antibiotik</h4>
													</div>
													<div class="modal-body">
														<form action="?page=antibiotik" method="POST" role="form">
														<div class="form-group">
															<input type="hidden" name="ID" class="form-control" value="<?php echo $row[0]; ?>" id="" placeholder="Input field">
														</div>
														<div class="form-group">
														
															<label for="">Antibiotik</label>
															
															<input type="text" id="form-field-1-1" name="NAMA" value="<?php echo $row[1]; ?>" placeholder="[ Antibiotik ]" class="form-control" />
														
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														<button type="submit" name="edit" class="btn btn-primary">Simpan perubahan</button>
														</form>
													</div>
												</div>
											</div>
										</div>
										<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myy<?php echo $row[0]; ?>">
											<span class="glyphicon glyphicon-trash" aria-hidden="true"> HAPUS</span>
										</button>
									<!-- Modal -->
										<div class="modal fade" id="myy<?php echo $row[0]; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Form Update Jurusan</h4>
												</div>
												<div class="modal-body">												
													Apakah Anda Ingin Menghapus Data <?php echo $row[1]; ?>
													<input type="hidden" name="ID" "<?php echo $row[0]; ?>"class="form-control" id="" placeholder="Nama Antibiotik">
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													<a type="submit" href="?page=antibiotik&id=<?php echo $row[0]?>" class="btn btn-primary">Hapus obat</a>											
												</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
							<?php
							}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>