<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];					
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						$SATU 			= $_POST['SATU'];
						$DUA 			= $_POST['DUA'];
						$TIGA 			= $_POST['TIGA'];
						$EMPAT 			= $_POST['EMPAT'];
						$LIMA 			= $_POST['LIMA'];
						$ENAM 			= $_POST['ENAM'];
						$TUJUH 			= $_POST['TUJUH'];
						$DELAPAN 		= $_POST['DELAPAN'];
						$SEMBILAN 		= $_POST['SEMBILAN'];
						$SEPULUH 		= $_POST['SEPULUH'];
						$SEBELAS  		= $_POST['SEBELAS'];
						$DUABELAS       = $_POST['DUABELAS'];
						$TIGABELAS 		= $_POST['TIGABELAS'];
						$EMPATBELAS 	= $_POST['EMPATBELAS'];
						$LIMABELAS 		= $_POST['LIMABELAS'];
						$ENAMBELAS 		= $_POST['ENAMBELAS'];
						$TUJUHBELAS 	= $_POST['TUJUHBELAS'];
						$DELAPANBELAS 	= $_POST['DELAPANBELAS'];
						$SEMBILANBELAS 	= $_POST['SEMBILANBELAS'];
						$DUAPULUH 		= $_POST['DUAPULUH'];
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_londri (
						ID_ISI,
						TANGGAL,
						AUDITOR,
						SATU,
						DUA,
						TIGA,
						EMPAT,
						LIMA,
						ENAM,
						TUJUH,
						DELAPAN,
						SEMBILAN,
						SEPULUH,
						SEBELAS,
						DUABELAS,
						TIGABELAS,
						EMPATBELAS,
						LIMABELAS,
						ENAMBELAS,
						TUJUHBELAS,
						DELAPANBELAS,
						SEMBILANBELAS,
						DUAPULUH,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',						
						'$AUDITOR',
						'$SATU',
						'$DUA',
						'$TIGA',
						'$EMPAT',
						'$LIMA',
						'$ENAM',
						'$TUJUH',
						'$DELAPAN',
						'$SEMBILAN',
						'$SEPULUH',
						'$SEBELAS',
						'$DUABELAS',
						'$TIGABELAS',
						'$EMPATBELAS',
						'$LIMABELAS',
						'$ENAMBELAS',
						'$TUJUHBELAS',
						'$DELAPANBELAS',
						'$SEMBILANBELAS',
						'$DUAPULUH',
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_londri'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						$SATU 			= $_POST['SATU'];
						$DUA 			= $_POST['DUA'];
						$TIGA 			= $_POST['TIGA'];
						$EMPAT 			= $_POST['EMPAT'];
						$LIMA 			= $_POST['LIMA'];
						$ENAM 			= $_POST['ENAM'];
						$TUJUH 			= $_POST['TUJUH'];
						$DELAPAN 		= $_POST['DELAPAN'];
						$SEMBILAN 		= $_POST['SEMBILAN'];
						$SEPULUH 		= $_POST['SEPULUH'];
						$SEBELAS  		= $_POST['SEBELAS'];
						$DUABELAS       = $_POST['DUABELAS'];
						$TIGABELAS 		= $_POST['TIGABELAS'];
						$EMPATBELAS 	= $_POST['EMPATBELAS'];
						$LIMABELAS 		= $_POST['LIMABELAS'];
						$ENAMBELAS 		= $_POST['ENAMBELAS'];
						$TUJUHBELAS 	= $_POST['TUJUHBELAS'];
						$DELAPANBELAS 	= $_POST['DELAPANBELAS'];
						$SEMBILANBELAS 	= $_POST['SEMBILANBELAS'];
						$DUAPULUH 		= $_POST['DUAPULUH'];
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_londri SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						AUDITOR='$AUDITOR',
						SATU='$SATU',
						DUA='$DUA',
						TIGA='$TIGA',
						EMPAT='$EMPAT',
						LIMA='$LIMA',
						ENAM='$ENAM',
						TUJUH='$TUJUH',
						DELAPAN='$DELAPAN',
						SEMBILAN='$SEMBILAN',
						SEPULUH='$SEPULUH',
						SEBELAS='$SEBELAS',
						DUABELAS='$DUABELAS',
						TIGABELAS='$TIGABELAS',
						EMPATBELAS='$EMPATBELAS',
						LIMABELAS='$LIMABELAS',
						ENAMBELAS='$ENAMBELAS',
						TUJUHBELAS='$TUJUHBELAS',
						DELAPANBELAS='$DELAPANBELAS',
						SEMBILANBELAS='$SEMBILANBELAS',
						DUAPULUH='$DUAPULUH',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_londri'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_londri ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_londri'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			