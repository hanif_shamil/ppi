
<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$SURVEYOR 		= $_POST['SURVEYOR'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						if(isset($_POST['BERSIH_TNGN'])){
							$BERSIH_TNGN = implode(',',$_POST['BERSIH_TNGN']);
						}else{
							$BERSIH_TNGN = "";
						}	
						if(isset($_POST['PASANG_STERIL'])){
							$PASANG_STERIL = implode(',',$_POST['PASANG_STERIL']);
						}else{
							$PASANG_STERIL = "";
						}	
						if(isset($_POST['ADA_TGL'])){
							$ADA_TGL = implode(',',$_POST['ADA_TGL']);
						}else{
							$ADA_TGL = "";
						}	
						if(isset($_POST['KATETER'])){
							$KATETER = implode(',',$_POST['KATETER']);
						}else{
							$KATETER = "";
						}	
						if(isset($_POST['PERINEAL'])){
							$PERINEAL = implode(',',$_POST['PERINEAL']);
						}else{
							$PERINEAL = "";
						}	
						if(isset($_POST['LEPAS_KATETER'])){
							$LEPAS_KATETER = implode(',',$_POST['LEPAS_KATETER']);
						}else{
							$LEPAS_KATETER = "";
						}	
						$USER 			= $_SESSION['userid'];	
						$TOTAL	 		= $_POST['TOTAL'];																	
						//echo "<pre>";print_r($_POST);exit();                            						
                       $query = "INSERT INTO db_ppi.tb_isk (
						ID_ISI,
						TANGGAL,
						RUANGAN,
						SURVEYOR,
						NOMR,
						NAMA,
						BERSIH_TNGN,
						PASANG_STERIL,
						ADA_TGL,
						KATETER,
						PERINEAL,
						LEPAS_KATETER,
						USER,
						TOTAL,						
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$RUANGAN',
						'$SURVEYOR',
						'$NOMR',
						'$NAMA',
						'$BERSIH_TNGN',
						'$PASANG_STERIL',	
						'$ADA_TGL',		
						'$KATETER',				
						'$PERINEAL',
						'$LEPAS_KATETER',
						'$USER',						
						'$TOTAL',																		
						'1')";
						//echo $query; die();		
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=isk'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$SURVEYOR 		= $_POST['SURVEYOR'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						if(isset($_POST['BERSIH_TNGN'])){
							$BERSIH_TNGN = implode(',',$_POST['BERSIH_TNGN']);
						}else{
							$BERSIH_TNGN = "";
						}	
						if(isset($_POST['PASANG_STERIL'])){
							$PASANG_STERIL = implode(',',$_POST['PASANG_STERIL']);
						}else{
							$PASANG_STERIL = "";
						}	
						if(isset($_POST['ADA_TGL'])){
							$ADA_TGL = implode(',',$_POST['ADA_TGL']);
						}else{
							$ADA_TGL = "";
						}	
						if(isset($_POST['KATETER'])){
							$KATETER = implode(',',$_POST['KATETER']);
						}else{
							$KATETER = "";
						}	
						if(isset($_POST['PERINEAL'])){
							$PERINEAL = implode(',',$_POST['PERINEAL']);
						}else{
							$PERINEAL = "";
						}	
						if(isset($_POST['LEPAS_KATETER'])){
							$LEPAS_KATETER = implode(',',$_POST['LEPAS_KATETER']);
						}else{
							$LEPAS_KATETER = "";
						}	
						$USER 			= $_SESSION['userid'];	
						$TOTAL	 		= $_POST['TOTAL'];				
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   					
                        $query2 = "UPDATE db_ppi.tb_isk SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						SURVEYOR='$SURVEYOR',
						NOMR='$NOMR',
						NAMA='$NAMA',
						BERSIH_TNGN='$BERSIH_TNGN',
						PASANG_STERIL='$PASANG_STERIL',	
						ADA_TGL='$ADA_TGL',		
						KATETER='$KATETER',				
						PERINEAL='$PERINEAL',
						LEPAS_KATETER='$LEPAS_KATETER',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_isk'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){						
						$query1 = "UPDATE db_ppi.tb_isk ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_isk'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?page=tabel_isk'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			