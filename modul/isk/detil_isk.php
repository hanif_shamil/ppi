<?php
$id=($_GET['id']);
$query="select ti.ID_ISI, ti.TGL_INPUT, ti.TANGGAL, ti.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, ti.NOMR, ti.NAMA, ti.SURVEYOR
,ti.BERSIH_TNGN, ti.PASANG_STERIL, ti.ADA_TGL, ti.KATETER, ti.PERINEAL, ti.LEPAS_KATETER, ti.USER, ti.TOTAL, ti.`STATUS`
from tb_isk ti
left join master.ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5			
where ti.ID_ISI='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$BERSIH_TNGN = explode(",", $dt['BERSIH_TNGN']);$PASANG_STERIL = explode(",", $dt['PASANG_STERIL']);$ADA_TGL = explode(",", $dt['ADA_TGL']);
$KATETER = explode(",", $dt['KATETER']);$PERINEAL = explode(",", $dt['PERINEAL']);$LEPAS_KATETER = explode(",", $dt['LEPAS_KATETER']);
  {
	  ?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Bundles ISK</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Bundles ISK</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_isk" method="post">
								<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $dt['ID_ISI']; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input value="<?php echo $dt['TANGGAL']; ?>" type="text" style="width:100%" disabled>	
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">										
											<input value="<?php echo $dt['RUANGAN']; ?>" type="text" style="width:100%" disabled>	
										</div>	
										<div class="col-sm-1">
											<input value="<?php echo $dt['NOMR']; ?>" type="text" style="width:100%" disabled>													
										</div>	
										<div class="col-sm-3">									
											<input value="<?php echo $dt['NAMA']; ?>" type="text" style="width:100%" disabled>												
										</div>																			
										<div class="col-sm-3">											
											<input value="<?php echo $dt['SURVEYOR']; ?>" type="text" style="width:100%" disabled>													
										</div>
									</div>										
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> BUNDLES ISK</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Melakukan kebersihan tangan</td>
														<td align="center"><label>
															<input name="BERSIH_TNGN[]" type="checkbox" <?php if(in_array("1", $BERSIH_TNGN)){ echo " checked=\"checked\""; } ?> id="BERSIH_TNGN" value="1" class="ace input-lg" disabled />
															
															<span class="lbl"></span>
															</label></td>
														<td align="center">
															<label>
															<input name="BERSIH_TNGN[]" type="checkbox" <?php if(in_array("2", $BERSIH_TNGN)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg" disabled />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Prinsip pemasangan steril</td>
														<td align="center"><label>
													<input name="PASANG_STERIL[]" type="checkbox" <?php if(in_array("1", $PASANG_STERIL)){ echo " checked=\"checked\""; } ?> id="PASANG_STERIL" value="1" class="ace input-lg" disabled />
													<span class="lbl"></span>
												</label></td>
														<td align="center"><label>
													<input name="PASANG_STERIL[]" type="checkbox" <?php if(in_array("2", $PASANG_STERIL)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg" disabled />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Ada tanggal pemasangan</td>
														<td align="center">
														<label>
													<input name="ADA_TGL[]" type="checkbox" <?php if(in_array("1", $ADA_TGL)){ echo " checked=\"checked\""; } ?> id="ADA_TGL" value="1" class="ace input-lg" disabled />
													<span class="lbl"></span>
												</label></td>
														<td align="center">
														<label>
													<input name="ADA_TGL[]" type="checkbox" <?php if(in_array("2", $ADA_TGL)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg" disabled />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>4</td>
														<td>Kateter difiksasi</td>
														<td align="center">
															<label>
															<input name="KATETER[]" type="checkbox" <?php if(in_array("1", $KATETER)){ echo " checked=\"checked\""; } ?> id="KATETER" value="1" class="ace input-lg" disabled />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="KATETER[]" type="checkbox" <?php if(in_array("2", $KATETER)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg" disabled />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Melakukan perineal hygiene</td>
														<td align="center">
															<label>
															<input name="PERINEAL[]" type="checkbox" <?php if(in_array("1", $PERINEAL)){ echo " checked=\"checked\""; } ?> id="PERINEAL" value="1" class="ace input-lg" disabled />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="PERINEAL[]" type="checkbox" <?php if(in_array("2", $PERINEAL)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg" disabled />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Lepaskan kateter sesingkat mungkin jika tidak ada indikasi </td>
														<td align="center">
															<label>
															<input name="LEPAS_KATETER[]" type="checkbox" <?php if(in_array("1", $LEPAS_KATETER)){ echo " checked=\"checked\""; } ?> id="LEPAS_KATETER" value="1" class="ace input-lg" disabled />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="LEPAS_KATETER[]" type="checkbox" <?php if(in_array("2", $LEPAS_KATETER)){ echo " checked=\"checked\""; } ?> value="2" id="1" class="ace input-lg" disabled />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
														<div class="input-group">
															<input name="TOTAL" value="<?php echo $dt['TOTAL']; ?>" disabled type="text" id="total" style="width:60px"></br>
															<span class="input-group-addon">
																%
															</span>
														</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>																						
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->
<?php
  }?>