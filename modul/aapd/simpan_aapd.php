<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];						
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['TOPI_ADA'])){
							$TOPI_ADA = implode(',',$_POST['TOPI_ADA']);
						}else{
							$TOPI_ADA = "";
						}		
						if(isset($_POST['TOPI_SESUAI'])){
							$TOPI_SESUAI = implode(',',$_POST['TOPI_SESUAI']);
						}else{
							$TOPI_SESUAI = "";
						}	
						if(isset($_POST['APRON_ADA'])){
							$APRON_ADA = implode(',',$_POST['APRON_ADA']);
						}else{
							$APRON_ADA = "";
						}	
						if(isset($_POST['APRON_SESUAI'])){
							$APRON_SESUAI = implode(',',$_POST['APRON_SESUAI']);
						}else{
							$APRON_SESUAI = "";
						}	
						if(isset($_POST['GAUN_ADA'])){
							$GAUN_ADA = implode(',',$_POST['GAUN_ADA']);
						}else{
							$GAUN_ADA = "";
						}
						if(isset($_POST['GAUN_SESUAI'])){
							$GAUN_SESUAI = implode(',',$_POST['GAUN_SESUAI']);
						}else{
							$GAUN_SESUAI = "";
						}
						if(isset($_POST['SARUNG_ADA'])){
							$SARUNG_ADA = implode(',',$_POST['SARUNG_ADA']);
						}else{
							$SARUNG_ADA = "";
						}
						if(isset($_POST['SARUNG_SESUAI'])){
							$SARUNG_SESUAI = implode(',',$_POST['SARUNG_SESUAI']);
						}else{
							$SARUNG_SESUAI = "";
						}
						if(isset($_POST['MASKER_ADA'])){
							$MASKER_ADA = implode(',',$_POST['MASKER_ADA']);
						}else{
							$MASKER_ADA = "";
						}
						if(isset($_POST['MASKER_SESUAI'])){
							$MASKER_SESUAI = implode(',',$_POST['MASKER_SESUAI']);
						}else{
							$MASKER_SESUAI = "";
						}
						if(isset($_POST['SEPATU_ADA'])){
							$SEPATU_ADA = implode(',',$_POST['SEPATU_ADA']);
						}else{
							$SEPATU_ADA = "";
						}
						if(isset($_POST['SEPATU_SESUAI'])){
							$SEPATU_SESUAI = implode(',',$_POST['SEPATU_SESUAI']);
						}else{
							$SEPATU_SESUAI = "";
						}
						if(isset($_POST['KACAMATA_ADA'])){
							$KACAMATA_ADA = implode(',',$_POST['KACAMATA_ADA']);
						}else{
							$KACAMATA_ADA = "";
						}
						if(isset($_POST['KACAMATA_SESUAI'])){
							$KACAMATA_SESUAI = implode(',',$_POST['KACAMATA_SESUAI']);
						}else{
							$KACAMATA_SESUAI = "";
						}
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_aapd (
						ID_ISI,
						TANGGAL,
						RUANGAN,
						AUDITOR,
						TOPI_ADA,
						TOPI_SESUAI,
						APRON_ADA,
						APRON_SESUAI,
						GAUN_ADA,
						GAUN_SESUAI,
						SARUNG_ADA,
						SARUNG_SESUAI,
						MASKER_ADA,
						MASKER_SESUAI,
						SEPATU_ADA,
						SEPATU_SESUAI,
						KACAMATA_ADA,
						KACAMATA_SESUAI,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$RUANGAN',
						'$AUDITOR',
						'$TOPI_ADA',
						'$TOPI_SESUAI',	
						'$APRON_ADA',		
						'$APRON_SESUAI',				
						'$GAUN_ADA',	
						'$GAUN_SESUAI',	
						'$SARUNG_ADA',	
						'$SARUNG_SESUAI',	
						'$MASKER_ADA',	
						'$MASKER_SESUAI',	
						'$SEPATU_ADA',	
						'$SEPATU_SESUAI',	
						'$KACAMATA_ADA',	
						'$KACAMATA_SESUAI',	
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_aapd'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);								
						$TOPI_ADA 		= $_POST['TOPI_ADA'];					
						$TOPI_SESUAI 	= $_POST['TOPI_SESUAI'];					
						$APRON_ADA 		= $_POST['APRON_ADA'];					
						$APRON_SESUAI 	= $_POST['APRON_SESUAI'];					
						$GAUN_ADA 		= $_POST['GAUN_ADA'];				
						$GAUN_SESUAI 	= $_POST['GAUN_SESUAI'];					
						$SARUNG_ADA 	= $_POST['SARUNG_ADA'];					
						$SARUNG_SESUAI 	= $_POST['SARUNG_SESUAI'];					
						$MASKER_ADA 	= $_POST['MASKER_ADA'];					
						$MASKER_SESUAI 	= $_POST['MASKER_SESUAI'];					
						$SEPATU_ADA 	= $_POST['SEPATU_ADA'];					
						$SEPATU_SESUAI 	= $_POST['SEPATU_SESUAI'];
						$KACAMATA_ADA 	= $_POST['KACAMATA_ADA'];
						$KACAMATA_SESUAI = $_POST['KACAMATA_SESUAI'];
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_aapd SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						AUDITOR='$AUDITOR',
						TOPI_ADA = '$TOPI_ADA',						
						TOPI_SESUAI = '$TOPI_SESUAI',
						APRON_ADA = '$APRON_ADA',
						APRON_SESUAI = '$APRON_SESUAI',					
						GAUN_ADA = '$GAUN_ADA',				
						GAUN_SESUAI = '$GAUN_SESUAI',					
						SARUNG_ADA = '$SARUNG_ADA',
						SARUNG_SESUAI = '$SARUNG_SESUAI',
						MASKER_ADA = '$MASKER_ADA',
						MASKER_SESUAI = '$MASKER_SESUAI',
						SEPATU_ADA = '$SEPATU_ADA',
						SEPATU_SESUAI = '$SEPATU_SESUAI',
						KACAMATA_ADA = '$KACAMATA_ADA',
						KACAMATA_SESUAI = '$KACAMATA_SESUAI',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_aapd'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_aapd ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_aapd'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			