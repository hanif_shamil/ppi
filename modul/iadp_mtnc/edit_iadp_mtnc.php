<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<?php
$id=($_GET['id']);
$query="SELECT * FROM db_ppi.tb_iadp_mtnc WHERE ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">Formulir Audit Bundles IADP : Maintenance</li>
				</ul><!-- /.breadcrumb -->


			</div>

			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Formulir Audit Bundles IADP : Maintenance</h4>

								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>

									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_iadp_mtnc" method="post">
										<input type="hidden" name="ID" class="form-control" value="<?php echo $dt['ID']; ?>" readonly>										
										<div class="form-group">										
											<div class="col-sm-2">
												<div class="input-group">
													<input class="form-control" value="<?php echo $dt['TANGGAL']; ?>" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-3">										
												<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" value="<?php echo $dt['RUANGAN']; ?>" data-placeholder="[ RUANGAN ]">
													<option value=""></option>
													<?php
													$sql = "select * from db_ppi.ruangan r
													where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
													$rs = mysqli_query($conn1,$sql);
													while ($data = mysqli_fetch_array($rs)) {
														?>
														<option <?php if($dt['RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
														<?php
													}
													?>	
												</select>
											</div>	
											<div class="col-sm-2">
												<input type="text" id="nomr" value="<?php echo $dt['NOMR']; ?>" name="NOMR" placeholder="[ MR ]" class="form-control" onkeyup="autofill()" />												
											</div>	
											<div class="col-sm-3">									
												<input type="text" id="nama" value="<?php echo $dt['NAMA']; ?>" name="NAMA" placeholder="[ NAMA ]" class="form-control" />												
											</div>																			
											<div class="col-sm-2">											
												<input type="text" name="AUDITOR" value="<?php echo $dt['AUDITOR']; ?>" placeholder="[ AUDITOR ]" class="form-control" />												
											</div>
										</div>									
										<hr />

										<div class="form-group">
											<div class="col-sm-12">											
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="text-align:center">NO</th>
															<th style="text-align:center"> BUNDLES IADP</th>
															<th width="10%" style="text-align:center">YA</th>
															<th width="10%" style="text-align:center">TIDAK</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>Melakukan kebersihan tangan</td>
															<td align="center"><label>
																<input type="radio" name="SATU[]" <?php if($dt['SATU']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">																																													
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU[]" type="radio" <?php if($dt['SATU']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Mengganti transparent dressing (ganti balutan ) setiap 7 hari (kecuali bila basah, kotor atau rusak/lepas harus segera diganti)</td>
														<td align="center">
															<label>
																<input type="radio" name="DUA[]" <?php if($dt['DUA']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">													
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center"><label>
															<input name="DUA[]" type="radio" <?php if($dt['DUA']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Mempertahankan teknik aseptic saat melakukan perawatan CVP atau ganti balutan</td>
													<td align="center">
														<label>
															<input type="radio" name="TIGA[]" <?php if($dt['TIGA']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="TIGA[]" type="radio" <?php if($dt['TIGA']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>4</td>
													<td>Menggunakan cairan chlorhexidine 2% mix alkohol 70%  untuk membersihkan area insersi dan lumen CVP saat melakukan perawatan cvp</td>
													<td align="center">
														<label>
															<input name="EMPAT[]" type="radio" <?php if($dt['EMPAT']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="EMPAT[]" type="radio" <?php if($dt['EMPAT']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>5</td>
													<td>Desinfeksi dengan swab alkohol 70% pada sambungan IV catheter, port dan konector threeway saat menyuntik obat atau menyambungkan line baru untuk pemberian terapi atau transfusi</td>
													<td align="center">
														<label>
															<input name="LIMA[]" type="radio" <?php if($dt['LIMA']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="LIMA[]" type="radio" <?php if($dt['LIMA']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>6</td>
													<td>Tulis tanggal dan jam setiap kali mengganti balutan dressing</td>
													<td align="center">
														<label>
															<input name="ENAM[]" type="radio" <?php if($dt['ENAM']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="ENAM[]" type="radio" <?php if($dt['ENAM']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td style="text-align:center" colspan="2">Total</td>
													<td style="text-align:center"></td>
													<td style="text-align:center"></td>
												</tr>
												<tr>
													<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
													<td colspan="2" style="text-align:center">
														<div class="input-group">
															<input name="TOTAL" type="text" value="<?php echo $dt['TOTAL']; ?>" id="total" style="width:100%"></br>
															<span class="input-group-addon">
																%
															</span>
														</div>														
													</td>														
												</tr>														
											</tbody>
										</table>											
									</div>
								</div>													
								<div class="form-group">										
									<div class="col-md-6 col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
										<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
									</div>				
									<div class="col-md-6 col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
										<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
									</div>
								</div>
								<div class="form-group">			
									<div class="col-md-6 col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
										<input type="text" id="nama" value="<?php echo $dt['KEPALA']; ?>" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
									</div>	
									<div class="col-md-6 col-sm-12">	
										<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
										<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
									</div>								
								</div>	
								<hr>
								<div class="form-group">							
									<div class="col-md-offset-3 col-md-9">
										<button class="btn btn-info" name="btnEdit" type="submit">
											<i class="ace-icon fa fa-check bigger-110"></i>
											Submit
										</button>
										&nbsp; &nbsp; &nbsp;
										<button class="btn" type="reset">
											%
											Reset
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div><!-- /.span -->
		</div>
	</div><!-- /.page-content -->	
</div> <!-- container -->
</div><!-- /.main-content -->
<?php } ?>