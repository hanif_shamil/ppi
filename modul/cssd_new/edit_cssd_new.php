<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>

<?php
$id=($_GET['id']);
$query="select * from db_ppi.tb_cssd_new where ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>
					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">FORMULIR AUDIT  CSSD  RUMAH SAKIT </li>
				</ul><!-- /.breadcrumb -->
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">FORMULIR AUDIT  CSSD  RUMAH SAKIT </h4>
								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>
									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_cssd_new" method="post">
										<input type="hidden" value="<?php echo $dt['ID']; ?>"  name="ID" >										
										<div class="form-group">
											<div class="col-sm-1">	
												<label class="control-label bolder blue">TANGGAL</label>
											</div>								
											<div class="col-sm-3">									
												<div class="input-group">
													<input class="form-control" value="<?php echo $dt['TANGGAL']; ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>												
											</div>
										</div>

										<div class="form-group">																				
											<div class="col-sm-12">
												<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
													<table class="table table-bordered">
														<thead>
															<tr>
																<th width="5%" style="text-align:center">NO</th>
																<th style="text-align:center"> KEGIATAN</th>
																<th width="5%" style="text-align:center">YA</th>
																<th width="5%" style="text-align:center">TIDAK</th>
																<th width="5%" style="text-align:center">NA</th>
																<th width="25%" style="text-align:center">KETERANGAN</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>A.</td>
																<td>KEPATUHAN PENGGUNAAN APD DI RUANG DEKONTAMINASI</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Penutup Kepala</td>
																<td align="center">
																	<label><input type="radio" <?php if($dt['A1']=='1') echo " checked "?> name="A1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A1" <?php if($dt['A1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A1" <?php if($dt['A1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_A1']; ?>" id="nama" name="KET_A1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2
																</td>
																<td>Masker
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['A2']=='1') echo " checked "?> name="A2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['A2']=='2') echo " checked "?> name="A2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['A2']=='3') echo " checked "?> name="A2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_A2']; ?>" id="nama" name="KET_A2" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Apron</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['A3']=='1') echo " checked "?> name="A3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['A3']=='2') echo " checked "?> name="A3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['A3']=='3') echo " checked "?> name="A3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_A3']; ?>" id="nama" name="KET_A3" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Sarung tangan</td>
																<td align="center">
																	<label>
																		<input name="A4" <?php if($dt['A4']=='1') echo " checked "?> type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A4" <?php if($dt['A4']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A4" <?php if($dt['A4']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_A4']; ?>" id="nama" name="KET_A4" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>5
																</td>
																<td>Safety Shoes
																</td>
																<td align="center">
																	<label>
																		<input name="A5" <?php if($dt['A5']=='1') echo " checked "?> type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A5" <?php if($dt['A5']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A5" <?php if($dt['A5']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_A5']; ?>" id="nama" name="KET_A5" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>6</td>
																<td>Kaca Mata Google/ Face Shield</td>
																<td align="center">
																	<label>
																		<input name="A6" <?php if($dt['A6']=='1') echo " checked "?> type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A6" <?php if($dt['A6']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A6" <?php if($dt['A6']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_A6']; ?>" id="nama" name="KET_A6" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>7</td>
																<td>Ear Plug/ Ear Muff</td>
																<td align="center">
																	<label>
																		<input name="A7" <?php if($dt['A7']=='1') echo " checked "?> type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A7" <?php if($dt['A7']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="A7" <?php if($dt['A7']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_A7']; ?>" id="nama" name="KET_A7" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>B.</td>
																<td>PROSES PEMBERSIHAN DAN PENCUCIAN</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Membersihkan instrumen dari kotoran</td>
																<td align="center"><label>
																	<input type="radio" <?php if($dt['B1']=='1') echo " checked "?> name="B1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>
																<td align="center">
																	<label>
																		<input name="B1" <?php if($dt['B1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="B1" <?php if($dt['B1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_B1']; ?>" id="nama" name="KET_B1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2
																</td>
																<td>Semua instrumen terendam didalam cairan desinfektan
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B2']=='1') echo " checked "?> name="B2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B2']=='2') echo " checked "?> name="B2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B2']=='3') echo " checked "?> name="B2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_B2']; ?>" id="nama" name="KET_B2" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>3
																</td>
																<td>Instrumen di sikat sampai sela-selanya
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B3']=='1') echo " checked "?> name="B3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B3']=='2') echo " checked "?> name="B3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B3']=='3') echo " checked "?> name="B3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_B3']; ?>" id="nama" name="KET_B3" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>4
																</td>
																<td>Instrumen di bilas dengan air mengalir 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B4']=='1') echo " checked "?> name="B4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B4']=='2') echo " checked "?> name="B4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B4']=='3') echo " checked "?> name="B4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_B4']; ?>" id="nama" name="KET_B4" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>5
																</td>
																<td>Instrumen di bilas dengan air panas suhu 60-80 derajat
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B5']=='1') echo " checked "?> name="B5" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B5']=='2') echo " checked "?> name="B5" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B5']=='3') echo " checked "?> name="B5" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_B5']; ?>" id="nama" name="KET_B5" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>6
																</td>
																<td>Instrumen di bilas dengan air RO 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B6']=='1') echo " checked "?> name="B6" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B6']=='2') echo " checked "?> name="B6" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B6']=='3') echo " checked "?> name="B6" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_B6']; ?>" id="nama" name="KET_B6" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>7
																</td>
																<td>Dikeringkan dengan kompresor
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B7']=='1') echo " checked "?> name="B7" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B7']=='2') echo " checked "?> name="B7" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B7']=='3') echo " checked "?> name="B7" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_B7']; ?>" id="nama" name="KET_B7" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>8
																</td>
																<td>Pengenceran cairan desinfektan sesuai takaran penggunaan
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B8']=='1') echo " checked "?> name="B8" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B8']=='2') echo " checked "?> name="B8" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B8']=='3') echo " checked "?> name="B8" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_B8']; ?>" id="nama" name="KET_B8" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>9
																</td>
																<td>Eye washer berfungsi dengan baik
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B9']=='1') echo " checked "?> name="B9" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B9']=='2') echo " checked "?> name="B9" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['B9']=='3') echo " checked "?> name="B9" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_B9']; ?>" id="nama" name="KET_B9" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>C.</td>
																<td>Desinfeksi dan Sterilisasi </td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1
																</td>
																<td>Instrumen medis didesinfeksi dengan benar
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1" id="AA" <?php if($dt['C1']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																		</label
																	</td>
																	<td align="center">
																		<label>
																			<input name="C1" type="radio" value="2" <?php if($dt['C1']=='2') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td align="center">
																		<label>
																			<input name="C1" type="radio" value="3" <?php if($dt['C1']=='3') echo " checked "?> id="AA" class="ace input-lg" onClick="tugas1()" />
																			<span class="lbl"></span>
																		</label>
																	</td>
																	<td>
																		<input type="text" id="nama" value="<?php echo $dt['KET_C1']; ?>" name="KET_C1" placeholder="" class="form-control" />		
																	</td>
																</tr>
																<tr>
																	<td>2
																	</td>
																	<td>Setiap proses sterilisasi divalidasi dengan benar (uji Bowie Dick -Uji Biologis -Parameter  fisik -indikator kimia)
																	</td>
																	<td align="center">
																		<label>
																			<input type="radio" name="C2" <?php if($dt['C2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																			<span class="lbl"></span>
																			</label
																		</td>
																		<td align="center">
																			<label>
																				<input name="C2" type="radio" <?php if($dt['C2']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																				<span class="lbl"></span>
																			</label>
																		</td>
																		<td align="center">
																			<label>
																				<input name="C2" type="radio" <?php if($dt['C2']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																				<span class="lbl"></span>
																			</label>
																		</td>
																		<td>
																			<input type="text" id="nama" value="<?php echo $dt['KET_C2']; ?>" name="KET_C2" placeholder="" class="form-control" />		
																		</td>
																	</tr>
																	<tr>
																		<td>D.</td>
																		<td>PENYIMPANAN </td>
																		<td></td>
																		<td></td>
																		<td></td>
																	</tr>
																	<tr>
																		<td>1
																		</td>
																		<td>Ruang penyimpanan menggunakan tekanan positif dengan hepafilter, suhu 18 - 25 ºC,  kelembaban 45 - 70%
																		</td>
																		<td align="center">
																			<label>
																				<input type="radio" <?php if($dt['D1']=='1') echo " checked "?> name="D1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																				<span class="lbl"></span>
																				</label
																			</td>
																			<td align="center">
																				<label>
																					<input name="D1" <?php if($dt['D1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td align="center">
																				<label>
																					<input name="D1" <?php if($dt['D1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td>
																				<input type="text" value="<?php echo $dt['KET_D1']; ?>" id="nama" name="KET_D1" placeholder="" class="form-control" />		
																			</td>
																		</tr>
																		<tr>
																			<td>2</td>
																			<td>Rak terbuat dari stainless dengan jarak dari dinding 5 cm, dari lantai 19-24 cm, dari langit-langit minimal 43 cm</td>
																			<td align="center">
																				<label>
																					<input type="radio" <?php if($dt['D2']=='1') echo " checked "?> name="D2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																					<span class="lbl"></span>
																				</label>
																			</td>

																			<td align="center">
																				<label>
																					<input type="radio" <?php if($dt['D2']=='2') echo " checked "?> name="D2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td align="center">
																				<label>
																					<input type="radio" <?php if($dt['D2']=='3') echo " checked "?> name="D2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td>
																				<input type="text" value="<?php echo $dt['KET_D2']; ?>" id="nama" name="KET_D2" placeholder="" class="form-control" />	
																			</td>
																		</tr>
																		<tr>
																			<td>3</td>
																			<td>Susunan menggunakan sistem FIFO</td>
																			<td align="center">
																				<label>
																					<input type="radio" <?php if($dt['D3']=='1') echo " checked "?> name="D3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td align="center">
																				<label>
																					<input type="radio" <?php if($dt['D3']=='2') echo " checked "?> name="D3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td align="center">
																				<label>
																					<input type="radio" <?php if($dt['D3']=='3') echo " checked "?> name="D3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td>
																				<input type="text" value="<?php echo $dt['KET_D3']; ?>" id="nama" name="KET_D3" placeholder="" class="form-control" />		
																			</td>
																		</tr>
																		<tr>
																			<td>E.</td>
																			<td>KESESUAIAN ALUR</td>
																			<td></td>
																			<td></td>
																			<td></td>
																		</tr>
																		<tr>
																			<td>1</td>
																			<td>Kepatuhan pengiriman alat kotor dan alat bersih terpisah sesuai alur</td>
																			<td align="center"><label>
																				<input type="radio" <?php if($dt['E1']=='1') echo " checked "?> name="E1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																				<span class="lbl"></span>
																			</label></td>

																			<td align="center">
																				<label>
																					<input name="E1" <?php if($dt['E1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td align="center">
																				<label>
																					<input name="E1" <?php if($dt['E1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td>
																				<input type="text" value="<?php echo $dt['KET_E1']; ?>" id="nama" name="KET_E1" placeholder="" class="form-control" />		
																			</td>
																		</tr>
																		<tr>
																			<td>2</td>
																			<td>Lokasi area dekontaminasi , pengemasan,sterilisasi, penyimpanan dan distribusi terpisah sesuai alur</td>
																			<td align="center">
																				<label>
																					<input type="radio" <?php if($dt['E2']=='1') echo " checked "?> name="E2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td align="center">
																				<label>
																					<input type="radio" <?php if($dt['E2']=='2') echo " checked "?> name="E2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td align="center">
																				<label>
																					<input type="radio" <?php if($dt['E2']=='3') echo " checked "?> name="E2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																					<span class="lbl"></span>
																				</label>
																			</td>
																			<td>
																				<input type="text" value="<?php echo $dt['KET_E2']; ?>" id="nama" name="KET_E2" placeholder="" class="form-control" />	
																			</td>
																		</tr>									
																		<tr>
																			<td style="text-align:center" colspan="4">JUMLAH TOTAL</td>
																			<td colspan="3" style="text-align:center">
																				<div class="input-group">
																					<input type="text" id="total" name="NILAI" value="<?php echo $dt['NILAI']; ?>" placeholder="[ NILAI ]" class="form-control" />
																					<span class="input-group-addon">
																						%
																					</span>
																				</div>													
																			</td>														
																		</tr>													
																	</tbody>
																</table>																									
															</div>
														</div>
													</div>																										
													<hr />
													<div class="form-group">										
														<div class="col-md-6 col-sm-12">
															<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
															<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
														</div>				
														<div class="col-md-6 col-sm-12">
															<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
															<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
														</div>
													</div>
													<div class="form-group">			
														<div class="col-md-6 col-sm-12">
															<label class="control-label bolder blue" style="text-decoration: underline">KOORDINATOR CSSD</label>	
															<input type="text" id="nama" value="<?php echo $dt['KOORDINATOR']; ?>" name="KOORDINATOR" placeholder="[ KOORDINATOR CSSD ]" class="form-control" />												
														</div>	
														<div class="col-md-6 col-sm-12">	
															<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
															<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
														</div>								
													</div>																											
													<hr />
													<div class="form-group">							
														<div class="col-md-offset-3 col-md-9">
															<button class="btn btn-info" name="btnEdit" type="submit">
																<i class="ace-icon fa fa-check bigger-110"></i>
																Submit
															</button>
															&nbsp; &nbsp; &nbsp;
															<button class="btn" type="reset">
																<i class="ace-icon fa fa-undo bigger-110"></i>
																Reset
															</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div><!-- /.span -->
							</div>
						</div><!-- /.page-content -->	
					<?php }
				?>