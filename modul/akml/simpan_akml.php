<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];						
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['PADAT_INFEKSI'])){
							$PADAT_INFEKSI = implode(',',$_POST['PADAT_INFEKSI']);
						}else{
							$PADAT_INFEKSI = "";
						}		
						if(isset($_POST['PADAT_NONINFEKSI'])){
							$PADAT_NONINFEKSI = implode(',',$_POST['PADAT_NONINFEKSI']);
						}else{
							$PADAT_NONINFEKSI = "";
						}	
						if(isset($_POST['CAIR_INFEKSI'])){
							$CAIR_INFEKSI = implode(',',$_POST['CAIR_INFEKSI']);
						}else{
							$CAIR_INFEKSI = "";
						}	
						if(isset($_POST['CAIR_NONINFEKSI'])){
							$CAIR_NONINFEKSI = implode(',',$_POST['CAIR_NONINFEKSI']);
						}else{
							$CAIR_NONINFEKSI = "";
						}	
						if(isset($_POST['BENDA_TAJAM'])){
							$BENDA_TAJAM = implode(',',$_POST['BENDA_TAJAM']);
						}else{
							$BENDA_TAJAM = "";
						}
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_akml (
						ID_ISI,
						TANGGAL,
						RUANGAN,
						AUDITOR,
						PADAT_INFEKSI,
						PADAT_NONINFEKSI,
						CAIR_INFEKSI,
						CAIR_NONINFEKSI,
						BENDA_TAJAM,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$RUANGAN',
						'$AUDITOR',
						'$PADAT_INFEKSI',
						'$PADAT_NONINFEKSI',	
						'$CAIR_INFEKSI',		
						'$CAIR_NONINFEKSI',				
						'$BENDA_TAJAM',	
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_akml'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['PADAT_INFEKSI'])){
							$PADAT_INFEKSI = implode(',',$_POST['PADAT_INFEKSI']);
						}else{
							$PADAT_INFEKSI = "";
						}		
						if(isset($_POST['PADAT_NONINFEKSI'])){
							$PADAT_NONINFEKSI = implode(',',$_POST['PADAT_NONINFEKSI']);
						}else{
							$PADAT_NONINFEKSI = "";
						}	
						if(isset($_POST['CAIR_INFEKSI'])){
							$CAIR_INFEKSI = implode(',',$_POST['CAIR_INFEKSI']);
						}else{
							$CAIR_INFEKSI = "";
						}	
						if(isset($_POST['CAIR_NONINFEKSI'])){
							$CAIR_NONINFEKSI = implode(',',$_POST['CAIR_NONINFEKSI']);
						}else{
							$CAIR_NONINFEKSI = "";
						}	
						if(isset($_POST['BENDA_TAJAM'])){
							$BENDA_TAJAM = implode(',',$_POST['BENDA_TAJAM']);
						}else{
							$BENDA_TAJAM = "";
						}	
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_akml SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						AUDITOR='$AUDITOR',
						PADAT_INFEKSI='$PADAT_INFEKSI',
						PADAT_NONINFEKSI='$PADAT_NONINFEKSI',
						CAIR_INFEKSI='$CAIR_INFEKSI',
						CAIR_NONINFEKSI='$CAIR_NONINFEKSI',
						BENDA_TAJAM='$BENDA_TAJAM',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_akml'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_akml ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_akml'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			