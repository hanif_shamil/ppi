<script type="text/javascript">
function tugas1()
{
var jumlah=0;
var jumlahx=0;
var nilai;
if(document.getElementById("PADAT_INFEKSI").checked)
{
nilai=document.getElementById("PADAT_INFEKSI").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("PADAT_INFEKSITDK").checked)
{
nilai=document.getElementById("PADAT_INFEKSITDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("PADAT_NONINFEKSI").checked)
{
nilai=document.getElementById("PADAT_NONINFEKSI").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("PADAT_NONINFEKSITDK").checked)
{
nilai=document.getElementById("PADAT_NONINFEKSITDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("CAIR_INFEKSI").checked)
{
nilai=document.getElementById("CAIR_INFEKSI").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("CAIR_INFEKSITDK").checked)
{
nilai=document.getElementById("CAIR_INFEKSITDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("CAIR_NONINFEKSI").checked)
{
nilai=document.getElementById("CAIR_NONINFEKSI").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("CAIR_NONINFEKSITDK").checked)
{
nilai=document.getElementById("CAIR_NONINFEKSITDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("BENDA_TAJAM").checked)
{
nilai=document.getElementById("BENDA_TAJAM").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("BENDA_TAJAMTDK").checked)
{
nilai=document.getElementById("BENDA_TAJAMTDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
}
</script>
<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISI) AS last FROM tb_akml WHERE ID_ISI LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Audit Kepatuhan Membuang Limbah</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Audit Kepatuhan Membuang Limbah</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_akml" method="post">
								<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-3">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" autocomplete="off" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-5">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
													where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0 and r.STATUS=1";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data[0]?>"><?=$data[3]?></option>
												<?php
												}
												?>	
											</select>
										</div>	
														
										<div class="col-sm-4">											
											<input type="text" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />												
										</div>
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> INDIKASI</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Membuang limbah padat infeksius ke kantong kuning</td>
														<td align="center"><label>
															<input type="checkbox" name="PADAT_INFEKSI[]" id="PADAT_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
															</label></td>
														<td align="center">
															<label>
															<input name="PADAT_INFEKSI[]" type="checkbox" value="2" id="PADAT_INFEKSITDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Membuang limbah padat non infeksius ke kantong hitam</td>
														<td align="center"><label>
														<input type="checkbox" name="PADAT_NONINFEKSI[]" id="PADAT_NONINFEKSI" value="1" class="ace input-lg" onClick="tugas1()">													
														<span class="lbl"></span>
														</label>
														</td>
														<td align="center"><label>
															<input name="PADAT_NONINFEKSI[]" type="checkbox" value="2" id="PADAT_NONINFEKSITDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Membuang limbah cair infeksius ke saluran infeksius</td>
														<td align="center">
															<label>
															<input type="checkbox" name="CAIR_INFEKSI[]" id="CAIR_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="CAIR_INFEKSI[]" type="checkbox" value="2" id="CAIR_INFEKSITDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Membuang limbah cair non infeksius ke saluran non infeksius</td>
														<td align="center">
															<label>
															<input name="CAIR_NONINFEKSI[]" type="checkbox" value="1" id="CAIR_NONINFEKSI" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="CAIR_NONINFEKSI[]" type="checkbox" value="2" id="CAIR_NONINFEKSITDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Membuang limbah benda tajam ke kontainer tahan air dan tahan tusuk</td>
														<td align="center">
															<label>
															<input name="BENDA_TAJAM[]" type="checkbox" value="1" id="BENDA_TAJAM" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="BENDA_TAJAM[]" type="checkbox" value="2" id="BENDA_TAJAMTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
														<div class="input-group">
															<input name="TOTAL" type="text" id="total" style="width:60px"></br>
															<span class="input-group-addon">
																%
															</span>
														</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													

									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->