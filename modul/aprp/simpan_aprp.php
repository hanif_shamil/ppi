<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						$RUANGAN 		= $_POST['RUANGAN'];

						$SATU_A 		= $_POST['SATU_A'];
						$DUA_A 			= $_POST['DUA_A'];
						$TIGA_A 		= $_POST['TIGA_A'];
						$EMPAT_A 		= $_POST['EMPAT_A'];
						$LIMA_A 		= $_POST['LIMA_A'];
						$ENAM_A 		= $_POST['ENAM_A'];
						$TUJUH_A 		= $_POST['TUJUH_A'];
						$DELAPAN_A 		= $_POST['DELAPAN_A'];
					
						$SATU_B 		= $_POST['SATU_B'];
						$DUA_B 			= $_POST['DUA_B'];
						$TIGA_B 		= $_POST['TIGA_B'];
						$EMPAT_B 		= $_POST['EMPAT_B'];
						$LIMA_B 		= $_POST['LIMA_B'];
						$ENAM_B 		= $_POST['ENAM_B'];
						$TUJUH_B 		= $_POST['TUJUH_B'];
						$DELAPAN_B 		= $_POST['DELAPAN_B'];
						$SEMBILAN_B 	= $_POST['SEMBILAN_B'];
						$SEPULUH_B 		= $_POST['SEPULUH_B'];

						$SATU_C 		= $_POST['SATU_C'];
						$DUA_C 			= $_POST['DUA_C'];
						$TIGA_C 		= $_POST['TIGA_C'];
						$EMPAT_C 		= $_POST['EMPAT_C'];
						$LIMA_C 		= $_POST['LIMA_C'];
						$ENAM_C 		= $_POST['ENAM_C'];

						$SATU_D 		= $_POST['SATU_D'];
						$DUA_D 			= $_POST['DUA_D'];
						$TIGA_D 		= $_POST['TIGA_D'];

						$SATU_E 		= $_POST['SATU_E'];
						$DUA_E 			= $_POST['DUA_E'];
						$TIGA_E 		= $_POST['TIGA_E'];
						$EMPAT_E 		= $_POST['EMPAT_E'];

						$SATU_F 		= $_POST['SATU_F'];
						$DUA_F 			= $_POST['DUA_F'];
						$TIGA_F 		= $_POST['TIGA_F'];
						$EMPAT_F 		= $_POST['EMPAT_F'];
						$LIMA_F 		= $_POST['LIMA_F'];
						$ENAM_F 		= $_POST['ENAM_F'];
						$TUJUH_F 		= $_POST['TUJUH_F'];

						$SATU_G 		= $_POST['SATU_G'];
						$DUA_G 			= $_POST['DUA_G'];
						$TIGA_G 		= $_POST['TIGA_G'];
						$EMPAT_G 		= $_POST['EMPAT_G'];
						$LIMA_G 		= $_POST['LIMA_G'];
						$ENAM_G 		= $_POST['ENAM_G'];

						$SATU_H 		= $_POST['SATU_H'];
						$DUA_H 			= $_POST['DUA_H'];
						$TIGA_H 		= $_POST['TIGA_H'];
						$EMPAT_H 		= $_POST['EMPAT_H'];
						$LIMA_H 		= $_POST['LIMA_H'];
						$ENAM_H 		= $_POST['ENAM_H'];
						$TUJUH_H 		= $_POST['TUJUH_H'];
						$DELAPAN_H 		= $_POST['DELAPAN_H'];	

						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_aprp (
						ID_ISI,
						RUANGAN,
						TANGGAL,
						AUDITOR,	
						SATU_A,
						DUA_A,
						TIGA_A,
						EMPAT_A,
						LIMA_A,
						ENAM_A,
						TUJUH_A,
						DELAPAN_A,
						SATU_B,
						DUA_B,
						TIGA_B,
						EMPAT_B,
						LIMA_B,
						ENAM_B,
						TUJUH_B,
						DELAPAN_B,
						SEMBILAN_B,
						SEPULUH_B,
						SATU_C,
						DUA_C,
						TIGA_C,
						EMPAT_C,
						LIMA_C,
						ENAM_C,
						SATU_D,
						DUA_D,
						TIGA_D,
						SATU_E,
						DUA_E,
						TIGA_E,
						EMPAT_E,
						SATU_F,
						DUA_F,
						TIGA_F,
						EMPAT_F,
						LIMA_F,
						ENAM_F,
						TUJUH_F,
						SATU_G,
						DUA_G,
						TIGA_G,
						EMPAT_G,
						LIMA_G,
						ENAM_G,
						SATU_H,
						DUA_H,
						TIGA_H,
						EMPAT_H,
						LIMA_H,
						ENAM_H,
						DELAPAN_H,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$RUANGAN',
						'$AUDITOR',					
						'$SATU_A',
						'$DUA_A',
						'$TIGA_A',
						'$EMPAT_A',
						'$LIMA_A',
						'$ENAM_A',
						'$TUJUH_A',
						'$DELAPAN_A',
						'$SATU_B',
						'$DUA_B',
						'$TIGA_B',
						'$EMPAT_B',
						'$LIMA_B',
						'$ENAM_B',
						'$TUJUH_B',
						'$DELAPAN_B',
						'$SEMBILAN_B',
						'$SEPULUH_B',
						'$SATU_C',
						'$DUA_C',
						'$TIGA_C',
						'$EMPAT_C',
						'$LIMA_C',
						'$ENAM_C',
						'$SATU_D',
						'$DUA_D',
						'$TIGA_D',
						'$SATU_E',
						'$DUA_E',
						'$TIGA_E',
						'$EMPAT_E',
						'$SATU_F',
						'$DUA_F',
						'$TIGA_F',
						'$EMPAT_F',
						'$LIMA_F',
						'$ENAM_F',
						'$TUJUH_F',
						'$SATU_G',
						'$DUA_G',
						'$TIGA_G',
						'$EMPAT_G',
						'$LIMA_G',
						'$ENAM_G',
						'$SATU_H',
						'$DUA_H',
						'$TIGA_H',
						'$EMPAT_H',
						'$LIMA_H',
						'$ENAM_H',
						'$DELAPAN_H',
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_aprp'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						$RUANGAN 		= $_POST['RUANGAN'];

						$SATU_A 		= $_POST['SATU_A'];
						$DUA_A 			= $_POST['DUA_A'];
						$TIGA_A 		= $_POST['TIGA_A'];
						$EMPAT_A 		= $_POST['EMPAT_A'];
						$LIMA_A 		= $_POST['LIMA_A'];
						$ENAM_A 		= $_POST['ENAM_A'];
						$TUJUH_A 		= $_POST['TUJUH_A'];
						$DELAPAN_A 		= $_POST['DELAPAN_A'];
					
						$SATU_B 		= $_POST['SATU_B'];
						$DUA_B 			= $_POST['DUA_B'];
						$TIGA_B 		= $_POST['TIGA_B'];
						$EMPAT_B 		= $_POST['EMPAT_B'];
						$LIMA_B 		= $_POST['LIMA_B'];
						$ENAM_B 		= $_POST['ENAM_B'];
						$TUJUH_B 		= $_POST['TUJUH_B'];
						$DELAPAN_B 		= $_POST['DELAPAN_B'];
						$SEMBILAN_B 	= $_POST['SEMBILAN_B'];
						$SEPULUH_B 		= $_POST['SEPULUH_B'];

						$SATU_C 		= $_POST['SATU_C'];
						$DUA_C 			= $_POST['DUA_C'];
						$TIGA_C 		= $_POST['TIGA_C'];
						$EMPAT_C 		= $_POST['EMPAT_C'];
						$LIMA_C 		= $_POST['LIMA_C'];
						$ENAM_C 		= $_POST['ENAM_C'];

						$SATU_D 		= $_POST['SATU_D'];
						$DUA_D 			= $_POST['DUA_D'];
						$TIGA_D 		= $_POST['TIGA_D'];

						$SATU_E 		= $_POST['SATU_E'];
						$DUA_E 			= $_POST['DUA_E'];
						$TIGA_E 		= $_POST['TIGA_E'];
						$EMPAT_E 		= $_POST['EMPAT_E'];

						$SATU_F 		= $_POST['SATU_F'];
						$DUA_F 			= $_POST['DUA_F'];
						$TIGA_F 		= $_POST['TIGA_F'];
						$EMPAT_F 		= $_POST['EMPAT_F'];
						$LIMA_F 		= $_POST['LIMA_F'];
						$ENAM_F 		= $_POST['ENAM_F'];
						$TUJUH_F 		= $_POST['TUJUH_F'];

						$SATU_G 		= $_POST['SATU_G'];
						$DUA_G 			= $_POST['DUA_G'];
						$TIGA_G 		= $_POST['TIGA_G'];
						$EMPAT_G 		= $_POST['EMPAT_G'];
						$LIMA_G 		= $_POST['LIMA_G'];
						$ENAM_G 		= $_POST['ENAM_G'];

						$SATU_H 		= $_POST['SATU_H'];
						$DUA_H 			= $_POST['DUA_H'];
						$TIGA_H 		= $_POST['TIGA_H'];
						$EMPAT_H 		= $_POST['EMPAT_H'];
						$LIMA_H 		= $_POST['LIMA_H'];
						$ENAM_H 		= $_POST['ENAM_H'];
						$TUJUH_H 		= $_POST['TUJUH_H'];
						$DELAPAN_H 		= $_POST['DELAPAN_H'];	

						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_aprp SET												
						TANGGAL		= '$TANGGAL',
						TGL_UBAH	= '$waktu',
						AUDITOR		= '$AUDITOR',	
						AUDITOR 	= '$AUDITOR',
						RUANGAN 	='$RUANGAN',

						SATU_A 		= '$SATU_A',
						DUA_A 		= '$DUA_A',
						TIGA_A 		= '$TIGA_A',
						EMPAT_A 	= '$EMPAT_A',
						LIMA_A 		= '$LIMA_A',
						ENAM_A 		= '$ENAM_A',
						TUJUH_A 	= '$TUJUH_A',
						DELAPAN_A 	= '$DELAPAN_A',
					
						SATU_B 		= '$SATU_B',
						DUA_B 		= '$DUA_B',
						TIGA_B 		= '$TIGA_B',
						EMPAT_B 	= '$EMPAT_B',
						LIMA_B 		= '$LIMA_B',
						ENAM_B 		= '$ENAM_B',
						TUJUH_B 	= '$TUJUH_B',
						DELAPAN_B 	= '$DELAPAN_B',
						SEMBILAN_B 	= '$SEMBILAN_B',
						SEPULUH_B 	= '$SEPULUH_B',

						SATU_C 		= '$SATU_C',
						DUA_C 		= '$DUA_C',
						TIGA_C 		= '$TIGA_C',
						EMPAT_C 	= '$EMPAT_C',
						LIMA_C 		= '$LIMA_C',
						ENAM_C 		= '$ENAM_C',

						SATU_D 		= '$SATU_D',
						DUA_D 		= '$DUA_D',
						TIGA_D 		= '$TIGA_D',

						SATU_E 		= '$SATU_E',
						DUA_E 		= '$DUA_E',
						TIGA_E 		= '$TIGA_E',
						EMPAT_E 	= '$EMPAT_E',

						SATU_F 		= '$SATU_F',
						DUA_F 		= '$DUA_F',
						TIGA_F 		= '$TIGA_F',
						EMPAT_F 	= '$EMPAT_F',
						LIMA_F 		= '$LIMA_F',
						ENAM_F 		= '$ENAM_F',
						TUJUH_F 	= '$TUJUH_F',

						SATU_G 		= '$SATU_G',
						DUA_G 		= '$DUA_G',
						TIGA_G 		= '$TIGA_G',
						EMPAT_G 	= '$EMPAT_G',
						LIMA_G 		= '$LIMA_G',
						ENAM_G 		= '$ENAM_G',

						SATU_H 		= '$SATU_H',
						DUA_H 		= '$DUA_H',
						TIGA_H 		= '$TIGA_H',
						EMPAT_H 	= '$EMPAT_H',
						LIMA_H 		= '$LIMA_H',
						ENAM_H 		= '$ENAM_H',
						TUJUH_H 	= '$TUJUH_H',
						DELAPAN_H 	= '$DELAPAN_H',	

						TOTAL	 	= '$TOTAL',			
						DIUBAH_OLEH	= '$USER'
						where ID_ISI= '$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_aprp'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_aprp ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_aprp'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			