<script type="text/javascript">

	function tugas1()
	{	
		var ya = $('#aa:checked').length;
		var tidak = $('#bb:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISI) AS last FROM tb_aprp WHERE ID_ISI LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Monitoring/Audit PPI di Ruang Perawatan</li>
			</ul><!-- /.breadcrumb -->


		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Monitoring/Audit PPI di Ruang Perawatan</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_aprp" method="post">
									<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-4">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" autocomplete="off" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>										
										<div class="col-sm-4">											
											<input type="text" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />												
										</div>
										<div class="col-sm-4">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
													?>
													<option value="<?=$data[0]?>"><?=$data[3]?></option>
													<?php
												}
												?>	
											</select>
										</div>	
									</div>									
									<hr />

									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> ELEMEN PENILAIAN</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td></td>
														<td colspan="3" style="font-weight: bold;">A. Personal</td>
													</tr>
													<tr>
														<td>1</td>
														<td>Personal hygiene baik</td>
														<td align="center"><label>
															<input type="checkbox" name="SATU_A" id="aa" value="1" class="ace input-lg" onClick="tugas1()">																	
															<span class="lbl"></span>
														</label></td>
														
														<td align="center">
															<label>
																<input name="SATU_A" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Pakaian rapih</td>
														<td align="center"><label>
															<input type="checkbox" name="DUA_A" id="aa" value="1" class="ace input-lg" onClick="tugas1()">													
															<span class="lbl"></span>
														</label></td>
														
														<td align="center"><label>
															<input type="checkbox" name="DUA_A" id="bb" value="2" class="ace input-lg" onClick="tugas1()">													
															<span class="lbl"></span>
														</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Rambut bersih dan rapih</td>
														<td align="center">
															<label>
																<input type="checkbox" name="TIGA_A" id="aa" value="1" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input type="checkbox" name="TIGA_A" id="bb" value="2" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Tidak menggunakan perhiasan tangan</td>
														<td align="center">
															<label>
																<input name="EMPAT_A" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="EMPAT_A" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Kuku pendek dan bersih</td>
														<td align="center">
															<label>
																<input name="LIMA_A" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="LIMA_A" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Penggunaan APD sesuai prosedur</td>
														<td align="center">
															<label>
																<input name="ENAM_A" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="ENAM_A" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>7</td>
														<td>Melakukan kebersihan tangan sesuai five moments</td>
														<td align="center">
															<label>
																<input name="TUJUH_A" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TUJUH_A" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>8</td>
														<td>Melapor kepada atasan jika diduga mengalami penyakit infeksi</td>
														<td align="center">
															<label>
																<input name="DELAPAN_A" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DELAPAN_A" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td></td>
														<td style="font-weight: bold;" colspan="3">B. Bedside Stand </td>
													</tr>
													<tr>
														<td>1</td>
														<td>Hand rub tersedia</td>
														<td align="center">
															<label>
																<input name="SATU_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="SATU_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Sarana cuci tangan lengkap</td>
														<td align="center">
															<label>
																<input name="DUA_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DUA_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Wastafel dalam keadaan bersih</td>
														<td align="center">
															<label>
																<input name="TIGA_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TIGA_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Meja pasien bersih</td>
														<td align="center">
															<label>
																<input name="EMPAT_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="EMPAT_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Tidak ada sisa makanan di sekitar pasien</td>
														<td align="center">
															<label>
																<input name="LIMA_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="LIMA_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Pispot atau urinal tertutup</td>
														<td align="center">
															<label>
																<input name="ENAM_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="ENAM_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>7</td>
														<td>Alat-alat yang ada di sekitar pasien bersih</td>
														<td align="center">
															<label>
																<input name="TUJUH_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TUJUH_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>8</td>
														<td>Tiang infuse bersih</td>
														<td align="center">
															<label>
																<input name="DELAPAN_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DELAPAN_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>9</td>
														<td>Alat suction, selang feeding, dan oksigen bersih</td>
														<td align="center">
															<label>
																<input name="SEMBILAN_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="SEMBILAN_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>10</td>
														<td>Commode bersih</td>
														<td align="center">
															<label>
																<input name="SEPULUH_B" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="SEPULUH_B" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td></td>
														<td style="font-weight: bold;" colspan="3">C. Bed</td>
													</tr>
													<tr>
														<td>1</td>
														<td>Railing bersih</td>
														<td align="center">
															<label>
																<input name="SATU_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="SATU_C" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Kasur menggunakan sarung kedap air</td>
														<td align="center">
															<label>
																<input name="DUA_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DUA_C" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Tempat tidur bersih tidak berdebu</td>
														<td align="center">
															<label>
																<input name="TIGA_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TIGA_C" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Tidak ada remah makanan</td>
														<td align="center">
															<label>
																<input name="EMPAT_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="EMPAT_C" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Linen bersih, tidak sobek, dan tidak ada noda</td>
														<td align="center">
															<label>
																<input name="LIMA_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="LIMA_C" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Alat lain di tempat tidur bersih e.g restraint, bantal</td>
														<td align="center">
															<label>
																<input name="ENAM_C" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="ENAM_C" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td></td>
														<td style="font-weight: bold;" colspan="3">D. Lemari Pasien</td>
													</tr>
													<tr>
														<td>1</td>
														<td>Lemari bersih dan tertutup</td>
														<td align="center">
															<label>
																<input name="SATU_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="SATU_D" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Pakaian diberi label</td>
														<td align="center">
															<label>
																<input name="DUA_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DUA_D" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Pakaian bersih tidak berbau</td>
														<td align="center">
															<label>
																<input name="TIGA_D" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TIGA_D" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>

													<tr>
														<td></td>
														<td style="font-weight: bold;" colspan="3">E. Kamar Mandi </td>
													</tr>
													<tr>
														<td>1</td>
														<td>Shower bersih dan berfungsi dengan baik</td>
														<td align="center">
															<label>
																<input name="SATU_E" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="SATU_E" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Tempat duduk toilet bersih dan tidak ada kerusakan</td>
														<td align="center">
															<label>
																<input name="DUA_E" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DUA_E" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Paper towel / tissue toilet tersedia</td>
														<td align="center">
															<label>
																<input name="TIGA_E" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TIGA_E" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Alat-alat pribadi pasien bersih</td>
														<td align="center">
															<label>
																<input name="EMPAT_E" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="EMPAT_E" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>
														</td>

														<td style="font-weight: bold;" colspan="3">F. Ruang Bersih</td>	

													</tr>
													<tr>
														<td>1</td>
														<td>Terdapat sarana cuci tangan yang bersih dan berfungsi dengan baik</td>
														<td align="center">
															<label>
																<input name="SATU_F" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="SATU_F" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Peralatan bersih, kering, dan disimpan dengan rapih</td>
														<td align="center">
															<label>
																<input name="DUA_F" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DUA_F" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Alat-alat steril tidak kadaluarsa</td>
														<td align="center">
															<label>
																<input name="TIGA_F" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TIGA_F" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Lemari linen bersih dan tertutup</td>
														<td align="center">
															<label>
																<input name="EMPAT_F" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="EMPAT_F" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Linen bersih tersusun rapih</td>
														<td align="center">
															<label>
																<input name="LIMA_F" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="LIMA_F" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Penyimpanan alat-alat yang steril terpisah dari alat-alat yang bersih</td>
														<td align="center">
															<label>
																<input name="ENAM_F" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="ENAM_F" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>7</td>
														<td>Area bersih terpisah dari area kotor.</td>
														<td align="center">
															<label>
																<input name="TUJUH_F" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TUJUH_F" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>
														</td>

														<td style="font-weight: bold;" colspan="3">G. Ruang Kotor</td>	

													</tr>
													<tr>
														<td>1</td>
														<td>Terdapat sarana cuci tangan yang bersih dan berfungsi dengan baik</td>
														<td align="center">
															<label>
																<input name="SATU_G" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="SATU_G" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Pump flusher berfungsi dengan baik</td>
														<td align="center">
															<label>
																<input name="DUA_G" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DUA_G" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Instrumen kotor ditempatkan dalam container tertutup</td>
														<td align="center">
															<label>
																<input name="TIGA_G" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TIGA_G" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Linen kotor ditempatkan dalam trolley linen kotor tertutup</td>
														<td align="center">
															<label>
																<input name="EMPAT_G" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="EMPAT_G" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Linen kotor infekius ditempatkan dalam trolley linen kotor infeksius atau dalam kantong plastik kuning</td>
														<td align="center">
															<label>
																<input name="LIMA_G" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="LIMA_G" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Area kotor terpisah dari area bersih</td>
														<td align="center">
															<label>
																<input name="ENAM_G" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														
														<td align="center">
															<label>
																<input name="ENAM_G" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>


													<tr>
														<td>
														</td>

														<td style="font-weight: bold;" colspan="3">H.Pembuangan Limbah</td>	

													</tr>
													<tr>
														<td>1</td>
														<td>Tersedia wadah limbah infeksius, non infeksius, dan limbah benda tajam</td>
														<td align="center">
															<label>
																<input name="SATU_H" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="SATU_H" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Ada label di setiap tempat sampah</td>
														<td align="center">
															<label>
																<input name="DUA_H" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DUA_H" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>3</td>
														<td>Tempat sampah infeksius menggunakan kantong kuning</td>
														<td align="center">
															<label>
																<input name="TIGA_H" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TIGA_H" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Tempat sampah non infeksius menggunakan kantong hitam</td>
														<td align="center">
															<label>
																<input name="EMPAT_H" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="EMPAT_H" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Tempat limbah benda tajam menggunakan container yang tahan air dan tahan benda tajam</td>
														<td align="center">
															<label>
																<input name="LIMA_H" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="LIMA_H" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Limbah dibuang setelah ¾ atau 2/3 penuh</td>
														<td align="center">
															<label>
																<input name="ENAM_H" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="ENAM_H" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>

													<tr>
														<td>7</td>
														<td>Tempat limbah dalam keadaan bersih dan tertutup</td>
														<td align="center">
															<label>
																<input name="TUJUH_H" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="TUJUH_H" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>8</td>
														<td>Pedal tempat sampah berfungsi baik</td>
														<td align="center">
															<label>
																<input name="DELAPAN_H" type="checkbox" value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														
														<td align="center">
															<label>
																<input name="DELAPAN_H" type="checkbox" value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
														
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
															<div class="input-group">
																<input name="TOTAL" type="text" id="total" style="width:100px"></br>
																<span class="input-group-addon">
																	%
																</span>
															</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													

									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->