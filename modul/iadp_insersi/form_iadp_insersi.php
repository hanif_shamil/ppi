<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Audit Bundles IADP : INSERSI</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Audit Bundles IADP : INSERSI</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_iadp_insersi" method="post">
									<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
													?>
													<option value="<?=$data[0]?>"><?=$data[3]?></option>
													<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-2">
											<input type="text" id="nomr" name="NOMR" placeholder="[ MR ]" class="form-control" onkeyup="autofill()" />												
										</div>	
										<div class="col-sm-5">									
											<input type="text" id="nama" name="NAMA" placeholder="[ NAMA ]" class="form-control" />												
										</div>																			
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> BUNDLES IADP : INSERSI</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Melakukan kebersihan tangan</td>
														<td align="center"><label>
															<input type="radio" name="SATU[]" id="AA" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="SATU[]" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>2</td>
													<td>Menggunakan APD maksimal (topi, masker, gaun/apron, sarung tangan steril)</td>
													<td align="center">
														<label>
															<input type="radio" name="DUA[]" id="AA" value="1" class="ace input-lg" onClick="tugas1()">													
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center"><label>
														<input name="DUA[]" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>3</td>
												<td>Pemilihan vena untuk insersi  yang tepat. Direkomendasikan vena subclavia atau jugularis, kecuali ada kontraindikasi. Hindari pemasangan di vena femoralis.</td>
												<td align="center">
													<label>
														<input type="radio" name="TIGA[]" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="TIGA[]" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()"/>
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>4</td>
												<td>Melakukan preparasi kulit dengan chlorhexidine 2% mix alkohol 70% </td>
												<td align="center">
													<label>
														<input name="EMPAT[]" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="EMPAT[]" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>5</td>
												<td>Menunggu cairan  chlorhexidine 2% mix alkohol 70% selama 20- 30 detik (tunggu sampai kering)</td>
												<td align="center">
													<label>
														<input name="LIMA[]" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="LIMA[]" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()"/>
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>6</td>
												<td>Memasang CVP harus dilakukan oleh staf ahli yang kompeten (dokter spesialis anestesi,dokter spesialis bedah vaskuler )</td>
												<td align="center">
													<label>
														<input name="ENAM[]" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="ENAM[]" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()"/>
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>7</td>
												<td>Menggunakan pack set steril untuk pemasangan CVP</td>
												<td align="center">
													<label>
														<input name="TUJUH[]" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="TUJUH[]" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()"/>
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>8</td>
												<td>Menutup lokasi pemasangan CVP dengan transparent dressing</td>
												<td align="center">
													<label>
														<input name="DELAPAN[]" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="DELAPAN[]" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()"/>
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>9</td>
												<td>Menulis tanggal pemasangan CVP di balutan/dressing</td>
												<td align="center">
													<label>
														<input name="SEMBILAN[]" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="SEMBILAN[]" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()"/>
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td style="text-align:center" colspan="2">Total</td>
												<td style="text-align:center"></td>
												<td style="text-align:center"></td>
											</tr>
											<tr>
												<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
												<td colspan="2" style="text-align:center">
													<div class="input-group">
														<input name="TOTAL" type="text" id="total" style="width:60px"></br>
														<span class="input-group-addon">
															%
														</span>
													</div>														
												</td>														
											</tr>														
										</tbody>
									</table>											
								</div>
							</div>													
							
							<div class="form-group">										
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
									<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
								</div>				
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
									<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
								</div>
							</div>
							<div class="form-group">			
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
									<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
								</div>	
								<div class="col-md-6 col-sm-12">	
									<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
									<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
								</div>								
							</div>	
							<hr />
							<div class="form-group">							
								<div class="col-md-offset-3 col-md-9">
									<button class="btn btn-info" name="btnsimpan" type="submit">
										<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
										%
										Reset
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><!-- /.span -->
	</div>
</div><!-- /.page-content -->	
</div> <!-- container -->
</div><!-- /.main-content -->