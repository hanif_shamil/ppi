<script type="text/javascript">
	function tugas1()
	{
		var jumlah=0;
		var jumlahx=0;
		var nilai;
		if(document.getElementById("SKREENING").checked)
		{
			nilai=document.getElementById("SKREENING").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("SKREENINGTDK").checked)
		{
			nilai=document.getElementById("SKREENINGTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("INSERSI").checked)
		{
			nilai=document.getElementById("INSERSI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("INSERSITDK").checked)
		{
			nilai=document.getElementById("INSERSITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("BERSIH_TNGN").checked)
		{
			nilai=document.getElementById("BERSIH_TNGN").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("BERSIH_TNGNTDK").checked)
		{
			nilai=document.getElementById("BERSIH_TNGNTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("APD_MAXIMAL").checked)
		{
			nilai=document.getElementById("APD_MAXIMAL").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("APD_MAXIMALTDK").checked)
		{
			nilai=document.getElementById("APD_MAXIMALTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("PREPARATION").checked)
		{
			nilai=document.getElementById("PREPARATION").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("PREPARATIONTDK").checked)
		{
			nilai=document.getElementById("PREPARATIONTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("DRESSING").checked)
		{
			nilai=document.getElementById("DRESSING").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("DRESSINGTDK").checked)
		{
			nilai=document.getElementById("DRESSINGTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
	}
</script>

<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Audit Bundles BMP/Intratekhal</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Audit Bundles BMP/Intratekhal</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_bmp" method="post">					
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0 and r.STATUS=1";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
													?>
													<option value="<?=$data[0]?>"><?=$data[3]?></option>
													<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-2">
											<input type="text" id="nomr" name="NOMR" placeholder="[ MR ]" autocomplete="off" class="form-control" onkeyup="autofill()" />												
										</div>	
										<div class="col-sm-5">									
											<input type="text" id="nama" name="NAMA" placeholder="[ NAMA ]" autocomplete="off" class="form-control" />												
										</div>																			

									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> </th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Skreening laboratorium haematologi rutin 1 (Hb, Leukosit, Trombosit)</td>
														<td align="center"><label>
															<input type="checkbox" name="SKREENING[]" id="SKREENING" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
														</label></td>
														<td align="center">
															<label>
																<input name="SKREENING[]" type="checkbox" value="2" id="SKREENINGTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Tehnik insersi</td>
														<td align="center"><label>
															<input type="checkbox" name="INSERSI[]" id="INSERSI" value="1" class="ace input-lg" onClick="tugas1()">													
															<span class="lbl"></span>
														</label></td>
														<td align="center"><label>
															<input name="INSERSI[]" type="checkbox" value="2" id="INSERSITDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Melakukan kebersihan tangan</td>
														<td align="center">
															<label>
																<input type="checkbox" name="BERSIH_TNGN[]" id="BERSIH_TNGN" value="1" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="BERSIH_TNGN[]" type="checkbox" value="2" id="BERSIH_TNGNTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Menggunakan APD maksimal (Apron, topi, masker, sarungan tangan steril)</td>
														<td align="center">
															<label>
																<input name="APD_MAXIMAL[]" type="checkbox" value="1" id="APD_MAXIMAL" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="APD_MAXIMAL[]" type="checkbox" value="2" id="APD_MAXIMALTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Skin preparation dengan chlorhexidine 2% dan alcohol 70%, tunggu sampai 20 detik sebelum inserasi</td>
														<td align="center">
															<label>
																<input name="PREPARATION[]" type="checkbox" value="1" id="PREPARATION" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="PREPARATION[]" type="checkbox" value="2" id="PREPARATIONTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Menutup area penusukan dengan menggunakan kassa steril satu lembar dan transparan dressing</td>
														<td align="center">
															<label>
																<input name="DRESSING[]" type="checkbox" value="1" id="DRESSING" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DRESSING[]" type="checkbox" value="2" id="DRESSINGTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
															<div class="input-group">
																<input name="TOTAL" type="text" id="total" style="width:60px"></br>
																<span class="input-group-addon">
																	%
																</span>
															</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													
									<div class="form-group">										
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
											<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
										</div>				
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
											<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
										</div>
									</div>
									<div class="form-group">			
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
											<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
										</div>	
										<div class="col-md-6 col-sm-12">	
											<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
											<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
										</div>								
									</div>
									<hr>
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->