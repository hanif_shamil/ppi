<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){

						$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
						$query = "SELECT max(ID_ISI) AS last FROM tb_bmp WHERE ID_ISI LIKE '$today%'";
						$hasil = mysqli_query($conn1,$query);
						$data  = mysqli_fetch_assoc($hasil);
						$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
						$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
						$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
						$nextID = $today.sprintf('%04s', $nextNoUrut);

						// $ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$SURVEYOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SKREENING'])){
							$SKREENING = implode(',',$_POST['SKREENING']);
						}else{
							$SKREENING = "";
						}		
						if(isset($_POST['INSERSI'])){
							$INSERSI = implode(',',$_POST['INSERSI']);
						}else{
							$INSERSI = "";
						}	
						if(isset($_POST['BERSIH_TNGN'])){
							$BERSIH_TNGN = implode(',',$_POST['BERSIH_TNGN']);
						}else{
							$BERSIH_TNGN = "";
						}	
						if(isset($_POST['APD_MAXIMAL'])){
							$APD_MAXIMAL = implode(',',$_POST['APD_MAXIMAL']);
						}else{
							$APD_MAXIMAL = "";
						}	
						if(isset($_POST['PREPARATION'])){
							$PREPARATION = implode(',',$_POST['PREPARATION']);
						}else{
							$PREPARATION = "";
						}
						if(isset($_POST['DRESSING'])){
							$DRESSING = implode(',',$_POST['DRESSING']);
						}else{
							$DRESSING = "";
						}						
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_bmp (
							ID_ISI,
							TANGGAL,
							RUANGAN,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							SURVEYOR,
							NOMR,
							NAMA,
							SKREENING,
							INSERSI,
							BERSIH_TNGN,
							APD_MAXIMAL,
							PREPARATION,
							DRESSING,
							TOTAL,
							USER,
							STATUS) 
						VALUES 
						('$nextID',
							'$TANGGAL',
							'$RUANGAN',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$SURVEYOR',
							'$NOMR',
							'$NAMA',
							'$SKREENING',
							'$INSERSI',	
							'$BERSIH_TNGN',		
							'$APD_MAXIMAL',				
							'$PREPARATION',	
							'$DRESSING',	
							'$TOTAL',
							'$USER',												
							'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_bmp'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$SURVEYOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SKREENING'])){
							$SKREENING = implode(',',$_POST['SKREENING']);
						}else{
							$SKREENING = "";
						}		
						if(isset($_POST['INSERSI'])){
							$INSERSI = implode(',',$_POST['INSERSI']);
						}else{
							$INSERSI = "";
						}	
						if(isset($_POST['BERSIH_TNGN'])){
							$BERSIH_TNGN = implode(',',$_POST['BERSIH_TNGN']);
						}else{
							$BERSIH_TNGN = "";
						}	
						if(isset($_POST['APD_MAXIMAL'])){
							$APD_MAXIMAL = implode(',',$_POST['APD_MAXIMAL']);
						}else{
							$APD_MAXIMAL = "";
						}	
						if(isset($_POST['PREPARATION'])){
							$PREPARATION = implode(',',$_POST['PREPARATION']);
						}else{
							$PREPARATION = "";
						}
						if(isset($_POST['DRESSING'])){
							$DRESSING = implode(',',$_POST['DRESSING']);
						}else{
							$DRESSING = "";
						}						
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_bmp SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						ANALISA = '$ANALISA', 
						TINDAKLANJUT = '$TINDAKLANJUT', 
						KEPALA = '$KEPALA',
						SURVEYOR='$SURVEYOR',
						NOMR='$NOMR',
						NAMA='$NAMA',
						SKREENING='$SKREENING',
						INSERSI='$INSERSI',
						BERSIH_TNGN='$BERSIH_TNGN',
						APD_MAXIMAL='$APD_MAXIMAL',
						PREPARATION='$PREPARATION',
						DRESSING='$DRESSING',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_bmp'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_bmp ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_bmp'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			