<script type="text/javascript">
	function tugas1()
	{
		var jumlah=0;
		var jumlahx=0;
		var nilai;
		if(document.getElementById("SKREENING").checked)
		{
			nilai=document.getElementById("SKREENING").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("SKREENINGTDK").checked)
		{
			nilai=document.getElementById("SKREENINGTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("INSERSI").checked)
		{
			nilai=document.getElementById("INSERSI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("INSERSITDK").checked)
		{
			nilai=document.getElementById("INSERSITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("BERSIH_TNGN").checked)
		{
			nilai=document.getElementById("BERSIH_TNGN").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("BERSIH_TNGNTDK").checked)
		{
			nilai=document.getElementById("BERSIH_TNGNTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("APD_MAXIMAL").checked)
		{
			nilai=document.getElementById("APD_MAXIMAL").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("APD_MAXIMALTDK").checked)
		{
			nilai=document.getElementById("APD_MAXIMALTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("PREPARATION").checked)
		{
			nilai=document.getElementById("PREPARATION").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("PREPARATIONTDK").checked)
		{
			nilai=document.getElementById("PREPARATIONTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("DRESSING").checked)
		{
			nilai=document.getElementById("DRESSING").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("DRESSINGTDK").checked)
		{
			nilai=document.getElementById("DRESSINGTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
	}
</script>
<?php
$id=($_GET['id']);
$query="select tbmp.ID_ISI, tbmp.TGL_INPUT, tbmp.TANGGAL, tbmp.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, tbmp.NOMR, tbmp.NAMA
, tbmp.SURVEYOR, tbmp.SKREENING, tbmp.INSERSI, tbmp.BERSIH_TNGN, tbmp.APD_MAXIMAL, tbmp.PREPARATION, tbmp.DRESSING, tbmp.TOTAL, tbmp.`STATUS`, tbmp.ANALISA, tbmp.TINDAKLANJUT, tbmp.KEPALA
from tb_bmp tbmp
left join ruangan ru ON ru.ID=tbmp.RUANGAN and ru.JENIS=5					
where tbmp.ID_ISI='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$SKREENING = explode(",", $dt['SKREENING']);$INSERSI = explode(",", $dt['INSERSI']);$BERSIH_TNGN = explode(",", $dt['BERSIH_TNGN']);
$APD_MAXIMAL = explode(",", $dt['APD_MAXIMAL']);$PREPARATION = explode(",", $dt['PREPARATION']);$DRESSING = explode(",", $dt['DRESSING']);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">Edit Forms</a>
					</li>
					<li class="active">Formulir Audit Bundles BMP/Intratekhal</li>
				</ul><!-- /.breadcrumb -->

				
			</div>

			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Formulir Audit Bundles BMP/Intratekhal</h4>

								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>

									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_bmp" method="post">
										<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $dt['ID_ISI']; ?>" readonly>										
										<div class="form-group">										
											<div class="col-sm-2">
												<div class="input-group">
													<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TANGGAL']; ?>" name="TANGGAL" type="text" />
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-3">										
												<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" value="<?php echo $dt['RUANGAN']; ?>" data-placeholder="[ RUANGAN ]">
													<option value=""></option>
													<?php
													$sql = "select * from db_ppi.ruangan r
													where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
													$rs = mysqli_query($conn1,$sql);
													while ($data = mysqli_fetch_array($rs)) {
														?>
														<option <?php if($dt['ID_RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
														<?php
													}
													?>	
												</select>
											</div>	
											<div class="col-sm-1">
												<input type="text" id="nomr" name="NOMR" value="<?php echo $dt['NOMR']; ?>" placeholder="[ MR ]" class="form-control" onkeyup="autofill()" />												
											</div>	
											<div class="col-sm-5">									
												<input type="text" id="nama" name="NAMA" value="<?php echo $dt['NAMA']; ?>" placeholder="[ NAMA ]" class="form-control" />												
											</div>																			
										</div>									
										<hr />
										
										<div class="form-group">
											<div class="col-sm-12">											
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="text-align:center">NO</th>
															<th style="text-align:center"> BUNDLES IDAP</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>Skreening laboratorium haematologi rutin 1 (Hb, Leukosit, Trombosit)</td>
															<td align="center"><label>
																<input type="checkbox" name="SKREENING[]" <?php if(in_array("1", $SKREENING)){ echo " checked=\"checked\""; } ?> id="SKREENING" value="1" class="ace input-lg" onClick="tugas1()">																																													
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="SKREENING[]" type="checkbox" <?php if(in_array("2", $SKREENING)){ echo " checked=\"checked\""; } ?> value="2" id="SKREENINGTDK" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Tehnik insersi</td>
															<td align="center"><label>
																<input type="checkbox" name="INSERSI[]" <?php if(in_array("1", $INSERSI)){ echo " checked=\"checked\""; } ?> id="INSERSI" value="1" class="ace input-lg" onClick="tugas1()">													
																<span class="lbl"></span>
															</label></td>
															<td align="center"><label>
																<input name="INSERSI[]" type="checkbox" <?php if(in_array("2", $INSERSI)){ echo " checked=\"checked\""; } ?> value="2" id="INSERSITDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label></td>
														</tr>
														<tr>
															<td>3</td>
															<td>Melakukan kebersihan tangan</td>
															<td align="center">
																<label>
																	<input type="checkbox" name="BERSIH_TNGN[]" <?php if(in_array("1", $BERSIH_TNGN)){ echo " checked=\"checked\""; } ?> id="BERSIH_TNGN" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="BERSIH_TNGN[]" type="checkbox" <?php if(in_array("2", $BERSIH_TNGN)){ echo " checked=\"checked\""; } ?> value="2" id="BERSIH_TNGNTDK" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Menggunakan APD maksimal (Apron, topi, masker, sarungan tangan steril)</td>
															<td align="center">
																<label>
																	<input name="APD_MAXIMAL[]" type="checkbox" value="1" <?php if(in_array("1", $APD_MAXIMAL)){ echo " checked=\"checked\""; } ?> id="APD_MAXIMAL" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="APD_MAXIMAL[]" type="checkbox" value="2" <?php if(in_array("2", $APD_MAXIMAL)){ echo " checked=\"checked\""; } ?> id="APD_MAXIMALTDK" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>5</td>
															<td>Skin preparation dengan chlorhexidine 2% dan alcohol 70%, tunggu sampai 20 detik sebelum inserasi</td>
															<td align="center">
																<label>
																	<input name="PREPARATION[]" type="checkbox" value="1" <?php if(in_array("1", $PREPARATION)){ echo " checked=\"checked\""; } ?> id="PREPARATION" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="PREPARATION[]" type="checkbox" value="2" <?php if(in_array("2", $PREPARATION)){ echo " checked=\"checked\""; } ?> id="PREPARATIONTDK" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Menutup area penusukan dengan menggunakan kassa steril satu lembar dan transparan dressing</td>
															<td align="center">
																<label>
																	<input name="DRESSING[]" type="checkbox" value="1" <?php if(in_array("1", $DRESSING)){ echo " checked=\"checked\""; } ?> id="DRESSING" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DRESSING[]" type="checkbox" value="2" <?php if(in_array("2", $DRESSING)){ echo " checked=\"checked\""; } ?> id="DRESSINGTDK" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td style="text-align:center" colspan="2">Total</td>
															<td style="text-align:center"></td>
															<td style="text-align:center"></td>
														</tr>
														<tr>
															<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
															<td colspan="2" style="text-align:center">
																<div class="input-group">
																	<input name="TOTAL" value="<?php echo $dt['TOTAL']; ?>" type="text" id="total" style="width:60px"></br>
																	<span class="input-group-addon">
																		%
																	</span>
																</div>														
															</td>														
														</tr>														
													</tbody>
												</table>											
											</div>
										</div>													
										<div class="form-group">										
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
												<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
											</div>				
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
												<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
											</div>
										</div>
										<div class="form-group">			
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
												<input type="text" id="nama" value="<?php echo $dt['KEPALA']; ?>" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
											</div>	
											<div class="col-md-6 col-sm-12">	
												<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
												<input type="text" id="nama" value="<?php echo $dt['SURVEYOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
											</div>								
										</div>	
										<hr>
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													%
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->	
		</div> <!-- container -->
	</div><!-- /.main-content -->
	<?php
}?>