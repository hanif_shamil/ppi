<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>

<?php
$id=($_GET['id']);
$query="select * from db_ppi.tb_akj_new where ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">FORMULIR AUDIT KAMAR JENAZAH </li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">FORMULIR AUDIT KAMAR JENAZAH </h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_akj_new" method="post">
								<input type="hidden" value="<?php echo $dt['ID']; ?>"  name="ID" >										
									<div class="form-group">
										<div class="col-sm-1">	
											<label class="control-label bolder blue">TANGGAL</label>
										</div>								
										<div class="col-sm-3">									
											<div class="input-group">
												<input class="form-control" value="<?php echo $dt['TANGGAL']; ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>												
										</div>
									</div>

									<div class="form-group">																				
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> KEGIATAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="5%" style="text-align:center">NA</th>
															<th width="25%" style="text-align:center">KETERANGAN</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>A.</td>
															<td>KEPATUHAN PENGGUNAAN APD PADA SAAT PENGAMBILAN JENAZAH DARI UNIT</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Masker</td>
															<td align="center">
																<label><input type="radio" <?php if($dt['A1']=='1') echo " checked "?> name="A1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" <?php if($dt['A1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" <?php if($dt['A1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" value="<?php echo $dt['KET_A1']; ?>" id="nama" name="KET_A1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Sarung Tangan Bersih
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['A2']=='1') echo " checked "?> name="A2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['A2']=='2') echo " checked "?> name="A2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['A2']=='3') echo " checked "?> name="A2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" value="<?php echo $dt['KET_A2']; ?>" id="nama" name="KET_A2" placeholder="" class="form-control" />		
															</td>
														</tr>
								
														<tr>
															<td>B.</td>
															<td>KEPATUHAN PENGGUNAAN APD PADA SAAT MEMANDIKAN JENAZAH</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Topi</td>
															<td align="center"><label>
																<input type="radio" <?php if($dt['B1']=='1') echo " checked "?> name="B1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="B1" <?php if($dt['B1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="B1" <?php if($dt['B1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" value="<?php echo $dt['KET_B1']; ?>" id="nama" name="KET_B1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Masker
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B2']=='1') echo " checked "?> name="B2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B2']=='2') echo " checked "?> name="B2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B2']=='3') echo " checked "?> name="B2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" value="<?php echo $dt['KET_B2']; ?>" id="nama" name="KET_B2" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>3
															</td>
															<td>Kaca mata pelindung
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B3']=='1') echo " checked "?> name="B3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B3']=='2') echo " checked "?> name="B3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B3']=='3') echo " checked "?> name="B3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" value="<?php echo $dt['KET_B3']; ?>" id="nama" name="KET_B3" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>4
															</td>
															<td>Apron
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B4']=='1') echo " checked "?> name="B4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B4']=='2') echo " checked "?> name="B4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B4']=='3') echo " checked "?> name="B4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" value="<?php echo $dt['KET_B4']; ?>" id="nama" name="KET_B4" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>5
															</td>
															<td>Sarung Tangan Karet
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B5']=='1') echo " checked "?> name="B5" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B5']=='2') echo " checked "?> name="B5" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B5']=='3') echo " checked "?> name="B5" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" value="<?php echo $dt['KET_B5']; ?>" id="nama" name="KET_B5" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>6
															</td>
															<td>Sepatu pelindung
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B6']=='1') echo " checked "?> name="B6" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B6']=='2') echo " checked "?> name="B6" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['B6']=='3') echo " checked "?> name="B6" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" value="<?php echo $dt['KET_B6']; ?>" id="nama" name="KET_B6" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>C.</td>
															<td>KEBERSIHAN RUANG DAN FASILITAS KAMAR JENAZAH </td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1
															</td>
															<td>Suhu 18 - 25 ºC, kelembaban 45 - 70%
															</td>
															<td align="center">
																<label>
																	<input type="radio" <?php if($dt['C1']=='1') echo " checked "?> name="C1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																	</label
																</td>
																<td align="center">
																	<label>
																		<input name="C1" <?php if($dt['C1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="C1" <?php if($dt['C1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_C1']; ?>" id="nama" name="KET_C1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Permukaan Tidak Berdebu</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C2']=='1') echo " checked "?> name="C2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>

																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C2']=='2') echo " checked "?> name="C2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C2']=='3') echo " checked "?> name="C2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_C2']; ?>" id="nama" name="KET_C2" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Dinding tidak Berjamur</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C3']=='1') echo " checked "?> name="C3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C3']=='2') echo " checked "?> name="C3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C3']=='3') echo " checked "?> name="C3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_C3']; ?>" id="nama" name="KET_C3" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Tidak ada Lawa-lawa
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C4']=='1') echo " checked "?> name="C4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C4']=='2') echo " checked "?> name="C4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C4']=='3') echo " checked "?> name="C4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C4']; ?>" name="KET_C4" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>5</td>
																<td>Tempat Limbah Tertutup
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C5']=='1') echo " checked "?> name="C5" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C5']=='2') echo " checked "?> name="C5" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C5']=='3') echo " checked "?> name="C5" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C5']; ?>" name="KET_C5" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>6</td>
																<td>Fasilitas Kebersihan Tangan lengkap : Sabun antiseptik,  paper towel dan  Handrub
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C6']=='1') echo " checked "?> name="C6" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C6']=='2') echo " checked "?> name="C6" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C6']=='3') echo " checked "?> name="C6" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C6']; ?>" name="KET_C6" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>7</td>
																<td>Tempat transit Jenazah bersih
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C7']=='1') echo " checked "?> name="C7" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C7']=='2') echo " checked "?> name="C7" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C7']=='3') echo " checked "?> name="C7" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C7']; ?>" name="KET_C7" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>8</td>
																<td> Keranda  Jenazah bersih dan tidak berkarat
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C8']=='1') echo " checked "?> name="C8" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C8']=='2') echo " checked "?> name="C8" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C8']=='3') echo " checked "?> name="C8" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C8']; ?>" name="KET_C8" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>9</td>
																<td>Kain Penutup keranda bersih
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C9']=='1') echo " checked "?> name="C9" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C9']=='2') echo " checked "?> name="C9" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C9']=='3') echo " checked "?> name="C9" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C9']; ?>" name="KET_C9" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>10</td>
																<td>Gorden bersih
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C10']=='1') echo " checked "?> name="C10" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C10']=='2') echo " checked "?> name="C10" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['C10']=='3') echo " checked "?> name="C10" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C10']; ?>" name="KET_C10" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>D.</td>
																<td>KESESUAIAN ALUR</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Limbah Non Infeksius menggunakan plastik hitam</td>
																<td align="center"><label>
																	<input type="radio" <?php if($dt['D1']=='1') echo " checked "?> name="D1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="D1" <?php if($dt['D1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="D1" <?php if($dt['D1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_D1']; ?>" id="nama" name="KET_D1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Limbah Infeksius menggunakan plastik kuning</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['D2']=='1') echo " checked "?> name="D2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['D2']=='2') echo " checked "?> name="D2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['D2']=='3') echo " checked "?> name="D2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_D2']; ?>" id="nama" name="KET_D2" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Penempatan Limbah B3 sesuai tempatnya</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['D3']=='1') echo " checked "?> name="D3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['D3']=='2') echo " checked "?> name="D3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['D3']=='3') echo " checked "?> name="D3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_D3']; ?>" id="nama" name="KET_D3" placeholder="" class="form-control" />	
																</td>
															</tr>	
															<tr>
																<td>E.</td>
																<td>KEPATUHAN PENGELOLAAN LINEN</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Linen Non Infeksius dimasukkan ke dalam plastik hitam</td>
																<td align="center"><label>
																	<input type="radio" <?php if($dt['E1']=='1') echo " checked "?> name="E1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="E1" <?php if($dt['E1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="E1" <?php if($dt['E1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_E1']; ?>" id="nama" name="KET_E1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Linen Infeksius dimasukkan ke dalam plastik kuning</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E2']=='1') echo " checked "?> name="E2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E2']=='2') echo " checked "?> name="E2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E2']=='3') echo " checked "?> name="E2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" value="<?php echo $dt['KET_E2']; ?>" id="nama" name="KET_E2" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td style="text-align:center" colspan="4">JUMLAH TOTAL</td>
																<td colspan="3" style="text-align:center">
																	<div class="input-group">
																		<input type="text" id="total" name="NILAI" value="<?php echo $dt['NILAI']; ?>" placeholder="[ NILAI ]" class="form-control" />
																		<span class="input-group-addon">
																			%
																		</span>
																	</div>													
																</td>														
															</tr>													
														</tbody>
													</table>																									
												</div>
											</div>
										</div>																										
										<hr />
										<div class="form-group">										
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
												<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
											</div>				
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
												<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
											</div>
										</div>
										<div class="form-group">			
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">KOORDINATOR KAMAR JENAZAH</label>	
												<input type="text" id="nama" value="<?php echo $dt['KOORDINATOR']; ?>" name="KOORDINATOR" placeholder="[ KOORDINATOR CSSD ]" class="form-control" />												
											</div>	
											<div class="col-md-6 col-sm-12">	
												<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
												<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
											</div>								
										</div>																											
										<hr />
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->	
<?php }
?>