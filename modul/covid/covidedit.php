<?php
$id=($_GET['id']);
$query="SELECT cov.*, ru.ID ID_RUANGAN, ru.DESKRIPSI RUANGAN, IF(ps.JENIS_KELAMIN=1,'Laki-laki',IF(ps.JENIS_KELAMIN=2,'Perempuan','-')) JENIS_KELAMIN
, neg.DESKRIPSI WARGANEGARA, kerja.DESKRIPSI PEKERJAAN, kon.NOMOR TELPON, pp.TANGGAL TGLMASUK_RI, kip.NOMOR KTP, ps.ALAMAT, cov.dxmedis
, CONCAT(dia.CODE,' - ',dia.STR) DIAGNOSIS
FROM db_ppi.tb_covid cov
left join ruangan ru ON ru.ID=cov.ruangan	
left join master.pasien ps ON ps.NORM=cov.nomr	
LEFT JOIN master.negara neg ON neg.ID=ps.KEWARGANEGARAAN	
LEFT JOIN master.referensi kerja ON kerja.ID=ps.PEKERJAAN AND kerja.JENIS=4
LEFT JOIN master.kontak_pasien kon ON kon.NORM=ps.NORM
LEFT JOIN pendaftaran.pendaftaran pp ON ps.NORM=pp.NORM
left join pendaftaran.kunjungan pk ON pk.NOPEN=pp.NOMOR
LEFT JOIN master.kartu_identitas_pasien kip ON kip.NORM=ps.NORM
LEFT JOIN master.mrconso dia ON dia.CODE=cov.dxmedis
where cov.id='$id'
ORDER BY pk.MASUK desc limit 1";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>
					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">Form Covid-19</li>
				</ul><!-- /.breadcrumb -->			
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Formulir Covid-19</h4>
								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>
									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpancovid" method="post">
										<input type="hidden" name="id" value="<?php echo $dt['id']; ?>">
										<div class="form-group">
											<div class="col-sm-2">
												<input type="text" id="nomr" name="nomr" autocomplete="off" value="<?php echo $dt['nomr']; ?>" placeholder="[ No.RM ]" class="form-control" onkeyup="autofill()" />
												<input type="hidden" name="nopen" value="<?php echo $dt['nopen']; ?>"  class="form-control" readonly />
											</div>							
											<div class="col-sm-3">
												<input type="text" id="nama" name="nama" value="<?php echo $dt['nama']; ?>" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
											</div>
											<div class="col-sm-2">
												<input type="text" id="tgl_lahir" name="tgl_lahir" value="<?php echo $dt['tgl_lahir']; ?>" placeholder="[ TANGGAL LAHIR ]" class="form-control" readonly />
											</div>														
											<div class="col-sm-2">
												<input name="" id="jk" placeholder="[ JENIS KELAMIN ]" value="<?php echo $dt['JENIS_KELAMIN']; ?>" class="form-control" readonly> 
											</div>
											<div class="col-sm-3">
												<input type="text" id="ktp" name="" placeholder=" [ NO. KTP ]" value="<?php echo $dt['KTP']; ?>" jkclass="form-control" autocomplete="off" readonly/>
											</div>
										</div>	

										<div class="form-group">
											<div class="col-sm-2">
												<input type="text" id="warganegara" name="" value="<?php echo $dt['WARGANEGARA']; ?>" autocomplete="off" placeholder="[ KEWARGANEGARAAN ]" class="form-control" readonly>
											</div>							
											<div class="col-sm-3">
												<input type="text" id="pekerjaan" name="" value="<?php echo $dt['PEKERJAAN']; ?>" placeholder="[ PEKERJAAN ]" class="form-control" readonly />
											</div>
											<div class="col-sm-2">
												<input type="text" id="telpon" name="" value="<?php echo $dt['TELPON']; ?>" placeholder="[ NO TELPON AKTIF ]" class="form-control" readonly />
											</div>														
											<div class="col-sm-5">
												<input name="" id="alamat" value="<?php echo $dt['ALAMAT']; ?>" placeholder="[ ALAMAT ]" class="form-control" readonly> 

											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-4">
												<input type="text" id="ruangan" name="" value="<?php echo $dt['RUANGAN']; ?>" placeholder=" [ RUANG RAWAT SAAT INI ]" class="form-control" autocomplete="off" readonly/>
												<input type="hidden" value="<?php echo $dt['ID_RUANGAN']; ?>" name="ruangan" class="form-control">
											</div>
											<div class="col-sm-3">

												<div class="input-group date">
													<input type="text" id="tgl_masuk" name="" value="<?php echo $dt['TGLMASUK_RI']; ?>" placeholder=" [ TANGGAL MASUK RUANG RAWAT ]" class="form-control" autocomplete="off" readonly/>
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>

											</div>
											<div class="col-sm-5">
												<!-- <input type="text" id="form-field-1-1" name="" value="<?php echo htmlspecialchars($dt['DIAGNOSIS']); ?>" placeholder=" [ DIAGNOSA KANKER]" class="form-control" readonly/> -->
												<select class="chosen-select form-control" name="dxmedis" id="form-field-select-3" data-placeholder="[ Antibiotik 1]">
												<option value=""></option>
												<?php
												$con73=mysqli_connect('192.168.7.3','simrsdev','G0l0ks4kt1');
												$sql = "select dia.ID, dia.ICD, dia.DIAGNOSA, CONCAT(dia.ICD,'-',dia.DIAGNOSA) DIAGMASUK
												FROM master.diagnosa_masuk dia
												GROUP BY dia.ICD";												
												$rs = mysqli_query($con73,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['dxmedis']==$data['ICD']){echo "selected";}?> value="<?php echo $data['ICD']; ?>"><?php echo $data['DIAGMASUK'] ?></option>			
												<?php
												}
												?>	
											</select>
											</select>
											</div>
										</div>

										<!-- <div class="form-group">
											<div class="col-sm-2">
												<input type="text" id="nomr" name="nomr" value="<?php echo $dt['nomr']; ?>" autocomplete="off" placeholder="[ No.RM ]" class="form-control" onkeyup="autofill()" />
											</div>							
											<div class="col-sm-3">
												<input type="text" id="nama" name="nama" value="<?php echo $dt['nama']; ?>" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
											</div>		
											<div class="col-sm-2">
												<input type="text" id="tgl_lahir" name="tgl_lahir" value="<?php echo $dt['tgl_lahir']; ?>" placeholder="[ TANGGAL LAHIR ]" class="form-control" readonly />
											</div>													
											<div class="col-sm-3">
												<input name="" id="ruangan" placeholder="[ RUANGAN ]" value="<?php echo $dt['RUANGAN']; ?>" class="form-control" readonly> 
												<input type="hidden" name="ruangan" id="id_ruangan" value="<?php echo $dt['ID_RUANGAN']; ?>" class="form-control">
											</div>
											<div class="col-sm-2">
												<input type="text" id="form-field-1-1" value="<?php echo $dt['dxmedis']; ?>" name="dxmedis" placeholder=" [ DIAGNOSA ]" class="form-control" />
											</div>
										</div>	 -->						
										<div class="form-group">
											
										</div>
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">							
											<label class="control-label bolder blue">GEJALA :</label>			
											<div class="form-group">
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Demam &#8805; 38 &#8451;</label>
														<div class="radio">
															<label>
																<input name="demam" value="1" <?php if($dt['demam']=='1') echo " checked "?> type="radio" class="ace" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="demam" value="2" <?php if($dt['demam']=='2') echo " checked "?> type="radio" class="ace"  />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Batuk</label>
														<div class="radio">
															<label>
																<input name="batuk" value="1" <?php if($dt['batuk']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="batuk" value="2" <?php if($dt['batuk']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Pilek</label>
														<div class="radio">
															<label>
																<input name="pilek" value="1" <?php if($dt['pilek']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="pilek" value="2" <?php if($dt['pilek']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Sakit Tenggorokan</label>
														<div class="radio">
															<label>
																<input name="tenggorokan" value="1" <?php if($dt['tenggorokan']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="tenggorokan" value="2" <?php if($dt['tenggorokan']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Sesak nafas</label>
														<div class="radio">
															<label>
																<input name="nafas" value="1" <?php if($dt['nafas']=='1') echo " checked "?> type="radio" class="ace"  required="on"/>
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="nafas" value="2" <?php if($dt['nafas']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>	
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Sakit Kepala</label>
														<div class="radio">
															<label>
																<input name="kepala" value="1" <?php if($dt['kepala']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="kepala" value="2" <?php if($dt['kepala']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Lemah (Malaise)</label>
														<div class="radio">
															<label>
																<input name="lemah" value="1" <?php if($dt['lemah']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="lemah" value="2" <?php if($dt['lemah']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Nyeri Otot</label>
														<div class="radio">
															<label>
																<input name="otot" value="1" <?php if($dt['otot']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="otot" value="2" <?php if($dt['otot']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Mual atau Muntah</label>
														<div class="radio">
															<label>
																<input name="mual" value="1" <?php if($dt['mual']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="mual" value="2" <?php if($dt['mual']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Nyeri Abdomen</label>
														<div class="radio">
															<label>
																<input name="nyeri" value="1" <?php if($dt['nyeri']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="nyeri" value="2" <?php if($dt['nyeri']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Diare</label>
														<div class="radio">
															<label>
																<input name="diare" value="1" <?php if($dt['diare']=='1') echo " checked "?> type="radio" class="ace" required="on"/>
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="diare" value="2" <?php if($dt['diare']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>										
											</div>
										</div>
										<div class="form-group">
										</div>
										<div class="form-group">
											<div class="col-sm-7">
												<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
													<label class="control-label bolder blue">DIAGNOSA MASUK :</label>
													<div class="form-group">
														<div class="col-sm-6">
															<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
																<label class="control-label bolder blue" style="text-decoration: underline">Pneumonia (Klinis atau Radiologi)</label>
																<div class="radio">
																	<label>
																		<input name="pneumonia" value="1" <?php if($dt['pneumonia']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																		<span class="lbl"> Ya</span>
																	</label>
																	<label>
																		<input name="pneumonia" value="2" <?php if($dt['pneumonia']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																		<span class="lbl"> Tidak</span>
																	</label>
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
																<label class="control-label bolder blue"  style="text-decoration: underline">ARDS (Acute Respiratory Syndrome)</label>
																<div class="radio">
																	<label>
																		<input name="ards" value="1" <?php if($dt['ards']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																		<span class="lbl"> Ya</span>
																	</label>
																	<label>
																		<input name="ards" value="2" <?php if($dt['ards']=='2') echo " checked "?> type="radio" class="ace" required="on" />
																		<span class="lbl"> Tidak</span>
																	</label>
																</div>
															</div>
														</div>										
													</div>
												</div>
											</div>
											<div class="col-sm-5">
												<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
													<label class="control-label bolder blue">PASIEN RUJUKAN :</label>
													<div class="form-group">
														<div class="col-sm-12">
															<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
																<label class="control-label bolder blue" style="text-decoration: underline">Hasil SWAB pada surat rujukan POSITIF</label>
																<div class="radio">
																	<label>
																		<input name="surat_swab" value="1" <?php if($dt['surat_swab']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																		<span class="lbl"> Ya</span>
																	</label>
																	<label>
																		<input name="surat_swab" value="2" <?php if($dt['surat_swab']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
																		<span class="lbl"> Tidak</span>
																	</label>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
										</div>
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
											<label class="control-label bolder blue">FAKTOR RIWAYAT PERJALANAN :</label>
											<div class="table-responsive-md">
												<table class="table table-bordered text-dark-m1">
													<tbody>
														<tr>	
															<td><label class="control-label bolder blue">Dalam 14 hari sebelum sakit, apakah memiliki riwayat perjalanan dari luar negeri</label></td>
															<td><div class="radio">
																<label>
																	<input name="perjalanan" value="1" <?php if($dt['perjalanan']=='1') echo " checked "?> type="radio" class="ace" required="on"/>
																	<span class="lbl"> Ya</span>
																</label>
																<label>
																	<input name="perjalanan" value="2" <?php if($dt['perjalanan']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
																	<span class="lbl"> Tidak</span>
																</label>
															</div>
														</td>
													</tr>
													<tr>
														<td><label class="control-label bolder blue">Dalam 14 hari sebelum sakit, apakah memiliki riwayat perjalanan dari transmisi lokal</label></td>
														<td><div class="radio">
															<label>
																<input name="perjalanan_transmisi" value="1" <?php if($dt['perjalanan_transmisi']=='1') echo " checked "?> type="radio" class="ace" required="on"/>
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="perjalanan_transmisi" value="2" <?php if($dt['perjalanan_transmisi']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<label class="control-label bolder blue">Dalam 14 hari sebelum sakit, apakah memiliki riwayat tinggal ke daerah transmisi lokal</label></td>
														<td><div class="radio">
															<label>
																<input name="transmisi_lokal" value="1" <?php if($dt['transmisi_lokal']=='1') echo " checked "?> type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="transmisi_lokal" value="2" <?php if($dt['transmisi_lokal']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</td>
												</tr>
												<tr>	
													<td><label class="control-label bolder blue">Dalam 14 hari sebelum sakit, apakah memiliki kontak dengan kasus suspek/probable COVID-19</label></td>
													<td><div class="radio">
														<label>
															<input name="kontak_suspek" value="1" <?php if($dt['kontak_suspek']=='1') echo " checked "?> type="radio" class="ace" required="on"/>
															<span class="lbl"> Ya</span>
														</label>
														<label>
															<input name="kontak_suspek" value="2" <?php if($dt['kontak_suspek']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
															<span class="lbl"> Tidak</span>
														</label>
													</div>
												</td>
											</tr>
											<tr>
												<td><label class="control-label bolder blue">Dalam 14 hari sebelum sakit, apakah memiliki kontak dengan kasus konfirmasi dan probable COVID-19</label></td>
												<td><div class="radio">
													<label>
														<input name="kontak_konfirmasi" value="1" <?php if($dt['kontak_konfirmasi']=='1') echo " checked "?> type="radio" class="ace" required="on"/>
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="kontak_konfirmasi" value="2" <?php if($dt['kontak_konfirmasi']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td><label class="control-label bolder blue">Apakah pasien termasuk dalam cluster ISPA berat (demam dan pneumonia membutuhkan perawatan RS) yang tidak diketahui penyebabnya</label></td>
											<td><div class="radio">
												<label>
													<input name="ispa" value="1" type="radio" <?php if($dt['ispa']=='1') echo " checked "?> class="ace" required="on"/>
													<span class="lbl"> Ya</span>
												</label>
												<label>
													<input name="ispa" value="2" type="radio" <?php if($dt['ispa']=='2') echo " checked "?> class="ace" required="on"/>
													<span class="lbl"> Tidak</span>
												</label>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="form-group">
					</div>

					<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
						<label class="control-label bolder blue">STATUS PASIEN :</label>
						<div class="form-group">
							<div class="col-sm-3">
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
									<label class="control-label bolder blue" style="text-decoration: underline">Suspect</label>
									<div class="radio">
										<label>
											<input name="suspect" value="1" <?php if($dt['suspect']=='1') echo " checked "?> type="radio" class="ace" required="on"/>
											<span class="lbl"> Ya</span>
										</label>
										<label>
											<input name="suspect" value="2" <?php if($dt['suspect']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
											<span class="lbl"> Tidak</span>
										</label>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
									<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Suspect</label>
									<div class="input-group">
										<input class="form-control" id="datetimepicker" value="<?php echo $dt['tanggal_suspect']; ?>" autocomplate="off" placeholder="[ Tanggal Rapid 1 ]" name="tanggal_suspect" type="text"/>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>	
								</div>
							</div>	
							<div class="col-sm-3">
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
									<label class="control-label bolder blue" style="text-decoration: underline">Probable</label>
									<div class="radio">
										<label>
											<input name="probable" value="1" <?php if($dt['probable']=='1') echo " checked "?> type="radio" class="ace" required="on"/>
											<span class="lbl"> Ya</span>
										</label>
										<label>
											<input name="probable" value="2" <?php if($dt['probable']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
											<span class="lbl"> Tidak</span>
										</label>
									</div>
								</div>
							</div>	
							<div class="col-sm-3">
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
									<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Probable</label>
									<div class="input-group">
										<input class="form-control" id="datetimepicker1" value="<?php echo $dt['tanggal_probable']; ?>" autocomplate="off" placeholder="[ Tanggal probable ]" name="tanggal_probable" type="text"/>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>										
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
									<label class="control-label bolder blue" style="text-decoration: underline">Konfirmasi</label>
									<div class="radio">
										<label>
											<input name="konfirmasi" <?php if($dt['konfirmasi']=='1') echo " checked "?> value="1" type="radio" class="ace" />
											<span class="lbl"> Ya</span>
										</label>
										<label>
											<input name="konfirmasi" <?php if($dt['konfirmasi']=='2') echo " checked "?> value="2" type="radio" class="ace" />
											<span class="lbl"> Tidak</span>
										</label>
									</div>
								</div>	
							</div>
							<div class="col-sm-3">
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
									<label class="control-label bolder blue" style="text-decoration: underline">Tanggal konfirmasi</label>
									<div class="input-group">
										<input class="form-control" id="datetimepicker11" autocomplate="off" value="<?php echo $dt['tanggal_konfirmasi']; ?>" placeholder="[ Tanggal konfirmasi ]" name="tanggal_konfirmasi" type="text"/>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>	
								</div>
							</div>	
							<div class="col-sm-3">
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
									<label class="control-label bolder blue" style="text-decoration: underline">Discarded</label>
									<div class="radio">
										<label>
											<input name="discarded" value="1" <?php if($dt['discarded']=='1') echo " checked "?> type="radio" class="ace" required="on" />
											<span class="lbl"> Ya</span>
										</label>
										<label>
											<input name="discarded" value="2" <?php if($dt['discarded']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
											<span class="lbl"> Tidak</span>
										</label>
									</div>
								</div>
							</div>	
							<div class="col-sm-3">
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
									<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Discarded</label>
									<div class="input-group">
										<input class="form-control" id="datetimepicker12" value="<?php echo $dt['tanggal_konfirmasi']; ?>" autocomplate="off" placeholder="[ Tanggal Discarded ]" name="tanggal_discarded" type="text"/>
										<span class="input-group-addon">
											<i class="fa fa-calendar bigger-110"></i>
										</span>
									</div>
								</div>
							</div>										
						</div>
					</div>
					<div class="form-group">
					</div>

					<div class="form-group">
						<div class="col-sm-3">	
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">

								<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Rapid test 1</label>	
								<div class="input-group">
									<input class="form-control" id="datetimepicker13" value="<?php echo $dt['tanggal_rapid1']; ?>" placeholder="[ Tanggal Rapid 1 ]" name="tanggal_rapid1" type="text"/>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>	
							</div>
						</div>	
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Hasil Rapid Test 1</label>
								<div class="radio">
									<label>
										<input name="rapid1" value="1" <?php if($dt['rapid1']=='1') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Positif</span>
									</label>
									<label>
										<input name="rapid1" value="2" <?php if($dt['rapid1']=='2') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Negatif</span>
									</label>
								</div>
							</div>
						</div>		
						<div class="col-sm-3">	
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">

								<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Rapid test 2</label>	
								<div class="input-group">
									<input class="form-control" id="datetimepicker14" value="<?php echo $dt['tanggal_rapid2']; ?>" placeholder="[ Tanggal Rapid 2 ]" name="tanggal_rapid2" type="text"/>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>	
							</div>
						</div>	
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">

								<label class="control-label bolder blue" style="text-decoration: underline">Hasil Rapid Test 2</label>
								<div class="radio">
									<label>
										<input name="rapid2" value="1" <?php if($dt['rapid2']=='1') echo " checked "?> rapid1 type="radio" class="ace" />
										<span class="lbl"> Positif</span>
									</label>
									<label>
										<input name="rapid2" value="2" <?php if($dt['rapid2']=='2') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Negatif</span>
									</label>
								</div>
							</div>
						</div>												
					</div>
					<div class="form-group">
						<div class="col-sm-3">	
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Swab 1</label>	
								<div class="input-group">
									<input class="form-control" id="datet1" value="<?php echo $dt['tanggal_swab1']; ?>" placeholder="[ Tanggal Swab 1 ]" name="tanggal_swab1" type="text"/>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>	
							</div>											
						</div>	
						<div class="col-sm-3">											
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Hasil Swab 1</label>
								<div class="radio">
									<label>
										<input name="swab1" value="1" <?php if($dt['swab1']=='1') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Positif</span>
									</label>
									<label>
										<input name="swab1" value="2" <?php if($dt['swab1']=='2') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Negatif</span>
									</label>
								</div>	
							</div>		
						</div>		
						<div class="col-sm-3">	
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Swab 2</label>	
								<div class="input-group">
									<input class="form-control" id="datet2" value="<?php echo $dt['tanggal_swab2']; ?>" placeholder="[ Tanggal Swab 2 ]" name="tanggal_swab2" type="text"/>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>	
							</div>											
						</div>	
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Hasil Swab 2</label>
								<div class="radio">
									<label>
										<input name="swab2" value="1" <?php if($dt['swab2']=='1') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Positif</span>
									</label>
									<label>
										<input name="swab2" value="2" <?php if($dt['swab2']=='2') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Negatif</span>
									</label>
								</div>											
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">	
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Swab 3</label>	
								<div class="input-group">
									<input class="form-control" id="datet3" value="<?php echo $dt['tanggal_swab3']; ?>" placeholder="[ Tanggal Swab 3 ]" name="tanggal_swab3" type="text"/>
									<span class="input-group-addon">
										<i class="fa fa-calendar bigger-110"></i>
									</span>
								</div>		
							</div>	
						</div>
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Hasil Swab 3</label>
								<div class="radio">
									<label>
										<input name="swab3" value="1" <?php if($dt['swab3']=='1') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Positif</span>
									</label>
									<label>
										<input name="swab3" value="2" <?php if($dt['swab3']=='2') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Negatif</span>
									</label>
								</div>
							</div>
						</div>
						<!-- <div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">PDP</label>
								<div class="radio">
									<label>
										<input name="pdp" value="1" <?php if($dt['pdp']=='1') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="pdp" value="2" <?php if($dt['pdp']=='2') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">ODP</label>
								<div class="radio">
									<label>
										<input name="odp" value="1" <?php if($dt['odp']=='1') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="odp" value="2" <?php if($dt['odp']=='2') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">OTG</label>
								<div class="radio">
									<label>
										<input name="otg" <?php if($dt['otg']=='1') echo " checked "?> value="1" type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="otg" <?php if($dt['otg']=='2') echo " checked "?> value="2" type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>	
						</div>
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Konfirmasi</label>
								<div class="radio">
									<label>
										<input name="konfirmasi" <?php if($dt['konfirmasi']=='1') echo " checked "?> value="1" type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="konfirmasi" <?php if($dt['konfirmasi']=='2') echo " checked "?> value="2" type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>	
						</div>
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Sembuh</label>
								<div class="radio">
									<label>
										<input name="sembuh" value="1" <?php if($dt['sembuh']=='1') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="sembuh" value="2" <?php if($dt['sembuh']=='2') echo " checked "?> type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>	
						</div> -->

						<div class="col-sm-6">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Keterangan</label>
								<input type="text" id="form-field-1-1" value="<?php echo $dt['keterangan']; ?>" name="keterangan" placeholder="[ KETERANGAN ]" class="form-control" />
							</div>	
						</div>										
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">STATUS AKHIR</label>
								<div class="radio">
									<label>
										<input name="status_akhir" value="1" <?php if($dt['status_akhir']=='1') echo " checked "?> type="radio" class="ace" required="on"/>
										<span class="lbl"> Meninggal</span>
									</label>
									<label>
										<input name="status_akhir" value="2" <?php if($dt['status_akhir']=='2') echo " checked "?> type="radio" class="ace" required="on"/>
										<span class="lbl"> Dirujuk</span>
									</label>
									<label>
										<input name="status_akhir" value="3" <?php if($dt['status_akhir']=='3') echo " checked "?> type="radio" class="ace" required="on"/>
										<span class="lbl"> Isolsasi mandiri</span>
									</label>
									<label>
										<input name="status_akhir" value="4" <?php if($dt['status_akhir']=='4') echo " checked "?> type="radio" class="ace" required="on"/>
										<span class="lbl"> APS</span>
									</label>
									<label>
										<input name="status_akhir" value="5" <?php if($dt['status_akhir']=='5') echo " checked "?> type="radio" class="ace" required="on"/>
										<span class="lbl"> APD/Sembuh</span>
									</label>
									<label>
										<input name="status_akhir" value="6" <?php if($dt['status_akhir']=='6') echo " checked "?> type="radio" class="ace" required="on"/>
										<span class="lbl"> Dirawat</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<hr />
					<div class="form-group">							
						<div class="col-md-offset-3 col-md-9">
							<button class="btn btn-info" name="btnEdit" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i>
								Simpan Edit
							</button>
							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset">
								<i class="ace-icon fa fa-undo bigger-110"></i>
								Reset
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div><!-- /.span -->
</div>
</div><!-- /.page-content -->	
</div> <!-- container -->
</div><!-- /.main-content -->
<?php
}?>