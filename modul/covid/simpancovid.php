<html>
<head>
	<script src="assets/sweetalert/dist/sweetalert-dev.js"></script>
	<link rel="stylesheet" href="assets/sweetalert/dist/sweetalert.css">

</head>
<body>

	<?php
	error_reporting(E_ALL & ~E_NOTICE);
	date_default_timezone_set('Asia/Jakarta');
	if(isset($_POST['btnsimpan'])){				
		$nomr 				= $_POST['nomr'];	
		$nama 				= addslashes($_POST['nama']);	
		$dxmedis 			= $_POST['dxmedis'];
		$nopen 				= $_POST['nopen'];
		$ruangan 			= $_POST['ruangan'];
		$tgl_lahir 			= $_POST['tgl_lahir'];	
		$demam 				= $_POST['demam'];	
		$batuk 				= $_POST['batuk'];	
		$pilek 				= $_POST['pilek'];	
		$tenggorokan 		= $_POST['tenggorokan'];	
		$nafas 				= $_POST['nafas'];	
		$kepala 			= $_POST['kepala'];	
		$lemah 				= $_POST['lemah'];	
		$otot 				= $_POST['otot'];	
		$mual 				= $_POST['mual'];	
		$nyeri 				= $_POST['nyeri'];	
		$diare 				= $_POST['diare'];	
		$pneumonia 			= $_POST['pneumonia'];	
		$ards 				= $_POST['ards'];	
		$surat_swab 		= $_POST['surat_swab'];	
		$perjalanan 		= $_POST['perjalanan'];	
		$perjalanan_transmisi = $_POST['perjalanan_transmisi'];	
		$transmisi_lokal 	= $_POST['transmisi_lokal'];	
		$kontak_suspek 		= $_POST['kontak_suspek'];	
		$kontak_konfirmasi 	= $_POST['kontak_konfirmasi'];	
		$ispa 				= $_POST['ispa'];	
		$suspect 			= $_POST['suspect'];	
		$tanggal_suspect 	= $_POST['tanggal_suspect'];	
		$probable 			= $_POST['probable'];	
		$tanggal_probable 	= $_POST['tanggal_probable'];	
		$konfirmasi 		= $_POST['konfirmasi'];	
		$tanggal_konfirmasi = $_POST['tanggal_konfirmasi'];	
		$discarded 			= $_POST['discarded'];	
		$tanggal_discarded 	= $_POST['tanggal_discarded'];	
		$aps 				= $_POST['aps'];	
		$tanggal_rapid1 	= $_POST['tanggal_rapid1'];	
		$rapid1 			= $_POST['rapid1'];	
		$tanggal_rapid2 	= $_POST['tanggal_rapid2'];	
		$rapid2 			= $_POST['rapid2'];		
		$tanggal_swab1 		= $_POST['tanggal_swab1'];	
		$swab1 				= $_POST['swab1'];		
		$tanggal_swab2 		= $_POST['tanggal_swab2'];
		$swab2 				= $_POST['swab2'];			
		$tanggal_swab3 		= $_POST['tanggal_swab3'];
		$swab3 				= $_POST['swab3'];			
		$keterangan 		= $_POST['keterangan'];	
		$status_akhir 		= $_POST['status_akhir'];	
		// $pdp 				= $_POST['pdp'];	
		// $odp 				= $_POST['odp'];	
		// $otg 				= $_POST['otg'];
		//$sembuh 			= $_POST['sembuh'];	
		$user 				= $_SESSION['userid'];	

		//echo "<pre>";print_r($nama);exit();  
		$query1 = "INSERT INTO db_ppi.tb_covid (
		nomr,
		nopen,
		nama,
		tgl_lahir,
		ruangan,
		dxmedis,
		
		demam, 
		batuk, 
		pilek, 
		tenggorokan, 
		nafas, 
		kepala, 
		lemah, 
		otot, 
		mual, 
		nyeri, 
		diare, 
		pneumonia, 
		ards, 
		surat_swab, 
		perjalanan, 
		perjalanan_transmisi, 
		transmisi_lokal, 
		kontak_suspek, 
		kontak_konfirmasi, 
		ispa, 
		suspect, 
		tanggal_suspect,
		probable, 
		tanggal_probable,
		discarded,
		tanggal_discarded, 
		aps, 
		tanggal_rapid1,
		rapid1,
		tanggal_rapid2,
		rapid2,
		tanggal_swab1,
		swab1,	
		tanggal_swab2,
		swab2,	
		tanggal_swab3,
		swab3,	
		konfirmasi,	
		tanggal_konfirmasi, 
		keterangan,
		status_akhir,
		user) 
		VALUES 
		('$nomr',
		'$nopen',
		'$nama',
		'$tgl_lahir',
		'$ruangan',
		'$dxmedis',
		'$demam', 
		'$batuk', 
		'$pilek', 
		'$tenggorokan', 
		'$nafas', 
		'$kepala', 
		'$lemah', 
		'$otot', 
		'$mual', 
		'$nyeri', 
		'$diare', 
		'$pneumonia', 
		'$ards', 
		'$surat_swab', 
		'$perjalanan', 
		'$perjalanan_transmisi', 
		'$transmisi_lokal', 
		'$kontak_suspek', 
		'$kontak_konfirmasi', 
		'$ispa', 
		'$suspect', 
		'$tanggal_suspect', 
		'$probable', 
		'$tanggal_probable',
		'$discarded', 
		'$tanggal_discarded',
		'$aps', 
		'$tanggal_rapid1',
		'$rapid1',
		'$tanggal_rapid2',	
		'$rapid2',		
		'$tanggal_swab1',				
		'$swab1',		
		'$tanggal_swab2',		
		'$swab2',			
		'$tanggal_swab3',
		'$swab3',
		'$konfirmasi',
		'$tanggal_konfirmasi', 
		'$keterangan',
		'$status_akhir',
		'$user')";
		//echo $query1; die();	
		$insert1=mysqli_multi_query($conn1,$query1);	
		$query2 = "INSERT INTO db_ppi.tb_log_covid (
		nomr,
		nopen,
		nama,
		tgl_lahir,
		ruangan,
		dxmedis,
		
		demam, 
		batuk, 
		pilek, 
		tenggorokan, 
		nafas, 
		kepala, 
		lemah, 
		otot, 
		mual, 
		nyeri, 
		diare, 
		pneumonia, 
		ards, 
		surat_swab, 
		perjalanan, 
		perjalanan_transmisi, 
		transmisi_lokal, 
		kontak_suspek, 
		kontak_konfirmasi, 
		ispa, 
		suspect, 
		tanggal_suspect,
		probable, 
		tanggal_probable,
		discarded,
		tanggal_discarded, 
		aps, 
		tanggal_rapid1,
		rapid1,
		tanggal_rapid2,
		rapid2,
		tanggal_swab1,
		swab1,	
		tanggal_swab2,
		swab2,	
		tanggal_swab3,
		swab3,	
		konfirmasi,	
		tanggal_konfirmasi, 
		keterangan,
		status_akhir,
		user) 
		VALUES 
		('$nomr',
		'$nopen',
		'$nama',
		'$tgl_lahir',
		'$ruangan',
		'$dxmedis',
		'$demam', 
		'$batuk', 
		'$pilek', 
		'$tenggorokan', 
		'$nafas', 
		'$kepala', 
		'$lemah', 
		'$otot', 
		'$mual', 
		'$nyeri', 
		'$diare', 
		'$pneumonia', 
		'$ards', 
		'$surat_swab', 
		'$perjalanan', 
		'$perjalanan_transmisi', 
		'$transmisi_lokal', 
		'$kontak_suspek', 
		'$kontak_konfirmasi', 
		'$ispa', 
		'$suspect', 
		'$tanggal_suspect', 
		'$probable', 
		'$tanggal_probable',
		'$discarded', 
		'$tanggal_discarded',
		'$aps', 
		'$tanggal_rapid1',
		'$rapid1',
		'$tanggal_rapid2',	
		'$rapid2',		
		'$tanggal_swab1',				
		'$swab1',		
		'$tanggal_swab2',		
		'$swab2',			
		'$tanggal_swab3',
		'$swab3',
		'$konfirmasi',
		'$tanggal_konfirmasi', 
		'$keterangan',
		'$status_akhir',
		'$user')";
		//echo $query2; die();	
		$insert2=mysqli_multi_query($conn1,$query2);	

		if($insert1 && $insert2){
			echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=listcovid'</script>"; 
		}
	}                        
	if(isset($_POST['btnEdit'])){
		$id     			= $_POST['id'];
		$nomr 				= $_POST['nomr'];	
		$nopen 				= $_POST['nopen'];
		$nama 				= addslashes($_POST['nama']);
		$dxmedis 			= $_POST['dxmedis'];
		$ruangan 			= $_POST['ruangan'];
		$tgl_lahir 			= $_POST['tgl_lahir'];	
		$demam 				= $_POST['demam'];	
		$batuk 				= $_POST['batuk'];	
		$pilek 				= $_POST['pilek'];	
		$tenggorokan 		= $_POST['tenggorokan'];	
		$nafas 				= $_POST['nafas'];	
		$kepala 			= $_POST['kepala'];	
		$lemah 				= $_POST['lemah'];	
		$otot 				= $_POST['otot'];	
		$mual 				= $_POST['mual'];	
		$nyeri 				= $_POST['nyeri'];	
		$diare 				= $_POST['diare'];	
		$pneumonia 			= $_POST['pneumonia'];	
		$ards 				= $_POST['ards'];	
		$surat_swab 		= $_POST['surat_swab'];	
		$perjalanan 		= $_POST['perjalanan'];	
		$perjalanan_transmisi = $_POST['perjalanan_transmisi'];	
		$transmisi_lokal 	= $_POST['transmisi_lokal'];	
		$kontak_suspek 		= $_POST['kontak_suspek'];	
		$kontak_konfirmasi 	= $_POST['kontak_konfirmasi'];	
		$ispa 				= $_POST['ispa'];	
		$suspect 			= $_POST['suspect'];	
		$tanggal_suspect 	= $_POST['tanggal_suspect'];	
		$probable 			= $_POST['probable'];	
		$tanggal_probable 	= $_POST['tanggal_probable'];	
		$konfirmasi 		= $_POST['konfirmasi'];	
		$tanggal_konfirmasi = $_POST['tanggal_konfirmasi'];	
		$discarded 			= $_POST['discarded'];	
		$tanggal_discarded 	= $_POST['tanggal_discarded'];	
		$aps 				= $_POST['aps'];	
		$tanggal_rapid1 	= $_POST['tanggal_rapid1'];	
		$rapid1 			= $_POST['rapid1'];	
		$tanggal_rapid2 	= $_POST['tanggal_rapid2'];	
		$rapid2 			= $_POST['rapid2'];		
		$tanggal_swab1 		= $_POST['tanggal_swab1'];	
		$swab1 				= $_POST['swab1'];		
		$tanggal_swab2 		= $_POST['tanggal_swab2'];
		$swab2 				= $_POST['swab2'];			
		$tanggal_swab3 		= $_POST['tanggal_swab3'];
		$swab3 				= $_POST['swab3'];			
		$keterangan 		= $_POST['keterangan'];	
		$status_akhir 		= $_POST['status_akhir'];	
		// $pdp 				= $_POST['pdp'];	
		// $odp 				= $_POST['odp'];	
		// $otg 				= $_POST['otg'];
		//$sembuh 			= $_POST['sembuh'];	
		$user 				= $_SESSION['userid'];		
						//echo "<pre>";print_r($_POST);exit();   						
		$query2 = "UPDATE db_ppi.tb_covid SET												
		nomr='$nomr',
		nopen='$nopen',
		nama='$nama',
		tgl_lahir='$tgl_lahir',
		ruangan='$ruangan',
		dxmedis='$dxmedis',
		
		demam = '$demam', 
		batuk = '$batuk', 
		pilek = '$pilek', 
		tenggorokan = '$tenggorokan', 
		nafas = '$nafas', 
		kepala = '$kepala', 
		lemah = '$lemah', 
		otot= '$otot', 
		mual= '$mual', 
		nyeri = '$nyeri', 
		diare= '$diare', 
		pneumonia = '$pneumonia', 
		ards = '$ards', 
		surat_swab = '$surat_swab', 
		perjalanan = '$perjalanan', 
		perjalanan_transmisi= '$perjalanan_transmisi', 
		transmisi_lokal = '$transmisi_lokal', 
		kontak_suspek = '$kontak_suspek', 
		kontak_konfirmasi = '$kontak_konfirmasi', 
		ispa = '$ispa', 
		suspect = '$suspect', 
		tanggal_suspect = '$tanggal_suspect', 
		probable = '$probable', 
		tanggal_probable = '$tanggal_probable',
		discarded = '$discarded', 
		tanggal_discarded = '$tanggal_discarded',
		aps = '$aps', 
		tanggal_rapid1='$tanggal_rapid1',
		rapid1='$rapid1',
		tanggal_rapid2='$tanggal_rapid2',
		rapid2='$rapid2',
		tanggal_swab1='$tanggal_swab1',
		swab1='$swab1',
		tanggal_swab2='$tanggal_swab2',
		swab2='$swab2',
		tanggal_swab3='$tanggal_swab3',
		swab3='$swab3',
		-- pdp='$pdp',
		-- odp='$odp',
		-- otg='$otg',
		konfirmasi='$konfirmasi',	
		tanggal_konfirmasi='$tanggal_konfirmasi', 
		keterangan='$keterangan',
		status_akhir= '$status_akhir',
		user = '$user'
		where id='$id'";
						// echo $query2; die();	
		$update=mysqli_query($conn1,$query2);

		$query3 = "INSERT INTO db_ppi.tb_log_covid (
		nomr,
		nopen,
		nama,
		tgl_lahir,
		ruangan,
		dxmedis,
		
		demam, 
		batuk, 
		pilek, 
		tenggorokan, 
		nafas, 
		kepala, 
		lemah, 
		otot, 
		mual, 
		nyeri, 
		diare, 
		pneumonia, 
		ards, 
		surat_swab, 
		perjalanan, 
		perjalanan_transmisi, 
		transmisi_lokal, 
		kontak_suspek, 
		kontak_konfirmasi, 
		ispa, 
		suspect, 
		tanggal_suspect,
		probable, 
		tanggal_probable,
		discarded,
		tanggal_discarded, 
		aps, 
		tanggal_rapid1,
		rapid1,
		tanggal_rapid2,
		rapid2,
		tanggal_swab1,
		swab1,	
		tanggal_swab2,
		swab2,	
		tanggal_swab3,
		swab3,	
		konfirmasi,	
		tanggal_konfirmasi, 
		keterangan,
		status_akhir,
		user) 
		VALUES 
		('$nomr',
		'$nopen',
		'$nama',
		'$tgl_lahir',
		'$ruangan',
		'$dxmedis',
		
		'$demam', 
		'$batuk', 
		'$pilek', 
		'$tenggorokan', 
		'$nafas', 
		'$kepala', 
		'$lemah', 
		'$otot', 
		'$mual', 
		'$nyeri', 
		'$diare', 
		'$pneumonia', 
		'$ards', 
		'$surat_swab', 
		'$perjalanan', 
		'$perjalanan_transmisi', 
		'$transmisi_lokal', 
		'$kontak_suspek', 
		'$kontak_konfirmasi', 
		'$ispa', 
		'$suspect', 
		'$tanggal_suspect', 
		'$probable', 
		'$tanggal_probable',
		'$discarded', 
		'$tanggal_discarded',
		'$aps', 
		'$tanggal_rapid1',
		'$rapid1',
		'$tanggal_rapid2',	
		'$rapid2',		
		'$tanggal_swab1',				
		'$swab1',		
		'$tanggal_swab2',		
		'$swab2',			
		'$tanggal_swab3',
		'$swab3',
		'$konfirmasi',
		'$tanggal_konfirmasi', 
		'$keterangan',
		'$status_akhir',
		'$user')";
		// echo $query; die();	
		$insert3=mysqli_multi_query($conn1,$query3);										
		if($update && $insert3){
			echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=listcovid'</script>"; 
		}
	}
	if(isset($_GET['id'])){
		$query1 = "UPDATE db_ppi.tb_covid ti SET ti.`status`=0 WHERE ti.`status` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
		$rs1 = mysqli_query($conn1,$query1);						
		if($rs1)
		{
			echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=listcovid'</script>";   
		}else{
			echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?page=listcovid'</script>"; 
		}	
	}

	?>
</div><!-- /content-panel -->
</div><!-- /col-lg-12 -->
</div><!-- /row -->
</section>
</section>			