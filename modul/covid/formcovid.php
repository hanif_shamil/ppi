<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Covid-19</li>
			</ul><!-- /.breadcrumb -->			
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Covid-19</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpancovid" method="post">	
									<div class="form-group">
										<div class="col-sm-2">
											<input type="text" id="nomr" name="nomr" autocomplete="off" placeholder="[ No.RM ]" class="form-control" onkeyup="autofill()" />
										</div>							
										<div class="col-sm-3">
											<input type="text" id="nama" name="nama" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
											<input type="hidden" id="nopen" name="nopen" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
										</div>
										<div class="col-sm-2">
											<input type="text" id="tgl_lahir" name="tgl_lahir" placeholder="[ TANGGAL LAHIR ]" class="form-control" readonly />
										</div>														
										<div class="col-sm-2">
											<input name="" id="jk" placeholder="[ JENIS KELAMIN ]" class="form-control" readonly> 
										</div>
										<div class="col-sm-3">
											<input type="text" id="ktp" name="" placeholder=" [ NO. KTP ]" class="form-control" autocomplete="off" readonly/>
										</div>
									</div>	
									
									<div class="form-group">
										<div class="col-sm-2">
											<input type="text" id="warganegara" name="" autocomplete="off" placeholder="[ KEWARGANEGARAAN ]" class="form-control" readonly>
										</div>							
										<div class="col-sm-3">
											<input type="text" id="pekerjaan" name="" placeholder="[ PEKERJAAN ]" class="form-control" readonly />
										</div>
										<div class="col-sm-2">
											<input type="text" id="telpon" name="" placeholder="[ NO TELPON AKTIF ]" class="form-control" readonly />
										</div>														
										<div class="col-sm-5">
											<input name="" id="alamat" placeholder="[ ALAMAT ]" class="form-control" readonly> 
											
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-4">
											<input type="text" id="ruangan" name="dxmedis" placeholder=" [ RUANG RAWAT SAAT INI ]" class="form-control" autocomplete="off" readonly/>
											<input type="hidden" name="ruangan" id="id_ruangan" class="form-control">
										</div>
										<div class="col-sm-3">
											
											<div class="input-group date">
												<input type="text" id="tgl_masuk" name="" placeholder=" [ TANGGAL MASUK RUANG RAWAT ]" class="form-control" autocomplete="off" readonly/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>

										</div>
										<!--<div class="col-sm-5">
											<input type="text" id="form-field-1-1" name="dxmedis" placeholder=" [ DIAGNOSA KANKER]" class="form-control" autocomplete="off"/>
										</div>-->
										<div class="col-sm-5">
											<input type="text" id="diagmasuk" name="" placeholder=" [ DIAGNOSA MEDIS ]" class="form-control" autocomplete="off" readonly/>
											<input type="hidden" id="icd" name="dxmedis" class="form-control" autocomplete="off" readonly/>
										</div>
									</div>
									
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">							
									<label class="control-label bolder blue">GEJALA :</label>			
									<div class="form-group">
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Demam &#8805; 38 &#8451;</label>
												<div class="radio">
													<label>
														<input name="demam" value="1" type="radio" class="ace" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="demam" value="2" type="radio" class="ace"  />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Batuk</label>
												<div class="radio">
													<label>
														<input name="batuk" value="1" type="radio" class="ace" required="on" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="batuk" value="2" type="radio" class="ace" required="on" />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Pilek</label>
												<div class="radio">
													<label>
														<input name="pilek" value="1" type="radio" class="ace" required="on" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="pilek" value="2" type="radio" class="ace" required="on" />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Sakit Tenggorokan</label>
												<div class="radio">
													<label>
														<input name="tenggorokan" value="1" type="radio" class="ace" required="on" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="tenggorokan" value="2" type="radio" class="ace" required="on" />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Sesak nafas</label>
												<div class="radio">
													<label>
														<input name="nafas" value="1" type="radio" class="ace"  required="on"/>
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="nafas" value="2" type="radio" class="ace" required="on" />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>	
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Sakit Kepala</label>
												<div class="radio">
													<label>
														<input name="kepala" value="1" type="radio" class="ace" required="on" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="kepala" value="2" type="radio" class="ace" required="on" />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Lemah (Malaise)</label>
												<div class="radio">
													<label>
														<input name="lemah" value="1" type="radio" class="ace" required="on" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="lemah" value="2" type="radio" class="ace" required="on" />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Nyeri Otot</label>
												<div class="radio">
													<label>
														<input name="otot" value="1" type="radio" class="ace" required="on" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="otot" value="2" type="radio" class="ace" required="on" />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Mual atau Muntah</label>
												<div class="radio">
													<label>
														<input name="mual" value="1" type="radio" class="ace" required="on" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="mual" value="2" type="radio" class="ace" required="on" />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Nyeri Abdomen</label>
												<div class="radio">
													<label>
														<input name="nyeri" value="1" type="radio" class="ace" required="on" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="nyeri" value="2" type="radio" class="ace" required="on" />
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Diare</label>
												<div class="radio">
													<label>
														<input name="diare" value="1" type="radio" class="ace" required="on"/>
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="diare" value="2" type="radio" class="ace" required="on"/>
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</div>
										</div>										
									</div>
								</div>
								<div class="form-group">
								</div>
								<div class="form-group">
									<div class="col-sm-7">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
											<label class="control-label bolder blue">DIAGNOSA MASUK :</label>
											<div class="form-group">
												<div class="col-sm-6">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Pneumonia (Klinis atau Radiologi)</label>
														<div class="radio">
															<label>
																<input name="pneumonia" value="1" type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="pneumonia" value="2" type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">ARDS (Acute Respiratory Syndrome)</label>
														<div class="radio">
															<label>
																<input name="ards" value="1" type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="ards" value="2" type="radio" class="ace" required="on" />
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>										
											</div>
										</div>
									</div>
									<div class="col-sm-5">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
											<label class="control-label bolder blue">PASIEN RUJUKAN :</label>
											<div class="form-group">
												<div class="col-sm-12">
													<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
														<label class="control-label bolder blue" style="text-decoration: underline">Hasil SWAB pada surat rujukan POSITIF</label>
														<div class="radio">
															<label>
																<input name="surat_swab" value="1" type="radio" class="ace" required="on" />
																<span class="lbl"> Ya</span>
															</label>
															<label>
																<input name="surat_swab" value="2" type="radio" class="ace" required="on"/>
																<span class="lbl"> Tidak</span>
															</label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
								</div>
								<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
									<label class="control-label bolder blue">FAKTOR RIWAYAT PERJALANAN :</label>
									<div class="table-responsive-md">
										<table class="table table-bordered text-dark-m1">
											<tbody>
												<tr>	
													<td><label class="control-label bolder blue">Dalam 14 hari sebelum sakit, apakah memiliki riwayat perjalanan dari luar negeri</label></td>
													<td><div class="radio">
														<label>
															<input name="perjalanan" value="1" type="radio" class="ace" required="on"/>
															<span class="lbl"> Ya</span>
														</label>
														<label>
															<input name="perjalanan" value="2" type="radio" class="ace" required="on"/>
															<span class="lbl"> Tidak</span>
														</label>
													</div>
												</td>
											</tr>
											<tr>
												<td><label class="control-label bolder blue">Dalam 14 hari sebelum sakit, apakah memiliki riwayat perjalanan dari transmisi lokal</label></td>
												<td><div class="radio">
													<label>
														<input name="perjalanan_transmisi" value="1" type="radio" class="ace" required="on"/>
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="perjalanan_transmisi" value="2" type="radio" class="ace" required="on"/>
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<label class="control-label bolder blue" style="text-align: left;">Dalam 14 hari sebelum sakit, apakah memiliki riwayat tinggal ke daerah transmisi lokal</label></td>
												<td><div class="radio">
													<label>
														<input name="transmisi_lokal" value="1" type="radio" class="ace" required="on" />
														<span class="lbl"> Ya</span>
													</label>
													<label>
														<input name="transmisi_lokal" value="2" type="radio" class="ace" required="on"/>
														<span class="lbl"> Tidak</span>
													</label>
												</div>
											</td>
										</tr>
										<tr>	
											<td><label class="control-label bolder blue" style="text-align: left;">Dalam 14 hari sebelum sakit, apakah memiliki kontak dengan kasus suspek/probable COVID-19</label></td>
											<td><div class="radio">
												<label>
													<input name="kontak_suspek" value="1" type="radio" class="ace" required="on"/>
													<span class="lbl"> Ya</span>
												</label>
												<label>
													<input name="kontak_suspek" value="2" type="radio" class="ace" required="on"/>
													<span class="lbl"> Tidak</span>
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<td><label class="control-label bolder blue">Dalam 14 hari sebelum sakit, apakah memiliki kontak dengan kasus konfirmasi dan probable COVID-19</label></td>
										<td><div class="radio">
											<label>
												<input name="kontak_konfirmasi" value="1" type="radio" class="ace" required="on"/>
												<span class="lbl"> Ya</span>
											</label>
											<label>
												<input name="kontak_konfirmasi" value="2" type="radio" class="ace" required="on"/>
												<span class="lbl"> Tidak</span>
											</label>
										</div>
									</td>
								</tr>
								<tr>
									<td><label class="control-label bolder blue" style="text-align: left;">Apakah pasien termasuk dalam cluster ISPA berat (demam dan pneumonia membutuhkan perawatan RS) yang tidak diketahui penyebabnya</label></td>
									<td><div class="radio">
										<label>
											<input name="ispa" value="1" type="radio" class="ace" required="on"/>
											<span class="lbl"> Ya</span>
										</label>
										<label>
											<input name="ispa" value="2" type="radio" class="ace" required="on"/>
											<span class="lbl"> Tidak</span>
										</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="form-group">
			</div>

			<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
				<label class="control-label bolder blue">STATUS PASIEN :</label>
				<div class="form-group">
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Suspect</label>
							<div class="radio">
								<label>
									<input name="suspect" value="1" type="radio" class="ace" required="on"/>
									<span class="lbl"> Ya</span>
								</label>
								<label>
									<input name="suspect" value="2" type="radio" class="ace" required="on"/>
									<span class="lbl"> Tidak</span>
								</label>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Suspect</label>
							<div class="input-group">
								<input class="form-control" id="datetimepicker" autocomplate="off" placeholder="[ Tanggal Rapid 1 ]" name="tanggal_suspect" type="text"/>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>	
						</div>
					</div>	
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Probable</label>
							<div class="radio">
								<label>
									<input name="probable" value="1" type="radio" class="ace" required="on"/>
									<span class="lbl"> Ya</span>
								</label>
								<label>
									<input name="probable" value="2" type="radio" class="ace" required="on"/>
									<span class="lbl"> Tidak</span>
								</label>
							</div>
						</div>
					</div>	
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Probable</label>
							<div class="input-group">
								<input class="form-control" id="datetimepicker1" autocomplate="off" placeholder="[ Tanggal Rapid 1 ]" name="tanggal_probable" type="text"/>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>
						</div>
					</div>										
				</div>
				<div class="form-group">
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Konfirmasi</label>
							<div class="radio">
								<label>
									<input name="konfirmasi" value="1" type="radio" class="ace" required="on"/>
									<span class="lbl"> Ya</span>
								</label>
								<label>
									<input name="konfirmasi" value="2" type="radio" class="ace" required="on"/>
									<span class="lbl"> Tidak</span>
								</label>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Tanggal konfirmasi</label>
							<div class="input-group">
								<input class="form-control" id="datetimepicker11" autocomplate="off" placeholder="[ Tanggal Rapid 1 ]" name="tanggal_konfirmasi" type="text"/>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>	
						</div>
					</div>	
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Discarded</label>
							<div class="radio">
								<label>
									<input name="discarded" value="1" type="radio" class="ace" required="on" />
									<span class="lbl"> Ya</span>
								</label>
								<label>
									<input name="discarded" value="2" type="radio" class="ace" required="on"/>
									<span class="lbl"> Tidak</span>
								</label>
							</div>
						</div>
					</div>	
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Discarded</label>
							<div class="input-group">
								<input class="form-control" id="datetimepicker12" autocomplate="off" placeholder="[ Tanggal Discarded ]" name="tanggal_discarded" type="text"/>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>
						</div>
					</div>										
				</div>
			</div>
			<div class="form-group">
			</div>
			<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
				<label class="control-label bolder blue">PEMERIKSAAN :</label>
				<div class="form-group">
					<div class="col-sm-3">	
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Rapid test 1</label>	
							<div class="input-group">
								<input class="form-control" id="datetimepicker13" autocomplate="off" placeholder="[ Tanggal Rapid 1 ]" name="tanggal_rapid1" type="text"/>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>	
						</div>
					</div>	
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Hasil Rapid Test 1</label>
							<div class="radio">
								<label>
									<input name="rapid1" value="1" type="radio" class="ace" />
									<span class="lbl"> Positif</span>
								</label>
								<label>
									<input name="rapid1" value="2" type="radio" class="ace" />
									<span class="lbl"> Negatif</span>
								</label>
							</div>
						</div>
					</div>		
					<div class="col-sm-3">	
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Rapid test 2</label>	
							<div class="input-group">
								<input class="form-control" id="datetimepicker14" placeholder="[ Tanggal Rapid 2 ]" name="tanggal_rapid2" type="text"/>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>	
						</div>
					</div>	
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Hasil Rapid Test 2</label>
							<div class="radio">
								<label>
									<input name="rapid2" value="1" type="radio" class="ace" />
									<span class="lbl"> Positif</span>
								</label>
								<label>
									<input name="rapid2" value="2" type="radio" class="ace" />
									<span class="lbl"> Negatif</span>
								</label>
							</div>
						</div>
					</div>												
				</div>
				<div class="form-group">
					<div class="col-sm-3">	
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Swab 1</label>	
							<div class="input-group">
								<input class="form-control" id="datet1" placeholder="[ Tanggal Swab 1 ]" name="tanggal_swab1" type="text"/>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>	
						</div>											
					</div>	
					<div class="col-sm-3">											
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Hasil Swab 1</label>
							<div class="radio">
								<label>
									<input name="swab1" value="1" type="radio" class="ace" />
									<span class="lbl"> Positif</span>
								</label>
								<label>
									<input name="swab1" value="2" type="radio" class="ace" />
									<span class="lbl"> Negatif</span>
								</label>
							</div>	
						</div>		
					</div>		
					<div class="col-sm-3">	
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Swab 2</label>	
							<div class="input-group">
								<input class="form-control" id="datet2" placeholder="[ Tanggal Swab 2 ]" name="tanggal_swab2" type="text"/>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>	
						</div>											
					</div>	
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Hasil Swab 2</label>
							<div class="radio">
								<label>
									<input name="swab2" value="1" type="radio" class="ace" />
									<span class="lbl"> Positif</span>
								</label>
								<label>
									<input name="swab2" value="2" type="radio" class="ace" />
									<span class="lbl"> Negatif</span>
								</label>
							</div>											
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-3">	
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Swab 3</label>	
							<div class="input-group">
								<input class="form-control" id="datet3" placeholder="[ Tanggal Swab 3 ]" name="tanggal_swab3" type="text"/>
								<span class="input-group-addon">
									<i class="fa fa-calendar bigger-110"></i>
								</span>
							</div>		
						</div>	
					</div>
					<div class="col-sm-3">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
							<label class="control-label bolder blue" style="text-decoration: underline">Hasil Swab 3</label>
							<div class="radio">
								<label>
									<input name="swab3" value="1" type="radio" class="ace" />
									<span class="lbl"> Positif</span>
								</label>
								<label>
									<input name="swab3" value="2" type="radio" class="ace" />
									<span class="lbl"> Negatif</span>
								</label>
							</div>
						</div>
					</div>
						<!-- <div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">PDP</label>
								<div class="radio">
									<label>
										<input name="pdp" value="1" type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="pdp" value="2" type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">ODP</label>
								<div class="radio">
									<label>
										<input name="odp" value="1" type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="odp" value="2" type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">OTG</label>
								<div class="radio">
									<label>
										<input name="otg" value="1" type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="otg" value="2" type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>	
						</div>
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Konfirmasi</label>
								<div class="radio">
									<label>
										<input name="konfirmasi" value="1" type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="konfirmasi" value="2" type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>	
						</div>
						<div class="col-sm-3">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Sembuh</label>
								<div class="radio">
									<label>
										<input name="sembuh" value="1" type="radio" class="ace" />
										<span class="lbl"> Ya</span>
									</label>
									<label>
										<input name="sembuh" value="2" type="radio" class="ace" />
										<span class="lbl"> Tidak</span>
									</label>
								</div>
							</div>	
						</div>-->
						<div class="col-sm-6">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">Keterangan</label>
								<input type="text" id="form-field-1-1" name="keterangan" placeholder="[ KETERANGAN ]" class="form-control" />
							</div>	
						</div>										
					</div>
					<div class="form-group">
						<div class="col-sm-7">
							<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
								<label class="control-label bolder blue" style="text-decoration: underline">STATUS AKHIR</label>
								<div class="radio">
									<label>
										<input name="status_akhir" value="1" type="radio" class="ace" required="on"/>
										<span class="lbl"> Meninggal</span>
									</label>
									<label>
										<input name="status_akhir" value="2" type="radio" class="ace" required="on"/>
										<span class="lbl"> Dirujuk</span>
									</label>
									<label>
										<input name="status_akhir" value="3" type="radio" class="ace" required="on"/>
										<span class="lbl"> Isolsasi mandiri</span>
									</label>
									<label>
										<input name="status_akhir" value="4" type="radio" class="ace" required="on"/>
										<span class="lbl"> APS</span>
									</label>
									<label>
										<input name="status_akhir" value="5" type="radio" class="ace" required="on"/>
										<span class="lbl"> APD/Sembuh</span>
									</label>
									<label>
										<input name="status_akhir" value="6" type="radio" class="ace" required="on"/>
										<span class="lbl"> Dirawat</span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr />
				<div class="form-group">							
					<div class="col-md-offset-3 col-md-9">
						<button class="btn btn-info" name="btnsimpan" type="submit">
							<i class="ace-icon fa fa-check bigger-110"></i>
							Submit
						</button>
						&nbsp; &nbsp; &nbsp;
						<button class="btn" type="reset">
							<i class="ace-icon fa fa-undo bigger-110"></i>
							Reset
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</div><!-- /.page-content -->	
