<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];						
						$SUPERVISOR 	= $_POST['SUPERVISOR'];
						$SATU_A  		= $_POST['SATU_A'];
						$SATU_B  		= $_POST['SATU_B'];
						$SATU_C  		= $_POST['SATU_C'];
						$SATU_D  		= $_POST['SATU_D'];
						$SATU_E  		= $_POST['SATU_E'];
						$SATU_F  		= $_POST['SATU_F'];
						$DUA_A  		= $_POST['DUA_A'];
						$DUA_B  		= $_POST['DUA_B'];
						$DUA_C  		= $_POST['DUA_C'];
						$DUA_D  		= $_POST['DUA_D'];
						$DUA_E  		= $_POST['DUA_E'];
						$DUA_F  		= $_POST['DUA_F'];
						$DUA_G  		= $_POST['DUA_G'];
						$DUA_H  		= $_POST['DUA_H'];
						$TIGA_A  		= $_POST['TIGA_A'];
						$TIGA_B  		= $_POST['TIGA_B'];
						$EMPAT_A  		= $_POST['EMPAT_A'];
						$EMPAT_B  		= $_POST['EMPAT_B'];
						$EMPAT_C  		= $_POST['EMPAT_C'];
						$EMPAT_D  		= $_POST['EMPAT_D'];
						$EMPAT_E  		= $_POST['EMPAT_E'];
						$LIMA_A  		= $_POST['LIMA_A'];
						$LIMA_B  		= $_POST['LIMA_B'];
						$LIMA_C  		= $_POST['LIMA_C'];
						$LIMA_D  		= $_POST['LIMA_D'];
						$LIMA_E  		= $_POST['LIMA_E'];
						$LIMA_F  		= $_POST['LIMA_F'];
						$LIMA_G  		= $_POST['LIMA_G'];
						$LIMA_H  		= $_POST['LIMA_H'];
						$LIMA_I  		= $_POST['LIMA_I'];
						$LIMA_J  		= $_POST['LIMA_J'];
						$LIMA_K  		= $_POST['LIMA_K'];
						$ENAM_A  		= $_POST['ENAM_A'];
						$ENAM_B  		= $_POST['ENAM_B'];
						$ENAM_C  		= $_POST['ENAM_C'];
						$ENAM_D  		= $_POST['ENAM_D'];
						$ENAM_E  		= $_POST['ENAM_E'];
						$TUJUH_A  		= $_POST['TUJUH_A'];
						$TUJUH_B  		= $_POST['TUJUH_B'];
						$TUJUH_C  		= $_POST['TUJUH_C'];
						$TUJUH_D  		= $_POST['TUJUH_D'];
						$TUJUH_E  		= $_POST['TUJUH_E'];
						$TUJUH_F  		= $_POST['TUJUH_F'];
						$TUJUH_G  		= $_POST['TUJUH_G'];
						$TUJUH_H  		= $_POST['TUJUH_H'];
						$TUJUH_I  		= $_POST['TUJUH_I'];
						$DELAPAN_A  		= $_POST['DELAPAN_A'];
						$DELAPAN_B  		= $_POST['DELAPAN_B'];
						$DELAPAN_C  		= $_POST['DELAPAN_C'];
						$DELAPAN_D  		= $_POST['DELAPAN_D'];
						$DELAPAN_E  		= $_POST['DELAPAN_E'];
						$DELAPAN_F  		= $_POST['DELAPAN_F'];
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_appi (
						ID_ISI,
						TANGGAL,
						RUANGAN,
						SUPERVISOR,
						SATU_A,  
						SATU_B,  
						SATU_C,  
						SATU_D,  
						SATU_E,  
						SATU_F,  
						DUA_A,  
						DUA_B,  
						DUA_C,  
						DUA_D,  
						DUA_E,  
						DUA_F,  
						DUA_G,  
						DUA_H,  
						TIGA_A,  
						TIGA_B,  
						EMPAT_A,  
						EMPAT_B,  
						EMPAT_C,  
						EMPAT_D,  
						EMPAT_E,  
						LIMA_A,  
						LIMA_B,  
						LIMA_C,  
						LIMA_D,  
						LIMA_E,  
						LIMA_F,  
						LIMA_G,  
						LIMA_H,  
						LIMA_I,  
						LIMA_J,  
						LIMA_K,  
						ENAM_A,  
						ENAM_B,  
						ENAM_C,  
						ENAM_D,  
						ENAM_E,  
						TUJUH_A,  
						TUJUH_B,  
						TUJUH_C,  
						TUJUH_D,  
						TUJUH_E,  
						TUJUH_F,  
						TUJUH_G,  
						TUJUH_H,  
						TUJUH_I,  
						DELAPAN_A,  
						DELAPAN_B,  
						DELAPAN_C,  
						DELAPAN_D,  
						DELAPAN_E,  
						DELAPAN_F, 
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$RUANGAN',
						'$SUPERVISOR',
						'$SATU_A',
						'$SATU_B',
						'$SATU_C',
						'$SATU_D',
						'$SATU_E',
						'$SATU_F',
						'$DUA_A',
						'$DUA_B',
						'$DUA_C',
						'$DUA_D',
						'$DUA_E',
						'$DUA_F',
						'$DUA_G',
						'$DUA_H',
						'$TIGA_A',
						'$TIGA_B',
						'$EMPAT_A',
						'$EMPAT_B',
						'$EMPAT_C',
						'$EMPAT_D',
						'$EMPAT_E',
						'$LIMA_A',
						'$LIMA_B',
						'$LIMA_C',
						'$LIMA_D',
						'$LIMA_E',
						'$LIMA_F',
						'$LIMA_G',
						'$LIMA_H',
						'$LIMA_I',
						'$LIMA_J',
						'$LIMA_K',
						'$ENAM_A',
						'$ENAM_B',
						'$ENAM_C',
						'$ENAM_D',
						'$ENAM_E',
						'$TUJUH_A',
						'$TUJUH_B',
						'$TUJUH_C',
						'$TUJUH_D',
						'$TUJUH_E',
						'$TUJUH_F',
						'$TUJUH_G',
						'$TUJUH_H',
						'$TUJUH_I',
						'$DELAPAN_A',
						'$DELAPAN_B',
						'$DELAPAN_C',
						'$DELAPAN_D',
						'$DELAPAN_E',
						'$DELAPAN_F',	
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_appi'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];						
						$SUPERVISOR 	= $_POST['SUPERVISOR'];
						$SATU_A  		= $_POST['SATU_A'];
						$SATU_B  		= $_POST['SATU_B'];
						$SATU_C  		= $_POST['SATU_C'];
						$SATU_D  		= $_POST['SATU_D'];
						$SATU_E  		= $_POST['SATU_E'];
						$SATU_F  		= $_POST['SATU_F'];
						$DUA_A  		= $_POST['DUA_A'];
						$DUA_B  		= $_POST['DUA_B'];
						$DUA_C  		= $_POST['DUA_C'];
						$DUA_D  		= $_POST['DUA_D'];
						$DUA_E  		= $_POST['DUA_E'];
						$DUA_F  		= $_POST['DUA_F'];
						$DUA_G  		= $_POST['DUA_G'];
						$DUA_H  		= $_POST['DUA_H'];
						$TIGA_A  		= $_POST['TIGA_A'];
						$TIGA_B  		= $_POST['TIGA_B'];
						$EMPAT_A  		= $_POST['EMPAT_A'];
						$EMPAT_B  		= $_POST['EMPAT_B'];
						$EMPAT_C  		= $_POST['EMPAT_C'];
						$EMPAT_D  		= $_POST['EMPAT_D'];
						$EMPAT_E  		= $_POST['EMPAT_E'];
						$LIMA_A  		= $_POST['LIMA_A'];
						$LIMA_B  		= $_POST['LIMA_B'];
						$LIMA_C  		= $_POST['LIMA_C'];
						$LIMA_D  		= $_POST['LIMA_D'];
						$LIMA_E  		= $_POST['LIMA_E'];
						$LIMA_F  		= $_POST['LIMA_F'];
						$LIMA_G  		= $_POST['LIMA_G'];
						$LIMA_H  		= $_POST['LIMA_H'];
						$LIMA_I  		= $_POST['LIMA_I'];
						$LIMA_J  		= $_POST['LIMA_J'];
						$LIMA_K  		= $_POST['LIMA_K'];
						$ENAM_A  		= $_POST['ENAM_A'];
						$ENAM_B  		= $_POST['ENAM_B'];
						$ENAM_C  		= $_POST['ENAM_C'];
						$ENAM_D  		= $_POST['ENAM_D'];
						$ENAM_E  		= $_POST['ENAM_E'];
						$TUJUH_A  		= $_POST['TUJUH_A'];
						$TUJUH_B  		= $_POST['TUJUH_B'];
						$TUJUH_C  		= $_POST['TUJUH_C'];
						$TUJUH_D  		= $_POST['TUJUH_D'];
						$TUJUH_E  		= $_POST['TUJUH_E'];
						$TUJUH_F  		= $_POST['TUJUH_F'];
						$TUJUH_G  		= $_POST['TUJUH_G'];
						$TUJUH_H  		= $_POST['TUJUH_H'];
						$TUJUH_I  		= $_POST['TUJUH_I'];
						$DELAPAN_A  		= $_POST['DELAPAN_A'];
						$DELAPAN_B  		= $_POST['DELAPAN_B'];
						$DELAPAN_C  		= $_POST['DELAPAN_C'];
						$DELAPAN_D  		= $_POST['DELAPAN_D'];
						$DELAPAN_E  		= $_POST['DELAPAN_E'];
						$DELAPAN_F  		= $_POST['DELAPAN_F'];
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_appi SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						SUPERVISOR 	= '$SUPERVISOR',
						SATU_A  		= '$SATU_A',
						SATU_B  		= '$SATU_B',
						SATU_C  		= '$SATU_C',
						SATU_D  		= '$SATU_D',
						SATU_E  		= '$SATU_E',
						SATU_F  		= '$SATU_F',
						DUA_A  		= '$DUA_A',
						DUA_B  		= '$DUA_B',
						DUA_C  		= '$DUA_C',
						DUA_D  		= '$DUA_D',
						DUA_E  		= '$DUA_E',
						DUA_F  		= '$DUA_F',
						DUA_G  		= '$DUA_G',
						DUA_H  		= '$DUA_H',
						TIGA_A  		= '$TIGA_A',
						TIGA_B  		= '$TIGA_B',
						EMPAT_A  		= '$EMPAT_A',
						EMPAT_B  		= '$EMPAT_B',
						EMPAT_C  		= '$EMPAT_C',
						EMPAT_D  		= '$EMPAT_D',
						EMPAT_E  		= '$EMPAT_E',
						LIMA_A  		= '$LIMA_A',
						LIMA_B  		= '$LIMA_B',
						LIMA_C  		= '$LIMA_C',
						LIMA_D  		= '$LIMA_D',
						LIMA_E  		= '$LIMA_E',
						LIMA_F  		= '$LIMA_F',
						LIMA_G  		= '$LIMA_G',
						LIMA_H  		= '$LIMA_H',
						LIMA_I  		= '$LIMA_I',
						LIMA_J  		= '$LIMA_J',
						LIMA_K  		= '$LIMA_K',
						ENAM_A  		= '$ENAM_A',
						ENAM_B  		= '$ENAM_B',
						ENAM_C  		= '$ENAM_C',
						ENAM_D  		= '$ENAM_D',
						ENAM_E  		= '$ENAM_E',
						TUJUH_A  		= '$TUJUH_A',
						TUJUH_B  		= '$TUJUH_B',
						TUJUH_C  		= '$TUJUH_C',
						TUJUH_D  		= '$TUJUH_D',
						TUJUH_E  		= '$TUJUH_E',
						TUJUH_F  		= '$TUJUH_F',
						TUJUH_G  		= '$TUJUH_G',
						TUJUH_H  		= '$TUJUH_H',
						TUJUH_I  		= '$TUJUH_I',
						DELAPAN_A  		= '$DELAPAN_A',
						DELAPAN_B  		= '$DELAPAN_B',
						DELAPAN_C  		= '$DELAPAN_C',
						DELAPAN_D  		= '$DELAPAN_D',
						DELAPAN_E  		= '$DELAPAN_E',
						DELAPAN_F  		= '$DELAPAN_F',
						TOTAL			= '$TOTAL',
						DIUBAH_OLEH		= '$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_appi'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_appi ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_appi'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			