<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$SURVEYOR 		= $_POST['SURVEYOR'];
						$ANALISA 		=	$_POST['ANALISA'];  
						$TINDAKLANJUT 	=	$_POST['TINDAKLANJUT'];  
						$KEPALA 		=	$_POST['KEPALA'];  
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SATU'])){
							$SATU = implode(',',$_POST['SATU']);
						}else{
							$SATU = "";
						}		
						if(isset($_POST['DUA'])){
							$DUA = implode(',',$_POST['DUA']);
						}else{
							$DUA = "";
						}	
						if(isset($_POST['TIGA'])){
							$TIGA = implode(',',$_POST['TIGA']);
						}else{
							$TIGA = "";
						}	
						if(isset($_POST['EMPAT'])){
							$EMPAT = implode(',',$_POST['EMPAT']);
						}else{
							$EMPAT = "";
						}	
						if(isset($_POST['LIMA'])){
							$LIMA = implode(',',$_POST['LIMA']);
						}else{
							$LIMA = "";
						}	
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_hap (
						TANGGAL,
						ANALISA,
						TINDAKLANJUT,
						KEPALA,
						AUDITOR,
						RUANGAN,
						SURVEYOR,
						NOMR,
						NAMA,
						SATU,
						DUA,
						TIGA,
						EMPAT,
						LIMA,
						TOTAL,
						USER) 
						VALUES 
						('$TANGGAL',
						'$ANALISA',  
						'$TINDAKLANJUT', 
						'$KEPALA',
						'$AUDITOR',
						'$RUANGAN',
						'$SURVEYOR',
						'$NOMR',
						'$NAMA',
						'$SATU',
						'$DUA',	
						'$TIGA',		
						'$EMPAT',				
						'$LIMA',	
						'$TOTAL',
						'$USER')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_hap'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$SURVEYOR 		= $_POST['SURVEYOR'];
						$ANALISA 		=	$_POST['ANALISA'];  
						$TINDAKLANJUT 	=	$_POST['TINDAKLANJUT'];  
						$KEPALA 		=	$_POST['KEPALA'];  
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SATU'])){
							$SATU = implode(',',$_POST['SATU']);
						}else{
							$SATU = "";
						}		
						if(isset($_POST['DUA'])){
							$DUA = implode(',',$_POST['DUA']);
						}else{
							$DUA = "";
						}	
						if(isset($_POST['TIGA'])){
							$TIGA = implode(',',$_POST['TIGA']);
						}else{
							$TIGA = "";
						}	
						if(isset($_POST['EMPAT'])){
							$EMPAT = implode(',',$_POST['EMPAT']);
						}else{
							$EMPAT = "";
						}	
						if(isset($_POST['LIMA'])){
							$LIMA = implode(',',$_POST['LIMA']);
						}else{
							$LIMA = "";
						}	
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_hap SET												
						TANGGAL='$TANGGAL',
						ANALISA = '$ANALISA', 
						TINDAKLANJUT = '$TINDAKLANJUT', 
						KEPALA = '$KEPALA',
						AUDITOR='$AUDITOR',
						RUANGAN='$RUANGAN',
						SURVEYOR='$SURVEYOR',
						NOMR='$NOMR',
						NAMA='$NAMA',
						SATU='$SATU',
						DUA='$DUA',
						TIGA='$TIGA',
						EMPAT='$EMPAT',
						LIMA='$LIMA',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID='$ID'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_hap'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_hap ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_hap'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_hap'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			