<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#BB:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<?php
$id=($_GET['id']);
$query="select ti.ID, ti.TGL_INPUT, ti.TANGGAL, ti.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, ti.NOMR, ti.NAMA
, ti.SURVEYOR, ti.SATU, ti.DUA, ti.TIGA, ti.EMPAT, ti.LIMA, ti.TOTAL, ti.`STATUS`
from tb_hap ti
left join master.ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5												
where ti.ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$SATU = explode(",", $dt['SATU']);$DUA = explode(",", $dt['DUA']);$TIGA = explode(",", $dt['TIGA']);
$EMPAT = explode(",", $dt['EMPAT']);$LIMA = explode(",", $dt['LIMA']);
  {
	  ?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Audit Bundles IADP</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Audit Bundles IADP</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_hap" method="post">
								<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $dt['ID']; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TANGGAL']; ?>" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" value="<?php echo $dt['RUANGAN']; ?>" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ID_RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
												<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-1">
											<input type="text" id="nomr" name="NOMR" value="<?php echo $dt['NOMR']; ?>" placeholder="[ MR ]" class="form-control" onkeyup="autofill()" />												
										</div>	
										<div class="col-sm-3">									
											<input type="text" id="nama" name="NAMA" value="<?php echo $dt['NAMA']; ?>" placeholder="[ NAMA ]" class="form-control" />												
										</div>																			
										<div class="col-sm-3">											
											<input type="text" name="SURVEYOR" value="<?php echo $dt['SURVEYOR']; ?>" placeholder="[ SURVEYOR ]" class="form-control" />												
										</div>
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> BUNDLES IDAP</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Melakukan kebersihan tangan</td>
														<td align="center"><label>
															<input type="radio" name="SATU[]" <?php if(in_array("1", $SATU)){ echo " checked=\"checked\""; } ?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
															</label></td>
														<td align="center">
															<label>
															<input name="SATU[]" type="radio" <?php if(in_array("2", $SATU)){ echo " checked=\"checked\""; } ?> value="2" id="BB" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Memberikan posisi tempat tidur 30-45° (bila tidak ada kontra indikasi, seperti fr.servikal)</td>
														<td align="center"><label>
														<input type="radio" name="DUA[]" <?php if(in_array("1", $DUA)){ echo " checked=\"checked\""; } ?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">													
													<span class="lbl"></span>
												</label></td>
														<td align="center"><label>
													<input name="DUA[]" type="radio" <?php if(in_array("2", $DUA)){ echo " checked=\"checked\""; } ?> value="2" id="BB" class="ace input-lg" onClick="tugas1()" />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Melakukan mobilisasi miring kiri dan kanan setiap 2-4 jam, dan lakukan masase /chest terapi (bila tidak ada kontra indikasi)</td>
														<td align="center">
															<label>
															<input type="radio" name="TIGA[]" <?php if(in_array("1", $TIGA)){ echo " checked=\"checked\""; } ?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="TIGA[]" type="radio" <?php if(in_array("2", $TIGA)){ echo " checked=\"checked\""; } ?> value="2" id="BB" onClick="tugas1()" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Melakukan oral hygiene dengan chlorhexidin 0.2% tiap 4-6 jam, dan sikat gigi tiap 12 jam</td>
														<td align="center">
															<label>
															<input name="EMPAT[]" type="radio" value="1" <?php if(in_array("1", $EMPAT)){ echo " checked=\"checked\""; } ?> id="AA" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="EMPAT[]" type="radio" value="2" <?php if(in_array("2", $EMPAT)){ echo " checked=\"checked\""; } ?> id="BB" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Memberi makan per NGT per drip, hindari pemberian makan dengan mendorong kateter tip.</td>
														<td align="center">
															<label>
															<input name="LIMA[]" type="radio" value="1" <?php if(in_array("1", $LIMA)){ echo " checked=\"checked\""; } ?> id="AA" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="LIMA[]" type="radio" value="2" <?php if(in_array("2", $LIMA)){ echo " checked=\"checked\""; } ?> id="BB" onClick="tugas1()" class="ace input-lg"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
														<div class="input-group">
															<input name="TOTAL" value="<?php echo $dt['TOTAL']; ?>" type="text" id="total" style="width:60px"></br>
															<span class="input-group-addon">
																%
															</span>
														</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													
									
									
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnEdit" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->
<?php
  }?>