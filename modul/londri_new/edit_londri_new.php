<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>

<?php
$id=($_GET['id']);
$query="select * from db_ppi.tb_londri_new where ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">FORMULIR AUDIT LAUNDRY</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">FORMULIR AUDIT LAUNDRY</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_londri_new" method="post">	
								<input type="hidden" value="<?php echo $dt['ID']; ?>"  name="ID" >									
									<div class="form-group">
										<div class="col-sm-1">	
											<label class="control-label bolder blue">TANGGAL</label>
										</div>								
										<div class="col-sm-3">									
											<div class="input-group">
												<input class="form-control" value="<?php echo $dt['TANGGAL']; ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>												
										</div>
									</div>

									<div class="form-group">																				
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> KEGIATAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="5%" style="text-align:center">NA</th>
															<th width="25%" style="text-align:center">KETERANGAN</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>A.</td>
															<td>FASILITAS</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Tersedia Fasilitas Cuci Tangan : westafel,  sabun antiseptik, paper towel  dan handrub</td>
															<td align="center">
																<label><input type="radio" <?php if($dt['A1']=='1') echo " checked "?> name="A1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" <?php if($dt['A1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" <?php if($dt['A1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A1']; ?>" name="KET_A1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Tersedia APD (topi, masker, google, apron, sepatu boot (sepatu tertutup)
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A2']; ?>" name="KET_A2" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Tersedia eye washer
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" <?php if($dt['A3']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" <?php if($dt['A3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" <?php if($dt['A3']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A3']; ?>" name="KET_A3" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>B.</td>
															<td>ALUR LINEN KOTOR DAN LINEN BERSIH
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Linen infeksius dimasukkan dalam plastik kuning 
															</td>
															<td align="center"><label>
																<input type="radio" name="B1" id="AA" <?php if($dt['B1']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="B1" type="radio" <?php if($dt['B1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="B1" type="radio" <?php if($dt['B1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B1']; ?>" name="KET_B1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Linen non  infeksius dimasukan dalam plastik hitam
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B2" <?php if($dt['B2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B2" <?php if($dt['B2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B2" <?php if($dt['B2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B2']; ?>" name="KET_B2" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>3
															</td>
															<td>Linen non infeksius di masukkan ke mesin cuci non infeksius
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B3" <?php if($dt['B3']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B3" <?php if($dt['B3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B3" <?php if($dt['B3']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B3']; ?>" name="KET_B3" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>4
															</td>
															<td>Linen infeksius langsung di masukan ke mesin cuci infeksius
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B4" <?php if($dt['B4']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B4" <?php if($dt['B4']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B4" <?php if($dt['B4']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B4']; ?>" name="KET_B4" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>5
															</td>
															<td>Pengangkutan linen kotor dan bersih menggunakan troli tertutup 
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B5" <?php if($dt['B5']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B5" <?php if($dt['B5']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B5" <?php if($dt['B5']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B5']; ?>" name="KET_B5" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>C.</td>
															<td>KEBERSIHAN LINGKUNGAN </td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1
															</td>
															<td>Area cuci bebas dari debu, tidak licin dan bersih
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1" <?php if($dt['C1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																	</label
																</td>
																<td align="center">
																	<label>
																		<input name="C1" type="radio" <?php if($dt['C1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="C1" type="radio" <?php if($dt['C3']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1']; ?>" name="KET_C1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Mesin washer tidak ada debu dan kotoran </td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2" <?php if($dt['C2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>

																<td align="center">
																	<label>
																		<input type="radio" name="C2" <?php if($dt['C2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2" <?php if($dt['C2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C2']; ?>" name="KET_C2" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Mesin pengering tidak ada debu dan kotoran 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C3" <?php if($dt['C3']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C3" <?php if($dt['C3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C3" <?php if($dt['C3']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C3']; ?>" name="KET_C3" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Mesin setrika (Iron Machine) tidak berdebu dan kotor
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C4" <?php if($dt['C4']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C4" <?php if($dt['C4']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C4" <?php if($dt['C4']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C4']; ?>" name="KET_C4" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>5</td>
																<td>Meja pelipatan bebas debu dan kotoran 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C5" <?php if($dt['C5']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C5" <?php if($dt['C5']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C5" <?php if($dt['C5']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C5']; ?>" name="KET_C5" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>6</td>
																<td>Lemari bersih dan tertata rapi
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C6" <?php if($dt['C6']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C6" <?php if($dt['C6']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C6" <?php if($dt['C6']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C6']; ?>" name="KET_C6" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>7</td>
																<td>Lantai dan dinding semua area bebas debu, kotoran dan jamur 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C7" <?php if($dt['C7']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C7" <?php if($dt['C7']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C7" <?php if($dt['C7']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C7']; ?>" name="KET_C7" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>8</td>
																<td>Troli linen dalam keadaan bersih untuk membawa linen ke ruang rawat
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C8" <?php if($dt['C8']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C8" <?php if($dt['C8']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C8" <?php if($dt['C8']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C8']; ?>" name="KET_C8" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>9</td>
																<td>Jadual pembersihan troli linen 1 minggu sekali oleh petugas loundry
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C9" <?php if($dt['C9']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C9" <?php if($dt['C9']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C9" <?php if($dt['C9']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C9']; ?>" name="KET_C9" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>D.</td>
																<td>KEPATUHAN PETUGAS</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Area kotor : Petugas menggunakan APD penutup kepala, masker, apron, sarung tangan dan safety shoes
																</td>
																<td align="center"><label>
																	<input type="radio" name="D1" <?php if($dt['D1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="D1" type="radio" <?php if($dt['D1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="D1" type="radio" <?php if($dt['D1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D1']; ?>" name="KET_D1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Area bersih : Petugas menggunakan APD masker dan penutup kepala
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" <?php if($dt['D2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" <?php if($dt['D2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" <?php if($dt['D2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D2']; ?>" name="KET_D2" placeholder="" class="form-control" />	
																</td>
															</tr>			
															<tr>
																<td>E.</td>
																<td>PROSES PELIPATAN
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Linen yang sudah bersih pastikan dilipat di meja yang bersih
																</td>
																<td align="center"><label>
																	<input type="radio" name="E1" <?php if($dt['E1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="E1" type="radio" <?php if($dt['E1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="E1" type="radio" <?php if($dt['E1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_E1']; ?>" name="KET_E1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Linen yang sudah bersih tidak diletakkan di lantai
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E2" <?php if($dt['E2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E2" <?php if($dt['E2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E2" <?php if($dt['E2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_E2']; ?>" name="KET_E2" placeholder="" class="form-control" />	
																</td>
															</tr>	
															<tr>
																<td>3</td>
																<td>Pemilahan linen yang rusak atau bernoda
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E3" <?php if($dt['E3']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E3" <?php if($dt['E3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E3" <?php if($dt['E3']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_E3']; ?>" name="KET_E3" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>F.</td>
																<td>DISTRIBUSI LINEN
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Ada pengaturan sistem FIFO (First In First Out) 
																</td>
																<td align="center"><label>
																	<input type="radio" name="F1" <?php if($dt['F1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="F1" <?php if($dt['F1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="F1" <?php if($dt['F1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F1']; ?>" name="KET_F1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Linen bersih di packing dengan plastik
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F2']=='1') echo " checked "?> name="F2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F2']=='2') echo " checked "?> name="F2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F2']=='3') echo " checked "?> name="F2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F2']; ?>" name="KET_F2" placeholder="" class="form-control" />	
																</td>
															</tr>	
															<tr>
																<td>3</td>
																<td>Pendistribusian linen bersih ke ruang rawat menggunakan troly yang covernya berwarna biru
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F3']=='1') echo " checked "?> name="F3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F3']=='2') echo " checked "?> name="F3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F3']=='3') echo " checked "?> name="F3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F3']; ?>" name="KET_F3" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Pengangkutan linen kotor dari ruang rawat menggunakan troly yang covernya berwarna kuning
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F4']=='1') echo " checked "?> name="F4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F4']=='2') echo " checked "?> name="F4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F4']=='3') echo " checked "?> name="F4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F4']; ?>" name="KET_F4" placeholder="" class="form-control" />	
																</td>
															</tr>																
															<tr>
																<td style="text-align:center" colspan="4">JUMLAH TOTAL</td>
																<td colspan="3" style="text-align:center">
																	<div class="input-group">
																		<input type="text" id="total" value="<?php echo $dt['NILAI']; ?>" name="NILAI" placeholder="[ NILAI ]" class="form-control" />
																		<span class="input-group-addon">
																			%
																		</span>
																	</div>													
																</td>														
															</tr>													
														</tbody>
													</table>																									
												</div>
											</div>
										</div>																										
										<hr />
										<div class="form-group">										
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
												<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
											</div>				
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
												<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
											</div>
										</div>
										<div class="form-group">			
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">KOORDINATOR LAUNDRY</label>	
												<input type="text" id="nama" value="<?php echo $dt['KOORDINATOR']; ?>" name="KOORDINATOR" placeholder="[ KOORDINATOR CSSD ]" class="form-control" />												
											</div>	
											<div class="col-md-6 col-sm-12">	
												<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
												<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
											</div>								
										</div>																											
										<hr />
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->	
<?php }
?>