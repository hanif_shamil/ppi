<script type="text/javascript">

	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">FORMULIR AUDIT LAUNDRY</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">FORMULIR AUDIT LAUNDRY</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_londri_new" method="post">										
									<div class="form-group">
										<div class="col-sm-1">	
											<label class="control-label bolder blue">TANGGAL</label>
										</div>								
										<div class="col-sm-3">									
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d') ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>												
										</div>
									</div>

									<div class="form-group">																				
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> KEGIATAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="5%" style="text-align:center">NA</th>
															<th width="25%" style="text-align:center">KETERANGAN</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>A.</td>
															<td>FASILITAS</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Tersedia Fasilitas Cuci Tangan : westafel,  sabun antiseptik, paper towel  dan handrub</td>
															<td align="center">
																<label><input type="radio" name="A1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_A1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Tersedia APD (topi, masker, google, apron, sepatu boot (sepatu tertutup)
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_A2" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Tersedia eye washer
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_A3" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>B.</td>
															<td>ALUR LINEN KOTOR DAN LINEN BERSIH
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Linen infeksius dimasukkan dalam plastik kuning 
															</td>
															<td align="center"><label>
																<input type="radio" name="B1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="B1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="B1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_B1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Linen non  infeksius dimasukan dalam plastik hitam
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_B2" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>3
															</td>
															<td>Linen non infeksius di masukkan ke mesin cuci non infeksius
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_B3" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>4
															</td>
															<td>Linen infeksius langsung di masukan ke mesin cuci infeksius
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_B4" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>5
															</td>
															<td>Pengangkutan linen kotor dan bersih menggunakan troli tertutup 
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B5" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B5" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B5" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_B5" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>C.</td>
															<td>KEBERSIHAN LINGKUNGAN </td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1
															</td>
															<td>Area cuci bebas dari debu, tidak licin dan bersih
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="C1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="C1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_C1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Mesin washer tidak ada debu dan kotoran </td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>

																<td align="center">
																	<label>
																		<input type="radio" name="C2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_C2" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Mesin pengering tidak ada debu dan kotoran 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_C3" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Mesin setrika (Iron Machine) tidak berdebu dan kotor
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_C4" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>5</td>
																<td>Meja pelipatan bebas debu dan kotoran 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C5" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C5" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C5" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_C5" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>6</td>
																<td>Lemari bersih dan tertata rapi
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C6" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C6" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C6" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_C6" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>7</td>
																<td>Lantai dan dinding semua area bebas debu, kotoran dan jamur 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C7" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C7" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C7" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_C7" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>8</td>
																<td>Troli linen dalam keadaan bersih untuk membawa linen ke ruang rawat
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C8" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C8" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C8" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_C8" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>9</td>
																<td>Jadual pembersihan troli linen 1 minggu sekali oleh petugas loundry
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C9" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C9" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C9" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_C9" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>D.</td>
																<td>KEPATUHAN PETUGAS</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Area kotor : Petugas menggunakan APD penutup kepala, masker, apron, sarung tangan dan safety shoes
																</td>
																<td align="center"><label>
																	<input type="radio" name="D1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="D1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="D1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_D1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Area bersih : Petugas menggunakan APD masker dan penutup kepala
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_D2" placeholder="" class="form-control" />	
																</td>
															</tr>			
															<tr>
																<td>E.</td>
																<td>PROSES PELIPATAN
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Linen yang sudah bersih pastikan dilipat di meja yang bersih
																</td>
																<td align="center"><label>
																	<input type="radio" name="E1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="E1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="E1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_E1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Linen yang sudah bersih tidak diletakkan di lantai
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_E2" placeholder="" class="form-control" />	
																</td>
															</tr>	
															<tr>
																<td>3</td>
																<td>Pemilahan linen yang rusak atau bernoda
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="E3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_E3" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>F.</td>
																<td>DISTRIBUSI LINEN
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Ada pengaturan sistem FIFO (First In First Out) 
																</td>
																<td align="center"><label>
																	<input type="radio" name="F1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="F1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="F1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_F1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Linen bersih di packing dengan plastik
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="F2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="F2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="F2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_F2" placeholder="" class="form-control" />	
																</td>
															</tr>	
															<tr>
																<td>3</td>
																<td>Pendistribusian linen bersih ke ruang rawat menggunakan troly yang covernya berwarna biru
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="F3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="F3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="F3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_F3" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Pengangkutan linen kotor dari ruang rawat menggunakan troly yang covernya berwarna kuning
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="F4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="F4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="F4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" name="KET_F4" placeholder="" class="form-control" />	
																</td>
															</tr>																
															<tr>
																<td style="text-align:center" colspan="4">JUMLAH TOTAL</td>
																<td colspan="3" style="text-align:center">
																	<div class="input-group">
																		<input type="text" id="total" name="NILAI" placeholder="[ NILAI ]" class="form-control" />
																		<span class="input-group-addon">
																			%
																		</span>
																	</div>													
																</td>														
															</tr>													
														</tbody>
													</table>																									
												</div>
											</div>
										</div>																										
										<hr />
										<div class="form-group">										
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
												<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
											</div>				
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
												<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
											</div>
										</div>
										<div class="form-group">			
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">KOORDINATOR LAUNDRY</label>	
												<input type="text" id="nama" name="KOORDINATOR" placeholder="[ KOORDINATOR LAUNDRY ]" class="form-control" />												
											</div>	
											<div class="col-md-6 col-sm-12">	
												<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
												<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
											</div>								
										</div>																											
										<hr />
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnsimpan" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->	