<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					if(isset($_POST['btnsimpan'])){
						$TANGGAL 			= $_POST['TANGGAL'];									
						$A1 				= $_POST['A1'];
						$KET_A1				= $_POST['KET_A1']; 
						$A2					= $_POST['A2'];
						$KET_A2				= $_POST['KET_A2']; 
						$A3					= $_POST['A3'];
						$KET_A3 			= $_POST['KET_A3'];	
						
						$B1 				= $_POST['B1'];
						$KET_B1 			=	$_POST['KET_B1'];  
						$B2 				=	$_POST['B2'];
						$KET_B2 			=	$_POST['KET_B2'];  
						$B3 				=	$_POST['B3'];
						$KET_B3 			=	$_POST['KET_B3'];  
						$B4 				=	$_POST['B4'];
						$KET_B4 			=	$_POST['KET_B4'];  
						$B5 				=	$_POST['B5'];
						$KET_B5 			=	$_POST['KET_B5'];  
						
						$C1 				=	$_POST['C1'];
						$KET_C1 			=	$_POST['KET_C1'];  
						$C2 				=	$_POST['C2'];
						$KET_C2 			=	$_POST['KET_C2'];  
						$C3 				=	$_POST['C3']; 
						$KET_C3 			=	$_POST['KET_C3']; 
						$C4 				=	$_POST['C4']; 
						$KET_C4 			=	$_POST['KET_C4']; 
						$C5 				=	$_POST['C5']; 
						$KET_C5 			=	$_POST['KET_C5']; 
						$C6 				=	$_POST['C6']; 
						$KET_C6 			=	$_POST['KET_C6']; 
						$C7 				=	$_POST['C7']; 
						$KET_C7 			=	$_POST['KET_C7']; 
						$C8 				=	$_POST['C8']; 
						$KET_C8 			=	$_POST['KET_C8']; 
						$C9 				=	$_POST['C9']; 
						$KET_C9 			=	$_POST['KET_C9']; 
						
						$D1 				=	$_POST['D1'];
						$KET_D1 			=	$_POST['KET_D1']; 
						$D2 				=	$_POST['D2'];
						$KET_D2 			=	$_POST['KET_D2']; 
						
						$E1 				=	$_POST['E1'];
						$KET_E1 			=	$_POST['KET_E1']; 
						$E2 				=	$_POST['E2'];
						$KET_E2 			=	$_POST['KET_E2']; 
						$E3 				=	$_POST['E3'];
						$KET_E3 			=	$_POST['KET_E3']; 
						
						$F1 				=	$_POST['F1'];
						$KET_F1 			=	$_POST['KET_F1']; 
						$F2 				=	$_POST['F2'];
						$KET_F2 			=	$_POST['KET_F2']; 
						$F3 				=	$_POST['F3'];
						$KET_F3 			=	$_POST['KET_F3']; 
						$F4 				=	$_POST['F4'];
						$KET_F4 			=	$_POST['KET_F4']; 
						
						$NILAI 				=	$_POST['NILAI'];  
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KOORDINATOR 		=	$_POST['KOORDINATOR'];  
						$AUDITOR 			=	addslashes($_POST['AUDITOR']); 
						$pengisi 			= $_SESSION['userid'];							
						//echo "<pre>";print_r($_POST);exit();                	
						$query = "INSERT INTO db_ppi.tb_londri_new (
						USER,
						TANGGAL,
						A1,
						KET_A1, 
						A2,
						KET_A2, 
						A3,
						KET_A3, 
						
						B1,
						KET_B1, 
						B2,
						KET_B2, 
						B3,
						KET_B3, 
						B4,
						KET_B4, 
						B5,
						KET_B5, 
						
						C1,
						KET_C1, 
						C2,
						KET_C2, 
						C3,
						KET_C3, 
						C4,
						KET_C4, 
						C5,
						KET_C5, 
						C6,
						KET_C6, 
						C7,
						KET_C7, 
						C8,
						KET_C8, 
						C9,
						KET_C9, 
						
						D1,
						KET_D1, 
						D2,
						KET_D2,
						
						E1,
						KET_E1, 
						E2,
						KET_E2,
						E3,
						KET_E3, 
						
						F1,
						KET_F1, 
						F2,
						KET_F2,
						F3,
						KET_F3, 
						F4,
						KET_F4, 			
						
						NILAI, 
						ANALISA,
						TINDAKLANJUT,
						KOORDINATOR,
						AUDITOR) 
						VALUES 
						('$pengisi',
						'$TANGGAL', 
						'$A1',  
						'$KET_A1',  
						'$A2',  
						'$KET_A2',  
						'$A3',  
						'$KET_A3',  
						  
						'$B1', 						
						'$KET_B1',  
						'$B2',  
						'$KET_B2',  
						'$B3',  
						'$KET_B3',  
						'$B4',  
						'$KET_B4',  
						'$B5',  
						'$KET_B5',
						
						'$C1',
						'$KET_C1', 
						'$C2',
						'$KET_C2', 
						'$C3',
						'$KET_C3', 
						'$C4',
						'$KET_C4', 
						'$C5',
						'$KET_C5', 
						'$C6',
						'$KET_C6', 
						'$C7',
						'$KET_C7', 
						'$C8',
						'$KET_C8', 
						'$C9',
						'$KET_C9', 

						'$D1',
						'$KET_D1', 
						'$D2',
						'$KET_D2',

						'$E1',
						'$KET_E1', 
						'$E2',
						'$KET_E2',
						'$E3',
						'$KET_E3', 

						'$F1',
						'$KET_F1', 
						'$F2',
						'$KET_F2',
						'$F3',
						'$KET_F3', 
						'$F4',
						'$KET_F4',
						
						'$NILAI',  
						'$ANALISA',  
						'$TINDAKLANJUT', 
						'$KOORDINATOR', 
						'$AUDITOR')";
						//echo $query; die();	
						$hasil=mysqli_query($conn1,$query);								
						if($hasil){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_londri_new'</script>"; 
						}                     
					else{
						echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
					}   
					}					
					if(isset($_POST['btnEdit'])){
						$ID					= $_POST['ID'];
						$TANGGAL 			= $_POST['TANGGAL'];									
						$A1 				= $_POST['A1'];
						$KET_A1				= $_POST['KET_A1']; 
						$A2					= $_POST['A2'];
						$KET_A2				= $_POST['KET_A2']; 
						$A3					= $_POST['A3'];
						$KET_A3 			= $_POST['KET_A3'];	
						
						$B1 				= $_POST['B1'];
						$KET_B1 			=	$_POST['KET_B1'];  
						$B2 				=	$_POST['B2'];
						$KET_B2 			=	$_POST['KET_B2'];  
						$B3 				=	$_POST['B3'];
						$KET_B3 			=	$_POST['KET_B3'];  
						$B4 				=	$_POST['B4'];
						$KET_B4 			=	$_POST['KET_B4'];  
						$B5 				=	$_POST['B5'];
						$KET_B5 			=	$_POST['KET_B5'];  
						
						$C1 				=	$_POST['C1'];
						$KET_C1 			=	$_POST['KET_C1'];  
						$C2 				=	$_POST['C2'];
						$KET_C2 			=	$_POST['KET_C2'];  
						$C3 				=	$_POST['C3']; 
						$KET_C3 			=	$_POST['KET_C3']; 
						$C4 				=	$_POST['C4']; 
						$KET_C4 			=	$_POST['KET_C4']; 
						$C5 				=	$_POST['C5']; 
						$KET_C5 			=	$_POST['KET_C5']; 
						$C6 				=	$_POST['C6']; 
						$KET_C6 			=	$_POST['KET_C6']; 
						$C7 				=	$_POST['C7']; 
						$KET_C7 			=	$_POST['KET_C7']; 
						$C8 				=	$_POST['C8']; 
						$KET_C8 			=	$_POST['KET_C8']; 
						$C9 				=	$_POST['C9']; 
						$KET_C9 			=	$_POST['KET_C9']; 
						
						$D1 				=	$_POST['D1'];
						$KET_D1 			=	$_POST['KET_D1']; 
						$D2 				=	$_POST['D2'];
						$KET_D2 			=	$_POST['KET_D2']; 
						
						$E1 				=	$_POST['E1'];
						$KET_E1 			=	$_POST['KET_E1']; 
						$E2 				=	$_POST['E2'];
						$KET_E2 			=	$_POST['KET_E2']; 
						$E3 				=	$_POST['E3'];
						$KET_E3 			=	$_POST['KET_E3']; 
						
						$F1 				=	$_POST['F1'];
						$KET_F1 			=	$_POST['KET_F1']; 
						$F2 				=	$_POST['F2'];
						$KET_F2 			=	$_POST['KET_F2']; 
						$F3 				=	$_POST['F3'];
						$KET_F3 			=	$_POST['KET_F3']; 
						$F4 				=	$_POST['F4'];
						$KET_F4 			=	$_POST['KET_F4']; 
						
						$NILAI 				=	$_POST['NILAI'];  
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KOORDINATOR 		=	$_POST['KOORDINATOR'];  
						$AUDITOR 			=	addslashes($_POST['AUDITOR']); 						
						$USER 				= $_SESSION['userid'];		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_londri_new SET												
						TANGGAL 			= '$TANGGAL',									
						A1 				= '$A1',
						KET_A1				= '$KET_A1', 
						A2					= '$A2',
						KET_A2				= '$KET_A2', 
						A3					= '$A3',
						KET_A3 			= '$KET_A3',	

						B1 				= '$B1',
						KET_B1 			=	'$KET_B1',  
						B2 				=	'$B2',
						KET_B2 			=	'$KET_B2',  
						B3 				=	'$B3',
						KET_B3 			=	'$KET_B3',  
						B4 				=	'$B4',
						KET_B4 			=	'$KET_B4',  
						B5 				=	'$B5',
						KET_B5 			=	'$KET_B5',  

						C1 				=	'$C1',
						KET_C1 			=	'$KET_C1',  
						C2 				=	'$C2',
						KET_C2 			=	'$KET_C2',  
						C3 				=	'$C3', 
						KET_C3 			=	'$KET_C3', 
						C4 				=	'$C4', 
						KET_C4 			=	'$KET_C4', 
						C5 				=	'$C5', 
						KET_C5 			=	'$KET_C5', 
						C6 				=	'$C6', 
						KET_C6 			=	'$KET_C6', 
						C7 				=	'$C7', 
						KET_C7 			=	'$KET_C7', 
						C8 				=	'$C8', 
						KET_C8 			=	'$KET_C8', 
						C9 				=	'$C9', 
						KET_C9 			=	'$KET_C9', 

						D1 				=	'$D1',
						KET_D1 			=	'$KET_D1', 
						D2 				=	'$D2',
						KET_D2 			=	'$KET_D2', 

						E1 				=	'$E1',
						KET_E1 			=	'$KET_E1', 
						E2 				=	'$E2',
						KET_E2 			=	'$KET_E2', 
						E3 				=	'$E3',
						KET_E3 			=	'$KET_E3', 

						F1 				=	'$F1',
						KET_F1 			=	'$KET_F1', 
						F2 				=	'$F2',
						KET_F2 			=	'$KET_F2', 
						F3 				=	'$F3',
						KET_F3 			=	'$KET_F3', 
						F4 				=	'$F4',
						KET_F4 			=	'$KET_F4', 

						NILAI 				=	'$NILAI',  
						ANALISA 			=	'$ANALISA',  
						TINDAKLANJUT 		=	'$TINDAKLANJUT',  
						KOORDINATOR 		=	'$KOORDINATOR',  
						AUDITOR 			=	'$AUDITOR', 
						DIUBAH_OLEH='$USER'
						where ID='$ID'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_londri_new'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_londri_new ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_londri_new'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_londri_new'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			