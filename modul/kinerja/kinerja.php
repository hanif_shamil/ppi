<style>
.input-group-addon {
    width:85%;
    text-align:left;
}
</style>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Indikator kinerja</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Indikator kinerja pencegahan dan pengendalian infeksi</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_k" method="post">	
								<div class="form-group">										
									<div class="col-sm-6">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">												
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
												<span class="input-group-addon" style="width:10%">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>	
									</div>
									<div class="col-sm-6">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ Ruangan ]" required>
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data[0]?>"><?=$data[3]?></option>
												<?php
												}
												?>	
											</select>
										</div>
									</div>
								</div>	
								<div class="form-group">
									<div class="col-sm-6">
										<div class="input-group">
											<span class="input-group-addon">
											1. Jumlah pasien yang terpasang infus purifer
											</span>
											<input type="text" name="SATU" placeholder="[  Jumlah ]" class="form-control" />
										</div>
									</div>
									<div class="col-sm-6">
										<div class="input-group">
											<span class="input-group-addon">
											2. Pasien yang terpasang infus purifer baru
											</span>
											<input type="text" name="DUA" placeholder="[  Jumlah ]" class="form-control" />
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-6">
										<div class="input-group">
											<span class="input-group-addon">
												3. Jumlah pasien yang terpasang CVC
											</span>
											<input type="text" name="TIGA" placeholder="[  Jumlah ]" class="form-control" />
										</div>
									</div>																		
									<div class="col-sm-6">
										<div class="input-group">
											<span class="input-group-addon">
												4. Pasien yang terpasang CVC baru
											</span>
											<input type="text" name="EMPAT" placeholder="[  Jumlah ]" class="form-control" />
										</div>
									</div>								
								</div>
								<div class="form-group">
									<div class="col-sm-6">
										<div class="input-group">
											<span class="input-group-addon">
												5. Pasien yang terpasang kateter urine
											</span>
											<input type="text" name="ENAM" placeholder="[ Jumlah ]" class="form-control" />
										</div>										
									</div>																		
									<div class="col-sm-6">
										<div class="input-group">
											<span class="input-group-addon">
												6. Jumlah pasien yang terpasang kateter urine baru
											</span>
											<input type="text" name="LIMA" placeholder="[ Jumlah ]" class="form-control" />
										</div>
									</div>								
								</div>
								<div class="form-group">
									<div class="col-sm-6">
										<div class="input-group">
										<span class="input-group-addon">
											7. Pasien yang terpasang Ventilator/ETT
										</span>
											<input type="text"name="TUJUH"  placeholder="[ Jumlah ]" class="form-control" />
										</div>
									</div>																		
									<div class="col-sm-6">
										<div class="input-group">
										<span class="input-group-addon">
											8. Pasien yang terpasang Ventilator/ETT baru
										</span>
											<input type="text" name="DELAPAN" placeholder="[ Jumlah ]" class="form-control" />
										</div>
									</div>									
								</div>
								<div class="form-group">
									<div class="col-sm-6">
										<div class="input-group">
										<span class="input-group-addon">
											9. Jumlah pasien operasi
										</span>
											<input type="text" name="SEMBILAN" placeholder="[ Jumlah ]" class="form-control" />
										</div>
									</div>																		
									<div class="col-sm-6">
										<div class="input-group">
										<span class="input-group-addon">
											10. Pasien total care
										</span>
											<input type="text" name="SEPULUH" placeholder="[ Jumlah ]" class="form-control" />
										</div>
									</div>									
								</div>			
								<div class="form-group">
									<div class="col-sm-6">
										<div class="input-group">
										<span class="input-group-addon">
											11. Jumlah pasien yang terpasang CDL
										</span>
											<input type="text" name="CDL1" placeholder="[ Jumlah ]" class="form-control" />
										</div>
									</div>																		
									<div class="col-sm-6">
										<div class="input-group">
										<span class="input-group-addon">
											12. Pasien yang terpasang CDL baru
										</span>
											<input type="text" name="CDL2" placeholder="[ Jumlah ]" class="form-control" />
										</div>
									</div>									
								</div>																																			
								<hr />
								<div class="form-group">							
									<div class="col-md-offset-3 col-md-9">
										<button class="btn btn-info" name="btnsimpan" type="submit">
											<i class="ace-icon fa fa-check bigger-110"></i>
											Submit
										</button>
										&nbsp; &nbsp; &nbsp;
										<button class="btn" type="reset">
											<i class="ace-icon fa fa-undo bigger-110"></i>
											Reset
										</button>
									</div>
								</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->