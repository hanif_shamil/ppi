<?php
function serialize_ke_string($serial)
{
    $hasil = unserialize($serial);
    return implode(', ', $hasil);
}
?>

<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Tables</a>
		</li>
		<li class="active">Laporan Indikator Kinerja</li>
	</ul><!-- /.breadcrumb -->

	<!--<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="ace-icon fa fa-search nav-search-icon"></i>
			</span>
		</form>
	</div><!-- /.nav-search -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="table-header">
						Laporan Indikator Kinerja
					</div>

					<!-- div.table-responsive -->

					<!-- div.dataTables_borderWrap -->
					<div>
						<table id="example1" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr align="center">
									<th><div align="center">Tanggal</div></th>
									<th><div align="center">Ruangan</div></th>
									<th><div align="center">1</div></th>
									<th><div align="center">2</div></th>
									<th><div align="center">3</div></th>
									<th><div align="center">4</div></th>
									<th><div align="center">5</div></th>
									<th><div align="center">6</div></th>
									<th><div align="center">7</div></th>
									<th><div align="center">8</div></th>
									<th><div align="center">9</div></th>
									<th><div align="center">10</div></th>
									
									<th><div align="center">OPSI</div></th>
								</tr>
							</thead>

							<tbody>
							<?php					
							$query="SELECT k.ID_KINERJA,k.TANGGAL, k.RUANGAN,k.SATU,k.DUA,k.TIGA,k.EMPAT,k.LIMA,k.ENAM,k.TUJUH,k.DELAPAN,k.SEMBILAN,k.SEPULUH, r.DESKRIPSI NM_RUANGAN 
							,(k.SATU + k.DUA + k.TIGA+ k.EMPAT + k.LIMA + k.ENAM + k.TUJUH + k.DELAPAN + k.SEMBILAN + k.SEPULUH) TOTAL
							FROM db_ppi.tb_kinerja k
							LEFT JOIN db_ppi.ruangan r ON r.ID=k.RUANGAN and r.JENIS=5
							where k.STATUS=1 order by k.ID_KINERJA desc ";								
							$info=mysqli_query($conn1,$query); 
							//untuk penomoran data
							//$no=1;						
							//menampilkan data
							while($row=mysqli_fetch_array($info)){
							?>
								<tr>
									<td><?php echo $row['TANGGAL'] ?></td>
									<td><?php echo $row['NM_RUANGAN'] ?></td>
									<td><?php echo $row['SATU'] ?></td>
									<td><?php echo $row['DUA'] ?></td>
									<td><?php echo $row['TIGA'] ?></td>
									<td><?php echo $row['EMPAT'] ?></td>	
									<td><?php echo $row['LIMA'] ?></td>	
									<td><?php echo $row['ENAM'] ?></td>	
									<td><?php echo $row['TUJUH'] ?></td>	
									<td><?php echo $row['DELAPAN'] ?></td>	
									<td><?php echo $row['SEMBILAN'] ?></td>	
									<td><?php echo $row['SEPULUH'] ?></td>	
																	
									<td width="10%" align="center" >
										<div class="hidden-sm hidden-xs action-buttons">
											<!--<a class="blue" href="?page=detail_hd&id=<?php echo $row['ID_KINERJA']?>">
												<i class="ace-icon fa fa-search-plus bigger-130"></i>												
											</a>-->
											<a class="green" href="?page=edit_k&id=<?php echo $row['ID_KINERJA']?>">
												<i class="glyphicon glyphicon-pencil btn btn-success btn-sm"></i>
											</a>
											<a class="red" href="?page=delet_k&id=<?php echo $row['ID_KINERJA']?>">
												<i class="glyphicon glyphicon-trash btn btn-danger btn-sm"></i>	
											</a>
										</div>
									</td>
								</tr>							
								<?php							
								}
								?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

