<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Data</a>
		</li>
		<li class="active">Data Audit Kamar Jenazah</li>
	</ul><!-- /.breadcrumb -->

	<!--<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="ace-icon fa fa-search nav-search-icon"></i>
			</span>
		</form>
	</div><!-- /.nav-search -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="table-header">
						Data Audit Kamar Jenazah
					</div>

					<!-- div.table-responsive -->

					<!-- div.dataTables_borderWrap -->
					<div>
						<table id="tabeldata" class="table table-striped table-bordered table-hover">
							<thead>
								<tr align="center">
									<th><div align="center">Tanggal</div></th>									
									<th><div align="center">Auditor</div></th>
									<th><div align="center">Total (%)</div></th>
									<th><div align="center">Opsi</div></th>
								</tr>								
								
							</thead>

							<tbody>
							<?php					
							$query="select ti.ID_ISI, ti.TGL_INPUT, ti.TANGGAL,ti.AUDITOR, ti.TOTAL, ti.`STATUS`
							from tb_akj ti					
							where ti.`STATUS`=1";							
							$info=mysqli_query($conn1,$query); 
							//untuk penomoran data
							//$no=1;						
							//menampilkan data
							while($row=mysqli_fetch_array($info)){
							?>
								<tr>
									<td><?php echo $row['TANGGAL'] ?></td>
									<td><?php echo $row['AUDITOR'] ?></td>								
									<td><?php echo $row['TOTAL'] ?></td>						
									<td width="15%" align="center" >
										<div class="hidden-sm hidden-xs action-buttons">
											<a class="blue" href="?page=det_akj&id=<?php echo $row['ID_ISI']?>">
												<i class="glyphicon glyphicon-search btn btn-info btn-sm"></i>											
											</a>
											<a class="green" href="?page=edit_akj&id=<?php echo $row['ID_ISI']?>">
												<i class="glyphicon glyphicon-pencil btn btn-success btn-sm"></i>
											</a>
											<a class="red" href="?page=simpan_akj&id=<?php echo $row['ID_ISI']?>">
												<i class="glyphicon glyphicon-trash btn btn-danger btn-sm"></i>	
											</a>
										</div>
									</td>
								</tr>							
								<?php							
								}
								?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

