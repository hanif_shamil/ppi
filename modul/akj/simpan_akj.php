<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['APD'])){
							$APD = implode(',',$_POST['APD']);
						}else{
							$APD = "";
						}		
						if(isset($_POST['MASKER'])){
							$MASKER = implode(',',$_POST['MASKER']);
						}else{
							$MASKER = "";
						}	
						if(isset($_POST['TOPI'])){
							$TOPI = implode(',',$_POST['TOPI']);
						}else{
							$TOPI = "";
						}	
						if(isset($_POST['APRON'])){
							$APRON = implode(',',$_POST['APRON']);
						}else{
							$APRON = "";
						}	
						if(isset($_POST['KACAMATA'])){
							$KACAMATA = implode(',',$_POST['KACAMATA']);
						}else{
							$KACAMATA = "";
						}
						if(isset($_POST['SARUNG_TANGAN'])){
							$SARUNG_TANGAN = implode(',',$_POST['SARUNG_TANGAN']);
						}else{
							$SARUNG_TANGAN = "";
						}
						if(isset($_POST['MEJA_MANDI'])){
							$MEJA_MANDI = implode(',',$_POST['MEJA_MANDI']);
						}else{
							$MEJA_MANDI = "";
						}
						if(isset($_POST['BRANKART'])){
							$BRANKART = implode(',',$_POST['BRANKART']);
						}else{
							$BRANKART = "";
						}
						if(isset($_POST['SIRAM_MEJA'])){
							$SIRAM_MEJA = implode(',',$_POST['SIRAM_MEJA']);
						}else{
							$SIRAM_MEJA = "";
						}
						if(isset($_POST['KLORIN'])){
							$KLORIN = implode(',',$_POST['KLORIN']);
						}else{
							$KLORIN = "";
						}
						if(isset($_POST['MEMBILAS'])){
							$MEMBILAS = implode(',',$_POST['MEMBILAS']);
						}else{
							$MEMBILAS = "";
						}
						if(isset($_POST['CUCI_APRON'])){
							$CUCI_APRON = implode(',',$_POST['CUCI_APRON']);
						}else{
							$CUCI_APRON = "";
						}
						if(isset($_POST['CUCI_SARUNG'])){
							$CUCI_SARUNG = implode(',',$_POST['CUCI_SARUNG']);
						}else{
							$CUCI_SARUNG = "";
						}
						if(isset($_POST['CUCI_SEPATU'])){
							$CUCI_SEPATU = implode(',',$_POST['CUCI_SEPATU']);
						}else{
							$CUCI_SEPATU = "";
						}
						if(isset($_POST['BUANG_MASKER'])){
							$BUANG_MASKER = implode(',',$_POST['BUANG_MASKER']);
						}else{
							$BUANG_MASKER = "";
						}
						if(isset($_POST['CUCI_TANGAN'])){
							$CUCI_TANGAN = implode(',',$_POST['CUCI_TANGAN']);
						}else{
							$CUCI_TANGAN = "";
						}
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_akj (
						ID_ISI,
						TANGGAL,						
						AUDITOR,
						APD,
						MASKER,
						TOPI,
						APRON,
						KACAMATA,
						SARUNG_TANGAN,
						MEJA_MANDI,
						BRANKART,
						SIRAM_MEJA,
						KLORIN,
						MEMBILAS,
						CUCI_APRON,
						CUCI_SARUNG,
						CUCI_SEPATU,
						BUANG_MASKER,
						CUCI_TANGAN,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',						
						'$AUDITOR',
						'$APD',
						'$MASKER',	
						'$TOPI',		
						'$APRON',				
						'$KACAMATA',	
						'$SARUNG_TANGAN',	
						'$MEJA_MANDI',	
						'$BRANKART',	
						'$SIRAM_MEJA',	
						'$KLORIN',	
						'$MEMBILAS',	
						'$CUCI_APRON',	
						'$CUCI_SARUNG',	
						'$CUCI_SEPATU',	
						'$BUANG_MASKER',	
						'$CUCI_TANGAN',	
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_akj'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AUDITOR = addslashes($_POST['AUDITOR']);
						$APD = $_POST['APD'];						
						$MASKER = $_POST['MASKER'];					
						$TOPI = $_POST['TOPI'];
						$APRON = $_POST['APRON'];						
						$KACAMATA = $_POST['KACAMATA'];
						$SARUNG_TANGAN = $_POST['SARUNG_TANGAN'];
						$MEJA_MANDI = $_POST['MEJA_MANDI'];
						$BRANKART = $_POST['BRANKART'];
						$SIRAM_MEJA = $_POST['SIRAM_MEJA'];
						$KLORIN = $_POST['KLORIN'];
						$MEMBILAS = $_POST['MEMBILAS'];
						$CUCI_APRON = $_POST['CUCI_APRON'];
						$CUCI_SARUNG = $_POST['CUCI_SARUNG'];
						$CUCI_SEPATU = $_POST['CUCI_SEPATU'];
						$BUANG_MASKER = $_POST['BUANG_MASKER'];
						$CUCI_TANGAN = $_POST['CUCI_TANGAN'];
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_akj SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						AUDITOR='$AUDITOR',
						APD = '$APD',						
						MASKER = '$MASKER',					
						TOPI = '$TOPI',
						APRON = '$APRON',						
						KACAMATA = '$KACAMATA',
						SARUNG_TANGAN = '$SARUNG_TANGAN',
						MEJA_MANDI = '$MEJA_MANDI',
						BRANKART = '$BRANKART',
						SIRAM_MEJA = '$SIRAM_MEJA',
						KLORIN = '$KLORIN',
						MEMBILAS = '$MEMBILAS',
						CUCI_APRON = '$CUCI_APRON',
						CUCI_SARUNG = '$CUCI_SARUNG',
						CUCI_SEPATU = '$CUCI_SEPATU',
						BUANG_MASKER = '$BUANG_MASKER',
						CUCI_TANGAN = '$CUCI_TANGAN',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_akj'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_akj ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_akj'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			