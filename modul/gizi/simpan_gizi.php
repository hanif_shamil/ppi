<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];						
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						$BAKCUCI 		= $_POST['BAKCUCI'];
						$DRAINASE 		= $_POST['DRAINASE'];
						$JENDELA_SKREEN	= $_POST['JENDELA_SKREEN'];
						$LEMARI			= $_POST['LEMARI'];
						$KULKAS			= $_POST['KULKAS'];
						$FREEZER		= $_POST['FREEZER'];
						$RAK			= $_POST['RAK'];
						$CHILLER		= $_POST['CHILLER'];
						$JENDELA		= $_POST['JENDELA'];
						$LANTAI			= $_POST['LANTAI'];
						$DINDING		= $_POST['DINDING'];
						$MEJA_LAYANAN	= $_POST['MEJA_LAYANAN'];
						$BAK_CUCI		= $_POST['BAK_CUCI'];
						$PERKAKAS		= $_POST['PERKAKAS'];
						$TALENAN		= $_POST['TALENAN'];
						$LANTAI_PENYIMPANAN	= $_POST['LANTAI_PENYIMPANAN'];
						$PINTU_PENYIMPANAN	= $_POST['PINTU_PENYIMPANAN'];
						$DINDING_PENYIMPANAN= $_POST['DINDING_PENYIMPANAN'];
						$RAK_PENYIMPANAN	= $_POST['RAK_PENYIMPANAN'];
						$KOMPOR				= $_POST['KOMPOR'];
						$KULKAS_PERALATAN	= $_POST['KULKAS_PERALATAN'];
						$FREZZER_PERALATAN	= $_POST['FREZZER_PERALATAN'];
						$TROLLY_MAKAN		= $_POST['TROLLY_MAKAN'];
						$CONTAINER_TUTUP	= $_POST['CONTAINER_TUTUP'];
						$CONTAINER_DITANDAI	= $_POST['CONTAINER_DITANDAI'];
						$JARAK_PENYIMPANAN	= $_POST['JARAK_PENYIMPANAN'];
						$KADALUARSA			= $_POST['KADALUARSA'];
						$TEMPERETUR			= $_POST['TEMPERETUR'];
						$TOPI				= $_POST['TOPI'];
						$MASKER				= $_POST['MASKER'];
						$CELEMEK			= $_POST['CELEMEK'];
						$CUCI_TANGAN_SBLM	= $_POST['CUCI_TANGAN_SBLM'];
						$CUCI_TANGAN_SSDH	= $_POST['CUCI_TANGAN_SSDH'];
						$BERSIH_SABUN		= $_POST['BERSIH_SABUN'];
						$BERSIH_DESINFEKSI	= $_POST['BERSIH_DESINFEKSI'];
						$SAJI_SEKALI		= $_POST['SAJI_SEKALI'];
						$SAJI_DUAJAM		= $_POST['SAJI_DUAJAM'];
						$MAKAN_TUTP			= $_POST['MAKAN_TUTP'];
						$TEMPT_SAMPAH_TUTP	= $_POST['TEMPT_SAMPAH_TUTP'];
						$TEMPT_SAMPAH_BERSIH= $_POST['TEMPT_SAMPAH_BERSIH'];
						$TOTAL	 			= $_POST['TOTAL'];							
						$USER 				= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_gizi (
						ID_ISI,
						TANGGAL,
						RUANGAN,
						AUDITOR,
						BAKCUCI,						
						DRAINASE,
						JENDELA_SKREEN,
						LEMARI,
						KULKAS,
						FREEZER,
						RAK,
						CHILLER,
						JENDELA,
						LANTAI,
						DINDING,
						MEJA_LAYANAN,
						BAK_CUCI,
						PERKAKAS,
						TALENAN,
						LANTAI_PENYIMPANAN,
						PINTU_PENYIMPANAN,
						DINDING_PENYIMPANAN,
						RAK_PENYIMPANAN,
						KOMPOR,
						KULKAS_PERALATAN,
						FREZZER_PERALATAN,
						TROLLY_MAKAN,
						CONTAINER_TUTUP,
						CONTAINER_DITANDAI,
						JARAK_PENYIMPANAN,
						KADALUARSA,
						TEMPERETUR,
						TOPI,
						MASKER,
						CELEMEK,
						CUCI_TANGAN_SBLM,
						CUCI_TANGAN_SSDH,
						BERSIH_SABUN,
						BERSIH_DESINFEKSI,
						SAJI_SEKALI,
						SAJI_DUAJAM,
						MAKAN_TUTP,
						TEMPT_SAMPAH_TUTP,
						TEMPT_SAMPAH_BERSIH,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$RUANGAN',
						'$AUDITOR',						
						'$BAKCUCI',
						'$DRAINASE',
						'$JENDELA_SKREEN',
						'$LEMARI',
						'$KULKAS',
						'$FREEZER',
						'$RAK',
						'$CHILLER',
						'$JENDELA',
						'$LANTAI',
						'$DINDING',
						'$MEJA_LAYANAN',
						'$BAK_CUCI',
						'$PERKAKAS',
						'$TALENAN',
						'$LANTAI_PENYIMPANAN',
						'$PINTU_PENYIMPANAN',
						'$DINDING_PENYIMPANAN',
						'$RAK_PENYIMPANAN',
						'$KOMPOR',
						'$KULKAS_PERALATAN',
						'$FREZZER_PERALATAN',
						'$TROLLY_MAKAN',
						'$CONTAINER_TUTUP',
						'$CONTAINER_DITANDAI',
						'$JARAK_PENYIMPANAN',
						'$KADALUARSA',
						'$TEMPERETUR',
						'$TOPI',
						'$MASKER',
						'$CELEMEK',
						'$CUCI_TANGAN_SBLM',
						'$CUCI_TANGAN_SSDH',
						'$BERSIH_SABUN',
						'$BERSIH_DESINFEKSI',
						'$SAJI_SEKALI',
						'$SAJI_DUAJAM',
						'$MAKAN_TUTP',
						'$TEMPT_SAMPAH_TUTP',
						'$TEMPT_SAMPAH_BERSIH',						
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_gizi'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];						
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						$BAKCUCI 		= $_POST['BAKCUCI'];
						$DRAINASE 		= $_POST['DRAINASE'];
						$JENDELA_SKREEN	= $_POST['JENDELA_SKREEN'];
						$LEMARI			= $_POST['LEMARI'];
						$KULKAS			= $_POST['KULKAS'];
						$FREEZER		= $_POST['FREEZER'];
						$RAK			= $_POST['RAK'];
						$CHILLER		= $_POST['CHILLER'];
						$JENDELA		= $_POST['JENDELA'];
						$LANTAI			= $_POST['LANTAI'];
						$DINDING		= $_POST['DINDING'];
						$MEJA_LAYANAN	= $_POST['MEJA_LAYANAN'];
						$BAK_CUCI		= $_POST['BAK_CUCI'];
						$PERKAKAS		= $_POST['PERKAKAS'];
						$TALENAN		= $_POST['TALENAN'];
						$LANTAI_PENYIMPANAN	= $_POST['LANTAI_PENYIMPANAN'];
						$PINTU_PENYIMPANAN	= $_POST['PINTU_PENYIMPANAN'];
						$DINDING_PENYIMPANAN= $_POST['DINDING_PENYIMPANAN'];
						$RAK_PENYIMPANAN	= $_POST['RAK_PENYIMPANAN'];
						$KOMPOR				= $_POST['KOMPOR'];
						$KULKAS_PERALATAN	= $_POST['KULKAS_PERALATAN'];
						$FREZZER_PERALATAN	= $_POST['FREZZER_PERALATAN'];
						$TROLLY_MAKAN		= $_POST['TROLLY_MAKAN'];
						$CONTAINER_TUTUP	= $_POST['CONTAINER_TUTUP'];
						$CONTAINER_DITANDAI	= $_POST['CONTAINER_DITANDAI'];
						$JARAK_PENYIMPANAN	= $_POST['JARAK_PENYIMPANAN'];
						$KADALUARSA			= $_POST['KADALUARSA'];
						$TEMPERETUR			= $_POST['TEMPERETUR'];
						$TOPI				= $_POST['TOPI'];
						$MASKER				= $_POST['MASKER'];
						$CELEMEK			= $_POST['CELEMEK'];
						$CUCI_TANGAN_SBLM	= $_POST['CUCI_TANGAN_SBLM'];
						$CUCI_TANGAN_SSDH	= $_POST['CUCI_TANGAN_SSDH'];
						$BERSIH_SABUN		= $_POST['BERSIH_SABUN'];
						$BERSIH_DESINFEKSI	= $_POST['BERSIH_DESINFEKSI'];
						$SAJI_SEKALI		= $_POST['SAJI_SEKALI'];
						$SAJI_DUAJAM		= $_POST['SAJI_DUAJAM'];
						$MAKAN_TUTP			= $_POST['MAKAN_TUTP'];
						$TEMPT_SAMPAH_TUTP	= $_POST['TEMPT_SAMPAH_TUTP'];
						$TEMPT_SAMPAH_BERSIH= $_POST['TEMPT_SAMPAH_BERSIH'];
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_gizi SET												
						TANGGAL					='$TANGGAL',
						TGL_UBAH				='$waktu',
						RUANGAN					='$RUANGAN',
						AUDITOR 				= '$AUDITOR',
						BAKCUCI 				= '$BAKCUCI',
						DRAINASE 				= '$DRAINASE',
						JENDELA_SKREEN			= '$JENDELA_SKREEN',
						LEMARI					= '$LEMARI',
						KULKAS					= '$KULKAS',
						FREEZER					= '$FREEZER',
						RAK						= '$RAK',
						CHILLER					= '$CHILLER',
						JENDELA					= '$JENDELA',
						LANTAI					= '$LANTAI',
						DINDING					= '$DINDING',
						MEJA_LAYANAN			= '$MEJA_LAYANAN',
						BAK_CUCI				= '$BAK_CUCI',
						PERKAKAS				= '$PERKAKAS',
						TALENAN					= '$TALENAN',
						LANTAI_PENYIMPANAN		= '$LANTAI_PENYIMPANAN',
						PINTU_PENYIMPANAN		= '$PINTU_PENYIMPANAN',
						DINDING_PENYIMPANAN		= '$DINDING_PENYIMPANAN',
						RAK_PENYIMPANAN			= '$RAK_PENYIMPANAN',
						KOMPOR					= '$KOMPOR',
						KULKAS_PERALATAN		= '$KULKAS_PERALATAN',
						FREZZER_PERALATAN		= '$FREZZER_PERALATAN',
						TROLLY_MAKAN			= '$TROLLY_MAKAN',
						CONTAINER_TUTUP			= '$CONTAINER_TUTUP',
						CONTAINER_DITANDAI		= '$CONTAINER_DITANDAI',
						JARAK_PENYIMPANAN		= '$JARAK_PENYIMPANAN',
						KADALUARSA				= '$KADALUARSA',
						TEMPERETUR				= '$TEMPERETUR',
						TOPI					= '$TOPI',
						MASKER					= '$MASKER',
						CELEMEK					= '$CELEMEK',
						CUCI_TANGAN_SBLM		= '$CUCI_TANGAN_SBLM',
						CUCI_TANGAN_SSDH		= '$CUCI_TANGAN_SSDH',
						BERSIH_SABUN			= '$BERSIH_SABUN',
						BERSIH_DESINFEKSI		= '$BERSIH_DESINFEKSI',
						SAJI_SEKALI				= '$SAJI_SEKALI',
						SAJI_DUAJAM				= '$SAJI_DUAJAM',
						MAKAN_TUTP				= '$MAKAN_TUTP',
						TEMPT_SAMPAH_TUTP		= '$TEMPT_SAMPAH_TUTP',
						TEMPT_SAMPAH_BERSIH		= '$TEMPT_SAMPAH_BERSIH',
						TOTAL					= '$TOTAL',
						DIUBAH_OLEH				= '$USER'
						where ID_ISI			= '$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_gizi'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_gizi ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_gizi'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			