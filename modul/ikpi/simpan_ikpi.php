<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$NOIZIN 		= $_POST['NOIZIN'];
						$LOKASI 		= $_POST['LOKASI'];
						$KOORDINATOR  	= $_POST['KOORDINATOR'];
						$KONTRAKTOR  	= $_POST['KONTRAKTOR'];
						$SUPERVISOR  	= $_POST['SUPERVISOR'];
						$TGL_MULAI		= $_POST['TGL_MULAI'];
						$DURASI  		= $_POST['DURASI'];
						$TGL_KADALUARSA	= $_POST['TGL_KADALUARSA'];
						$TELEPON  		= $_POST['TELEPON'];
						$TIPE_A 		= $_POST['TIPE_A']; 
						$TIPE_B 		= $_POST['TIPE_B']; 
						$TIPE_C 		= $_POST['TIPE_C']; 
						$TIPE_D 		= $_POST['TIPE_D']; 
						$KELOMPOK_A 	= $_POST['KELOMPOK_A']; 
						$KELOMPOK_B 	= $_POST['KELOMPOK_B']; 
						$KELOMPOK_C 	= $_POST['KELOMPOK_C']; 
						$KELOMPOK_D 	= $_POST['KELOMPOK_D']; 
						$PERSYARATAN 	= $_POST['PERSYARATAN']; 
						$IZIN_DIMINTA 	= $_POST['IZIN_DIMINTA']; 
						$IZIN_DISAHKAN 	= $_POST['IZIN_DISAHKAN']; 
						$TGL_DIMINTA 		= $_POST['TGL_DIMINTA']; 
						$TGL_DISAHKAN 	= $_POST['TGL_DISAHKAN'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$AUDITOR		= addslashes($_POST['AUDITOR']);						
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_ikpi (
							ID_ISI,
							NOIZIN,
							LOKASI,
							KOORDINATOR,
							KONTRAKTOR,
							SUPERVISOR,
							TGL_MULAI,
							DURASI,
							TGL_KADALUARSA,
							TELEPON,
							TIPE_A, 
							TIPE_B, 
							TIPE_C, 
							TIPE_D, 
							KELOMPOK_A, 
							KELOMPOK_B, 
							KELOMPOK_C, 
							KELOMPOK_D,
							PERSYARATAN,
							IZIN_DIMINTA,
							IZIN_DISAHKAN,
							TGL_DIMINTA,
							TGL_DISAHKAN, 
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							AUDITOR,
							USER,
							STATUS) 
						VALUES 
						('$ID_ISI',
							'$NOIZIN',
							'$LOKASI',
							'$KOORDINATOR',
							'$KONTRAKTOR',
							'$SUPERVISOR',
							'$TGL_MULAI',
							'$DURASI',
							'$TGL_KADALUARSA',
							'$TELEPON',
							'$TIPE_A', 
							'$TIPE_B', 
							'$TIPE_C', 
							'$TIPE_D', 
							'$KELOMPOK_A', 
							'$KELOMPOK_B', 
							'$KELOMPOK_C', 
							'$KELOMPOK_D', 
							'$PERSYARATAN', 
							'$IZIN_DIMINTA', 
							'$IZIN_DISAHKAN', 
							'$TGL_DIMINTA', 
							'$TGL_DISAHKAN',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$AUDITOR',	 
							'$USER',												
							'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_ikpi'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$NOIZIN 		= $_POST['NOIZIN'];
						$LOKASI 		= $_POST['LOKASI'];
						$KOORDINATOR  	= $_POST['KOORDINATOR'];
						$KONTRAKTOR  	= $_POST['KONTRAKTOR'];
						$SUPERVISOR  	= $_POST['SUPERVISOR'];
						$TGL_MULAI		= $_POST['TGL_MULAI'];
						$DURASI  		= $_POST['DURASI'];
						$TGL_KADALUARSA	= $_POST['TGL_KADALUARSA'];
						$TELEPON  		= $_POST['TELEPON'];
						$TIPE_A 		= $_POST['TIPE_A']; 
						$TIPE_B 		= $_POST['TIPE_B']; 
						$TIPE_C 		= $_POST['TIPE_C']; 
						$TIPE_D 		= $_POST['TIPE_D']; 
						$KELOMPOK_A 	= $_POST['KELOMPOK_A']; 
						$KELOMPOK_B 	= $_POST['KELOMPOK_B']; 
						$KELOMPOK_C 	= $_POST['KELOMPOK_C']; 
						$KELOMPOK_D 	= $_POST['KELOMPOK_D'];	
						$PERSYARATAN 	= $_POST['PERSYARATAN']; 
						$IZIN_DIMINTA 	= $_POST['IZIN_DIMINTA']; 
						$IZIN_DISAHKAN 	= $_POST['IZIN_DISAHKAN']; 
						$TGL_DIMINTA 	= $_POST['TGL_DIMINTA']; 
						$TGL_DISAHKAN 	= $_POST['TGL_DISAHKAN'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$AUDITOR		= addslashes($_POST['AUDITOR']);										
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_ikpi SET												
						TGL_UBAH		= '$waktu',
						NOIZIN 			= '$NOIZIN',
						LOKASI 			= '$LOKASI',
						KOORDINATOR  	= '$KOORDINATOR',
						KONTRAKTOR  	= '$KONTRAKTOR',
						SUPERVISOR  	= '$SUPERVISOR',
						TGL_MULAI		= '$TGL_MULAI',
						DURASI  		= '$DURASI',
						TGL_KADALUARSA	= '$TGL_KADALUARSA',
						TELEPON  		= '$TELEPON',
						TIPE_A 			= '$TIPE_A', 
						TIPE_B 			= '$TIPE_B', 
						TIPE_C 			= '$TIPE_C', 
						TIPE_D 			= '$TIPE_D', 
						KELOMPOK_A 		= '$KELOMPOK_A', 
						KELOMPOK_B 		= '$KELOMPOK_B', 
						KELOMPOK_C 		= '$KELOMPOK_C', 
						KELOMPOK_D 		= '$KELOMPOK_D',
						PERSYARATAN 	= '$PERSYARATAN',
						IZIN_DIMINTA 	= '$IZIN_DIMINTA',
						IZIN_DISAHKAN 	= '$IZIN_DISAHKAN', 
						TGL_DIMINTA 	= '$TGL_DIMINTA',
						TGL_DISAHKAN 	= '$TGL_DISAHKAN',
						ANALISA 		= '$ANALISA', 
						TINDAKLANJUT 	= '$TINDAKLANJUT', 
						KEPALA 			= '$KEPALA',
						AUDITOR			='$AUDITOR',				
						DIUBAH_OLEH		= '$USER'
						where ID_ISI	= '$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_ikpi'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_ikpi ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_ikpi'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			