<?php
$id=($_GET['id']);
$query="select ti.ID_ISI,ti.NOIZIN, ti.LOKASI, ti.KOORDINATOR, ti.KONTRAKTOR, ti.SUPERVISOR, ti.TGL_MULAI, ti.DURASI, ti.TGL_KADALUARSA, ti.TELEPON 
,ti.TIPE_A, ti.TIPE_B, ti.TIPE_C, ti.TIPE_D, ti.KELOMPOK_A, ti.KELOMPOK_B, ti.KELOMPOK_C, ti.KELOMPOK_D,ti.PERSYARATAN, ti.IZIN_DIMINTA, ti.IZIN_DISAHKAN, ti.TGL_DIMINTA, ti.TGL_DISAHKAN, ti.`STATUS`,ti.ANALISA, ti.TINDAKLANJUT, ti.KEPALA, ti.AUDITOR
from tb_ikpi ti										
where ti.ID_ISI='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">Izin Konstruksi Pengendalian Infeksi</li>
				</ul><!-- /.breadcrumb -->

				
			</div>

			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Izin Konstruksi Pengendalian Infeksi</h4>

								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>

									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_ikpi" method="post">
										<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $dt['ID_ISI']; ?>" readonly>	
										<div class="form-group">
											<div class="col-sm-4">											
												<input type="text" name="NOIZIN" value="<?php echo $dt['NOIZIN']; ?>" placeholder="[ NO. IZIN ]" class="form-control" />								
											</div>
											<div class="col-sm-4">											
												<input type="text" name="LOKASI" value="<?php echo $dt['LOKASI']; ?>" placeholder="[ LOKASI KONSTRUKSI ]" class="form-control" />								
											</div>	
											<div class="col-sm-4">											
												<input type="text" name="KOORDINATOR" value="<?php echo $dt['KOORDINATOR']; ?>" placeholder="[ KOORDINATOR PROYEK ]" class="form-control" />								
											</div>	
											<hr>
											<div class="col-sm-4">											
												<input type="text" name="KONTRAKTOR" value="<?php echo $dt['KONTRAKTOR']; ?>" placeholder="[ KONTRAKTOR KERJA ]" class="form-control" />								
											</div>
											
											<div class="col-sm-4">											
												<input type="text" name="SUPERVISOR" value="<?php echo $dt['SUPERVISOR']; ?>" placeholder="[ SUPERVISOR ]" class="form-control" />								
											</div>	
											
											<div class="col-sm-4">
												<div class="input-group">
													<input class="form-control" autocomplete="off" value="<?php echo $dt['TGL_MULAI']; ?>" id="datetimepicker1" placeholder="[ TANGGAL MULAI PROYEK ]" name="TGL_MULAI" type="text" />
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>										
											<br></br>
											<div class="col-sm-4">											
												<input type="text" name="DURASI" value="<?php echo $dt['DURASI']; ?>" placeholder="[ PERKIRAAN DURASI ]" class="form-control" />								
											</div>
											<div class="col-sm-4">
												<div class="input-group">
													<input class="form-control" autocomplete="off" value="<?php echo $dt['TGL_KADALUARSA']; ?>" id="datetimepicker" placeholder="[ TANGGAL IZIN KADALUARSA ]" name="TGL_KADALUARSA" type="text" />
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-4">											
												<input type="text" name="TELEPON" value="<?php echo $dt['TELEPON']; ?>" placeholder="[ TELEPON ]" class="form-control" />								
											</div>	
										</div>			
										
										<hr />
										
										<div class="form-group">
											<div class="col-sm-12">											
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="35%" style="text-align:center">AKTIVITAS KONSTRUKSI</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="35%" style="text-align:center"> KELOMPOK PENGENDALIAN INFEKSI</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td align="center"><label>
																<input type="radio" <?php if($dt['TIPE_A']=='1') echo " checked "?> name="TIPE_A" id="PADAT_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">										
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="TIPE_A" <?php if($dt['TIPE_A']=='2') echo " checked "?> type="radio" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>TIPE A: Inspeksi, aktivitas non-invasif</td>
															<td align="center"><label>
																<input type="radio" <?php if($dt['KELOMPOK_A']=='1') echo " checked "?> name="KELOMPOK_A" id="PADAT_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="KELOMPOK_A" <?php if($dt['KELOMPOK_A']=='2') echo " checked "?> type="radio" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>KELOMPOK 1: Risiko Rendah </td>
														</tr>
														<tr>
															<td align="center"><label>
																<input type="radio" <?php if($dt['TIPE_B']=='1') echo " checked "?> name="TIPE_B" id="PADAT_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="TIPE_B" <?php if($dt['TIPE_B']=='2') echo " checked "?> type="radio" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>TIPE B: Skala kecil, durasi singkat, tingkat sedang sampai tinggi</td>
															<td align="center"><label>
																<input type="radio" <?php if($dt['KELOMPOK_B']=='1') echo " checked "?> name="KELOMPOK_B" id="PADAT_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="KELOMPOK_B" <?php if($dt['KELOMPOK_B']=='2') echo " checked "?> type="radio" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>KELOMPOK 2: Risiko Sedang </td>
														</tr>
														<tr>
															<td align="center"><label>
																<input type="radio" <?php if($dt['TIPE_C']=='1') echo " checked "?> name="TIPE_C" id="PADAT_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="TIPE_C" <?php if($dt['TIPE_C']=='2') echo " checked "?> type="radio" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>TIPE C: Aktivitas menghasilkan debu tingkat sedang sampai tinggi, memerlukan lebih dari 1 shift kerja untuk penyelesaian </td>
															<td align="center"><label>
																<input type="radio" <?php if($dt['KELOMPOK_C']=='1') echo " checked "?> name="KELOMPOK_C" id="PADAT_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="KELOMPOK_C" <?php if($dt['KELOMPOK_C']=='2') echo " checked "?> type="radio" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>KELOMPOK 3: Risiko Tinggi </td>
														</tr>
														<tr>
															<td align="center"><label>
																<input type="radio" <?php if($dt['TIPE_D']=='1') echo " checked "?> name="TIPE_D" id="PADAT_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="TIPE_D" <?php if($dt['TIPE_D']=='2') echo " checked "?> type="radio" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>TIPE D: Durasi lama dan aktivitas konstruksi membutuhkan shift kerja yang berturutan.</td>
															<td align="center"><label>
																<input type="radio" <?php if($dt['KELOMPOK_D']=='1') echo " checked "?> name="KELOMPOK_D" id="PADAT_INFEKSI" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="KELOMPOK_D" <?php if($dt['KELOMPOK_D']=='2') echo " checked "?> type="radio" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>KELOMPOK 4: Risiko sangat Tinggi </td>
														</tr>
														<tr>
															<td colspan="2">
																KELAS I
															</td>
															<td colspan="4">
																1. Melaksanakan kerja dengan metode yang meminimalkan debu dari lokasi konstruksi<br>
																2. Mengganti plafon yang dilepaskan untuk inspeksi sesegera mungkin.<br>
																3. Pembongkaran minor untuk perombakan ulang
															</td>
														</tr>
														<tr>
															<td colspan="2">
																KELAS II
															</td>
															<td colspan="4">
																1.	Menyediakan sarana aktif untuk mencegah debu terbang ke atmosfer. <br>
																2.	Basahi permukaan kerja untuk mengontrol debu saat pemotongan. <br>
																3.	Segel pintu yang tidak terpakai dengan lakban.<br>
																4.	Tutup dan segel ventilasi udara.<br>
																5.	Seka permukaan dengan pembersih/disinfektan. <br>
																6.	Tempatkan sampah konstruksi dalam wadah yang tertutup rapat sebelum dipindahkan.<br> 
																7.	Pel basah dan/atau vakum dengan alat vacuum dengan filter HEPA sebelum meninggalkan area kerja.<br>
																8.	Tempatkan keset di pintu masuk dan keluar area kerja. <br>
																9.	Isolasi sistem HVAC pada lokasi tempat berlangsungnya pekerjaan; kembalikan seperti semula saat pekerjaan selesai. <br>

															</td>
														</tr>
														<tr>
															<td colspan="2">
																KELAS III 
															</td>
															<td colspan="4">
																1.	Dapatkan izin pengendalian infeksi sebelum konstruksi dimulai. <br>
																2.	Isolasi sistem HVAC pada lokasi tempat berlangsungnya pekerjaan untuk mencegah kontaminasi sistem saluran.<br>
																3.	Lengkapi semua barier kritis atau implementasikan metode pengontrolan kubus sebelum konstruksi dimulai.<br>
																4.	 Pertahankan tekanan udara negatif di lokasi kerja menggunakan unit filtrasi udara dengan filter HEPA.<br>
																5.	Jangan menghilangkan barier dari area kerja sampai proyek selesai dan diperiksa oleh Pencegahan dan Pengendalian Infeksi serta dibersihkan secara menyeluruh oleh Layanan Lingkungan.<br>
																6.	Vakum area kerja dengan alat vakum dengan filter HEPA.<br>
																7.	 Pel basah dengan pembersih/disinfektan.<br>
																8.	Buang material barier dengan hati-hati untuk meminimalkan penyebaran kotoran dan debris yang terkait dengan konstruksi.<br>
																9.	Tempatkan sampah konstruksi dalam wadah yang tertutup rapat sebelum dipindahkan.<br>
																10.	Tutupi tempat sampah atau troli yang dipakai untuk transportasi. Plester penutupnya.<br>
																11.	Setelah selesai, kembalikan sistem HVAC seperti semula pada lokasi pekerjaan.

															</td>
														</tr>
														<tr>
															<td colspan="2">
																KELAS IV
															</td>
															<td colspan="4">
																1.	Dapatkan izin pengendalian infeksi sebelum konstruksi dimulai.<br>
																2.	Isolasi sistem HVAC pada lokasi tempat berlangsungnya pekerjaan untuk mencegah kontaminasi sistem saluran.<br>
																3.	Lengkapi semua barier kritis atau implementasikan metode pengontrolan kubus sebelum konstruksi dimulai.<br>
																4.	Pertahankan tekanan udara negatif di lokasi kerja menggunakan unit filtrasi udara dengan filter HEPA.<br>
																5.	Segel lubang, pipa, saluran, atau tusukan dengan benar.<br>
																6.	Buat ruang serambi/anteroom dan pastikan semua personil untuk melewati ruangan ini sehingga mereka dapat divakum menggunakan alat vakum dengan filter HEPA sebelum meninggalkan area kerja atau mereka dapat memakai baju kerja dari kain atau kertas yang dilepaskan setiap kali meninggalkan area kerja.<br>
																7.	Semua personil yang memasukki area kerja diwajibkan untuk memakai penutup sepatu.<br>
																8.	Jangan menghilangkan barier dari area kerja sampai proyek selesai dan diperiksa oleh Pencegahan dan Pengendalian Infeksi serta dibersihkan secara menyeluruh oleh Layanan Lingkungan.<br>
																9.	Vakum area kerja dengan alat vakum dengan filter HEPA.<br>
																10.	Pel basah dengan disinfektan.<br>
																11.	Buang material barier dengan hati-hati untuk meminimalkan penyebaran kotoran dan debris yang terkait dengan konstruksi.<br>
																12.	Tempatkan sampah konstruksi dalam wadah yang tertutup rapat sebelum dipindahkan.<br>
																13.	Tutupi tempat sampah atau troli yang dipakai untuk transportasi. Plester penutupnya.<br>
																14.	Setelah selesai, kembalikan sistem HVAC seperti semula pada lokasi pekerjaan.
															</td>
														</tr>
														<tr>
															<td colspan="6">
																<div class="col-sm-12">											
																	<input type="text" value="<?php echo $dt['PERSYARATAN']; ?>" name="PERSYARATAN" placeholder="[ PERSYARATAN TAMBAHAN ]" class="form-control" />								
																</div>
															</td>
														</tr>
														<tr>
															<td colspan="3">
																<div class="col-sm-12">											
																	<input type="text" value="<?php echo $dt['IZIN_DIMINTA']; ?>" name="IZIN_DIMINTA" placeholder="[ IZIN DIMINTA OLEH ]" class="form-control" />								
																</div>
															</td>
															<td colspan="3">
																<div class="col-sm-12">											
																	<input type="text" value="<?php echo $dt['IZIN_DISAHKAN']; ?>" name="IZIN_DISAHKAN" placeholder="[ IZIN DISAHKAN OLEH ]" class="form-control" />								
																</div>
															</td>
														</tr>
														<tr>
															<td colspan="3">
																<div class="col-sm-12">											
																	<div class="input-group">
																		<input class="form-control" value="<?php echo $dt['TGL_DIMINTA']; ?>" autocomplete="off" id="datetimepicker11" placeholder="[ TANGGAL ]" name="TGL_DIMINTA" type="text" />
																		<span class="input-group-addon">
																			<i class="fa fa-calendar bigger-110"></i>
																		</span>
																	</div>							
																</div>
															</td>
															<td colspan="3">
																<div class="col-sm-12">											
																	<div class="input-group">
																		<input class="form-control" value="<?php echo $dt['TGL_DISAHKAN']; ?>" autocomplete="off" id="datetimepicker12" placeholder="[ TANGGAL ]" name="TGL_DISAHKAN" type="text" />
																		<span class="input-group-addon">
																			<i class="fa fa-calendar bigger-110"></i>
																		</span>
																	</div>							
																</div>
															</td>
														</tr>

													</tbody>
												</table>											
											</div>
										</div>													
										<div class="form-group">										
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
												<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
											</div>				
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
												<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
											</div>
										</div>
										<div class="form-group">			
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
												<input type="text" id="nama" value="<?php echo $dt['KEPALA']; ?>" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
											</div>	
											<div class="col-md-6 col-sm-12">	
												<label class="control-label bolder blue" style="text-decoration: underline">OBSERVATOR</label>				
												<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
											</div>								
										</div>
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													%
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->	
		</div> <!-- container -->
	</div><!-- /.main-content -->
	<?php
}?>