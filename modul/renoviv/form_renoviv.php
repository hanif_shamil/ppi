
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Pemantauan Selama Renov/Konstruksi Bangunan Kelas IV</li>
			</ul><!-- /.breadcrumb -->
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Pemantauan Selama Renov/Konstruksi Bangunan Kelas IV</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_renoviv" method="post">									
									<div class="form-group">										
										<div class="col-sm-6">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" autocomplete="off" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-6">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ AREA RENOVASI ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
													?>
													<option value="<?=$data[0]?>"><?=$data[3]?></option>
													<?php
												}
												?>	
											</select>
										</div>	
										<!-- <div class="col-sm-5">
											<input type="text" id="nomr" name="OBSERVATOR" placeholder="[ OBSERVATOR ]" class="form-control" />												
										</div> -->	
									</div>									
									<hr />
									<h4 class="widget-title">KELAS IV</h4>
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> </th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
														<th width="5%" style="text-align:center">NA</th>
														<th width="30%" style="text-align:center">KETERANGAN</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Mengisolasi sistem HVAC di area kerja untuk mencegah kontaminasi sistem saluran</td>
														<td align="center"><label>
															<input type="radio" name="SATU" id="ISOLASI_HVAC" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
														</label></td>
														<td align="center">
															<label>
																<input name="SATU" type="radio" value="2" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU" type="radio" value="3" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<input type="text" id="nomr" name="KETERANGAN_1" placeholder="" class="form-control" />	
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Siapkan pembatas area kerja atau terapkan metode kontrol kubus (menutup area kerja dengan plastik dan menyegel dengan vakum HEPA untuk menyedot debu keluar) sebelum konstruksi dimulai</td>
														<td align="center"><label>
															<input type="radio" name="DUA" id="VAKUM_HEPA" value="1" class="ace input-lg" onClick="tugas1()">													
															<span class="lbl"></span>
														</label></td>
														<td align="center"><label>
															<input name="DUA" type="radio" value="2" id="VAKUM_HEPATDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="DUA" type="radio" value="3" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<input type="text" id="nomr" name="KETERANGAN_2" placeholder="" class="form-control" />	
													</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Menjaga tekanan udara negatif dalam tempat kerja dengan menggunakan unit penyaringan udara HEPA</td>
													<td align="center">
														<label>
															<input type="radio" name="TIGA" id="UDARA_HEPA" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="TIGA" type="radio" value="2" id="UDARA_HEPATDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="TIGA" type="radio" value="3" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<input type="text" id="nomr" name="KETERANGAN_3" placeholder="" class="form-control" />	
													</td>
												</tr>
												<tr>
													<td>4</td>
													<td>Menyegel lubang, pipa dan saluran</td>
													<td align="center">
														<label>
															<input name="EMPAT" type="radio" value="1" id="WADAH_TUTUP" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="EMPAT" type="radio" value="2" id="WADAH_TUTUPTDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="EMPAT" type="radio" value="3" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<input type="text" id="nomr" name="KETERANGAN_4" placeholder="" class="form-control" />	
													</td>
												</tr>
												<tr>
													<td>5</td>
													<td>Membuat anteroom dan mewajibkan semua personel untuk melewati ruangan ini sehingga mereka dapat disedot menggunakan vacuum cleaner HEPA sebelum meninggalkan  tempat kerja atau mereka bisa memakai pakaian kerja yang lepas setiap kali mereka meninggalkan tempat kerja.</td>
													<td align="center">
														<label>
															<input name="LIMA" type="radio" value="1" id="GEROBAK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="LIMA" type="radio" value="2" id="GEROBAKTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="LIMA" type="radio" value="3" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<input type="text" id="nomr" name="KETERANGAN_5" placeholder="" class="form-control" />	
													</td>
												</tr>
												<tr>
													<td>6</td>
													<td>Semua personel memasuki tempat kerja diwajibkan untuk memakai penutup sepatu. Sepatu harus diganti setiap kali keluar dari area kerja.</td>
													<td align="center">
														<label>
															<input name="ENAM" type="radio" value="1" id="GEROBAK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="ENAM" type="radio" value="2" id="GEROBAKTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="ENAM" type="radio" value="3" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<input type="text" id="nomr" name="KETERANGAN_6" placeholder="" class="form-control" />	
													</td>
												</tr>										
											</tbody>
										</table>											
									</div>
								</div>													
								<div class="form-group">										
									<div class="col-md-6 col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
										<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
									</div>				
									<div class="col-md-6 col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
										<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
									</div>
								</div>
								<div class="form-group">			
									<div class="col-md-6 col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
										<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
									</div>	
									<div class="col-md-6 col-sm-12">	
										<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
										<input type="text" id="nama" name="OBSERVATOR" placeholder="[ OBSERVATOR ]" class="form-control" />
									</div>								
								</div>
								<div class="form-group">							
									<div class="col-md-offset-3 col-md-9">
										<button class="btn btn-info" name="btnsimpan" type="submit">
											<i class="ace-icon fa fa-check bigger-110"></i>
											Submit
										</button>
										&nbsp; &nbsp; &nbsp;
										<button class="btn" type="reset">												
											Reset
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div><!-- /.span -->
		</div>
	</div><!-- /.page-content -->	
</div> <!-- container -->
</div><!-- /.main-content -->