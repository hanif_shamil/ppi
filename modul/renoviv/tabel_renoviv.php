<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Data</a>
		</li>
		<li class="active">Data Formulir Pemantauan Selama Renov/Konstruksi Bangunan Kelas IV</li>
	</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="table-header">
						Data Formulir Pemantauan Selama Renov/Konstruksi Bangunan Kelas IV
					</div>
					<div>
						<table id="tabeldata" class="table table-striped table-bordered table-hover">
							<thead>
								<tr align="center">
									<th width="10%"><div align="center">Tanggal</div></th>									
									<th><div align="center">Area renovasi</div></th>
									<th><div align="center">Observator</div></th>
									<th width="10%"><div align="center">Opsi</div></th>
								</tr>								
								
							</thead>

							<tbody>
								<?php					
								$query="select ti.ID, ti.TGL_SIMPAN, ti.TANGGAL, ti.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, ti.OBSERVATOR, ti.`STATUS`
								from tb_renoviv ti
								left join ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5							
								where ti.`STATUS`=1";							
								$info=mysqli_query($conn1,$query); 
								while($row=mysqli_fetch_array($info)){
									?>
									<tr>
										<td><?php echo $row['TANGGAL'] ?></td>
										<td><?php echo $row['RUANGAN'] ?></td>								
										<td><?php echo $row['OBSERVATOR'] ?></td>						
										<td align="center" >
											<div class="hidden-sm hidden-xs action-buttons">
												<!-- <a class="btn btn-info btn-xs" href="?page=det_renoviv&id=<?php echo $row['ID']?>">
													<i class="glyphicon glyphicon-search"></i>												
												</a> -->
												<a class="btn btn-success btn-xs" href="?page=edit_renoviv&id=<?php echo $row['ID']?>">
													<i class="glyphicon glyphicon-pencil"></i>
												</a>
												<a class="btn btn-danger btn-xs" href="?page=simpan_renoviv&id=<?php echo $row['ID']?>">
													<i class="glyphicon glyphicon-trash"></i>	
												</a>
											</div>
										</td>
									</tr>							
									<?php							
								}
								?>	
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

