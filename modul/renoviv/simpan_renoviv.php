<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$OBSERVATOR		= $_POST['OBSERVATOR'];
						$SATU			= $_POST['SATU'];
						$DUA			= $_POST['DUA'];
						$TIGA			= $_POST['TIGA'];
						$EMPAT			= $_POST['EMPAT'];
						$LIMA			= $_POST['LIMA'];
						$ENAM			= $_POST['ENAM'];
						
						$KETERANGAN_1 	= $_POST['KETERANGAN_1'];
						$KETERANGAN_2 	= $_POST['KETERANGAN_2'];
						$KETERANGAN_3 	= $_POST['KETERANGAN_3'];
						$KETERANGAN_4 	= $_POST['KETERANGAN_4'];
						$KETERANGAN_5 	= $_POST['KETERANGAN_5'];
						$KETERANGAN_6 	= $_POST['KETERANGAN_6'];						
						$USER 			= $_SESSION['userid'];													
						// echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_renoviv (
							TANGGAL,
							RUANGAN,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							OBSERVATOR,
							SATU,
							DUA,
							TIGA,
							EMPAT,
							LIMA,
							KETERANGAN_1,
							KETERANGAN_2,
							KETERANGAN_3,
							KETERANGAN_4,
							KETERANGAN_5,
							KETERANGAN_6,
							ENAM,
							USER) 
						VALUES 
						('$TANGGAL',
							'$RUANGAN',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$OBSERVATOR',					
							'$SATU',
							'$DUA',	
							'$TIGA',		
							'$EMPAT',				
							'$LIMA',
							'$KETERANGAN_1',
							'$KETERANGAN_2',
							'$KETERANGAN_3',
							'$KETERANGAN_4',
							'$KETERANGAN_5',
							'$KETERANGAN_6',
							'$ENAM',						
							'$USER')";
						// echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_renoviv'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID			= $_POST['ID'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$OBSERVATOR 	= $_POST['OBSERVATOR'];
						$SATU			= $_POST['SATU'];
						$DUA			= $_POST['DUA'];
						$TIGA			= $_POST['TIGA'];
						$EMPAT			= $_POST['EMPAT'];
						$LIMA			= $_POST['LIMA'];
						$ENAM			= $_POST['ENAM'];
						$KETERANGAN_1 	= $_POST['KETERANGAN_1'];
						$KETERANGAN_2 	= $_POST['KETERANGAN_2'];
						$KETERANGAN_3 	= $_POST['KETERANGAN_3'];
						$KETERANGAN_4 	= $_POST['KETERANGAN_4'];
						$KETERANGAN_5 	= $_POST['KETERANGAN_5'];
						$KETERANGAN_6 	= $_POST['KETERANGAN_6'];								
						$USER 			= $_SESSION['userid'];			
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_renoviv SET												
						TANGGAL			='$TANGGAL',
						RUANGAN			='$RUANGAN',
						ANALISA = '$ANALISA', 
						TINDAKLANJUT = '$TINDAKLANJUT', 
						KEPALA = '$KEPALA',
						OBSERVATOR		='$OBSERVATOR',
						SATU			='$SATU',
						DUA				='$DUA',
						TIGA			='$TIGA',
						EMPAT			='$EMPAT',
						LIMA			='$LIMA',
						KETERANGAN_1 	= '$KETERANGAN_1',
						KETERANGAN_2 	= '$KETERANGAN_2',
						KETERANGAN_3 	= '$KETERANGAN_3',
						KETERANGAN_4 	= '$KETERANGAN_4',
						KETERANGAN_5 	= '$KETERANGAN_5',
						KETERANGAN_6 	= '$KETERANGAN_6',
						ENAM			= '$ENAM',
						DIUBAH_OLEH		= '$USER'
						where ID		= '$ID'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_renoviv'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_renoviv ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_renoviv'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_renoviv'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			