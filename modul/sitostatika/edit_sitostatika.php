<script type="text/javascript">

function tugas1()
{	
var ya = $('#AA:checked').length;
var tidak = $('#SS:checked').length;
document.getElementById("total").value= ya/(ya+tidak)*100;	
}
</script>

<?php
$id=($_GET['id']);
$query="SELECT * FROM db_ppi.tb_sitostatika where ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
?>
<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="ace-icon fa fa-home home-icon"></i>
<a href="#">Home</a>
</li>
<li>
<a href="#">Forms</a>
</li>
<li class="active">Formulir Monitoring Tindakan Intra Tekal Pemberian Obat Sitostika</li>
</ul><!-- /.breadcrumb -->
</div>
<div class="page-content">
<div class="row">
<div class="col-xs-12 col-sm-12">
<div class="widget-box">
	<div class="widget-header">
		<h4 class="widget-title">Formulir Monitoring Tindakan Intra Tekal Pemberian Obat Sitostika</h4>
		<div class="widget-toolbar">
			<a href="#" data-action="collapse">
				<i class="ace-icon fa fa-chevron-up"></i>
			</a>
			<a href="#" data-action="close">
				<i class="ace-icon fa fa-times"></i>
			</a>
		</div>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<form class="form-horizontal" id="sample-form" action="index.php?page=update_sitostatika" method="post">	
			<input type="hidden" value="<?php echo $dt['ID']; ?>"  name="ID" >	
				<div class="form-group">
					<div class="col-sm-3">
						<input type="text" id="nomr" name="NOMR" value="<?php echo $dt['NOMR']; ?>"  placeholder="[ MR ]" class="form-control" onkeyup="autofill()" />
					</div>	
					<div class="col-sm-6">									
						<input type="text" id="nama" name="NAMA" value="<?php echo $dt['NAMA']; ?>"  placeholder="[ NAMA ]" class="form-control" />												
					</div>
					<div class="col-sm-3">									
						<input type="text" id="umur" name="UMUR" value="<?php echo $dt['UMUR']; ?>"  placeholder="[ UMUR ]" class="form-control" />												
					</div>	
				</div>	
				<div class="form-group">						
					<div class="col-sm-3">
						<input type="text" id="diagnosa" name="DIAGNOSA" value="<?php echo $dt['DIAGNOSA']; ?>"  placeholder="[ DIAGNOSA ]" class="form-control">										
					</div>	
					<div class="col-sm-6">									
						<input type="text" id="dpjp" name="DOKTER" value="<?php echo $dt['DOKTER']; ?>"  placeholder="[ DOKTER AUDITEE ]" class="form-control" />												
					</div>

					<div class="col-sm-3">									
						<div class="input-group">
							<input class="form-control" value="<?php echo date('Y/m/d') ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
							<span class="input-group-addon">
								<i class="fa fa-calendar bigger-110"></i>
							</span>
						</div>												
					</div>
				</div>

				<div class="form-group">																				
					<div class="col-sm-12">
						<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">

							<table class="table table-bordered">
								<thead>
									<tr>
										<th width="5%" style="text-align:center">NO</th>
										<th style="text-align:center"> KEGIATAN</th>
										<th width="5%" style="text-align:center">YA</th>
										<th width="5%" style="text-align:center">TIDAK</th>
										<th width="5%" style="text-align:center">NA</th>
										<th width="30%" style="text-align:center">KETERANGAN</th>
									</tr>
								</thead>
								<tbody>														
									<tr>
										<td>1</td>
										<td>Melakukan Kebersihan tangan</td>
										<td align="center">
											<label><input type="radio" name="SATU" <?php if($dt['SATU']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">					
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="SATU" type="radio" <?php if($dt['SATU']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="SATU" type="radio" <?php if($dt['SATU']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" value="<?php echo $dt['KET_SATU']; ?>"  name="KET_SATU" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td>2</td>
										<td>Menggunakan APD sesuai indikasi :</td>
										<td align="center">
										</td>
										<td align="center">
										</td>
										<td>
										</td>
									</tr>
									<tr>
										<td></td>
										<td>a. Sarung tangan steril</td>
										<td align="center">
											<label>
												<input type="radio" name="DUA_A" <?php if($dt['DUA_A']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input type="radio" name="DUA_A" <?php if($dt['DUA_A']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input type="radio" name="DUA_A" <?php if($dt['DUA_A']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" value="<?php echo $dt['KET_DUA_A']; ?>"  name="KET_DUA_A" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td></td>
										<td>b. Masker bedah</td>
										<td align="center">
											<label>
												<input name="DUA_B" type="radio" <?php if($dt['DUA_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="DUA_B" type="radio" <?php if($dt['DUA_B']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="DUA_B" type="radio" <?php if($dt['DUA_B']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" value="<?php echo $dt['KET_DUA_B']; ?>"  name="KET_DUA_B" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td></td>
										<td>c. google</td>
										<td align="center">
											<label>
												<input name="DUA_C" type="radio" <?php if($dt['DUA_C']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="DUA_C" type="radio" <?php if($dt['DUA_C']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="DUA_C" type="radio" <?php if($dt['DUA_C']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" value="<?php echo $dt['KET_DUA_C']; ?>"  name="KET_DUA_C" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td></td>
										<td>d. Apron / Gaun</td>
										<td align="center">
											<label>
												<input name="DUA_D" type="radio" <?php if($dt['DUA_D']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="DUA_D" type="radio" <?php if($dt['DUA_D']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="DUA_D" type="radio" <?php if($dt['DUA_D']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" value="<?php echo $dt['KET_DUA_D']; ?>"  name="KET_DUA_D" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td></td>
										<td>e.  Topi</td>
										<td align="center">
											<label>
												<input name="DUA_E" type="radio" <?php if($dt['DUA_E']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="DUA_E" type="radio" <?php if($dt['DUA_E']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="DUA_E" type="radio" <?php if($dt['DUA_E']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" value="<?php echo $dt['KET_DUA_E']; ?>"  name="KET_DUA_E" placeholder="" class="form-control" />		
										</td>
									</tr>	

									<tr>
										<td>3</td>
										<td>Skin preparation dengan chlorhexidin 2% mix Alkohol 70%, tunggu sampai 20 detik sebelum insersi</td>
										<td align="center"><label>
											<input type="radio" name="TIGA" id="AA" <?php if($dt['TIGA']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">											
											<span class="lbl"></span>
										</label></td>

										<td align="center">
											<label>
												<input name="TIGA" type="radio" <?php if($dt['TIGA']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td align="center">
											<label>
												<input name="TIGA" type="radio" <?php if($dt['TIGA']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
												<span class="lbl"></span>
											</label>
										</td>
										<td>
											<input type="text" id="nama" value="<?php echo $dt['KET_TIGA']; ?>"  name="KET_TIGA" placeholder="" class="form-control" />		
										</td>
									</tr>
									<tr>
										<td>4</td>
										<td>Lokasi Penusukan Sesuai/ Tepat</td>
										<td align="center"><label>
											<input type="radio" name="EMPAT" id="AA" <?php if($dt['EMPAT']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">		
											<span class="lbl"></span>
										</label>
									</td>
									<td align="center"><label>
										<input type="radio" name="EMPAT" id="SS" <?php if($dt['EMPAT']=='2') echo " checked "?> value="2" class="ace input-lg" onClick="tugas1()">
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center"><label>
										<input type="radio" name="EMPAT" id="AA" <?php if($dt['EMPAT']=='3') echo " checked "?> value="3" class="ace input-lg" onClick="tugas1()">
										<span class="lbl"></span>
									</label>
								</td>
								<td>
									<input type="text" id="nama" value="<?php echo $dt['KET_EMPAT']; ?>"  name="KET_EMPAT" placeholder="" class="form-control" />		
								</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Menutup area penusukan dengan menggunakan kassa steril dan transparant dresssing</td>
								<td align="center"><label>
									<input type="radio" name="LIMA" id="AA" <?php if($dt['LIMA']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">			
									<span class="lbl"></span>
								</label></td>
								<td align="center">
									<label>
										<input name="LIMA" type="radio" value="2" <?php if($dt['LIMA']=='2') echo " checked "?> id="SS" class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td align="center">
									<label>
										<input name="LIMA" type="radio" value="3" <?php if($dt['LIMA']=='3') echo " checked "?> id="AA" class="ace input-lg" onClick="tugas1()" />
										<span class="lbl"></span>
									</label>
								</td>
								<td>
									<input type="text" id="nama" value="<?php echo $dt['KET_LIMA']; ?>"  name="KET_LIMA" placeholder="" class="form-control" />		
								</td>
							</tr>											
						</tbody>
					</table>																
				</div>
			</div>
		</div>														
		<div class="form-group">										
			<div class="col-md-6 col-sm-12">
				<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
				<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
			</div>				
			<div class="col-md-6 col-sm-12">
				<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
				<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
			</div>
		</div>
		<div class="form-group">			
			<div class="col-md-6 col-sm-12">
				<label class="control-label bolder blue" style="text-decoration: underline">Kepala Unit/Kepala Ruangan</label>	
				<input type="text" id="nama" value="<?php echo $dt['KEPALA']; ?>"  name="KEPALA" placeholder="[ Kepala Unit/Kepala Ruangan ]" class="form-control" />		
			</div>	
			<div class="col-md-6 col-sm-12">	
				<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
				<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>"  name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
			</div>								
		</div>																											
		<hr />
		<div class="form-group">							
			<div class="col-md-offset-3 col-md-9">
				<button class="btn btn-info" name="btnEdit" type="submit">
					<i class="ace-icon fa fa-check bigger-110"></i>
					Submit
				</button>
				&nbsp; &nbsp; &nbsp;
				<button class="btn" type="reset">
					<i class="ace-icon fa fa-undo bigger-110"></i>
					Reset
				</button>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div><!-- /.span -->
</div>
</div><!-- /.page-content -->	
<?php }
?>