<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					$TANGGAL 			= $_POST['TANGGAL'];									
					$NOMR				= $_POST['NOMR'];
					$NAMA				= addslashes($_POST['NAMA']);
					$UMUR				= $_POST['UMUR'];
					$DOKTER				= $_POST['DOKTER'];
					$DIAGNOSA			= $_POST['DIAGNOSA'];	
					$SATU 				= $_POST['SATU'];
					$KET_SATU				= $_POST['KET_SATU']; 
					$DUA_A					= $_POST['DUA_A'];
					$KET_DUA_A			= $_POST['KET_DUA_A']; 
					$DUA_B					= $_POST['DUA_B'];
					$KET_DUA_B 			= $_POST['KET_DUA_B'];	
					$DUA_C					= $_POST['DUA_C'];
					$KET_DUA_C				= $_POST['KET_DUA_C'];  
					$DUA_D 				= $_POST['DUA_D'];
					$KET_DUA_D			= $_POST['KET_DUA_D'];
					$DUA_E					= $_POST['DUA_E'];
					$KET_DUA_E 			= $_POST['KET_DUA_E'];  
					$TIGA 				= $_POST['TIGA'];
					$KET_TIGA				= $_POST['KET_TIGA'];  
					$EMPAT 				= $_POST['EMPAT'];
					$KET_EMPAT 			=	$_POST['KET_EMPAT'];  
					$LIMA 				=	$_POST['LIMA'];
					$KET_LIMA			=	$_POST['KET_LIMA'];   
					$ANALISA 			=	$_POST['ANALISA'];  
					$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
					$KEPALA 			=	$_POST['KEPALA'];  
					$AUDITOR 			=	addslashes($_POST['AUDITOR']); 
					$pengisi 			= $_SESSION['userid'];							
						//echo "<pre>";print_r($_POST);exit();                	
					$query = "INSERT INTO db_ppi.tb_sitostatika (
					USER,
					NOMR,
					NAMA,
					UMUR,
					DIAGNOSA,
					DOKTER,
					TANGGAL,
					SATU,
					KET_SATU, 
					DUA_A,
					KET_DUA_A, 
					DUA_B,
					KET_DUA_B, 
					DUA_C,
					KET_DUA_C, 
					DUA_D,
					KET_DUA_D, 
					DUA_E,
					KET_DUA_E, 
					TIGA,
					KET_TIGA, 
					EMPAT,
					KET_EMPAT, 
					LIMA,
					KET_LIMA, 
					ANALISA,
					TINDAKLANJUT,
					KEPALA,
					AUDITOR) 
					VALUES 
					('$pengisi',
					'$NOMR',
					'$NAMA',
					'$UMUR',
					'$DIAGNOSA',
					'$DOKTER',
					'$TANGGAL',
					'$SATU',
					'$KET_SATU', 
					'$DUA_A',
					'$KET_DUA_A', 
					'$DUA_B',
					'$KET_DUA_B', 
					'$DUA_C',
					'$KET_DUA_C', 
					'$DUA_D',
					'$KET_DUA_D', 
					'$DUA_E',
					'$KET_DUA_E', 
					'$TIGA',
					'$KET_TIGA', 
					'$EMPAT',
					'$KET_EMPAT', 
					'$LIMA',
					'$KET_LIMA', 
					'$ANALISA',
					'$TINDAKLANJUT',
					'$KEPALA',
					'$AUDITOR')";
					//echo $query; die();	
					$hasil=mysqli_query($conn1,$query);								
					if($hasil){
						echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_sitostatika'</script>"; 
					}                     
					else{
						echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
					}                         
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			