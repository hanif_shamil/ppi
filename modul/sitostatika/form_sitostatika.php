<script type="text/javascript">

	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Monitoring Tindakan Intra Tekal Pemberian Obat Sitostika</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Monitoring Tindakan Intra Tekal Pemberian Obat Sitostika</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_sitostatika" method="post">		
									<div class="form-group">
										<div class="col-sm-3">
											<input type="text" id="nomr" name="NOMR" placeholder="[ MR ]" class="form-control" onkeyup="autofill()" />
										</div>	
										<div class="col-sm-6">									
											<input type="text" id="nama" name="NAMA" placeholder="[ NAMA ]" class="form-control" />												
										</div>
										<div class="col-sm-3">									
											<input type="text" id="umur" name="UMUR" placeholder="[ UMUR ]" class="form-control" />												
										</div>	
									</div>	
									<div class="form-group">						
										<div class="col-sm-3">
											<input type="text" id="diagnosa" name="DIAGNOSA" placeholder="[ DIAGNOSA ]" class="form-control">										
										</div>	
										<div class="col-sm-6">									
											<input type="text" id="dpjp" name="DOKTER" placeholder="[ DOKTER AUDITEE ]" class="form-control" />												
										</div>

										<div class="col-sm-3">									
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d') ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>												
										</div>
									</div>

									<div class="form-group">																				
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">

												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> KEGIATAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="5%" style="text-align:center">NA</th>
															<th width="30%" style="text-align:center">KETERANGAN</th>
														</tr>
													</thead>
													<tbody>														
														<tr>
															<td>1</td>
															<td>Melakukan Kebersihan tangan</td>
															<td align="center">
																<label><input type="radio" name="SATU" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="SATU" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="SATU" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_SATU" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Menggunakan APD sesuai indikasi :</td>
															<td align="center">
															</td>

															<td align="center">
															</td>
															<td>

															</td>
														</tr>
														<tr>
															<td></td>
															<td>a. Sarung tangan steril</td>
															<td align="center">
																<label>
																	<input type="radio" name="DUA_A" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="DUA_A" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="DUA_A" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_DUA_A" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td></td>
															<td>b. Masker bedah</td>
															<td align="center">
																<label>
																	<input name="DUA_B" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_B" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_B" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_DUA_B" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td></td>
															<td>c. google</td>
															<td align="center">
																<label>
																	<input name="DUA_C" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_C" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_C" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_DUA_C" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td></td>
															<td>d. Apron / Gaun</td>
															<td align="center">
																<label>
																	<input name="DUA_D" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_D" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_D" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_DUA_D" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td></td>
															<td>e.  Topi</td>
															<td align="center">
																<label>
																	<input name="DUA_E" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_E" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_E" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_DUA_E" placeholder="" class="form-control" />		
															</td>
														</tr>	
														<tr>
															<td>3</td>
															<td>Skin preparation dengan chlorhexidin 2% mix Alkohol 70%, tunggu sampai 20 detik sebelum insersi</td>
															<td align="center"><label>
																<input type="radio" name="TIGA" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="TIGA" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="TIGA" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_TIGA" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Lokasi Penusukan Sesuai/ Tepat</td>
															<td align="center"><label>
																<input type="radio" name="EMPAT" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center"><label>
															<input type="radio" name="EMPAT" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center"><label>
															<input type="radio" name="EMPAT" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
														</label>
													</td>
													<td>
														<input type="text" id="nama" name="KET_EMPAT" placeholder="" class="form-control" />		
													</td>
												</tr>
												<tr>
													<td>5</td>
													<td>Menutup area penusukan dengan menggunakan kassa steril dan transparant dresssing</td>
													<td align="center"><label>
														<input type="radio" name="LIMA" id="AA" value="1" class="ace input-lg" onClick="tugas1()">			
														<span class="lbl"></span>
													</label></td>
													<td align="center">
														<label>
															<input name="LIMA" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="LIMA" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td>
														<input type="text" id="nama" name="KET_LIMA" placeholder="" class="form-control" />		
													</td>
												</tr>											
											</tbody>
										</table>																
									</div>
								</div>
							</div>														
							<div class="form-group">										
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
									<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
								</div>				
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
									<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
								</div>
							</div>
							<div class="form-group">			
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">Kepala Unit/Kepala Ruangan</label>	
									<input type="text" id="nama" name="KEPALA" placeholder="[ Kepala Unit/Kepala Ruangan ]" class="form-control" />		
								</div>	
								<div class="col-md-6 col-sm-12">	
									<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
									<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
								</div>								
							</div>																											
							<hr />
							<div class="form-group">							
								<div class="col-md-offset-3 col-md-9">
									<button class="btn btn-info" name="btnsimpan" type="submit">
										<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
										<i class="ace-icon fa fa-undo bigger-110"></i>
										Reset
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><!-- /.span -->
	</div>
</div><!-- /.page-content -->	