<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					if(isset($_POST['btnsimpan'])){
						$TANGGAL 			= $_POST['TANGGAL'];									
						$A1 				= $_POST['A1'];
						$KET_A1				= $_POST['KET_A1']; 
						$A2					= $_POST['A2'];
						$KET_A2				= $_POST['KET_A2']; 
						$A3					= $_POST['A3'];
						$KET_A3 			= $_POST['KET_A3'];	
						$A4					= $_POST['A4'];
						$KET_A4				= $_POST['KET_A4'];  
						$A5 				= $_POST['A5'];
						$KET_A5 			= $_POST['KET_A5'];
						$A6					= $_POST['A6'];
						$KET_A6 			= $_POST['KET_A6'];   
						$A7					= $_POST['A7'];
						$KET_A7				= $_POST['KET_A7'];   
						$B1 				= $_POST['B1'];
						$KET_B1 			=	$_POST['KET_B1'];  
						$B2 				=	$_POST['B2'];
						$KET_B2 			=	$_POST['KET_B2'];  
						$B3 				=	$_POST['B3'];
						$KET_B3 			=	$_POST['KET_B3'];  
						$B4 				=	$_POST['B4'];
						$KET_B4 			=	$_POST['KET_B4'];  
						$B5 				=	$_POST['B5'];
						$KET_B5 			=	$_POST['KET_B5'];  
						$B6 				=	$_POST['B6'];
						$KET_B6 			=	$_POST['KET_B6'];  
						
						$C1A 				=	$_POST['C1A'];
						$KET_C1A 			=	$_POST['KET_C1A'];  
						$C1B 				=	$_POST['C1B'];
						$KET_C1B			=	$_POST['KET_C1B'];  
						$C1C 				=	$_POST['C1C']; 
						$KET_C1C 			=	$_POST['KET_C1C']; 
						$C1D 				=	$_POST['C1D']; 
						$KET_C1D 			=	$_POST['KET_C1D']; 
						$C1E 				=	$_POST['C1E']; 
						$KET_C1E 			=	$_POST['KET_C1E']; 
						$C2A 				=	$_POST['C2A'];
						$KET_C2A 			=	$_POST['KET_C2A'];  
						$C2B 				=	$_POST['C2B'];
						$KET_C2B			=	$_POST['KET_C2B']; 
						$D1 				=	$_POST['D1'];
						$KET_D1 			=	$_POST['KET_D1']; 
						$D2 				=	$_POST['D2'];
						$KET_D2 			=	$_POST['KET_D2']; 
						$D3 				=	$_POST['D3'];
						$KET_D3 			=	$_POST['KET_D3']; 
						$D4 				=	$_POST['D4'];
						$KET_D4 			=	$_POST['KET_D4']; 
						$D5 				=	$_POST['D5'];
						$KET_D5 			=	$_POST['KET_D5']; 
						$D6 				=	$_POST['D6'];
						$KET_D6 			=	$_POST['KET_D6']; 
						$D7 				=	$_POST['D7'];
						$KET_D7 			=	$_POST['KET_D7']; 
						$E1 				=	$_POST['E1'];
						$KET_E1 			=	$_POST['KET_E1']; 
						$E2 				=	$_POST['E2'];
						$KET_E2 			=	$_POST['KET_E2']; 
						$E3 				=	$_POST['E3'];
						$KET_E3 			=	$_POST['KET_E3']; 
						$E4 				=	$_POST['E4'];
						$KET_E4 			=	$_POST['KET_E4']; 
						$E5 				=	$_POST['E5'];
						$KET_E5 			=	$_POST['KET_E5']; 
						$F1 				=	$_POST['F1'];
						$KET_F1 			=	$_POST['KET_F1']; 
						$F2 				=	$_POST['F2'];
						$KET_F2 			=	$_POST['KET_F2']; 
						$F3 				=	$_POST['E3'];
						$KET_F3 			=	$_POST['KET_F3'];
						$NILAI 				=	$_POST['NILAI'];  
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KOORDINATOR 		=	$_POST['KOORDINATOR'];  
						$AUDITOR 			=	addslashes($_POST['AUDITOR']); 
						$pengisi 			= $_SESSION['userid'];							
						//echo "<pre>";print_r($_POST);exit();                	
						$query = "INSERT INTO db_ppi.tb_gizi_new (
						USER,
						TANGGAL,
						A1,
						KET_A1, 
						A2,
						KET_A2, 
						A3,
						KET_A3, 
						A4,
						KET_A4, 
						A5,
						KET_A5, 
						A6,
						KET_A6, 
						A7,
						KET_A7, 
						B1,
						KET_B1, 
						B2,
						KET_B2, 
						B3,
						KET_B3, 
						B4,
						KET_B4, 
						B5,
						KET_B5, 
						B6,
						KET_B6, 
						C1A,
						KET_C1A, 
						C1B,
						KET_C1B, 
						C1C,
						KET_C1C, 
						C1D,
						KET_C1D, 
						C1E,
						KET_C1E, 
						C2A,
						KET_C2A, 
						C2B,
						KET_C2B, 
						D1,
						KET_D1, 
						D2,
						KET_D2, 
						D3,
						KET_D3, 
						D4,
						KET_D4,
						D5,
						KET_D5, 
						D6,
						KET_D6,
						D7,
						KET_D7, 
						
						E1,
						KET_E1, 
						E2,
						KET_E2, 
						E3,
						KET_E3, 
						E4,
						KET_E4,
						E5,
						KET_E5, 
						
						F1,
						KET_F1, 
						F2,
						KET_F2, 
						F3,
						KET_F3, 
						
						
						NILAI, 
						ANALISA,
						TINDAKLANJUT,
						KOORDINATOR,
						AUDITOR) 
						VALUES 
						('$pengisi',
						'$TANGGAL', 
						'$A1',  
						'$KET_A1',  
						'$A2',  
						'$KET_A2',  
						'$A3',  
						'$KET_A3',  
						'$A4',  
						'$KET_A4',  
						'$A5',  
						'$KET_A5',  
						'$A6',  
						'$KET_A6',  
						'$A7', 
						'$KET_A7',  
						'$B1', 						
						'$KET_B1',  
						'$B2',  
						'$KET_B2',  
						'$B3',  
						'$KET_B3',  
						'$B4',  
						'$KET_B4',  
						'$B5',  
						'$KET_B5',  
						'$B6',  
						'$KET_B6',  
						'$C1A', 
						'$KET_C1A', 
						'$C1B', 
						'$KET_C1B',  
						'$C1C', 
						'$KET_C1C',  
						'$C1D', 
						'$KET_C1D',  
						'$C1E', 
						'$KET_C1E',  
						'$C2A', 
						'$KET_C2A',  
						'$C2B', 
						'$KET_C2B',  
						'$D1', 
						'$KET_D1',  
						'$D2', 
						'$KET_D2',  
						'$D3', 
						'$KET_D3', 
						'$D4', 
						'$KET_D4', 
						'$D5', 
						'$KET_D5',  
						'$D6', 
						'$KET_D6', 
						'$D7', 
						'$KET_D7',  
						
						'$E1', 
						'$KET_E1',  
						'$E2', 
						'$KET_E2',  
						'$E3', 
						'$KET_E3',  
						'$E4', 
						'$KET_E4', 
						'$E5', 
						'$KET_E5',  
						
						'$F1', 
						'$KET_F1',  
						'$F2', 
						'$KET_F2',  
						'$F3', 
						'$KET_F3',  
						
						'$NILAI',  
						'$ANALISA',  
						'$TINDAKLANJUT', 
						'$KOORDINATOR', 
						'$AUDITOR')";
						//echo $query; die();	
						$hasil=mysqli_query($conn1,$query);								
						if($hasil){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_gizi_new'</script>"; 
						}                     
					else{
						echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
					}   
					}					
					if(isset($_POST['btnEdit'])){
						$ID			= $_POST['ID'];
						$TANGGAL 			= $_POST['TANGGAL'];									
						$A1 				= $_POST['A1'];
						$KET_A1				= $_POST['KET_A1']; 
						$A2					= $_POST['A2'];
						$KET_A2				= $_POST['KET_A2']; 
						$A3					= $_POST['A3'];
						$KET_A3 			= $_POST['KET_A3'];	
						$A4					= $_POST['A4'];
						$KET_A4				= $_POST['KET_A4'];  
						$A5 				= $_POST['A5'];
						$KET_A5 			= $_POST['KET_A5'];
						$A6					= $_POST['A6'];
						$KET_A6 			= $_POST['KET_A6'];   
						$A7					= $_POST['A7'];
						$KET_A7				= $_POST['KET_A7'];   
						$B1 				= $_POST['B1'];
						$KET_B1 			=	$_POST['KET_B1'];  
						$B2 				=	$_POST['B2'];
						$KET_B2 			=	$_POST['KET_B2'];  
						$B3 				=	$_POST['B3'];
						$KET_B3 			=	$_POST['KET_B3'];  
						$B4 				=	$_POST['B4'];
						$KET_B4 			=	$_POST['KET_B4'];  
						$B5 				=	$_POST['B5'];
						$KET_B5 			=	$_POST['KET_B5'];  
						$B6 				=	$_POST['B6'];
						$KET_B6 			=	$_POST['KET_B6'];  
						
						$C1A 				=	$_POST['C1A'];
						$KET_C1A 			=	$_POST['KET_C1A'];  
						$C1B 				=	$_POST['C1B'];
						$KET_C1B			=	$_POST['KET_C1B'];  
						$C1C 				=	$_POST['C1C']; 
						$KET_C1C 			=	$_POST['KET_C1C']; 
						$C1D 				=	$_POST['C1D']; 
						$KET_C1D 			=	$_POST['KET_C1D']; 
						$C1E 				=	$_POST['C1E']; 
						$KET_C1E 			=	$_POST['KET_C1E']; 
						$C2A 				=	$_POST['C2A'];
						$KET_C2A 			=	$_POST['KET_C2A'];  
						$C2B 				=	$_POST['C2B'];
						$KET_C2B			=	$_POST['KET_C2B']; 
						$D1 				=	$_POST['D1'];
						$KET_D1 			=	$_POST['KET_D1']; 
						$D2 				=	$_POST['D2'];
						$KET_D2 			=	$_POST['KET_D2']; 
						$D3 				=	$_POST['D3'];
						$KET_D3 			=	$_POST['KET_D3']; 
						$D4 				=	$_POST['D4'];
						$KET_D4 			=	$_POST['KET_D4']; 
						$D5 				=	$_POST['D5'];
						$KET_D5 			=	$_POST['KET_D5']; 
						$D6 				=	$_POST['D6'];
						$KET_D6 			=	$_POST['KET_D6']; 
						$D7 				=	$_POST['D7'];
						$KET_D7 			=	$_POST['KET_D7']; 
						$E1 				=	$_POST['E1'];
						$KET_E1 			=	$_POST['KET_E1']; 
						$E2 				=	$_POST['E2'];
						$KET_E2 			=	$_POST['KET_E2']; 
						$E3 				=	$_POST['E3'];
						$KET_E3 			=	$_POST['KET_E3']; 
						$E4 				=	$_POST['E4'];
						$KET_E4 			=	$_POST['KET_E4']; 
						$E5 				=	$_POST['E5'];
						$KET_E5 			=	$_POST['KET_E5']; 
						$F1 				=	$_POST['F1'];
						$KET_F1 			=	$_POST['KET_F1']; 
						$F2 				=	$_POST['F2'];
						$KET_F2 			=	$_POST['KET_F2']; 
						$F3 				=	$_POST['E3'];
						$KET_F3 			=	$_POST['KET_F3'];
						$NILAI 				=	$_POST['NILAI'];  
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KOORDINATOR 		=	$_POST['KOORDINATOR'];  
						$AUDITOR 			=	addslashes($_POST['AUDITOR']); 						
						$USER 				= $_SESSION['userid'];		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_gizi_new SET												
						TANGGAL 			= '$TANGGAL',									
						A1 				= '$A1',
						KET_A1				= '$KET_A1',
						A2					= '$A2',
						KET_A2				= '$KET_A2',
						A3					= '$A3',
						KET_A3 			= '$KET_A3',	
						A4					= '$A4',
						KET_A4				= '$KET_A4', 
						A5 				= '$A5',
						KET_A5 			= '$KET_A5',
						A6					= '$A6',
						KET_A6 			= '$KET_A6',  
						A7					= '$A7',
						KET_A7				= '$KET_A7',  
						B1 				= '$B1',
						KET_B1 			=	'$KET_B1', 
						B2 				=	'$B2',
						KET_B2 			=	'$KET_B2', 
						B3 				=	'$B3',
						KET_B3 			=	'$KET_B3', 
						B4 				=	'$B4',
						KET_B4 			=	'$KET_B4', 
						B5 				=	'$B5',
						KET_B5 			=	'$KET_B5', 
						B6 				=	'$B6',
						KET_B6 			=	'$KET_B6', 

						C1A 				=	'$C1A',
						KET_C1A 			=	'$KET_C1A', 
						C1B 				=	'$C1B',
						KET_C1B			=	'$KET_C1B', 
						C1C 				=	'$C1C',
						KET_C1C 			=	'$KET_C1C',
						C1D 				=	'$C1D',
						KET_C1D 			=	'$KET_C1D',
						C1E 				=	'$C1E',
						KET_C1E 			=	'$KET_C1E',
						C2A 				=	'$C2A',
						KET_C2A 			=	'$KET_C2A', 
						C2B 				=	'$C2B',
						KET_C2B			=	'$KET_C2B',
						D1 				=	'$D1',
						KET_D1 			=	'$KET_D1',
						D2 				=	'$D2',
						KET_D2 			=	'$KET_D2',
						D3 				=	'$D3',
						KET_D3 			=	'$KET_D3',
						D4 				=	'$D4',
						KET_D4 			=	'$KET_D4',
						D5 				=	'$D5',
						KET_D5 			=	'$KET_D5',
						D6 				=	'$D6',
						KET_D6 			=	'$KET_D6',
						D7 				=	'$D7',
						KET_D7 			=	'$KET_D7',
						E1 				=	'$E1',
						KET_E1 			=	'$KET_E1',
						E2 				=	'$E2',
						KET_E2 			=	'$KET_E2',
						E3 				=	'$E3',
						KET_E3 			=	'$KET_E3',
						E4 				=	'$E4',
						KET_E4 			=	'$KET_E4',
						E5 				=	'$E5',
						KET_E5 			=	'$KET_E5',
						F1 				=	'$F1',
						KET_F1 			=	'$KET_F1',
						F2 				=	'$F2',
						KET_F2 			=	'$KET_F2',
						F3 				=	'$E3',
						KET_F3 			=	'$KET_F3',
						NILAI 				=	'$NILAI', 
						ANALISA 			=	'$ANALISA', 
						TINDAKLANJUT 		=	'$TINDAKLANJUT', 
						KOORDINATOR 		=	'$KOORDINATOR', 
						AUDITOR 			=	'$AUDITOR',						
						DIUBAH_OLEH='$USER'
						where ID='$ID'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_gizi_new'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_gizi_new ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_gizi_new'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_gizi_new'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			