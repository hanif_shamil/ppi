<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>

<?php
$id=($_GET['id']);
$query="select * from db_ppi.tb_gizi_new where ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">FORMULIR AUDIT  INSTALASI GIZI</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">FORMULIR AUDIT  INSTALASI GIZI</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_gizi_new" method="post">
								<input type="hidden" value="<?php echo $dt['ID']; ?>"  name="ID" >										
									<div class="form-group">
										<div class="col-sm-1">	
											<label class="control-label bolder blue">TANGGAL</label>
										</div>								
										<div class="col-sm-3">									
											<div class="input-group">
												<input class="form-control" value="<?php echo $dt['TANGGAL']; ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>												
										</div>
									</div>

									<div class="form-group">																				
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> KEGIATAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="5%" style="text-align:center">NA</th>
															<th width="25%" style="text-align:center">KETERANGAN</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>A.</td>
															<td>KEBERSIHAN DAPUR</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Lantai Tidak Licin
															</td>
															<td align="center">
																<label><input type="radio" <?php if($dt['A1']=='1') echo " checked "?> name="A1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" <?php if($dt['A1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" <?php if($dt['A1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A1']; ?>" name="KET_A1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Permukaan Tidak Berdebu
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A2']; ?>" name="KET_A2" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Tidak ada Lawa-lawa
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" <?php if($dt['A3']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" <?php if($dt['A3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" <?php if($dt['A3']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A3']; ?>" name="KET_A3" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Tempat Limbah Tertutup dan dalam keadaan bersih
															</td>
															<td align="center">
																<label>
																	<input name="A4" type="radio" <?php if($dt['A4']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A4" type="radio" <?php if($dt['A4']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A4" type="radio" <?php if($dt['A4']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A4']; ?>" name="KET_A4" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>5
															</td>
															<td>Penempatan Tempat Limbah Sesuai Lokasi
															</td>
															<td align="center">
																<label>
																	<input name="A5" type="radio" <?php if($dt['A5']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A5" type="radio" <?php if($dt['A5']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A5" type="radio" <?php if($dt['A5']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A5']; ?>" name="KET_A5" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Wastafel Cuci Tangan Selalu bersih dan Bebas dari Peralatan
															</td>
															<td align="center">
																<label>
																	<input name="A6" type="radio" <?php if($dt['A6']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A6" type="radio" <?php if($dt['A6']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A6" type="radio" <?php if($dt['A6']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A6']; ?>" name="KET_A6" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>7</td>
															<td>Rak Penyimpanan Bersih
															</td>
															<td align="center">
																<label>
																	<input name="A7" type="radio" <?php if($dt['A7']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A7" type="radio" <?php if($dt['A7']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A7" type="radio" <?php if($dt['A7']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A7']; ?>" name="KET_A7" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>B.</td>
															<td>KEBERSIHAN ALAT MASAK DAN ALAT MAKAN
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Bak cuci bersih dengan air mengalir yang panas dan dingin
															</td>
															<td align="center"><label>
																<input type="radio" name="B1" <?php if($dt['B1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="B1" <?php if($dt['B1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="B1" type="radio" <?php if($dt['B1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B1']; ?>" name="KET_B1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Lemari alat makan dan alat masak bersih, bebas debu
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B2" <?php if($dt['B2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B2" <?php if($dt['B2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B2" <?php if($dt['B2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B2']; ?>" name="KET_B2" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>3
															</td>
															<td>Kompor Bersih
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B3" <?php if($dt['B3']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B3" <?php if($dt['B3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B3" <?php if($dt['B3']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B3']; ?>" name="KET_B3" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>4
															</td>
															<td>Talenan dan Pisau dipisah untuk daging dan sayur
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B4" <?php if($dt['B4']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B4" <?php if($dt['B4']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B4" <?php if($dt['B4']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B4']; ?>" name="KET_B4" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>5
															</td>
															<td>Alat Masak Bersih
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B5" <?php if($dt['B5']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B5" <?php if($dt['B5']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B5" <?php if($dt['B5']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B5']; ?>" name="KET_B5" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>6
															</td>
															<td>Alat Makan bersih
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B6" <?php if($dt['B6']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B6" <?php if($dt['B6']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="B6" <?php if($dt['B6']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B6']; ?>" name="KET_B6" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>C.</td>
															<td>GUDANG PENYIMPANAN MAKANAN
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1
															</td>
															<td>Pengaturan Penyimpanan
															</td>
															<td align="center">																
																</td>
																<td align="center">
																	
																</td>
																<td align="center">
																	
																</td>
																<td>
																			
																</td>
															</tr>
															<tr>
																<td>a</td>
																<td>Temperatur Kulkas dan Chiller dimonitoring dan dicatat setiap hari
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1A" <?php if($dt['C1A']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>

																<td align="center">
																	<label>
																		<input type="radio" name="C1A" <?php if($dt['C1A']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1A" <?php if($dt['C1A']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1A']; ?>" name="KET_C1A" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>b</td>
																<td>Semua barang disimpan dalam container tertutup dan dalam keadaan selalu bersih
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1B" <?php if($dt['C1B']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1B" <?php if($dt['C1B']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1B" <?php if($dt['C1B']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1B']; ?>" name="KET_C1B" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>c</td>
																<td>Ada Rotasi Penyimpanan FIFO & FEFO
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1C" <?php if($dt['C1C']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1C" <?php if($dt['C1C']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1C" <?php if($dt['C1C']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1C']; ?>" name="KET_C1C" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>d</td>
																<td>Ada Kartu Stok/ Cek disetiap Barang
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1D" <?php if($dt['C1D']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1D" <?php if($dt['C1D']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1D" <?php if($dt['C1D']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1D']; ?>" name="KET_C1D" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>e</td>
																<td>Lemari Penyimpanan makanan Rapi dan Sesuai Jenisnya dan berada 15 cm dari lantai dan jauh dari dinding
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1E" <?php if($dt['C1E']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1E" <?php if($dt['C1E']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1E" <?php if($dt['C1E']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1E']; ?>" name="KET_C1E" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
															<td>2</td>
															<td>Pengaturan Penyimpanan
															</td>
															<td align="center">																
																</td>
																<td align="center">
																	
																</td>
																<td align="center">
																	
																</td>
																<td>
																	
																</td>
															</tr>
															<tr>
																<td>a</td>
																<td>Bebas dari Binatang dan Serangga (kecoa,lalat,semut,tikus,kucing)
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2A" <?php if($dt['C2A']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>

																<td align="center">
																	<label>
																		<input type="radio" name="C2A" <?php if($dt['C2A']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2A" <?php if($dt['C2A']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C2A']; ?>" name="KET_C2A" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>b</td>
																<td>Bebas dari Debu
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2B" <?php if($dt['C2B']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2B" <?php if($dt['C2B']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2B" <?php if($dt['C2B']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C2B']; ?>" name="KET_C2B" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>D.</td>
																<td>PENGOLAHAN
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Menggunakan Penjepit/ Sendok Sayur/ Centong  Makan
																</td>
																<td align="center"><label>
																	<input type="radio" name="D1" <?php if($dt['D1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="D1" type="radio" <?php if($dt['D1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="D1" type="radio" <?php if($dt['D1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D1']; ?>" name="KET_D1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Petugas melakukan Cuci Tangan 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" <?php if($dt['D2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" <?php if($dt['D2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" <?php if($dt['D2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D2']; ?>" name="KET_D2" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Bahan Makanan sesuai Spesifikasi
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D3" <?php if($dt['D3']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D3" <?php if($dt['D3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D3" <?php if($dt['D3']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D3']; ?>" name="KET_D3" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Tempat Persiapan/ Meja Peracik Bersih
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D4" <?php if($dt['D4']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D4" <?php if($dt['D4']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D4" <?php if($dt['D4']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D4']; ?>" name="KET_D4" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>5</td>
																<td>Peralatan Pengolahan Tidak dicampur Penggunaannya
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D5" <?php if($dt['D5']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D5" <?php if($dt['D5']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D5" <?php if($dt['D5']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D5']; ?>" name="KET_D5" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>6</td>
																<td>Simpan Makanan Matang dalam Kontainer Tertutup
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D6" <?php if($dt['D6']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D6" <?php if($dt['D6']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D6" <?php if($dt['D6']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D6']; ?>" name="KET_D6" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>7</td>
																<td>Terpisah dengan  bahan makanan mentah
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D7" <?php if($dt['D7']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D7" <?php if($dt['D7']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D7" <?php if($dt['D7']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D7']; ?>" name="KET_D7" placeholder="" class="form-control" />	
																</td>
															</tr>	
															<tr>
																<td>E.</td>
																<td>PENJAMAH MAKANAN
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Pakaian Bersih & Rapi
																</td>
																<td align="center"><label>
																	<input type="radio" name="E1" <?php if($dt['E1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="E1" <?php if($dt['E1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="E1" type="radio" <?php if($dt['E1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_E1']; ?>" name="KET_E1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Menggunakan APD (Celemek, Topi, Masker, Sarung tangan & Sepatu Tertutup)
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E2']=='1') echo " checked "?> name="E2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E2']=='2') echo " checked "?> name="E2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E2']=='3') echo " checked "?> name="E2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_E2']; ?>" name="KET_E2" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Berkuku Pendek
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E3']=='1') echo " checked "?> name="E3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E3']=='2') echo " checked "?> name="E3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E3']=='3') echo " checked "?> name="E3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_E3']; ?>" name="KET_E3" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Tidak Memakai Perhiasan (Cincin, gelang & Jam tangan)
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E4']=='1') echo " checked "?> name="E4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E4']=='2') echo " checked "?> name="E4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E4']=='3') echo " checked "?> name="E4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_E4']; ?>" name="KET_E4" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>5</td>
																<td>Mencuci tangan sebelum dan sesudah kontak dengan makanan
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E5']=='1') echo " checked "?> name="E5" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E5']=='2') echo " checked "?> name="E5" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['E5']=='3') echo " checked "?> name="E5" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_E5']; ?>" name="KET_E5" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>F.</td>
																<td>CARA PENYAJIAN MAKANAN
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Penyajian  Makanan di Wrapping
																</td>
																<td align="center"><label>
																	<input type="radio" <?php if($dt['F1']=='1') echo " checked "?> name="F1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="F1" <?php if($dt['F1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="F1" <?php if($dt['F1']=='3') echo " checked "?> type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F1']; ?>" name="KET_F1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Makanan disajikan dalam waktu 2 jam dari persiapan 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F2']=='1') echo " checked "?> name="F2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F2']=='2') echo " checked "?> name="F2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F2']=='3') echo " checked "?> name="F2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F2']; ?>" name="KET_F2" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Selama pengiriman makanan ditutup 
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F3']=='1') echo " checked "?> name="F3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F3']=='2') echo " checked "?> name="F3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F3']=='3') echo " checked "?> name="F3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F3']; ?>" name="KET_F3" placeholder="" class="form-control" />	
																</td>
															</tr>															
															<tr>
																<td style="text-align:center" colspan="4">JUMLAH TOTAL</td>
																<td colspan="3" style="text-align:center">
																	<div class="input-group">
																		<input type="text" id="total" value="<?php echo $dt['NILAI']; ?>" name="NILAI" placeholder="[ NILAI ]" class="form-control" />
																		<span class="input-group-addon">
																			%
																		</span>
																	</div>													
																</td>														
															</tr>													
														</tbody>
													</table>																									
												</div>
											</div>
										</div>																										
										<hr />
										<div class="form-group">										
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
												<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
											</div>				
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
												<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
											</div>
										</div>
										<div class="form-group">			
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">KOORDINATOR GIZI</label>	
												<input type="text" id="nama" value="<?php echo $dt['KOORDINATOR']; ?>" name="KOORDINATOR" placeholder="[ KOORDINATOR GIZI ]" class="form-control" />												
											</div>	
											<div class="col-md-6 col-sm-12">	
												<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
												<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
											</div>								
										</div>																											
										<hr />
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->		
<?php }
?>