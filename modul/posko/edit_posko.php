<script type="text/javascript">

	function tugas1()
	{	
		var ya = $('#aa:checked').length;
		var tidak = $('#bb:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<?php
$id=($_GET['id']);
$query="select ti.ID_ISI, ti.TGL_INPUT, ti.TANGGAL, ti.AREA,ti.PROYEK,ti.KAINSTALASI, ti.PANITIA_PPI, ti.KONTRAKTOR, ti.PETUGAS_K3,
ti.SATU_A, ti.DUA_A, ti.TIGA_A, ti.EMPAT_A, ti.LIMA_A, ti.ENAM_A, ti.TUJUH_A, ti.DELAPAN_A, ti.SEMBILAN_A, ti.SATU_B, ti.DUA_B,ti.TIGA_B,ti.EMPAT_B
, ti.LIMA_B, ti.ENAM_B, ti.TUJUH_B, ti.SATU_C, ti.DUA_C, ti.SATU_D, ti.DUA_D, ti.SATU_E, ti.DUA_E, ti.SATU_F, ti.SATU_G, ti.DUA_G,ti.TOTAL, ti.`STATUS`,ti.ANALISA, ti.TINDAKLANJUT, ti.KEPALA, ti.AUDITOR
from tb_posko ti															
where ti.ID_ISI='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">Formulir Cheklist Post-Konstruksi</li>
				</ul><!-- /.breadcrumb -->

				
			</div>

			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Formulir Cheklist Post-Konstruksi</h4>

								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>

									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>

							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_posko" method="post">
									<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $dt['ID_ISI']; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TANGGAL']; ?>" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-5">										
											<input type="text" name="AREA" value="<?php echo $dt['AREA']; ?>" placeholder="[ AREA ]" class="form-control" />
										</div>	
										
										<div class="col-sm-5">											
											<input type="text" name="PROYEK" value="<?php echo $dt['PROYEK']; ?>" placeholder="[ PROYEK ]" class="form-control" />												
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-3">										
											<input type="text" name="KAINSTALASI" value="<?php echo $dt['KAINSTALASI']; ?>" placeholder="[ KA. INSTALASI ]" class="form-control" />
										</div>	
										
										<div class="col-sm-3">											
											<input type="text" name="PANITIA_PPI" value="<?php echo $dt['PANITIA_PPI']; ?>" placeholder="[ KA. PANITIA PPI ]" class="form-control" />												
										</div>
										<div class="col-sm-3">										
											<input type="text" name="KONTRAKTOR" value="<?php echo $dt['KONTRAKTOR']; ?>" placeholder="[ KONTRAKTOR ]" class="form-control" />
										</div>	
										
										<div class="col-sm-3">											
											<input type="text" name="PETUGAS_K3" value="<?php echo $dt['PETUGAS_K3']; ?>" placeholder="[ PETUGAS K3 ]" class="form-control" />												
										</div>
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> KRITERIA</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td align="center" style="font-weight: bold;">A</td>
														<td align="" style="font-weight: bold;">Penyelesaian Proyek</td>
														<td align="center"></td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td rowspan="9"></td>
														<td>1. Pembilasan sistem air utama untuk membersihkan debu pada pipa</td>
														<td align="center">
															<label>
																<input type="checkbox" name="SATU_A" <?php if($dt['SATU_A']=='1') echo " checked "?> id="aa" value="1" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_A" type="checkbox" <?php if($dt['SATU_A']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>2. Pembersihan zona konstruksi sebelum memindahkan barrier konstruksi</td>
														<td align="center">
															<label>
																<input name="DUA_A" type="checkbox" <?php if($dt['DUA_A']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_A" type="checkbox" <?php if($dt['DUA_A']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg" onClick="hitung( this )" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>3. Pemeriksaan jamur dan lumut. Bila ditemukan lakukan pembersihan</td>
														<td align="center">
															<label>
																<input name="TIGA_A" type="checkbox" <?php if($dt['TIGA_A']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TIGA_A" type="checkbox" <?php if($dt['TIGA_A']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>4. Verifikasi parameter ventilasi pada area baru sesuai kebutuhan</td>
														<td align="center">
															<label>
																<input name="EMPAT_A" type="checkbox" <?php if($dt['EMPAT_A']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="EMPAT_A" type="checkbox" <?php if($dt['EMPAT_A']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>5. Jangan menerima apabila terdapat kekurangan ventilasi terutama di daerah perawatan khusus</td>
														<td align="center">
															<label>
																<input name="LIMA_A" type="checkbox" <?php if($dt['LIMA_A']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="LIMA_A" type="checkbox" <?php if($dt['LIMA_A']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>6. Bersihkan atau ganti filter HVAC sesuai prosedur penahanan debu yang tepat</td>
														<td align="center">
															<label>
																<input name="ENAM_A" type="checkbox" <?php if($dt['ENAM_A']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="ENAM_A" type="checkbox" <?php if($dt['ENAM_A']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>7. Pindahkan barrier dan bersihkan daerah dari semua debu yang dihasilkan selama pekerjaan/proyek</td>
														<td align="center">
															<label>
																<input name="TUJUH_A" type="checkbox" <?php if($dt['TUJUH_A']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TUJUH_A" type="checkbox" <?php if($dt['TUJUH_A']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>8. Pastikan bahwa keseimbangan tekanan udara di kamar operasi dan lingkungan sekitarnya dapat dicapai sebelum ruangan digunakan</td>
														<td align="center">
															<label>
																<input name="DELAPAN_A" type="checkbox" <?php if($dt['DELAPAN_A']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DELAPAN_A" type="checkbox" <?php if($dt['DELAPAN_A']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>9. Komisi ruang sesuai indikasi terutama di kamar operasi dan lingkungan sekitarnya, pastikan bahwa spesifikasi teknis sesuai yang disyaratkan</td>
														<td align="center">
															<label>
																<input name="SEMBILAN_A" type="checkbox" <?php if($dt['SEMBILAN_A']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SEMBILAN_A" type="checkbox" <?php if($dt['SEMBILAN_A']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td align="center" style="font-weight: bold;">B</td>
														<td align="" style="font-weight: bold;">Apakah sistem berikut ini diuji dan berfungsibaik?</td>
														<td align="center">
															
														</td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td rowspan="7"></td>
														<td>1. Alarm kebakaran – lepaskan penutup detektor dan lakukan pengujian dari panel kontrol</td>
														<td align="center">
															<label>
																<input name="SATU_B" type="checkbox" <?php if($dt['SATU_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_B" type="checkbox" <?php if($dt['SATU_B']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>2. Sprinkler / Penyemprot air – terhubung ke saluran utama dan bertekanan cukup</td>
														<td align="center">
															<label>
																<input name="DUA_B" type="checkbox" <?php if($dt['DUA_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_B" type="checkbox" <?php if($dt['DUA_B']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
														
													</tr>
													<tr>
														<td>3. Listrik – pengujian switch/tombol dan pengontrolan</td>
														<td align="center">
															<label>
																<input name="TIGA_B" type="checkbox" <?php if($dt['TIGA_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TIGA_B" type="checkbox" <?php if($dt['TIGA_B']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4. Sumber air buka, dan cek suhu</td>
														<td align="center">
															<label>
																<input name="EMPAT_B" type="checkbox" <?php if($dt['EMPAT_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="EMPAT_B" type="checkbox" <?php if($dt['EMPAT_B']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>5. Gas medis</td>
														<td align="center">
															<label>
																<input name="LIMA_B" type="checkbox" <?php if($dt['LIMA_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="LIMA_B" type="checkbox" <?php if($dt['LIMA_B']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>6. Limbah – hilangkan sumbatan</td>
														<td align="center">
															<label>
																<input name="ENAM_B" type="checkbox" <?php if($dt['ENAM_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="ENAM_B" type="checkbox" <?php if($dt['ENAM_B']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>7. HVAC – pemasangan filter, menghilangkan penyumbatan, uji keseimbangan tekanan</td>
														<td align="center">
															<label>
																<input name="TUJUH_B" type="checkbox" <?php if($dt['TUJUH_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TUJUH_B" type="checkbox" <?php if($dt['TUJUH_B']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td align="center" style="font-weight: bold;">C</td>
														<td align="" style="font-weight: bold;">Lingkungan</td>
														<td align="center">														
														</td>
														<td align="center">															
														</td>
													</tr>
													
													<tr>
														<td rowspan="2"></td>
														<td>1. Bersihkan puing-puing, peralatan, perlengkapan, dan bahan-bahan bangunan</td>
														<td align="center">
															<label>
																<input name="SATU_C" type="checkbox" <?php if($dt['SATU_C']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_C" type="checkbox" <?php if($dt['SATU_C']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>2. Vacuum dan bersihkan permukaan di semua area konstruksi untuk menghilangkan debu</td>
														<td align="center">
															<label>
																<input name="DUA_C" type="checkbox" <?php if($dt['DUA_C']=='1') echo " checked "?>value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_C" type="checkbox" <?php if($dt['DUA_C']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td align="center" style="font-weight: bold;">D</td>
														<td align="" style="font-weight: bold;">Isolation barriers</td>
														<td align="center">
															
														</td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td rowspan="2"></td>
														<td>1. Pelindung harus dilap basah, disedot dengan HEPA, atau diberi uap air sebelum dibongkar</td>
														<td align="center">
															<label>
																<input name="SATU_D" type="checkbox" <?php if($dt['SATU_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_D" type="checkbox" <?php if($dt['SATU_D']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>2. Pelindung harus dipindahkan dengan hati-hati untuk meminimalkan penyebaran kotoran dan puing-puing</td>
														<td align="center">
															<label>
																<input name="DUA_D" type="checkbox" <?php if($dt['DUA_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_D" type="checkbox" <?php if($dt['DUA_D']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td align="center" style="font-weight: bold;">E</td>
														<td align="" style="font-weight: bold;">Pengendalian infeksi</td>
														<td align="center">
															
														</td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td rowspan="2"></td>
														<td>1. Tinjau indikasi untuk melakukan kultur lingkungan dengan  satker terkait</td>
														<td align="center">
															<label>
																<input name="SATU_E" type="checkbox" <?php if($dt['SATU_E']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_E" type="checkbox" <?php if($dt['SATU_E']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>2. Periksa daerah konstruksi setelah pembersihan akhir dan menyetujui penggunaannya</td>
														<td align="center">
															<label>
																<input name="DUA_E" type="checkbox" <?php if($dt['DUA_E']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_E" type="checkbox" <?php if($dt['DUA_E']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td align="center" style="font-weight: bold;">F</td>
														<td align="" style="font-weight: bold;">Keamanan Kebakaran</td>
														<td align="center">
															
														</td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td></td>
														<td>1. Tersedianya peralatan pemadam kebakaran</td>
														<td align="center">
															<label>
																<input name="SATU_F" type="checkbox" <?php if($dt['SATU_F']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_F" type="checkbox" <?php if($dt['SATU_F']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td align="center" style="font-weight: bold;">G</td>
														<td align="" style="font-weight: bold;">Keselamatan Jiwa</td>
														<td align="center">
															
														</td>
														<td align="center">
															
														</td>
													</tr>
													<tr>
														<td rowspan="2"></td>
														<td>1. Pintu keluar dan rute ke IGD dibuat kembali</td>
														<td align="center">
															<label>
																<input name="SATU_G" type="checkbox" <?php if($dt['SATU_G']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="SATU_G" type="checkbox" <?php if($dt['SATU_G']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														
														<td>2. Penempatan tanda pintu keluar dengan tepat</td>
														<td align="center">
															<label>
																<input name="DUA_G" type="checkbox" <?php if($dt['DUA_G']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="DUA_G" type="checkbox" <?php if($dt['DUA_G']=='2') echo " checked "?> value="2" id="bb" onClick="tugas1()" class="ace input-lg"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
															<div class="input-group">
																<input name="TOTAL" value="<?php echo $dt['TOTAL']; ?>" type="text" id="total" style="width:60px"></br>
																<span class="input-group-addon">
																	%
																</span>
															</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													
									<div class="form-group">										
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
											<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
										</div>				
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
											<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
										</div>
									</div>
									<div class="form-group">			
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
											<input type="text" id="nama" value="<?php echo $dt['KEPALA']; ?>" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
										</div>	
										<div class="col-md-6 col-sm-12">	
											<label class="control-label bolder blue" style="text-decoration: underline">OBSERVATOR</label>				
											<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
										</div>								
									</div>
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnEdit" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->	
		</div> <!-- container -->
	</div><!-- /.main-content -->
	<?php
}?>