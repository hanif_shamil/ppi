<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AREA 			= $_POST['AREA'];						
						$PROYEK 		= $_POST['PROYEK'];
						$KAINSTALASI 	= $_POST['KAINSTALASI'];
						$PANITIA_PPI 	= $_POST['PANITIA_PPI'];
						$KONTRAKTOR 	= $_POST['KONTRAKTOR'];
						$PETUGAS_K3 	= $_POST['PETUGAS_K3'];
						$SATU_A = $_POST['SATU_A'];
						$DUA_A = $_POST['DUA_A'];
						$TIGA_A = $_POST['TIGA_A'];
						$EMPAT_A = $_POST['EMPAT_A'];
						$LIMA_A = $_POST['LIMA_A'];
						$ENAM_A = $_POST['ENAM_A'];
						$TUJUH_A = $_POST['TUJUH_A'];
						$DELAPAN_A = $_POST['DELAPAN_A'];
						$SEMBILAN_A = $_POST['SEMBILAN_A'];
						$SATU_B = $_POST['SATU_B'];
						$DUA_B = $_POST['DUA_B'];
						$TIGA_B = $_POST['TIGA_B'];
						$EMPAT_B = $_POST['EMPAT_B'];
						$LIMA_B = $_POST['LIMA_B'];
						$ENAM_B = $_POST['ENAM_B'];
						$TUJUH_B = $_POST['TUJUH_B'];
						$SATU_C = $_POST['SATU_C'];
						$DUA_C = $_POST['DUA_C'];
						$SATU_D = $_POST['SATU_D'];
						$DUA_D = $_POST['DUA_D'];
						$SATU_E = $_POST['SATU_E'];
						$DUA_E = $_POST['DUA_E'];
						$SATU_F = $_POST['SATU_F'];
						$SATU_G = $_POST['SATU_G'];
						$DUA_G = $_POST['DUA_G'];							
						$TOTAL	 		= $_POST['TOTAL'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$AUDITOR		= addslashes($_POST['AUDITOR']);								
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_posko (
							ID_ISI,
							TANGGAL,
							AREA,
							PROYEK,
							KAINSTALASI,
							PANITIA_PPI,
							KONTRAKTOR,
							PETUGAS_K3,
							SATU_A,
							DUA_A,
							TIGA_A,
							EMPAT_A,
							LIMA_A,
							ENAM_A,
							TUJUH_A,
							DELAPAN_A,
							SEMBILAN_A,
							SATU_B,
							DUA_B,
							TIGA_B,
							EMPAT_B,
							LIMA_B,
							ENAM_B,
							TUJUH_B,
							SATU_C,
							DUA_C,
							SATU_D,
							DUA_D,
							SATU_E,
							DUA_E,
							SATU_F,
							SATU_G,
							DUA_G,
							TOTAL,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							AUDITOR,
							USER,
							STATUS) 
						VALUES 
						('$ID_ISI',
							'$TANGGAL',
							'$AREA',
							'$PROYEK',
							'$KAINSTALASI',
							'$PANITIA_PPI',
							'$KONTRAKTOR',
							'$PETUGAS_K3',
							'$SATU_A',
							'$DUA_A',
							'$TIGA_A',
							'$EMPAT_A',
							'$LIMA_A',
							'$ENAM_A',
							'$TUJUH_A',
							'$DELAPAN_A',
							'$SEMBILAN_A',
							'$SATU_B',
							'$DUA_B',
							'$TIGA_B',
							'$EMPAT_B',
							'$LIMA_B',
							'$ENAM_B',
							'$TUJUH_B',
							'$SATU_C',
							'$DUA_C',
							'$SATU_D',
							'$DUA_D',
							'$SATU_E',
							'$DUA_E',
							'$SATU_F',
							'$SATU_G',
							'$DUA_G',
							'$TOTAL',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$AUDITOR',	
							'$USER',												
							'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_posko'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AREA 			= $_POST['AREA'];						
						$PROYEK 		= $_POST['PROYEK'];
						$KAINSTALASI 	= $_POST['KAINSTALASI'];
						$PANITIA_PPI 	= $_POST['PANITIA_PPI'];
						$KONTRAKTOR 	= $_POST['KONTRAKTOR'];
						$PETUGAS_K3 	= $_POST['PETUGAS_K3'];
						$SATU_A = $_POST['SATU_A'];
						$DUA_A = $_POST['DUA_A'];
						$TIGA_A = $_POST['TIGA_A'];
						$EMPAT_A = $_POST['EMPAT_A'];
						$LIMA_A = $_POST['LIMA_A'];
						$ENAM_A = $_POST['ENAM_A'];
						$TUJUH_A = $_POST['TUJUH_A'];
						$DELAPAN_A = $_POST['DELAPAN_A'];
						$SEMBILAN_A = $_POST['SEMBILAN_A'];
						$SATU_B = $_POST['SATU_B'];
						$DUA_B = $_POST['DUA_B'];
						$TIGA_B = $_POST['TIGA_B'];
						$EMPAT_B = $_POST['EMPAT_B'];
						$LIMA_B = $_POST['LIMA_B'];
						$ENAM_B = $_POST['ENAM_B'];
						$TUJUH_B = $_POST['TUJUH_B'];
						$SATU_C = $_POST['SATU_C'];
						$DUA_C = $_POST['DUA_C'];
						$SATU_D = $_POST['SATU_D'];
						$DUA_D = $_POST['DUA_D'];
						$SATU_E = $_POST['SATU_E'];
						$DUA_E = $_POST['DUA_E'];
						$SATU_F = $_POST['SATU_F'];
						$SATU_G = $_POST['SATU_G'];
						$DUA_G = $_POST['DUA_G'];							
						$TOTAL	 		= $_POST['TOTAL'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$AUDITOR		= addslashes($_POST['AUDITOR']);							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_posko SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						AREA 			= '$AREA',						
						PROYEK 		= '$PROYEK',
						KAINSTALASI 	= '$KAINSTALASI',
						PANITIA_PPI 	= '$PANITIA_PPI',
						KONTRAKTOR 	= '$KONTRAKTOR',
						PETUGAS_K3 	= '$PETUGAS_K3',
						SATU_A = '$SATU_A',
						DUA_A = '$DUA_A',
						TIGA_A = '$TIGA_A',
						EMPAT_A = '$EMPAT_A',
						LIMA_A = '$LIMA_A',
						ENAM_A = '$ENAM_A',
						TUJUH_A = '$TUJUH_A',
						DELAPAN_A = '$DELAPAN_A',
						SEMBILAN_A = '$SEMBILAN_A',
						SATU_B = '$SATU_B',
						DUA_B = '$DUA_B',
						TIGA_B = '$TIGA_B',
						EMPAT_B = '$EMPAT_B',
						LIMA_B = '$LIMA_B',
						ENAM_B = '$ENAM_B',
						TUJUH_B = '$TUJUH_B',
						SATU_C = '$SATU_C',
						DUA_C = '$DUA_C',
						SATU_D = '$SATU_D',
						DUA_D = '$DUA_D',
						SATU_E = '$SATU_E',
						DUA_E = '$DUA_E',
						SATU_F = '$SATU_F',
						SATU_G = '$SATU_G',
						DUA_G = '$DUA_G',		
						TOTAL='$TOTAL',
						ANALISA 		= '$ANALISA', 
						TINDAKLANJUT 	= '$TINDAKLANJUT', 
						KEPALA 			= '$KEPALA',
						AUDITOR			='$AUDITOR',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_posko'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_posko ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_posko'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			