<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						
						$PENEMPATAN			= $_POST['PENEMPATAN'];
						$TANGGAL 			= $_POST['TANGGAL'];									
						$NILAI 				= $_POST['NILAI'];
						$NOMR				= $_POST['NOMR'];
						$NAMA				= addslashes($_POST['NAMA']);
						$UMUR				= $_POST['UMUR'];
						$DPJP				= $_POST['DPJP'];
						$DIAGNOSA			= $_POST['DIAGNOSA'];	
						$A1 				= $_POST['A1'];
						$KET_A1				= $_POST['KET_A1']; 
						$A2					= $_POST['A2'];
						$KET_A2				= $_POST['KET_A2']; 
						$A3					= $_POST['A3'];
						$KET_A3 			= $_POST['KET_A3'];	
						$A4					= $_POST['A4'];
						$KET_A4				= $_POST['KET_A4'];  
						$A5 				= $_POST['A5'];
						$KET_A5 			= $_POST['KET_A5'];
						$A6					= $_POST['A6'];
						$KET_A6 			= $_POST['KET_A6'];   
						$B1 				= $_POST['B1'];
						$KET_B1 			=	$_POST['KET_B1'];  
						$B2 				=	$_POST['B2'];
						$KET_B2 			=	$_POST['KET_B2'];  
						$C1 				=	$_POST['C1'];
						$KET_C1 			=	$_POST['KET_C1'];  
						$C2 				=	$_POST['C2'];
						$KET_C2 			=	$_POST['KET_C2'];  
						$C3 				=	$_POST['C3']; 
						$KET_C3 			=	$_POST['KET_C3']; 
						$C4 				=	$_POST['C4'];
						$KET_C4 			=	$_POST['KET_C4'];  
						$C5 				=	$_POST['C5'];
						$KET_C5 			=	$_POST['KET_C5'];  
						$C6 				=	$_POST['C6'];
						$KET_C6 			=	$_POST['KET_C6'];  
						$D1 				=	$_POST['D1'];
						$KET_D1 			=	$_POST['KET_D1']; 
						$D2 				=	$_POST['D2'];
						$KET_D2 			=	$_POST['KET_D2']; 
						$D3 				=	$_POST['D3'];
						$KET_D3				= 	$_POST['KET_D3'];  
						$D4 				=	$_POST['D4'];
						$KET_D4 			=	$_POST['KET_D4'];  
						$D5 				=	$_POST['D5'];
						$KET_D5 			=	$_POST['KET_D5'];  
						$D6 				=	$_POST['D6'];
						$KET_D6				=	$_POST['KET_D6'];  
						$NILAI 				=	$_POST['NILAI'];  
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 			=	addslashes($_POST['AUDITOR']); 
						$pengisi 			= $_SESSION['userid'];							
						//echo "<pre>";print_r($_POST);exit();                	
						$query = "INSERT INTO db_ppi.tb_isone (
						USER,
						PENEMPATAN,
						MR,
						NAMA,
						UMUR,
						DIAGNOSA,
						DPJP,
						TANGGAL,
						A1,
						KET_A1, 
						A2,
						KET_A2, 
						A3,
						KET_A3, 
						A4,
						KET_A4, 
						A5,
						KET_A5, 
						A6,
						KET_A6, 
						B1,
						KET_B1, 
						B2,
						KET_B2, 
						C1,
						KET_C1, 
						C2,
						KET_C2, 
						C3,
						KET_C3, 
						C4,
						KET_C4, 
						C5,
						KET_C5, 
						C6,
						KET_C6, 
						D1,
						KET_D1, 
						D2,
						KET_D2, 
						D3,
						KET_D3, 
						D4,
						KET_D4, 
						D5,
						KET_D5, 
						D6,
						KET_D6, 
						NILAI, 
						ANALISA,
						TINDAKLANJUT,
						KEPALA,
						AUDITOR) 
						VALUES 
						('$pengisi',
						'$PENEMPATAN',  
						'$NOMR', 
						'$NAMA',
						'$UMUR', 
						'$DIAGNOSA',
						'$DPJP',
						'$TANGGAL', 
						'$A1',  
						'$KET_A1',  
						'$A2',  
						'$KET_A2',  
						'$A3',  
						'$KET_A3',  
						'$A4',  
						'$KET_A4',  
						'$A5',  
						'$KET_A5',  
						'$A6',  
						'$KET_A6',  
						'$B1',  
						'$KET_B1',  
						'$B2',  
						'$KET_B2',  
						'$C1',  
						'$KET_C1',  
						'$C2',  
						'$KET_C2',  
						'$C3',  
						'$KET_C3',  
						'$C4',  
						'$KET_C4',  
						'$C5',  
						'$KET_C5',  
						'$C6',  
						'$KET_C6',  
						'$D1',  
						'$KET_D1',  
						'$D2',  
						'$KET_D2',  
						'$D3',  
						'$KET_D3',  
						'$D4',  
						'$KET_D4',  
						'$D5',  
						'$KET_D5',  
						'$D6',  
						'$KET_D6',  
						'$NILAI',  
						'$ANALISA',  
						'$TINDAKLANJUT', 
						'$KEPALA', 
						'$AUDITOR')";
						//echo $query2; die();	
						$hasil=mysqli_query($conn1,$query);								
						if($hasil){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_isone'</script>"; 
						}                     
					else{
						echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
					}                         
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			