<script type="text/javascript">
	function tugas1()
	{
		var jumlah=0;
		var jumlahx=0;
		var nilai;
		if(document.getElementById("SABUN").checked)
		{
			nilai=document.getElementById("SABUN").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("SABUNTDK").checked)
		{
			nilai=document.getElementById("SABUNTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("PAPER").checked)
		{
			nilai=document.getElementById("PAPER").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("PAPERTDK").checked)
		{
			nilai=document.getElementById("PAPERTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("BEBAS_ALAT").checked)
		{
			nilai=document.getElementById("BEBAS_ALAT").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("BEBAS_ALATTDK").checked)
		{
			nilai=document.getElementById("BEBAS_ALATTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("CUCI_TANGAN").checked)
		{
			nilai=document.getElementById("CUCI_TANGAN").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("CUCI_TANGANTDK").checked)
		{
			nilai=document.getElementById("CUCI_TANGANTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("TMPT_SAMPAH").checked)
		{
			nilai=document.getElementById("TMPT_SAMPAH").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("TMPT_SAMPAHTDK").checked)
		{
			nilai=document.getElementById("TMPT_SAMPAHTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("HANDRUP").checked)
		{
			nilai=document.getElementById("HANDRUP").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("HANDRUPTDK").checked)
		{
			nilai=document.getElementById("HANDRUPTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("PETUNJUK").checked)
		{
			nilai=document.getElementById("PETUNJUK").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("PETUNJUKTDK").checked)
		{
			nilai=document.getElementById("PETUNJUKTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
	}
</script>
<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISI) AS last FROM tb_afkt WHERE ID_ISI LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Audit Fasilitas Kebersihan Tangan</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Audit Fasilitas Kebersihan Tangan</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_afkt" method="post">
									<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-6">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" autocomplete="off" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-6">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0 and r.STATUS=1";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
													?>
													<option value="<?=$data[0]?>"><?=$data[3]?></option>
													<?php
												}
												?>	
											</select>
										</div>																
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> FASILITAS KEBERSIHAN TANGAN</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Tersedia sabun cair anti microbial di setiap wastafel</td>
														<td align="center"><label>
															<input type="checkbox" name="SABUN[]" id="SABUN" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
														</label></td>
														<td align="center">
															<label>
																<input name="SABUN[]" type="checkbox" value="2" id="SABUNTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Teresedia paper towel di setiap wastafel</td>
														<td align="center"><label>
															<input type="checkbox" name="PAPER[]" id="PAPER" value="1" class="ace input-lg" onClick="tugas1()">													
															<span class="lbl"></span>
														</label></td>
														<td align="center"><label>
															<input name="PAPER[]" type="checkbox" value="2" id="PAPERTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Wastafel bebas dari peralatan yang tidak tepat</td>
														<td align="center">
															<label>
																<input type="checkbox" name="BEBAS_ALAT[]" id="BEBAS_ALAT" value="1" class="ace input-lg" onClick="tugas1()">
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="BEBAS_ALAT[]" type="checkbox" value="2" id="BEBAS_ALATTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Fasilitas cuci tangan bersih</td>
														<td align="center">
															<label>
																<input name="CUCI_TANGAN[]" type="checkbox" value="1" id="CUCI_TANGAN" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="CUCI_TANGAN[]" type="checkbox" value="2" id="CUCI_TANGANTDK" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Ada tempat sampah di bawah wastafel</td>
														<td align="center">
															<label>
																<input name="TMPT_SAMPAH[]" type="checkbox" value="1" id="TMPT_SAMPAH" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="TMPT_SAMPAH[]" type="checkbox" value="2" id="TMPT_SAMPAHTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Tersedia handrup di setiap tempat tidur pasien</td>
														<td align="center">
															<label>
																<input name="HANDRUP[]" type="checkbox" value="1" id="HANDRUP" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="HANDRUP[]" type="checkbox" value="2" id="HANDRUPTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>7</td>
														<td>Tersedia petunjuk cara mencuci tangan di setiap wastafel</td>
														<td align="center">
															<label>
																<input name="PETUNJUK[]" type="checkbox" value="1" id="PETUNJUK" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
																<input name="PETUNJUK[]" type="checkbox" value="2" id="PETUNJUKTDK" class="ace input-lg" onClick="tugas1()"/>
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
															<div class="input-group">
																<input name="TOTAL" type="text" id="total" style="width:60px"></br>
																<span class="input-group-addon">
																	%
																</span>
															</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													
									<div class="form-group">										
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
											<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
										</div>				
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
											<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
										</div>
									</div>
									<div class="form-group">			
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
											<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
										</div>	
										<div class="col-md-6 col-sm-12">	
											<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
											<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
										</div>								
									</div>
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->