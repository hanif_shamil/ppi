<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SABUN'])){
							$SKREENING = implode(',',$_POST['SABUN']);
						}else{
							$SABUN = "";
						}		
						if(isset($_POST['PAPER'])){
							$PAPER = implode(',',$_POST['PAPER']);
						}else{
							$PAPER = "";
						}	
						if(isset($_POST['BEBAS_ALAT'])){
							$BEBAS_ALAT = implode(',',$_POST['BEBAS_ALAT']);
						}else{
							$BEBAS_ALAT = "";
						}	
						if(isset($_POST['CUCI_TANGAN'])){
							$CUCI_TANGAN = implode(',',$_POST['CUCI_TANGAN']);
						}else{
							$CUCI_TANGAN = "";
						}	
						if(isset($_POST['TMPT_SAMPAH'])){
							$TMPT_SAMPAH = implode(',',$_POST['TMPT_SAMPAH']);
						}else{
							$TMPT_SAMPAH = "";
						}
						if(isset($_POST['HANDRUP'])){
							$HANDRUP = implode(',',$_POST['HANDRUP']);
						}else{
							$HANDRUP = "";
						}
						if(isset($_POST['PETUNJUK'])){
							$PETUNJUK = implode(',',$_POST['PETUNJUK']);
						}else{
							$PETUNJUK = "";
						}						
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_afkt (
							ID_ISI,
							TANGGAL,
							RUANGAN,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							AUDITOR,					
							SABUN,
							PAPER,
							BEBAS_ALAT,
							CUCI_TANGAN,
							TMPT_SAMPAH,
							HANDRUP,
							PETUNJUK,
							TOTAL,
							USER,
							STATUS) 
						VALUES 
						('$ID_ISI',
							'$TANGGAL',
							'$RUANGAN',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$AUDITOR',
							'$SABUN',
							'$PAPER',	
							'$BEBAS_ALAT',		
							'$CUCI_TANGAN',				
							'$TMPT_SAMPAH',	
							'$HANDRUP',	
							'$PETUNJUK',
							'$TOTAL',
							'$USER',												
							'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_afkt'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SABUN'])){
							$SABUN = implode(',',$_POST['SABUN']);
						}else{
							$SABUN = "";
						}		
						if(isset($_POST['PAPER'])){
							$PAPER = implode(',',$_POST['PAPER']);
						}else{
							$PAPER = "";
						}	
						if(isset($_POST['BEBAS_ALAT'])){
							$BEBAS_ALAT = implode(',',$_POST['BEBAS_ALAT']);
						}else{
							$BEBAS_ALAT = "";
						}	
						if(isset($_POST['CUCI_TANGAN'])){
							$CUCI_TANGAN = implode(',',$_POST['CUCI_TANGAN']);
						}else{
							$CUCI_TANGAN = "";
						}	
						if(isset($_POST['TMPT_SAMPAH'])){
							$TMPT_SAMPAH = implode(',',$_POST['TMPT_SAMPAH']);
						}else{
							$TMPT_SAMPAH = "";
						}
						if(isset($_POST['HANDRUP'])){
							$HANDRUP = implode(',',$_POST['HANDRUP']);
						}else{
							$HANDRUP = "";
						}
						if(isset($_POST['PETUNJUK'])){
							$PETUNJUK = implode(',',$_POST['PETUNJUK']);
						}else{
							$PETUNJUK = "";
						}						
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_afkt SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						ANALISA = '$ANALISA', 
						TINDAKLANJUT = '$TINDAKLANJUT', 
						KEPALA = '$KEPALA',
						AUDITOR='$AUDITOR',
						SABUN='$SABUN',
						PAPER='$PAPER',
						BEBAS_ALAT='$BEBAS_ALAT',
						CUCI_TANGAN='$CUCI_TANGAN',
						TMPT_SAMPAH='$TMPT_SAMPAH',
						HANDRUP='$HANDRUP',
						PETUNJUK='$PETUNJUK',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_afkt'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_afkt ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_afkt'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			