<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];	
						$TUJUAN 		= $_POST['TUJUAN'];							
						$PENGIRIM 		= $_POST['PENGIRIM'];
						$PENERIMA 		= $_POST['PENERIMA'];
						$VERIFIKATOR 	= $_POST['VERIFIKATOR'];
						if(isset($_POST['MENGINFORMASI'])){
							$MENGINFORMASI = implode(',',$_POST['MENGINFORMASI']);
						}else{
							$MENGINFORMASI = "";
						}		
						if(isset($_POST['IDENTIFIKASI'])){
							$IDENTIFIKASI = implode(',',$_POST['IDENTIFIKASI']);
						}else{
							$IDENTIFIKASI = "";
						}	
						if(isset($_POST['MELENGKAPI'])){
							$MELENGKAPI = implode(',',$_POST['MELENGKAPI']);
						}else{
							$MELENGKAPI = "";
						}	
						if(isset($_POST['MENGISI'])){
							$MENGISI = implode(',',$_POST['MENGISI']);
						}else{
							$MENGISI = "";
						}	
						if(isset($_POST['EDUKASI'])){
							$EDUKASI = implode(',',$_POST['EDUKASI']);
						}else{
							$EDUKASI = "";
						}
						if(isset($_POST['CUCI_TANGAN'])){
							$CUCI_TANGAN = implode(',',$_POST['CUCI_TANGAN']);
						}else{
							$CUCI_TANGAN = "";
						}
						if(isset($_POST['PAKAI_APD'])){
							$PAKAI_APD = implode(',',$_POST['PAKAI_APD']);
						}else{
							$PAKAI_APD = "";
						}
						if(isset($_POST['RUANG_ISOLASI'])){
							$RUANG_ISOLASI = implode(',',$_POST['RUANG_ISOLASI']);
						}else{
							$RUANG_ISOLASI = "";
						}
						if(isset($_POST['BUANG_APD'])){
							$BUANG_APD = implode(',',$_POST['BUANG_APD']);
						}else{
							$BUANG_APD = "";
						}
						if(isset($_POST['LINEN_KOTOR'])){
							$LINEN_KOTOR = implode(',',$_POST['LINEN_KOTOR']);
						}else{
							$LINEN_KOTOR = "";
						}
						if(isset($_POST['BED_BERSIH'])){
							$BED_BERSIH = implode(',',$_POST['BED_BERSIH']);
						}else{
							$BED_BERSIH = "";
						}
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_tpia (
							ID_ISI,
							TANGGAL,
							RUANGAN,
							TUJUAN,
							PENGIRIM,
							PENERIMA,
							VERIFIKATOR,
							MENGINFORMASI,
							IDENTIFIKASI,
							MELENGKAPI,
							MENGISI,
							EDUKASI,
							CUCI_TANGAN,
							PAKAI_APD,
							RUANG_ISOLASI,
							BUANG_APD,
							LINEN_KOTOR,
							BED_BERSIH,
							TOTAL,
							USER,
							STATUS) 
						VALUES 
						('$ID_ISI',
							'$TANGGAL',
							'$RUANGAN',
							'$TUJUAN',
							'$PENGIRIM',
							'$PENERIMA',
							'$VERIFIKATOR',
							'$MENGINFORMASI',
							'$IDENTIFIKASI',	
							'$MELENGKAPI',		
							'$MENGISI',				
							'$EDUKASI',	
							'$CUCI_TANGAN',	
							'$PAKAI_APD',	
							'$RUANG_ISOLASI',	
							'$BUANG_APD',	
							'$LINEN_KOTOR',	
							'$BED_BERSIH',	
							'$TOTAL',
							'$USER',												
							'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_tpia'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];	
						$TUJUAN 		= $_POST['TUJUAN'];							
						$PENGIRIM 		= $_POST['PENGIRIM'];
						$PENERIMA 		= $_POST['PENERIMA'];
						$VERIFIKATOR 	= $_POST['VERIFIKATOR'];
						if(isset($_POST['MENGINFORMASI'])){
							$MENGINFORMASI = implode(',',$_POST['MENGINFORMASI']);
						}else{
							$MENGINFORMASI = "";
						}		
						if(isset($_POST['IDENTIFIKASI'])){
							$IDENTIFIKASI = implode(',',$_POST['IDENTIFIKASI']);
						}else{
							$IDENTIFIKASI = "";
						}	
						if(isset($_POST['MELENGKAPI'])){
							$MELENGKAPI = implode(',',$_POST['MELENGKAPI']);
						}else{
							$MELENGKAPI = "";
						}	
						if(isset($_POST['MENGISI'])){
							$MENGISI = implode(',',$_POST['MENGISI']);
						}else{
							$MENGISI = "";
						}	
						if(isset($_POST['EDUKASI'])){
							$EDUKASI = implode(',',$_POST['EDUKASI']);
						}else{
							$EDUKASI = "";
						}
						if(isset($_POST['CUCI_TANGAN'])){
							$CUCI_TANGAN = implode(',',$_POST['CUCI_TANGAN']);
						}else{
							$CUCI_TANGAN = "";
						}
						if(isset($_POST['PAKAI_APD'])){
							$PAKAI_APD = implode(',',$_POST['PAKAI_APD']);
						}else{
							$PAKAI_APD = "";
						}
						if(isset($_POST['RUANG_ISOLASI'])){
							$RUANG_ISOLASI = implode(',',$_POST['RUANG_ISOLASI']);
						}else{
							$RUANG_ISOLASI = "";
						}
						if(isset($_POST['BUANG_APD'])){
							$BUANG_APD = implode(',',$_POST['BUANG_APD']);
						}else{
							$BUANG_APD = "";
						}
						if(isset($_POST['LINEN_KOTOR'])){
							$LINEN_KOTOR = implode(',',$_POST['LINEN_KOTOR']);
						}else{
							$LINEN_KOTOR = "";
						}
						if(isset($_POST['BED_BERSIH'])){
							$BED_BERSIH = implode(',',$_POST['BED_BERSIH']);
						}else{
							$BED_BERSIH = "";
						}	
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_tpia SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						TUJUAN='$TUJUAN',
						PENGIRIM='$PENGIRIM',
						PENERIMA='$PENERIMA',
						VERIFIKATOR='$VERIFIKATOR',
						MENGINFORMASI='$MENGINFORMASI',
						IDENTIFIKASI='$IDENTIFIKASI',
						MELENGKAPI='$MELENGKAPI',
						MENGISI='$MENGISI',
						EDUKASI='$EDUKASI',
						CUCI_TANGAN='$CUCI_TANGAN',
						PAKAI_APD='$PAKAI_APD',
						RUANG_ISOLASI='$RUANG_ISOLASI',
						BUANG_APD='$BUANG_APD',
						LINEN_KOTOR='$LINEN_KOTOR',
						BED_BERSIH='$BED_BERSIH',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_tpia'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_tpia ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_tpia'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			