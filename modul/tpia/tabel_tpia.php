<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Data</a>
		</li>
		<li class="active">Data Monitoring Transfer Pasien Infeksi Airborne</li>
	</ul><!-- /.breadcrumb -->

	<!--<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="ace-icon fa fa-search nav-search-icon"></i>
			</span>
		</form>
	</div> -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Data Monitoring Transfer Pasien Infeksi Airborne</h4>
							<div class="widget-toolbar">
								<a href="index.php?page=tpia">
									<i class="ace-icon fa fa-plus" style="color:blue"> Tambah data</i>
								</a>
							</div>
						</div>
						<!-- div.table-responsive -->

						<!-- div.dataTables_borderWrap -->
						<div>
							<table id="tabeldata" class="table table-striped table-bordered table-hover">
								<thead>
									<tr align="center">
										<th width="10%"><div align="center">Tanggal</div></th>
										<th><div align="center">Ruang Asal</div></th>
										<th><div align="center">Ruang Tujuan</div></th>
										<th><div align="center">Pengirim</div></th>
										<th><div align="center">Penerima</div></th>
										<th><div align="center">Verifikator</div></th>
										<th><div align="center">Total (%)</div></th>
										<th width="10%"><div align="center">Opsi</div></th>
									</tr>								
									
								</thead>

								<tbody>
									<?php					
									$query="select ti.ID_ISI, ti.TGL_INPUT, ti.TANGGAL, ti.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN,rut.DESKRIPSI RUANG_TUJUAN, ti.PENGIRIM, ti.PENERIMA, ti.MENGINFORMASI, ti.IDENTIFIKASI, ti.VERIFIKATOR
									, ti.MELENGKAPI, ti.MENGISI, ti.EDUKASI, ti.CUCI_TANGAN, ti.PAKAI_APD, ti.RUANG_ISOLASI, ti.BUANG_APD, ti.LINEN_KOTOR, ti.BED_BERSIH, ti.TOTAL, ti.`STATUS`
									from tb_tpia ti
									left join ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5
									left join ruangan rut ON rut.ID=ti.TUJUAN and rut.JENIS=5							
									where ti.`STATUS`=1";							
									$info=mysqli_query($conn1,$query); 
							//untuk penomoran data
							//$no=1;						
							//menampilkan data
									while($row=mysqli_fetch_array($info)){
										?>
										<tr>
											<td><?php echo $row['TANGGAL'] ?></td>
											<td><?php echo $row['RUANGAN'] ?></td>
											<td><?php echo $row['RUANG_TUJUAN'] ?></td>
											<td><?php echo $row['PENGIRIM'] ?></td>
											<td><?php echo $row['PENERIMA'] ?></td>	
											<td><?php echo $row['VERIFIKATOR'] ?></td>							
											<td><?php echo $row['TOTAL'] ?></td>						
											<td align="center" >
												<div class="hidden-sm hidden-xs action-buttons">
											<!-- <a class="blue" href="?page=det_&id=<?php echo $row['ID_ISI']?>">								
												<i class="glyphicon glyphicon-search btn btn-info btn-sm"></i>			
											</a> -->
											<a class="btn btn-success btn-xs" href="?page=edit_tpia&id=<?php echo $row['ID_ISI']?>">
												<i class="glyphicon glyphicon-pencil"></i>
											</a>
											<a class="btn btn-danger btn-xs" href="?page=simpan_tpia&id=<?php echo $row['ID_ISI']?>">
												<i class="glyphicon glyphicon-trash"></i>	
											</a>
											
										</div>
									</td>
								</tr>							
								<?php							
							}
							?>	
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->

