<script type="text/javascript">
	function tugas1()
	{
		var jumlah=0;
		var jumlahx=0;
		var nilai;

		if(document.getElementById("MENGINFORMASI").checked)
		{
			nilai=document.getElementById("MENGINFORMASI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("MENGINFORMASITDK").checked)
		{
			nilai=document.getElementById("MENGINFORMASITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("IDENTIFIKASI").checked)
		{
			nilai=document.getElementById("IDENTIFIKASI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("IDENTIFIKASITDK").checked)
		{
			nilai=document.getElementById("IDENTIFIKASITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("MELENGKAPI").checked)
		{
			nilai=document.getElementById("MELENGKAPI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("MELENGKAPITDK").checked)
		{
			nilai=document.getElementById("MELENGKAPITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("MENGISI").checked)
		{
			nilai=document.getElementById("MENGISI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("MENGISITDK").checked)
		{
			nilai=document.getElementById("MENGISITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("EDUKASI").checked)
		{
			nilai=document.getElementById("EDUKASI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("EDUKASITDK").checked)
		{
			nilai=document.getElementById("EDUKASITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("CUCI_TANGAN").checked)
		{
			nilai=document.getElementById("CUCI_TANGAN").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("CUCI_TANGANTDK").checked)
		{
			nilai=document.getElementById("CUCI_TANGANTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("PAKAI_APD").checked)
		{
			nilai=document.getElementById("PAKAI_APD").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("PAKAI_APDTDK").checked)
		{
			nilai=document.getElementById("PAKAI_APDTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("RUANG_ISOLASI").checked)
		{
			nilai=document.getElementById("RUANG_ISOLASI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("RUANG_ISOLASITDK").checked)
		{
			nilai=document.getElementById("RUANG_ISOLASITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("BUANG_APD").checked)
		{
			nilai=document.getElementById("BUANG_APD").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("BUANG_APDTDK").checked)
		{
			nilai=document.getElementById("BUANG_APDTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("LINEN_KOTOR").checked)
		{
			nilai=document.getElementById("LINEN_KOTOR").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("LINEN_KOTORTDK").checked)
		{
			nilai=document.getElementById("LINEN_KOTORTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("BED_BERSIH").checked)
		{
			nilai=document.getElementById("BED_BERSIH").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("BED_BERSIHTDK").checked)
		{
			nilai=document.getElementById("BED_BERSIHTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
	}
</script>
<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISI) AS last FROM tb_tpia WHERE ID_ISI LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Monitoring Transfer Pasien Infeksi Airborne</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Monitoring Transfer Pasien Infeksi Airborne</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_tpia" method="post">
									<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" autocomplete="off" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-2">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ASAL ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
													?>
													<option value="<?=$data[0]?>"><?=$data[3]?></option>
													<?php
												}
												?>	
											</select>
										</div>
										<div class="col-sm-2">										
											<select class="chosen-select form-control" name="TUJUAN" id="form-field-select-3" data-placeholder="[ RUANGAN TUJUAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
													?>
													<option value="<?=$data[0]?>"><?=$data[3]?></option>
													<?php
												}
												?>	
											</select>
										</div>										

										<div class="col-sm-2">											
											<input type="text" name="PENGIRIM" placeholder="[PTGS YG MEMINDAH]" class="form-control" />												
										</div>
										<div class="col-sm-2">											
											<input type="text" name="PENERIMA" placeholder="[PTGS YG MENRIMA]" class="form-control" />												
										</div>
										<div class="col-sm-2">											
											<input type="text" name="VERIFIKATOR" placeholder="[VERIFIKATOR]" class="form-control" />												
										</div>
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> KEGIATAN</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Menginformasikan ke ruang perawatan isolasi tekanan negatif bahwa akan ada pasien infeksi airborne  yang akan di transfer ke ruang tersebut</td>
														<td align="center"><label>
															<input type="checkbox" name="MENGINFORMASI[]" id="MENGINFORMASI" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
														</label></td>
														<td align="center">
															<label>
																<input name="MENGINFORMASI[]" type="checkbox" value="2" id="MENGINFORMASITDK" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Melakukan identifikasi pasien yang akan di transfer, meliputi: nama, tanggal lahir, nomor MR</td>
														<td align="center"><label>
															<input type="checkbox" name="IDENTIFIKASI[]" id="IDENTIFIKASI" value="1" class="ace input-lg" onClick="tugas1()">													
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center"><label>
														<input name="IDENTIFIKASI[]" type="checkbox" value="2" id="IDENTIFIKASITDK" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>3</td>
												<td>Melengkapi formulir pemindahan/ transfer pasien rawat</td>
												<td align="center">
													<label>
														<input type="checkbox" name="MELENGKAPI[]" id="MELENGKAPI" value="1" class="ace input-lg" onClick="tugas1()">
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="MELENGKAPI[]" type="checkbox" value="2" id="MELENGKAPITDK" class="ace input-lg" onClick="tugas1()"/>
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>4</td>
												<td>Mengisi form monitoring transfer pasien infeksi airborne</td>
												<td align="center">
													<label>
														<input name="MENGISI[]" type="checkbox" value="1" id="MENGISI" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="MENGISI[]" type="checkbox" value="2" id="MENGISITDK" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>5</td>
												<td>Petugas melakukan Edukasi pasien tentang etika batuk dan tidak membuang dahak di sembarang tempat.</td>
												<td align="center">
													<label>
														<input name="EDUKASI[]" type="checkbox" value="1" id="EDUKASI" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="EDUKASI[]" type="checkbox" value="2" id="EDUKASITDK" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>6</td>
												<td>Petugas melakukan kebersihan tangan sebelum dan sesudah mentransfer pasien infeksi airborne</td>
												<td align="center">
													<label>
														<input name="CUCI_TANGAN[]" type="checkbox" value="1" id="CUCI_TANGAN" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="CUCI_TANGAN[]" type="checkbox" value="2" id="CUCI_TANGANTDK" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>7</td>
												<td>Petugas yang mentransfer dan yang menerima pasien infeksi airborne menggunakan APD; masker N-95 (jika tidak tersedia sebagai alternatif bisa menggunakan masker bedah),  Untuk pasien yang sudah terdiagnosa infeksi airborne  wajib menggunakan masker bedah sebelum di transfer ke ruang perawatan</td>
												<td align="center">
													<label>
														<input name="PAKAI_APD[]" type="checkbox" value="1" id="PAKAI_APD" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="PAKAI_APD[]" type="checkbox" value="2" id="PAKAI_APDTDK" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>8</td>
												<td>Pasien ditempatkan di ruang isolasi tekanan negatif di ruang perawatan lantai 7 (Anggrek 2)</td>
												<td align="center">
													<label>
														<input name="RUANG_ISOLASI[]" type="checkbox" value="1" id="RUANG_ISOLASI" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="RUANG_ISOLASI[]" type="checkbox" value="2" id="RUANG_ISOLASITDK" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>9</td>
												<td>Setelah selesai melakukan transfer pasien Infeksi airborne, APD: masker, buang ke tempat sampah infeksius.</td>
												<td align="center">
													<label>
														<input name="BUANG_APD[]" type="checkbox" value="1" id="BUANG_APD" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="BUANG_APD[]" type="checkbox" value="2" id="BUANG_APDTDK" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>10</td>
												<td>Linen kotor (seprei, sarung bantal, selimut dan pakaian pasien) masukkan ke dalam plastik kuning untuk dikirim ke laundry.</td>
												<td align="center">
													<label>
														<input name="LINEN_KOTOR[]" type="checkbox" value="1" id="LINEN_KOTOR" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="LINEN_KOTOR[]" type="checkbox" value="2" id="LINEN_KOTORTDK" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td>11</td>
												<td>Bed yang digunakan saat transfer pasien infeksi airborne dibersihkan menggunakan cairan desinfektan : sterisid</td>
												<td align="center">
													<label>
														<input name="BED_BERSIH[]" type="checkbox" value="1" id="BED_BERSIH" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
												<td align="center">
													<label>
														<input name="BED_BERSIH[]" type="checkbox" value="2" id="BED_BERSIHTDK" class="ace input-lg" onClick="tugas1()" />
														<span class="lbl"></span>
													</label>
												</td>
											</tr>
											<tr>
												<td style="text-align:center" colspan="2">Total</td>
												<td style="text-align:center"></td>
												<td style="text-align:center"></td>
											</tr>
											<tr>
												<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
												<td colspan="2" style="text-align:center">
													<div class="input-group">
														<input name="TOTAL" type="text" id="total" style="width:60px"></br>
														<span class="input-group-addon">
															%
														</span>
													</div>														
												</td>														
											</tr>														
										</tbody>
									</table>											
								</div>
							</div>													

							<div class="form-group">							
								<div class="col-md-offset-3 col-md-9">
									<button class="btn btn-info" name="btnsimpan" type="submit">
										<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
										%
										Reset
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><!-- /.span -->
	</div>
</div><!-- /.page-content -->	
</div> <!-- container -->
</div><!-- /.main-content -->