<script type="text/javascript">

	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">FORMULIR AUDIT MANAJEMEN LIMBAH  </li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">FORMULIR AUDIT MANAJEMEN LIMBAH  </h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_limbah_new" method="post">										
									<div class="form-group">
										<div class="col-sm-1">	
											<label class="control-label bolder blue">TANGGAL</label>
										</div>								
										<div class="col-sm-3">									
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d') ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>												
										</div>
										<div class="col-sm-1">	
											<label class="control-label bolder blue">RUANGAN</label>
										</div>	
										<div class="col-sm-5">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0 and r.STATUS=1";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
													?>
													<option value="<?=$data[0]?>"><?=$data[3]?></option>
													<?php
												}
												?>	
											</select>
										</div>	
									</div>

									<div class="form-group">																				
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> KEGIATAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="5%" style="text-align:center">NA</th>
															<th width="25%" style="text-align:center">KETERANGAN</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>A.</td>
															<td>PEMILAHAN SAMPAH DARI UNIT
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Limbah Infeksius ditempatkan di plastik kuning
															</td>
															<td align="center">
																<label><input type="radio" name="A1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_A1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Limbah Non Infeksius ditempatkan di plastik hitam
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_A2" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Limbah Benda Tajam dimasukkan di safety box
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_A3" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Limbah Botol Kaca dan palbote infus di tempatkan di plastik transparan
															</td>
															<td align="center">
																<label>
																	<input name="A4" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A4" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A4" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_A4" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>5
															</td>
															<td>Limbah Sitostatika ditempatkan di plastik ungu
															</td>
															<td align="center">
																<label>
																	<input name="A5" type="radio" value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A5" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A5" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_A5" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>B.</td>
															<td>TEMPAT  PEMBUANGAN LIMBAH DI UNIT
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Limbah Benda Tajam safety box diganti jika sudah 3/4 penuh
															</td>
															<td align="center"><label>
																<input type="radio" name="B1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="B1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="B1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_B1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>a
															</td>
															<td>Penggantian plastik sampah dilakukan  jika sudah  3/4 penuh
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BA" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BA" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BA" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_BA" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>b
															</td>
															<td>Safety box harus tahan bocor dan tahan tusukan
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BB" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BB" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BB" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_BB" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>C.</td>
															<td>TRANSPORTASI LIMBAH
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1
															</td>
															<td>Penggunaan APD Lengkap
															</td>
															<td align="center">																
															</td>
															<td align="center">
																
															</td>
															<td align="center">
																
															</td>
															<td>
																
															</td>
														</tr>
														<tr>
															<td>a</td>
															<td>Masker
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1A" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>

															<td align="center">
																<label>
																	<input type="radio" name="C1A" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1A" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_C1A" placeholder="" class="form-control" />	
															</td>
														</tr>
														<tr>
															<td>b</td>
															<td>Apron
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1B" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1B" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1B" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_C1B" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>c</td>
															<td>Sarung Tangan
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1C" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1C" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1C" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_C1C" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>d</td>
															<td>Safety Shoes
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1D" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1D" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C1D" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_C1D" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Solobin Tertutup untuk mengangkut limbah
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_C2" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>3</td>
															<td>Solobin selalu dalam keadaan bersih
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>

															<td align="center">
																<label>
																	<input type="radio" name="C3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="C3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_C3" placeholder="" class="form-control" />	
															</td>
														</tr>
														
														<tr>
															<td>D.</td>
															<td>PENGELOLAAN LIMBAH CAIRAN TUBUH INFEKSIUS
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Cairan di buang di spoelhoek/ lubang pembuangan limbah cairan tubuh infeksius
															</td>
															<td align="center"><label>
																<input type="radio" name="D1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>

															<td align="center">
																<label>
																	<input name="D1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="D1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_D1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Menggunakan APD : Sarung tangan dan masker
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="D2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="D2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="D2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_D2" placeholder="" class="form-control" />	
															</td>
														</tr>
														
														<tr>
															<td>E.</td>
															<td>PENANGANAN DAN PEMBUANGAN DARAH SERTA KOMPONEN DARAH DI LABORAT
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Darah dan komponen darah yang tidak terpakai di buang di limbah infeksius
															</td>
															<td align="center"><label>
																<input type="radio" name="E1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>

															<td align="center">
																<label>
																	<input name="E1" type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="E1" type="radio" value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_E1" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>F.</td>
															<td>PEMILAHAN LIMBAH DI TPS
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Penggunaan APD Lengkap
															</td>
															<td align="center"></td>

															<td align="center">
																
															</td>
															<td align="center">
																
															</td>
															<td>
																
															</td>
														</tr>
														<tr>
															<td>a</td>
															<td>Masker
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1A" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1A" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1A" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F1A" placeholder="" class="form-control" />	
															</td>
														</tr>
														<tr>
															<td>b</td>
															<td>Apron
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1B" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1B" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1B" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F1B" placeholder="" class="form-control" />	
															</td>
														</tr>	
														<tr>
															<td>c</td>
															<td>Sarung Tangan
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1C" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1C" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1C" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F1C" placeholder="" class="form-control" />	
															</td>
														</tr>
														<tr>
															<td>d</td>
															<td>Safety Shoes
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1D" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1D" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F1D" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F1D" placeholder="" class="form-control" />	
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Ketersediaan Wastafel Cuci Tangan
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F2" placeholder="" class="form-control" />	
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Ketersediaan Sabun & paper towel untuk Cuci Tangan
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F3" placeholder="" class="form-control" />	
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Eye Washer tersedia dan berfungsi dengan baik
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F4" placeholder="" class="form-control" />	
															</td>
														</tr>
														<tr>
															<td>5</td>
															<td>Tempat Limbah Di TPS sesuai antara yang infeksius dan non infeksius
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F5" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F5" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F5" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F5" placeholder="" class="form-control" />	
															</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Tempat TPS Bersih
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F6" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F6" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F6" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F6" placeholder="" class="form-control" />	
															</td>
														</tr>
														<tr>
															<td>7</td>
															<td>Sampah di TPS diangkut  setiap hari
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F7" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F7" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="F7" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" name="KET_F7" placeholder="" class="form-control" />	
															</td>
														</tr>															
														<tr>
															<td style="text-align:center" colspan="4">JUMLAH TOTAL</td>
															<td colspan="3" style="text-align:center">
																<div class="input-group">
																	<input type="text" id="total" name="NILAI" placeholder="[ NILAI ]" class="form-control" />
																	<span class="input-group-addon">
																		%
																	</span>
																</div>													
															</td>														
														</tr>													
													</tbody>
												</table>																									
											</div>
										</div>
									</div>																										
									<hr />
									<div class="form-group">										
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
											<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
										</div>				
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
											<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
										</div>
									</div>
									<div class="form-group">			
										<div class="col-md-6 col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
											<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
										</div>	
										<div class="col-md-6 col-sm-12">	
											<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
											<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
										</div>								
									</div>																											
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	