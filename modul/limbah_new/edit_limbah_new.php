<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#SS:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>

<?php
$id=($_GET['id']);
$query="select * from db_ppi.tb_limbah_new where ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">FORMULIR AUDIT MANAJEMEN LIMBAH</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">FORMULIR AUDIT MANAJEMEN LIMBAH</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_limbah_new" method="post">
								<input type="hidden" value="<?php echo $dt['ID']; ?>"  name="ID" >										
									<div class="form-group">
										<div class="col-sm-1">	
											<label class="control-label bolder blue">TANGGAL</label>
										</div>								
										<div class="col-sm-3">									
											<div class="input-group">
												<input class="form-control" value="<?php echo $dt['TANGGAL']; ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>												
										</div>
										<div class="col-sm-1">	
											<label class="control-label bolder blue">RUANGAN</label>
										</div>
										<div class="col-sm-5">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" value="<?php echo $dt['RUANGAN']; ?>" data-placeholder="[ RUANGAN ]">
													<option value=""></option>
													<?php
													$sql = "select * from db_ppi.ruangan r
													where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
													$rs = mysqli_query($conn1,$sql);
													while ($data = mysqli_fetch_array($rs)) {
														?>
														<option <?php if($dt['RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
														<?php
													}
													?>	
												</select>
										</div>
									</div>

									<div class="form-group">																				
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> KEGIATAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="5%" style="text-align:center">NA</th>
															<th width="25%" style="text-align:center">KETERANGAN</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>A.</td>
															<td>KEBERSIHAN DAPUR</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Limbah Infeksius ditempatkan di plastik kuning
															</td>
															<td align="center">
																<label><input type="radio" <?php if($dt['A1']=='1') echo " checked "?> name="A1" id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" <?php if($dt['A1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A1" type="radio" <?php if($dt['A1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A1']; ?>" name="KET_A1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>2
															</td>
															<td>Limbah Non Infeksius ditempatkan di plastik hitam
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A2" <?php if($dt['A2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A2']; ?>" name="KET_A2" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Limbah Benda Tajam dimasukkan di safety box
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" <?php if($dt['A3']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" <?php if($dt['A3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="A3" <?php if($dt['A3']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A3']; ?>" name="KET_A3" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Limbah Botol Kaca dan palbote infus di tempatkan di plastik transparan
															</td>
															<td align="center">
																<label>
																	<input name="A4" type="radio" <?php if($dt['A4']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A4" type="radio" <?php if($dt['A4']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A4" type="radio" <?php if($dt['A4']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A4']; ?>" name="KET_A4" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>5
															</td>
															<td>Limbah Sitostatika ditempatkan di plastik ungu
															</td>
															<td align="center">
																<label>
																	<input name="A5" type="radio" <?php if($dt['A5']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A5" type="radio" <?php if($dt['A5']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="A5" type="radio" <?php if($dt['A5']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_A5']; ?>" name="KET_A5" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														<tr>
															<td>B.</td>
															<td>TEMPAT  PEMBUANGAN LIMBAH DI UNIT
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1</td>
															<td>Limbah Benda Tajam safety box diganti jika sudah 3/4 penuh
															</td>
															<td align="center"><label>
																<input type="radio" name="B1" <?php if($dt['B1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="B1" <?php if($dt['B1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="B1" type="radio" <?php if($dt['B1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_B1']; ?>" name="KET_B1" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>a
															</td>
															<td>Penggantian plastik sampah dilakukan  jika sudah  3/4 penuh
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BA" <?php if($dt['BA']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BA" <?php if($dt['BA']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BA" <?php if($dt['BA']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_BA']; ?>" name="KET_BA" placeholder="" class="form-control" />		
															</td>
														</tr>
														<tr>
															<td>b
															</td>
															<td>Safety box harus tahan bocor dan tahan tusukan
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BB" <?php if($dt['BB']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BB" <?php if($dt['BB']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input type="radio" name="BB" <?php if($dt['BB']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td>
																<input type="text" id="nama" value="<?php echo $dt['KET_BB']; ?>" name="KET_BB" placeholder="" class="form-control" />		
															</td>
														</tr>
														
														
														<tr>
															<td>C.</td>
															<td>TRANSPORTASI LIMBAH
															</td>
															<td></td>
															<td></td>
															<td></td>
														</tr>
														<tr>
															<td>1
															</td>
															<td>Penggunaan APD Lengkap
															</td>
															<td align="center">																
																</td>
																<td align="center">
																	
																</td>
																<td align="center">
																	
																</td>
																<td>
																			
																</td>
															</tr>
															<tr>
																<td>a</td>
																<td>Masker
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1A" <?php if($dt['C1A']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>

																<td align="center">
																	<label>
																		<input type="radio" name="C1A" <?php if($dt['C1A']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1A" <?php if($dt['C1A']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1A']; ?>" name="KET_C1A" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>b</td>
																<td>Apron
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1B" <?php if($dt['C1B']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1B" <?php if($dt['C1B']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1B" <?php if($dt['C1B']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1B']; ?>" name="KET_C1B" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>c</td>
																<td>Sarung Tangan
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1C" <?php if($dt['C1C']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1C" <?php if($dt['C1C']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1C" <?php if($dt['C1C']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1C']; ?>" name="KET_C1C" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>d</td>
																<td>Safety Shoes
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1D" <?php if($dt['C1D']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1D" <?php if($dt['C1D']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C1D" <?php if($dt['C1D']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C1D']; ?>" name="KET_C1D" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Solobin Tertutup untuk mengangkut limbah
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2" <?php if($dt['C2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2" <?php if($dt['C2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C2" <?php if($dt['C2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C2']; ?>" name="KET_C2" placeholder="" class="form-control" />		
																</td>
															</tr>
															
															<tr>
																<td>3</td>
																<td>Solobin selalu dalam keadaan bersih
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C3" <?php if($dt['C3']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>

																<td align="center">
																	<label>
																		<input type="radio" name="C3" <?php if($dt['C3']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="C3" <?php if($dt['C3']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_C3']; ?>" name="KET_C3" placeholder="" class="form-control" />	
																</td>
															</tr>
															
															<tr>
																<td>D.</td>
																<td>PENGELOLAAN LIMBAH CAIRAN TUBUH INFEKSIUS
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Cairan di buang di spoelhoek/ lubang pembuangan limbah cairan tubuh infeksius
																</td>
																<td align="center"><label>
																	<input type="radio" name="D1" <?php if($dt['D1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="D1" type="radio" <?php if($dt['D1']=='2') echo " checked "?> value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="D1" type="radio" <?php if($dt['D1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D1']; ?>" name="KET_D1" placeholder="" class="form-control" />		
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Menggunakan APD : Sarung tangan dan masker
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" <?php if($dt['D2']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" <?php if($dt['D2']=='2') echo " checked "?> id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" name="D2" <?php if($dt['D2']=='3') echo " checked "?> id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_D2']; ?>" name="KET_D2" placeholder="" class="form-control" />	
																</td>
															</tr>
															
															<tr>
																<td>E.</td>
																<td>PENANGANAN DAN PEMBUANGAN DARAH SERTA KOMPONEN DARAH DI LABORAT
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Darah dan komponen darah yang tidak terpakai di buang di limbah infeksius
																</td>
																<td align="center"><label>
																	<input type="radio" name="E1" <?php if($dt['E1']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">											
																	<span class="lbl"></span>
																</label></td>

																<td align="center">
																	<label>
																		<input name="E1" <?php if($dt['E1']=='2') echo " checked "?> type="radio" value="2" id="SS" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input name="E1" type="radio" <?php if($dt['E1']=='3') echo " checked "?> value="3" id="AA" class="ace input-lg" onClick="tugas1()" />
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_E1']; ?>" name="KET_E1" placeholder="" class="form-control" />		
																</td>
															</tr>
															
															<tr>
																<td>F.</td>
																<td>PEMILAHAN LIMBAH DI TPS
																</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
															<tr>
																<td>1</td>
																<td>Penggunaan APD Lengkap
																</td>
																<td align="center"></td>

																<td align="center">
																	
																</td>
																<td align="center">
																	
																</td>
																<td>
																		
																</td>
															</tr>
															<tr>
																<td>a</td>
																<td>Masker
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1A']=='1') echo " checked "?> name="F1A" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1A']=='2') echo " checked "?> name="F1A" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1A']=='3') echo " checked "?> name="F1A" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F1A']; ?>" name="KET_F1A" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>b</td>
																<td>Apron
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1B']=='1') echo " checked "?> name="F1B" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1B']=='2') echo " checked "?> name="F1B" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1B']=='3') echo " checked "?> name="F1B" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F1B']; ?>" name="KET_F1B" placeholder="" class="form-control" />	
																</td>
															</tr>

															<tr>
																<td>c</td>
																<td>Sarung Tangan
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1C']=='1') echo " checked "?> name="F1C" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1C']=='2') echo " checked "?> name="F1C" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1C']=='3') echo " checked "?> name="F1C" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F1C']; ?>" name="KET_F1C" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>d</td>
																<td>Safety Shoes
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1D']=='1') echo " checked "?> name="F1D" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1D']=='2') echo " checked "?> name="F1D" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F1D']=='3') echo " checked "?> name="F1D" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F1D']; ?>" name="KET_F1D" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>2</td>
																<td>Ketersediaan Wastafel Cuci Tangan
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F2']=='1') echo " checked "?> name="F2" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F2']=='2') echo " checked "?> name="F2" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F2']=='3') echo " checked "?> name="F2" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F2']; ?>" name="KET_F2" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>3</td>
																<td>Ketersediaan Sabun & paper towel untuk Cuci Tangan
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F3']=='1') echo " checked "?> name="F3" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F3']=='2') echo " checked "?> name="F3" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F3']=='3') echo " checked "?> name="F3" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F3']; ?>" name="KET_F3" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>4</td>
																<td>Eye Washer tersedia dan berfungsi dengan baik
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F4']=='1') echo " checked "?> name="F4" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F4']=='2') echo " checked "?> name="F4" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F4']=='3') echo " checked "?> name="F4" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F4']; ?>" name="KET_F4" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>5</td>
																<td>Tempat Limbah Di TPS sesuai antara yang infeksius dan non infeksius
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F5']=='1') echo " checked "?> name="F5" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F5']=='2') echo " checked "?> name="F5" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F5']=='3') echo " checked "?> name="F5" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F5']; ?>" name="KET_F5" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>6</td>
																<td>Tempat TPS Bersih
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F6']=='1') echo " checked "?> name="F6" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F6']=='2') echo " checked "?> name="F6" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F6']=='3') echo " checked "?> name="F6" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F6']; ?>" name="KET_F6" placeholder="" class="form-control" />	
																</td>
															</tr>
															<tr>
																<td>7</td>
																<td>Sampah di TPS diangkut  setiap hari
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F7']=='1') echo " checked "?> name="F7" id="AA" value="1" class="ace input-lg" onClick="tugas1()">		
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F7']=='2') echo " checked "?> name="F7" id="SS" value="2" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td align="center">
																	<label>
																		<input type="radio" <?php if($dt['F7']=='3') echo " checked "?> name="F7" id="AA" value="3" class="ace input-lg" onClick="tugas1()">
																		<span class="lbl"></span>
																	</label>
																</td>
																<td>
																	<input type="text" id="nama" value="<?php echo $dt['KET_F7']; ?>" name="KET_F7" placeholder="" class="form-control" />	
																</td>
															</tr>															
															<tr>
																<td style="text-align:center" colspan="4">JUMLAH TOTAL</td>
																<td colspan="3" style="text-align:center">
																	<div class="input-group">
																		<input type="text" id="total" value="<?php echo $dt['NILAI']; ?>" name="NILAI" placeholder="[ NILAI ]" class="form-control" />
																		<span class="input-group-addon">
																			%
																		</span>
																	</div>													
																</td>														
															</tr>													
														</tbody>
													</table>																									
												</div>
											</div>
										</div>																										
										<hr />
										<div class="form-group">										
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
												<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
											</div>				
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
												<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
											</div>
										</div>
										<div class="form-group">			
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
												<input type="text" id="nama" value="<?php echo $dt['KEPALA']; ?>" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
											</div>	
											<div class="col-md-6 col-sm-12">	
												<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
												<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
											</div>								
										</div>																											
										<hr />
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->		
<?php }
?>