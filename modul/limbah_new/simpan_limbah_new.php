<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					if(isset($_POST['btnsimpan'])){
						$TANGGAL 			= $_POST['TANGGAL'];
						$RUANGAN 			= $_POST['RUANGAN'];

						if(isset($_POST['A1'])){
							$A1 = $_POST['A1'];
						}else{
							$A1 = "";
						}	

						if(isset($_POST['A2'])){
							$A2 = $_POST['A2'];
						}else{
							$A2 = "";
						}

						if(isset($_POST['A3'])){
							$A3 = $_POST['A3'];
						}else{
							$A3 = "";
						}

						if(isset($_POST['A4'])){
							$A4 = $_POST['A4'];
						}else{
							$A4 = "";
						}

						if(isset($_POST['A5'])){
							$A5 = $_POST['A5'];
						}else{
							$A5 = "";
						}

						if(isset($_POST['B1'])){
							$B1 = $_POST['B1'];
						}else{
							$B1 = "";
						}

						if(isset($_POST['BA'])){
							$BA = $_POST['BA'];
						}else{
							$BA = "";
						}

						if(isset($_POST['BB'])){
							$BB = $_POST['BB'];
						}else{
							$BB = "";
						}

						if(isset($_POST['C1A'])){
							$C1A = $_POST['C1A'];
						}else{
							$C1A = "";
						}

						if(isset($_POST['C1B'])){
							$C1B = $_POST['C1B'];
						}else{
							$C1B = "";
						}

						if(isset($_POST['C1C'])){
							$C1C = $_POST['C1C'];
						}else{
							$C1C = "";
						}

						if(isset($_POST['C1D'])){
							$C1D = $_POST['C1D'];
						}else{
							$C1D = "";
						}

						if(isset($_POST['C2'])){
							$C2 = $_POST['C2'];
						}else{
							$C2 = "";
						}

						if(isset($_POST['C3'])){
							$C3 = $_POST['C3'];
						}else{
							$C3 = "";
						}

						if(isset($_POST['D1'])){
							$D1 = $_POST['D1'];
						}else{
							$D1 = "";
						}

						if(isset($_POST['D2'])){
							$D2 = $_POST['D2'];
						}else{
							$D2 = "";
						}	

						if(isset($_POST['E1'])){
							$E1 = $_POST['E1'];
						}else{
							$E1 = "";
						}	

						if(isset($_POST['F1A'])){
							$F1A = $_POST['F1A'];
						}else{
							$F1A = "";
						}	

						if(isset($_POST['F1B'])){
							$F1B = $_POST['F1B'];
						}else{
							$F1B = "";
						}

						if(isset($_POST['F1C'])){
							$F1C = $_POST['F1C'];
						}else{
							$F1C = "";
						}

						if(isset($_POST['F1D'])){
							$F1D = $_POST['F1D'];
						}else{
							$F1D = "";
						}

						if(isset($_POST['F2'])){
							$F2 = $_POST['F2'];
						}else{
							$F2 = "";
						}

						if(isset($_POST['F3'])){
							$F3 = $_POST['F3'];
						}else{
							$F3 = "";
						}

						if(isset($_POST['F4'])){
							$F4 = $_POST['F4'];
						}else{
							$F4 = "";
						}

						if(isset($_POST['F5'])){
							$F5 = $_POST['F5'];
						}else{
							$F5 = "";
						}

						if(isset($_POST['F6'])){
							$F6 = $_POST['F6'];
						}else{
							$F6 = "";
						}

						if(isset($_POST['F7'])){
							$F7 = $_POST['F7'];
						}else{
							$F7 = "";
						}

						$KET_A1				= $_POST['KET_A1']; 
						$KET_A2				= $_POST['KET_A2']; 						
						$KET_A3 			= $_POST['KET_A3'];		
						$KET_A4				= $_POST['KET_A4'];  						
						$KET_A5 			= $_POST['KET_A5'];

						$KET_B1 			=	$_POST['KET_B1'];  					
						$KET_BA 			=	$_POST['KET_BA'];  					
						$KET_BB 			=	$_POST['KET_BB'];  

						$KET_C1A 			=	$_POST['KET_C1A'];  					
						$KET_C1B			=	$_POST['KET_C1B'];  					
						$KET_C1C 			=	$_POST['KET_C1C']; 					 
						$KET_C1D 			=	$_POST['KET_C1D']; 						 
						$KET_C2 			=	$_POST['KET_C2']; 						
						$KET_C3 			=	$_POST['KET_C3'];  

						$KET_D1 			=	$_POST['KET_D1']; 						
						$KET_D2 			=	$_POST['KET_D2']; 

						$KET_E1 			=	$_POST['KET_E1']; 
						

						$KET_F1A 			=	$_POST['KET_F1A']; 						
						$KET_F1B 			=	$_POST['KET_F1B']; 						
						$KET_F1C 			=	$_POST['KET_F1C'];						
						$KET_F1D 			=	$_POST['KET_F1D']; 					
						$KET_F2 			=	$_POST['KET_F2']; 						
						$KET_F3 			=	$_POST['KET_F3'];					
						$KET_F4 			=	$_POST['KET_F4'];
						$KET_F5 			=	$_POST['KET_F5'];					
						$KET_F6 			=	$_POST['KET_F6'];						
						$KET_F7 			=	$_POST['KET_F7'];						
						
						$NILAI 				=	$_POST['NILAI'];  
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 			=	addslashes($_POST['AUDITOR']); 
						$pengisi 			= $_SESSION['userid'];							
						// echo "<pre>";print_r($_POST);exit();                	
						$query = "INSERT INTO db_ppi.tb_limbah_new (
							USER,
							TANGGAL,
							RUANGAN,
							A1, 				
							KET_A1,				 
							A2,					
							KET_A2,				 
							A3,					
							KET_A3, 				
							A4,					
							KET_A4,				  
							A5, 				
							KET_A5, 			

							B1, 				
							KET_B1, 			  
							BA,				
							KET_BA,			  
							BB,				
							KET_BB, 			  


							C1A, 			
							KET_C1A,		  
							C1B, 			
							KET_C1B,	  
							C1C, 			 
							KET_C1C, 		 
							C1D, 			 
							KET_C1D, 			
							C2, 				 
							KET_C2, 			 
							C3, 				
							KET_C3, 			  

							D1, 				
							KET_D1, 			 
							D2, 				
							KET_D2, 			 

							E1, 				
							KET_E1, 			 

							F1A, 				
							KET_F1A, 			
							F1B, 			
							KET_F1B, 			
							F1C, 			
							KET_F1C, 		
							F1D, 			
							KET_F1D, 			
							F2, 				
							KET_F2, 			 
							F3, 				
							KET_F3, 			
							F4, 				
							KET_F4, 			
							F5, 				
							KET_F5, 			
							F6, 				
							KET_F6, 			
							F7, 				
							KET_F7, 						

							NILAI, 
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							AUDITOR) 
						VALUES 
						('$pengisi',
							'$TANGGAL', 
							'$RUANGAN',
							'$A1', 				
							'$KET_A1',				 
							'$A2',					
							'$KET_A2',				 
							'$A3',					
							'$KET_A3', 				
							'$A4',					
							'$KET_A4',				  
							'$A5', 				
							'$KET_A5', 			

							'$B1', 				
							'$KET_B1', 			  
							'$BA', 				
							'$KET_BA', 			  
							'$BB', 				
							'$KET_BB', 			  

							'$C1A', 			
							'$KET_C1A', 		  
							'$C1B', 			
							'$KET_C1B',		  
							'$C1C', 			 
							'$KET_C1C', 		 
							'$C1D', 			 
							'$KET_C1D', 			
							'$C2', 				 
							'$KET_C2', 			 
							'$C3', 				
							'$KET_C3', 			  

							'$D1', 				
							'$KET_D1', 			 
							'$D2', 				
							'$KET_D2', 			 

							'$E1', 				
							'$KET_E1', 			 

							'$F1A', 				
							'$KET_F1A', 			
							'$F1B', 			
							'$KET_F1B', 			
							'$F1C', 			
							'$KET_F1C', 		
							'$F1D', 			
							'$KET_F1D', 			
							'$F2', 				
							'$KET_F2', 			 
							'$F3', 				
							'$KET_F3', 			
							'$F4', 				
							'$KET_F4', 			
							'$F5', 				
							'$KET_F5', 			
							'$F6', 				
							'$KET_F6', 			
							'$F7', 				
							'$KET_F7', 			 

							'$NILAI',  
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA', 
							'$AUDITOR')";
						//echo $query; die();	
						$hasil=mysqli_query($conn1,$query);								
						if($hasil){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_limbah_new'</script>"; 
						}                     
						else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
						}   
					}					
					if(isset($_POST['btnEdit'])){
						$ID			= $_POST['ID'];
						$TANGGAL 			= $_POST['TANGGAL'];
						$RUANGAN 			= $_POST['RUANGAN'];	

												if(isset($_POST['A1'])){
							$A1 = $_POST['A1'];
						}else{
							$A1 = "";
						}	

						if(isset($_POST['A2'])){
							$A2 = $_POST['A2'];
						}else{
							$A2 = "";
						}

						if(isset($_POST['A3'])){
							$A3 = $_POST['A3'];
						}else{
							$A3 = "";
						}

						if(isset($_POST['A4'])){
							$A4 = $_POST['A4'];
						}else{
							$A4 = "";
						}

						if(isset($_POST['A5'])){
							$A5 = $_POST['A5'];
						}else{
							$A5 = "";
						}

						if(isset($_POST['B1'])){
							$B1 = $_POST['B1'];
						}else{
							$B1 = "";
						}

						if(isset($_POST['BA'])){
							$BA = $_POST['BA'];
						}else{
							$BA = "";
						}

						if(isset($_POST['BB'])){
							$BB = $_POST['BB'];
						}else{
							$BB = "";
						}

						if(isset($_POST['C1A'])){
							$C1A = $_POST['C1A'];
						}else{
							$C1A = "";
						}

						if(isset($_POST['C1B'])){
							$C1B = $_POST['C1B'];
						}else{
							$C1B = "";
						}

						if(isset($_POST['C1C'])){
							$C1C = $_POST['C1C'];
						}else{
							$C1C = "";
						}

						if(isset($_POST['C1D'])){
							$C1D = $_POST['C1D'];
						}else{
							$C1D = "";
						}

						if(isset($_POST['C2'])){
							$C2 = $_POST['C2'];
						}else{
							$C2 = "";
						}

						if(isset($_POST['C3'])){
							$C3 = $_POST['C3'];
						}else{
							$C3 = "";
						}

						if(isset($_POST['D1'])){
							$D1 = $_POST['D1'];
						}else{
							$D1 = "";
						}

						if(isset($_POST['D2'])){
							$D2 = $_POST['D2'];
						}else{
							$D2 = "";
						}	

						if(isset($_POST['E1'])){
							$E1 = $_POST['E1'];
						}else{
							$E1 = "";
						}	

						if(isset($_POST['F1A'])){
							$F1A = $_POST['F1A'];
						}else{
							$F1A = "";
						}	

						if(isset($_POST['F1B'])){
							$F1B = $_POST['F1B'];
						}else{
							$F1B = "";
						}

						if(isset($_POST['F1C'])){
							$F1C = $_POST['F1C'];
						}else{
							$F1C = "";
						}

						if(isset($_POST['F1D'])){
							$F1D = $_POST['F1D'];
						}else{
							$F1D = "";
						}

						if(isset($_POST['F2'])){
							$F2 = $_POST['F2'];
						}else{
							$F2 = "";
						}

						if(isset($_POST['F3'])){
							$F3 = $_POST['F3'];
						}else{
							$F3 = "";
						}

						if(isset($_POST['F4'])){
							$F4 = $_POST['F4'];
						}else{
							$F4 = "";
						}

						if(isset($_POST['F5'])){
							$F5 = $_POST['F5'];
						}else{
							$F5 = "";
						}

						if(isset($_POST['F6'])){
							$F6 = $_POST['F6'];
						}else{
							$F6 = "";
						}

						if(isset($_POST['F7'])){
							$F7 = $_POST['F7'];
						}else{
							$F7 = "";
						}

						$KET_A1				= $_POST['KET_A1']; 
						$KET_A2				= $_POST['KET_A2']; 						
						$KET_A3 			= $_POST['KET_A3'];		
						$KET_A4				= $_POST['KET_A4'];  						
						$KET_A5 			= $_POST['KET_A5'];
						
						$KET_B1 			=	$_POST['KET_B1'];  					
						$KET_BA 			=	$_POST['KET_BA'];  					
						$KET_BB 			=	$_POST['KET_BB'];  
						
						$KET_C1A 			=	$_POST['KET_C1A'];  					
						$KET_C1B			=	$_POST['KET_C1B'];  					
						$KET_C1C 			=	$_POST['KET_C1C']; 					 
						$KET_C1D 			=	$_POST['KET_C1D']; 						 
						$KET_C2 			=	$_POST['KET_C2']; 						
						$KET_C3 			=	$_POST['KET_C3'];  
						
						$KET_D1 			=	$_POST['KET_D1']; 						
						$KET_D2 			=	$_POST['KET_D2']; 
						
						$KET_E1 			=	$_POST['KET_E1']; 
						
						
						$KET_F1A 			=	$_POST['KET_F1A']; 						
						$KET_F1B 			=	$_POST['KET_F1B']; 						
						$KET_F1C 			=	$_POST['KET_F1C'];						
						$KET_F1D 			=	$_POST['KET_F1D']; 					
						$KET_F2 			=	$_POST['KET_F2']; 						
						$KET_F3 			=	$_POST['KET_F3'];					
						$KET_F4 			=	$_POST['KET_F4'];
						$KET_F5 			=	$_POST['KET_F5'];					
						$KET_F6 			=	$_POST['KET_F6'];						
						$KET_F7 			=	$_POST['KET_F7'];	

						$NILAI 				=	$_POST['NILAI'];  
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 			=	addslashes($_POST['AUDITOR']); 						
						$USER 				= $_SESSION['userid'];		
						// echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_limbah_new SET												
						TANGGAL 		= '$TANGGAL',
						RUANGAN 		= '$RUANGAN',						
						A1 				= '$A1',
						KET_A1			= '$KET_A1', 
						A2				= '$A2',
						KET_A2			= '$KET_A2', 
						A3				= '$A3',
						KET_A3 			= '$KET_A3',	
						A4				= '$A4',
						KET_A4			= '$KET_A4',  
						A5 				= '$A5',
						KET_A5 			= '$KET_A5',
						
						B1 				= 	'$B1',
						KET_B1 			=	'$KET_B1',  
						BA 				=	'$BA',
						KET_BA 			=	'$KET_BA',  
						BB 				=	'$BB',
						KET_BB 			=	'$KET_BB',  
						C1A 			=	'$C1A',
						KET_C1A 		=	'$KET_C1A',  
						C1B 			=	'$C1B',
						KET_C1B			=	'$KET_C1B',  
						C1C 			=	'$C1C', 
						KET_C1C 		=	'$KET_C1C', 
						C1D 			=	'$C1D', 
						KET_C1D 		=	'$KET_C1D', 
						C2 				=	'$C2', 
						KET_C2 			=	'$KET_C2', 
						C3 				=	'$C3',
						KET_C3 			=	'$KET_C3',  
						
						D1 				=	'$D1',
						KET_D1 			=	'$KET_D1', 
						D2 				=	'$D2',
						KET_D2 			=	'$KET_D2', 
						
						E1 				=	'$E1',
						KET_E1 			=	'$KET_E1', 
						
						F1A 			=	'$F1A',
						KET_F1A 		=	'$KET_F1A', 
						F1B 			=	'$F1B',
						KET_F1B 		=	'$KET_F1B', 
						F1C 			=	'$F1C',
						KET_F1C 		=	'$KET_F1C',
						F1D 			=	'$F1D',
						KET_F1D 		=	'$KET_F1D', 
						F2 				=	'$F2',
						KET_F2 			=	'$KET_F2', 
						F3 				=	'$F3',
						KET_F3 			=	'$KET_F3',
						F4 				=	'$F4',
						KET_F4 			=	'$KET_F4',
						F5 				=	'$F5',
						KET_F5 			=	'$KET_F5',
						F6 				=	'$F6',
						KET_F6 			=	'$KET_F6',
						F7 				=	'$F7',
						KET_F7 			=	'$KET_F7',
						NILAI 				=	'$NILAI', 
						ANALISA 			=	'$ANALISA', 
						TINDAKLANJUT 		=	'$TINDAKLANJUT', 
						KEPALA 				=	'$KEPALA', 
						AUDITOR 			=	'$AUDITOR',						
						DIUBAH_OLEH='$USER'
						where ID='$ID'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_limbah_new'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_limbah_new ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_limbah_new'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_limbah_new'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			