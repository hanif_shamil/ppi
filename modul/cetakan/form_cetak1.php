<style>
	.button {
		background-color: white;
		border: 1px solid #ddd;
		color: #3e4752;
		padding: 15px 32px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		margin: 4px 2px;
		cursor: pointer;
	}
	.button {width: 100%; height:100px}
</style>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Cetakan</li>
			</ul><!-- /.breadcrumb -->			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Form cetakan</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<div class="row">
									<div class="col-md-12">
										<form class="form-horizontal" action="modul/cetakan/cetak_laporan.php" method="post" target="_blank">			
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-5 control-label no-padding-right" for="form-field-1"> Periode Awal :</label>
													<div class="col-md-7">
														<div class='input-group date' id='datetimepicker'>
															<input type='text' value="<?php echo date('Y/m/d H:i:s') ?>" name="tanggal1" id="tanggal2" class="form-control" />	
															<span class="input-group-addon">
																<span class="glyphicon glyphicon-calendar"></span>
															</span>															
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-5 control-label no-padding-right" for="form-field-1-1"> Periode Akhir :</label>
													<div class="col-md-7">
														<div class='input-group date' id='datetimepicker1'>															
															<input type='text' value="<?php echo date('Y/m/d H:i:s') ?>" name="tanggal2" id="tanggal2" class="form-control" />	
															<span class="input-group-addon">
																<span class="glyphicon glyphicon-calendar"></span>
															</span>															
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="col-md-5 control-label no-padding-right" for="form-field-1-1"> Ruangan :</label>
													<div class="col-md-7">
														<div class="control-group">										
															<select class="chosen-select form-control" name="ruangan" id="form-field-select-3" data-placeholder="[ Ruangan ]">
																<option value="1"></option>
																<?php
																$sql = "select * from db_ppi.ruangan r
																where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
																$rs = mysqli_query($conn1,$sql);
																while ($data = mysqli_fetch_array($rs)) {
																	?>
																	<option value="<?=$data[0]?>"><?=$data[3]?></option>
																	<?php
																}
																?>	
															</select>
														</div>
													</div>
												</div>
											</div>												
											
										</div>	
									</div> <!-- row -->
									<div class="row">
										<div class="col-md-12">
											<div class="panel panel-info">
												<div class="panel-heading">	<img width="15" height="15" alt="150x150" src="assets/images/pdf-small.png" />
												Pilih laporan</div>
												<div class="panel-body">
													<?php if($_SESSION['userid']!=133){ ?>
														<div class="col-md-3">
															<button type="submit" name="harian" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Harian 
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="kinerja" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Indikator kinerja
															</button>			
														</div>								
														<div class="col-md-3">
															<button type="submit" name="bulanan" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Bulanan 
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="handy" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Hand Hygiene
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="detilhandy" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Detil Hand Hygiene
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="iadp" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Bundle IADP 
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="isk" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Bundle ISK 
															</button>			
														</div>										
														<div class="col-md-3">
															<button type="submit" name="vap" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Bundle VAP 
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="ido" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Bundle IDO </button>			
														</div>											
														<div class="col-md-3">
															<button type="submit" name="afkt" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Fasilitas kebersihan tangan 
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="akml" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Kepatuhan membuang limbah 
															</button>			
														</div>										
														<!-- <div class="col-md-3">
															<button type="submit" name="aapd" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Kepatuhan penggunaan APD 
															</button>			
														</div> -->
														<div class="col-md-3">
														<button class="button button" name="report" value="btn-apdnew" id="">
															<img width="20" height="20" src="assets/images/pdf.png" /> Kepatuhan penggunaan APD NEW
														</button>
														</div>
														<div class="col-md-3">
															<button type="submit" name="tpia" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Monitoring Transfer Pasien Infeksi Airborne 
															</button>			
														</div>											
														<div class="col-md-3">
															<button type="submit" name="appi" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Audit Pencegahan & Pengendalian Infeksi 
															</button>			
														</div>	
														<div class="col-md-3">
															<button type="submit" name="renov" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Pemantauan selama renov 
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="bmp" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Audit BMP
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="akj" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Audit Kamar Jenazah
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="cssd" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Audit CSSD
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="gizi" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Audit Gizi
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="aprp" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Audit PPI Ruang Perawatan
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="londri" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Audit Laundry
															</button>			
														</div>
														<div class="col-md-3">
															<button type="submit" name="langkah" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Audit Laporan 6 Langkah kebersihan tangan
															</button>			
														</div>	
														<div class="col-md-3">
															<button type="submit" name="bedah" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Audit PPI di Kamar Bedah
															</button>			
														</div>	
														<div class="col-md-3">
															<button type="submit" name="covid" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Covid-19
															</button>			
														</div>	

													<?php } else { ?>
														<div class="col-md-3">
															<button type="submit" name="covid" class="button button"><img width="20" height="20"  src="assets/images/pdf.png" /> Rekap Covid-19
															</button>	
														<?php } ?>		
													</div>										
												</div>
											</div>
										</div>
									</form>	
								</div>							
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.row -->
		</div>
	</div><!-- /.page-content -->	
</div><!-- /.main-content -->
<script>
    document.getElementById("btn-apdnew").addEventListener("click", function () {
        // Ambil nilai input tanggal dan ruangan dari form
        var tglAwal = document.getElementsByName("tanggal1")[0].value;
        var tglAkhir = document.getElementsByName("tanggal2")[0].value;
        var ruangan = document.getElementById("form-field-select-3").value;

        // Membuat URL untuk mengarahkan ke Apdnew.php dengan parameter yang dikirim
        var url = "http://192.168.7.138/cetakanppi/Apdnew.php?TGL_AWAL=" + encodeURIComponent(tglAwal) +
                  "&TGL_AKHIR=" + encodeURIComponent(tglAkhir) +
                  "&RUANGAN=" + encodeURIComponent(ruangan);

        // Arahkan browser ke URL tersebut
        window.open(url, '_blank');
    });
</script>
