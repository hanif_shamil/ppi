<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					$TANGGAL 			= $_POST['TANGGAL'];									
					$NOMR				= $_POST['NOMR'];
					$NAMA				= addslashes($_POST['NAMA']);
					$UMUR				= $_POST['UMUR'];
					$DOKTER				= $_POST['DOKTER'];
					$DIAGNOSA			= $_POST['DIAGNOSA'];	
					$SATU 				= $_POST['SATU'];
					$KET_SATU				= $_POST['KET_SATU']; 
					$DUA 				= $_POST['DUA'];
					$KET_DUA				= $_POST['KET_DUA']; 
					$TIGA_A					= $_POST['TIGA_A'];
					$KET_TIGA_A			= $_POST['KET_TIGA_A']; 
					$TIGA_B					= $_POST['TIGA_B'];
					$KET_TIGA_B 			= $_POST['KET_TIGA_B'];	
					$TIGA_C					= $_POST['TIGA_C'];
					$KET_TIGA_C				= $_POST['KET_TIGA_C'];  
					$TIGA_D 				= $_POST['TIGA_D'];
					$KET_TIGA_D			= $_POST['KET_TIGA_D'];
					$TIGA_E					= $_POST['TIGA_E'];
					$KET_TIGA_E 		= $_POST['KET_TIGA_E'];  
					$EMPAT 				= $_POST['EMPAT'];
					$KET_EMPAT 			=	$_POST['KET_EMPAT'];  
					$LIMA 				=	$_POST['LIMA'];
					$KET_LIMA			=	$_POST['KET_LIMA'];  
					$ENAM 				=	$_POST['ENAM'];
					$KET_ENAM			=	$_POST['KET_ENAM'];  
					$NILAI 				=	$_POST['NILAI'];  					
					$ANALISA 			=	$_POST['ANALISA'];  
					$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
					$KEPALA 			=	$_POST['KEPALA'];  
					$AUDITOR 			=	addslashes($_POST['AUDITOR']); 
					$pengisi 			= $_SESSION['userid'];							
						//echo "<pre>";print_r($_POST);exit();                	
					$query = "INSERT INTO db_ppi.tb_lumbal (
					USER,
					NOMR,
					NAMA,
					UMUR,
					DIAGNOSA,
					DOKTER,
					TANGGAL,
					SATU,
					KET_SATU, 
					DUA,
					KET_DUA, 
					TIGA_A,
					KET_TIGA_A, 
					TIGA_B,
					KET_TIGA_B, 
					TIGA_C,
					KET_TIGA_C, 
					TIGA_D,
					KET_TIGA_D, 
					TIGA_E,
					KET_TIGA_E, 
					EMPAT,
					KET_EMPAT, 
					LIMA,
					KET_LIMA, 
					ENAM,
					KET_ENAM, 
					NILAI, 
					ANALISA,
					TINDAKLANJUT,
					KEPALA,
					AUDITOR) 
					VALUES 
					('$pengisi',
					'$NOMR',
					'$NAMA',
					'$UMUR',
					'$DIAGNOSA',
					'$DOKTER',
					'$TANGGAL',
					'$SATU',
					'$KET_SATU', 
					'$DUA',
					'$KET_DUA', 
					'$TIGA_A',
					'$KET_TIGA_A', 
					'$TIGA_B',
					'$KET_TIGA_B', 
					'$TIGA_C',
					'$KET_TIGA_C', 
					'$TIGA_D',
					'$KET_TIGA_D', 
					'$TIGA_E',
					'$KET_TIGA_E', 
					'$EMPAT',
					'$KET_EMPAT', 
					'$LIMA',
					'$KET_LIMA', 
					'$ENAM',
					'$KET_ENAM', 
					'$NILAI', 
					'$ANALISA',
					'$TINDAKLANJUT',
					'$KEPALA',
					'$AUDITOR')";
					//echo $query; die();	
					$hasil=mysqli_query($conn1,$query);								
					if($hasil){
						echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_lumbal'</script>"; 
					}                     
					else{
						echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
					}                         
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			