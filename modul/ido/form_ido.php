<script type="text/javascript">
	function tugas1()
{
var jumlah=0;
var nilai;


if(document.getElementById("TREATMENT").checked)
{
nilai=document.getElementById("TREATMENT").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("ANTIBIOTIK").checked)
{
nilai=document.getElementById("ANTIBIOTIK").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("NORMOTHER").checked)
{
nilai=document.getElementById("NORMOTHER").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("GULA").checked)
{
nilai=document.getElementById("GULA").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("CUKUR").checked)
{
nilai=document.getElementById("CUKUR").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("CHLOREXID").checked)
{
nilai=document.getElementById("CHLOREXID").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("KLUAR_MASUK").checked)
{
nilai=document.getElementById("KLUAR_MASUK").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("ANTIMICROB").checked)
{
nilai=document.getElementById("ANTIMICROB").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("ALKOHOL").checked)
{
nilai=document.getElementById("ALKOHOL").value;
jumlah=jumlah+parseInt(nilai);
}
document.getElementById("total").value=jumlah/9*100;
}
	</script>
<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISI) AS last FROM tb_iadp WHERE ID_ISI LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Audit Bundles IADP</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Audit Bundles IDO</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_ido" method="post">
								<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-2">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" value="<?php echo date('Y/m/d H:i:s') ?>" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data[0]?>"><?=$data[3]?></option>
												<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-1">
											<input type="text" id="nomr" name="NOMR" placeholder="[ MR ]" autocomplete="off" class="form-control" onkeyup="autofill()" />												
										</div>	
										<div class="col-sm-3">									
											<input type="text" id="nama" name="NAMA" placeholder="[ NAMA ]" autocomplete="off" class="form-control" />												
										</div>																			
										<div class="col-sm-3">											
											<input type="text" name="SURVEYOR" placeholder="[ SURVEYOR ]" class="form-control" />												
										</div>
									</div>										
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> BUNDLES IDO</th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Memberikan treatmen para operasi salep dengan kandungan mupirocin 2% pada kedua lubang hidung dan mandi dengan chlorexidin 4% pada pasien yang teridentifikasi Carrier Staphylococus aureus</td>
														<td align="center"><label>
															<input name="TREATMENT[]" type="checkbox" id="TREATMENT" value="1" class="ace input-lg" onClick="tugas1()" />
															
															<span class="lbl"></span>
															</label></td>
														<td align="center">
															<label>
															<input name="TREATMENT[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Memberikan Antibiotik Prophylaksi 1-2 jam sebelum insisi, ketika ada indikasi</td>
														<td align="center"><label>
													<input name="ANTIBIOTIK[]" type="checkbox" id="ANTIBIOTIK" value="1" class="ace input-lg" onClick="tugas1()" />
													<span class="lbl"></span>
												</label></td>
														<td align="center"><label>
													<input name="ANTIBIOTIK[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Mempertahankan suhu pasien normothermia</td>
														<td align="center">
														<label>
													<input name="NORMOTHER[]" type="checkbox" id="NORMOTHER" value="1" class="ace input-lg" onClick="tugas1()" />
													<span class="lbl"></span>
												</label></td>
														<td align="center">
														<label>
													<input name="NORMOTHER[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>4</td>
														<td>Mengontrol nilai gula darah</td>
														<td align="center">
															<label>
															<input name="GULA[]" type="checkbox" id="GULA" value="1" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="GULA[]" type="checkbox" id="" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Melakukan pencukuran rambut di area operasi menggunakan clipper dan dilakukan di pagi hari</td>
														<td align="center">
															<label>
															<input name="CUKUR[]" type="checkbox" id="CUKUR" value="1" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="CUKUR[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>6</td>
														<td>Menggunakan sabun chlorexidin 4% 1 hari sebelum operasi (Pagi dan Sore)</td>
														<td align="center">
															<label>
															<input name="CHLOREXID[]" type="checkbox" id="CHLOREXID" value="1" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="CHLOREXID[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>7</td>
														<td>Meminimalkan keluar masuk petugas di ruang operasi</td>
														<td align="center">
															<label>
															<input name="KLUAR_MASUK[]" type="checkbox" id="KLUAR_MASUK" value="1" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="KLUAR_MASUK[]" type="checkbox" id="" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>8</td>
														<td>Melakukan kebersihan tangan sebelum pembedahan dengan menggunakan sabun antimicrobial dan air dan atau menggunakan handrub sebelum menggunakan sarung tangan</td>
														<td align="center">
															<label>
															<input name="ANTIMICROB[]" type="checkbox" id="ANTIMICROB" value="1" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="ANTIMICROB[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>9</td>
														<td>Menggunakan cairan berbasis alkohol dengan kandungan tambahan chlorexidin untuk persiapan kulit sebelum pembedahan dipergunakan pada pasien dengan pembedahan</td>
														<td align="center">
															<label>
															<input name="ALKOHOL[]" type="checkbox" id="ALKOHOL" value="1" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="ALKOHOL[]" type="checkbox" value="2" id="1" class="ace input-lg" onClick="hitung( this )" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
														<div class="input-group">
															<input name="TOTAL" type="text" id="total" style="width:60px"></br>
															<span class="input-group-addon">
																%
															</span>
														</div>														
														</td>														
													</tr>													
												</tbody>
											</table>											
										</div>
									</div>													
									
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->