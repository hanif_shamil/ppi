<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$SURVEYOR 		= $_POST['SURVEYOR'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						if(isset($_POST['TREATMENT'])){
							$TREATMENT = implode(',',$_POST['TREATMENT']);
						}else{
							$TREATMENT = "";
						}	
						if(isset($_POST['ANTIBIOTIK'])){
							$ANTIBIOTIK = implode(',',$_POST['ANTIBIOTIK']);
						}else{
							$ANTIBIOTIK = "";
						}	
						if(isset($_POST['NORMOTHER'])){
							$NORMOTHER = implode(',',$_POST['NORMOTHER']);
						}else{
							$NORMOTHER = "";
						}
						if(isset($_POST['GULA'])){
							$GULA = implode(',',$_POST['GULA']);
						}else{
							$GULA = "";
						}							
						if(isset($_POST['CUKUR'])){
							$CUKUR = implode(',',$_POST['CUKUR']);
						}else{
							$CUKUR = "";
						}	
						if(isset($_POST['CHLOREXID'])){
							$CHLOREXID = implode(',',$_POST['CHLOREXID']);
						}else{
							$CHLOREXID = "";
						}	
						if(isset($_POST['KLUAR_MASUK'])){
							$KLUAR_MASUK = implode(',',$_POST['KLUAR_MASUK']);
						}else{
							$KLUAR_MASUK = "";
						}	
						if(isset($_POST['ANTIMICROB'])){
							$ANTIMICROB = implode(',',$_POST['ANTIMICROB']);
						}else{
							$ANTIMICROB = "";
						}	
						if(isset($_POST['ALKOHOL'])){
							$ALKOHOL = implode(',',$_POST['ALKOHOL']);
						}else{
							$ALKOHOL = "";
						}	
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];							
						//echo "<pre>";print_r($_POST);exit();                                      						
                        $query = "INSERT INTO db_ppi.tb_ido (
						ID_ISI,
						TANGGAL,
						RUANGAN,
						NOMR,
						NAMA,
						SURVEYOR,
						TREATMENT,
						ANTIBIOTIK,
						NORMOTHER,
						GULA,
						CUKUR,
						CHLOREXID,
						KLUAR_MASUK,
						ANTIMICROB,
						ALKOHOL,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$RUANGAN',
						'$NOMR',
						'$NAMA',
						'$SURVEYOR',
						'$TREATMENT',
						'$ANTIBIOTIK',
						'$NORMOTHER',
						'$GULA',
						'$CUKUR',
						'$CHLOREXID',
						'$KLUAR_MASUK',
						'$ANTIMICROB',
						'$ALKOHOL',
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=ido'</script>"; 
						}
						}                      
						else{
                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
                        }	
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$SURVEYOR 		= $_POST['SURVEYOR'];
						if(isset($_POST['TREATMENT'])){
							$TREATMENT = implode(',',$_POST['TREATMENT']);
						}else{
							$TREATMENT = "";
						}	
						if(isset($_POST['ANTIBIOTIK'])){
							$ANTIBIOTIK = implode(',',$_POST['ANTIBIOTIK']);
						}else{
							$ANTIBIOTIK = "";
						}
						if(isset($_POST['NORMOTHER'])){
							$NORMOTHER = implode(',',$_POST['NORMOTHER']);
						}else{
							$NORMOTHER = "";
						}
						if(isset($_POST['GULA'])){
							$GULA = implode(',',$_POST['GULA']);
						}else{
							$GULA = "";
						}
						if(isset($_POST['CUKUR'])){
							$CUKUR = implode(',',$_POST['CUKUR']);
						}else{
							$CUKUR = "";
						}
						if(isset($_POST['CHLOREXID'])){
							$CHLOREXID = implode(',',$_POST['CHLOREXID']);
						}else{
							$CHLOREXID = "";
						}
						if(isset($_POST['KLUAR_MASUK'])){
							$KLUAR_MASUK = implode(',',$_POST['KLUAR_MASUK']);
						}else{
							$KLUAR_MASUK = "";
						}
						if(isset($_POST['ANTIMICROB'])){
							$ANTIMICROB = implode(',',$_POST['ANTIMICROB']);
						}else{
							$ANTIMICROB = "";
						}
						if(isset($_POST['ALKOHOL'])){
							$ALKOHOL = implode(',',$_POST['ALKOHOL']);
						}else{
							$ALKOHOL = "";
						}
						$USER 			= $_SESSION['userid'];	
						$TOTAL	 		= $_POST['TOTAL'];				
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   					
                        $query2 = "UPDATE db_ppi.tb_ido SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						SURVEYOR='$SURVEYOR',
						NOMR='$NOMR',
						NAMA='$NAMA',
						TREATMENT='$TREATMENT',
						ANTIBIOTIK='$ANTIBIOTIK',
						NORMOTHER='$NORMOTHER',
						GULA='$GULA',
						CUKUR='$CUKUR',
						CHLOREXID='$CHLOREXID',
						KLUAR_MASUK='$KLUAR_MASUK', 
						ANTIMICROB='$ANTIMICROB',
						ALKOHOL='$ALKOHOL',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_ido'</script>"; 
						}
						}                        
						$id = $_GET['id'];
						if(isset($_GET['id'])){						
						$query1 = "UPDATE db_ppi.tb_ido ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_ido'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?page=tabel_ido'</script>"; 
						}	
						}						
?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			