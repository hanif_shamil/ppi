<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISIAN) AS last FROM tb_isian WHERE ID_ISIAN LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Harian</li>
			</ul><!-- /.breadcrumb -->			
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Surveilans Harian Infeksi Rumah Sakit</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
							<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_h" method="post" onsubmit="return validateForm()">
								<input type="hidden" name="id_isian" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-4">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d') ?>" id="datetimepicker1" autocomplete="off" placeholder="[ Tanggal survey ]" name="tanggal" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>								
										<div class="col-sm-4">
											<input type="text" id="form-field-1-1" name="surveyor" placeholder="[ Surveyor ]" class="form-control" />
										</div>
										<div class="col-sm-4">
											<input type="text" id="form-field-1-1" name="divisi" placeholder="[ Divisi ]" value="PPI" class="form-control" readonly />
										</div>																		
									</div>
									<hr />									
									<div class="form-group">																																																			
										<div class="col-sm-1">
											<input type="text" id="nomr" name="nomr" autocomplete="off" placeholder="No.RM" class="form-control" onkeyup="autofill()" />
										</div>							
										<div class="col-sm-3">
											<input type="text" id="nama" name="nama" placeholder="[ Nama ]" class="form-control" readonly />
										</div>														
										<div class="col-sm-1">
											<input type="" id="umur_t" name="" placeholder="[ Usia ]" class="form-control" readonly>
											<input type="hidden" id="umur" name="umur" placeholder="[ Usia ]" class="form-control" readonly>
										</div>															
										<div class="col-sm-2">
											<input name="" id="jenis_kelamin" placeholder="[ Jenis Kelamin ]" class="form-control" readonly> 
											<input type="hidden" name="id_jenis_kelamin" id="id_jenis_kelamin" placeholder="Jenis Kelamin" class="form-control">
										</div>
										<div class="col-sm-2">
											<input name="" id="ruangan" placeholder="[ Ruangan ]" class="form-control" readonly> 
											<input type="hidden" name="ruangan" id="id_ruangan" class="form-control">
										</div>
										<div class="col-sm-1">	
											<div class="input-group">										
												<input type="text" id="form-field-1-1" name="suhu" placeholder="[ Suhu ]" class="form-control" />
												<!--<span class="input-group-addon">
													&deg;C
												</span>-->										
											</div>	
										</div>
										<div class="col-sm-2">
											<input type="text" id="form-field-1-1" name="dxmedis" placeholder=" [DX. MEDIS ]" class="form-control" />
										</div>
									</div>							
									<hr />	
									<div class="form-group">
										<div class="col-sm-12">
											<label class="control-label bolder blue" style="text-decoration: underline">Pemberian antibiotik :</label>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="antibiotik1" id="form-field-select-3" data-placeholder="[ Antibiotik 1]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab
												where ab.`STATUS`=1";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data['ID']?>"><?=$data['NAMA_OBAT']?></option>
												<?php
												}
												?>	
											</select>
										</div>
										<div class="col-sm-1">
											<input name="ke1" id="ruangan" type="number" placeholder="[ Day ]" class="form-control"> 
										</div>											
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="antibiotik2" id="form-field-select-3" data-placeholder="[ Antibiotik 2]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab where ab.`STATUS`=1";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data['ID']?>"><?=$data['NAMA_OBAT']?></option>
												<?php
												}
												?>	
											</select>
										</div>
										<div class="col-sm-1">
											<input name="ke2" id="ruangan" type="number" placeholder="Day" class="form-control"> 
										</div>											
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="antibiotik3" id="form-field-select-3" data-placeholder="[ Antibiotik 3]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab where ab.`STATUS`=1";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data['ID']?>"><?=$data['NAMA_OBAT']?></option>
												<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-1">
											<input name="ke3" id="ruangan" type="number" placeholder="Day" class="form-control"> 
										</div>
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="antibiotik4" id="form-field-select-3" data-placeholder="[ Antibiotik 4]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab where ab.`STATUS`=1";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data['ID']?>"><?=$data['NAMA_OBAT']?></option>
												<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-1">
											<input name="ke4" id="ruangan" type="number" placeholder="Day" class="form-control"> 
										</div>											
									</div>
									<hr />
									<div class="form-group">													
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tirah baring :</label>
												<div class="checkbox">
													<label>
														<input name="tirah[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> Ya</span>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-4">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Infeksi Rumah Sakit :</label>
												<div class="checkbox">
													<label>
														<input name="vap[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> VAP</span>
													</label>												
							<!-- 						<label>
														<input name="hap[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> HAP</span>
													</label> -->												
													<label>
														<input name="isk[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> ISK</span>
													</label>												
													<label>
														<input name="iad[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> IADP</span>
													</label>
													<label>
														<input name="ido[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> IDO</span>
													</label>
													<label>
														<input name="plebitis[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> PLEBITIS</span>
													</label>
												</div>										
											</div>
										</div>
										<div class="col-sm-4">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tindakan :</label>
												<div class="checkbox">
													<label>
														<input name="uc[]" type="checkbox" value="1" class="ace" />
														<span class="lbl"> UC</span>
													</label>											
													<label>
														<input name="ivl[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> IVL</span>
													</label>											
													<label>
														<input name="cvl[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> CVL</span>
													</label>											
													<label>
														<input name="ett[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> ETT/VENT</span>
													</label>
												</div>									
											</div>
										</div>
<!-- 										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Plebitis :</label>
												<div class="checkbox">
													<label>
														<input name="plebitis[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> Ya</span>
													</label>
												</div>
											</div>
										</div> -->
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
													<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Order :</label>
												<div class="input-group">
													<input class="form-control" id="datetimepicker" placeholder="[ Tanggal survey ]" name="tanggal_order" type="text"/>
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>	
										</div>	
									</div>														
									<div class="form-group">
										<div class="col-xs-12 col-sm-12">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Hasil kultur :</label>
												<div class="checkbox">
													<label>
														<input name="cvp[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> CVP</span>
														<input type="text" id="form-field-1-1" name="hcvp" placeholder="[ CVP ]" class="form-control" style="width:180px" />
													</label>
													<label>
														<input name="darah[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> DARAH</span>
														<input type="text" id="form-field-1-1" name="hdarah" placeholder="[ DARAH ]" class="form-control" style="width:180px" />
													</label>											
													<label>
														<input name="swabluka[]" value="1" type="checkbox" class="ace" />
														<span class="lbl"> SWAB LUKA</span>
														<input type="text" id="form-field-1-1" name="hluka" placeholder="[ SWAB LUKA ]" class="form-control" style="width:180px" />
													</label>												
													<label>
														<input name="sputum[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> SPUTUM</span>
														<input type="text" id="form-field-1-1" name="hsputum" placeholder="[ SPUTUM ]" class="form-control" style="width:180px" />
													</label>
													
													<label>
														<input name="urine[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> URINE</span>
														<input type="text" id="form-field-1-1" name="hurine" placeholder="[ URINE ]" class="form-control" style="width:180px" />
													</label><br></br>
													<label>
														<input name="swabketiak[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> SWAB KETIAK</span>
														<input type="text" id="form-field-1-1" name="hketiak" placeholder="[ SWAB KETIAK ]" class="form-control" style="width:180px" />
													</label>
													<label>
														<input name="swabhidung[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> SWAB HIDUNG</span>
														<input type="text" id="form-field-1-1" name="hhidung" placeholder="[ SWAB HIDUNG ]" class="form-control" style="width:180px" />
													</label>
													<label>
														<input name="cdl[]" value="1" class="ace" type="checkbox" />
														<span class="lbl"> CDL</span>
														<input type="text" id="form-field-1-1" name="hcdl" placeholder="[ CDL ]" class="form-control" style="width:180px" />
													</label>
												</div>												
											</div>
										</div>																				
									</div>																									
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->

<script>
	function validateForm() {
        var nama = document.getElementById('nama').value;
        if (nama.trim() === "") {
            alert("Nama tidak boleh kosong. Pastikan nomor RM yang dimasukkan benar.");
			document.getElementById('nomr').value = "";
			document.getElementById('nomr').focus();
            return false; 
        }
        return true; 
    }
</script>