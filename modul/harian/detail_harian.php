<?php
$id=($_GET['id']);
$query="select tdb.ID_DETAIL, tdb.ID_ISIAN, tdb.TGL_DETAIL, tdb.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, tdb.NAMA, tdb.NOMR, tdb.DIVISI, tdb.SURVEYOR
, tdb.SUHU, tdb.UMUR, if(tdb.JENIS_KELAMIN=1,'Laki-kai','Perempuan') JENIS_KELAMIN, tdb.JENIS_KELAMIN ID_JENIS_KELAMIN, tdb.DX_MEDIS, tdb.UC, tdb.IVL
, tdb.CVL,tdb.ETT, tdb.VAP, tdb.HAP, tdb.ISK, tdb.IAD, tdb.TIRAH, tdb.DARAH, tdb.CDL, tdb.SWAB, tdb.SPUTUM, tdb.URINE, tdb.LAIN, tdb.ANTIBIOTIK, ab.NAMA_OBAT, tdb.ANTIBIO_KE
from tb_detail_isian tdb
LEFT JOIN referensi rjk ON rjk.ID=tdb.JENIS_KELAMIN and rjk.JENIS=1
LEFT JOIN ruangan ru ON ru.ID=tdb.RUANGAN and ru.JENIS=5
left join antibiotik ab ON ab.ID=tdb.ANTIBIOTIK
where tdb.ID_ISIAN='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$UC = explode(",", $dt['UC']);$IVL = explode(",", $dt['IVL']);$CVL = explode(",", $dt['CVL']);$CDL = explode(",", $dt['CDL']);
$ETT = explode(",", $dt['ETT']);$VAP = explode(",", $dt['VAP']);$HAP = explode(",", $dt['HAP']);
$ISK = explode(",", $dt['ISK']);$IAD = explode(",", $dt['IAD']);$TIRAH = explode(",", $dt['TIRAH']);
$DARAH = explode(",", $dt['DARAH']);$SWAB = explode(",", $dt['SWAB']);$SPUTUM = explode(",", $dt['SPUTUM']);$URINE = explode(",", $dt['URINE']);
 {
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Edit Form Harian</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Edit Surveilans Harian Infeksi Rumah Sakit</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_h" method="post">
								<input type="hidden" name="id_isian" class="form-control" value="<?php echo $id; ?>" readonly>									
									<div class="form-group">										
										<div class="col-sm-3">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TGL_DETAIL']; ?>" name="tanggal" type="text"/disabled>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>								
										<div class="col-sm-3">
											<input type="text" id="form-field-1-1" name="surveyor" value="<?php echo $dt['SURVEYOR']; ?>" placeholder="[ Surveyor ]" class="form-control" /disabled>
										</div>
										<div class="col-sm-2">
											<input type="text" id="form-field-1-1" name="divisi" value="<?php echo $dt['DIVISI']; ?>" placeholder="[ Divisi ]" class="form-control" /disabled>
										</div>
										<div class="col-sm-2">	
											<div class="input-group">										
												<input type="text" id="form-field-1-1" name="suhu" value="<?php echo $dt['SUHU']; ?>" placeholder="[ Suhu ]" class="form-control" /disabled>
												<span class="input-group-addon">
													&deg;C
												</span>											
											</div>	
										</div>
										<div class="col-sm-2">										
											<input type="text" id="form-field-1-1" name="suhu" value="<?php echo $dt['DX_MEDIS']; ?>" placeholder="[ Suhu ]" class="form-control" /disabled>																					
										</div>	
									</div>
									<hr />									
									<div class="form-group">																																																			
										<div class="col-sm-2">
											<input type="text" id="nomr" name="nomr" value="<?php echo $dt['NOMR']; ?>" placeholder="[ No.RM ]" class="form-control" onkeyup="autofill()" /disabled>
										</div>							
										<div class="col-sm-3">
											<input type="text" id="nama" name="nama" value="<?php echo $dt['NAMA']; ?>" placeholder="[ Nama ]" class="form-control" disabled />
										</div>														
										<div class="col-sm-2">
											<input type="text" id="umur" value="<?php echo $dt['UMUR']; ?>" name="umur" placeholder="[ Usia ]" class="form-control" disabled>
										</div>															
										<div class="col-sm-2">
											<input name="" id="jenis_kelamin" value="<?php echo $dt['JENIS_KELAMIN']; ?>" placeholder="[ Jenis Kelamin ]" class="form-control" disabled> 
											<input type="hidden" name="id_jenis_kelamin" value="<?php echo $dt['ID_JENIS_KELAMIN']; ?>" id="id_jenis_kelamin" placeholder="Jenis Kelamin" class="form-control">
										</div>
										<div class="col-sm-3">
											<input name="" id="ruangan" value="<?php echo $dt['RUANGAN']; ?>" placeholder="[ Ruangan ]" class="form-control" disabled> 
											<input type="hidden" name="ruangan" value="<?php echo $dt['ID_RUANGAN']; ?>" id="id_ruangan" class="form-control">
										</div>
									</div>							
									<div class="form-group">
										<div class="col-sm-4">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Antibiotik :</label>
												<select multiple="" class="chosen-select form-control" name="antibiotik[]" id="form-field-select-4" data-placeholder="[ Antibiotik ]" disabled>
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>							
												<option value="<?php echo $data['NAMA_OBAT'] ?>" <?php echo in_array($data['NAMA_OBAT'], unserialize($dt['ANTIBIOTIK'])) ? 'selected="selected"' : '' ?>><?php echo $data['NAMA_OBAT'] ?></option>
												<?php
												}
												?>	
											</select>												
											</div>
										</div>
										<div class="col-sm-2">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Antibotik yang ke:</label>
											<input type="text" id="form-field-1-1" name="antibioke" value="<?php echo $dt['ANTIBIO_KE']; ?>" placeholder="[ Pemberian ke ? ]" class="form-control" / disabled>
										</div>
										</div>	
										<div class="col-sm-4">
										<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Hasil Lain</label>
											<input type="text" id="form-field-1-1" value="<?php echo $dt['LAIN']; ?>" name="lain" placeholder="[ Hasil lain ]" class="form-control" / disabled>
										</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tirah baring :</label>
												<div class="checkbox">
													<label>
														<input name="tirah[]" value="1" <?php if(in_array("1", $TIRAH)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" / disabled>
														<span class="lbl"> Ya</span>
													</label>
												</div>
											</div>
										</div>																																							
									</div>															
									<div class="form-group">
										<div class="col-xs-12 col-sm-6">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tindakan :</label>
												<div class="checkbox">
													<label>
														<input name="uc[]" type="checkbox" value="1" <?php if(in_array("1", $UC)){ echo " checked=\"checked\""; } ?> class="ace" /disabled>							
														<span class="lbl"> UC</span>
													</label>											
													<label>
														<input name="ivl[]" value="IVL" <?php if(in_array("1", $IVL)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" /disabled>
														<span class="lbl"> IVL</span>
													</label>											
													<label>
														<input name="cvl[]" value="CVL" <?php if(in_array("1", $CVL)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" /disabled>
														<span class="lbl"> CVL</span>
													</label>											
													<label>
														<input name="ett[]" value="ETT/VENT" <?php if(in_array("1",$ETT)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" /disabled>
														<span class="lbl"> ETT/VENT</span>
													</label>
												</div>									
											</div>
										</div>							
										<div class="col-xs-12 col-sm-6">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Infeksi Rumah Sakit :</label>
												<div class="checkbox">
													<label>
														<input name="vap[]" value="1" <?php if(in_array("1", $VAP)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" /disabled>
														<span class="lbl"> VAP</span>
													</label>
												
													<label>
														<input name="hap[]" value="1" <?php if(in_array("1", $HAP)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" /disabled>
														<span class="lbl"> HAP</span>
													</label>
												
													<label>
														<input name="isk[]" value="1" <?php if(in_array("1", $ISK)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" /disabled>
														<span class="lbl"> ISK</span>
													</label>
												
													<label>
														<input name="iad[]" value="1" <?php if(in_array("1", $IAD)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" /disabled>
														<span class="lbl"> IAD</span>
													</label>
												</div>										
											</div>
										</div>										
									</div>							
									
									
									<!--<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>-->
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->
<?php
	}
	?>