<?php
function serialize_ke_string($serial)
{
    $hasil = unserialize($serial);
    return implode(', ', $hasil);
}
?>

<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Tables</a>
		</li>
		<li class="active">Laporan Harian</li>
	</ul><!-- /.breadcrumb -->

	<!--<div class="nav-search" id="nav-search">
		<form class="form-search">
			<span class="input-icon">
				<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
				<i class="ace-icon fa fa-search nav-search-icon"></i>
			</span>
		</form>
	</div><!-- /.nav-search -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Data Laporan Harian</h4>
							<div class="widget-toolbar">
								<a href="index.php?page=harian">
									<i class="ace-icon fa fa-plus" style="color:blue"> Tambah data</i>
								</a>
							</div>
						</div>						

					<!-- div.table-responsive -->

					<!-- div.dataTables_borderWrap -->
					<div>
						<table id="datatabelss" class="table table-striped table-bordered table-hover" width="100%">
							<thead>
								<tr align="center">
									<th width="11%"><div align="center">Tanggal Survey</div></th>
									<th width="20%"><div align="center">NAMA</div></th>
									<th width="10%"><div align="center">RM</div></th>
									<th><div align="center">Ruangan</div></th>																
									<th><div align="center">Diaxnosa Medis</div></th>
									<th width="10%"><div align="center">Opsi</div></th>
								</tr>																
							</thead>							
						</table>
					</div>
				</div>
				</div>
			</div>
			<!-- PAGE CONTENT ENDS -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</div><!-- /.page-content -->

