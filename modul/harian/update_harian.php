<html>
<head>
<script src="assets/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="assets/sweetalert/dist/sweetalert.css">

</head>
<body>
	<?php
		date_default_timezone_set('Asia/Jakarta');
		if(isset($_POST['btnEdit'])){
		$id_isian 			= $_POST['id_isian'];							
		$tanggal 			= $_POST['tanggal'];
		$tanggal_order 		= $_POST['tanggal_order'];						
		$ruangan 			= isset($_POST['ruangan']) ? $_POST['ruangan'] : '';							
		$surveyor 			= $_POST['surveyor'];			
		$divisi 			= $_POST['divisi'];					
		$suhu 				= $_POST['suhu'];			
		$nomr 				= $_POST['nomr'];			
		$nama 				= addslashes($_POST['nama']);			
		$umur 				= $_POST['umur'];		
		$id_jenis_kelamin 	= $_POST['id_jenis_kelamin'];	
		$dxmedis 			= $_POST['dxmedis'];						
		if(isset($_POST['uc'])){
			$uc = implode(',',$_POST['uc']);
		}else{
			$uc = "";
		}
		if(isset($_POST['ivl'])){
			$ivl = implode(',',$_POST['ivl']);
		}else{
			$ivl = "";
		}
		if(isset($_POST['cvl'])){
			$cvl = implode(',',$_POST['cvl']);
		}else{
			$cvl = "";
		}						
		if(isset($_POST['ett'])){
			$ett = implode(',',$_POST['ett']);
		}else{
			$ett = "";
		}
		if(isset($_POST['vap'])){
			$vap = implode(',',$_POST['vap']);
		}else{
			$vap = "";
		}	
		// if(isset($_POST['hap'])){
		// 	$hap = implode(',',$_POST['hap']);
		// }else{
		// 	$hap = "";
		// }	
		if(isset($_POST['isk'])){
			$isk = implode(',',$_POST['isk']);
		}else{
			$isk = "";
		}	
		if(isset($_POST['iad'])){
			$iad = implode(',',$_POST['iad']);
		}else{
			$iad = "";
		}
		
		if(isset($_POST['ido'])){
			$ido = implode(',',$_POST['ido']);
		}else{
			$ido = "";
		}
		
		if(isset($_POST['tirah'])){
			$tirah = implode(',',$_POST['tirah']);
		}else{
			$tirah = "";
		}
		if(isset($_POST['plebitis'])){
			$plebitis = implode(',',$_POST['plebitis']);
		}else{
			$plebitis = "";
		}
		
		if(isset($_POST['cvp'])){
		$cvp = implode(',',$_POST['cvp']);
		}else{
		$cvp = "";
		}
		$hcvp =$_POST['hcvp'];
		
		if(isset($_POST['darah'])){
			$darah = implode(',',$_POST['darah']);
		}else{
			$darah = "";
		}
		$hdarah =$_POST['hdarah'];
		
		if(isset($_POST['sputum'])){
			$sputum = implode(',',$_POST['sputum']);
		}else{
			$sputum = "";
		}
		$hsputum = $_POST['hsputum'];
		if(isset($_POST['swabhidung'])){
			$swabhidung = implode(',',$_POST['swabhidung']);
		}else{
			$swabhidung = "";
		}
		$hhidung = $_POST['hhidung'];
		if(isset($_POST['swabketiak'])){
			$swabketiak = implode(',',$_POST['swabketiak']);
		}else{
			$swabketiak = "";
		}
		$hketiak = $_POST['hketiak'];
		if(isset($_POST['swabluka'])){
			$swabluka = implode(',',$_POST['swabluka']);
		}else{
			$swabluka = "";
		}
		$hluka = $_POST['hluka'];
		if(isset($_POST['urine'])){
			$urine = implode(',',$_POST['urine']);
		}else{
			$urine = "";
		}	
		$hurine = $_POST['hurine'];				
		if(isset($_POST['cdl'])){
			$cdl = implode(',',$_POST['cdl']);
		}else{
			$cdl = "";
		}	
		$hcdl = $_POST['hcdl'];			
		$antibiotik1 = $_POST['antibiotik1'];
		$ke1 = $_POST['ke1'];
		$antibiotik2 = $_POST['antibiotik2'];
		$ke2 = $_POST['ke2'];
		$antibiotik3 = $_POST['antibiotik3'];
		$ke3 = $_POST['ke3'];
		$antibiotik4 = $_POST['antibiotik4'];
		$ke4 = $_POST['ke4'];					
		
		$pengisi 			= $_SESSION['userid'];			
		$waktu = date("Y-m-d H:i:s");							
		//echo "<pre>";print_r($_POST);exit();                
		$query ="UPDATE db_ppi.tb_isian SET TGL_UBAH='$waktu', UBAH_OLEH='$pengisi' where  ID_ISIAN='$id_isian'";
		//echo $query; die();	
		$rs = mysqli_query($conn1,$query);
		if($rs)	{						
		$query2 = "UPDATE db_ppi.tb_detail_isian SET						
		TGL_DETAIL='$tanggal',
		TGL_ORDER='$tanggal_order',
		RUANGAN='$ruangan',
		SURVEYOR='$surveyor',
		DIVISI='$divisi',
		SUHU='$suhu',
		NOMR='$nomr',
		NAMA='$nama',
		UMUR='$umur',
		JENIS_KELAMIN='$id_jenis_kelamin',
		DX_MEDIS='$dxmedis',
		UC='$uc',
		IVL='$ivl',			
		CVL='$cvl',
		ETT='$ett',
		VAP='$vap',
		ISK='$isk',
		IAD='$iad',
		IDO='$ido',
		TIRAH='$tirah',
		PLEBITIS='$plebitis',
		CVP='$cvp',
		DARAH='$darah',
		SPUTUM='$sputum',
		SWAB_LUKA='$swabluka',
		SWAB_HIDUNG='$swabhidung',
		SWAB_KETIAK='$swabketiak',
		URINE='$urine',
		CDL='$cdl',
		HCVP='$hcvp',
		HDARAH='$hdarah',
		HSPUTUM='$hsputum',
		HLUKA='$hluka',
		HURINE='$hurine',
		HCDL='$hcdl',
		HKETIAK='$hketiak',
		HHIDUNG='$hhidung',
		ANTIBIOTIK1='$antibiotik1',
		ANTIBIOTIK2='$antibiotik2',
		ANTIBIOTIK3='$antibiotik3',
		ANTIBIOTIK4='$antibiotik4',
		KE1='$ke1',
		KE2='$ke2',
		KE3='$ke3',
		KE4='$ke4'
		where ID_ISIAN='$id_isian'";
		//echo $query2; die();	
		$insert=mysqli_query($conn1,$query2);									
		if($insert){
			 echo '<script>
		swal({
		html: true,
		title: "Sukses",
		text: "Update Survey Harian Berhasil",
		type: "success",
		confirmButtonText: "Lihat Inputan",
		closeOnConfirm: false,
		closeOnCancel: false
		},
		function(isConfirm) {
		if (isConfirm) {
		window.location.href="index.php?modul=tabell";
		} });</script>'; 	
		}
		}
		} else
		if(isset($_POST['btnVerif'])){
		$id_isian 			= $_POST['id_isian'];									
		//echo "<pre>";print_r($_POST);exit();                
		$query3 ="UPDATE db_ppi.tb_detail_isian SET VERIF='2' where  ID_ISIAN='$id_isian'";
		$verf=mysqli_query($conn1,$query3);
		//echo $query3; die();									
		if($verf){
			 echo '<script>
		swal({
		html: true,
		title: "Sukses",
		text: "Data sudah diverifikasi",
		type: "success",
		confirmButtonText: "Lihat Inputan",
		closeOnConfirm: false,
		closeOnCancel: false
		},
		function(isConfirm) {
		if (isConfirm) {
		window.location.href="index.php?modul=tabell";
		} });</script>'; 	
		}
		} 
   		
		else{
			echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
		}                         
?>
</body>
</html>	