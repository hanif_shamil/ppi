<html>
<head>
<script src="assets/sweetalert/dist/sweetalert-dev.js"></script>
<link rel="stylesheet" href="assets/sweetalert/dist/sweetalert.css">

</head>
<body>
<?php
$id=($_GET['id']);
$ver= "select * from tb_detail_isian di where di.ID_ISIAN='$id' and  di.VERIF='2'"; 
$rs1 = mysqli_query($conn1,$ver);
$baca = mysqli_fetch_array($rs1);
//echo $ver; die();	
	if($baca > 1){
	echo '<script>
	swal({
		html: true,
		title: "VERIFIED",
		text: "Data sudah diverifikasi Oleh PPI",
		type: "warning",
		confirmButtonText: "OK",
		closeOnConfirm: false,
		closeOnCancel: false
		},
		function(isConfirm) {
		if (isConfirm) {
		window.location.href="index.php?modul=tabell";
		} });</script>'; 						
	} else {
$query="select tdb.ID_DETAIL, tdb.ID_ISIAN, tdb.TGL_DETAIL, tdb.TGL_ORDER, tdb.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, tdb.NAMA, tdb.NOMR
, tdb.SURVEYOR, tdb.SUHU, tdb.UMUR, if(tdb.JENIS_KELAMIN=1,'Laki-laki','Perempuan') JENIS_KELAMIN, tdb.JENIS_KELAMIN ID_JENIS_KELAMIN
, tdb.DX_MEDIS, tdb.DIVISI, tdb.UC, tdb.IVL, tdb.CVL,tdb.ETT, tdb.VAP, tdb.ISK, tdb.IAD,tdb.IDO, tdb.TIRAH, tdb.PLEBITIS, tdb.CVP, tdb.DARAH, tdb.SPUTUM
, tdb.SWAB_LUKA, tdb.SWAB_HIDUNG, tdb.SWAB_KETIAK, tdb.URINE, tdb.CDL,tdb.HCVP, tdb.HDARAH, tdb.HSPUTUM, tdb.HLUKA, tdb.HURINE, tdb.HCDL, tdb.HKETIAK, tdb.HHIDUNG
, tdb.ANTIBIOTIK1, tdb.ANTIBIOTIK2, tdb.ANTIBIOTIK3, tdb.ANTIBIOTIK4, tdb.KE1, tdb.KE2, tdb.KE3, tdb.KE4
, ab1.NAMA_OBAT OBAT1, ab2.NAMA_OBAT OBAT2, ab3.NAMA_OBAT OBAT3, ab4.NAMA_OBAT OBAT4
from tb_detail_isian tdb
LEFT JOIN referensi rjk ON rjk.ID=tdb.JENIS_KELAMIN and rjk.JENIS=1
LEFT JOIN ruangan ru ON ru.ID=tdb.RUANGAN and ru.JENIS=5
LEFT JOIN antibiotik ab1 ON ab1.ID=tdb.ANTIBIOTIK1
LEFT JOIN antibiotik ab2 ON ab2.ID=tdb.ANTIBIOTIK2
LEFT JOIN antibiotik ab3 ON ab3.ID=tdb.ANTIBIOTIK3
LEFT JOIN antibiotik ab4 ON ab4.ID=tdb.ANTIBIOTIK4
where tdb.ID_ISIAN='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$UC = explode(",", $dt['UC']);$IVL = explode(",", $dt['IVL']);$CVL = explode(",", $dt['CVL']);$ETT = explode(",", $dt['ETT']);$VAP = explode(",", $dt['VAP']);
$ISK = explode(",", $dt['ISK']);$IAD = explode(",", $dt['IAD']);$TIRAH = explode(",", $dt['TIRAH']);$PLEBITIS = explode(",", $dt['PLEBITIS']);
$CVP = explode(",", $dt['CVP']);$DARAH = explode(",", $dt['DARAH']);$SWAB_LUKA = explode(",", $dt['SWAB_LUKA']);$SWAB_HIDUNG = explode(",", $dt['SWAB_HIDUNG']);$SWAB_KETIAK = explode(",", $dt['SWAB_KETIAK']);
$SPUTUM = explode(",", $dt['SPUTUM']);$URINE = explode(",", $dt['URINE']);$IDO = explode(",", $dt['IDO']);$CDL = explode(",", $dt['CDL']);
 {
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Edit Form Harian</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Edit Surveilans Harian Infeksi Rumah Sakit</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="?page=update_h" method="post"  onsubmit="return validateForm()">
								<input type="hidden" name="id_isian" class="form-control" value="<?php echo $id; ?>" readonly>									
									<div class="form-group">										
										<div class="col-sm-4">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TGL_DETAIL']; ?>" name="tanggal" type="text"/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>								
										<div class="col-sm-4">
											<input type="text" id="form-field-1-1" name="surveyor" value="<?php echo $dt['SURVEYOR']; ?>" placeholder="[ Surveyor ]" class="form-control" />
										</div>
										<div class="col-sm-4">
											<input type="text" id="form-field-1-1" name="divisi" value="<?php echo $dt['DIVISI']; ?>" placeholder="[ Divisi ]" class="form-control" />
										</div>
																				
									</div>
									<hr />									
									<div class="form-group">																																																			
										<div class="col-sm-1">
											<input type="text" id="nomr" name="nomr" value="<?php echo $dt['NOMR']; ?>" placeholder="[ No.RM ]" class="form-control" onkeyup="autofill()" />
										</div>							
										<div class="col-sm-3">
											<input type="text" id="nama" name="nama" value="<?php echo $dt['NAMA']; ?>" placeholder="[ Nama ]" class="form-control" readonly />
										</div>														
										<div class="col-sm-1">
											<input type="text" id="umur" value="<?php echo $dt['UMUR']; ?>" name="umur" placeholder="[ Usia ]" class="form-control" readonly>
										</div>															
										<div class="col-sm-2">
											<input name="" id="jenis_kelamin" value="<?php echo $dt['JENIS_KELAMIN']; ?>" placeholder="[ Jenis Kelamin ]" class="form-control" readonly> 
											<input type="hidden" name="id_jenis_kelamin" value="<?php echo $dt['ID_JENIS_KELAMIN']; ?>" id="id_jenis_kelamin" placeholder="Jenis Kelamin" class="form-control">
										</div>
										<div class="col-sm-2">
											<select class="chosen-select form-control" id="id_ruangan" name="ruangan" data-placeholder="[ Ruangan]">
												<option value=""></option>
												<?php
												$sql = "select ru.ID, ru.DESKRIPSI from db_ppi.ruangan ru
												where ru.JENIS=5 and ru.JENIS_KUNJUNGAN=3";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ID_RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>	
												<?php
												}
												?>	
											</select>			
										</div>	
										<div class="col-sm-1">	
											<div class="input-group">										
												<input type="text" id="form-field-1-1" name="suhu" value="<?php echo $dt['SUHU']; ?>" placeholder="[ Suhu ]" class="form-control" />
												<!--<span class="input-group-addon">
													&deg;C
												</span>-->											
											</div>	
										</div>
										<div class="col-sm-2">
											<input type="text" id="form-field-1-1" value="<?php echo $dt['DX_MEDIS']; ?>" name="dxmedis" placeholder=" [DX. MEDIS ]" class="form-control" />
										</div>										
									</div>
									<hr />									
									<div class="form-group">
									<div class="col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">Pemberian antibiotik :</label>
									</div>
									</div>
									<div class="form-group">
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="antibiotik1" id="form-field-select-3" data-placeholder="[ Antibiotik 1]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ANTIBIOTIK1']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['NAMA_OBAT'] ?></option>			
												<?php
												}
												?>	
											</select>
										</div>
										<div class="col-sm-1">
											<input name="ke1" id="" placeholder="[ Ke ]" value="<?php echo $dt['KE1']; ?>" class="form-control"> 
										</div>											
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="antibiotik2" id="form-field-select-3" data-placeholder="[ Antibiotik 2]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ANTIBIOTIK2']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['NAMA_OBAT'] ?></option>
												<?php
												}
												?>	
											</select>
										</div>
										<div class="col-sm-1">
											<input name="ke2" id="" placeholder="[ Ke ]" value="<?php echo $dt['KE2']; ?>" class="form-control"> 
										</div>											
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="antibiotik3" id="form-field-select-3" data-placeholder="[ Antibiotik 3]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ANTIBIOTIK3']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['NAMA_OBAT'] ?></option>
												<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-1">
											<input name="ke3" id="" placeholder="[ Ke ]" value="<?php echo $dt['KE3']; ?>" class="form-control"> 
										</div>
										<div class="col-sm-2">
											<select class="chosen-select form-control" name="antibiotik4" id="form-field-select-3" data-placeholder="[ Antibiotik 4]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.antibiotik ab";												
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ANTIBIOTIK4']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['NAMA_OBAT'] ?></option>
												<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-1">
											<input name="ke4" id="" placeholder="[ Ke ]" value="<?php echo $dt['KE4']; ?>" class="form-control"> 
										</div>											
									</div>
									<hr />
									<div class="form-group">													
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tirah baring :</label>
												<div class="checkbox">
													<label>
														<input name="tirah[]" value="1" <?php if(in_array("1", $TIRAH)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> Ya</span>
													</label>
												</div>
											</div>
										</div>																																															
										<div class="col-xs-12 col-sm-4">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Infeksi Rumah Sakit :</label>
												<div class="checkbox">
													<label>
														<input name="vap[]" value="1" <?php if(in_array("1", $VAP)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> VAP</span>
													</label>											
													<label>
														<input name="isk[]" value="1" <?php if(in_array("1", $ISK)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> ISK</span>
													</label>
													<label>
														<input name="iad[]" value="1" <?php if(in_array("1", $IAD)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> IAD</span>
													</label>
													<label>
														<input name="ido[]" value="1" <?php if(in_array("1", $IDO)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> IDO</span>
													</label>
													<label>
														<input name="plebitis[]" value="1" <?php if(in_array("1", $PLEBITIS)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> PLEBITIS</span>
													</label>
												</div>										
											</div>
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tindakan :</label>
												<div class="checkbox">
													<label>
														<input name="uc[]" type="checkbox" value="1" <?php if(in_array("1", $UC)){ echo " checked=\"checked\""; } ?> class="ace" />							
														<span class="lbl"> UC</span>
													</label>											
													<label>
														<input name="ivl[]" value="1" <?php if(in_array("1", $IVL)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> IVL</span>
													</label>											
													<label>
														<input name="cvl[]" value="1" <?php if(in_array("1", $CVL)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> CVL</span>
													</label>											
													<label>
														<input name="ett[]" value="1" <?php if(in_array("1",$ETT)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> ETT/VENT</span>
													</label>
												</div>									
											</div>
										</div>
										<div class="col-sm-2">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
													<label class="control-label bolder blue" style="text-decoration: underline">Tanggal Order :</label>
												<div class="input-group">
													<input class="form-control" value="<?php echo $dt['TGL_ORDER']; ?>" id="datetimepicker" placeholder="[ Tanggal survey ]" name="tanggal_order" type="text"/>
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>	
										</div>
									</div>									
									<div class="form-group">
										<div class="col-xs-12 col-sm-12">							
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Hasil kultur :</label>
												<div class="checkbox">
													<label>
														<input name="cvp[]" value="1" value="1" <?php if(in_array("1",$CVP)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> CVP</span>
														<input type="text" id="form-field-1-1" name="hcvp" value="<?php echo $dt['HCVP']; ?>" placeholder="[ CVP ]" class="form-control" style="width:180px" />
													</label>
													<label>
														<input name="darah[]" value="1" value="1" <?php if(in_array("1",$DARAH)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> DARAH</span>
														<input type="text" id="form-field-1-1" name="hdarah" value="<?php echo $dt['HDARAH']; ?>" placeholder="[ DARAH ]" class="form-control" style="width:180px" />
													</label>											
													<label>
														<input name="swabluka[]" value="1" value="1" <?php if(in_array("1",$SWAB_LUKA)){ echo " checked=\"checked\""; } ?> type="checkbox" class="ace" />
														<span class="lbl"> SWAB LUKA</span>
														<input type="text" id="form-field-1-1" name="hluka" value="<?php echo $dt['HLUKA']; ?>" placeholder="[ SWAB LUKA ]" class="form-control" style="width:180px" />
													</label>
												
													<label>
														<input name="sputum[]" value="1" value="1" <?php if(in_array("1",$SPUTUM)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> SPUTUM</span>
														<input type="text" id="form-field-1-1" name="hsputum" value="<?php echo $dt['HSPUTUM']; ?>" placeholder="[ SPUTUM ]" class="form-control" style="width:180px" />
													</label>
													<label>
														<input name="urine[]" value="1" value="1" <?php if(in_array("1",$URINE)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> URINE</span>
														<input type="text" id="form-field-1-1" name="hurine" value="<?php echo $dt['HURINE']; ?>" placeholder="[ URINE ]" class="form-control" style="width:180px" />
													</label>
													<label>
														<input name="swabketiak[]" value="1" value="1" <?php if(in_array("1",$SWAB_KETIAK)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> SWAB KETIAK</span>
														<input type="text" id="form-field-1-1" name="hketiak" value="<?php echo $dt['HKETIAK']; ?>" placeholder="[ SWAB KETIAK ]" class="form-control" style="width:180px" />
													</label>
													<label>
														<input name="swabhidung[]" value="1" value="1" <?php if(in_array("1",$SWAB_HIDUNG)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> SWAB HIDUNG</span>
														<input type="text" id="form-field-1-1" name="hhidung" value="<?php echo $dt['HHIDUNG']; ?>" placeholder="[ SWAB HIDUNG ]" class="form-control" style="width:180px" />
													</label>
													<label>
														<input name="cdl[]" value="1" value="1" <?php if(in_array("1",$CDL)){ echo " checked=\"checked\""; } ?> class="ace" type="checkbox" />
														<span class="lbl"> CDL</span>
														<input type="text" id="form-field-1-1" name="hcdl" value="<?php echo $dt['HCDL']; ?>" placeholder="[ CDL ]" class="form-control" style="width:180px" />
													</label>
												</div>
											</div>
										</div>																				
									</div>																
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnEdit" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>	
											<?php if ($_SESSION['id_bagian']=='6')
											{?>
											&nbsp; &nbsp; &nbsp;
											<button class="btn btn-warning" name="btnVerif" type="submit">
												<i class="ace-icon fa fa-key bigger-110"></i>
												Lock
											</button>
											<?php } else { ?>
												<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
											<?php }
												?>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->
<?php
	}
	}
	?>
</body>
</html>	

<script>
	function validateForm() {
        var nama = document.getElementById('nama').value;
        if (nama.trim() === "") {
            alert("Nama tidak boleh kosong. Pastikan nomor RM yang dimasukkan benar.");
			document.getElementById('nomr').value = "";
			document.getElementById('nomr').focus();
            return false; 
        }
        return true; 
    }
</script>