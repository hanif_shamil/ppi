<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SATU'])){
							$SATU = implode(',',$_POST['SATU']);
						}else{
							$SATU = "";
						}		
						if(isset($_POST['DUA'])){
							$DUA = implode(',',$_POST['DUA']);
						}else{
							$DUA = "";
						}	
						if(isset($_POST['TIGA'])){
							$TIGA = implode(',',$_POST['TIGA']);
						}else{
							$TIGA = "";
						}	
						if(isset($_POST['EMPAT'])){
							$EMPAT = implode(',',$_POST['EMPAT']);
						}else{
							$EMPAT = "";
						}
						if(isset($_POST['LIMA'])){
							$LIMA = implode(',',$_POST['LIMA']);
						}else{
							$LIMA = "";
						}	
						if(isset($_POST['ENAM_A'])){
							$ENAM_A = implode(',',$_POST['ENAM_A']);
						}else{
							$ENAM_A = "";
						}	
						if(isset($_POST['ENAM_B'])){
							$ENAM_B = implode(',',$_POST['ENAM_B']);
						}else{
							$ENAM_B = "";
						}
						if(isset($_POST['ENAM_C'])){
							$ENAM_C = implode(',',$_POST['ENAM_C']);
						}else{
							$ENAM_C = "";
						}
						if(isset($_POST['ENAM_D'])){
							$ENAM_D = implode(',',$_POST['ENAM_D']);
						}else{
							$ENAM_D = "";
						}
						if(isset($_POST['ENAM_E'])){
							$ENAM_E = implode(',',$_POST['ENAM_E']);
						}else{
							$ENAM_E = "";
						}	
						if(isset($_POST['TUJUH_A'])){
							$TUJUH_A = implode(',',$_POST['TUJUH_A']);
						}else{
							$TUJUH_A = "";
						}
						if(isset($_POST['TUJUH_B'])){
							$TUJUH_B = implode(',',$_POST['TUJUH_B']);
						}else{
							$TUJUH_B = "";
						}
						if(isset($_POST['TUJUH_C'])){
							$TUJUH_C = implode(',',$_POST['TUJUH_C']);
						}else{
							$TUJUH_C = "";
						}
						if(isset($_POST['TUJUH_D'])){
							$TUJUH_D = implode(',',$_POST['TUJUH_D']);
						}else{
							$TUJUH_D = "";
						}
						if(isset($_POST['ENAM_E'])){
							$ENAM_E = implode(',',$_POST['ENAM_E']);
						}else{
							$ENAM_E = "";
						}
						if(isset($_POST['TUJUH_E'])){
							$TUJUH_E = implode(',',$_POST['TUJUH_E']);
						}else{
							$TUJUH_E = "";
						}
						if(isset($_POST['TUJUH_F'])){
							$TUJUH_F = implode(',',$_POST['TUJUH_F']);
						}else{
							$TUJUH_F = "";
						}
						if(isset($_POST['DELAPAN'])){
							$DELAPAN = implode(',',$_POST['DELAPAN']);
						}else{
							$DELAPAN = "";
						}
						if(isset($_POST['SEMBILAN'])){
							$SEMBILAN = implode(',',$_POST['SEMBILAN']);
						}else{
							$SEMBILAN = "";
						}
						if(isset($_POST['SEPULUH'])){
							$SEPULUH = implode(',',$_POST['SEPULUH']);
						}else{
							$SEPULUH = "";
						}
						if(isset($_POST['SEBELAS'])){
							$SEBELAS = implode(',',$_POST['SEBELAS']);
						}else{
							$SEBELAS = "";
						}
						if(isset($_POST['DUABELAS'])){
							$DUABELAS = implode(',',$_POST['DUABELAS']);
						}else{
							$DUABELAS = "";
						}
						if(isset($_POST['TIGABELAS'])){
							$TIGABELAS = implode(',',$_POST['TIGABELAS']);
						}else{
							$TIGABELAS = "";
						}	
						if(isset($_POST['EMPATBELAS'])){
							$EMPATBELAS = implode(',',$_POST['EMPATBELAS']);
						}else{
							$EMPATBELAS = "";
						}	
						if(isset($_POST['LIMABELAS'])){
							$LIMABELAS = implode(',',$_POST['LIMABELAS']);
						}else{
							$LIMABELAS = "";
						}	
						if(isset($_POST['ENAMBELAS'])){
							$ENAMBELAS = implode(',',$_POST['ENAMBELAS']);
						}else{
							$ENAMBELAS = "";
						}							
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_ido_new (
							TANGGAL,
							RUANGAN,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							AUDITOR,
							NOMR,
							NAMA,
							SATU,
							DUA,
							TIGA,
							EMPAT,
							LIMA,
							ENAM_A,
							ENAM_B,
							ENAM_C,
							ENAM_E,
							TUJUH_A,
							TUJUH_B,
							TUJUH_C,
							TUJUH_D,
							TUJUH_E,
							TUJUH_F,
							DELAPAN,
							SEMBILAN,
							SEPULUH,
							SEBELAS,
							DUABELAS,
							TIGABELAS,
							EMPATBELAS,
							LIMABELAS,
							ENAMBELAS,
							
							TOTAL,
							USER) 
						VALUES 
						('$TANGGAL',
							'$RUANGAN',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$AUDITOR',
							'$NOMR',
							'$NAMA',
							'$SATU',
							'$DUA',
							'$TIGA',
							'$EMPAT',
							'$LIMA',
							'$ENAM_A',
							'$ENAM_B',
							'$ENAM_C',
							'$ENAM_E',
							'$TUJUH_A',
							'$TUJUH_B',
							'$TUJUH_C',
							'$TUJUH_D',
							'$TUJUH_E',
							'$TUJUH_F',
							'$DELAPAN',
							'$SEMBILAN',
							'$SEPULUH',
							'$SEBELAS',
							'$DUABELAS',
							'$TIGABELAS',
							'$EMPATBELAS',
							'$LIMABELAS',
							'$ENAMBELAS',
							
							'$TOTAL',
							'$USER')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
						
						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_ido_new'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID			= $_POST['ID'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$NOMR 			= $_POST['NOMR'];
						$NAMA 			= addslashes($_POST['NAMA']);
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];  
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SATU'])){
							$SATU = implode(',',$_POST['SATU']);
						}else{
							$SATU = "";
						}		
						if(isset($_POST['DUA'])){
							$DUA = implode(',',$_POST['DUA']);
						}else{
							$DUA = "";
						}	
						if(isset($_POST['TIGA'])){
							$TIGA = implode(',',$_POST['TIGA']);
						}else{
							$TIGA = "";
						}	
						if(isset($_POST['EMPAT'])){
							$EMPAT = implode(',',$_POST['EMPAT']);
						}else{
							$EMPAT = "";
						}
						if(isset($_POST['LIMA'])){
							$LIMA = implode(',',$_POST['LIMA']);
						}else{
							$LIMA = "";
						}	
						if(isset($_POST['ENAM_A'])){
							$ENAM_A = implode(',',$_POST['ENAM_A']);
						}else{
							$ENAM_A = "";
						}	
						if(isset($_POST['ENAM_B'])){
							$ENAM_B = implode(',',$_POST['ENAM_B']);
						}else{
							$ENAM_B = "";
						}
						if(isset($_POST['ENAM_C'])){
							$ENAM_C = implode(',',$_POST['ENAM_C']);
						}else{
							$ENAM_C = "";
						}
						if(isset($_POST['ENAM_D'])){
							$ENAM_D = implode(',',$_POST['ENAM_D']);
						}else{
							$ENAM_D = "";
						}
						if(isset($_POST['ENAM_E'])){
							$ENAM_E = implode(',',$_POST['ENAM_E']);
						}else{
							$ENAM_E = "";
						}	
						if(isset($_POST['TUJUH_A'])){
							$TUJUH_A = implode(',',$_POST['TUJUH_A']);
						}else{
							$TUJUH_A = "";
						}
						if(isset($_POST['TUJUH_B'])){
							$TUJUH_B = implode(',',$_POST['TUJUH_B']);
						}else{
							$TUJUH_B = "";
						}
						if(isset($_POST['TUJUH_C'])){
							$TUJUH_C = implode(',',$_POST['TUJUH_C']);
						}else{
							$TUJUH_C = "";
						}
						if(isset($_POST['TUJUH_D'])){
							$TUJUH_D = implode(',',$_POST['TUJUH_D']);
						}else{
							$TUJUH_D = "";
						}
						if(isset($_POST['ENAM_E'])){
							$ENAM_E = implode(',',$_POST['ENAM_E']);
						}else{
							$ENAM_E = "";
						}
						if(isset($_POST['TUJUH_E'])){
							$TUJUH_E = implode(',',$_POST['TUJUH_E']);
						}else{
							$TUJUH_E = "";
						}
						if(isset($_POST['TUJUH_F'])){
							$TUJUH_F = implode(',',$_POST['TUJUH_F']);
						}else{
							$TUJUH_F = "";
						}
						if(isset($_POST['DELAPAN'])){
							$DELAPAN = implode(',',$_POST['DELAPAN']);
						}else{
							$DELAPAN = "";
						}
						if(isset($_POST['SEMBILAN'])){
							$SEMBILAN = implode(',',$_POST['SEMBILAN']);
						}else{
							$SEMBILAN = "";
						}
						if(isset($_POST['SEPULUH'])){
							$SEPULUH = implode(',',$_POST['SEPULUH']);
						}else{
							$SEPULUH = "";
						}
						if(isset($_POST['SEBELAS'])){
							$SEBELAS = implode(',',$_POST['SEBELAS']);
						}else{
							$SEBELAS = "";
						}
						if(isset($_POST['DUABELAS'])){
							$DUABELAS = implode(',',$_POST['DUABELAS']);
						}else{
							$DUABELAS = "";
						}
						if(isset($_POST['TIGABELAS'])){
							$TIGABELAS = implode(',',$_POST['TIGABELAS']);
						}else{
							$TIGABELAS = "";
						}	
						if(isset($_POST['EMPATBELAS'])){
							$EMPATBELAS = implode(',',$_POST['EMPATBELAS']);
						}else{
							$EMPATBELAS = "";
						}	
						if(isset($_POST['LIMABELAS'])){
							$LIMABELAS = implode(',',$_POST['LIMABELAS']);
						}else{
							$LIMABELAS = "";
						}	
						if(isset($_POST['ENAMBELAS'])){
							$ENAMBELAS = implode(',',$_POST['ENAMBELAS']);
						}else{
							$ENAMBELAS = "";
						}
						
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_ido_new SET												
						TANGGAL='$TANGGAL',
						RUANGAN='$RUANGAN',
						ANALISA = '$ANALISA', 
						TINDAKLANJUT = '$TINDAKLANJUT', 
						KEPALA = '$KEPALA',
						AUDITOR='$AUDITOR',
						NOMR='$NOMR',
						NAMA='$NAMA',
						SATU='$SATU',
						DUA='$DUA',
						TIGA='$TIGA',
						EMPAT='$EMPAT',
						LIMA='$LIMA',
						
						ENAM_A='$ENAM_A',
						ENAM_B='$ENAM_B',
						ENAM_C='$ENAM_C',
						ENAM_D='$ENAM_D',
						ENAM_E='$ENAM_E',
						TUJUH_A='$TUJUH_A',
						TUJUH_B='$TUJUH_B',
						TUJUH_C='$TUJUH_C',
						TUJUH_D='$TUJUH_D',
						TUJUH_E='$TUJUH_E',
						TUJUH_F='$TUJUH_F',
						DELAPAN='$DELAPAN',
						SEMBILAN = '$SEMBILAN',
						SEPULUH ='$SEPULUH',
						SEBELAS ='$SEBELAS',
						DUABELAS = '$DUABELAS',
						TIGABELAS ='$TIGABELAS',
						EMPATBELAS = '$EMPATBELAS',
						LIMABELAS ='$LIMABELAS',
						ENAMBELAS ='$ENAMBELAS',
						
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID='$ID'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_ido_new'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_ido_new ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_ido_new'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_ido_new'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			