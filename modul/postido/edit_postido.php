<?php
$id=($_GET['id']);
$query="SELECT ruan.DESKRIPSI RUANGAN ,ruan.ID ID_RUANGAN, ps.NORM,ps.NAMA, IF(ps.JENIS_KELAMIN=1,'52', '53') KODE_JENIS_KELAMIN, IF(ps.JENIS_KELAMIN=1,'Laki-Laki', 'Perempuan') JENIS_KELAMIN
, ps.JENIS_KELAMIN ID_JK,DATE_FORMAT(ps.TANGGAL_LAHIR, '%d-%m-%Y') TANGGAL_LAHIR, p.TANGGAL TGL_MASUK
, (YEAR(CURDATE())-YEAR(ps.TANGGAL_LAHIR)) UMUR, concat((YEAR(CURDATE())-YEAR(ps.TANGGAL_LAHIR)),' Th') UMUR_T
, ref.DESKRIPSI PENJAMIN, ref.ID ID_PENJAMIN, IF(ref.ID=2,'56',IF(ref.ID=62,'57',IF(1,'54','55'))) PENANGGUNG
, IF(ref.ID=2,'BPJS',IF(ref.ID=62,'Karyawan RSKD',IF(1,'Umum/Tanpa Asuransi','Jaminan Perusahaan'))) NAMA_PENANGGUNG
, neg.DESKRIPSI KEWARGANEGARAAN, pek.DESKRIPSI PEKERJAAN, kp.NOMOR TELPON, ps.ALAMAT, kip.NOMOR KTP, p.NOMOR NOPEN
, dia.ICD, dia.DIAGNOSA, CONCAT(dia.ICD,'-',dia.DIAGNOSA) DIAGMASUK, ido.*, kk.NOMOR TELPKEL, kel.NAMA NAMAKEL
FROM pendaftaran.pendaftaran p
LEFT JOIN pendaftaran.kunjungan pk ON p.NOMOR = pk.NOPEN
LEFT JOIN master.ruangan ruan ON ruan.ID = pk.RUANGAN
LEFT JOIN master.pasien ps ON ps.NORM = p.NORM
LEFT JOIN pendaftaran.tujuan_pasien tp ON tp.NOPEN = p.NOMOR
LEFT JOIN master.ruangan r ON r.ID = tp.RUANGAN	
LEFT JOIN pendaftaran.penjamin pj ON pj.NOPEN = p.NOMOR
LEFT JOIN master.referensi ref ON ref.ID = pj.JENIS AND ref.JENIS=10
left JOIN master.negara neg ON neg.ID=ps.KEWARGANEGARAAN
LEFT JOIN master.referensi pek ON pek.ID=ps.PEKERJAAN and pek.JENIS=4
LEFT JOIN master.kontak_pasien kp ON kp.NORM=ps.NORM
LEFT JOIN master.kartu_identitas_pasien kip ON kip.NORM=ps.NORM
LEFT JOIN master.diagnosa_masuk dia ON dia.ID=p.DIAGNOSA_MASUK
left JOIN db_ppi.tb_postido ido ON ido.nomr=ps.NORM
	LEFT JOIN master.kontak_keluarga_pasien kk ON kk.NORM=p.NORM
	LEFT JOIN master.keluarga_pasien kel ON kel.NORM=p.NORM 
WHERE ido.id = '$id' AND p.`STATUS`!=0 #AND pk.`STATUS`=1 
AND ruan.JENIS_KUNJUNGAN in  (2,3)
ORDER BY pk.MASUK DESC LIMIT 1";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">FORMULIR</a>
					</li>
					<li class="active">EDIT SURVEILANS PASIEN POST-OPERATIF INFEKSI DAERAH OPERASI (IDO)</li>
				</ul>			
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">EDIT SURVEILANS PASIEN POST-OPERATIF IDO</h4>
								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>
									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpanpostido" method="post">
										<input type="hidden" name="ID" class="form-control" value="<?php echo $id; ?>" readonly>	
										<div class="form-group">
											<div class="col-sm-2">
												<label>No MR</label>
												<input type="text" id="nomr" name="nomr" value="<?php echo $dt['nomr']; ?>" autocomplete="off" placeholder="[ No.RM ]" class="form-control" onkeyup="autofill()" />
											</div>							
											<div class="col-sm-4">
												<label>Nama Pasien</label>
												<input type="text" id="nama" name="nama" value="<?php echo $dt['NAMA']; ?>" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
												<input type="hidden" id="nopen" name="nopen" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
											</div>
											<div class="col-sm-2">
												<label>Tanggal Lahir</label>
												<div class="input-group date">
													<input name="" id="tgl_lahir" value="<?php echo $dt['NAMA']; ?>" placeholder="[ TANGGAL LAHIR ]" class="form-control" readonly> 
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>	
											<div class="col-sm-2">
												<label>Umur</label>
												<input name="" id="umur_t" value="<?php echo $dt['UMUR']; ?>" placeholder="[ UMUR ]" class="form-control" readonly> 
											</div>	
											<div class="col-sm-2">
												<label>Telepon Pasien</label>
												<input name="" id="telpon" value="<?php echo $dt['TELPON']; ?>" placeholder="[ TELP PASIEN ]" class="form-control" readonly> 
											</div>

										</div>	
										<div class="form-group">
											<div class="col-sm-8">
												<label>Alamat</label>
												<input name="" id="alamat" value="<?php echo $dt['ALAMAT']; ?>" placeholder="[ ALAMAT PASIEN ]" class="form-control" readonly> 
											</div>
											<div class="col-sm-2">
												<label>Telepon Keluarga</label>
												<input name="" id="telpkel" value="<?php echo $dt['TELPKEL']; ?>" placeholder="[ TELP KELUARGA ]" class="form-control" readonly> 
											</div>
											<div class="col-sm-2">
												<label>Nama Keluarga Pasien</label>
												<input name="" id="namakel" value="<?php echo $dt['NAMAKEL']; ?>" placeholder="[ TELP KELUARGA ]" class="form-control" readonly> 
											</div>														
										</div>	
										<div class="form-group">
											<div class="col-sm-4">
												<label>Tanggal Masuk RS untuk Operasi</label>					
												<div class="input-group date">
													<input type="text" id="" name="tglmasuk" value="<?php echo $dt['tglmasuk']; ?>" placeholder="TGL MASUK RS UNTUK OPERASI" class="form-control datetime" autocomplete="off">
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-3">
												<label>Tanggal Keluar Rumah Sakit</label>					
												<div class="input-group date">
													<input type="text" id="" name="tglkeluar" value="<?php echo $dt['tglkeluar']; ?>" placeholder="TGL KELUAR RS" class="form-control datetime" autocomplete="off">
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>								
										</div>
										<hr />
										<div class="form-group">
											<div class="col-sm-2">
												<label>Post OP hari ke</label>
												<input type="text" name="postke" autocomplete="off" value="<?php echo $dt['postke']; ?>" placeholder="[ Post OP ke ]" class="form-control">
											</div>
											<div class="col-sm-3">
												<label>Tanggal</label>					
												<div class="input-group date">
													<input type="text" id="" name="tglsurvey" value="<?php echo $dt['tglsurvey']; ?>" placeholder="Tanggal" class="form-control datetime" autocomplete="off">
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-4">
												<label>Event (Prosedur operasi)</label>
												<input type="text" name="event" autocomplete="off" value="<?php echo $dt['event']; ?>" placeholder="[ Event (Prosedur operasi) ]" class="form-control">
											</div>
											<div class="col-sm-3">
												<label>Antibiotik</label>
												<input type="text" name="antibiotik" autocomplete="off" value="<?php echo $dt['antibiotik']; ?>" placeholder="[ Antibiotik ]" class="form-control">
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-9">
												<label>Tanda gejala IDO</label>
												<input type="text" name="gejala" autocomplete="off" value="<?php echo $dt['gejala']; ?>" placeholder="[ Tanda gejala IDO ]" class="form-control">
											</div>
											<div class="col-sm-3">
												<label>Nama Petugas</label>
												<input type="text" name="petugas" autocomplete="off" value="<?php echo $dt['petugas']; ?>" placeholder="[ Nama Petugas ]" class="form-control">
											</div>	
										</div>

										<hr />
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													%
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }
?>