<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$nomr  				= $_POST['nomr']; 
						$tglmasuk  			= $_POST['tglmasuk']; 
						$tglkeluar  		= $_POST['tglkeluar']; 
						$postke  			= $_POST['postke']; 
						$tglsurvey  		= $_POST['tglsurvey']; 
						$event  			= $_POST['event']; 
						$antibiotik  		= $_POST['antibiotik']; 
						$gejala  			= $_POST['gejala']; 
						$petugas  			= $_POST['petugas']; 					
						$user 				= $_SESSION['userid'];													
						// echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_postido (
							nomr,
							tglmasuk,  
							tglkeluar,  
							postke,  
							tglsurvey,  
							event, 
							antibiotik, 
							gejala,  
							petugas,
							user) 
						VALUES 
						(
							'$nomr',
							'$tglmasuk',
							'$tglkeluar',
							'$postke',
							'$tglsurvey',
							'$event',
							'$antibiotik',
							'$gejala',
							'$petugas',
							'$user')";
						// echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabelpostido'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$id  				= $_POST['ID']; 
						$nomr  				= $_POST['nomr']; 
						$tglmasuk  			= $_POST['tglmasuk']; 
						$tglkeluar  		= $_POST['tglkeluar']; 
						$postke  			= $_POST['postke']; 
						$tglsurvey  		= $_POST['tglsurvey']; 
						$event  			= $_POST['event']; 
						$antibiotik  		= $_POST['antibiotik']; 
						$gejala  			= $_POST['gejala']; 
						$petugas  			= $_POST['petugas'];  				
						$user 				= $_SESSION['userid'];			
						// echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_postido SET												
						nomr           		= '$nomr',
						tglmasuk           	= '$tglmasuk',
						tglkeluar           = '$tglkeluar',
						postke           	= '$postke',
						tglsurvey           = '$tglsurvey',
						event           	= '$event',
						antibiotik          = '$antibiotik',
						gejala           	= '$gejala',
						petugas           	= '$petugas',
						diubah_oleh 		= '$user'
						where id 			= '$id'";
						// echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabelpostido'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_postido ti SET ti.`STATUS`=0 WHERE ti.id = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabelpostido'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabelpostido'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			