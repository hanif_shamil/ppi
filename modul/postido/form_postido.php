<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">FORMULIR</a>
				</li>
				<li class="active">SURVEILANS PASIEN POST-OPERATIF INFEKSI DAERAH OPERASI (IDO)</li>
			</ul>			
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">SURVEILANS PASIEN POST-OPERATIF IDO</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpanpostido" method="post">
									<div class="form-group">
										<div class="col-sm-2">
											<label>No MR</label>
											<input type="text" id="nomr" name="nomr" autocomplete="off" placeholder="[ No.RM ]" class="form-control postid" onkeyup="autofill()" />
										</div>							
										<div class="col-sm-4">
											<label>Nama Pasien</label>
											<input type="text" id="nama" name="nama" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
											<input type="hidden" id="nopen" name="nopen" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
										</div>
										<div class="col-sm-2">
											<label>Tanggal Lahir</label>
											<div class="input-group date">
												<input name="" id="tgl_lahir" placeholder="[ TANGGAL LAHIR ]" class="form-control" readonly> 
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>	
										<div class="col-sm-2">
											<label>Umur</label>
											<input name="" id="umur_t" placeholder="[ UMUR ]" class="form-control" readonly> 
										</div>	
										<div class="col-sm-2">
											<label>Telepon Pasien</label>
											<input name="" id="telpon" placeholder="[ TELP PASIEN ]" class="form-control" readonly> 
										</div>

									</div>	
									<div class="form-group">
										<div class="col-sm-8">
											<label>Alamat</label>
											<input name="" id="alamat" placeholder="[ ALAMAT PASIEN ]" class="form-control" readonly> 
										</div>
										<div class="col-sm-2">
											<label>Telepon Keluarga</label>
											<input name="" id="telpkel" placeholder="[ TELP KELUARGA ]" class="form-control" readonly> 
										</div>
										<div class="col-sm-2">
											<label>Nama Keluarga Pasien</label>
											<input name="" id="namakel" placeholder="[ TELP KELUARGA ]" class="form-control" readonly> 
										</div>														
									</div>	
									<div class="form-group">
										<div class="col-sm-4">
											<label>Tanggal Masuk RS untuk Operasi</label>					
											<div class="input-group date">
												<input type="text" id="" name="tglmasuk" placeholder="TGL MASUK RS UNTUK OPERASI" class="form-control datetime" autocomplete="off">
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">
											<label>Tanggal Keluar Rumah Sakit</label>					
											<div class="input-group date">
												<input type="text" id="" name="tglkeluar" placeholder="TGL KELUAR RS" class="form-control datetime" autocomplete="off">
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>								
									</div>
									<hr />
									<div class="form-group">
										<div class="col-sm-2">
											<label>Post OP hari ke</label>
											<input type="text" id="postid" name="postke" autocomplete="off" placeholder="[ Post OP ke ]" class="form-control">
										</div>
										<div class="col-sm-3">
											<label>Tanggal</label>					
											<div class="input-group date">
												<input type="text" id="" name="tglsurvey" placeholder="Tanggal" class="form-control datetime" autocomplete="off">
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-4">
											<label>Event (Prosedur operasi)</label>
											<input type="text" name="event" autocomplete="off" placeholder="[ Event (Prosedur operasi) ]" class="form-control">
										</div>
										<div class="col-sm-3">
											<label>Antibiotik</label>
											<input type="text" name="antibiotik" autocomplete="off" placeholder="[ Antibiotik ]" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-9">
											<label>Tanda gejala IDO</label>
											<input type="text" name="gejala" autocomplete="off" placeholder="[ Tanda gejala IDO ]" class="form-control">
										</div>
										<div class="col-sm-3">
											<label>Nama Petugas</label>
											<input type="text" name="petugas" autocomplete="off" placeholder="[ Nama Petugas ]" class="form-control">
										</div>	
									</div>

									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>