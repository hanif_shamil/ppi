<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>

		<li>
			<a href="#">Data</a>
		</li>
		<li class="active">DATA SURVEILANS PASIEN POST-OPERATIF INFEKSI DAERAH OPERASI (IDO)</li>
	</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
	<div class="row">
		<div class="col-xs-12">
			<!-- PAGE CONTENT BEGINS -->						
			<div class="row">
				<div class="col-xs-12">
					<!--<div class="clearfix">
						<div class="pull-right tableTools-container"></div>  -- > buat print, simpan dan export tabel
					</div> --> 
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">DATA SURVEILANS PASIEN POST-OPERATIF INFEKSI DAERAH OPERASI (IDO)</h4>
							<div class="widget-toolbar">
								<a href="index.php?page=form_postido">
									<i class="ace-icon fa fa-plus" style="color:blue"> Tambah data</i>
								</a>
							</div>
						</div>
						<!-- div.table-responsive -->
						<!-- div.dataTables_borderWrap -->
						<div>
							<table id="tabeldata" class="table table-striped table-bordered table-hover">
								<thead>
									<tr align="center">
										<th><div align="center">NOMR</div></th>
										<th><div align="center">Nama</div></th>
										<th><div align="center">Tanggal</div></th>
										<th><div align="center">Event</div></th>
										<th><div align="center">Antibiotik</div></th>
										<th><div align="center">Gejala</div></th>
										<th><div align="center">Petugas</div></th>
										<th width="11%"><div align="center">Opsi</div></th>
									</tr>								

								</thead>

								<tbody>
									<?php					
									$query="SELECT ps.NAMA,ps.TANGGAL_LAHIR, id.* 
									FROM db_ppi.tb_postido id
									LEFT JOIN master.pasien ps ON ps.NORM=id.nomr
									WHERE id.STATUS=1";							
									$info=mysqli_query($conn1,$query); 
									while($row=mysqli_fetch_array($info)){
										?>
										<tr>
											<td><?php echo $row['nomr'] ?></td>
											<td><?php echo $row['NAMA'] ?></td>
											<td><?php echo date('d F Y', strtotime($row['tglsurvey'])) ?></td>
											<td><?php echo $row['event'] ?></td>								
											<td><?php echo $row['antibiotik'] ?></td>
											<td><?php echo $row['gejala'] ?></td>
											<td><?php echo $row['petugas'] ?></td>	
											<td align="center">
												
													<a href="?page=edit_postido&id=<?php echo $row['id']?>">
														<i class="ace-icon fa fa-pencil btn btn-success btn-xs"></i>
													</a>
													<a href="?page=simpanpostido&id=<?php echo $row['id']?>">
														<i class="ace-icon fa fa-trash-o btn btn-danger btn-xs"></i>
													</a>
													<a href="http://192.168.7.189/cetakan_hanif/cetakanppi/postido.php?id=<?php echo $row['nomr']?>" target="_blank">
														<i class="ace-icon fa fa-print btn btn-warning btn-xs"></i>
													</a>
												
											</td>
										</tr>							
										<?php							
									}
									?>	
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>