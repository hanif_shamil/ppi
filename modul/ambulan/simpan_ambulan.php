<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$ID				= $_POST['ID'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SATU_A'])){
							$SATU_A = implode(',',$_POST['SATU_A']);
						}else{
							$SATU_A = "";
						}		
						if(isset($_POST['DUA_A'])){
							$DUA_A = implode(',',$_POST['DUA_A']);
						}else{
							$DUA_A = "";
						}	
						if(isset($_POST['TIGA_A'])){
							$TIGA_A = implode(',',$_POST['TIGA_A']);
						}else{
							$TIGA_A = "";
						}	
						if(isset($_POST['EMPAT_A'])){
							$EMPAT_A = implode(',',$_POST['EMPAT_A']);
						}else{
							$EMPAT_A = "";
						}	
						if(isset($_POST['LIMA_A'])){
							$LIMA_A = implode(',',$_POST['LIMA_A']);
						}else{
							$LIMA_A = "";
						}	
						if(isset($_POST['ENAM_A'])){
							$ENAM_A = implode(',',$_POST['ENAM_A']);
						}else{
							$ENAM_A = "";
						}	
						if(isset($_POST['TUJUH_A'])){
							$TUJUH_A = implode(',',$_POST['TUJUH_A']);
						}else{
							$TUJUH_A = "";
						}	
						if(isset($_POST['DELAPAN_A'])){
							$DELAPAN_A = implode(',',$_POST['DELAPAN_A']);
						}else{
							$DELAPAN_A = "";
						}
						if(isset($_POST['SEMBILAN_A'])){
							$SEMBILAN_A = implode(',',$_POST['SEMBILAN_A']);
						}else{
							$SEMBILAN_A = "";
						}
						if(isset($_POST['SEPULUH_A'])){
							$SEPULUH_A = implode(',',$_POST['SEPULUH_A']);
						}else{
							$SEPULUH_A = "";
						}

						if(isset($_POST['SATU_B'])){
							$SATU_B = implode(',',$_POST['SATU_B']);
						}else{
							$SATU_B = "";
						}		
						if(isset($_POST['DUA_B'])){
							$DUA_B = implode(',',$_POST['DUA_B']);
						}else{
							$DUA_B = "";
						}	
						if(isset($_POST['TIGA_B'])){
							$TIGA_B = implode(',',$_POST['TIGA_B']);
						}else{
							$TIGA_B = "";
						}	
						if(isset($_POST['EMPAT_B'])){
							$EMPAT_B = implode(',',$_POST['EMPAT_B']);
						}else{
							$EMPAT_B = "";
						}	
						if(isset($_POST['LIMA_B'])){
							$LIMA_B = implode(',',$_POST['LIMA_B']);
						}else{
							$LIMA_B = "";
						}	
						if(isset($_POST['ENAM_B'])){
							$ENAM_B = implode(',',$_POST['ENAM_B']);
						}else{
							$ENAM_B = "";
						}	
						if(isset($_POST['TUJUH_B'])){
							$TUJUH_B = implode(',',$_POST['TUJUH_B']);
						}else{
							$TUJUH_B = "";
						}	
						if(isset($_POST['DELAPAN_B'])){
							$DELAPAN_B = implode(',',$_POST['DELAPAN_B']);
						}else{
							$DELAPAN_B = "";
						}
						if(isset($_POST['SEMBILAN_B'])){
							$SEMBILAN_B = implode(',',$_POST['SEMBILAN_B']);
						}else{
							$SEMBILAN_B = "";
						}
						if(isset($_POST['SEPULUH_B'])){
							$SEPULUH_B = implode(',',$_POST['SEPULUH_B']);
						}else{
							$SEPULUH_B = "";
						}	
						if(isset($_POST['SEBELAS_B'])){
							$SEBELAS_B = implode(',',$_POST['SEBELAS_B']);
						}else{
							$SEBELAS_B = "";
						}	
						if(isset($_POST['DUABELAS_B'])){
							$DUABELAS_B = implode(',',$_POST['DUABELAS_B']);
						}else{
							$DUABELAS_B = "";
						}	
						if(isset($_POST['TIGABELAS_B'])){
							$TIGABELAS_B = implode(',',$_POST['TIGABELAS_B']);
						}else{
							$TIGABELAS_B = "";
						}	
						if(isset($_POST['EMPATBELAS_B'])){
							$EMPATBELAS_B = implode(',',$_POST['EMPATBELAS_B']);
						}else{
							$EMPATBELAS_B = "";
						}	
						if(isset($_POST['LIMABELAS_B'])){
							$LIMABELAS_B = implode(',',$_POST['LIMABELAS_B']);
						}else{
							$LIMABELAS_B = "";
						}							
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_ambulan (
							TANGGAL,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							AUDITOR,
							SATU_A,
							DUA_A,
							TIGA_A,
							EMPAT_A,
							LIMA_A,
							ENAM_A,
							TUJUH_A,
							DELAPAN_A,
							SEMBILAN_A,
							SEPULUH_A,
							SATU_B,
							DUA_B,
							TIGA_B,
							EMPAT_B,
							LIMA_B,
							ENAM_B,
							TUJUH_B,
							DELAPAN_B,
							SEMBILAN_B,
							SEPULUH_B,
							SEBELAS_B,
							DUABELAS_B,
							TIGABELAS_B,
							EMPATBELAS_B,
							LIMABELAS_B,
							TOTAL,
							USER) 
						VALUES 
						('$TANGGAL',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$AUDITOR',
							'$SATU_A',
							'$DUA_A',
							'$TIGA_A',
							'$EMPAT_A',
							'$LIMA_A',
							'$ENAM_A',
							'$TUJUH_A',
							'$DELAPAN_A',
							'$SEMBILAN_A',
							'$SEPULUH_A',
							'$SATU_B',
							'$DUA_B',
							'$TIGA_B',
							'$EMPAT_B',
							'$LIMA_B',
							'$ENAM_B',
							'$TUJUH_B',
							'$DELAPAN_B',
							'$SEMBILAN_B',
							'$SEPULUH_B',
							'$SEBELAS_B',
							'$DUABELAS_B',
							'$TIGABELAS_B',
							'$EMPATBELAS_B',
							'$LIMABELAS_B',						
							'$TOTAL',
							'$USER')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_ambulan'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID			= $_POST['ID'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						if(isset($_POST['SATU_A'])){
							$SATU_A = implode(',',$_POST['SATU_A']);
						}else{
							$SATU_A = "";
						}		
						if(isset($_POST['DUA_A'])){
							$DUA_A = implode(',',$_POST['DUA_A']);
						}else{
							$DUA_A = "";
						}	
						if(isset($_POST['TIGA_A'])){
							$TIGA_A = implode(',',$_POST['TIGA_A']);
						}else{
							$TIGA_A = "";
						}	
						if(isset($_POST['EMPAT_A'])){
							$EMPAT_A = implode(',',$_POST['EMPAT_A']);
						}else{
							$EMPAT_A = "";
						}	
						if(isset($_POST['LIMA_A'])){
							$LIMA_A = implode(',',$_POST['LIMA_A']);
						}else{
							$LIMA_A = "";
						}	
						if(isset($_POST['ENAM_A'])){
							$ENAM_A = implode(',',$_POST['ENAM_A']);
						}else{
							$ENAM_A = "";
						}	
						if(isset($_POST['TUJUH_A'])){
							$TUJUH_A = implode(',',$_POST['TUJUH_A']);
						}else{
							$TUJUH_A = "";
						}	
						if(isset($_POST['DELAPAN_A'])){
							$DELAPAN_A = implode(',',$_POST['DELAPAN_A']);
						}else{
							$DELAPAN_A = "";
						}
						if(isset($_POST['SEMBILAN_A'])){
							$SEMBILAN_A = implode(',',$_POST['SEMBILAN_A']);
						}else{
							$SEMBILAN_A = "";
						}
						if(isset($_POST['SEPULUH_A'])){
							$SEPULUH_A = implode(',',$_POST['SEPULUH_A']);
						}else{
							$SEPULUH_A = "";
						}

						if(isset($_POST['SATU_B'])){
							$SATU_B = implode(',',$_POST['SATU_B']);
						}else{
							$SATU_B = "";
						}		
						if(isset($_POST['DUA_B'])){
							$DUA_B = implode(',',$_POST['DUA_B']);
						}else{
							$DUA_B = "";
						}	
						if(isset($_POST['TIGA_B'])){
							$TIGA_B = implode(',',$_POST['TIGA_B']);
						}else{
							$TIGA_B = "";
						}	
						if(isset($_POST['EMPAT_B'])){
							$EMPAT_B = implode(',',$_POST['EMPAT_B']);
						}else{
							$EMPAT_B = "";
						}	
						if(isset($_POST['LIMA_B'])){
							$LIMA_B = implode(',',$_POST['LIMA_B']);
						}else{
							$LIMA_B = "";
						}	
						if(isset($_POST['ENAM_B'])){
							$ENAM_B = implode(',',$_POST['ENAM_B']);
						}else{
							$ENAM_B = "";
						}	
						if(isset($_POST['TUJUH_B'])){
							$TUJUH_B = implode(',',$_POST['TUJUH_B']);
						}else{
							$TUJUH_B = "";
						}	
						if(isset($_POST['DELAPAN_B'])){
							$DELAPAN_B = implode(',',$_POST['DELAPAN_B']);
						}else{
							$DELAPAN_B = "";
						}
						if(isset($_POST['SEMBILAN_B'])){
							$SEMBILAN_B = implode(',',$_POST['SEMBILAN_B']);
						}else{
							$SEMBILAN_B = "";
						}
						if(isset($_POST['SEPULUH_B'])){
							$SEPULUH_B = implode(',',$_POST['SEPULUH_B']);
						}else{
							$SEPULUH_B = "";
						}	
						if(isset($_POST['SEBELAS_B'])){
							$SEBELAS_B = implode(',',$_POST['SEBELAS_B']);
						}else{
							$SEBELAS_B = "";
						}	
						if(isset($_POST['DUABELAS_B'])){
							$DUABELAS_B = implode(',',$_POST['DUABELAS_B']);
						}else{
							$DUABELAS_B = "";
						}	
						if(isset($_POST['TIGABELAS_B'])){
							$TIGABELAS_B = implode(',',$_POST['TIGABELAS_B']);
						}else{
							$TIGABELAS_B = "";
						}	
						if(isset($_POST['EMPATBELAS_B'])){
							$EMPATBELAS_B = implode(',',$_POST['EMPATBELAS_B']);
						}else{
							$EMPATBELAS_B = "";
						}	
						if(isset($_POST['LIMABELAS_B'])){
							$LIMABELAS_B = implode(',',$_POST['LIMABELAS_B']);
						}else{
							$LIMABELAS_B = "";
						}							
						$TOTAL	 		= $_POST['TOTAL'];						
						$USER 			= $_SESSION['userid'];		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_ambulan SET												
						TANGGAL='$TANGGAL',
						ANALISA 		= '$ANALISA', 
						TINDAKLANJUT 	= '$TINDAKLANJUT', 
						KEPALA 			= '$KEPALA',
						AUDITOR='$AUDITOR',
						SATU_A='$SATU_A',
						DUA_A='$DUA_A',
						TIGA_A='$TIGA_A',
						EMPAT_A='$EMPAT_A',
						LIMA_A='$LIMA_A',
						ENAM_A='$ENAM_A',
						TUJUH_A='$TUJUH_A',
						DELAPAN_A='$DELAPAN_A',
						SEMBILAN_A='$SEMBILAN_A',
						SEPULUH_A='$SEPULUH_A',
						SATU_B='$SATU_B',
						DUA_B='$DUA_B',
						TIGA_B='$TIGA_B',
						EMPAT_B='$EMPAT_B',
						LIMA_B='$LIMA_B',
						ENAM_B='$ENAM_B',
						TUJUH_B='$TUJUH_B',
						DELAPAN_B='$DELAPAN_B',
						SEMBILAN_B='$SEMBILAN_B',
						SEPULUH_B='$SEPULUH_B',
						SEBELAS_B='$SEBELAS_B',
						DUABELAS_B='$DUABELAS_B',
						TIGABELAS_B='$TIGABELAS_B',
						EMPATBELAS_B='$EMPATBELAS_B',
						LIMABELAS_B='$LIMABELAS_B',
						TOTAL='$TOTAL'
						where ID='$ID'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_ambulan'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_ambulan ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_ambulan'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabel_ambulan'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			