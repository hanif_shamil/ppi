<script type="text/javascript">
	function tugas1()
	{	
		var ya = $('#AA:checked').length;
		var tidak = $('#BB:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<?php
$id=($_GET['id']);
$query="select * from db_ppi.tb_ambulan ti											
where ti.ID='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">Formulir Audit Pembersihan Ambulance</li>
				</ul><!-- /.breadcrumb -->


			</div>

			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Formulir Audit Pembersihan Ambulance</h4>

								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>

									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_ambulan" method="post">
										<input type="hidden" name="ID" class="form-control" value="<?php echo $dt['ID']; ?>" readonly>									
										<div class="form-group">										
											<div class="col-sm-3">
												<div class="input-group">
													<input class="form-control" value="<?php echo $dt['TANGGAL']; ?>" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>

										</div>									
										<div class="form-group">
											<div class="col-sm-12">											
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="text-align:center">NO</th>
															<th style="text-align:center"> KEGIATAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td></td>
															<td>A.	Pembersihan Ambulance setelah digunakan pada pasien non infeksius</td>
															<td align="center">

															</td>
															<td align="center">

															</td>
														</tr>
														<tr>
															<td>1</td>
															<td>Melakukan pembersihan ambulance setiap selesai digunakan</td>
															<td align="center"><label>
																<input type="radio" name="SATU_A[]" <?php if($dt['SATU_A']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">													
																<span class="lbl"></span>
															</label></td>
															<td align="center"><label>
																<input name="SATU_A[]" type="radio" <?php if($dt['SATU_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()" />
																<span class="lbl"></span>
															</label></td>
														</tr>
														<tr>
															<td>2</td>
															<td>Melakukan kebersihan tangan sebelum dan sesudah membersihkan ambulance</td>
															<td align="center">
																<label>
																	<input type="radio" name="DUA_A[]" <?php if($dt['DUA_A']=='1') echo " checked "?> id="AA" value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_A[]" type="radio" <?php if($dt['DUA_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Menggunakan APD: sarung tangan latex yang sampai sesiku</td>
															<td align="center">
																<label>
																	<input name="TIGA_A[]" type="radio" <?php if($dt['TIGA_A']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="TIGA_A[]" type="radio" <?php if($dt['TIGA_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Pembersihan dilakukan dari area luar ke dalam (body luar ambulance- ruang setir supir- ruang pasien)</td>
															<td align="center">
																<label>
																	<input name="EMPAT_A[]" type="radio" <?php if($dt['EMPAT_A']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="EMPAT_A[]" type="radio" <?php if($dt['EMPAT_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>	
														<tr>
															<td>5</td>
															<td>Seluruh permukaan bagian dalam dan luar ambulance dibersihkan secara merata menggunakan deterjen, kemudian dibilas dengan air bersih.</td>
															<td align="center">
																<label>
																	<input name="LIMA_A[]" type="radio" <?php if($dt['LIMA_A']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="LIMA_A[]" type="radio" <?php if($dt['LIMA_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>	
														<tr>
															<td>6</td>
															<td>Melakukan desinfeksi dengan sterisid dengan cara menyemprotkan sterisid ke kain lap baru bersihkan ke seluruh permukaan bagian dalam dan luar ambulance</td>
															<td align="center">
																<label>
																	<input name="ENAM_A[]" type="radio" <?php if($dt['ENAM_A']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="ENAM_A[]" type="radio" <?php if($dt['ENAM_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>	
														<tr>
															<td>7</td>
															<td>Alat-alat yang ada di dalamnya yaitu alat semi kritikal seperti laryngoscope, masker ambubag setelah digunakan lakukan dekontaminasi tingkat tinggi di CSSD (kirim ke CSSD)</td>
															<td align="center">
																<label>
																	<input name="TUJUH_A[]" type="radio" <?php if($dt['TUJUH_A']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="TUJUH_A[]" type="radio" <?php if($dt['TUJUH_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>	
														<tr>
															<td>8</td>
															<td>Alat-alat non kritikal seperti handle laryngoscope, Cuff tensi meter, stetoscope, collar brace, stretcher, defibrillator, dll setelah digunakan, lakukan desinfeksi  permukaan dengan sterisid, dengan cara meyemprotkan sterisid ke kain lap terlebih dahulu baru di lap ke peralatan  tersebut dari arah bersih ke kotor.</td>
															<td align="center">
																<label>
																	<input name="DELAPAN_A[]" type="radio" <?php if($dt['DELAPAN_A']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DELAPAN_A[]" type="radio" <?php if($dt['DELAPAN_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>9</td>
															<td>Melepaskan sarung tangan dan membersihkan sarung tangan latex menggunakan detergent.</td>
															<td align="center">
																<label>
																	<input name="SEMBILAN_A[]" type="radio" <?php if($dt['SEMBILAN_A']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="SEMBILAN_A[]" type="radio" <?php if($dt['SEMBILAN_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>10</td>
															<td>Linen pasien (sarung bantal, laken, selimut) dimasukkan ke dalam plastik hitam dan kirim ke laundry</td>
															<td align="center">
																<label>
																	<input name="SEPULUH_A[]" type="radio" <?php if($dt['SEPULUH_A']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="SEPULUH_A[]" type="radio" <?php if($dt['SEPULUH_A']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td></td>
															<td>B.	Pembersihan Ambulance setelah digunakan pada pasien  infeksius</td>
															<td align="center">

															</td>
															<td align="center">

															</td>
														</tr>
														<tr>
															<td>1</td>
															<td>Petugas ambulance melakukan dekontaminasi ambulance setelah digunakan.</td>
															<td align="center">
																<label>
																	<input name="SATU_B[]" type="radio" <?php if($dt['SATU_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="SATU_B[]" type="radio" <?php if($dt['SATU_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Melakukan kebersihan tangan sebelum dan sesudah membersihkan ambulance</td>
															<td align="center">
																<label>
																	<input name="DUA_B[]" type="radio" <?php if($dt['DUA_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUA_B[]" type="radio" <?php if($dt['DUA_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Menggunakan APD: masker, gaun, google dan sarung tangan dan sepatu boot</td>
															<td align="center">
																<label>
																	<input name="TIGA_B[]" type="radio" <?php if($dt['TIGA_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="TIGA_B[]" type="radio" <?php if($dt['TIGA_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Pembersihan dilakukan dari area luar ke dalam (body luar ambulance- ruang setir supir- ruang pasien)</td>
															<td align="center">
																<label>
																	<input name="EMPAT_B[]" type="radio" <?php if($dt['EMPAT_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="EMPAT_B[]" type="radio" <?php if($dt['EMPAT_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>5</td>
															<td>Seluruh permukaan bagian dalam ambulan dibersihkan menggunakan deterjen secara merata, kemudian bilas dengan air bersih.</td>
															<td align="center">
																<label>
																	<input name="LIMA_B[]" type="radio" <?php if($dt['LIMA_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="LIMA_B[]" type="radio" <?php if($dt['LIMA_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Melakukan desinfeksi dengan sterisid yang disemprotkan ke kain lap terlebihdahulu, lalu bersihkan seluruh area dalam ambulance</td>
															<td align="center">
																<label>
																	<input name="ENAM_B[]" type="radio" <?php if($dt['ENAM_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="ENAM_B[]" type="radio" <?php if($dt['ENAM_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>7</td>
															<td>Bila ada tumpahan darah atau cairan tubuh, serap darah/cairan tubuh dengan kertas koran/HVS kemudian siram dengan larutan klorin 0,5% dan biarkan selama 10 menit kemudian dibersihkan dengan sterisid.</td>
															<td align="center">
																<label>
																	<input name="TUJUH_B[]" type="radio" <?php if($dt['TUJUH_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="TUJUH_B[]" type="radio" <?php if($dt['TUJUH_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>8</td>
															<td>Melakukan desinfeksi dengan larutan klorin 0,5% bila cairan tubuh pasien sudah mengering</td>
															<td align="center">
																<label>
																	<input name="DELAPAN_B[]" type="radio" <?php if($dt['DELAPAN_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DELAPAN_B[]" type="radio" <?php if($dt['DELAPAN_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>9</td>
															<td>Alat-alat yang ada di dalamnya yaitu alat semi kritikal seperti laryngoscope dan Ambubag setelah digunakan lakukan dekontaminasi tingkat tinggi di CSSD (kirim ke CSSD)</td>
															<td align="center">
																<label>
																	<input name="SEMBILAN_B[]" type="radio" <?php if($dt['SEMBILAN_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="SEMBILAN_B[]" type="radio" <?php if($dt['SEMBILAN_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>10</td>
															<td>Alat-alat non kritikal seperti handle laryngoscope, Cuff tensi meter, stetoscope, collar brace, stretcher, defibrillator, dll setelah digunakan, lakukan desinfeksi  permukaan dengan sterisid, dengan cara meyemprotkan sterisid ke kain lap terlebih dahulu, lalu di lap ke peralatan  tersebut dari arah bersih ke kotor.(bila peralatan tersebut terkena cairan tubuh pasien bersihkan terlebihdahulu dengan detergent, baru desinfeksi dengan sterisid)</td>
															<td align="center">
																<label>
																	<input name="SEPULUH_B[]" type="radio" <?php if($dt['SEPULUH_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="SEPULUH_B[]" type="radio" <?php if($dt['SEPULUH_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>11</td>
															<td>Memasukkan linen kotor ke dalam palstik infeksius untuk dikirim ke laundry.</td>
															<td align="center">
																<label>
																	<input name="SEBELAS_B[]" type="radio" <?php if($dt['SEBELAS_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="SEBELAS_B[]" type="radio" <?php if($dt['SEBELAS_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>12</td>
															<td>Bagian luar ambulan disiram menggunakan air mengalir (shower) dan  larutan klorin 0.5%.</td>
															<td align="center">
																<label>
																	<input name="DUABELAS_B[]" type="radio" <?php if($dt['DUABELAS_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="DUABELAS_B[]" type="radio" <?php if($dt['DUABELAS_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>13</td>
															<td>Setelah disiram pintu ambulan dibuka untuk menjamin adanya pertukaran udara</td>
															<td align="center">
																<label>
																	<input name="TIGABELAS_B[]" type="radio" <?php if($dt['TIGABELAS_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="TIGABELAS_B[]" type="radio" <?php if($dt['TIGABELAS_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>14</td>
															<td>Lepaskan APD dan tempatkan ke tempat sampah infeksius</td>
															<td align="center">
																<label>
																	<input name="EMPATBELAS_B[]" type="radio" <?php if($dt['EMPATBELAS_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="EMPATBELAS_B[]" type="radio" <?php if($dt['EMPATBELAS_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>15</td>
															<td>Petugas ambulan melakukan pembersihan diri dengan mandi menggunakan sabun chlorhexidin 4% kemudian mengganti pakaian dengan yang bersih.</td>
															<td align="center">
																<label>
																	<input name="LIMABELAS_B[]" type="radio" <?php if($dt['LIMABELAS_B']=='1') echo " checked "?> value="1" id="AA" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="LIMABELAS_B[]" type="radio" <?php if($dt['LIMABELAS_B']=='2') echo " checked "?> value="2" id="BB" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td style="text-align:center" colspan="2">Total</td>
															<td style="text-align:center"></td>
															<td style="text-align:center"></td>
														</tr>
														<tr>
															<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
															<td colspan="2" style="text-align:center">
																<div class="input-group">
																	<input name="TOTAL" type="text" value="<?php echo $dt['TOTAL']; ?>" id="total" style="width:60px"></br>
																	<span class="input-group-addon">
																		%
																	</span>
																</div>														
															</td>														
														</tr>														
													</tbody>
												</table>											
											</div>
										</div>													
										<div class="form-group">										
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
												<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
											</div>				
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
												<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
											</div>
										</div>
										<div class="form-group">			
											<div class="col-md-6 col-sm-12">
												<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
												<input type="text" id="nama" value="<?php echo $dt['KEPALA']; ?>" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
											</div>	
											<div class="col-md-6 col-sm-12">	
												<label class="control-label bolder blue" style="text-decoration: underline">OBSERVATOR</label>				
												<input type="text" id="nama" value="<?php echo $dt['AUDITOR']; ?>" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
											</div>								
										</div>
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													%
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->	
		</div> <!-- container -->
	</div><!-- /.main-content -->
	<?php
}?>