<html>
<head>
	<script src="assets/sweetalert/dist/sweetalert-dev.js"></script>
	<link rel="stylesheet" href="assets/sweetalert/dist/sweetalert.css">

</head>
<body>

	<?php
	date_default_timezone_set('Asia/Jakarta');
	if(isset($_POST['btnEdit'])){
		$ID 			= $_POST['ID'];	
		$TANGGAL 			= $_POST['TANGGAL'];			
		$RUANGAN 			= $_POST['RUANGAN'];								
		$NILAI 				= $_POST['NILAI'];
		$ANALISA 			=	$_POST['ANALISA'];  
		$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
		$KEPALA 			=	$_POST['KEPALA'];  
		$AUDITOR 			= addslashes($_POST['AUDITOR']);
		if(isset($_POST['PETUGAS'])){
			$PETUGAS = implode(',',$_POST['PETUGAS']);
		}else{
			$PETUGAS = "";
		}						
		if(isset($_POST['TOPI'])){
			$TOPI = implode(',',$_POST['TOPI']);
		}else{
			$TOPI = "";
		}					
		if(isset($_POST['APRON'])){
			$APRON = implode(',',$_POST['APRON']);
		}else{
			$APRON = "";
		}						
		if(isset($_POST['GAUN'])){
			$GAUN = implode(',',$_POST['GAUN']);
		}else{
			$GAUN = "";
		}														
		if(isset($_POST['SARUNGTANGAN'])){
			$SARUNGTANGAN = implode(',',$_POST['SARUNGTANGAN']);
		}else{
			$SARUNGTANGAN = "";
		}						
		if(isset($_POST['MASKER'])){
			$MASKER = implode(',',$_POST['MASKER']);
		}else{
			$MASKER = "";
		}					
		if(isset($_POST['SEPATU'])){
			$SEPATU = implode(',',$_POST['SEPATU']);
		}else{
			$SEPATU = "";
		}	
		if(isset($_POST['KACAMATA'])){
			$KACAMATA = implode(',',$_POST['KACAMATA']);
		}else{
			$KACAMATA = "";
		}		
		if(isset($_POST['HAZMART'])){
			$HAZMART = implode(',',$_POST['HAZMART']);
		}else{
			$HAZMART = "";
		}					
		$pengisi 			= $_SESSION['userid'];					
		$waktu = date("Y-m-d H:i:s");							
//echo "<pre>";print_r($_POST);exit();                											
		$query2 = "UPDATE db_ppi.tb_apdnew SET						
		RUANGAN='$RUANGAN',
		TANGGAL='$TANGGAL',
		PETUGAS='$PETUGAS',
		TOPI='$TOPI',						
		APRON='$APRON',						
		GAUN='$GAUN',						
		SARUNGTANGAN='$SARUNGTANGAN',						
		MASKER='$MASKER',						
		SEPATU='$SEPATU',
		KACAMATA='$KACAMATA',
		HAZMART='$HAZMART',
		NILAI='$NILAI',
		TGL_UBAH='$waktu', 
		PENGUBAH='$pengisi',
		ANALISA = '$ANALISA', 
		TINDAKLANJUT = '$TINDAKLANJUT', 
		KEPALA = '$KEPALA',
		AUDITOR='$AUDITOR'
		where ID='$ID'";
// echo $query2; die();	
		$insert=mysqli_query($conn1,$query2);									
		if($insert){
			echo '<script>
			swal({
				html: true,
				title: "Sukses",
				text: "Sukses Update Audit APD",
				type: "success",
				confirmButtonText: "OK",
				closeOnConfirm: false,
				closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						window.location.href="index.php?page=tabel_apdnew";
					} 
					});
					</script>'; 
				}
			}
			if(isset($_POST['btnVerif'])){
				$id_isian 			= $_POST['ID_HAND'];									
//echo "<pre>";print_r($_POST);exit();                
				$query3 ="UPDATE db_ppi.tb_apdnew SET VERIF='2' where ID='$id_isian'";
				$verf=mysqli_query($conn1,$query3);
//echo $query3; die();									
				if($verf){
					echo '<script>
					swal({
						html: true,
						title: "Sukses",
						text: "Data sudah diverifikasi",
						type: "success",
						confirmButtonText: "OK",
						closeOnConfirm: false,
						closeOnCancel: false
						},
						function(isConfirm) {
							if (isConfirm) {
								window.location.href="index.php?page=tabel_apdnew";
							} });</script>'; 	
						}
					} 						

					?>
				</body>
				</html>	