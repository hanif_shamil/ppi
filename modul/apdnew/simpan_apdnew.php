<html>
<head>
	<script src="assets/sweetalert/dist/sweetalert-dev.js"></script>
	<link rel="stylesheet" href="assets/sweetalert/dist/sweetalert.css">
</head>
<body>
	<?php
	date_default_timezone_set('Asia/Jakarta');
	if(isset($_POST['btnsimpan'])){						
		$TANGGAL 			= $_POST['TANGGAL'];		
		$RUANGAN 			= $_POST['RUANGAN'];								
		$NILAI 				= $_POST['NILAI'];
		$ANALISA 			=	$_POST['ANALISA'];  
		$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
		$KEPALA 			=	$_POST['KEPALA'];  
		$AUDITOR 			= addslashes($_POST['AUDITOR']);
		if(isset($_POST['PETUGAS'])){
			$PETUGAS = implode(',',$_POST['PETUGAS']);
		}else{
			$PETUGAS = "";
		}						
		if(isset($_POST['TOPI'])){
			$TOPI = implode(',',$_POST['TOPI']);
		}else{
			$TOPI = "";
		}

		if(isset($_POST['APRON'])){
			$APRON = implode(',',$_POST['APRON']);
		}else{
			$APRON = "";
		}

		if(isset($_POST['GAUN'])){
			$GAUN = implode(',',$_POST['GAUN']);
		}else{
			$GAUN = "";
		}		

		if(isset($_POST['SARUNGTANGAN'])){
			$SARUNGTANGAN = implode(',',$_POST['SARUNGTANGAN']);
		}else{
			$SARUNGTANGAN = "";
		}

		if(isset($_POST['MASKER'])){
			$MASKER = implode(',',$_POST['MASKER']);
		}else{
			$MASKER = "";
		}

		if(isset($_POST['SEPATU'])){
			$SEPATU = implode(',',$_POST['SEPATU']);
		}else{
			$SEPATU = "";
		}	
		if(isset($_POST['KACAMATA'])){
			$KACAMATA = implode(',',$_POST['KACAMATA']);
		}else{
			$KACAMATA = "";
		}	
	}	
	if(isset($_POST['HAZMART'])){
		$HAZMART = implode(',',$_POST['HAZMART']);
	}else{
		$HAZMART = "";
	}			

	$pengisi 			= $_SESSION['userid'];							
						//echo "<pre>";print_r($_POST);exit();                	
	$query2 = "INSERT INTO db_ppi.tb_apdnew (
		RUANGAN,
		TANGGAL,
		PETUGAS,
		TOPI,
		APRON,
		GAUN,
		SARUNGTANGAN,
		MASKER,
		SEPATU,
		KACAMATA,
		HAZMART,
		NILAI,
		PENGISI,
		ANALISA,
		TINDAKLANJUT,
		KEPALA,
		AUDITOR) 
	VALUES 
	('$RUANGAN',
		'$TANGGAL',
		'$PETUGAS',
		'$TOPI',
		'$APRON',
		'$GAUN',
		'$SARUNGTANGAN',
		'$MASKER',
		'$SEPATU',
		'$KACAMATA',
		'$HAZMART',
		'$NILAI',	
		'$pengisi',
		'$ANALISA',
		'$TINDAKLANJUT',
		'$KEPALA',
		'$AUDITOR')";
						// echo $query2; die();	
	$insert=mysqli_query($conn1,$query2);									
	if($insert){
		echo '<script>
		swal({
			html: true,
			title: "Sukses",
			text: "Sukses Simpan Audit APD",
			type: "success",
			confirmButtonText: "OK",
			closeOnConfirm: false,
			closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					window.location.href="index.php?page=tabel_apdnew";
				} 
				});
				</script>'; 
			}                     
			else{
				echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
			}                         
			?>
		</body>
		</html>			