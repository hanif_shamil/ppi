<?php
function serialize_ke_string($serial)
{
	$hasil = unserialize($serial);
	return implode(', ', $hasil);
}
?>
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
	<ul class="breadcrumb">
		<li>
			<i class="ace-icon fa fa-home home-icon"></i>
			<a href="#">Home</a>
		</li>
		<li>
			<a href="#">Tables</a>
		</li>
		<li class="active">Laporan Kepatuha APD</li>
	</ul>
	<div class="widget-box">
		<div class="widget-header">
			<h4 class="widget-title">Data Laporan Kepatuhan APD</h4>
			<div class="widget-toolbar">
				<a href="index.php?page=apdnew">
					<i class="ace-icon fa fa-plus" style="color:blue"> Tambah data</i>
				</a>
			</div>
		</div>	
		<div>
			<table id="tbapdnew" class="table table-striped table-bordered table-hover" width="100%">
				<thead>
					<tr align="center">
						<th rowspan="2" width="10%"><div align="center">Tanggal</div></th>
						<th rowspan="2"><div align="center">Ruangan</div></th>
						<th colspan="8"><div align="center">INDIKATOR</div></th>
						<th rowspan="2" width="7%"><div align="center">Nilai</div></th>				
						<th rowspan="2" width="7%"><div align="center">Opsi</div></th>
					</tr>
					<tr>
						<td align="center" width="9%">TOPI</td>
						<td align="center" width="8%">APRON</td>
						<td align="center" width="8%">GAUN</td>
						<td align="center" width="10%">SARNG TANGAN</td>
						<td align="center" width="8%">MASKER</td>
						<td align="center" width="8%">SEPATU</td>
						<td align="center" width="8%">KACAMATA</td>
						<td align="center" width="8%">HAZMART</td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<!-- PAGE CONTENT ENDS -->

