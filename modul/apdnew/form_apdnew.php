<script type="text/javascript">
	function tugas1()
	{
		var jumlah=0;
		var jumlahx=0;
//var nilaix;
		var nilai;

		if(document.getElementById("TOPI").checked)
		{
			nilai=document.getElementById("TOPI").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("TOPITDK").checked)
		{
			nilai=document.getElementById("TOPITDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("APRON").checked)
		{
			nilai=document.getElementById("APRON").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("APRONTDK").checked)
		{
			nilai=document.getElementById("APRONTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}

		if(document.getElementById("GAUN").checked)
		{
			nilai=document.getElementById("GAUN").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("GAUNTDK").checked)
		{
			nilai=document.getElementById("GAUNTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("SARUNGTANGAN").checked)
		{
			nilai=document.getElementById("SARUNGTANGAN").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("SARUNGTANGANTDK").checked)
		{
			nilai=document.getElementById("SARUNGTANGANTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("MASKER").checked)
		{
			nilai=document.getElementById("MASKER").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("MASKERTDK").checked)
		{
			nilai=document.getElementById("MASKERTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}

		if(document.getElementById("SEPATU").checked)
		{
			nilai=document.getElementById("SEPATU").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("SEPATUTDK").checked)
		{
			nilai=document.getElementById("SEPATUTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("KACAMATA").checked)
		{
			nilai=document.getElementById("KACAMATA").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("KACAMATATDK").checked)
		{
			nilai=document.getElementById("KACAMATATDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}
		if(document.getElementById("HAZMART").checked)
		{
			nilai=document.getElementById("HAZMART").value;
			jumlah=jumlah+parseInt(nilai);
		}
		if(document.getElementById("HAZMARTTDK").checked)
		{
			nilai=document.getElementById("HAZMARTTDK").value;
			jumlahx=jumlahx+parseInt(nilai)/2;
		}

		document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
	}
</script>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Form Monitoring Kepatuhan APD</li>
			</ul><!-- /.breadcrumb -->


		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Monitoring Kepatuhan APD</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_apdnew" method="post">								
									<div class="form-group">										
										<div class="col-sm-3">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Tanggal survey :</label>
												<div class="input-group">
													<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" id="datetimepicker1" placeholder="[ Tanggal survey ]" name="TANGGAL" type="text"/>
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>	
										</div>
										<div class="col-sm-3">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Ruangan :</label>
												<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ Ruangan ]">
													<option value=""></option>
													<?php
													$sql = "select * from db_ppi.ruangan r
													where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0 and r.STATUS=1";
													$rs = mysqli_query($conn1,$sql);
													while ($data = mysqli_fetch_array($rs)) {
														?>
														<option value="<?=$data[0]?>"><?=$data[3]?></option>
														<?php
													}
													?>	
												</select>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">Jenis petugas kesehatan :</label>
												<div class="radio">
													<label>
														<input name="PETUGAS[]" type="radio" value="11" class="ace" />
														<span class="lbl"> LAB</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="12" type="radio" class="ace" />
														<span class="lbl"> DR</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="13" class="ace" type="radio" />
														<span class="lbl"> PWT</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="14" class="ace" type="radio" />
														<span class="lbl"> POS</span>
													</label>
													<label>
														<input name="PETUGAS[]" type="radio" value="15" class="ace" />
														<span class="lbl"> TH</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="16" type="radio" class="ace" />
														<span class="lbl"> M. KEP</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="17" class="ace" type="radio" />
														<span class="lbl"> M. KED</span>
													</label>											
													<label>
														<input name="PETUGAS[]" value="18" class="ace" type="radio" />
														<span class="lbl"> DLL</span>
													</label>
												</div>									
											</div>
										</div>
									</div>
									<hr />									
									<div class="form-group">																																																			
										<div class="col-sm-12">
											<div class="control-group" style="border: 1px solid #eee;  padding: 10px;">
												<label class="control-label bolder blue" style="text-decoration: underline">INDIKASI PENGGUNAAN APD</label>
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="5%" style="text-align:center">NO</th>
															<th style="text-align:center"> INDIKATOR</th>
															<th width="5%" style="text-align:center">YA</th>

															<th width="5%" style="text-align:center">TIDAK</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>TOPI DIGUNAKAN SESUAI INDIKASI</td>
															<td align="center"><label>
																<input type="radio" name="TOPI[]" id="TOPI" value="1" class="ace input-lg" onClick="tugas1()">																																													
																<span class="lbl"></span>
															</label></td>

															<td align="center">
																<label>
																	<input name="TOPI[]" type="radio" value="2" id="TOPITDK" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>APRON DIGUNAKAN SESUAI INDIKASI</td>
															<td align="center"><label>
																<input type="radio" name="APRON[]" id="APRON" value="1" class="ace input-lg" onClick="tugas1()">													
																<span class="lbl"></span>
															</label>
														</td>

														<td align="center"><label>
															<input type="radio" name="APRON[]" id="APRONTDK" value="2" class="ace input-lg" onClick="tugas1()">													
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>3</td>
													<td>GAUN DIGUNAKAN SESUAI INDIKASI</td>
													<td align="center">
														<label>
															<input type="radio" name="GAUN[]" id="GAUN" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center">
														<label>
															<input type="radio" name="GAUN[]" id="GAUNTDK" value="2" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>4</td>
													<td>SARUNG TANGAN DIGUNAKAN SESUAI INDIKASI</td>
													<td align="center">
														<label>
															<input name="SARUNGTANGAN[]" type="radio" value="1" id="SARUNGTANGAN" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center">
														<label>
															<input name="SARUNGTANGAN[]" type="radio" value="2" id="SARUNGTANGANTDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>5</td>
													<td>MASKER DIGUNAKAN SESUAI INDIKASI</td>
													<td align="center">
														<label>
															<input name="MASKER[]" type="radio" value="1" id="MASKER" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center">
														<label>
															<input name="MASKER[]" type="radio" value="2" id="MASKERTDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>	
												<tr>
													<td>6</td>
													<td>SEPATU PELINDUNG DIGUNAKAN SESUAI INDIKASI</td>
													<td align="center">
														<label>
															<input name="SEPATU[]" type="radio" value="1" id="SEPATU" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center">
														<label>
															<input name="SEPATU[]" type="radio" value="2" id="SEPATUTDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>	
												<tr>
													<td>7</td>
													<td>PELINDUNG MATA ( KACAMATA/FACE SHIELD ) SESUAI INDIKASI</td>
													<td align="center">
														<label>
															<input name="KACAMATA[]" type="radio" value="1" id="KACAMATA" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center">
														<label>
															<input name="KACAMATA[]" type="radio" value="2" id="KACAMATATDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>
												<tr>
													<td>8</td>
													<td>HAZMART DIPAKAI SESUAI INDIKASI</td>
													<td align="center">
														<label>
															<input name="HAZMART[]" type="radio" value="1" id="HAZMART" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>

													<td align="center">
														<label>
															<input name="HAZMART[]" type="radio" value="2" id="HAZMARTTDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
												</tr>									
												<tr>
													<td style="text-align:center" colspan="2">NILAI KEPATUHAN</td>
													<td colspan="3" style="text-align:center">
														<div class="input-group">
															<input type="text" id="total" name="NILAI" placeholder="[ NILAI ]" class="form-control" />
															<span class="input-group-addon">
																%
															</span>
														</div>													
													</td>														
												</tr>													
											</tbody>
										</table>																									
									</div>
								</div>
							</div>																					<div class="form-group">										
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
									<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"></textarea>
								</div>				
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
									<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"></textarea>
								</div>
							</div>
							<div class="form-group">			
								<div class="col-md-6 col-sm-12">
									<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
									<input type="text" id="nama" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
								</div>	
								<div class="col-md-6 col-sm-12">	
									<label class="control-label bolder blue" style="text-decoration: underline">AUDITOR</label>				
									<input type="text" id="nama" name="AUDITOR" placeholder="[ AUDITOR ]" class="form-control" />
								</div>								
							</div>					
							<hr />
							<div class="form-group">							
								<div class="col-md-offset-3 col-md-9">
									<button class="btn btn-info" name="btnsimpan" type="submit">
										<i class="ace-icon fa fa-check bigger-110"></i>
										Submit
									</button>
									&nbsp; &nbsp; &nbsp;
									<button class="btn" type="reset">
										<i class="ace-icon fa fa-undo bigger-110"></i>
										Reset
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div><!-- /.span -->
	</div>
</div><!-- /.page-content -->	
</div> <!-- container -->
		</div><!-- /.main-content -->