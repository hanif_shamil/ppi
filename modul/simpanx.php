<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses Simpan</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						if(isset($_POST['btnsimpan'])){
						$id_isian 			= $_POST['id_isian'];							
						$tanggal 			= $_POST['tanggal'];		
						$ruangan 			= $_POST['ruangan'];								
						$surveyor 			= $_POST['surveyor'];			
						$divisi 			= $_POST['divisi'];					
						$suhu 				= $_POST['suhu'];			
						$nomr 				= $_POST['nomr'];			
						$nama 				= addslashes($_POST['nama']);			
						$umur 				= $_POST['umur'];		
						$id_jenis_kelamin 	= $_POST['id_jenis_kelamin'];	
						$dxmedis 			= $_POST['dxmedis'];
						
						if(isset($_POST['tindakan'])){
							$tindakan = implode(',',$_POST['tindakan']);
						}else{
							$tindakan = "";
						}	
						
						if(isset($_POST['infeksi'])){
							$infeksi = implode(',',$_POST['infeksi']);
						}else{
							$infeksi = "";
						}						
						$tirah 				= $_POST['tirah'];	
						$kultur 			= $_POST['kultur'];	
						$antibiotik 		= $_POST['antibiotik'];	

						$pengisi 			= $_SESSION['userid'];	
						
						//echo "<pre>";print_r($_POST);exit();                
                        $query ="INSERT INTO db_ppi.tb_isian (ID_ISIAN,USER,STATUS) VALUES ('$id_isian','$pengisi','1')";
						$rs = mysqli_query($conn1,$query);
						if($rs)	{
						
                        $query2 = "INSERT INTO db_ppi.tb_detail_isian (
						ID_ISIAN,
						TGL_DETAIL,
						RUANGAN,
						SURVEYOR,
						DIVISI,
						SUHU,
						NOMR,
						NAMA,
						UMUR,
						JENIS_KELAMIN,
						DX_MEDIS,
						TINDAKAN,
						INFEKSI,						
						TIRAH,
						KULTUR,
						ANTIBIOTIK,
						STATUS) 
						VALUES 
						('$id_isian',
						'$tanggal',
						'$ruangan',
						'$surveyor',
						'$divisi',
						'$suhu',	
						'$nomr',		
						'$nama',				
						'$umur',		
						'$id_jenis_kelamin',		
						'$dxmedis',			
						'$tindakan',
						'$infeksi',
						'$tirah',		
						'$kultur',
						'$antibiotik',					
						'1')";
						//echo $query2; die();	
						$insert=mysqli_query($conn1,$query2);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?modul=list'</script>"; 
						}
						}
						}                       
						else{
                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Ups, Data Gagal Di simpan !</div>';
                        }                         
?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			