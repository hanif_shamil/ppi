<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$OBSERVATOR		= $_POST['OBSERVATOR'];
						if(isset($_POST['ISOLASI_HVAC'])){
							$ISOLASI_HVAC = implode(',',$_POST['ISOLASI_HVAC']);
						}else{
							$ISOLASI_HVAC = "";
						}		
						if(isset($_POST['VAKUM_HEPA'])){
							$VAKUM_HEPA = implode(',',$_POST['VAKUM_HEPA']);
						}else{
							$VAKUM_HEPA = "";
						}	
						if(isset($_POST['UDARA_HEPA'])){
							$UDARA_HEPA = implode(',',$_POST['UDARA_HEPA']);
						}else{
							$UDARA_HEPA = "";
						}	
						if(isset($_POST['WADAH_TUTUP'])){
							$WADAH_TUTUP = implode(',',$_POST['WADAH_TUTUP']);
						}else{
							$WADAH_TUTUP = "";
						}	
						if(isset($_POST['GEROBAK'])){
							$GEROBAK = implode(',',$_POST['GEROBAK']);
						}else{
							$GEROBAK = "";
						}									
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_renov (
							ID_ISI,
							TANGGAL,
							RUANGAN,
							ANALISA,
							TINDAKLANJUT,
							KEPALA,
							OBSERVATOR,
							ISOLASI_HVAC,
							VAKUM_HEPA,
							UDARA_HEPA,
							WADAH_TUTUP,
							GEROBAK,
							TOTAL,
							USER,
							STATUS) 
						VALUES 
						('$ID_ISI',
							'$TANGGAL',
							'$RUANGAN',
							'$ANALISA',  
							'$TINDAKLANJUT', 
							'$KEPALA',
							'$OBSERVATOR',					
							'$ISOLASI_HVAC',
							'$VAKUM_HEPA',	
							'$UDARA_HEPA',		
							'$WADAH_TUTUP',				
							'$GEROBAK',		
							'$TOTAL',
							'$USER',												
							'1')";
						echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_renov'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$RUANGAN 		= $_POST['RUANGAN'];
						$ANALISA 			=	$_POST['ANALISA'];  
						$TINDAKLANJUT 		=	$_POST['TINDAKLANJUT'];  
						$KEPALA 			=	$_POST['KEPALA'];
						$OBSERVATOR 	= $_POST['OBSERVATOR'];
						if(isset($_POST['ISOLASI_HVAC'])){
							$ISOLASI_HVAC = implode(',',$_POST['ISOLASI_HVAC']);
						}else{
							$ISOLASI_HVAC = "";
						}		
						if(isset($_POST['VAKUM_HEPA'])){
							$VAKUM_HEPA = implode(',',$_POST['VAKUM_HEPA']);
						}else{
							$VAKUM_HEPA = "";
						}	
						if(isset($_POST['UDARA_HEPA'])){
							$UDARA_HEPA = implode(',',$_POST['UDARA_HEPA']);
						}else{
							$UDARA_HEPA = "";
						}	
						if(isset($_POST['WADAH_TUTUP'])){
							$WADAH_TUTUP = implode(',',$_POST['WADAH_TUTUP']);
						}else{
							$WADAH_TUTUP = "";
						}	
						if(isset($_POST['GEROBAK'])){
							$GEROBAK = implode(',',$_POST['GEROBAK']);
						}else{
							$GEROBAK = "";
						}									
						$TOTAL	 		= $_POST['TOTAL'];					
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_renov SET												
						TANGGAL='$TANGGAL',
						TGL_UBAH='$waktu',
						RUANGAN='$RUANGAN',
						ANALISA = '$ANALISA', 
						TINDAKLANJUT = '$TINDAKLANJUT', 
						KEPALA = '$KEPALA',
						OBSERVATOR='$OBSERVATOR',
						ISOLASI_HVAC='$ISOLASI_HVAC',
						VAKUM_HEPA='$VAKUM_HEPA',
						UDARA_HEPA='$UDARA_HEPA',
						WADAH_TUTUP='$WADAH_TUTUP',
						GEROBAK='$GEROBAK',
						TOTAL='$TOTAL',
						DIUBAH_OLEH='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_renov'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_renov ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_renov'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			