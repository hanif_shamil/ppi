<script type="text/javascript">
function tugas1()
{
var jumlah=0;
var jumlahx=0;
var nilai;

if(document.getElementById("ISOLASI_HVAC").checked)
{
nilai=document.getElementById("ISOLASI_HVAC").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("ISOLASI_HVACTDK").checked)
{
nilai=document.getElementById("ISOLASI_HVACTDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("VAKUM_HEPA").checked)
{
nilai=document.getElementById("VAKUM_HEPA").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("VAKUM_HEPATDK").checked)
{
nilai=document.getElementById("VAKUM_HEPATDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("UDARA_HEPA").checked)
{
nilai=document.getElementById("UDARA_HEPA").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("UDARA_HEPATDK").checked)
{
nilai=document.getElementById("UDARA_HEPATDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("WADAH_TUTUP").checked)
{
nilai=document.getElementById("WADAH_TUTUP").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("WADAH_TUTUPTDK").checked)
{
nilai=document.getElementById("WADAH_TUTUPTDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("GEROBAK").checked)
{
nilai=document.getElementById("GEROBAK").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("GEROBAKTDK").checked)
{
nilai=document.getElementById("GEROBAKTDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
}
</script>
<?php
$today = date("ymd");
// cari id terakhir yang berawalan tanggal hari ini
$query = "SELECT max(ID_ISI) AS last FROM tb_renov WHERE ID_ISI LIKE '$today%'";
$hasil = mysqli_query($conn1,$query);
$data  = mysqli_fetch_assoc($hasil);
$lastID = $data['last'];
// baca nomor urut transaksi dari id transaksi terakhir
$lastNoUrut = substr($lastID, 8, 4);
// nomor urut ditambah 1
$nextNoUrut = $lastNoUrut + 1;
// membuat format nomor transaksi berikutnya
$nextID = $today.sprintf('%04s', $nextNoUrut);
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Pemantauan Selama Renov/Konstruksi Bangunan</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Pemantauan Selama Renov/Konstruksi Bangunan</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_renov" method="post">
								<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $nextID; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-3">
											<div class="input-group">
												<input class="form-control" value="<?php echo date('Y/m/d H:i:s') ?>" autocomplete="off" id="datetimepicker1" placeholder="[ Tanggal ]" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-4">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" data-placeholder="[ AREA RENOVASI ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option value="<?=$data[0]?>"><?=$data[3]?></option>
												<?php
												}
												?>	
											</select>
										</div>	
										<div class="col-sm-5">
											<input type="text" id="nomr" name="OBSERVATOR" placeholder="[ OBSERVATOR ]" class="form-control" />												
										</div>	
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"> </th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>1</td>
														<td>Mengisolasi sistem HVAC di area kerja untuk mencegah kontaminasi sistem saluran</td>
														<td align="center"><label>
															<input type="checkbox" name="ISOLASI_HVAC[]" id="ISOLASI_HVAC" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
															</label></td>
														<td align="center">
															<label>
															<input name="ISOLASI_HVAC[]" type="checkbox" value="2" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Siapkan pembatas area kerja atau terapkan metode kontrol kubus (menutup area kerja dengan plastik dan menyegel dengan vakum HEPA untuk menyedot debu keluar) sebelum konstruksi dimulai</td>
														<td align="center"><label>
														<input type="checkbox" name="VAKUM_HEPA[]" id="VAKUM_HEPA" value="1" class="ace input-lg" onClick="tugas1()">													
													<span class="lbl"></span>
												</label></td>
														<td align="center"><label>
													<input name="VAKUM_HEPA[]" type="checkbox" value="2" id="VAKUM_HEPATDK" class="ace input-lg" onClick="tugas1()" />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Menjaga tekanan udara negatif dalam tempat kerja dengan menggunakan unit penyaringan udara HEPA</td>
														<td align="center">
															<label>
															<input type="checkbox" name="UDARA_HEPA[]" id="UDARA_HEPA" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="UDARA_HEPA[]" type="checkbox" value="2" id="UDARA_HEPATDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Letakkan limbah konstruksi dalam wadah yang tertutup rapat sebelum dibuang</td>
														<td align="center">
															<label>
															<input name="WADAH_TUTUP[]" type="checkbox" value="1" id="WADAH_TUTUP" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="WADAH_TUTUP[]" type="checkbox" value="2" id="WADAH_TUTUPTDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Tutup wadah atau gerobak tranportasi limbah</td>
														<td align="center">
															<label>
															<input name="GEROBAK[]" type="checkbox" value="1" id="GEROBAK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="GEROBAK[]" type="checkbox" value="2" id="GEROBAKTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
														<div class="input-group">
															<input name="TOTAL" type="text" id="total" style="width:60px"></br>
															<span class="input-group-addon">
																%
															</span>
														</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													

									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->