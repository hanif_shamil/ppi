<?php
$id=($_GET['id']);
$query="select ti.ID_ISI, ti.TGL_INPUT, ti.TANGGAL, ti.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, ti.OBSERVATOR, ti.ISOLASI_HVAC,ti.KET_ISOLASI_HVAC,
ti.VAKUM_HEPA, ti.KET_VAKUM_HEPA, ti.KET_UDARA_HEPA, ti.KET_WADAH_TUTUP, ti.UDARA_HEPA, ti.WADAH_TUTUP, ti.GEROBAK,ti.KET_GEROBAK, ti.TOTAL, ti.`STATUS`,ti.ANALISA, ti.TINDAKLANJUT, ti.KEPALA
from tb_renov ti
left join ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5											
where ti.ID_ISI='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$ISOLASI_HVAC = explode(",", $dt['ISOLASI_HVAC']);$VAKUM_HEPA = explode(",", $dt['VAKUM_HEPA']);$UDARA_HEPA = explode(",", $dt['UDARA_HEPA']);
$WADAH_TUTUP = explode(",", $dt['WADAH_TUTUP']);$GEROBAK = explode(",", $dt['GEROBAK']);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">Formulir Audit Pemantauan Selama Renov/Konstruksi Bangunan KELAS III</li>
				</ul><!-- /.breadcrumb -->	
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Formulir Audit Pemantauan Selama Renov/Konstruksi Bangunan KELAS III</h4>

								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>

									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_renov" method="post">
										<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $dt['ID_ISI']; ?>" readonly>										
										<div class="form-group">										
											<div class="col-sm-6">
												<div class="input-group">
													<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TANGGAL']; ?>" name="TANGGAL" type="text" />
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-6">										
												<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" value="<?php echo $dt['RUANGAN']; ?>" data-placeholder="[ RUANGAN ]">
													<option value=""></option>
													<?php
													$sql = "select * from db_ppi.ruangan r
													where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
													$rs = mysqli_query($conn1,$sql);
													while ($data = mysqli_fetch_array($rs)) {
														?>
														<option <?php if($dt['ID_RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
														<?php
													}
													?>	
												</select>
											</div>																											
											<!-- <div class="col-sm-4">											
												<input type="text" name="OBSERVATOR" value="<?php echo $dt['OBSERVATOR']; ?>" placeholder="[ OBSERVATOR ]" class="form-control" />												
											</div> -->
										</div>									
										<hr />

										<div class="form-group">
											<div class="col-sm-12">											
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="text-align:center">NO</th>
															<th style="text-align:center"></th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
															<th width="5%" style="text-align:center">NA</th>
															<th width="30%" style="text-align:center">KETERANGAN</th>
														</tr>
													</thead>
													<tbody>	
														<tr>
															<td>1</td>
															<td>Mengisolasi sistem HVAC di area kerja untuk mencegah kontaminasi sistem saluran</td>
															<td align="center"><label>
																<input type="radio" name="ISOLASI_HVAC[]" <?php if(in_array("1", $ISOLASI_HVAC)){ echo " checked=\"checked\""; } ?> id="ISOLASI_HVAC" value="1" class="ace input-lg" onClick="tugas1()">																																													
																<span class="lbl"></span>
															</label></td>
															<td align="center">
																<label>
																	<input name="ISOLASI_HVAC[]" type="radio" <?php if(in_array("2", $ISOLASI_HVAC)){ echo " checked=\"checked\""; } ?> value="2" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<label>
																	<input name="ISOLASI_HVAC[]" type="radio" <?php if(in_array("3", $ISOLASI_HVAC)){ echo " checked=\"checked\""; } ?> value="3" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
																	<span class="lbl"></span>
																</label>
															</td>
															<td align="center">
																<input type="text" id="nomr" value="<?php echo $dt['KET_ISOLASI_HVAC']; ?>" name="KET_ISOLASI_HVAC" placeholder="" class="form-control" />			
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>
																Siapkan pembatas area kerja atau terapkan metode kontrol kubus (menutup area kerja dengan plastik dan menyegel dengan vakum HEPA untuk menyedot debu keluar) sebelum konstruksi dimulai
															</td>
															<td align="center"><label>
																<input type="radio" name="VAKUM_HEPA[]" <?php if(in_array("1", $ISOLASI_HVAC)){ echo " checked=\"checked\""; } ?> id="VAKUM_HEPA" value="1" class="ace input-lg" onClick="tugas1()">													
																<span class="lbl"></span>
															</label>
														</td>
														<td align="center"><label>
															<input name="VAKUM_HEPA[]" type="radio" <?php if(in_array("2", $ISOLASI_HVAC)){ echo " checked=\"checked\""; } ?> value="2" id="VAKUM_HEPATDK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="VAKUM_HEPA[]" type="radio" <?php if(in_array("3", $VAKUM_HEPA)){ echo " checked=\"checked\""; } ?> value="3" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<input type="text" id="nomr" value="<?php echo $dt['KET_VAKUM_HEPA']; ?>" name="KET_VAKUM_HEPA" placeholder="" class="form-control" />			
													</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Menjaga tekanan udara negatif dalam tempat kerja dengan menggunakan unit penyaringan udara HEPA</td>
													<td align="center">
														<label>
															<input type="radio" name="UDARA_HEPA[]" <?php if(in_array("1", $UDARA_HEPA)){ echo " checked=\"checked\""; } ?> id="UDARA_HEPA" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="UDARA_HEPA[]" type="radio" <?php if(in_array("2", $UDARA_HEPA)){ echo " checked=\"checked\""; } ?> value="2" id="UDARA_HEPATDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="UDARA_HEPA[]" type="radio" <?php if(in_array("3", $UDARA_HEPA)){ echo " checked=\"checked\""; } ?> value="3" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<input type="text" id="nomr" value="<?php echo $dt['KET_UDARA_HEPA']; ?>" name="KET_UDARA_HEPA" placeholder="" class="form-control" />			
													</td>
												</tr>
												<tr>
													<td>4</td>
													<td>Letakkan limbah konstruksi dalam wadah yang tertutup rapat sebelum dibuang</td>
													<td align="center">
														<label>
															<input name="WADAH_TUTUP[]" type="radio" <?php if(in_array("1", $WADAH_TUTUP)){ echo " checked=\"checked\""; } ?> value="1" id="WADAH_TUTUP" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="WADAH_TUTUP[]" type="radio" <?php if(in_array("2", $WADAH_TUTUP)){ echo " checked=\"checked\""; } ?> value="2" id="WADAH_TUTUPTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="WADAH_TUTUP[]" type="radio" value="3" <?php if(in_array("3", $WADAH_TUTUP)){ echo " checked=\"checked\""; } ?> id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<input type="text" id="nomr" value="<?php echo $dt['KET_WADAH_TUTUP']; ?>" name="KET_WADAH_TUTUP" placeholder="" class="form-control" />			
													</td>
												</tr>
												<tr>
													<td>5</td>
													<td>Tutup wadah atau gerobak tranportasi limbah</td>
													<td align="center">
														<label>
															<input name="GEROBAK[]" type="radio" value="1" <?php if(in_array("1", $GEROBAK)){ echo " checked=\"checked\""; } ?> id="GEROBAK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="GEROBAK[]" type="radio" value="2" <?php if(in_array("2", $GEROBAK)){ echo " checked=\"checked\""; } ?> id="GEROBAKTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<label>
															<input name="GEROBAK[]" type="radio" value="3" <?php if(in_array("3", $GEROBAK)){ echo " checked=\"checked\""; } ?> id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
														</label>
													</td>
													<td align="center">
														<input type="text" id="nomr" value="<?php echo $dt['KET_GEROBAK']; ?>" name="KET_GEROBAK" placeholder="" class="form-control" />			
													</td>
												</tr>											
											</tbody>
										</table>											
									</div>
								</div>													
								<div class="form-group">										
									<div class="col-md-6 col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">ANALISA</label>
										<textarea class="form-control" name="ANALISA" id="form-field-8" placeholder="ANALISA"><?php echo $dt['ANALISA']; ?></textarea>
									</div>				
									<div class="col-md-6 col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">TINDAK LANJUT</label>
										<textarea class="form-control" name="TINDAKLANJUT" id="form-field-8" placeholder="TINDAK LANJUT"><?php echo $dt['TINDAKLANJUT']; ?></textarea>
									</div>
								</div>
								<div class="form-group">			
									<div class="col-md-6 col-sm-12">
										<label class="control-label bolder blue" style="text-decoration: underline">KEPALA UNIT/KEPALA RUANGAN</label>	
										<input type="text" id="nama" value="<?php echo $dt['KEPALA']; ?>" name="KEPALA" placeholder="[ KEPALA UNIT/KEPALA RUANGAN ]" class="form-control" />												
									</div>	
									<div class="col-md-6 col-sm-12">	
										<label class="control-label bolder blue" style="text-decoration: underline">OBSERVATOR</label>				
										<input type="text" id="nama" value="<?php echo $dt['OBSERVATOR']; ?>" name="OBSERVATOR" placeholder="[ OBSERVATOR ]" class="form-control" />
									</div>								
								</div>	

								<hr />
								<div class="form-group">							
									<div class="col-md-offset-3 col-md-9">
										<button class="btn btn-info" name="btnEdit" type="submit">
											<i class="ace-icon fa fa-check bigger-110"></i>
											Submit
										</button>
										&nbsp; &nbsp; &nbsp;
										<button class="btn" type="reset">
											%
											Reset
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div><!-- /.span -->
		</div>
	</div><!-- /.page-content -->	
</div> <!-- container -->
</div><!-- /.main-content -->
<?php
}?>