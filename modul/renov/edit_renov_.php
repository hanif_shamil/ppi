<script type="text/javascript">
function tugas1()
{
var jumlah=0;
var jumlahx=0;
var nilai;

if(document.getElementById("ISOLASI_HVAC").checked)
{
nilai=document.getElementById("ISOLASI_HVAC").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("ISOLASI_HVACTDK").checked)
{
nilai=document.getElementById("ISOLASI_HVACTDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("VAKUM_HEPA").checked)
{
nilai=document.getElementById("VAKUM_HEPA").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("VAKUM_HEPATDK").checked)
{
nilai=document.getElementById("VAKUM_HEPATDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("UDARA_HEPA").checked)
{
nilai=document.getElementById("UDARA_HEPA").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("UDARA_HEPATDK").checked)
{
nilai=document.getElementById("UDARA_HEPATDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("WADAH_TUTUP").checked)
{
nilai=document.getElementById("WADAH_TUTUP").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("WADAH_TUTUPTDK").checked)
{
nilai=document.getElementById("WADAH_TUTUPTDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
if(document.getElementById("GEROBAK").checked)
{
nilai=document.getElementById("GEROBAK").value;
jumlah=jumlah+parseInt(nilai);
}
if(document.getElementById("GEROBAKTDK").checked)
{
nilai=document.getElementById("GEROBAKTDK").value;
jumlahx=jumlahx+parseInt(nilai)/2;
}
document.getElementById("total").value=jumlah/(jumlah+jumlahx)*100;
}
</script>
<?php
$id=($_GET['id']);
$query="select ti.ID_ISI, ti.TGL_INPUT, ti.TANGGAL, ti.RUANGAN ID_RUANGAN, ru.DESKRIPSI RUANGAN, ti.OBSERVATOR, ti.ISOLASI_HVAC, ti.VAKUM_HEPA
, ti.UDARA_HEPA, ti.WADAH_TUTUP, ti.GEROBAK, ti.TOTAL, ti.`STATUS`
from tb_renov ti
left join ruangan ru ON ru.ID=ti.RUANGAN and ru.JENIS=5											
where ti.ID_ISI='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$ISOLASI_HVAC = explode(",", $dt['ISOLASI_HVAC']);$VAKUM_HEPA = explode(",", $dt['VAKUM_HEPA']);$UDARA_HEPA = explode(",", $dt['UDARA_HEPA']);
$WADAH_TUTUP = explode(",", $dt['WADAH_TUTUP']);$GEROBAK = explode(",", $dt['GEROBAK']);
  {
	  ?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">Forms</a>
				</li>
				<li class="active">Formulir Edit Pemantauan Selama Renov/Konstruksi Bangunan</li>
			</ul><!-- /.breadcrumb -->

			
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">Formulir Edit Pemantauan Selama Renov/Konstruksi Bangunan</h4>

							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>

								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>

						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_renov" method="post">
								<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $dt['ID_ISI']; ?>" readonly>										
									<div class="form-group">										
										<div class="col-sm-3">
											<div class="input-group">
												<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TANGGAL']; ?>" name="TANGGAL" type="text" />
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-5">										
											<select class="chosen-select form-control" name="RUANGAN" id="form-field-select-3" value="<?php echo $dt['RUANGAN']; ?>" data-placeholder="[ RUANGAN ]">
												<option value=""></option>
												<?php
												$sql = "select * from db_ppi.ruangan r
												where r.JENIS=5 and r.JENIS_KUNJUNGAN !=0";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
												?>
												<option <?php if($dt['ID_RUANGAN']==$data['ID']){echo "selected";}?> value="<?php echo $data['ID']; ?>"><?php echo $data['DESKRIPSI'] ?></option>
												<?php
												}
												?>	
											</select>
										</div>																											
										<div class="col-sm-4">											
											<input type="text" name="OBSERVATOR" value="<?php echo $dt['OBSERVATOR']; ?>" placeholder="[ OBSERVATOR ]" class="form-control" />												
										</div>
									</div>									
									<hr />
									
									<div class="form-group">
										<div class="col-sm-12">											
											<table class="table table-bordered">
												<thead>
													<tr>
														<th style="text-align:center">NO</th>
														<th style="text-align:center"></th>
														<th width="5%" style="text-align:center">YA</th>
														<th width="5%" style="text-align:center">TIDAK</th>
													</tr>
												</thead>
												<tbody>
													
													<tr>
														<td>1</td>
														<td>Mengisolasi sistem HVAC di area kerja untuk mencegah kontaminasi sistem saluran</td>
														<td align="center"><label>
															<input type="checkbox" name="ISOLASI_HVAC[]" <?php if(in_array("1", $ISOLASI_HVAC)){ echo " checked=\"checked\""; } ?> id="ISOLASI_HVAC" value="1" class="ace input-lg" onClick="tugas1()">																																													
															<span class="lbl"></span>
															</label></td>
														<td align="center">
															<label>
															<input name="ISOLASI_HVAC[]" type="checkbox" <?php if(in_array("2", $ISOLASI_HVAC)){ echo " checked=\"checked\""; } ?> value="2" id="ISOLASI_HVACTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>2</td>
														<td>Siapkan pembatas area kerja atau terapkan metode kontrol kubus (menutup area kerja dengan plastik dan menyegel dengan vakum HEPA untuk menyedot debu keluar) sebelum konstruksi dimulai</td>
														<td align="center"><label>
														<input type="checkbox" name="VAKUM_HEPA[]" <?php if(in_array("1", $ISOLASI_HVAC)){ echo " checked=\"checked\""; } ?> id="VAKUM_HEPA" value="1" class="ace input-lg" onClick="tugas1()">													
													<span class="lbl"></span>
												</label></td>
														<td align="center"><label>
													<input name="VAKUM_HEPA[]" type="checkbox" <?php if(in_array("2", $ISOLASI_HVAC)){ echo " checked=\"checked\""; } ?> value="2" id="VAKUM_HEPATDK" class="ace input-lg" onClick="tugas1()" />
													<span class="lbl"></span>
												</label></td>
													</tr>
													<tr>
														<td>3</td>
														<td>Menjaga tekanan udara negatif dalam tempat kerja dengan menggunakan unit penyaringan udara HEPA</td>
														<td align="center">
															<label>
															<input type="checkbox" name="UDARA_HEPA[]" <?php if(in_array("1", $UDARA_HEPA)){ echo " checked=\"checked\""; } ?> id="UDARA_HEPA" value="1" class="ace input-lg" onClick="tugas1()">
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="UDARA_HEPA[]" type="checkbox" <?php if(in_array("2", $UDARA_HEPA)){ echo " checked=\"checked\""; } ?> value="2" id="UDARA_HEPATDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>4</td>
														<td>Letakkan limbah konstruksi dalam wadah yang tertutup rapat sebelum dibuang</td>
														<td align="center">
															<label>
															<input name="WADAH_TUTUP[]" type="checkbox" <?php if(in_array("1", $WADAH_TUTUP)){ echo " checked=\"checked\""; } ?> value="1" id="WADAH_TUTUP" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="WADAH_TUTUP[]" type="checkbox" <?php if(in_array("2", $WADAH_TUTUP)){ echo " checked=\"checked\""; } ?> value="2" id="WADAH_TUTUPTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td>5</td>
														<td>Tutup wadah atau gerobak tranportasi limbah</td>
														<td align="center">
															<label>
															<input name="GEROBAK[]" type="checkbox" value="1" <?php if(in_array("1", $GEROBAK)){ echo " checked=\"checked\""; } ?> id="GEROBAK" class="ace input-lg" onClick="tugas1()" />
															<span class="lbl"></span>
															</label>
														</td>
														<td align="center">
															<label>
															<input name="GEROBAK[]" type="checkbox" value="2" <?php if(in_array("2", $GEROBAK)){ echo " checked=\"checked\""; } ?> id="GEROBAKTDK" class="ace input-lg" onClick="tugas1()"/>
															<span class="lbl"></span>
															</label>
														</td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Total</td>
														<td style="text-align:center"></td>
														<td style="text-align:center"></td>
													</tr>
													<tr>
														<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
														<td colspan="2" style="text-align:center">
														<div class="input-group">
															<input name="TOTAL" value="<?php echo $dt['TOTAL']; ?>" type="text" id="total" style="width:60px"></br>
															<span class="input-group-addon">
																%
															</span>
														</div>														
														</td>														
													</tr>														
												</tbody>
											</table>											
										</div>
									</div>													
									
									
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnEdit" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div><!-- /.span -->
			</div>
		</div><!-- /.page-content -->	
	</div> <!-- container -->
</div><!-- /.main-content -->
<?php
  }?>