<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
						error_reporting(E_ALL & ~E_NOTICE);
						date_default_timezone_set('Asia/Jakarta');
						if(isset($_POST['btnsimpan'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						$SATU_A 		= $_POST['SATU_A'];
						$DUA_A 			= $_POST['DUA_A'];
						$TIGA_A 		= $_POST['TIGA_A'];
						$EMPAT_A 		= $_POST['EMPAT_A'];
						$LIMA_A 		= $_POST['LIMA_A'];
						$ENAM_A 		= $_POST['ENAM_A'];
						$TUJUH_A 		= $_POST['TUJUH_A'];
						$DELAPAN_A 		= $_POST['DELAPAN_A'];
						$SEMBILAN_A 	= $_POST['SEMBILAN_A'];
						$SATU_B 		= $_POST['SATU_B'];
						$DUA_B 			= $_POST['DUA_B'];
						$TIGA_B 		= $_POST['TIGA_B'];
						$EMPAT_B 		= $_POST['EMPAT_B'];
						$SATU_C 		= $_POST['SATU_C'];
						$DUA_C 			= $_POST['DUA_C'];
						$TIGA_C 		= $_POST['TIGA_C'];
						$EMPAT_C 		= $_POST['EMPAT_C'];
						$LIMA_C 		= $_POST['LIMA_C'];
						$ENAM_C 		= $_POST['ENAM_C'];
						$TUJUH_C 		= $_POST['TUJUH_C'];
						$DELAPAN_C 		= $_POST['DELAPAN_C'];
						$SATU_D 		= $_POST['SATU_D'];
						$DUA_D 			= $_POST['DUA_D'];
						$TIGA_D 		= $_POST['TIGA_D'];
						$EMPAT_D 		= $_POST['EMPAT_D'];
						$LIMA_D 		= $_POST['LIMA_D'];
						$ENAM_D 		= $_POST['ENAM_D'];
						$TUJUH_D 		= $_POST['TUJUH_D'];
						$DELAPAN_D 		= $_POST['DELAPAN_D'];
						$SEMBILAN_D 	= $_POST['SEMBILAN_D'];
						$SEPULUH_D 		= $_POST['SEPULUH_D'];
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];													
						//echo "<pre>";print_r($_POST);exit();                                       						
                        $query = "INSERT INTO db_ppi.tb_apkb (
						ID_ISI,
						TANGGAL,
						AUDITOR,
						SATU_A,	
						DUA_A,
						TIGA_A,
						EMPAT_A,
						LIMA_A,
						ENAM_A,
						TUJUH_A,
						DELAPAN_A,
						SEMBILAN_A,
						SATU_B,
						DUA_B,
						TIGA_B,
						EMPAT_B,
						SATU_C,
						DUA_C,
						TIGA_C,
						EMPAT_C,
						LIMA_C,
						ENAM_C,
						TUJUH_C,
						DELAPAN_C,
						SATU_D,
						DUA_D,
						TIGA_D,
						EMPAT_D,
						LIMA_D,
						ENAM_D,
						TUJUH_D,
						DELAPAN_D,
						SEMBILAN_D,
						SEPULUH_D,
						TOTAL,
						USER,
						STATUS) 
						VALUES 
						('$ID_ISI',
						'$TANGGAL',
						'$AUDITOR',	
						'$SATU_A',				
						'$DUA_A',
						'$TIGA_A',
						'$EMPAT_A',
						'$LIMA_A',
						'$ENAM_A',
						'$TUJUH_A',
						'$DELAPAN_A',
						'$SEMBILAN_A',
						'$SATU_B',
						'$DUA_B',
						'$TIGA_B',
						'$EMPAT_B',
						'$SATU_C',
						'$DUA_C',
						'$TIGA_C',
						'$EMPAT_C',
						'$LIMA_C',
						'$ENAM_C',
						'$TUJUH_C',
						'$DELAPAN_C',
						'$SATU_D',
						'$DUA_D',
						'$TIGA_D',
						'$EMPAT_D',
						'$LIMA_D',
						'$ENAM_D',
						'$TUJUH_D',
						'$DELAPAN_D',
						'$SEMBILAN_D',
						'$SEPULUH_D',
						'$TOTAL',
						'$USER',												
						'1')";
						//echo $query; die();	
						$insert=mysqli_query($conn1,$query);	
								
						if($insert){
							 echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabel_apkb'</script>"; 
						}
						}                      
						if(isset($_POST['btnEdit'])){
						$ID_ISI			= $_POST['ID_ISI'];
						$TANGGAL 		= $_POST['TANGGAL'];
						$AUDITOR 		= addslashes($_POST['AUDITOR']);
						$SATU_A 		= $_POST['SATU_A'];
						$DUA_A 			= $_POST['DUA_A'];
						$TIGA_A 		= $_POST['TIGA_A'];
						$EMPAT_A 		= $_POST['EMPAT_A'];
						$LIMA_A 		= $_POST['LIMA_A'];
						$ENAM_A 		= $_POST['ENAM_A'];
						$TUJUH_A 		= $_POST['TUJUH_A'];
						$DELAPAN_A 		= $_POST['DELAPAN_A'];
						$SEMBILAN_A 	= $_POST['SEMBILAN_A'];
						$SATU_B 		= $_POST['SATU_B'];
						$DUA_B 			= $_POST['DUA_B'];
						$TIGA_B 		= $_POST['TIGA_B'];
						$EMPAT_B 		= $_POST['EMPAT_B'];
						$SATU_C 		= $_POST['SATU_C'];
						$DUA_C 			= $_POST['DUA_C'];
						$TIGA_C 		= $_POST['TIGA_C'];
						$EMPAT_C 		= $_POST['EMPAT_C'];
						$LIMA_C 		= $_POST['LIMA_C'];
						$ENAM_C 		= $_POST['ENAM_C'];
						$TUJUH_C 		= $_POST['TUJUH_C'];
						$DELAPAN_C 		= $_POST['DELAPAN_C'];
						$SATU_D 		= $_POST['SATU_D'];
						$DUA_D 			= $_POST['DUA_D'];
						$TIGA_D 		= $_POST['TIGA_D'];
						$EMPAT_D 		= $_POST['EMPAT_D'];
						$LIMA_D 		= $_POST['LIMA_D'];
						$ENAM_D 		= $_POST['ENAM_D'];
						$TUJUH_D 		= $_POST['TUJUH_D'];
						$DELAPAN_D 		= $_POST['DELAPAN_D'];
						$SEMBILAN_D 	= $_POST['SEMBILAN_D'];
						$SEPULUH_D 		= $_POST['SEPULUH_D'];
						$TOTAL	 		= $_POST['TOTAL'];							
						$USER 			= $_SESSION['userid'];	
						$waktu = date("Y-m-d H:i:s");		
						//echo "<pre>";print_r($_POST);exit();   						
                        $query2 = "UPDATE db_ppi.tb_apkb SET												
						TANGGAL		= '$TANGGAL',
						TGL_UBAH	= '$waktu',
						AUDITOR		= '$AUDITOR',	
						SATU_A 		= '$SATU_A',
						DUA_A 		= '$DUA_A',
						TIGA_A 		= '$TIGA_A',
						EMPAT_A 	= '$EMPAT_A',
						LIMA_A 		= '$LIMA_A',
						ENAM_A 		= '$ENAM_A',
						TUJUH_A 	= '$TUJUH_A',
						DELAPAN_A	= '$DELAPAN_A',
						SEMBILAN_A 	= '$SEMBILAN_A',
						SATU_B 		= '$SATU_B',
						DUA_B 		= '$DUA_B',
						TIGA_B 		= '$TIGA_B',
						EMPAT_B 	= '$EMPAT_B',
						SATU_C 		= '$SATU_C',
						DUA_C 		= '$DUA_C',
						TIGA_C 		= '$TIGA_C',
						EMPAT_C 	= '$EMPAT_C',
						LIMA_C 		= '$LIMA_C',
						ENAM_C 		= '$ENAM_C',
						TUJUH_C 	= '$TUJUH_C',
						TUJUH_D 	= '$TUJUH_D',
						SATU_D 		= '$SATU_D',
						DUA_D 		= '$DUA_D',
						TIGA_D 		= '$TIGA_D',
						EMPAT_D 	= '$EMPAT_D',
						LIMA_D 		= '$LIMA_D',
						ENAM_D 		= '$ENAM_D',
						TUJUH_D 	= '$TUJUH_D',
						DELAPAN_D 	= '$DELAPAN_D',
						SEMBILAN_D 	= '$SEMBILAN_D',
						SEPULUH_D 	= '$SEPULUH_D',	
						TOTAL		='$TOTAL',
						DIUBAH_OLEH	='$USER'
						where ID_ISI='$ID_ISI'";
						//echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							 echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabel_apkb'</script>"; 
						}
						}
						$id = $_GET['id'];
						if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_apkb ti SET ti.`STATUS`=0 WHERE ti.`STATUS` = 1 AND ti.ID_ISI = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							 echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabel_apkb'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=list'</script>"; 
						}	
						}						
						?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			