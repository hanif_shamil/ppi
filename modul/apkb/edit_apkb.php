<script type="text/javascript">

	function tugas1()
	{	
		var ya = $('#aa:checked').length;
		var tidak = $('#bb:checked').length;
		document.getElementById("total").value= ya/(ya+tidak)*100;	
	}
</script>
<?php
$id=($_GET['id']);
$query="select ti.ID_ISI,ti.TGL_INPUT, ti.TGL_UBAH, ti.TANGGAL, ti.AUDITOR
,ti.SATU_A, ti.DUA_A, ti.TIGA_A, ti.EMPAT_A, ti.LIMA_A, ti.ENAM_A, ti.TUJUH_A,ti.DELAPAN_A, ti.DUA_A, ti.SEMBILAN_A, ti.SATU_B, ti.DUA_B, ti.TIGA_B
,ti.EMPAT_B, ti.SATU_C, ti.DUA_C, ti.TIGA_C, ti.EMPAT_C, ti.LIMA_C, ti.ENAM_C, ti.TUJUH_C, ti.DELAPAN_C, ti.SATU_D, ti.DUA_D, ti.TIGA_D
,ti.EMPAT_D, ti.LIMA_D, ti.ENAM_D, ti.TUJUH_D, ti.DELAPAN_D, ti.SEMBILAN_D, ti.SEPULUH_D, ti.TOTAL
from db_ppi.tb_apkb ti				
where ti.ID_ISI='$id'";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">Forms</a>
					</li>
					<li class="active">Formulir Monitoring/Audit PPI di Kamar Bedah</li>
				</ul><!-- /.breadcrumb -->


			</div>

			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">Formulir Monitoring/Audit PPI di Kamar Bedah</h4>

								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>

									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>

							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpan_apkb" method="post">
										<input type="hidden" name="ID_ISI" class="form-control" value="<?php echo $dt['ID_ISI']; ?>" readonly>										
										<div class="form-group">										
											<div class="col-sm-3">
												<div class="input-group">
													<input class="form-control" id="datetimepicker1" value="<?php echo $dt['TANGGAL']; ?>" name="TANGGAL" type="text" />
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>										
											<div class="col-sm-5">											
												<input type="text" name="AUDITOR" value="<?php echo $dt['AUDITOR']; ?>" placeholder="[ AUDITOR ]" class="form-control" />												
											</div>
										</div>									
										<hr />

										<div class="form-group">
											<div class="col-sm-12">											
												<table class="table table-bordered">
													<thead>
														<tr>
															<th style="text-align:center">NO</th>
															<th style="text-align:center"> ELEMEN PENILAIAN</th>
															<th width="5%" style="text-align:center">YA</th>
															<th width="5%" style="text-align:center">TIDAK</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td></td>
															<td colspan="3" style="font-weight: bold;">Personal</td>
														</tr>
														<tr>
															<td>1</td>
															<td>Personal hygiene baik</td>
															<td align="center"><label>
																<input type="checkbox" name="SATU_A" id="aa" <?php if($dt['SATU_A']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">																	
																<span class="lbl"></span>
															</label></td>
															
															<td align="center">
																<label>
																	<input name="SATU_A" type="checkbox" <?php if($dt['SATU_A']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Menggunakan pakaian khusus OK</td>
															<td align="center"><label>
																<input type="checkbox" name="DUA_A" id="aa" <?php if($dt['DUA_A']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">													
																<span class="lbl"></span>
															</label></td>
															
															<td align="center"><label>
																<input type="checkbox" name="DUA_A" id="bb" <?php if($dt['DUA_A']=='2') echo " checked "?> value="2" class="ace input-lg" onClick="tugas1()">													
																<span class="lbl"></span>
															</label></td>
														</tr>
														<tr>
															<td>3</td>
															<td>Menggunakan penutup kepala dan masker</td>
															<td align="center">
																<label>
																	<input type="checkbox" name="TIGA_A" id="aa" <?php if($dt['TIGA_A']=='1') echo " checked "?> value="1" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input type="checkbox" name="TIGA_A" id="bb" <?php if($dt['TIGA_A']=='2') echo " checked "?> value="2" class="ace input-lg" onClick="tugas1()">
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Tidak menggunakan perhiasan tangan</td>
															<td align="center">
																<label>
																	<input name="EMPAT_A" type="checkbox" value="1" <?php if($dt['EMPAT_A']=='1') echo " checked "?> id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="EMPAT_A" type="checkbox" value="2" <?php if($dt['EMPAT_A']=='2') echo " checked "?> id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>5</td>
															<td>Kuku pendek dan bersih</td>
															<td align="center">
																<label>
																	<input name="LIMA_A" type="checkbox" value="1" id="aa" <?php if($dt['LIMA_A']=='1') echo " checked "?> class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="LIMA_A" type="checkbox" value="2" <?php if($dt['LIMA_A']=='2') echo " checked "?> id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Menggunakan sepatu  tertutup penuh dan dapat dicuci yang digunakan hanya di OK</td>
															<td align="center">
																<label>
																	<input name="ENAM_A" type="checkbox" value="1" <?php if($dt['ENAM_A']=='1') echo " checked "?> id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="ENAM_A" type="checkbox" value="2" <?php if($dt['ENAM_A']=='2') echo " checked "?> id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>7</td>
															<td>Staff yang mengalami luka terbuka harus ditutup dengan dressing waterproof</td>
															<td align="center">
																<label>
																	<input name="TUJUH_A" type="checkbox" value="1" <?php if($dt['TUJUH_A']=='1') echo " checked "?> id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="TUJUH_A" type="checkbox" value="2" <?php if($dt['TUJUH_A']=='2') echo " checked "?> id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>8</td>
															<td>Menggunakan APD lengkap saat tindakan (penutup kepala, sarung tangan, masker, goggles, gown, sepatu)</td>
															<td align="center">
																<label>
																	<input name="DELAPAN_A" type="checkbox" value="1" <?php if($dt['DELAPAN_A']=='1') echo " checked "?> id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="DELAPAN_A" type="checkbox" value="2" <?php if($dt['DELAPAN_A']=='2') echo " checked "?> id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>9</td>
															<td>Cuci tangan bedah dengan benar</td>
															<td align="center">
																<label>
																	<input name="SEMBILAN_A" type="checkbox" <?php if($dt['SEMBILAN_A']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="SEMBILAN_A" type="checkbox" <?php if($dt['SEMBILAN_A']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td></td>
															<td style="font-weight: bold;" colspan="4">Kondisi Ruangan</td>
														</tr>
														<tr>
															<td>1</td>
															<td>Pintu selalu tertutup</td>
															<td align="center">
																<label>
																	<input name="SATU_B" type="checkbox" <?php if($dt['SATU_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="SATU_B" type="checkbox" <?php if($dt['SATU_B']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Pertahankan tekanan selalu positif, suhu dan kelembaban berada dalam rentang yang dipersyaratkan</td>
															<td align="center">
																<label>
																	<input name="DUA_B" type="checkbox" <?php if($dt['DUA_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="DUA_B" type="checkbox" <?php if($dt['DUA_B']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Pembatasan jumlah orang di dalam kamar bedah</td>
															<td align="center">
																<label>
																	<input name="TIGA_B" type="checkbox" <?php if($dt['TIGA_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="TIGA_B" type="checkbox" <?php if($dt['TIGA_B']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Pembatasan akses masuk kamar bedah</td>
															<td align="center">
																<label>
																	<input name="EMPAT_B" type="checkbox" <?php if($dt['EMPAT_B']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="EMPAT_B" type="checkbox" <?php if($dt['EMPAT_B']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td></td>
															<td style="font-weight: bold;" colspan="4">Pembuangan Limbah</td>
														</tr>
														<tr>
															<td>1</td>
															<td>Penghasil limbah benda tajam harus langsung membuangnya</td>
															<td align="center">
																<label>
																	<input name="SATU_C" type="checkbox" <?php if($dt['SATU_C']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="SATU_C" type="checkbox" <?php if($dt['SATU_C']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Limbah benda tajam dibuang dalam sharp box yang tahan air dan tahan tembus.</td>
															<td align="center">
																<label>
																	<input name="DUA_C" type="checkbox" <?php if($dt['DUA_C']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="DUA_C" type="checkbox" <?php if($dt['DUA_C']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Sharp box dibuang dalam kondisi terkunci (3/4 penuh/sampai batas yang ada dalam box)</td>
															<td align="center">
																<label>
																	<input name="TIGA_C" type="checkbox" <?php if($dt['TIGA_C']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="TIGA_C" type="checkbox" <?php if($dt['TIGA_C']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Limbah organ dibuang dalam kantong kuning</td>
															<td align="center">
																<label>
																	<input name="EMPAT_C" type="checkbox" <?php if($dt['EMPAT_C']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="EMPAT_C" type="checkbox" <?php if($dt['EMPAT_C']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>5</td>
															<td>Limbah infeksius dibuang dalam kantung kuning</td>
															<td align="center">
																<label>
																	<input name="LIMA_C" type="checkbox" <?php if($dt['LIMA_C']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="LIMA_C" type="checkbox" <?php if($dt['LIMA_C']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Limbah rumah tangga dibuang dalam kantung hitam</td>
															<td align="center">
																<label> 
																	<input name="ENAM_C" type="checkbox" <?php if($dt['ENAM_C']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="ENAM_C" type="checkbox" <?php if($dt['ENAM_C']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>7</td>
															<td>Limbah sitotoksik dibuang dalam kantung ungu</td>
															<td align="center">
																<label>
																	<input name="TUJUH_C" type="checkbox" <?php if($dt['TUJUH_C']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="TUJUH_C" type="checkbox" <?php if($dt['TUJUH_C']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>													
														<tr>
															<td>8</td>
															<td>Tumpahan darah segera dibersihkan dengan detergen dan disinfektan</td>
															<td align="center">
																<label>
																	<input name="DELAPAN_C" type="checkbox" <?php if($dt['DELAPAN_C']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="DELAPAN_C" type="checkbox" <?php if($dt['DELAPAN_C']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td></td>
															<td style="font-weight: bold;" colspan="4">Pembersihan Lingkungan</td>
														</tr>
														<tr>
															<td>1</td>
															<td>Pembersihan antarpasien dilakukan dengan menggunakan larutan disinfectant untuk membersihkan lantai dan alat-alat lain</td>
															<td align="center">
																<label>
																	<input name="SATU_D" type="checkbox" <?php if($dt['SATU_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="SATU_D" type="checkbox" <?php if($dt['SATU_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>2</td>
															<td>Terdapat jadwal pembersihan harian dan mingguan</td>
															<td align="center">
																<label>
																	<input name="DUA_D" type="checkbox" <?php if($dt['DUA_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="DUA_D" type="checkbox" <?php if($dt['DUA_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>3</td>
															<td>Pembersihan dilakukan terhadap semua alat yang ada di dalam kamar bedah</td>
															<td align="center">
																<label>
																	<input name="TIGA_D" type="checkbox" <?php if($dt['TIGA_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="TIGA_D" type="checkbox" <?php if($dt['TIGA_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>4</td>
															<td>Hanya barang yang digunakan saja yang boleh berada di dalam kamar bedah</td>
															<td align="center">
																<label>
																	<input name="EMPAT_D" type="checkbox" <?php if($dt['EMPAT_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="EMPAT_D" type="checkbox" <?php if($dt['EMPAT_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>5</td>
															<td>Secara visual bebas dari debu</td>
															<td align="center">
																<label>
																	<input name="LIMA_D" type="checkbox" <?php if($dt['LIMA_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="LIMA_D" type="checkbox" <?php if($dt['LIMA_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>6</td>
															<td>Alat-alat dan alat habis pakai disimpan tidak boleh sejajar dengan lantai</td>
															<td align="center">
																<label>
																	<input name="ENAM_D" type="checkbox" <?php if($dt['LIMA_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="ENAM_D" type="checkbox" <?php if($dt['LIMA_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>7</td>
															<td>Grill ventilasi tidak tersumbat dan tidak berdebu</td>
															<td align="center">
																<label>
																	<input name="TUJUH_D" type="checkbox" <?php if($dt['TUJUH_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="TUJUH_D" type="checkbox" <?php if($dt['TUJUH_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>8</td>
															<td>Penyimpanan alat habis pakai tidak boleh terlalu banyak dan harus dengan system FIFO</td>
															<td align="center">
																<label>
																	<input name="DELAPAN_D" type="checkbox" <?php if($dt['DELAPAN_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="DELAPAN_D" type="checkbox" <?php if($dt['DELAPAN_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>9</td>
															<td>Lantai,dinding,plafo dan cat di kamar bedah harus selalu dalam kondisi baik</td>
															<td align="center">
																<label>
																	<input name="SEMBILAN_D" type="checkbox" <?php if($dt['SEMBILAN_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="SEMBILAN_D" type="checkbox" <?php if($dt['SEMBILAN_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td>10</td>
															<td>Pemeriksaan kualitas udara dilakukan jika ada perbaikan major. Pemeriksaan dilakukan dengan meletakkan agar plate dalam ruangan kondisi tertutup.</td>
															<td align="center">
																<label>
																	<input name="SEPULUH_D" type="checkbox" <?php if($dt['SEPULUH_D']=='1') echo " checked "?> value="1" id="aa" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
															
															<td align="center">
																<label>
																	<input name="SEPULUH_D" type="checkbox" <?php if($dt['SEPULUH_D']=='2') echo " checked "?> value="2" id="bb" class="ace input-lg" onClick="tugas1()" />
																	<span class="lbl"></span>
																</label>
															</td>
														</tr>
														<tr>
															<td style="text-align:center" colspan="2">Total</td>
															<td style="text-align:center"></td>
															<td style="text-align:center"></td>
													
														</tr>
														<tr>
															<td style="text-align:center" colspan="2">Rumus = YA/YA+TIDAK X 100% = ............%</td>
															<td colspan="3" style="text-align:center">
																<div class="input-group">
																	<input name="TOTAL" type="text" id="total" value="<?php echo $dt['TOTAL']; ?>" style="width:100px"></br>
																	<span class="input-group-addon">
																		%
																	</span>
																</div>														
															</td>														
														</tr>														
													</tbody>
												</table>											
											</div>
										</div>													


										<hr />
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													%
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div><!-- /.span -->
				</div>
			</div><!-- /.page-content -->	
		</div> <!-- container -->
	</div><!-- /.main-content -->
	<?php
}?>