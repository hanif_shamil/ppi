<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Proses</h3>
		<div class="row mt">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="content-panel">
					<center>
						<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i>
						<span class="sr-only">Loading...</span>
					</center>
					<?php
					error_reporting(E_ALL & ~E_NOTICE);
					date_default_timezone_set('Asia/Jakarta');
					if(isset($_POST['btnsimpan'])){
						$nomr  					= $_POST['nomr']; 
						$nama  					= addslashes($_POST['nama']); 
						$nopen  				= $_POST['nopen']; 
						$tglmasukruangan  		= $_POST['tglmasukruangan']; 
						$asalruangan  			= $_POST['asalruangan']; 
						$dxmedis  				= $_POST['dxmedis']; 
						$tglsurveilan  			= $_POST['tglsurveilan']; 
						$tb  					= $_POST['tb']; 
						$bb  					= $_POST['bb']; 
						$tindakan  				= $_POST['tindakan']; 
						$tgl_rencana  			= $_POST['tgl_rencana']; 
						if(isset($_POST['dokteroperator'])){
							$dokteroperator = implode(',',$_POST['dokteroperator']);
						}else{
							$dokteroperator = "";
						}
						$suhu  					= $_POST['suhu']; 
						$lab_tanggal  			= $_POST['lab_tanggal']; 
						$glukosa  				= $_POST['glukosa']; 
						$hb  					= $_POST['hb']; 
						$leukosit  				= $_POST['leukosit']; 
						$albumin  				= $_POST['albumin']; 
						$tgl_mandi1  			= $_POST['tgl_mandi1']; 
						$tgl_mandi2  			= $_POST['tgl_mandi2']; 
						$mandi_menggunakan  	= $_POST['mandi_menggunakan']; 
						$mandi_menggunakan_lain = $_POST['mandi_menggunakan_lain'];
						$alat_cukur  			= $_POST['alat_cukur']; 
						$lokasi_mandi  			= $_POST['lokasi_mandi'];  
						$tgl_cukur  			= $_POST['tgl_cukur']; 
						$asascore  				= $_POST['asascore']; 
						$petugas_asa_score  	= $_POST['petugas_asa_score']; 
						$klasifikasi_lukaop  	= $_POST['klasifikasi_lukaop']; 
						$urgensi_op  			= $_POST['urgensi_op']; 
						$ruang_operasi  		= $_POST['ruang_operasi']; 
						if(isset($_POST['nama_operator'])){
							$nama_operator = implode(',',$_POST['nama_operator']);
						}else{
							$nama_operator = "";
						}
						$nama_asisten  			= $_POST['nama_asisten'];
						$asisten_dokter			= $_POST['asisten_dokter'];
						$perawat_instrumen  	= $_POST['perawat_instrumen'];
						$perawat_sirkuler  		= $_POST['perawat_sirkuler'];
						$dokter_anastesi  		= $_POST['dokter_anastesi'];
						$perawat_anastesi  		= $_POST['perawat_anastesi'];
						$ab_dibutuhkan  		= $_POST['ab_dibutuhkan']; 
						$ab_alasanlain  		= $_POST['ab_alasanlain']; 

						$ab_profilaksis  				= $_POST['ab_profilaksis'];
						$nama_ab_prof 					= $_POST['nama_ab_prof'];
						$dosis_ab_prof 					= $_POST['dosis_ab_prof'];
						$waktu_ab_prof 					= $_POST['waktu_ab_prof'];
						$ulang_ab_prof 					= $_POST['ulang_ab_prof'];
						$waktu_pemberian_ulang_ab_prof	= $_POST['waktu_pemberian_ulang_ab_prof'];
						$antiseptik  			= $_POST['antiseptik'];
						$teknik_preparasi  		= $_POST['teknik_preparasi'];
						if(isset($_POST['nama_hh_operator'])){
							$nama_hh_operator = implode(',',$_POST['nama_hh_operator']);
						}else{
							$nama_hh_operator = "";
						}
						$nama_hh_sirkuler 		= $_POST['nama_hh_sirkuler'];
						$nama_hh_perawat  		= $_POST['nama_hh_perawat'];
						$nama_hh_asisten  		= $_POST['nama_hh_asisten'];
						$hh_perawat  			= $_POST['hh_perawat'];
						$hh_sirkuler  			= $_POST['hh_sirkuler'];
						$hh_operator 			= $_POST['hh_operator'];
						$hh_asisten 			= $_POST['hh_asisten'];

						$jml_petugas_op  		= $_POST['jml_petugas_op']; 
						$jml_selain_petugas_op  = $_POST['jml_selain_petugas_op']; 
						$jml_pintu_dibuka  		= $_POST['jml_pintu_dibuka']; 
						$terpasang_drain  		= $_POST['terpasang_drain'];
						$lokasi_drain  			= $_POST['lokasi_drain'];
						$tipe_drain  			= $_POST['tipe_drain']; 

						$penggunaan_alat_reuse  = $_POST['penggunaan_alat_reuse']; 
						$nama_alat_reuse1  		= $_POST['nama_alat_reuse1']; 
						$nama_alat_reuse2  		= $_POST['nama_alat_reuse2']; 
						$nama_alat_reuse3  		= $_POST['nama_alat_reuse3']; 
						$alat_reuse1 			= $_POST['alat_reuse1']; 
						$alat_reuse2  			= $_POST['alat_reuse2']; 
						$alat_reuse3  			= $_POST['alat_reuse3']; 
						$penggunaan_implant  	= $_POST['penggunaan_implant']; 
						$jns_implant  			= $_POST['jns_implant']; 
						$jns_implant_lain  		= $_POST['jns_implant_lain']; 
						$ab_post_op  			= $_POST['ab_post_op']; 
						$nama_ab_op  			= $_POST['nama_ab_op'];
						$dosis_ab_op  			= $_POST['dosis_ab_op'];
						$selama_ab_op  			= $_POST['selama_ab_op'];
						$alasan_pos_op  		= $_POST['alasan_pos_op'];
						$alasan_lain_post_op  	= $_POST['alasan_lain_post_op']; 

						$hasil_lab_leukosit  	= $_POST['hasil_lab_leukosit']; 
						$hasil_lab_crcp  		= $_POST['hasil_lab_crcp']; 
						$hasil_lab_pct  		= $_POST['hasil_lab_pct'];
						$hasil_lab_gds  		= $_POST['hasil_lab_gds']; 
						$isi_lab_leukosit  		= $_POST['isi_lab_leukosit']; 
						$isi_lab_crcp  			= $_POST['isi_lab_crcp']; 
						$isi_lab_pct  			= $_POST['isi_lab_pct'];
						$isi_lab_gds  			= $_POST['isi_lab_gds']; 
						$tgl_hasil_lab_op  		= $_POST['tgl_hasil_lab_op'];

						$tgl_isi_form  			= $_POST['tgl_isi_form']; 
						$tgl_selesai_isi  		= $_POST['tgl_selesai_isi']; 
						$nama_petugas  			= $_POST['nama_petugas']; 						
						$user 					= $_SESSION['userid'];													
						// echo "<pre>";print_r($_POST);exit();                                       						
						$query = "INSERT INTO db_ppi.tb_survido (
							nomr,
							tglsurveilan,  
							tb,  
							bb,  
							tindakan,  
							tgl_rencana, 
							dokteroperator, 
							suhu,  
							lab_tanggal,  
							glukosa,  
							hb,  
							leukosit,  
							albumin,  
							tgl_mandi1,  
							tgl_mandi2,  
							mandi_menggunakan,  
							mandi_menggunakan_lain,  
							alat_cukur,  
							lokasi_mandi,  
							tgl_cukur,  
							asascore,  
							petugas_asa_score,  
							klasifikasi_lukaop,  
							urgensi_op,   
							ruang_operasi,  
							nama_operator,  
							nama_asisten,
							asisten_dokter, 
							perawat_instrumen,  
							perawat_sirkuler, 
							dokter_anastesi,
							perawat_anastesi,  
							ab_dibutuhkan,
							ab_alasanlain,  

							ab_profilaksis,  				
							nama_ab_prof, 					
							dosis_ab_prof, 					
							waktu_ab_prof, 					
							ulang_ab_prof, 					
							waktu_pemberian_ulang_ab_prof,

							antiseptik,  
							teknik_preparasi, 
							nama_hh_operator,  
							nama_hh_perawat, 
							nama_hh_sirkuler,
							nama_hh_asisten, 
							hh_perawat,  
							hh_sirkuler, 
							hh_operator,
							hh_asisten, 

							jml_petugas_op,  
							jml_selain_petugas_op, 
							jml_pintu_dibuka,  
							terpasang_drain,  
							lokasi_drain,  
							tipe_drain,  

							penggunaan_alat_reuse,
							nama_alat_reuse1,  
							nama_alat_reuse2,  
							nama_alat_reuse3,  
							alat_reuse1, 
							alat_reuse2,  
							alat_reuse3,  
							penggunaan_implant,  

							jns_implant,  
							jns_implant_lain,  
							ab_post_op, 
							nama_ab_op, 
							dosis_ab_op, 
							selama_ab_op,  
							alasan_pos_op,  
							alasan_lain_post_op,  

							hasil_lab_leukosit,  
							hasil_lab_crcp,  
							hasil_lab_pct,  
							hasil_lab_gds,  
							isi_lab_leukosit,  
							isi_lab_crcp,  
							isi_lab_pct,  
							isi_lab_gds,  
							tgl_hasil_lab_op,  

							tgl_isi_form, 
							tgl_selesai_isi,  
							nama_petugas, 
							user) 
						VALUES 
						(
							'$nomr',
							'$tglsurveilan',
							'$tb',
							'$bb',
							'$tindakan',
							'$tgl_rencana',
							'$dokteroperator',
							'$suhu',
							'$lab_tanggal',
							'$glukosa',
							'$hb',
							'$leukosit',
							'$albumin',
							'$tgl_mandi1',
							'$tgl_mandi2',
							'$mandi_menggunakan',
							'$mandi_menggunakan_lain',
							'$alat_cukur',
							'$lokasi_mandi',
							'$tgl_cukur',
							'$asascore',
							'$petugas_asa_score',
							'$klasifikasi_lukaop',
							'$urgensi_op',
							'$ruang_operasi',
							'$nama_operator',
							'$nama_asisten',
							'$asisten_dokter',
							'$perawat_instrumen',
							'$perawat_sirkuler',
							'$dokter_anastesi',
							'$perawat_anastesi',
							'$ab_dibutuhkan',
							'$ab_alasanlain',

							'$ab_profilaksis',  				
							'$nama_ab_prof', 					
							'$dosis_ab_prof', 					
							'$waktu_ab_prof', 					
							'$ulang_ab_prof', 					
							'$waktu_pemberian_ulang_ab_prof',

							'$antiseptik',
							'$teknik_preparasi',
							'$nama_hh_operator',
							'$nama_hh_perawat',
							'$nama_hh_sirkuler',
							'$nama_hh_asisten',
							'$hh_perawat',
							'$hh_sirkuler',
							'$hh_operator',
							'$hh_asisten',

							'$jml_petugas_op',
							'$jml_selain_petugas_op',
							'$jml_pintu_dibuka',
							'$terpasang_drain',
							'$lokasi_drain',
							'$tipe_drain',

							'$penggunaan_alat_reuse',
							'$nama_alat_reuse1',
							'$nama_alat_reuse2',
							'$nama_alat_reuse3',
							'$alat_reuse1', 
							'$alat_reuse2',
							'$alat_reuse3',
							'$penggunaan_implant',

							'$jns_implant',
							'$jns_implant_lain',
							'$ab_post_op',
							'$nama_ab_op', 
							'$dosis_ab_op', 
							'$selama_ab_op', 
							'$alasan_pos_op',
							'$alasan_lain_post_op',

							'$hasil_lab_leukosit',
							'$hasil_lab_crcp',
							'$hasil_lab_pct',
							'$hasil_lab_gds',
							'$isi_lab_leukosit',
							'$isi_lab_crcp',
							'$isi_lab_pct',
							'$isi_lab_gds',
							'$tgl_hasil_lab_op',

							'$tgl_isi_form',
							'$tgl_selesai_isi',
							'$nama_petugas',

							'$user')";
						// echo $query; die();	
						$insert=mysqli_query($conn1,$query);	

						if($insert){
							echo "<script>alert('Laporan Berhasil dimasukan!'); window.location = 'index.php?page=tabelsurvido'</script>"; 
						}
					}                      
					if(isset($_POST['btnEdit'])){
						$id  					= $_POST['ID']; 
						$nomr  					= $_POST['nomr']; 
						$nama  					= addslashes($_POST['nama']); 
						$nopen  				= $_POST['nopen']; 
						$tglmasukruangan  		= $_POST['tglmasukruangan']; 
						$asalruangan  			= $_POST['asalruangan']; 
						$dxmedis  				= $_POST['dxmedis']; 
						$tglsurveilan  			= $_POST['tglsurveilan']; 
						$tb  					= $_POST['tb']; 
						$bb  					= $_POST['bb']; 
						$tindakan  				= $_POST['tindakan']; 
						$tgl_rencana  			= $_POST['tgl_rencana']; 
						if(isset($_POST['dokteroperator'])){
							$dokteroperator = implode(',',$_POST['dokteroperator']);
						}else{
							$dokteroperator = "";
						}
						$suhu  					= $_POST['suhu']; 
						$lab_tanggal  			= $_POST['lab_tanggal']; 
						$glukosa  				= $_POST['glukosa']; 
						$hb  					= $_POST['hb']; 
						$leukosit  				= $_POST['leukosit']; 
						$albumin  				= $_POST['albumin']; 
						$tgl_mandi1  			= $_POST['tgl_mandi1']; 
						$tgl_mandi2  			= $_POST['tgl_mandi2']; 
						$mandi_menggunakan  	= $_POST['mandi_menggunakan']; 
						$mandi_menggunakan_lain = $_POST['mandi_menggunakan_lain'];
						$alat_cukur  			= $_POST['alat_cukur']; 
						$lokasi_mandi  			= $_POST['lokasi_mandi'];  
						$tgl_cukur  			= $_POST['tgl_cukur']; 
						$asascore  				= $_POST['asascore']; 
						$petugas_asa_score  	= $_POST['petugas_asa_score']; 
						$klasifikasi_lukaop  	= $_POST['klasifikasi_lukaop']; 
						$urgensi_op  			= $_POST['urgensi_op']; 
						$ruang_operasi  		= $_POST['ruang_operasi']; 
						if(isset($_POST['nama_operator'])){
							$nama_operator = implode(',',$_POST['nama_operator']);
						}else{
							$nama_operator = "";
						}
						$nama_asisten  			= $_POST['nama_asisten'];
						$asisten_dokter  		= $_POST['asisten_dokter'];
						$perawat_instrumen  	= $_POST['perawat_instrumen'];
						$perawat_sirkuler  		= $_POST['perawat_sirkuler'];
						$dokter_anastesi  		= $_POST['dokter_anastesi'];
						$perawat_anastesi  		= $_POST['perawat_anastesi'];
						$ab_dibutuhkan  		= $_POST['ab_dibutuhkan']; 
						$ab_alasanlain  		= $_POST['ab_alasanlain']; 

						$ab_profilaksis  				= $_POST['ab_profilaksis'];
						$nama_ab_prof 					= $_POST['nama_ab_prof'];
						$dosis_ab_prof 					= $_POST['dosis_ab_prof'];
						$waktu_ab_prof 					= $_POST['waktu_ab_prof'];
						$ulang_ab_prof 					= $_POST['ulang_ab_prof'];
						$waktu_pemberian_ulang_ab_prof	= $_POST['waktu_pemberian_ulang_ab_prof'];
						$antiseptik  			= $_POST['antiseptik'];
						$teknik_preparasi  		= $_POST['teknik_preparasi'];
						if(isset($_POST['nama_hh_operator'])){
							$nama_hh_operator = implode(',',$_POST['nama_hh_operator']);
						}else{
							$nama_hh_operator = "";
						}
						$nama_hh_sirkuler 		= $_POST['nama_hh_sirkuler'];
						$nama_hh_perawat  		= $_POST['nama_hh_perawat'];
						$nama_hh_asisten  		= $_POST['nama_hh_asisten'];
						$hh_perawat  			= $_POST['hh_perawat'];
						$hh_sirkuler  			= $_POST['hh_sirkuler'];
						$hh_operator 			= $_POST['hh_operator'];
						$hh_asisten 			= $_POST['hh_asisten'];

						$jml_petugas_op  		= $_POST['jml_petugas_op']; 
						$jml_selain_petugas_op  = $_POST['jml_selain_petugas_op']; 
						$jml_pintu_dibuka  		= $_POST['jml_pintu_dibuka']; 
						$terpasang_drain  		= $_POST['terpasang_drain'];
						$lokasi_drain  			= $_POST['lokasi_drain'];
						$tipe_drain  			= $_POST['tipe_drain']; 

						$penggunaan_alat_reuse  = $_POST['penggunaan_alat_reuse']; 
						$nama_alat_reuse1  		= $_POST['nama_alat_reuse1']; 
						$nama_alat_reuse2  		= $_POST['nama_alat_reuse2']; 
						$nama_alat_reuse3  		= $_POST['nama_alat_reuse3']; 
						$alat_reuse1 			= $_POST['alat_reuse1']; 
						$alat_reuse2  			= $_POST['alat_reuse2']; 
						$alat_reuse3  			= $_POST['alat_reuse3']; 
						$penggunaan_implant  	= $_POST['penggunaan_implant']; 
						$jns_implant  			= $_POST['jns_implant']; 
						$jns_implant_lain  		= $_POST['jns_implant_lain']; 
						$ab_post_op  			= $_POST['ab_post_op']; 
						$nama_ab_op  			= $_POST['nama_ab_op'];
						$dosis_ab_op  			= $_POST['dosis_ab_op'];
						$selama_ab_op  			= $_POST['selama_ab_op'];
						$alasan_pos_op  		= $_POST['alasan_pos_op'];
						$alasan_lain_post_op  	= $_POST['alasan_lain_post_op']; 

						$hasil_lab_leukosit  	= $_POST['hasil_lab_leukosit']; 
						$hasil_lab_crcp  		= $_POST['hasil_lab_crcp']; 
						$hasil_lab_pct  		= $_POST['hasil_lab_pct'];
						$hasil_lab_gds  		= $_POST['hasil_lab_gds']; 
						$isi_lab_leukosit  		= $_POST['isi_lab_leukosit']; 
						$isi_lab_crcp  			= $_POST['isi_lab_crcp']; 
						$isi_lab_pct  			= $_POST['isi_lab_pct'];
						$isi_lab_gds  			= $_POST['isi_lab_gds']; 
						$tgl_hasil_lab_op  		= $_POST['tgl_hasil_lab_op'];

						$tgl_isi_form  			= $_POST['tgl_isi_form']; 
						$tgl_selesai_isi  		= $_POST['tgl_selesai_isi']; 
						$nama_petugas  			= $_POST['nama_petugas']; 				
						$user 					= $_SESSION['userid'];			
						// echo "<pre>";print_r($_POST);exit();   						
						$query2 = "UPDATE db_ppi.tb_survido SET												
						nomr           					= '$nomr',
						tglsurveilan           			= '$tglsurveilan',
						tb           					= '$tb',
						bb           					= '$bb',
						tindakan           				= '$tindakan',
						tgl_rencana           			= '$tgl_rencana',
						dokteroperator           		= '$dokteroperator',
						suhu           					= '$suhu',
						lab_tanggal           			= '$lab_tanggal',
						glukosa           				= '$glukosa',
						hb           					= '$hb',
						leukosit           				= '$leukosit',
						albumin           				= '$albumin',
						tgl_mandi1           			= '$tgl_mandi1',
						tgl_mandi2           			= '$tgl_mandi2',
						mandi_menggunakan           	= '$mandi_menggunakan',
						mandi_menggunakan_lain          = '$mandi_menggunakan_lain',
						alat_cukur           			= '$alat_cukur',
						lokasi_mandi           			= '$lokasi_mandi',
						tgl_cukur           			= '$tgl_cukur',
						asascore           				= '$asascore',
						petugas_asa_score           	= '$petugas_asa_score',
						klasifikasi_lukaop           	= '$klasifikasi_lukaop',
						urgensi_op           			= '$urgensi_op',
						ruang_operasi           		= '$ruang_operasi',
						nama_operator           		= '$nama_operator',
						nama_asisten           			= '$nama_asisten',
						asisten_dokter           		= '$asisten_dokter',
						perawat_instrumen           	= '$perawat_instrumen',
						perawat_sirkuler           		= '$perawat_sirkuler',
						dokter_anastesi           		= '$dokter_anastesi',
						perawat_anastesi           		= '$perawat_anastesi',
						ab_dibutuhkan           		= '$ab_dibutuhkan',
						ab_alasanlain           		= '$ab_alasanlain',

						ab_profilaksis           		= '$ab_profilaksis',  				
						nama_ab_prof           			= '$nama_ab_prof', 					
						dosis_ab_prof           		= '$dosis_ab_prof', 					
						waktu_ab_prof           		= '$waktu_ab_prof', 					
						ulang_ab_prof           		= '$ulang_ab_prof', 					
						waktu_pemberian_ulang_ab_prof	= '$waktu_pemberian_ulang_ab_prof',

						antiseptik           			= '$antiseptik',
						teknik_preparasi           		= '$teknik_preparasi',
						nama_hh_operator           		= '$nama_hh_operator',
						nama_hh_perawat           		= '$nama_hh_perawat',
						nama_hh_sirkuler           		= '$nama_hh_sirkuler',
						nama_hh_asisten           		= '$nama_hh_asisten',
						hh_perawat           			= '$hh_perawat',
						hh_sirkuler           			= '$hh_sirkuler',
						hh_operator           			= '$hh_operator',
						hh_asisten           			= '$hh_asisten',

						jml_petugas_op           		= '$jml_petugas_op',
						jml_selain_petugas_op           = '$jml_selain_petugas_op',
						jml_pintu_dibuka           		= '$jml_pintu_dibuka',
						terpasang_drain           		= '$terpasang_drain',
						lokasi_drain           			= '$lokasi_drain',
						tipe_drain           			= '$tipe_drain',

						penggunaan_alat_reuse           = '$penggunaan_alat_reuse',
						nama_alat_reuse1           		= '$nama_alat_reuse1',
						nama_alat_reuse2           		= '$nama_alat_reuse2',
						nama_alat_reuse3           		= '$nama_alat_reuse3',
						alat_reuse1           			= '$alat_reuse1', 
						alat_reuse2           			= '$alat_reuse2',
						alat_reuse3           			= '$alat_reuse3',
						penggunaan_implant           	= '$penggunaan_implant',

						jns_implant           			= '$jns_implant',
						jns_implant_lain           		= '$jns_implant_lain',
						ab_post_op           			= '$ab_post_op',
						nama_ab_op           			= '$nama_ab_op', 
						dosis_ab_op           			= '$dosis_ab_op', 
						selama_ab_op           			= '$selama_ab_op', 
						alasan_pos_op           		= '$alasan_pos_op',
						alasan_lain_post_op           	= '$alasan_lain_post_op',

						hasil_lab_leukosit           	= '$hasil_lab_leukosit',
						hasil_lab_crcp           		= '$hasil_lab_crcp',
						hasil_lab_pct           		= '$hasil_lab_pct',
						hasil_lab_gds           		= '$hasil_lab_gds',
						isi_lab_leukosit           		= '$isi_lab_leukosit',
						isi_lab_crcp           			= '$isi_lab_crcp',
						isi_lab_pct           			= '$isi_lab_pct',
						isi_lab_gds           			= '$isi_lab_gds',
						tgl_hasil_lab_op           		= '$tgl_hasil_lab_op',

						tgl_isi_form           			= '$tgl_isi_form',
						tgl_selesai_isi           		= '$tgl_selesai_isi',
						nama_petugas           			= '$nama_petugas',
						diubah_oleh 					= '$user'
						where id 						= '$id'";
						// echo $query2; die();	
						$update=mysqli_query($conn1,$query2);									
						if($update){
							echo "<script>alert('Laporan Berhasil diubah!'); window.location = 'index.php?page=tabelsurvido'</script>"; 
						}
					}
					$id = $_GET['id'];
					if(isset($_GET['id'])){
						$query1 = "UPDATE db_ppi.tb_survido ti SET ti.`status`=0 WHERE ti.id = '$id'";
						//die($query1);
						$rs1 = mysqli_query($conn1,$query1);						
						if($rs1)
						{
							echo "<script>alert('Menghapus laporan!'); window.location = 'index.php?page=tabelsurvido'</script>";   
						}else{
							echo "<script>alert('Laporan Gagal dihapus!'); window.location = 'index.php?modul=tabelsurvido'</script>"; 
						}	
					}						
					?>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-12 -->
		</div><!-- /row -->
	</section>
</section>			