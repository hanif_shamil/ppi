<?php
$id=($_GET['id']);
$query="SELECT ruan.DESKRIPSI RUANGAN ,ruan.ID ID_RUANGAN, ps.NORM,ps.NAMA, IF(ps.JENIS_KELAMIN=1,'52', '53') KODE_JENIS_KELAMIN, IF(ps.JENIS_KELAMIN=1,'Laki-Laki', 'Perempuan') JENIS_KELAMIN
, ps.JENIS_KELAMIN ID_JK,DATE_FORMAT(ps.TANGGAL_LAHIR, '%d-%m-%Y') TANGGAL_LAHIR, p.TANGGAL TGL_MASUK
, (YEAR(CURDATE())-YEAR(ps.TANGGAL_LAHIR)) UMUR, concat((YEAR(CURDATE())-YEAR(ps.TANGGAL_LAHIR)),' Th') UMUR_T
, ref.DESKRIPSI PENJAMIN, ref.ID ID_PENJAMIN, IF(ref.ID=2,'56',IF(ref.ID=62,'57',IF(1,'54','55'))) PENANGGUNG
, IF(ref.ID=2,'BPJS',IF(ref.ID=62,'Karyawan RSKD',IF(1,'Umum/Tanpa Asuransi','Jaminan Perusahaan'))) NAMA_PENANGGUNG
, neg.DESKRIPSI KEWARGANEGARAAN, pek.DESKRIPSI PEKERJAAN, kp.NOMOR TELPON, ps.ALAMAT, kip.NOMOR KTP, p.NOMOR NOPEN
, dia.ICD, dia.DIAGNOSA, CONCAT(dia.ICD,'-',dia.DIAGNOSA) DIAGMASUK, ido.*
FROM pendaftaran.pendaftaran p
LEFT JOIN pendaftaran.kunjungan pk ON p.NOMOR = pk.NOPEN
LEFT JOIN master.ruangan ruan ON ruan.ID = pk.RUANGAN
LEFT JOIN master.pasien ps ON ps.NORM = p.NORM
LEFT JOIN pendaftaran.tujuan_pasien tp ON tp.NOPEN = p.NOMOR
LEFT JOIN master.ruangan r ON r.ID = tp.RUANGAN	
LEFT JOIN pendaftaran.penjamin pj ON pj.NOPEN = p.NOMOR
LEFT JOIN master.referensi ref ON ref.ID = pj.JENIS AND ref.JENIS=10
left JOIN master.negara neg ON neg.ID=ps.KEWARGANEGARAAN
LEFT JOIN master.referensi pek ON pek.ID=ps.PEKERJAAN and pek.JENIS=4
LEFT JOIN master.kontak_pasien kp ON kp.NORM=ps.NORM
LEFT JOIN master.kartu_identitas_pasien kip ON kip.NORM=ps.NORM
LEFT JOIN master.diagnosa_masuk dia ON dia.ID=p.DIAGNOSA_MASUK
left JOIN db_ppi.tb_survido ido ON ido.nomr=ps.NORM
WHERE ido.id = '$id' AND p.`STATUS`!=0 #AND pk.`STATUS`=1 
AND ruan.JENIS_KUNJUNGAN in  (2,3)
ORDER BY pk.MASUK DESC LIMIT 1";
$hasil = mysqli_query($conn1,$query);
$dt = mysqli_fetch_array($hasil);
$droperator = explode(",", $dt['dokteroperator']);
$namaoperator = explode(",", $dt['nama_operator']);
$nama_hh_operator = explode(",", $dt['nama_hh_operator']);
{
	?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs ace-save-state" id="breadcrumbs">
				<ul class="breadcrumb">
					<li>
						<i class="ace-icon fa fa-home home-icon"></i>
						<a href="#">Home</a>
					</li>

					<li>
						<a href="#">FORMULIR</a>
					</li>
					<li class="active">SURVEILANS PASIEN PERI-OPERATIF INFEKSI DAERAH OPERASI (IDO)</li>
				</ul>			
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="widget-box">
							<div class="widget-header">
								<h4 class="widget-title">SURVEILANS PASIEN PERI-OPERATIF IDO</h4>
								<div class="widget-toolbar">
									<a href="#" data-action="collapse">
										<i class="ace-icon fa fa-chevron-up"></i>
									</a>
									<a href="#" data-action="close">
										<i class="ace-icon fa fa-times"></i>
									</a>
								</div>
							</div>
							<div class="widget-body">
								<div class="widget-main">
									<form class="form-horizontal" id="sample-form" action="index.php?page=simpansurvido" method="post">
										<input type="hidden" id="id" name="ID" value="<?php echo $id; ?>" class="form-control" readonly />
										<div class="form-group">
											<div class="col-sm-2">
												<label>No MR</label>
												<input type="text" id="nomr" name="nomr" autocomplete="off" placeholder="[ No.RM ]" value="<?php echo $dt['nomr']; ?>" class="form-control" onkeyup="autofill()" />
											</div>							
											<div class="col-sm-4">
												<label>Nama Pasien</label>
												<input type="text" id="nama" name="nama" value="<?php echo $dt['NAMA']; ?>" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
											</div>
											<div class="col-sm-2">
												<label>Tanggal Lahir</label>
												<div class="input-group date">
													<input name="" id="tgl_lahir" placeholder="[ TANGGAL LAHIR ]" value="<?php echo $dt['TANGGAL_LAHIR']; ?>" class="form-control" readonly> 
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>	
											<div class="col-sm-2">
												<label>Umur</label>
												<input name="" id="umur_t" value="<?php echo $dt['UMUR']; ?>" placeholder="[ TANGGAL LAHIR ]" class="form-control" readonly> 
											</div>	
											<div class="col-sm-2">
												<label>Jenis Kelamin</label>
												<input name="" id="jk" value="<?php echo $dt['JENIS_KELAMIN']; ?>" placeholder="[ JENIS KELAMIN ]" class="form-control" readonly> 
											</div>														
										</div>	
										<div class="form-group">
											<div class="col-sm-2">
												<label>Tanggal Masuk</label>					
												<div class="input-group date">
													<input type="text" id="tgl_masuk" value="<?php echo $dt['TGL_MASUK']; ?>" name="tglmasukruangan" placeholder="TGL MASUK RUANG RAWAT" class="form-control" autocomplete="off" readonly/>
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-3">
												<label>Asal Ruangan</label>		
												<input type="text" id="id_ruangan" value="<?php echo $dt['RUANGAN']; ?>" name="asalruangan" placeholder=" [ RUANG RAWAT SAAT INI ]" class="form-control" autocomplete="off" readonly/>
											</div>
											<div class="col-sm-3">
												<label>Diagnosa Primer</label>	
												<input type="text" id="diagmasuk" value="<?php echo $dt['DIAGNOSA']; ?>" name="" placeholder=" [ DIAGNOSA MEDIS ]" class="form-control" autocomplete="off" readonly/>

											</div>
											<div class="col-sm-2">
												<label>Tanggal Surveilans</label>					
												<div class="input-group date">
													<input type="text" id="" name="tglsurveilan" value="<?php echo $dt['tglsurveilan']; ?>" placeholder="TGL SURVEILANS" class="form-control datetime " autocomplete="off">
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
											<div class="col-sm-1">
												<label>TB</label>	
												<input type="text" name="tb" placeholder=" [ TB ]" value="<?php echo $dt['tb']; ?>" class="form-control" autocomplete="off">
											</div>
											<div class="col-sm-1">
												<label>BB</label>	
												<input type="text" name="bb" placeholder=" [ BB ]" value="<?php echo $dt['bb']; ?>" class="form-control" autocomplete="off">
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-10">
												<label>Tindakan Operasi</label>		
												<input type="text" name="tindakan" value="<?php echo $dt['tindakan']; ?>" placeholder=" [ TINDAKAN OPERASI ]" class="form-control" autocomplete="off">
											</div>
											<div class="col-sm-2">
												<label>Rencana Operasi</label>					
												<div class="input-group date">
													<input type="text" id="datetimepicker" value="<?php echo $dt['tgl_rencana']; ?>" name="tgl_rencana" placeholder="TGL RENCANA" class="form-control" autocomplete="off">
													<span class="input-group-addon">
														<i class="fa fa-calendar bigger-110"></i>
													</span>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="col-sm-12">
												<label>Dokter Bedah/Operator</label>		
												<select multiple="" name="dokteroperator[]" class="chosen-select form-control" id="form-field-select-4" data-placeholder="Pilih Nama Dokter">
													<option value=""></option>
													<?php
													$query = mysqli_query($conn1,"SELECT dok.ID,master.getNamaLengkapPegawai(dok.NIP) DOKTER
														FROM master.dokter dok
														LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
														WHERE p.`STATUS`=1 AND dok.`STATUS`=1");
													$i=0;
													while($data = mysqli_fetch_array($query)) {
														if(in_array($data[0],$droperator)) $str_flag = "selected";
														else $str_flag="";
														?>
														<option value="<?=$data[0];?>" <?php echo $str_flag; ?>><?=$data[1];?></option>
														<?php
														$i++;
													}
													?>
												</select>
											</div>
										</div>


										<div class="widget-header" style="text-align: center;">
											<h4 class="widget-title">PRE/PERI - OPERATIVE PROCESS MEASURES</h4>
										</div>
										<div class="widget-header">
											<h4 class="widget-title">PRE OP (DI RUANGAN RAWAT INAP)</h4>
										</div>

										<div class="row">
											<div class="col-xs-12 col-sm-12">
												<div class="widget-box">
													<div class="widget-header">
														<h4 class="widget-title">PERSIAPAN PRE-OP</h4>
													</div>

													<div class="widget-body">
														<div class="widget-main">
															<div class="form-group">
																<div class="col-sm-2">
																	<label>Suhu Pasien</label>		
																	<input type="text" name="suhu" value="<?php echo $dt['suhu']; ?>" placeholder=" [ SUHU PASIEN ]" class="form-control" autocomplete="off">
																</div>
																<div class="col-sm-3">
																	<label>Hasil Lab Tanggal</label>		
																	<input type="text" id="datetimepicker1" name="lab_tanggal" value="<?php echo $dt['lab_tanggal']; ?>" placeholder=" [ TANGGAL HASIL LAB ]" class="form-control" autocomplete="off">
																</div>
																<div class="col-sm-2">
																	<label>Glukosa</label>		
																	<input type="text" name="glukosa" value="<?php echo $dt['glukosa']; ?>" placeholder=" [ GLUKOSA ]" class="form-control" autocomplete="off">
																</div>

																<div class="col-sm-1">
																	<label>HB</label>		
																	<input type="text" name="hb" value="<?php echo $dt['hb']; ?>" placeholder=" [ HB ]" class="form-control" autocomplete="off">
																</div>
																<div class="col-sm-2">
																	<label>Leukosit</label>		
																	<input type="text" name="leukosit" value="<?php echo $dt['leukosit']; ?>" placeholder=" [ LEUKOSIT ]" class="form-control" autocomplete="off">
																</div>
																<div class="col-sm-2">
																	<label>Albumin</label>		
																	<input type="text" name="albumin" value="<?php echo $dt['albumin']; ?>" placeholder=" [ ALBUMIN ]" class="form-control" autocomplete="off">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12">
												<div class="widget-box">
													<div class="widget-body">
														<div class="widget-main">
															<div>
																<h4 class="media-heading">
																	<a href="#" class="blue">Mandi</a>
																</h4>
															</div>
															<p>
																Mulai sore hari sehari sebelum operasi dan pagi hari pada saat mau operasi
															</p>
															<div class="form-group">
																<div class="col-sm-2">
																	<label>Tanggal Mandi 1</label>		
																	<input type="text" id="datetimepicker11" value="<?php echo $dt['tgl_mandi1']; ?>" name="tgl_mandi1" placeholder=" [ TANGGAL MANDI 1 ]" class="form-control" autocomplete="off">
																</div>
																<div class="col-sm-2">
																	<label>Tanggal Mandi 2</label>		
																	<input type="text"name="tgl_mandi2" id="datetimepicker12" value="<?php echo $dt['tgl_mandi2']; ?>" placeholder=" [ TANGGAL MANDI 2 ]" class="form-control" autocomplete="off">
																</div>
																<div class="col-sm-4">
																	<label>Mandi menggunakan</label><br>	
																	<input type="radio" class="ace" id="sabun" <?php if($dt['mandi_menggunakan']=='1') echo " checked "?> value="1" name="mandi_menggunakan" value="1" autocomplete="off" />
																	<label class="lbl" for="sabun"> Sabun anti bakteri/Chlorhexiden; </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<input type="radio" class="ace" id="bukan" <?php if($dt['mandi_menggunakan']=='2') echo " checked "?> value="2" name="mandi_menggunakan" value="2" autocomplete="off" />
																	<label class="lbl" for="bukan"> Jika bukan, menggunakan</label>
																</div>
																<div class="col-sm-4">
																	<label>&nbsp;</label>		
																	<input type="text"name="mandi_menggunakan_lain" value="<?php echo $dt['mandi_menggunakan_lain']; ?>" placeholder=" [ JIKA BUKAN MENGGUNAKAN SABUN ]" class="form-control" autocomplete="off">
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-4">
																	<label>Mencukur rambut menggunakan</label><br>		
																	<input type="radio" class="ace" <?php if($dt['alat_cukur']=='1') echo " checked "?> id="razor" value="1" name="alat_cukur" autocomplete="off" />
																	<label class="lbl" for="razor"> Razor</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<input type="radio" class="ace" <?php if($dt['alat_cukur']=='2') echo " checked "?> id="klipper" value="2" name="alat_cukur" autocomplete="off" />
																	<label class="lbl" for="kliper"> Klipper</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<input type="radio" class="ace" <?php if($dt['alat_cukur']=='3') echo " checked "?> id="tidak_cukur" value="3" name="alat_cukur" autocomplete="off" />
																	<label class="lbl" for="tdkcukur"> Tidak cukur</label>
																</div>
																<div class="col-sm-4">
																	<label>Dimana</label><br>
																	<input type="radio" class="ace" id="rumah" <?php if($dt['lokasi_mandi']=='1') echo " checked "?> value="1" name="lokasi_mandi" autocomplete="off" />
																	<label class="lbl" for="diaman1"> Rumah </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<input type="radio" class="ace" id="ruang_perawatan" <?php if($dt['lokasi_mandi']=='2') echo " checked "?> value="2" name="lokasi_mandi" autocomplete="off" />
																	<label class="lbl" for="diaman2"> Ruang Perawatan</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																	<input type="radio" class="ace" id="ok" value="3" <?php if($dt['lokasi_mandi']=='3') echo " checked "?> name="lokasi_mandi" autocomplete="off" />
																	<label class="lbl" for="diaman3"> OK</label>
																</div>
																<div class="col-sm-2">
																	<label>Tanggal Cukur</label>		
																	<input type="text"name="tgl_cukur" id="datetimepicker13" value="<?php echo $dt['tgl_cukur']; ?>" placeholder=" [ TANGGAL CUKUR ]" class="form-control" autocomplete="off">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12">
												<div class="widget-box">
													<div class="widget-header">
														<h4 class="widget-title">ASA Score</h4>
													</div>

													<div class="widget-body">
														<div class="widget-main">
															<div class="form-group">
																<div class="col-sm-12">
																	<input type="radio" class="ace" name="asascore" <?php if($dt['asascore']=='1') echo " checked "?> value="1" id="asa_score1" autocomplete="off" />
																	<label class="lbl" for="asa_score1"> Status kesehatan pasien normal, hermodinamik stabil </label><br>
																	<input type="radio" class="ace" name="asascore" <?php if($dt['asascore']=='2') echo " checked "?> value="2" id="asa_score2" autocomplete="off" />
																	<label class="lbl" for="asa_score2"> Gangguan sistemik sedang, (seperti hipertensi, diabetes terkontrol)</label><br>
																	<input type="radio" class="ace" name="asascore" <?php if($dt['asascore']=='3') echo " checked "?>value="3" id="asa_score3" autocomplete="off" />
																	<label class="lbl" for="asa_score3"> Gangguan sistemik berat, tidak ada kelemahan/kelumpuhan (seperti : COPD, Diabetes, keganasan)</label><br>
																	<input type="radio" class="ace" name="asascore" <?php if($dt['asascore']=='4') echo " checked "?> value="4" id="asa_score4" autocomplete="off" />
																	<label class="lbl" for="asa_score4"> Gangguan sistemik berat disertai kelemahan/kelumpuhan bersifat constant dan mengancam kehidupan (seperti: preeklamasi, perdarahan berat)</label><br>
																	<input type="radio" class="ace" name="asascore" <?php if($dt['asascore']=='5') echo " checked "?> value="5" id="asa_score5" autocomplete="off" />
																	<label class="lbl" for="asa_score5"> Pasien yang sekarat, tidak bisa selamat dengan atau tanpa dioperasi (seperti: trauma besar dan luas)</label>
																</div>
															</div>
															<div class="form-group">
																<div class="col-sm-3">
																	<label>Nama Petugas</label>
																	<input name="petugas_asa_score" value="<?php echo $dt['petugas_asa_score']; ?>" placeholder="[ NAMA PETUGAS ]" class="form-control"> 
																</div>	
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="widget-header">
											<h4 class="widget-title">INTRA OP (DI RUANGAN OPERASI/IBS)</h4>
										</div>
										<div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title">KLASIFIKASI LUKA OPERASI</h4>
											</div>
											<div class="widget-body">
												<div class="widget-main">
													<div class="form-group">
														<div class="col-sm-2">
															<label class="lbl" for="ok3"> Bersih</label><br>
														</div>
														<div class="col-sm-10">
															<input type="radio" class="ace" id="bersih" <?php if($dt['klasifikasi_lukaop']=='1') echo " checked "?> value="1" name="klasifikasi_lukaop" autocomplete="off" />
															<label class="lbl" for="bersih1"> Operasi pada jaringan steril, tidak ada bakteri residen (bedah syaraf, mastectomy, exremitas)</label><br>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-2">
															<label class="lbl" for="ok3"> Bersih-Terkontaminasi </label><br>
														</div>
														<div class="col-sm-10">
															<input type="radio" class="ace" value="2" <?php if($dt['klasifikasi_lukaop']=='2') echo " checked "?> name="klasifikasi_lukaop" id="bersih_terkontaminasi" autocomplete="off" />
															<label class="lbl" for="bersih2"> Operasi yang membuka traktus yang ada bakteri residen histerektomi, laparotomi, secio, apendictomi, trachesotomy, nefrostomy, operasi pada oroforing</label><br>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-2">
															<label class="lbl" for="ok3"> Terkontaminasi</label><br>
														</div>
														<div class="col-sm-10">
															<input type="radio" class="ace" id="terkontaminasi" <?php if($dt['klasifikasi_lukaop']=='3') echo " checked "?> value="3" name="klasifikasi_lukaop" autocomplete="off" />
															<label class="lbl" for="bersih3"> Operasi yang masuk ke dalam jaringan dengan bakteri yang tidak terkontrol (atau GI dengan perforasi, Apendektomy dengan perforasi)</label><br>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-2">
															<label class="lbl" for="ok3"> Kotor/Infeksi</label><br>
														</div>
														<div class="col-sm-10">
															<input type="radio" class="ace" id="kotor" value="4" <?php if($dt['klasifikasi_lukaop']=='4') echo " checked "?> name="klasifikasi_lukaop" autocomplete="off" />
															<label class="lbl" for="bersih4"> Kontaminasi berat (adanya luka infeksi) atau sudah ada infeksi sebelum operasi</label><br>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="widget-box">
											<div class="widget-header">
												<h4 class="widget-title">URGENSI OPERASI</h4>
											</div>
											<div class="widget-body">
												<div class="widget-main">
													<div class="form-group">												
														<div class="col-sm-12">
															<input type="radio" class="ace" id="urgensi_op_1" <?php if($dt['urgensi_op']=='1') echo " checked "?> name="urgensi_op" value="1" autocomplete="off" />
															<label class="lbl" for="urgensi_op_1"> Emergency - harus segera dioperasi untuk menyelamatkan hidup (perdarahan massif)</label><br>
															<input type="radio" class="ace" id="urgensi_op_2" <?php if($dt['urgensi_op']=='2') echo " checked "?> name="urgensi_op" value="2" autocomplete="off" />
															<label class="lbl" for="urgensi_op_2"> Urgent - harus dioperasi dalam waktu 24-48 jam (operasi untuk refair fraktur)</label><br>
															<input type="radio" class="ace" id="urgensi_op_3" <?php if($dt['urgensi_op']=='3') echo " checked "?> name="urgensi_op" value="3" autocomplete="off" />
															<label class="lbl" for="urgensi_op_3"> Semi elektif - harus dioperasi dalam beberapa hari sampai 1 minggu (operasi tumor/kanker)</label><br>
															<input type="radio" class="ace" id="urgensi_op_4" <?php if($dt['urgensi_op']=='4') echo " checked "?> name="urgensi_op" value="4" autocomplete="off" />
															<label class="lbl" for="urgensi_op_4"> Elektif - tidak ada batasan waktu, bisa direncanakan (operasi kosmetik)</label><br>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="widget-box">	
											<div class="widget-body">
												<div class="widget-main">
													<div class="form-group">
														<div class="col-sm-7">
															<label>Ruang Operasi</label><br>		
															<input type="radio" class="ace" id="ok1" value="1" name="ruang_operasi" <?php if($dt['ruang_operasi']=='1') echo " checked "?>  autocomplete="off" />
															<label class="lbl" for="ok1"> OK1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="radio" class="ace" id="ok2" value="2" name="ruang_operasi" <?php if($dt['ruang_operasi']=='2') echo " checked "?>  autocomplete="off" />
															<label class="lbl" for="ok2"> OK2</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="radio" class="ace" id="ok3" value="3" name="ruang_operasi" <?php if($dt['ruang_operasi']=='3') echo " checked "?>  autocomplete="off" />
															<label class="lbl" for="ok3"> OK3</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="radio" class="ace" id="ok4" value="4" name="ruang_operasi" <?php if($dt['ruang_operasi']=='4') echo " checked "?>  autocomplete="off" />
															<label class="lbl" for="ok4"> OK4</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="radio" class="ace" id="ok5" value="5" name="ruang_operasi" <?php if($dt['ruang_operasi']=='5') echo " checked "?>  autocomplete="off" />
															<label class="lbl" for="ok5"> OK5</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="radio" class="ace" id="ok6" value="6" name="ruang_operasi" <?php if($dt['ruang_operasi']=='6') echo " checked "?>  autocomplete="off" />
															<label class="lbl" for="ok6"> OK6</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="radio" class="ace" id="ok7" value="7" name="ruang_operasi" <?php if($dt['ruang_operasi']=='7') echo " checked "?>  autocomplete="off" />
															<label class="lbl" for="ok7"> OK7</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
															<input type="radio" class="ace" id="ok8" value="8" name="ruang_operasi" <?php if($dt['ruang_operasi']=='8') echo " checked "?>  autocomplete="off" />
															<label class="lbl" for="ok8"> OK8</label>
														</div>
														<div class="col-sm-5">
															<label>Nama operator</label><br>		
															<select multiple="" class="chosen-select form-control" name="nama_operator[]" id="form-field-select-4" data-placeholder="PILIH OPERATOR">
																<option value=""></option>
																<?php
																$query = mysqli_query($conn1,"SELECT dok.ID,master.getNamaLengkapPegawai(dok.NIP) DOKTER
																	FROM master.dokter dok
																	LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
																	WHERE p.`STATUS`=1 AND dok.`STATUS`=1");
																$i=0;
																while($data = mysqli_fetch_array($query)) {
																	if(in_array($data[0],$namaoperator)) $str_flag = "selected";
																	else $str_flag="";
																	?>
																	<option value="<?=$data[0];?>" <?php echo $str_flag; ?>><?=$data[1];?></option>
																	<?php
																	$i++;
																}
																?>
															</select>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-4">
															<label>Asisten</label><br>		
															<select class="chosen-select form-control" name="nama_asisten" data-placeholder="PILIH ASISTEN">
																<option value=""></option>
																<?php
																$sql = "SELECT dok.NIP,master.getNamaLengkapPegawai(dok.NIP) NAMA
																FROM master.dokter dok
																LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
																WHERE p.`STATUS`=1 AND dok.`STATUS`=1
																UNION
																SELECT pr.NIP,master.getNamaLengkapPegawai(pr.NIP) NAMA
																FROM master.perawat pr
																LEFT JOIN master.pegawai p ON p.NIP=pr.NIP
																WHERE p.`STATUS`=1 AND pr.`STATUS`=1";
																$rs = mysqli_query($conn1,$sql);
																while ($data = mysqli_fetch_array($rs)) {
																	?>
																	<option <?php if($dt['nama_asisten']==$data[0]){echo "selected";}?> value="<?php echo $data[0]; ?>"><?php echo $data[1] ?></option>
																	<?php
																}
																?>	
															</select>
														</div>
														<div class="col-sm-4">
															<label class="lbl" for=""> Asisten Dokter</label>
															<input type="text" class="form-control" value="<?php echo $dt['asisten_dokter']; ?>" placeholder="Asisten Dokter" name="asisten_dokter" autocomplete="off" />
														</div>
														<div class="col-sm-4">
															<label>Perawat Instrumen</label><br>		
															<select class="chosen-select form-control" name="perawat_instrumen" data-placeholder="PILIH PERAWAT">
																<option value=""></option>
																<?php
																$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
																where pg.PROFESI=6";
																$rs = mysqli_query($conn1,$sql);
																while ($data = mysqli_fetch_array($rs)) {
																	?>

																	<option <?php if($dt['perawat_instrumen']==$data[0]){echo "selected";}?> value="<?php echo $data[0]; ?>"><?php echo $data[1] ?></option>
																	<?php
																}
																?>	
															</select>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-4">
															<label>Perawat Sirkuler</label><br>
															<select class="chosen-select form-control" name="perawat_sirkuler" data-placeholder="PILIH PERAWAT">
																<option value=""></option>
																<?php
																$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
																where pg.PROFESI=6";
																$rs = mysqli_query($conn1,$sql);
																while ($data = mysqli_fetch_array($rs)) {
																	?>

																	<option <?php if($dt['perawat_sirkuler']==$data[0]){echo "selected";}?> value="<?php echo $data[0]; ?>"><?php echo $data[1] ?></option>
																	<?php
																}
																?>	
															</select>
														</div>
														<div class="col-sm-4">
															<label>Dokter Anastesi</label><br>		
															<select class="chosen-select form-control" name="dokter_anastesi" data-placeholder="PILIH DOKTER ANASTESI">
																<option value=""></option>
																<?php
																$sql = "SELECT dok.ID,master.getNamaLengkapPegawai(dok.NIP) DOKTER
																FROM master.dokter dok
																LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
																WHERE p.`STATUS`=1 AND dok.`STATUS`=1";
																$rs = mysqli_query($conn1,$sql);
																while ($data = mysqli_fetch_array($rs)) {
																	?>

																	<option <?php if($dt['dokter_anastesi']==$data[0]){echo "selected";}?> value="<?php echo $data[0]; ?>"><?php echo $data[1] ?></option>
																	<?php
																}
																?>	
															</select>
														</div>
														<div class="col-sm-4">
															<label>Perawat Anastesi</label><br>		
															<select class="chosen-select form-control" name="perawat_anastesi" data-placeholder="PILIH PERAWAT">
																<option value=""></option>
																<?php
																$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
																where pg.PROFESI=19";
																$rs = mysqli_query($conn1,$sql);
																while ($data = mysqli_fetch_array($rs)) {
																	?>

																	<option <?php if($dt['perawat_anastesi']==$data[0]){echo "selected";}?> value="<?php echo $data[0]; ?>"><?php echo $data[1] ?></option>
																	<?php
																}
																?>	
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="widget-box">
											<div class="widget-main">
												<div>
													<h4 class="media-heading">
														<a href="#" class="blue">Antibiotic profilaksis</a>
													</h4>
												</div>

												<div class="form-group">
													<div class="col-sm-6">
														<div class="form-group">												
															<div class="col-sm-12">
																<input type="radio" class="ace" name="ab_profilaksis" <?php if($dt['ab_profilaksis']=='1') echo " checked "?> value="1" id="antibiotik_tdkbutuh" autocomplete="off" />
																<label class="lbl" for="antibiotik_tdkbutuh"> Tidak dibutuhkan</label><br>
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-12">
																<input type="radio" class="ace" id="dibutuhkan_ab" value="2" name="ab_profilaksis" <?php if($dt['ruang_operasi']=='2') echo " checked "?> autocomplete="off" />
																<label class="lbl" for="dibutuhkan_ab"> Dibutuhkan namun tidak dapat diberikan, dengan alasan:</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="ab_dibutuhkan1" name="ab_dibutuhkan" <?php if($dt['ab_dibutuhkan']=='1') echo " checked "?>  value="1" autocomplete="off" />
																<label class="lbl" for="ab_dibutuhkan1"> Tidak ada obat</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="ab_dibutuhkan2" name="ab_dibutuhkan" <?php if($dt['ab_dibutuhkan']=='2') echo " checked "?>  value="2" autocomplete="off" />
																<label class="lbl" for="ab_dibutuhkan2"> Alasan lain :</label>
																<input type="text" class="form-control" name="ab_alasanlain" value="<?php echo $dt['ab_alasanlain']; ?>" autocomplete="off" />
															</div>
														</div>

													</div>

													<div class="col-sm-6">
														<div class="form-group">												
															<div class="col-sm-12">
																<input type="radio" class="ace" id="ok3" value="3" name="ab_profilaksis" <?php if($dt['ab_profilaksis']=='3') echo " checked "?> autocomplete="off" />
																<label class="lbl" for="ok3"> Diberikan anti biotic</label><br>
																<input type="text" class="form-control" name="nama_ab_prof" value="<?php echo $dt['nama_ab_prof']; ?>" autocomplete="off" />
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-2">
																<label>Dosis</label>
																<input type="text" class="form-control" name="dosis_ab_prof" value="<?php echo $dt['dosis_ab_prof']; ?>" autocomplete="off" />
															</div>
															<div class="col-sm-4">
																<label>Waktu pemberian</label>
																<input type="text" class="form-control" name="waktu_ab_prof" value="<?php echo $dt['waktu_ab_prof']; ?>" autocomplete="off" />
															</div>
															<div class="col-sm-2">
																<label>D. ulang</label>
																<input type="text" class="form-control" name="ulang_ab_prof" value="<?php echo $dt['ulang_ab_prof']; ?>" autocomplete="off" />
															</div>
															<div class="col-sm-4">
																<label>Waktu pemberian</label>
																<input type="text" class="form-control" name="waktu_pemberian_ulang_ab_prof" value="<?php echo $dt['waktu_pemberian_ulang_ab_prof']; ?>" autocomplete="off" />
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="widget-box">
											<div class="widget-main">
												<div>
													<h4 class="media-heading">
														<a href="#" class="blue">Preparasi kulit saat pembedahan (kondisi steril)</a>
													</h4>
												</div>
												<div class="form-group">
													<div class="col-sm-8">
														<label>Menggunakan Cairan antiseptik</label><br>		
														<input type="radio" class="ace" id="antiseptik1" value="1" name="antiseptik" <?php if($dt['antiseptik']=='1') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="antiseptik1"> Chlorhexidin 2% + alkohol 70%</label>&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="antiseptik2" value="2" name="antiseptik" <?php if($dt['antiseptik']=='2') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="antiseptik2"> Iodin + Alkohol 70%</label>&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="antiseptik3" value="3"name="antiseptik" <?php if($dt['antiseptik']=='3') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="antiseptik3"> Chlorexidin 2% + aqua</label>&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="antiseptik4" value="4" name="antiseptik" <?php if($dt['antiseptik']=='4') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="antiseptik4"> Iodin + Aqua</label>
													</div>
													<div class="col-sm-4">
														<label>Teknik preparasi kulit dilakukan dengan benar</label><br>		
														<input type="radio" class="ace" id="teknik_preparasi1" value="1" name="teknik_preparasi" <?php if($dt['teknik_preparasi']=='1') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="teknik_preparasi1"> Ya</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="teknik_preparasi2" value="2" name="teknik_preparasi" <?php if($dt['teknik_preparasi']=='2') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="teknik_preparasi2"> Tidak</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													</div>
												</div>
											</div>
										</div>
										<div class="widget-box">
											<div class="widget-main">
												<div>
													<h4 class="media-heading">
														<a href="#" class="blue">Hand hygiene Bedah Menggunakan Cara : </a>
													</h4>
												</div>
												<div class="form-group">
													<div class="col-sm-5">
														<label><b>Operator</b></label><br>		
														<select multiple="" class="chosen-select form-control" name="nama_hh_operator[]" id="form-field-select-4" data-placeholder="Pilih Operator">
															<option value=""></option>
															<?php
															$query = mysqli_query($conn1,"SELECT dok.ID,master.getNamaLengkapPegawai(dok.NIP) DOKTER
																FROM master.dokter dok
																LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
																WHERE p.`STATUS`=1 AND dok.`STATUS`=1");
															$i=0;
															while($data = mysqli_fetch_array($query)) {
																if(in_array($data[0],$nama_hh_operator)) $str_flag = "selected";
																else $str_flag="";
																?>
																<option value="<?=$data[0];?>" <?php echo $str_flag; ?>><?=$data[1];?></option>
																<?php
																$i++;
															}
															?>
														</select>

													</div>
													<div class="col-sm-5">
														<label>&nbsp;</label><br>		
														<input type="radio" class="ace" id="hh_operator1" value="1" name="hh_operator" <?php if($dt['hh_operator']=='1') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="hh_operator1"> Handcrub</label>&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="hh_operator2" value="2" name="hh_operator" <?php if($dt['hh_operator']=='2') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="hh_operator2"> Sabun antiseftik + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="hh_operator3" value="3" name="hh_operator" <?php if($dt['hh_operator']=='3') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="hh_operator3"> Sabun biasa + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-5">
														<label><b>Perawat instrumen</b></label><br>		
														<select class="chosen-select form-control" name="nama_hh_perawat" data-placeholder="PILIH PERAWAT">
															<option value=""></option>
															<?php
															$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
															where pg.PROFESI=6";
															$rs = mysqli_query($conn1,$sql);
															while ($data = mysqli_fetch_array($rs)) {
																?>
																<option <?php if($dt['nama_hh_perawat']==$data[0]){echo "selected";}?> value="<?php echo $data[0]; ?>"><?php echo $data[1] ?></option>
																<?php
															}
															?>	
														</select>

													</div>
													<div class="col-sm-5">
														<label>&nbsp;</label><br>		
														<input type="radio" class="ace" id="hh_perawat1" value="1" name="hh_perawat" <?php if($dt['hh_perawat']=='1') echo " checked "?>  autocomplete="off" />
														<label class="lbl" for="hh_perawat1"> Handcrub</label>&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="hh_perawat2" value="2" name="hh_perawat" <?php if($dt['hh_perawat']=='2') echo " checked "?>  autocomplete="off" />
														<label class="lbl" for="hh_perawat2"> Sabun antiseftik + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="hh_perawat3" value="3" name="hh_perawat" <?php if($dt['hh_perawat']=='3') echo " checked "?>  autocomplete="off" />
														<label class="lbl" for="hh_perawat3"> Sabun biasa + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-5">
														<label><b>Sirkuler</b></label><br>		
														<select class="chosen-select form-control" name="nama_hh_sirkuler" id="form-field-select-4" data-placeholder="Sirkuler">
															<option value=""></option>
															<?php
															$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
															where pg.PROFESI=6";
															$rs = mysqli_query($conn1,$sql);
															while ($data = mysqli_fetch_array($rs)) {
																?>

																<option <?php if($dt['nama_hh_sirkuler']==$data[0]){echo "selected";}?> value="<?php echo $data[0]; ?>"><?php echo $data[1] ?></option>
																<?php
															}
															?>	
														</select>

													</div>
													<div class="col-sm-5">
														<label>&nbsp;</label><br>		
														<input type="radio" class="ace" id="hh_sirkuler1" value="1" name="hh_sirkuler" <?php if($dt['hh_sirkuler']=='1') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="hh_sirkuler1"> Handcrub</label>&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="hh_sirkuler2" value="2" name="hh_sirkuler" <?php if($dt['hh_sirkuler']=='2') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="hh_sirkuler2"> Sabun antiseftik + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="hh_sirkuler3" value="3" name="hh_sirkuler" <?php if($dt['hh_sirkuler']=='3') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="hh_sirkuler3"> Sabun biasa + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-5">
														<label class="lbl" for="ok3"> Asisten</label>
														<input type="text" name="nama_hh_asisten" value="<?php echo $dt['nama_hh_asisten']; ?>" placeholder="Asisten" class="form-control" autocomplete="off">
													</div>
													<div class="col-sm-5">
														<label>&nbsp;</label><br>		
														<input type="radio" class="ace" id="hh_asisten1" <?php if($dt['hh_asisten']=='1') echo " checked "?> value="1" name="hh_asisten" autocomplete="off" />
														<label class="lbl" for="hh_asisten1"> Handcrub</label>&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="hh_asisten2" <?php if($dt['hh_asisten']=='2') echo " checked "?> value="2" name="hh_asisten" autocomplete="off" />
														<label class="lbl" for="hh_asisten2"> Sabun antiseftik + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="hh_asisten3" <?php if($dt['hh_asisten']=='3') echo " checked "?> value="3" name="hh_asisten" autocomplete="off" />
														<label class="lbl" for="hh_asisten3"> Sabun biasa + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
													</div>
												</div>
											</div>
										</div>
										<div class="widget-box">
											<div class="widget-main">
												<div>
													<h4 class="media-heading">
														<a href="#" class="blue">Traffic Ruang Operasi</a>
													</h4>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label>Total jumlahpetugas saat mulai operasi</label>		
														<input type="number" name="jml_petugas_op" value="<?php echo $dt['jml_petugas_op']; ?>" placeholder=" [ JUMLAH ]" class="form-control" autocomplete="off">
													</div>
													<div class="col-sm-4">
														<label>Jumlah orang yang masuk selain petugas operasi</label>		
														<input type="number"name="jml_selain_petugas_op" value="<?php echo $dt['jml_selain_petugas_op']; ?>" placeholder=" [ JUMLAH ]" class="form-control" autocomplete="off">
													</div>
													<div class="col-sm-4">
														<label>Jumlah pintu ruang operasi dibuka selama operasi</label>		
														<input type="number"name="jml_pintu_dibuka" value="<?php echo $dt['jml_pintu_dibuka']; ?>" placeholder=" [ JUMLAH ]" class="form-control" autocomplete="off">
													</div>
												</div>
											</div>
										</div>

										<div class="widget-box">
											<div class="widget-main">
												<div>
													<h4 class="media-heading">
														<a href="#" class="blue">Drain/implant</a>
													</h4>
												</div>
												<div class="form-group">
													<div class="col-sm-2">
														<label>Terpasang Drain?</label><br>
														<input type="radio" class="ace" id="terpasang_drain" <?php if($dt['terpasang_drain']=='2') echo " checked "?> value="2" name="terpasang_drain" autocomplete="off" />
														<label class="lbl" for="terpasang_drain"> Tidak </label>&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="terpasang_drain" <?php if($dt['terpasang_drain']=='1') echo " checked "?> value="1" name="terpasang_drain" autocomplete="off" />
														<label class="lbl" for="terpasang_drain"> Ya, Lokasi </label><br></br>
													</div>
													<div class="col-sm-4">
														<input type="text" class="form-control" style="width:350px" value="<?php echo $dt['lokasi_drain']; ?>" name="lokasi_drain" placeholder="LOKASI DRAIN" autocomplete="off" />
													</div>
													<div class="col-sm-6">
														<label>Jika Ya, Tipe Drain?</label><br>
														<input type="radio" class="ace" id="tipe_drain1" value="1" name="tipe_drain" <?php if($dt['tipe_drain']=='1') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="tipe_drain1"> Terbuka </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="tipe_drain2" value="2" name="tipe_drain" <?php if($dt['tipe_drain']=='2') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="tipe_drain2"> Tertutup </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="tipe_drain3" value="3" name="tipe_drain" <?php if($dt['tipe_drain']=='3') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="tipe_drain3"> Vakum </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="tipe_drain4" value="4" name="tipe_drain" <?php if($dt['tipe_drain']=='4') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="tipe_drain4"> Tidak Vakum</label>
													</div>
												</div>
											</div>
										</div>

										<div class="widget-box">
											<div class="widget-main">
												<div>
													<h4 class="media-heading">
														<a href="#" class="blue">Penggunaan alat Re-Use</a>
													</h4>
												</div>
												<div class="form-group">
													<div class="col-sm-2">
														<input type="radio" <?php if($dt['penggunaan_alat_reuse']=='1') echo " checked "?> name="penggunaan_alat_reuse" class="ace" value="1" id="penggunaan_alat_reuse1" autocomplete="off" />
														<label class="lbl" for="penggunaan_alat_reuse1"> Tidak </label>&nbsp;&nbsp;&nbsp;
														<input type="radio" <?php if($dt['penggunaan_alat_reuse']=='2') echo " checked "?> class="ace" name="penggunaan_alat_reuse" value="2" id="penggunaan_alat_reuse2" autocomplete="off" />
														<label class="lbl" for="penggunaan_alat_reuse2"> Ya</label>
													</div>

													<div class="col-sm-3">
														<label class="lbl" for="ok3">Nama alat </label>
														<input type="text" class="form-control" name="nama_alat_reuse1" value="<?php echo $dt['nama_alat_reuse1']; ?>" id="nama_alat_reuse1" autocomplete="off" />	
														<label>Re-Use ke :</label><br>
														<input type="radio" class="ace" id="alat_reuse11" value="1" name="alat_reuse1" <?php if($dt['alat_reuse1']=='1') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse11"> 1 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="alat_reuse12" value="2" name="alat_reuse1" <?php if($dt['alat_reuse1']=='2') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse12"> 2 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="alat_reuse13" value="3" name="alat_reuse1" <?php if($dt['alat_reuse1']=='3') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse13"> 3 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="alat_reuse14" value="4" name="alat_reuse1" <?php if($dt['alat_reuse1']=='4') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse14"> 4</label>
													</div>
													<div class="col-sm-3">
														<label class="lbl" for="ok3">Nama alat </label>
														<input type="text" class="form-control" name="nama_alat_reuse2" value="<?php echo $dt['nama_alat_reuse2']; ?>" id="nama_alat_reuse2" autocomplete="off" />	
														<label>Re-Use ke :</label><br>
														<input type="radio" class="ace" id="alat_reuse21" value="1" name="alat_reuse2" <?php if($dt['alat_reuse2']=='1') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse21"> 1 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="alat_reuse22" value="2" name="alat_reuse2" <?php if($dt['alat_reuse2']=='2') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse22"> 2 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="alat_reuse23" value="3" name="alat_reuse2" <?php if($dt['alat_reuse2']=='3') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse23"> 3 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="alat_reuse24" value="4" name="alat_reuse2" <?php if($dt['alat_reuse2']=='4') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse24"> 4</label>
													</div>
													<div class="col-sm-3">
														<label class="lbl" for="ok3">Nama alat </label>
														<input type="text" class="form-control" name="nama_alat_reuse3" value="<?php echo $dt['nama_alat_reuse3']; ?>" id="nama_alat_reuse3" autocomplete="off" />	
														<label>Re-Use ke :</label><br>
														<input type="radio" class="ace" id="alat_reuse31" value="1" name="alat_reuse3" <?php if($dt['alat_reuse3']=='1') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse31"> 1 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="alat_reuse32" value="2" name="alat_reuse3" <?php if($dt['alat_reuse3']=='2') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse32"> 2 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="alat_reuse33" value="3" name="alat_reuse3" <?php if($dt['alat_reuse3']=='3') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse33"> 3 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="alat_reuse34" value="4" name="alat_reuse3" <?php if($dt['alat_reuse3']=='4') echo " checked "?> autocomplete="off" />
														<label class="lbl" for="alat_reuse34"> 4</label>
													</div>
												</div>
											</div>
										</div>

										<div class="widget-box">
											<div class="widget-main">
												<div class="row">
													<div class="col-sm-4">
														<div>
															<h4 class="media-heading">
																<a href="#" class="blue">Penggunaan implant</a>
															</h4>
														</div>
														<div class="form-group">
															<div class="col-sm-6">
																<input type="radio" class="ace" <?php if($dt['penggunaan_implant']=='1') echo " checked "?> id="penggunaan_implant1" value="1" name="penggunaan_implant" autocomplete="off" />
																<label class="lbl" for="penggunaan_implant1"> Ya, jenis implant</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="jns_implant_metal" value="1" name="jns_implant" <?php if($dt['jns_implant']=='1') echo " checked "?> autocomplete="off" />
																<label class="lbl" for="jns_implant_metal"> Metal</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="jns_implant_skin" value="2" name="jns_implant" <?php if($dt['jns_implant']=='2') echo " checked "?> autocomplete="off" />
																<label class="lbl" for="jns_implant_skin"> Skin graf</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="penggunaan_implant2" name="penggunaan_implant" <?php if($dt['penggunaan_implant']=='2') echo " checked "?> value="2" autocomplete="off" />
																<label class="lbl" for="penggunaan_implant2"> Lain-lain tuliskan</label>
																<span></span><input type="text" class="form-control" name="jns_implant_lain" value="<?php echo $dt['jns_implant_lain']; ?>" autocomplete="off" /><br>
																<input type="radio" class="ace" id="penggunaan_implant3" <?php if($dt['penggunaan_implant']=='3') echo " checked "?> value="3" name="penggunaan_implant" autocomplete="off" />
																<label class="lbl" for="penggunaan_implant3"> Tidak</label>
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div>
															<h4 class="media-heading">
																<a href="#" class="blue">ANTIBIOTIK (AB) POST OPERASI</a>
															</h4>
														</div>
														<div class="form-group">
															<div class="col-sm-6">
																<label>Apakah AB diberikan Post ooperasi</label><br>	
																<input type="radio" class="ace" id="ab_post_op_ya" <?php if($dt['ab_post_op']=='1') echo " checked "?> value="1" name="ab_post_op" autocomplete="off" />
																<label class="lbl" for="ab_post_op_ya"> Ya, tuliskan</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $dt['nama_ab_op']; ?>" placeholder="NAMA ANTI BIOTIK" name="nama_ab_op" class="ace" id="ok3" autocomplete="off" /><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" value="<?php echo $dt['dosis_ab_op']; ?>" placeholder="DOSIS" class="ace" name="dosis_ab_op" id="ok3" autocomplete="off" /><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="number" value="<?php echo $dt['selama_ab_op']; ?>" placeholder="SELAMA" name="selama_ab_op" class="ace" id="ok3" autocomplete="off" /><br><br>
																<input type="radio" class="ace" id="ab_post_op_tdk" value="2" <?php if($dt['ab_post_op']=='2') echo " checked "?> name="ab_post_op" autocomplete="off" />
																<label class="lbl" for="ab_post_op_tdk"> Tidak</label>

															</div>
															<div class="col-sm-6">
																<label class="lbl" for="ok3"> Alasan :</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="alasan_pos_op1" name="alasan_pos_op" <?php if($dt['alasan_pos_op']=='1') echo " checked "?> value="1" autocomplete="off" />
																<label class="lbl" for="alasan_pos_op1"> Post OP Profilaksis</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="alasan_pos_op2" name="alasan_pos_op" <?php if($dt['alasan_pos_op']=='2') echo " checked "?> value="2" autocomplete="off" />
																<label class="lbl" for="alasan_pos_op2"> Drain/implant</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="alasan_pos_op3" name="alasan_pos_op" <?php if($dt['alasan_pos_op']=='3') echo " checked "?> value="3" autocomplete="off" />
																<label class="lbl" for="alasan_pos_op3"> Infeksi</label><br>
																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="alasan_pos_op4" name="alasan_pos_op" <?php if($dt['alasan_pos_op']=='4') echo " checked "?> value="4" autocomplete="off" />
																<label class="lbl" for="alasan_pos_op4"> Alasan lain</label>
																<span></span><input type="text" class="form-control" name="alasan_lain_post_op" value="<?php echo $dt['alasan_lain_post_op']; ?>" autocomplete="off" /><br>

															</div>
														</div>

													</div>
												</div>
											</div>
										</div>

										<div class="widget-box">
											<div class="widget-main">
												<div>
													<h4 class="media-heading">
														<a href="#" class="blue">Hasil Laboratorium intra Op/Post Op</a>
													</h4>
												</div>
												<div class="form-group">
													<div class="col-sm-2">
														<input type="checkbox" class="ace" id="hasil_lab_intra1" <?php if($dt['hasil_lab_leukosit']=='1') echo " checked "?> value="1" name="hasil_lab_leukosit" autocomplete="off" />
														<label class="lbl" for="hasil_lab_intra1"> Leukosit </label>
														<input type="text" class="form-control" name="isi_lab_leukosit" value="<?php echo $dt['isi_lab_leukosit']; ?>" style="width:150px" autocomplete="off" />
													</div>
													<div class="col-sm-2">
														<input type="checkbox" class="ace" id="hasil_lab_intra2" <?php if($dt['hasil_lab_crcp']=='1') echo " checked "?> value="1" name="hasil_lab_crcp" autocomplete="off" />
														<label class="lbl" for="hasil_lab_intra2"> CRP</label>
														<input type="text" class="form-control" name="isi_lab_crcp" value="<?php echo $dt['isi_lab_crcp']; ?>" style="width:150px" autocomplete="off" />
													</div>
													<div class="col-sm-2">
														<input type="checkbox" class="ace" id="hasil_lab_intra3" <?php if($dt['hasil_lab_pct']=='1') echo " checked "?> value="1" name="hasil_lab_pct" autocomplete="off" />
														<label class="lbl" for="hasil_lab_intra3"> PCT (Procalcitonin)</label>
														<input type="text" class="form-control" name="isi_lab_pct" value="<?php echo $dt['isi_lab_pct']; ?>" style="width:150px" autocomplete="off" />
													</div>
													<div class="col-sm-2">
														<input type="checkbox" class="ace" id="hasil_lab_intra4" <?php if($dt['hasil_lab_gds']=='1') echo " checked "?> value="1" name="hasil_lab_gds" autocomplete="off" />
														<label class="lbl" for="hasil_lab_intra4"> GDS</label>
														<input type="text" class="form-control" name="isi_lab_gds" value="<?php echo $dt['isi_lab_gds']; ?>" style="width:150px" autocomplete="off" />
													</div>
													<div class="col-sm-4">
														<label class="lbl" for="ok3"> Tanggal</label>
														<div class="input-group date">
															<input type="text" id="datetimepicker14" name="tgl_hasil_lab_op" value="<?php echo $dt['tgl_hasil_lab_op']; ?>" placeholder="TGL HASIL LAB INTRA OP/POST OP" class="form-control" autocomplete="off">
															<span class="input-group-addon">
																<i class="fa fa-calendar bigger-110"></i>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="widget-box">
											<div class="widget-main">
												<div class="form-group">
													<div class="col-sm-4">
														<label class="lbl" for="ok3"> Tanggal pengisian form</label>
														<div class="input-group date">
															<input type="text" id="datet1" name="tgl_isi_form" value="<?php echo $dt['tgl_isi_form']; ?>" placeholder="TANGGAL PENGISIAN FORM" class="form-control datetimepicker1" autocomplete="off">
															<span class="input-group-addon">
																<i class="fa fa-calendar bigger-110"></i>
															</span>
														</div>
													</div>
													<div class="col-sm-4">
														<label class="lbl" for="ok3"> Tanggal selesai pegisian form</label>
														<div class="input-group date">
															<input type="text" id="datet3" name="tgl_selesai_isi" value="<?php echo $dt['tgl_selesai_isi']; ?>" placeholder="TANGGAL SELESAI PENGISIAN" class="form-control" autocomplete="off">
															<span class="input-group-addon">
																<i class="fa fa-calendar bigger-110"></i>
															</span>
														</div>
													</div>
													<div class="col-sm-4">
														<label class="lbl" for="ok3"> Nama Petugas</label>
														<input type="text" id="tgl_masuk" name="nama_petugas" value="<?php echo $dt['nama_petugas']; ?>" placeholder="NAMA PETUGAS" class="form-control" autocomplete="off">
													</div>
												</div>
											</div>
										</div>
										<hr />
										<div class="form-group">							
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" name="btnEdit" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													%
													Reset
												</button>
											</div>
										</div>
									</form>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }
?>