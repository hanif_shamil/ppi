<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>

				<li>
					<a href="#">FORMULIR</a>
				</li>
				<li class="active">SURVEILANS PASIEN PERI-OPERATIF INFEKSI DAERAH OPERASI (IDO)</li>
			</ul>			
		</div>
		<div class="page-content">
			<div class="row">
				<div class="col-xs-12 col-sm-12">
					<div class="widget-box">
						<div class="widget-header">
							<h4 class="widget-title">SURVEILANS PASIEN PERI-OPERATIF IDO</h4>
							<div class="widget-toolbar">
								<a href="#" data-action="collapse">
									<i class="ace-icon fa fa-chevron-up"></i>
								</a>
								<a href="#" data-action="close">
									<i class="ace-icon fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="widget-body">
							<div class="widget-main">
								<form class="form-horizontal" id="sample-form" action="index.php?page=simpansurvido" method="post">
									<div class="form-group">
										<div class="col-sm-2">
											<label>No MR</label>
											<input type="text" id="nomr" name="nomr" autocomplete="off" placeholder="[ No.RM ]" class="form-control" onkeyup="autofill()" />
										</div>							
										<div class="col-sm-4">
											<label>Nama Pasien</label>
											<input type="text" id="nama" name="nama" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
											<input type="hidden" id="nopen" name="nopen" placeholder="[ NAMA PASIEN ]" class="form-control" readonly />
										</div>
										<div class="col-sm-2">
											<label>Tanggal Lahir</label>
											<div class="input-group date">
												<input name="" id="tgl_lahir" placeholder="[ TANGGAL LAHIR ]" class="form-control" readonly> 
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>	
										<div class="col-sm-2">
											<label>Umur</label>
											<input name="" id="umur_t" placeholder="[ TANGGAL LAHIR ]" class="form-control" readonly> 
										</div>	
										<div class="col-sm-2">
											<label>Jenis Kelamin</label>
											<input name="" id="jk" placeholder="[ JENIS KELAMIN ]" class="form-control" readonly> 
										</div>														
									</div>	
									<div class="form-group">
										<div class="col-sm-2">
											<label>Tanggal Masuk</label>					
											<div class="input-group date">
												<input type="text" id="tgl_masuk" name="tglmasukruangan" placeholder="TGL MASUK RUANG RAWAT" class="form-control" autocomplete="off" readonly/>
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-3">
											<label>Asal Ruangan</label>		
											<input type="text" id="id_ruangan" name="asalruangan" placeholder=" [ RUANG RAWAT SAAT INI ]" class="form-control" autocomplete="off" readonly/>
										</div>
										<div class="col-sm-3">
											<label>Diagnosa Primer</label>	
											<input type="text" id="diagmasuk" name="" placeholder=" [ DIAGNOSA MEDIS ]" class="form-control" autocomplete="off" readonly/>
											<input type="hidden" id="icd" name="dxmedis" class="form-control" autocomplete="off" readonly/>
										</div>
										<div class="col-sm-2">
											<label>Tanggal Surveilans</label>					
											<div class="input-group date">
												<input type="text" id="" name="tglsurveilan" placeholder="TGL SURVEILANS" class="form-control datetime " autocomplete="off">
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
										<div class="col-sm-1">
											<label>TB</label>	
											<input type="text" name="tb" placeholder=" [ TB ]" class="form-control" autocomplete="off">
										</div>
										<div class="col-sm-1">
											<label>BB</label>	
											<input type="text" name="bb" placeholder=" [ BB ]" class="form-control" autocomplete="off">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-10">
											<label>Tindakan Operasi</label>		
											<input type="text" name="tindakan" placeholder=" [ TINDAKAN OPERASI ]" class="form-control" autocomplete="off">
										</div>
										<div class="col-sm-2">
											<label>Rencana Operasi</label>					
											<div class="input-group date">
												<input type="text" id="datetimepicker" name="tgl_rencana" placeholder="TGL RENCANA" class="form-control" autocomplete="off">
												<span class="input-group-addon">
													<i class="fa fa-calendar bigger-110"></i>
												</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<label>Dokter Bedah/Operator</label>		
											<select multiple="" name="dokteroperator[]" class="chosen-select form-control" id="form-field-select-4" data-placeholder="Pilih Nama Dokter">
												<option value=""></option>
												<?php
												$sql = "SELECT dok.ID,master.getNamaLengkapPegawai(dok.NIP) DOKTER
												FROM master.dokter dok
												LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
												WHERE p.`STATUS`=1 AND dok.`STATUS`=1";
												$rs = mysqli_query($conn1,$sql);
												while ($data = mysqli_fetch_array($rs)) {
													?>
													<option value="<?=$data[0]?>"><?=$data[1]?></option>
													<?php
												}
												?>	
											</select>
										</div>
										
									</div>


									<div class="widget-header" style="text-align: center;">
										<h4 class="widget-title">PRE/PERI - OPERATIVE PROCESS MEASURES</h4>
									</div>
									<div class="widget-header">
										<h4 class="widget-title">PRE OP (DI RUANGAN RAWAT INAP)</h4>
									</div>

									<div class="row">
										<div class="col-xs-12 col-sm-12">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title">PERSIAPAN PRE-OP</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<div class="col-sm-2">
																<label>Suhu Pasien</label>		
																<input type="text" name="suhu" placeholder=" [ SUHU PASIEN ]" class="form-control" autocomplete="off">
															</div>
															<div class="col-sm-3">
																<label>Hasil Lab Tanggal</label>		
																<input type="text" id="datetimepicker1" name="lab_tanggal" placeholder=" [ TANGGAL HASIL LAB ]" class="form-control" autocomplete="off">
															</div>
															<div class="col-sm-2">
																<label>Glukosa</label>		
																<input type="text" name="glukosa" placeholder=" [ GLUKOSA ]" class="form-control" autocomplete="off">
															</div>

															<div class="col-sm-1">
																<label>HB</label>		
																<input type="text" name="hb" placeholder=" [ HB ]" class="form-control" autocomplete="off">
															</div>
															<div class="col-sm-2">
																<label>Leukosit</label>		
																<input type="text" name="leukosit" placeholder=" [ LEUKOSIT ]" class="form-control" autocomplete="off">
															</div>
															<div class="col-sm-2">
																<label>Albumin</label>		
																<input type="text" name="albumin" placeholder=" [ ALBUMIN ]" class="form-control" autocomplete="off">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12">
											<div class="widget-box">
												<div class="widget-body">
													<div class="widget-main">
														<div>
															<h4 class="media-heading">
																<a href="#" class="blue">Mandi</a>
															</h4>
														</div>
														<p>
															Mulai sore hari sehari sebelum operasi dan pagi hari pada saat mau operasi
														</p>
														<div class="form-group">
															<div class="col-sm-2">
																<label>Tanggal Mandi 1</label>		
																<input type="text" id="datetimepicker11" name="tgl_mandi1" placeholder=" [ TANGGAL MANDI 1 ]" class="form-control" autocomplete="off">
															</div>
															<div class="col-sm-2">
																<label>Tanggal Mandi 2</label>		
																<input type="text"name="tgl_mandi2" id="datetimepicker12" placeholder=" [ TANGGAL MANDI 2 ]" class="form-control" autocomplete="off">
															</div>
															<div class="col-sm-4">
																<label>Mandi menggunakan</label><br>	
																<input type="radio" class="ace" id="sabun" value="1" name="mandi_menggunakan" value="1" autocomplete="off" />
																<label class="lbl" for="sabun"> Sabun anti bakteri/Chlorhexiden; </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input type="radio" class="ace" id="bukan" value="2" name="mandi_menggunakan" value="2" autocomplete="off" />
																<label class="lbl" for="bukan"> Jika bukan, menggunakan</label>
															</div>
															<div class="col-sm-4">
																<label>&nbsp;</label>		
																<input type="text"name="mandi_menggunakan_lain" placeholder=" [ JIKA BUKAN MENGGUNAKAN SABUN ]" class="form-control" autocomplete="off">
															</div>
														</div>
														<div class="form-group">
															<div class="col-sm-4">
																<label>Mencukur rambut menggunakan</label><br>		
																<input type="radio" class="ace" id="razor" value="1" name="alat_cukur" autocomplete="off" />
																<label class="lbl" for="razor"> Razor</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input type="radio" class="ace" id="klipper" value="2" name="alat_cukur" autocomplete="off" />
																<label class="lbl" for="kliper"> Klipper</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input type="radio" class="ace" id="tidak_cukur" value="3" name="alat_cukur" autocomplete="off" />
																<label class="lbl" for="tdkcukur"> Tidak cukur</label>
															</div>
															<div class="col-sm-4">
																<label>Dimana</label><br>
																<input type="radio" class="ace" id="rumah" value="1" name="lokasi_mandi" autocomplete="off" />
																<label class="lbl" for="ok3"> Rumah </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input type="radio" class="ace" id="ruang_perawatan" value="2" name="lokasi_mandi" autocomplete="off" />
																<label class="lbl" for="ok3"> Ruang Perawatan</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input type="radio" class="ace" id="ok" value="3" name="lokasi_mandi" autocomplete="off" />
																<label class="lbl" for="ok3"> OK</label>
															</div>
															<div class="col-sm-2">
																<label>Tanggal Cukur</label>		
																<input type="text"name="tgl_cukur" id="datetimepicker13" placeholder=" [ TANGGAL CUKUR ]" class="form-control" autocomplete="off">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-12">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title">ASA Score</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<div class="col-sm-12">
																<input type="radio" class="ace" name="asascore" value="1" id="asa_score1" autocomplete="off" />
																<label class="lbl" for="asa1"> ASA 1 : Pasien Sehat </label><br>
																<input type="radio" class="ace" name="asascore" value="2" id="asa_score2" autocomplete="off" />
																<label class="lbl" for="asa2"> ASA 2 : Pasien dengan gangguan sistemik ringan sedang
																</label><br>
																<input type="radio" class="ace" name="asascore" value="3" id="asa_score3" autocomplete="off" />
																<label class="lbl" for="asa3"> ASA 3 : Pasien dengan gangguan sistemik berat
																</label><br>
																<input type="radio" class="ace" name="asascore" value="4" id="asa_score4" autocomplete="off" />
																<label class="lbl" for="asa4"> ASA 4 : Pasien dengan gangguan sistemik berat yang mengancam kehidupan
																</label><br>
																<input type="radio" class="ace" name="asascore" value="5" id="asa_score5" autocomplete="off" />
																<label class="lbl" for="asa5"> ASA 5 : Pasien tidak diharapkan hidup walaupun dioperasi atau tidak
																</label>
															</div> 
														</div>
														<div class="form-group">
															<div class="col-sm-3">
																<label>Nama Petugas</label>
																<input name="petugas_asa_score" placeholder="[ NAMA PETUGAS ]" class="form-control"> 
															</div>	
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="widget-header">
										<h4 class="widget-title">INTRA OP (DI RUANGAN OPERASI/IBS)</h4>
									</div>
									<div class="widget-box">
										<div class="widget-header">
											<h4 class="widget-title">KLASIFIKASI LUKA OPERASI</h4>
										</div>
										<div class="widget-body">
											<div class="widget-main">
												<div class="form-group">
													<div class="col-sm-2">
														<label class="lbl" for="ok3"> Bersih</label><br>
													</div>
													<div class="col-sm-10">
														<input type="radio" class="ace" id="bersih" value="1" name="klasifikasi_lukaop" autocomplete="off" />
														<label class="lbl" for="bersih1"> Operasi pada jaringan steril, tidak ada bakteri residen (bedah syaraf, mastectomy, exremitas)</label><br>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-2">
														<label class="lbl" for="ok3"> Bersih-Terkontaminasi </label><br>
													</div>
													<div class="col-sm-10">
														<input type="radio" class="ace" value="2" name="klasifikasi_lukaop" id="bersih_terkontaminasi" autocomplete="off" />
														<label class="lbl" for="bersih2"> Operasi yang membuka traktus yang ada bakteri residen histerektomi, laparotomi, secio, apendictomi, trachesotomy, nefrostomy, operasi pada oroforing</label><br>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-2">
														<label class="lbl" for="ok3"> Terkontaminasi</label><br>
													</div>
													<div class="col-sm-10">
														<input type="radio" class="ace" id="terkontaminasi" value="3" name="klasifikasi_lukaop" autocomplete="off" />
														<label class="lbl" for="bersih3"> Operasi yang masuk ke dalam jaringan dengan bakteri yang tidak terkontrol (atau GI dengan perforasi, Apendektomy dengan perforasi)</label><br>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-2">
														<label class="lbl" for="ok3"> Kotor/Infeksi</label><br>
													</div>
													<div class="col-sm-10">
														<input type="radio" class="ace" id="kotor" value="4" name="klasifikasi_lukaop" autocomplete="off" />
														<label class="lbl" for="bersih4"> Kontaminasi berat (adanya luka infeksi) atau sudah ada infeksi sebelum operasi</label><br>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="widget-box">
										<div class="widget-header">
											<h4 class="widget-title">URGENSI OPERASI</h4>
										</div>
										<div class="widget-body">
											<div class="widget-main">
												<div class="form-group">												
													<div class="col-sm-12">
														<input type="radio" class="ace" id="urgensi_op_1" name="urgensi_op" value="1" autocomplete="off" />
														<label class="lbl" for="urgensi_op_1"> Emergency - harus segera dioperasi untuk menyelamatkan hidup (perdarahan massif)</label><br>
														<input type="radio" class="ace" id="urgensi_op_2" name="urgensi_op" value="2" autocomplete="off" />
														<label class="lbl" for="urgensi_op_2"> Urgent - harus dioperasi dalam waktu 24-48 jam (operasi untuk refair fraktur)</label><br>
														<input type="radio" class="ace" id="urgensi_op_3" name="urgensi_op" value="3" autocomplete="off" />
														<label class="lbl" for="urgensi_op_3"> Semi elektif - harus dioperasi dalam beberapa hari sampai 1 minggu (operasi tumor/kanker)</label><br>
														<input type="radio" class="ace" id="urgensi_op_4" name="urgensi_op" value="4" autocomplete="off" />
														<label class="lbl" for="urgensi_op_3"> Elektif - tidak ada batasan waktu, bisa direncanakan (operasi kosmetik)</label><br>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="widget-box">	
										<div class="widget-body">
											<div class="widget-main">
												<div class="form-group">
													<div class="col-sm-7">
														<label>Ruang Operasi</label><br>		
														<input type="radio" class="ace" id="ok1" value="1" name="ruang_operasi" autocomplete="off" />
														<label class="lbl" for="ok1"> OK1</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="ok2" value="2" name="ruang_operasi" autocomplete="off" />
														<label class="lbl" for="ok2"> OK2</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="ok3" value="3" name="ruang_operasi" autocomplete="off" />
														<label class="lbl" for="ok3"> OK3</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="ok4" value="4" name="ruang_operasi" autocomplete="off" />
														<label class="lbl" for="ok4"> OK4</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="ok5" value="5" name="ruang_operasi" autocomplete="off" />
														<label class="lbl" for="ok5"> OK5</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="ok6" value="6" name="ruang_operasi" autocomplete="off" />
														<label class="lbl" for="ok6"> OK6</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="ok1" value="7" name="ruang_operasi" autocomplete="off" />
														<label class="lbl" for="ok7"> OK7</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="radio" class="ace" id="ok1" value="8" name="ruang_operasi" autocomplete="off" />
														<label class="lbl" for="ok8"> OK8</label>
													</div>
													<div class="col-sm-5">
														<label>Nama operator</label><br>		
														<select multiple="" class="chosen-select form-control" name="nama_operator[]" id="form-field-select-4" data-placeholder="PILIH OPERATOR">
															<option value=""></option>
															<?php
															$sql = "SELECT dok.ID,master.getNamaLengkapPegawai(dok.NIP) DOKTER
															FROM master.dokter dok
															LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
															WHERE p.`STATUS`=1 AND dok.`STATUS`=1";
															$rs = mysqli_query($conn1,$sql);
															while ($data = mysqli_fetch_array($rs)) {
																?>
																<option value="<?=$data[0]?>"><?=$data[1]?></option>
																<?php
															}
															?>	
														</select>
													</div>
													<div class="col-sm-4">
														<label>Asisten</label><br>		
														<select class="chosen-select form-control" name="nama_asisten" data-placeholder="PILIH ASISTEN">
															<option value=""></option>
															<?php
															$sql = "SELECT dok.NIP,master.getNamaLengkapPegawai(dok.NIP) NAMA
															FROM master.dokter dok
															LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
															WHERE p.`STATUS`=1 AND dok.`STATUS`=1
															UNION
															SELECT pr.NIP,master.getNamaLengkapPegawai(pr.NIP) NAMA
															FROM master.perawat pr
															LEFT JOIN master.pegawai p ON p.NIP=pr.NIP
															WHERE p.`STATUS`=1 AND pr.`STATUS`=1";
															$rs = mysqli_query($conn1,$sql);
															while ($data = mysqli_fetch_array($rs)) {
																?>
																<option value="<?=$data[0]?>"><?=$data[1]?></option>
																<?php
															}
															?>	
														</select>
													</div>
													<div class="col-sm-4">
														<label class="lbl" for=""> Asisten Dokter</label>
														<input type="text" class="form-control" placeholder="Asisten Dokter" name="asisten_dokter" autocomplete="off" />
													</div>
													<div class="col-sm-4">
														<label>Perawat Instrumen</label><br>		
														<select class="chosen-select form-control" name="perawat_instrumen" data-placeholder="PILIH PERAWAT">
															<option value=""></option>
															<?php
															$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
															where pg.PROFESI=6";
															$rs = mysqli_query($conn1,$sql);
															while ($data = mysqli_fetch_array($rs)) {
																?>
																<option value="<?=$data[0]?>"><?=$data[1]?></option>
																<?php
															}
															?>	
														</select>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label>Perawat Sirkuler</label><br>
														<select class="chosen-select form-control" name="perawat_sirkuler" data-placeholder="PILIH PERAWAT">
															<option value=""></option>
															<?php
															$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
															where pg.PROFESI=6";
															$rs = mysqli_query($conn1,$sql);
															while ($data = mysqli_fetch_array($rs)) {
																?>
																<option value="<?=$data[0]?>"><?=$data[1]?></option>
																<?php
															}
															?>	
														</select>
													</div>
													<div class="col-sm-4">
														<label>Dokter Anastesi</label><br>		
														<select class="chosen-select form-control" name="dokter_anastesi" data-placeholder="PILIH DOKTER ANASTESI">
															<option value=""></option>
															<?php
															$sql = "SELECT dok.ID,master.getNamaLengkapPegawai(dok.NIP) DOKTER
															FROM master.dokter dok
															LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
															WHERE p.`STATUS`=1 AND dok.`STATUS`=1";
															$rs = mysqli_query($conn1,$sql);
															while ($data = mysqli_fetch_array($rs)) {
																?>
																<option value="<?=$data[0]?>"><?=$data[1]?></option>
																<?php
															}
															?>	
														</select>
													</div>
													<div class="col-sm-4">
														<label>Perawat Anastesi</label><br>		
														<select class="chosen-select form-control" name="perawat_anastesi" data-placeholder="PILIH PERAWAT">
															<option value=""></option>
															<?php
															$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
															where pg.PROFESI=19";
															$rs = mysqli_query($conn1,$sql);
															while ($data = mysqli_fetch_array($rs)) {
																?>
																<option value="<?=$data[0]?>"><?=$data[1]?></option>
																<?php
															}
															?>	
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="widget-box">
										<div class="widget-main">
											<div>
												<h4 class="media-heading">
													<a href="#" class="blue">Antibiotic profilaksis</a>
												</h4>
											</div>

											<div class="form-group">
												<div class="col-sm-6">
													<div class="form-group">												
														<div class="col-sm-12">
															<input type="radio" class="ace" name="ab_profilaksis" value="1" id="antibiotik_tdkbutuh" autocomplete="off" />
															<label class="lbl" for="antibiotik_tdkbutuh"> Tidak dibutuhkan</label><br>
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-12">
															<input type="radio" class="ace" id="dibutuhkan_ab" value="2" name="ab_profilaksis" autocomplete="off" />
															<label class="lbl" for="dibutuhkan_ab"> Dibutuhkan namun tidak dapat diberikan, dengan alasan:</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="ab_dibutuhkan1" name="ab_dibutuhkan" value="1" autocomplete="off" />
															<label class="lbl" for="ab_dibutuhkan1"> Tidak ada obat</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="ab_dibutuhkan2" name="ab_dibutuhkan" value="2" autocomplete="off" />
															<label class="lbl" for="ab_dibutuhkan2"> Alasan lain :</label>
															<input type="text" class="form-control" name="ab_alasanlain" autocomplete="off" />
														</div>
													</div>

												</div>

												<div class="col-sm-6">
													<div class="form-group">												
														<div class="col-sm-12">
															<input type="radio" class="ace" id="ok3" value="3" name="ab_profilaksis" autocomplete="off" />
															<label class="lbl" for="ok3"> Diberikan anti biotic</label><br>
															<input type="text" class="form-control" name="nama_ab_prof" autocomplete="off" />
														</div>
													</div>
													<div class="form-group">
														<div class="col-sm-2">
															<label>Dosis</label>
															<input type="text" class="form-control" name="dosis_ab_prof" autocomplete="off" />
														</div>
														<div class="col-sm-4">
															<label>Waktu pemberian</label>
															<input type="text" class="form-control" name="waktu_ab_prof" autocomplete="off" />
														</div>
														<div class="col-sm-2">
															<label>D. ulang</label>
															<input type="text" class="form-control" name="ulang_ab_prof" autocomplete="off" />
														</div>
														<div class="col-sm-4">
															<label>Waktu pemberian</label>
															<input type="text" class="form-control" name="waktu_pemberian_ulang_ab_prof" autocomplete="off" />
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="widget-box">
										<div class="widget-main">
											<div>
												<h4 class="media-heading">
													<a href="#" class="blue">Preparasi kulit saat pembedahan (kondisi steril)</a>
												</h4>
											</div>
											<div class="form-group">
												<div class="col-sm-8">
													<label>Menggunakan Cairan antiseptik</label><br>		
													<input type="radio" class="ace" id="antiseptik1" value="1" name="antiseptik" autocomplete="off" />
													<label class="lbl" for="antiseptik1"> Chlorhexidin 2% + alkohol 70%</label>&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="antiseptik2" value="2" name="antiseptik" autocomplete="off" />
													<label class="lbl" for="antiseptik2"> Iodin + Alkohol 70%</label>&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="antiseptik3" value="3"name="antiseptik" autocomplete="off" />
													<label class="lbl" for="antiseptik3"> Chlorexidin 2% + aqua</label>&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="antiseptik4" value="4" name="antiseptik" autocomplete="off" />
													<label class="lbl" for="antiseptik4"> Iodin + Aqua</label>
												</div>
												<div class="col-sm-4">
													<label>Teknik preparasi kulit dilakukan dengan benar</label><br>		
													<input type="radio" class="ace" id="teknik_preparasi1" value="1" name="teknik_preparasi" autocomplete="off" />
													<label class="lbl" for="teknik_preparasi1"> Ya</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="teknik_preparasi2" value="2" name="teknik_preparasi" autocomplete="off" />
													<label class="lbl" for="teknik_preparasi2"> Tidak</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
										</div>
									</div>
									<div class="widget-box">
										<div class="widget-main">
											<div>
												<h4 class="media-heading">
													<a href="#" class="blue">Hand hygiene Bedah Menggunakan Cara : </a>
												</h4>
											</div>
											<div class="form-group">
												<div class="col-sm-5">
													<label><b>Operator</b></label><br>		
													<select multiple="" class="chosen-select form-control" name="nama_hh_operator[]" id="form-field-select-4" data-placeholder="Pilih Operator">
														<option value=""></option>
														<?php
														$sql = "SELECT dok.ID,master.getNamaLengkapPegawai(dok.NIP) DOKTER
														FROM master.dokter dok
														LEFT JOIN master.pegawai p ON p.NIP=dok.NIP
														WHERE p.`STATUS`=1 AND dok.`STATUS`=1";
														$rs = mysqli_query($conn1,$sql);
														while ($data = mysqli_fetch_array($rs)) {
															?>
															<option value="<?=$data[0]?>"><?=$data[1]?></option>
															<?php
														}
														?>	
													</select>
												</div>
												<div class="col-sm-5">
													<label>&nbsp;</label><br>		
													<input type="radio" class="ace" id="hh_operator1" value="1" name="hh_operator" autocomplete="off" />
													<label class="lbl" for="hh_operator1"> Handcrub</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="hh_operator2" value="2" name="hh_operator" autocomplete="off" />
													<label class="lbl" for="hh_operator2"> Sabun antiseftik + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="hh_operator3" value="3" name="hh_operator" autocomplete="off" />
													<label class="lbl" for="hh_operator3"> Sabun biasa + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-5">
													<label><b>Perawat instrumen</b></label><br>		
													<select class="chosen-select form-control" name="nama_hh_perawat" data-placeholder="PILIH PERAWAT">
														<option value=""></option>
														<?php
														$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
														where pg.PROFESI=6";
														$rs = mysqli_query($conn1,$sql);
														while ($data = mysqli_fetch_array($rs)) {
															?>
															<option value="<?=$data[0]?>"><?=$data[1]?></option>
															<?php
														}
														?>	
													</select>

												</div>
												<div class="col-sm-5">
													<label>&nbsp;</label><br>		
													<input type="radio" class="ace" id="hh_perawat1" value="1" name="hh_perawat" autocomplete="off" />
													<label class="lbl" for="hh_perawat1"> Handcrub</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="hh_perawat2" value="2" name="hh_perawat" autocomplete="off" />
													<label class="lbl" for="hh_perawat2"> Sabun antiseftik + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="hh_perawat3" value="3" name="hh_perawat" autocomplete="off" />
													<label class="lbl" for="hh_perawat3"> Sabun biasa + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-5">
													<label><b>Sirkuler</b></label><br>		
													<select class="chosen-select form-control" name="nama_hh_sirkuler" id="form-field-select-4" data-placeholder="Sirkuler">
														<option value=""></option>
														<?php
														$sql = "SELECT pg.NIP, master.getNamaLengkapPegawai(pg.NIP) PERAWAT FROM master.pegawai pg
														where pg.PROFESI=6";
														$rs = mysqli_query($conn1,$sql);
														while ($data = mysqli_fetch_array($rs)) {
															?>
															<option value="<?=$data[0]?>"><?=$data[1]?></option>
															<?php
														}
														?>	
													</select>

												</div>
												<div class="col-sm-5">
													<label>&nbsp;</label><br>		
													<input type="radio" class="ace" id="hh_sirkuler1" value="1" name="hh_sirkuler" autocomplete="off" />
													<label class="lbl" for="hh_sirkuler1"> Handcrub</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="hh_sirkuler2" value="2" name="hh_sirkuler" autocomplete="off" />
													<label class="lbl" for="hh_sirkuler2"> Sabun antiseftik + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="hh_sirkuler3" value="3" name="hh_sirkuler" autocomplete="off" />
													<label class="lbl" for="hh_sirkuler3"> Sabun biasa + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-5">
													<label class="lbl" for="ok3"> Asisten</label>
													<input type="text" name="nama_hh_asisten" placeholder="Asisten" class="form-control" autocomplete="off">
												</div>
												<div class="col-sm-5">
													<label>&nbsp;</label><br>		
													<input type="radio" class="ace" id="hh_asisten1" value="1" name="hh_asisten" autocomplete="off" />
													<label class="lbl" for="hh_asisten1"> Handcrub</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="hh_asisten2" value="2" name="hh_asisten" autocomplete="off" />
													<label class="lbl" for="hh_asisten2"> Sabun antiseftik + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="hh_asisten3" value="3" name="hh_asisten" autocomplete="off" />
													<label class="lbl" for="hh_asisten3"> Sabun biasa + air</label>&nbsp;&nbsp;&nbsp;&nbsp;
												</div>
											</div>
										</div>
									</div>
									<div class="widget-box">
										<div class="widget-main">
											<div>
												<h4 class="media-heading">
													<a href="#" class="blue">Traffic Ruang Operasi</a>
												</h4>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label>Total jumlahpetugas saat mulai operasi</label>		
													<input type="number"name="jml_petugas_op" placeholder=" [ JUMLAH ]" class="form-control" autocomplete="off">
												</div>
												<div class="col-sm-4">
													<label>Jumlah orang yang masuk selain petugas operasi</label>		
													<input type="number"name="jml_selain_petugas_op" placeholder=" [ JUMLAH ]" class="form-control" autocomplete="off">
												</div>
												<div class="col-sm-4">
													<label>Jumlah pintu ruang operasi dibuka selama operasi</label>		
													<input type="number"name="jml_pintu_dibuka" placeholder=" [ JUMLAH ]" class="form-control" autocomplete="off">
												</div>
											</div>
										</div>
									</div>

									<div class="widget-box">
										<div class="widget-main">
											<div>
												<h4 class="media-heading">
													<a href="#" class="blue">Drain/implant</a>
												</h4>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<label>Terpasang Drain?</label><br>
													<input type="radio" class="ace" id="terpasang_drain" value="2" name="terpasang_drain" autocomplete="off" />
													<label class="lbl" for="terpasang_drain"> Tidak </label>&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="terpasang_drain" value="1" name="terpasang_drain" autocomplete="off" />
													<label class="lbl" for="terpasang_drain"> Ya, Lokasi </label>
												</div>
												<div class="col-sm-4">
													<input type="text" class="form-control" style="width:350px" name="lokasi_drain" placeholder="LOKASI DRAIN" autocomplete="off" />
												</div>
												<div class="col-sm-6">
													<label>Jika Ya, Tipe Drain?</label><br>
													<input type="radio" class="ace" id="tipe_drain1" value="1" name="tipe_drain" autocomplete="off" />
													<label class="lbl" for="tipe_drain1"> Terbuka </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="tipe_drain2" value="2" name="tipe_drain" autocomplete="off" />
													<label class="lbl" for="tipe_drain2"> Tertutup </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="tipe_drain3" value="3" name="tipe_drain" autocomplete="off" />
													<label class="lbl" for="tipe_drain3"> Vakum </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="tipe_drain4" value="4" name="tipe_drain" autocomplete="off" />
													<label class="lbl" for="tipe_drain4"> Tidak Vakum</label>
												</div>
											</div>
										</div>
									</div>

									<div class="widget-box">
										<div class="widget-main">
											<div>
												<h4 class="media-heading">
													<a href="#" class="blue">Penggunaan alat Re-Use</a>
												</h4>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<input type="radio" name="penggunaan_alat_reuse" class="ace" value="1" id="penggunaan_alat_reuse1" autocomplete="off" />
													<label class="lbl" for="penggunaan_alat_reuse1"> Tidak </label>&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" name="penggunaan_alat_reuse" value="2" id="penggunaan_alat_reuse2" autocomplete="off" />
													<label class="lbl" for="penggunaan_alat_reuse2"> Ya</label>
												</div>

												<div class="col-sm-3">
													<label class="lbl" for="ok3">Nama alat & Nomor Kode </label>
													<input type="text" class="form-control" name="nama_alat_reuse1" id="nama_alat_reuse1" autocomplete="off" />	
													<label>Re-Use ke :</label><br>
													<input type="radio" class="ace" id="alat_reuse11" value="1" name="alat_reuse1" autocomplete="off" />
													<label class="lbl" for="alat_reuse11"> 1 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="alat_reuse12" value="2" name="alat_reuse1" autocomplete="off" />
													<label class="lbl" for="alat_reuse12"> 2 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="alat_reuse13" value="3" name="alat_reuse1" autocomplete="off" />
													<label class="lbl" for="alat_reuse13"> 3 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="alat_reuse14" value="4" name="alat_reuse1" autocomplete="off" />
													<label class="lbl" for="alat_reuse14"> 4</label>
												</div>
												<div class="col-sm-3">
													<label class="lbl" for="ok3">Nama alat & Nomor Kode </label>
													<input type="text" class="form-control" name="nama_alat_reuse2" id="nama_alat_reuse2" autocomplete="off" />	
													<label>Re-Use ke :</label><br>
													<input type="radio" class="ace" id="alat_reuse21" value="1" name="alat_reuse2" autocomplete="off" />
													<label class="lbl" for="alat_reuse21"> 1 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="alat_reuse22" value="2" name="alat_reuse2" autocomplete="off" />
													<label class="lbl" for="alat_reuse22"> 2 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="alat_reuse23" value="3" name="alat_reuse2" autocomplete="off" />
													<label class="lbl" for="alat_reuse23"> 3 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="alat_reuse24" value="4" name="alat_reuse2" autocomplete="off" />
													<label class="lbl" for="alat_reuse24"> 4</label>
												</div>
												<div class="col-sm-3">
													<label class="lbl" for="ok3">Nama alat & Nomor Kode </label>
													<input type="text" class="form-control" name="nama_alat_reuse3" id="nama_alat_reuse3" autocomplete="off" />	
													<label>Re-Use ke :</label><br>
													<input type="radio" class="ace" id="alat_reuse31" value="1" name="alat_reuse3" autocomplete="off" />
													<label class="lbl" for="alat_reuse31"> 1 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="alat_reuse32" value="2" name="alat_reuse3" autocomplete="off" />
													<label class="lbl" for="alat_reuse32"> 2 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="alat_reuse33" value="3" name="alat_reuse3" autocomplete="off" />
													<label class="lbl" for="alat_reuse33"> 3 </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type="radio" class="ace" id="alat_reuse34" value="4" name="alat_reuse3" autocomplete="off" />
													<label class="lbl" for="alat_reuse34"> 4</label>
												</div>
											</div>
										</div>
									</div>

									<div class="widget-box">
										<div class="widget-main">
											<div class="row">
												<div class="col-sm-4">
													<div>
														<h4 class="media-heading">
															<a href="#" class="blue">Penggunaan implant</a>
														</h4>
													</div>
													<div class="form-group">
														<div class="col-sm-6">
															<input type="radio" class="ace" id="penggunaan_implant1" value="1" name="penggunaan_implant" autocomplete="off" />
															<label class="lbl" for="penggunaan_implant1"> Ya, jenis implant</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="jns_implant_metal" value="1" name="jns_implant" autocomplete="off" />
															<label class="lbl" for="jns_implant_metal"> Metal</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="jns_implant_skin" value="2" name="jns_implant" autocomplete="off" />
															<label class="lbl" for="jns_implant_skin"> Skin graf</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="penggunaan_implant2" name="penggunaan_implant" value="2" autocomplete="off" />
															<label class="lbl" for="penggunaan_implant2"> Lain-lain tuliskan</label>
															<span></span><input type="text" class="form-control" name="jns_implant_lain" autocomplete="off" /><br>
															<input type="radio" class="ace" id="penggunaan_implant3" value="3" name="penggunaan_implant" autocomplete="off" />
															<label class="lbl" for="penggunaan_implant3"> Tidak</label>
														</div>
													</div>
												</div>
												<div class="col-sm-6">
													<div>
														<h4 class="media-heading">
															<a href="#" class="blue">ANTIBIOTIK (AB) POST OPERASI</a>
														</h4>
													</div>
													<div class="form-group">
														<div class="col-sm-6">
															<label>Apakah AB diberikan Post ooperasi</label><br>	
															<input type="radio" class="ace" id="ab_post_op_ya" value="1" name="ab_post_op" autocomplete="off" />
															<label class="lbl" for="ab_post_op_ya"> Ya, tuliskan</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" placeholder="NAMA ANTI BIOTIK" name="nama_ab_op" class="ace" id="ok3" autocomplete="off" /><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" placeholder="DOSIS" class="ace" name="dosis_ab_op" id="ok3" autocomplete="off" /><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="number" placeholder="SELAMA" name="selama_ab_op" class="ace" id="ok3" autocomplete="off" /><br><br>
															<input type="radio" class="ace" id="ab_post_op_tdk" value="2" name="ab_post_op" autocomplete="off" />
															<label class="lbl" for="ab_post_op_tdk"> Tidak</label>


														</div>
														<div class="col-sm-6">
															<label class="lbl" for="ok3"> Alasan :</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="alasan_pos_op1" name="alasan_pos_op" value="1" autocomplete="off" />
															<label class="lbl" for="alasan_pos_op1"> Post OP Profilaksis</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="alasan_pos_op2" name="alasan_pos_op" value="2" autocomplete="off" />
															<label class="lbl" for="alasan_pos_op2"> Drain/implant</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="alasan_pos_op3" name="alasan_pos_op" value="3" autocomplete="off" />
															<label class="lbl" for="alasan_pos_op3"> Infeksi</label><br>
															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" class="ace" id="alasan_pos_op4" name="alasan_pos_op" value="4" autocomplete="off" />
															<label class="lbl" for="alasan_pos_op4"> Alasan lain</label>
															<span></span><input type="text" class="form-control" name="alasan_lain_post_op" autocomplete="off" /><br>

														</div>
													</div>

												</div>
											</div>
										</div>
									</div>

									<div class="widget-box">
										<div class="widget-main">
											<div>
												<h4 class="media-heading">
													<a href="#" class="blue">Hasil Laboratorium intra Op/Post Op</a>
												</h4>
											</div>
											<div class="form-group">
												<div class="col-sm-2">
													<input type="checkbox" class="ace" id="hasil_lab_intra1" value="1" name="hasil_lab_leukosit" autocomplete="off" />
													<label class="lbl" for="hasil_lab_intra1"> Leukosit </label>
													<input type="text" class="form-control" name="isi_lab_leukosit" style="width:150px" autocomplete="off" />
												</div>
												<div class="col-sm-2">
													<input type="checkbox" class="ace" id="hasil_lab_intra2" value="1" name="hasil_lab_crcp" autocomplete="off" />
													<label class="lbl" for="hasil_lab_intra2"> CRP</label>
													<input type="text" class="form-control" name="isi_lab_crcp" style="width:150px" autocomplete="off" />
												</div>
												<div class="col-sm-2">
													<input type="checkbox" class="ace" id="hasil_lab_intra3" value="1" name="hasil_lab_pct" autocomplete="off" />
													<label class="lbl" for="hasil_lab_intra3"> PCT (Procalcitonin)</label>
													<input type="text" class="form-control" name="isi_lab_pct" style="width:150px" autocomplete="off" />
												</div>
												<div class="col-sm-2">
													<input type="checkbox" class="ace" id="hasil_lab_intra4" value="1" name="hasil_lab_gds" autocomplete="off" />
													<label class="lbl" for="hasil_lab_intra4"> GDS</label>
													<input type="text" class="form-control" name="isi_lab_gds" style="width:150px" autocomplete="off" />
												</div>
												<div class="col-sm-4">
													<label class="lbl" for="ok3"> Tanggal</label>
													<div class="input-group date">
														<input type="text" id="datetimepicker14" name="tgl_hasil_lab_op" placeholder="TGL HASIL LAB INTRA OP/POST OP" class="form-control" autocomplete="off">
														<span class="input-group-addon">
															<i class="fa fa-calendar bigger-110"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="widget-box">
										<div class="widget-main">
											<div class="form-group">
												<div class="col-sm-4">
													<label class="lbl" for="ok3"> Tanggal pengisian form</label>
													<div class="input-group date">
														<input type="text" id="datet1" name="tgl_isi_form" placeholder="TANGGAL PENGISIAN FORM" class="form-control datetimepicker1" autocomplete="off">
														<span class="input-group-addon">
															<i class="fa fa-calendar bigger-110"></i>
														</span>
													</div>
												</div>
												<div class="col-sm-4">
													<label class="lbl" for="ok3"> Tanggal selesai pegisian form</label>
													<div class="input-group date">
														<input type="text" id="datet3" name="tgl_selesai_isi" placeholder="TANGGAL SELESAI PENGISIAN" class="form-control" autocomplete="off">
														<span class="input-group-addon">
															<i class="fa fa-calendar bigger-110"></i>
														</span>
													</div>
												</div>
												<div class="col-sm-4">
													<label class="lbl" for="ok3"> Nama Petugas</label>
													<input type="text" id="tgl_masuk" name="nama_petugas" placeholder="NAMA PETUGAS" class="form-control" autocomplete="off">
												</div>
											</div>
										</div>
									</div>
									<hr />
									<div class="form-group">							
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" name="btnsimpan" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												%
												Reset
											</button>
										</div>
									</div>
								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>